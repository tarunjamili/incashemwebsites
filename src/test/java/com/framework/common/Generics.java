package com.framework.common;

import com.framework.configurations.Configuration;
import com.framework.logger.TestLogger;
import com.github.javafaker.Faker;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.Coordinates;
import org.openqa.selenium.interactions.Locatable;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.IResultMap;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.internal.Utils;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by Rahul R.
 * Date: 2019-03-19
 * Time:
 * Project Name: InCashMe
 */

public class Generics extends TestLogger {

    protected static int _logStep = 1;

    private static Faker faker = new Faker(new Locale("en-IND"));

    public static String username;
    public static String password;
    protected static String testData;
    protected static String externalEmail;
    protected static String externalPassword;
    protected static String externalNumber;
    protected static String createUserNumber;

    protected static WebDriverWait wait;

    /**
     * To find element by given xpath locator
     *
     * @param driver  Instance of WebDriver
     * @param locator Locator String
     * @return WebElement by passed locator
     */
    public static WebElement findElementByXPath(WebDriver driver, String locator) {
        return driver.findElement(By.xpath(locator));
    }

    /**
     * To find element list by given xpath locator
     *
     * @param driver  Instance of WebDriver
     * @param locator Locator String
     * @return WebElement by passed locator
     */
    public static List<WebElement> findElementsByXPath(WebDriver driver, String locator) {
        return driver.findElements(By.xpath(locator));
    }

    /**
     * To find element by given Name locator
     *
     * @param driver  Instance of WebDriver
     * @param locator Locator String
     * @return WebElement by passed locator
     */
    public static WebElement findElementByName(WebDriver driver, String locator) {
        return driver.findElement(By.name(locator));
    }

    /**
     * To find element list by given Name locator
     *
     * @param driver  Instance of WebDriver
     * @param locator Locator String
     * @return WebElement by passed locator
     */
    public static List<WebElement> findElementsByName(WebDriver driver, String locator) {
        return driver.findElements(By.name(locator));
    }

    /**
     * To find element by given ID locator
     *
     * @param driver  Instance of WebDriver
     * @param locator Locator String
     * @return WebElement by passed locator
     */
    public static WebElement findElementByID(WebDriver driver, String locator) {
        return driver.findElement(By.id(locator));
    }

    /**
     * To find element list by given ID locator
     *
     * @param driver  Instance of WebDriver
     * @param locator Locator String
     * @return WebElement by passed locator
     */
    public static List<WebElement> findElementsByID(WebDriver driver, String locator) {
        return driver.findElements(By.id(locator));
    }

    /**
     * Pause for passed seconds
     *
     * @param secs Time in Seconds
     */
    public static void pause(int secs) {
        try {
            Thread.sleep(secs * 1000);
        } catch (InterruptedException interruptedException) {
            System.out.println("Failure in Pause.");
        }
    }

    /**
     * To clear and send the value to the text field
     *
     * @param webElement WebElement
     * @param value      String Value
     */
    public static void type(WebElement webElement, String value) {
        clear(webElement);
        pause(1);
        webElement.sendKeys(value);
    }

    /**
     * To clear the value from the text field
     *
     * @param webElement WebElement
     */
    public static void clear(WebElement webElement) {
        webElement.clear();
    }

    public static void sendKeys(WebElement webElement, String value) {
        webElement.sendKeys(value);
    }

    /**
     * To click on particular WebElement
     *
     * @param driver  WebDriver
     * @param element WebElement
     */
    public static void clickOn(WebDriver driver, WebElement element) {
        pause(1);
        try {
            element.click();
        } catch (ElementClickInterceptedException ex) {
            clickOnJS(driver, element);
        }
    }

    public static void clickOnJS(WebDriver driver, WebElement element) {
        wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(element));
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", element);
    }

    /**
     * To get text from the element
     *
     * @param element WebElement
     * @return Text from the WebElement
     */
    public static String getText(WebElement element) {
        //wait.until(ExpectedConditions.visibilityOfAllElements(element));
        return element.getText().trim();
    }

    /**
     * To get text from the element
     *
     * @param element WebElement
     * @return Text from the WebElement
     */
    public static String getInnerText(WebElement element) {
        return element.getAttribute("innerText").trim();
    }

    /**
     * To get text from the element
     *
     * @param element WebElement
     * @return Text from the WebElement
     */
    public static String getTextJS(WebDriver driver, WebElement element) {
        return ((JavascriptExecutor) driver).executeScript("return $(arguments[0]).text();", element).toString();
    }

    /**
     * To check if element is available in page or not
     *
     * @param element WebElement
     * @return if web element display or not
     */
    public static boolean isElementDisplay(WebElement element) {
        return element.isDisplayed();
    }

    /**
     * To check if element is available in page or not
     *
     * @param element WebElement
     * @return if web element display or not
     */
    public static boolean isElementPresent(WebElement element) {
        try {
            return element.isDisplayed();
        } catch (NoSuchElementException nse) {
            return false;
        }
    }

    public static boolean isElementPresent(WebDriver driver, String locator) {
        try {
            return findElementByXPath(driver, locator).isDisplayed();
        } catch (NoSuchElementException nse) {
            return false;
        }
    }

    /**
     * @param driver  WebDriver
     * @param element WebElement
     */
    public static void scrollToElement(WebDriver driver, WebElement element) {
        Actions actions = new Actions(driver);
        actions.moveToElement(element);
        actions.perform();
        pause(1);
    }

    public static void scrollElement(WebElement element) {
        Coordinates cor = ((Locatable) element).getCoordinates();
        cor.inViewPort();
        pause(2);
    }

    public static void scrollToBottom(WebDriver driver) {
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
    }


    /**
     * To highlight the selected element
     *
     * @param driver  WebDriver
     * @param element WebElement
     */
    private static void highlightElement(WebDriver driver, WebElement element) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].style.border = '3px solid yellow'", element);
        pause(2);
    }

    /**
     * To open the URL in browser window
     *
     * @param driver WebDriver
     * @param url    URL String
     */
    public static void openURL(WebDriver driver, String url) {
        driver.get(url);
    }

    public static String getTitle(WebDriver driver) {
        return driver.getTitle();
    }

    public static void close(WebDriver driver) {
        driver.close();
    }

    /**
     * To refresh the web page
     *
     * @param driver WebDriver
     */
    public static void refresh(WebDriver driver) {
        driver.navigate().refresh();
    }

    /**
     * To get string with the random characters for with the passed characters length limit
     *
     * @param length String length
     * @return Random string
     */
    public static String getRandomCharacters(int length) {
        return RandomStringUtils.randomAlphabetic(length);
    }

    /**
     * To get the invalid email address
     *
     * @return Invalid Email Address
     */
    public static String getInvalidEmail() {
        return getRandomFirstName().toLowerCase() + "." + getRandomLastName().toLowerCase() + "testmail.com";
    }

    public static String getRegistrationEmail() {
        return "tarun.jamili" + System.currentTimeMillis() + "@mailinator.com";
    }

    public static String getRandomFirstName() {
        return faker.name().firstName();
    }

    public static String getRandomLastName() {
        return faker.name().lastName();
    }

    public static String getRandomStreetName() {
        return faker.address().streetName();
    }

    public static String getBuildingNumber() {
        return faker.address().buildingNumber();
    }

    public static String getRandomVillage() {
        return faker.address().cityName();
    }

    public static String getRandomUsername() {
        return faker.name().username();
    }

    public static String getRandomDistrict() {
        return faker.address().city();
    }

    public static String getRandomCountry() {
        return faker.address().country();
    }

    public static String getRandomPinCode() {
        return faker.address().zipCode();
    }

    public static String getRandomBusinessName() {
        return faker.company().name();
    }

    public static String getRandomDBAName() {
        return faker.company().profession();
    }

    public static String getRandomURL() {
        return faker.company().url();
    }

    public static String getRandomGSTNumber() {
        return getRandomCharacters(2).toUpperCase() + getRandomCharacters(5).toUpperCase()
                + getRandomNumberBetween(1000, 9999) + (int) Math.round(Math.random()) +
                getRandomCharacters(1).toUpperCase() + "Z" + (int) Math.round(Math.random());
    }

    /**
     * To get the random email address or unregistered email address
     *
     * @return Random Email Address
     */
    public static String getUnRegisteredEmail() {
        return getRandomFirstName().toLowerCase() + "." + getRandomLastName().toLowerCase() + "@testmail.com";
    }

    public static boolean getRandomBoolean() {
        return Math.random() < 0.5;
    }

    public static String getRandomMessage() {
        return faker.lorem().fixedString(100);
    }

    /**
     * To get the random password
     *
     * @return Random Password
     */
    public static String getRandomPassword() {
        return getRandomCharacters(3).toLowerCase() +
                getRandomCharacters(2).toUpperCase() + "@" + getRandomNumber();
    }

    public static int getRandomPIN() {
        return getRandomNumberBetween(1000, 9999);
    }

    /**
     * To get the random number from 10000 to 99999
     *
     * @return Random Number
     */
    public static int getRandomNumber() {
        return faker.number().numberBetween(10000, 99999);
    }

    /**
     * To get the random number from 10000 to 99999
     *
     * @return Random Number
     */
    public static int getRandomNumberBetween(int min, int max) {
        return faker.number().numberBetween(min, max);
    }

    /**
     * To check if the list is empty or not
     *
     * @param list List
     * @return true if list is empty
     */
    public static boolean isListEmpty(List list) {
        return list.size() == 0;
    }

    public static int sizeOf(List list) {
        return list.size();
    }

    public static int lastIndexOf(List list) {
        return sizeOf(list) - 1;
    }

    public static int getIntegerFromString(String str) {
        return Integer.parseInt(str.replaceAll("[^0-9.]+", ""));
    }

    public static long getLongFromString(String str) {
        return Long.parseLong(str.replaceAll("[^0-9.]+", ""));
    }

    public static double getDoubleFromString(String str) {
        return Double.parseDouble(str.replaceAll("[^0-9.]+", ""));
    }

    public static double formatTwoDecimal(double num) {
        DecimalFormat format = new DecimalFormat("0.00");
        return getDoubleFromString(format.format(num));
    }

    public static void switchToWindow(WebDriver driver) {
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
    }

    public static void switchToFrame(WebDriver driver, String id) {
        driver.switchTo().frame(id);
    }

    /**
     * To get the failure exception in single line
     *
     * @param tests Test Result
     */
    public static void getShortException(IResultMap tests) {

        for (ITestResult result : tests.getAllResults()) {

            Throwable exception = result.getThrowable();
            List<String> msgs = Reporter.getOutput(result);
            boolean hasReporterOutput = msgs.size() > 0;
            boolean hasThrowable = exception != null;
            if (hasThrowable) {
                boolean wantsMinimalOutput = result.getStatus() == ITestResult.SUCCESS;
                if (hasReporterOutput) {
                    testInfoLog((wantsMinimalOutput ? "Expected Exception" : "Failure Reason:"), "");
                }

                String str = Utils.shortStackTrace(exception, true);
                System.out.println(str);
                Scanner scanner = new Scanner(str);
                String firstLine = scanner.nextLine();
                testValidationLog(firstLine);
            }
        }
    }

    /**
     * To kill IE Server Instance once execution completed
     */
    public static void killIEServer() {
        try {
            pause(5);
            String[] cmd = new String[3];
            cmd[0] = "cmd.exe";
            cmd[1] = "/C";
            cmd[2] = "taskkill /F /IM iexplore.exe";
            Process process = Runtime.getRuntime().exec(cmd);
            Process process1 = Runtime.getRuntime().exec("taskkill /F /IM IEDriverServer.exe");
            System.err.println(process + "" + process1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void setPassword(String user, String key, String value) {
        Configuration.writeProperty(user, key, value);
    }

    public static String getPropertyValueOf(String user, String key) {
        return Configuration.getUserProperty(user, key);
    }

    public static void setAttribute(WebDriver driver, String id, String attr) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("document.getElementById('" + id + "').setAttribute('attr', '" + attr + "')");
    }

    public static long getMonthDifferenceBetween(LocalDate joinMonthYear, LocalDate currentMonthYear) {
        return ChronoUnit.MONTHS.between(
                joinMonthYear.withDayOfMonth(1),
                currentMonthYear.withDayOfMonth(1));
    }

    public static String getLastFileModified(String dir) {
        File fl = new File(dir);
        File[] files = fl.listFiles(new FileFilter() {
            public boolean accept(File file) {
                return file.isFile();
            }
        });
        long lastMod = Long.MIN_VALUE;
        File choice = null;
        for (File file : files) {
            if (file.lastModified() > lastMod) {
                choice = file;
                lastMod = file.lastModified();
            }
        }
        return choice.getName();
    }

    public static void createDownloadDirectory() {
        File directory = new File(FILE_DOWNLOAD_PATH);

        if (!directory.exists()) {
            directory.mkdir();
        }
    }

    public static void deleteDownloadDirectory() {
        try {
            FileUtils.deleteDirectory(new File(FILE_DOWNLOAD_PATH));
        } catch (IOException io) {
            testValidationLog("Failed to delete the folder.");
        }
    }

    public void updateShowList(WebDriver driver, WebElement select, String option) {

        scrollToElement(driver, select);

        Select showView = new Select(select);

        switch (option.toLowerCase()) {
            case "10":
                showView.selectByValue("10");
                break;
            case "50":
                showView.selectByValue("50");
                break;
            case "100":
                showView.selectByValue("100");
                break;
            default:
                showView.selectByValue("-1");
                break;
        }

        testStepsLog(_logStep++, "Change Show List : " + option);
    }

    public static Double findMin(List<Double> list) {
        if (list == null || list.size() == 0) {
            return Double.MAX_VALUE;
        }
        return Collections.min(list);
    }

    public static Double findMax(List<Double> list) {
        if (list == null || list.size() == 0) {
            return Double.MIN_VALUE;
        }
        return Collections.max(list);
    }

    public static void makeScreenshot(WebDriver driver, String screenshotName) {

        WebDriver augmentedDriver = new Augmenter().augment(driver);

        File screenshot = ((TakesScreenshot) augmentedDriver).getScreenshotAs(OutputType.FILE);
        String nameWithExtension = screenshotName + ".png";

        try {
            String reportFolder = "test-output" + File.separator;
            String screenshotsFolder = "screenshots";
            File screenshotFolder = new File(reportFolder + screenshotsFolder);
            if (!screenshotFolder.getAbsoluteFile().exists()) {
                screenshotFolder.mkdir();
            }
            FileUtils.copyFile(screenshot,
                    new File(screenshotFolder + File.separator + nameWithExtension).getAbsoluteFile());
        } catch (IOException e) {
            testWarningLog("Failed to capture screenshot: " + e.getMessage());
        }
        testInfoLog("Step Failure<br>Please check attached screenshot : <br><br>", getScreenshotLink(nameWithExtension));
    }

    public static String getScreenshotLink(String screenshot_name) {
        return "<img src='../test-output/screenshots/" + screenshot_name + "' width='683' height='384'/>";
    }

    public static String getExtentScreenShot(WebDriver driver, String screenshot_name) {
        String destination = "";
        try {
            String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
            TakesScreenshot ts = (TakesScreenshot) driver;
            File source = ts.getScreenshotAs(OutputType.FILE);
            destination = System.getProperty("user.dir") + "/Screenshots/" + screenshot_name + dateName + ".png";
            File finalDestination = new File(destination);
            FileUtils.copyFile(source, finalDestination);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return destination;
    }

    public static String getCurrentTimeStampString() {

        Date date = new Date();

        SimpleDateFormat sd = new SimpleDateFormat("MMddHHmmssSS");
        TimeZone timeZone = TimeZone.getDefault();
        Calendar cal = Calendar.getInstance(new SimpleTimeZone(timeZone.getOffset(date.getTime()), "GMT"));
        sd.setCalendar(cal);
        return sd.format(date);
    }

    public void stepFailure(WebDriver driver) {
        makeScreenshot(driver, getCurrentTimeStampString());
        TestLogger.failure();
    }

    public void stepPassed() {
        success();
    }

    public void deleteCookies(WebDriver driver) {
        driver.manage().deleteAllCookies();
    }

    public void quit(WebDriver driver) {
        driver.quit();
    }

    public void maximizeWindow(WebDriver driver) {
        driver.manage().window().maximize();
    }

    public void implicitWaitOf(WebDriver driver, int seconds) {
        driver.manage().timeouts().implicitlyWait(seconds, TimeUnit.SECONDS);
    }

    public static void openMailinator(WebDriver driver, String email) {
        String url = "https://www.mailinator.com/";
        openURL(driver, url);
        type(findElementByXPath(driver, ".//*[@id='inboxfield']"), email);
        clickOn(driver, findElementsByXPath(driver, "//button[contains(text(),'Go')]").get(0));
        pause(2);
    }

    public static String openEmailAndGetURL(WebDriver driver) {
        clickOn(driver, findElementsByXPath(driver, "//table//td[contains(text(),'donotreply@incashme.com')]").get(0));
        pause(1);
        switchToFrame(driver, "msg_body");
        String url = findElementByXPath(driver, "//a[contains(text(),'Recover Password')]").getAttribute("href");
        return url;
    }

    public static void deleteMail(WebDriver driver) {
        driver.navigate().back();
        clickOn(driver, findElementsByXPath(driver, "//table//input[contains(@id,'check')]").get(0));
        pause(1);
        clickOn(driver, findElementByXPath(driver, "//div//button[contains(@title,'Delete')]"));
        pause(1);
        if (isElementPresent(findElementByXPath(driver, "//*[contains(text(),'1 message deleted')]"))) {
            System.err.println("Failed to delete the message, please recheck in mailinator account.");
        } else {
            System.out.println("Message deleted successfully");
        }
    }

    public static int getRandomIndex(List list) {
        return getRandomNumberBetween(0, lastIndexOf(list));
    }

    public static String getNumericMonth(String month) {

        String num;

        switch (month) {
            case "Jan":
                num = "01";
                break;
            case "Feb":
                num = "02";
                break;
            case "Mar":
                num = "03";
                break;
            case "Apr":
                num = "04";
                break;
            case "May":
                num = "05";
                break;
            case "Jun":
                num = "06";
                break;
            case "Jul":
                num = "07";
                break;
            case "Aug":
                num = "08";
                break;
            case "Sep":
                num = "09";
                break;
            case "Oct":
                num = "10";
                break;
            case "Nov":
                num = "11";
                break;
            case "Dec":
                num = "12";
                break;
            default:
                num = "00";
                break;
        }

        return num;
    }

    public static LinkedHashMap<String, String> getTestLoginDetails(String wb, String st, String tc) {

        XSSFWorkbook workbook;

        LinkedHashMap<String, String> credentials = new LinkedHashMap<String, String>();
        LinkedList<String> data = new LinkedList<String>();

        try {
            workbook = new XSSFWorkbook(new FileInputStream(wb));

            String value = "";

            int noOfSheets = workbook.getNumberOfSheets();

            for (int i = 0; i < noOfSheets; i++) {
                if (workbook.getSheetName(i).equalsIgnoreCase(st)) {
                    XSSFSheet sheet = workbook.getSheetAt(i);
                    for (int rowNumber = 0; rowNumber <= sheet.getLastRowNum(); rowNumber++) {
                        XSSFRow row = sheet.getRow(rowNumber);
                        if (row.getCell(1).getStringCellValue().equalsIgnoreCase(tc)) {
                            for (int columnNumber = 0; columnNumber < row.getLastCellNum(); columnNumber++) {
                                XSSFCell c = row.getCell(columnNumber);
                                if (c != null) {
                                    switch (c.getCellType()) {
                                        case NUMERIC:
                                            if (DateUtil.isCellDateFormatted(c)) {
                                                value = (new SimpleDateFormat(
                                                        "dd-MM-yyyy").format(
                                                        c.getDateCellValue()));
                                                data.add(value);
                                            } else {
                                                value = (long) (c.getNumericCellValue()) + "";
                                                data.add(value);
                                            }
                                            break;
                                        case STRING:
                                            value = c.getStringCellValue();
                                            data.add(value);
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            testValidationLog("Failed to load Excel Sheet.");
        }

        credentials.put("username", data.get(2));
        credentials.put("password", data.get(3));
        if (data.size() > 4) {
            credentials.put("testData", data.get(4));
        }

        System.out.println("Test Name : " + tc);
        System.out.println("Credentials : " + credentials);

        return credentials;
    }

    protected static void getTestCredentials(String methodName) {

        LinkedHashMap<String, String> credentials;

        if (methodName.toLowerCase().split("_")[0].equalsIgnoreCase("superAgent")) {
            if (URL.contains("dev."))
                credentials = getTestLoginDetails(WORKBOOK, "Dev_" + SUPER_AGENT, methodName);
            else
                credentials = getTestLoginDetails(WORKBOOK, "Prod_" + SUPER_AGENT, methodName);
            username = credentials.get("username");
            password = credentials.get("password");
            if (credentials.containsKey("testData")) createUserNumber = credentials.get("testData").split("\\.")[0];
        } else if (methodName.toLowerCase().split("_")[0].equalsIgnoreCase("agent")) {
            if (URL.contains("dev."))
                credentials = getTestLoginDetails(WORKBOOK, "Dev_" + AGENT, methodName);
            else
                credentials = getTestLoginDetails(WORKBOOK, "Prod_" + AGENT, methodName);
            username = credentials.get("username");
            password = credentials.get("password");
            if (credentials.containsKey("testData")) testData = credentials.get("testData").split("\\.")[0];
        } else if (methodName.toLowerCase().split("_")[0].equalsIgnoreCase("merchant")) {
            if (URL.contains("dev."))
                credentials = getTestLoginDetails(WORKBOOK, "Dev_" + MERCHANT, methodName);
            else
                credentials = getTestLoginDetails(WORKBOOK, "Prod_" + MERCHANT, methodName);
            username = credentials.get("username");
            password = credentials.get("password");
            if (credentials.containsKey("testData")) testData = credentials.get("testData");
            try {
                externalNumber = testData.split(",")[0];
            } catch (Exception e) {
                System.out.println("No Mobile Number/Pin entered");
            }
            try {
                externalEmail = testData.split(",")[1];
            } catch (Exception e) {
                System.out.println("No Email ID entered");
            }
            try {
                externalPassword = testData.split(",")[2];
            } catch (Exception e) {
                System.out.println("No Password entered");
            }
        } else if (methodName.toLowerCase().split("_")[0].equalsIgnoreCase("consumer")) {
            if (URL.contains("dev."))
                credentials = getTestLoginDetails(WORKBOOK, "Dev_" + CONSUMER, methodName);
            else
                credentials = getTestLoginDetails(WORKBOOK, "Prod_" + CONSUMER, methodName);
            username = credentials.get("username");
            password = credentials.get("password");
            if (credentials.containsKey("testData")) testData = credentials.get("testData");
            try {
                externalNumber = testData.split(",")[0];
            } catch (Exception e) {
                System.out.println("No Mobile Number/Pin entered");
            }
            try {
                externalEmail = testData.split(",")[1];
            } catch (Exception e) {
                System.out.println("No Email ID entered");
            }
            try {
                externalPassword = testData.split(",")[2];
            } catch (Exception e) {
                System.out.println("No Password entered");
            }
        }
    }

    public static void scrollToTop(WebDriver driver) {

        WebElement element = driver.findElement(By.tagName("header"));

        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView();", element);

    }

    public static void openLinkInNewWindow(WebDriver driver, String url) {
        ((JavascriptExecutor) driver).executeScript("window.open(arguments[0])", url);
        switchToWindow(driver);
    }

    public static void closeCurrentWindow(WebDriver driver) {
        driver.close();
        switchToWindow(driver);
    }

    public void mouseHoverTo(WebDriver driver, WebElement element) {
        Actions action = new Actions(driver);
        action.moveToElement(element).build().perform();
        pause(1);
    }

}
