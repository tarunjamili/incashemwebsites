package com.framework.init;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.framework.common.Generics;
import com.framework.configurations.Configuration;
import com.framework.logger.TestLogger;
import com.incashme.agent.indexpage.*;
import com.incashme.agent.verification.*;
import com.incashme.consumer.indexpage.*;
import com.incashme.consumer.verification.*;
import com.incashme.merchant.indexpage.*;
import com.incashme.merchant.verification.*;
import com.incashme.superAgent.indexpage.*;
import com.incashme.superAgent.verification.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import java.lang.reflect.Method;

/**
 * Created by Rahul R.
 * Date: 2019-03-19
 * Time:
 * Project Name: InCashMe
 */

public class SeleniumInit extends Generics implements Configuration {

    public WebDriver driver;

    protected SuperAgentLoginIndexPage superAgentLoginIndexPage;
    protected SuperAgentLoginVerification superAgentLoginVerification;

    protected SuperAgentDashboardIndexPage superAgentDashboardIndexPage;
    protected SuperAgentDashboardVerification superAgentDashboardVerification;

    protected SuperAgentStatementIndexPage superAgentStatementIndexPage;
    protected SuperAgentStatementVerification superAgentStatementVerification;

    protected SuperAgentCommissionIndexPage superAgentCommissionIndexPage;
    protected SuperAgentCommissionVerification superAgentCommissionVerification;

    protected SuperAgentAgentListIndexPage superAgentAgentListIndexPage;
    protected SuperAgentAgentListVerification superAgentAgentListVerification;

    protected AgentLoginIndexPage agentLoginIndexPage;
    protected AgentLoginVerification agentLoginVerification;

    protected AgentDashboardIndexPage agentDashboardIndexPage;
    protected AgentDashboardVerification agentDashboardVerification;

    protected AgentTargetTopUpDetailsIndexPage agentTargetTopUpDetailsIndexPage;
    protected AgentTargetTopUpDetailsVerification agentTargetTopUpDetailsVerification;

    protected AgentConsumerTargetDetailsIndexPage agentConsumerTargetDetailsIndexPage;
    protected AgentConsumerTargetDetailsVerification agentConsumerTargetDetailsVerification;

    protected AgentMerchantTargetDetailsIndexPage agentMerchantTargetDetailsIndexPage;
    protected AgentMerchantTargetDetailsVerification agentMerchantTargetDetailsVerification;

    protected AgentTransactionHistoryIndexPage agentTransactionHistoryIndexPage;
    protected AgentTransactionHistoryVerification agentTransactionHistoryVerification;

    protected AgentCommissionIndexPage agentCommissionIndexPage;
    protected AgentCommissionVerification agentCommissionVerification;

    protected AgentUserListIndexPage agentUserListIndexPage;
    protected AgentUserListVerification agentUserListVerification;

    protected AgentStatementIndexPage agentStatementIndexPage;
    protected AgentStatementVerification agentStatementVerification;

    protected MerchantLoginIndexPage merchantLoginIndexPage;
    protected MerchantLoginVerification merchantLoginVerification;

    protected MerchantDashboardIndexPage merchantDashboardIndexPage;
    protected MerchantDashboardVerification merchantDashboardVerification;

    protected MerchantStatementIndexPage merchantStatementIndexPage;
    protected MerchantStatementVerification merchantStatementVerification;

    protected MerchantWithdrawIndexPage merchantWithdrawIndexPage;
    protected MerchantWithdrawVerification merchantWithdrawVerification;

    protected MerchantTransactionHistoryIndexPage merchantTransactionHistoryIndexPage;
    protected MerchantTransactionHistoryVerification merchantTransactionHistoryVerification;

    protected MerchantAdditionalDeviceIndexPage merchantAdditionalDeviceIndexPage;
    protected MerchantAdditionalDeviceVerification merchantAdditionalDeviceVerification;

    protected MerchantInvoiceIndexPage merchantInvoiceIndexPage;
    protected MerchantInvoiceVerification merchantInvoiceVerification;

    protected MerchantDonationIndexPage merchantDonationIndexPage;
    protected MerchantDonationVerification merchantDonationVerification;

    protected ConsumerLoginIndexPage consumerLoginIndexPage;
    protected ConsumerLoginVerification consumerLoginVerification;

    protected ConsumerDashboardIndexPage consumerDashboardIndexPage;
    protected ConsumerDashboardVerification consumerDashboardVerification;

    protected ConsumerTransferIndexPage consumerTransferIndexPage;
    protected ConsumerTransferVerification consumerTransferVerification;

    protected ConsumerRequestIndexPage consumerRequestIndexPage;
    protected ConsumerRequestVerification consumerRequestVerification;

    protected ConsumerStatementIndexPage consumerStatementIndexPage;
    protected ConsumerStatementVerification consumerStatementVerification;

    protected ConsumerTransactionHistoryIndexPage consumerTransactionHistoryIndexPage;
    protected ConsumerTransactionHistoryVerification consumerTransactionHistoryVerification;

    protected ConsumerAddMoneyIndexPage consumerAddMoneyIndexPage;
    protected ConsumerAddMoneyVerification consumerAddMoneyVerification;

    protected ConsumerWithdrawMoneyIndexPage consumerWithdrawMoneyIndexPage;
    protected ConsumerWithdrawMoneyVerification consumerWithdrawMoneyVerification;

    @BeforeSuite(alwaysRun = true)
    public void startReport(ITestContext testContext) {
        ExtentInitializer.initializeReport(testContext.getCurrentXmlTest().getSuite().getName());
    }

    /**
     * To initialize the driver before executing the test cases
     *
     * @param method Test Method Instance
     */
    @BeforeMethod(alwaysRun = true)
    public void setUp(Method method) {

        DesiredCapabilities capability;

        getTestCredentials(method.getName());

        switch (BROWSER.toLowerCase()) {
            case "firefox":
            case "mozilla firefox":
                capability = BrowserCaps.configureMozillaFirefox();
                break;
            case "ie":
            case "ie11":
            case "internet explorer":
                capability = BrowserCaps.configureInternetExplorer11();
                break;
            case "chrome":
            case "google chrome":
            default:
                capability = BrowserCaps.configureGoogleChrome();
                break;
        }

        driver = new RemoteWebDriver(Configuration.getRemoteGridURL(), capability);

        implicitWaitOf(driver, 10);
        maximizeWindow(driver);

        if (method.getName().startsWith("agent")) {
            agentLoginIndexPage = new AgentLoginIndexPage(driver);
            agentLoginVerification = new AgentLoginVerification(driver);
            agentDashboardIndexPage = new AgentDashboardIndexPage(driver);
            agentDashboardVerification = new AgentDashboardVerification(driver);
            agentTargetTopUpDetailsIndexPage = new AgentTargetTopUpDetailsIndexPage(driver);
            agentTargetTopUpDetailsVerification = new AgentTargetTopUpDetailsVerification(driver);
            agentConsumerTargetDetailsIndexPage = new AgentConsumerTargetDetailsIndexPage(driver);
            agentConsumerTargetDetailsVerification = new AgentConsumerTargetDetailsVerification(driver);
            agentMerchantTargetDetailsIndexPage = new AgentMerchantTargetDetailsIndexPage(driver);
            agentMerchantTargetDetailsVerification = new AgentMerchantTargetDetailsVerification(driver);
            agentTransactionHistoryIndexPage = new AgentTransactionHistoryIndexPage(driver);
            agentTransactionHistoryVerification = new AgentTransactionHistoryVerification(driver);
            agentCommissionIndexPage = new AgentCommissionIndexPage(driver);
            agentCommissionVerification = new AgentCommissionVerification(driver);
            agentUserListIndexPage = new AgentUserListIndexPage(driver);
            agentUserListVerification = new AgentUserListVerification(driver);
            agentStatementIndexPage = new AgentStatementIndexPage(driver);
            agentStatementVerification = new AgentStatementVerification(driver);

            openURL(driver, URL);
        } else if (method.getName().startsWith("superAgent")) {
            superAgentLoginIndexPage = new SuperAgentLoginIndexPage(driver);
            superAgentLoginVerification = new SuperAgentLoginVerification(driver);
            superAgentDashboardIndexPage = new SuperAgentDashboardIndexPage(driver);
            superAgentDashboardVerification = new SuperAgentDashboardVerification(driver);
            superAgentStatementIndexPage = new SuperAgentStatementIndexPage(driver);
            superAgentStatementVerification = new SuperAgentStatementVerification(driver);
            superAgentCommissionIndexPage = new SuperAgentCommissionIndexPage(driver);
            superAgentCommissionVerification = new SuperAgentCommissionVerification(driver);
            superAgentAgentListIndexPage = new SuperAgentAgentListIndexPage(driver);
            superAgentAgentListVerification = new SuperAgentAgentListVerification(driver);

            openURL(driver, URL);
        } else if (method.getName().startsWith("merchant")) {
            merchantLoginIndexPage = new MerchantLoginIndexPage(driver);
            merchantLoginVerification = new MerchantLoginVerification(driver);
            merchantDashboardIndexPage = new MerchantDashboardIndexPage(driver);
            merchantDashboardVerification = new MerchantDashboardVerification(driver);
            merchantStatementIndexPage = new MerchantStatementIndexPage(driver);
            merchantStatementVerification = new MerchantStatementVerification(driver);
            merchantWithdrawIndexPage = new MerchantWithdrawIndexPage(driver);
            merchantWithdrawVerification = new MerchantWithdrawVerification(driver);
            merchantTransactionHistoryIndexPage = new MerchantTransactionHistoryIndexPage(driver);
            merchantTransactionHistoryVerification = new MerchantTransactionHistoryVerification(driver);
            merchantAdditionalDeviceIndexPage = new MerchantAdditionalDeviceIndexPage(driver);
            merchantAdditionalDeviceVerification = new MerchantAdditionalDeviceVerification(driver);
            merchantInvoiceIndexPage = new MerchantInvoiceIndexPage(driver);
            merchantInvoiceVerification = new MerchantInvoiceVerification(driver);
            merchantDonationIndexPage = new MerchantDonationIndexPage(driver);
            merchantDonationVerification = new MerchantDonationVerification(driver);

            consumerLoginIndexPage = new ConsumerLoginIndexPage(driver);
            consumerLoginVerification = new ConsumerLoginVerification(driver);
            consumerDashboardIndexPage = new ConsumerDashboardIndexPage(driver);
            consumerDashboardVerification = new ConsumerDashboardVerification(driver);
            consumerRequestIndexPage = new ConsumerRequestIndexPage(driver);
            consumerRequestVerification = new ConsumerRequestVerification(driver);

            openURL(driver, MERCHANT_URL);
        } else if (method.getName().startsWith("consumer")) {
            consumerLoginIndexPage = new ConsumerLoginIndexPage(driver);
            consumerLoginVerification = new ConsumerLoginVerification(driver);
            consumerDashboardIndexPage = new ConsumerDashboardIndexPage(driver);
            consumerDashboardVerification = new ConsumerDashboardVerification(driver);
            consumerTransferIndexPage = new ConsumerTransferIndexPage(driver);
            consumerTransferVerification = new ConsumerTransferVerification(driver);
            consumerRequestIndexPage = new ConsumerRequestIndexPage(driver);
            consumerRequestVerification = new ConsumerRequestVerification(driver);
            consumerStatementIndexPage = new ConsumerStatementIndexPage(driver);
            consumerStatementVerification = new ConsumerStatementVerification(driver);
            consumerTransactionHistoryIndexPage = new ConsumerTransactionHistoryIndexPage(driver);
            consumerTransactionHistoryVerification = new ConsumerTransactionHistoryVerification(driver);
            consumerAddMoneyIndexPage = new ConsumerAddMoneyIndexPage(driver);
            consumerAddMoneyVerification = new ConsumerAddMoneyVerification(driver);
            consumerWithdrawMoneyIndexPage = new ConsumerWithdrawMoneyIndexPage(driver);
            consumerWithdrawMoneyVerification = new ConsumerWithdrawMoneyVerification(driver);

            merchantLoginIndexPage = new MerchantLoginIndexPage(driver);
            merchantLoginVerification = new MerchantLoginVerification(driver);
            merchantDashboardIndexPage = new MerchantDashboardIndexPage(driver);
            merchantDashboardVerification = new MerchantDashboardVerification(driver);

            openURL(driver, CONSUMER_URL);
        }

    }

    /**
     * To close the resources once the test execution is completed
     *
     * @param testResult Test Result
     */
    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult testResult) {

        String testName;

        ITestContext ex = testResult.getTestContext();

        try {
            testName = testResult.getName();
            if (testResult.getStatus() == ITestResult.FAILURE) {

                logger.log(Status.FAIL, MarkupHelper.createLabel(testName + " - Test Case Failed", ExtentColor.RED));
                logger.log(Status.FAIL, MarkupHelper.createLabel(testResult.getThrowable() + " - Test Case Failed", ExtentColor.RED));
                String screenshotPath = getExtentScreenShot(driver, testName);
                logger.fail("Test Case Failed Snapshot is below " + logger.addScreenCaptureFromPath(screenshotPath));

                System.out.println();
                System.out.println("TEST FAILED - " + testName);
                System.out.println();
                System.out.println("ERROR MESSAGE: " + testResult.getThrowable());
                System.out.println("\n");

                Reporter.setCurrentTestResult(testResult);

                String screenshotName = getCurrentTimeStampString() + testName;

                makeScreenshot(driver, screenshotName);
                TestLogger.failure();

                getShortException(ex.getFailedTests());

            } else if ((testResult.getStatus() == ITestResult.SUCCESS)) {
                logger.log(Status.PASS, MarkupHelper.createLabel(testName + " Test Case PASSED", ExtentColor.GREEN));
                System.out.println("TEST PASSED - " + testName + "\n");
            } else if ((testResult.getStatus() == ITestResult.SKIP)) {
                logger.log(Status.SKIP, MarkupHelper.createLabel(testName + " - Test Case Skipped", ExtentColor.ORANGE));
            }

            deleteCookies(driver);
            close(driver);
            quit(driver);

        } catch (Exception throwable) {
            System.err.println("Exception ::\n" + throwable);
        } finally {
            if (BrowserCaps.browserName.contains("internet explorer")) {
                deleteCookies(driver);
                killIEServer();
                pause(5);
            }
            deleteDownloadDirectory();
        }
    }

    /**
     * To flush the extent report details.
     */
    @AfterSuite(alwaysRun = true)
    public void endReport() {
        ExtentInitializer.flushReport();
    }

}