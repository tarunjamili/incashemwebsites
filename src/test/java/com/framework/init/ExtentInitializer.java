package com.framework.init;

import com.aventstack.extentreports.AnalysisStrategy;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.framework.configurations.Configuration;

import java.io.File;

/**
 * Created by Rahul R.
 * Date: 2019-04-24
 * Time: 11:47
 * Project Name: InCashMe
 */
public class ExtentInitializer implements Configuration {

    protected static ExtentReports extent;
    protected static ExtentTest logger;

    static void initializeReport(String suiteName) {

        File directory = new File(PROJECT_DIR + File.separator + "ExtentReports");
        if (!directory.exists()) {
            directory.mkdir();
        }

        ExtentHtmlReporter htmlReporter;
        htmlReporter = new ExtentHtmlReporter(PROJECT_DIR + File.separator + "ExtentReports" +
                File.separator + "Report_" + suiteName + "_" + System.currentTimeMillis() + ".html");

        extent = new ExtentReports();
        extent.attachReporter(htmlReporter);

        extent.setSystemInfo("OS : ", System.getProperty("os.name"));
        extent.setSystemInfo("OS Architecture : ", System.getProperty("os.arch"));
        extent.setSystemInfo("Java Version : ", System.getProperty("java.version"));
        extent.setSystemInfo("User Name : ", System.getProperty("user.name"));
        extent.setSystemInfo("Machine Name : ", System.getProperty("machine.name"));
        extent.setSystemInfo("IP Address : ", System.getProperty("machine.address"));
        extent.setAnalysisStrategy(AnalysisStrategy.TEST);

        htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
        htmlReporter.config().setChartVisibilityOnOpen(true);
        htmlReporter.config().setEncoding("utf-8");
        htmlReporter.config().setDocumentTitle("InCashMe Automation Test Report");
        htmlReporter.config().setReportName("<img src='https://www.kiwiqa.com/wp-content/uploads/2017/06/KiwiQA_option2.png' " +
                "width='auto' height='40' style='margin-top: 5px;' align='center'/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
                "<img src='https://lh3.googleusercontent.com/1lBjQFh0VLTTkAA5LHmNeTHaAmjYIGDTdihGlsWEuCiVh1XdU4iv1Pu-C" +
                "mPilwm71jxZmFIud-OCCuqmRfPgQQwceu-5_AkPZ2tSOA8SoYlL7YhZYjLAaBg-Rxttx7GaMQB8Y9yzLERvW9j1kxqiduvHdFdMLt" +
                "5z8FbyG06nRU8XrSeWkJfbmUOzx01qrpBpAYoLHU1u_ybuUn2B9J4SRTcyL40EbYam5DZdZhhOVc74Mn9pcjvWd83U1N6yglKxqyE" +
                "GHe8havZi_cWjlc_I8N-qUjLCC5knRs7HwX2joxKscWfN6YrkBwDPIjYHzdFcwOg-c7twvKQX5RYXuc9ZP1YG_s7MHCkPsNCu5j1" +
                "QANrOzil9rb3UXXg4vD_aCFPhJkl2kzXNj1-eiW-q6N1eZyzOEBJFiaftTqSHJQduCgpgKd0m5MHH-iNnZmzbov3HIhLlX7RE_nHQ" +
                "ncmKq_1i1nDWTklYzWWHHSsg18MtUiGGRfhd6h-FEDeJaoxctdobPFIUz6IjDSI8TfppHKPpVnjBO-0fjnEBX-d6B2XiXFDunxgm" +
                "bQLTF6UUK-L-1jylLTglZNlyV36oK9Bgv3FyhtCBd33ChUyXuP-0rOQqtHI-MEL2EbEvRjy2W5a6bVakSnPH8uIggVnICLGvE2Jb" +
                "_vcHVBuR8Uw1oTbYMQRiqukLFR1DagoE0UNyiBE9HN5IKsb5ziqaDmEzHLkjBcMOxZQg=w390-h121-no' width='auto' height" +
                "='40'" +
                " style='margin-top: 5px;' align='center'></span>");
        htmlReporter.config().setTheme(Theme.DARK);
        htmlReporter.config().setJS("$(document).ready(function(){$('.brand-logo').hide(); $('.brand-logo').html('KiwiQA');});");
    }

    static void flushReport() {
        extent.flush();
    }

}
