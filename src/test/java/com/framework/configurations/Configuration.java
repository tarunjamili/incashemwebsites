package com.framework.configurations;

import com.framework.common.Common;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

/**
 * Created by Rahul R.
 * Date: 2019-03-19
 * Time:
 * Project Name: InCashMe
 */

public interface Configuration {

    Properties configProp = new Properties();
    Properties userProp = new Properties();

    String configurationPath = "Resources/config.properties";
    String superAgentList = "Resources/superagent.properties";
    String agentList = "Resources/agent.properties";
    String merchantList = "Resources/merchant.properties";
    String consumerList = "Resources/consumer.properties";

    String SUPER_AGENT = "SuperAgent";
    String AGENT = "Agent";
    String MERCHANT = "Merchant";
    String CONSUMER = "Consumer";

    String PROJECT_DIR = getProjectDir();

    String BROWSER = getProperty("browser");
    String URL = getProperty("url");
    String MERCHANT_URL = getProperty("merchant_url");
    String CONSUMER_URL = getProperty("consumer_url");

    String SELENIUM_HUB = getProperty("selenium_hub");
    String SELENIUM_PORT = getProperty("selenium_port");

    String FILE_DOWNLOAD_PATH = PROJECT_DIR + File.separator + "download";
    String FILE_UPLOAD_PATH = PROJECT_DIR + File.separator + "Resources" + File.separator + "FileUploads";

    String REPORT_ICONS = PROJECT_DIR + File.separator + "report-icns" + File.separator;

    String PROFILE_PIC = FILE_UPLOAD_PATH + File.separator + "Profile_pic.jpg";
    String ADHAAR_FRONT = FILE_UPLOAD_PATH + File.separator + "Adhaar_Front.jpg";
    String ADHAAR_BACK = FILE_UPLOAD_PATH + File.separator + "Adhaar_Back.jpg";
    String INVALID_IMAGE = FILE_UPLOAD_PATH + File.separator + "dummy.pdf";

    String WORKBOOK = PROJECT_DIR + File.separator + "Resources" + File.separator + "UsersList.xlsx";

    String TEST_CARD_NUMBER = "5123456789012346";
    String TEST_CARD_NAME = "TestAutomation";
    String TEST_CARD_EXP_DATE = "05" + "" + "20";
    String TEST_CARD_CVV = "123";

    boolean isPositiveExecution = Boolean.parseBoolean(getProperty("isPositiveExecution"));

    static URL getRemoteGridURL() {

        URL REMOTE_GRID_URL = null;

        try {
            REMOTE_GRID_URL = new URL("http://" + SELENIUM_HUB + ":" + SELENIUM_PORT + "/wd/hub");
        } catch (MalformedURLException ex) {
            Common.log("Error occurred in Remote Grid URL.");
        }

        return REMOTE_GRID_URL;
    }

    static boolean getExecutionType() {
        return !getProperty("positiveExecution").equalsIgnoreCase("false");
    }

    /**
     * To get the current project directory path
     *
     * @return Project Directory path
     */
    static String getProjectDir() {
        return System.getProperty("user.dir");
    }

    /**
     * To get the Test Configuration Property values
     *
     * @param key Key
     * @return Value of Key
     */
    static String getProperty(String key) {
        InputStream input = null;
        try {
            input = new FileInputStream(configurationPath);
            configProp.load(input);
        } catch (Exception e) {
            Common.log("Error occurred while reading the file.");
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return configProp.getProperty(key);
    }

    /**
     * To get the User Configuration Property values
     *
     * @param key Key
     * @return Value of Key
     */
    static String getUserProperty(String user, String key) {
        InputStream input = null;
        try {
            userProp.clear();
            switch (user) {
                case "Super Agent":
                    input = new FileInputStream(superAgentList);
                    break;
                case "Agent":
                    input = new FileInputStream(agentList);
                    break;
                case "Merchant":
                    input = new FileInputStream(merchantList);
                    break;
                case "Consumer":
                    input = new FileInputStream(consumerList);
                    break;
            }
            userProp.load(input);
        } catch (Exception e) {
            Common.log("Error occurred while reading the file.");
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return userProp.getProperty(key);
    }

    static void writeProperty(String user, String key, String value) {
        OutputStream output = null;
        try {
            switch (user) {
                case "Super Agent":
                    output = new FileOutputStream(superAgentList);
                    break;
                case "Agent":
                    output = new FileOutputStream(agentList);
                    break;
                case "Merchant":
                    output = new FileOutputStream(merchantList);
                    break;
                case "Consumer":
                    output = new FileOutputStream(consumerList);
                    break;
            }
            userProp.setProperty(key, value);
            userProp.store(output, null);
        } catch (Exception e) {
            Common.log("Error occurred while reading the file.");
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}