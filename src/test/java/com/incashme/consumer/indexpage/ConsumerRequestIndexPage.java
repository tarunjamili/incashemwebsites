package com.incashme.consumer.indexpage;

import com.framework.init.AbstractPage;
import com.incashme.consumer.verification.ConsumerRequestVerification;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class ConsumerRequestIndexPage extends AbstractPage {

    public ConsumerRequestIndexPage(WebDriver driver) {
        super(driver);
    }

    public static double _requestReceiverBalance = 0.00d;

    public static String _invoiceID = "";
    public static String _invoiceUserName = "";
    public static String _invoiceUserID = "";
    public static String _invoiceUserNumber = "";
    public static String _invoiceAmount = "";

    public static String _donationID = "";
    public static String _donationUserName = "";
    public static String _donationUserID = "";
    public static String _donationUserNumber = "";
    public static double _donationAmount = 0.0d;


    @FindBy(xpath = "//button[contains(@class,'btn-green') and text()='Pay']")
    private WebElement btnPayInvoice;

    @FindBy(xpath = "//button[contains(@class,'btn-green') and text()='Confirm']")
    private WebElement btnConfirmPayInvoice;

    @FindAll(value = {@FindBy(xpath = "//ul//li[contains(@class,'req')]//div[contains(@class,'dntn-id')]")})
    private List<WebElement> lstRequestIDCard;

    @FindAll(value = {@FindBy(xpath = "//ul//li[contains(@class,'invc-req')]")})
    private List<WebElement> lstInvoiceRequestCard;

    @FindAll(value = {@FindBy(xpath = "//ul//li[contains(@class,'invc-req')]//div[contains(@class,'dntn-id')]")})
    private List<WebElement> lstInvoiceRequestCardID;

    @FindAll(value = {@FindBy(xpath = "//ul//li[contains(@class,'invc-req')]//div[contains(@class,'dntn-name')]")})
    private List<WebElement> lstInvoiceRequestCardUserName;

    @FindAll(value = {@FindBy(xpath = "//ul//li[contains(@class,'invc-req')]//div[contains(@class,'dntr-id')]")})
    private List<WebElement> lstInvoiceRequestCardUserID;

    @FindAll(value = {@FindBy(xpath = "//ul//li[contains(@class,'invc-req')]//div[contains(@class,'dntr-no')]")})
    private List<WebElement> lstInvoiceRequestCardUserNumber;

    @FindAll(value = {@FindBy(xpath = "//ul//li[contains(@class,'invc-req')]//div[contains(@class,'dntn-amnt')]")})
    private List<WebElement> lstInvoiceRequestCardAmount;

    @FindAll(value = {@FindBy(xpath = "//ul//li[contains(@class,'dntn-req')]")})
    private List<WebElement> lstDonationRequestCard;

    @FindAll(value = {@FindBy(xpath = "//ul//li[contains(@class,'dntn-req')]//div[contains(@class,'dntn-id')]")})
    private List<WebElement> lstDonationRequestCardID;

    @FindAll(value = {@FindBy(xpath = "//ul//li[contains(@class,'dntn-req')]//div[contains(@class,'dntn-name')]")})
    private List<WebElement> lstDonationRequestCardUserName;

    @FindAll(value = {@FindBy(xpath = "//ul//li[contains(@class,'dntn-req')]//div[contains(@class,'dntr-id')]")})
    private List<WebElement> lstDonationRequestCardUserID;

    @FindAll(value = {@FindBy(xpath = "//ul//li[contains(@class,'dntn-req')]//div[contains(@class,'dntr-no')]")})
    private List<WebElement> lstDonationRequestCardUserNumber;

    @FindBy(xpath = "//button[contains(@class,'btn-green') and contains(text(),'Home')]")
    private WebElement btnBackToHomePay;

    @FindBy(xpath = "//input[@id='amount']")
    private WebElement txtDonationAmount;

    @FindBy(xpath = "//button[contains(@class,'btn-green') and text()='Donate']")
    private WebElement btnPayDonation;

    @FindBy(xpath = "//div[contains(@class,'agnt-cnf-ftr')]//button[contains(@class,'btn-pink') and text()='Cancel']")
    private WebElement btnCancelSendDonationRequest;

    @FindBy(xpath = "//button[contains(@class,'btn-danger') and text()='Cancel']")
    private WebElement btnCancelRequest;

    @FindBy(xpath = ".//div[contains(@class,'swal2-actions')]//button[contains(text(),'Yes')]")
    private WebElement btnCancelRequestYes;

    @FindBy(xpath = ".//div[contains(@class,'swal2-actions')]//button[contains(text(),'Cancel')]")
    private WebElement btnCancelRequestCancel;

    @FindBy(xpath = "//ul/li[contains(@id,'li_request')]")
    private WebElement btnMenuRequest;

    public ConsumerRequestVerification clickOnRequests() {

        testStepsLog(_logStep++, "Click on Requests button.");
        clickOn(driver, btnMenuRequest);

        return new ConsumerRequestVerification(driver);
    }

    public ConsumerRequestVerification getRequestReceiverBalance() {
        ConsumerDashboardIndexPage.getCurrentBalance();
        _requestReceiverBalance = getDoubleFromString(ConsumerDashboardIndexPage._currentBalance);
        return new ConsumerRequestVerification(driver);
    }


    public ConsumerRequestVerification openRecentRequest() {
        testStepsLog(_logStep++, "Open Recent Invoice.");
        clickOn(driver, lstRequestIDCard.get(0));

        return new ConsumerRequestVerification(driver);
    }

    public ConsumerRequestVerification openInvoiceRequest() {
        testStepsLog(_logStep++, "Open Invoice Request.");
        scrollToElement(driver, lstInvoiceRequestCard.get(0));
        clickOn(driver, lstInvoiceRequestCard.get(0));

        return new ConsumerRequestVerification(driver);
    }

    public ConsumerRequestVerification openDonationRequest() {
        testStepsLog(_logStep++, "Open Donation Request.");
        scrollToElement(driver, lstDonationRequestCard.get(0));
        clickOn(driver, lstDonationRequestCard.get(0));

        return new ConsumerRequestVerification(driver);
    }

    public ConsumerRequestVerification clickOnPayButton() {
        testStepsLog(_logStep++, "Click on Pay button.");
        clickOn(driver, btnPayInvoice);

        return new ConsumerRequestVerification(driver);
    }

    public ConsumerRequestVerification clickOnCancelDonationButton() {
        testStepsLog(_logStep++, "Click on Cancel button.");
        clickOn(driver, btnCancelSendDonationRequest);

        return new ConsumerRequestVerification(driver);
    }

    public ConsumerRequestVerification clickOnDonateButton() {
        testStepsLog(_logStep++, "Click on Donate button.");
        clickOn(driver, btnPayDonation);

        return new ConsumerRequestVerification(driver);
    }

    public ConsumerRequestVerification clickOnConfirmButton() {
        testStepsLog(_logStep++, "Click on Confirm button.");
        clickOn(driver, btnConfirmPayInvoice);

        return new ConsumerRequestVerification(driver);
    }

    public ConsumerRequestVerification clickOnBackToHomePaid() {

        testStepsLog(_logStep++, "Click on Back To Home button.");
        clickOn(driver, btnBackToHomePay);

        return new ConsumerRequestVerification(driver);

    }

    public ConsumerRequestVerification enterMinAmount() {
        _donationAmount = 0.01;
        testStepsLog(_logStep++, "Enter Amount : " + _donationAmount);
        type(txtDonationAmount, String.valueOf(_donationAmount));
        return new ConsumerRequestVerification(driver);
    }

    public ConsumerRequestVerification enterMaxAmount() {
        _donationAmount = 9999999;
        testStepsLog(_logStep++, "Enter Amount : " + _donationAmount);
        type(txtDonationAmount, String.valueOf(_donationAmount));
        return new ConsumerRequestVerification(driver);
    }

    public ConsumerRequestVerification enterValidAmount() {
        _donationAmount = formatTwoDecimal(getDoubleFromString(getRandomNumberBetween(100, 200) + "."
                + getRandomNumberBetween(10, 99)));
        testStepsLog(_logStep++, "Enter Amount : " + _donationAmount);
        type(txtDonationAmount, String.valueOf(_donationAmount));
        return new ConsumerRequestVerification(driver);
    }

    public ConsumerRequestVerification enterAmountMoreThanFunds() {
        _donationAmount = _requestReceiverBalance + 100;
        testStepsLog(_logStep++, "Enter Amount : " + _donationAmount);
        type(txtDonationAmount, String.valueOf(_donationAmount));
        return new ConsumerRequestVerification(driver);
    }

    public boolean isBalanceLessThanAccepted() {
        return _requestReceiverBalance < 100000;
    }

    public boolean isInvoiceRequestAvailable() {
        return !isListEmpty(lstInvoiceRequestCard);
    }

    public boolean isDonationRequestAvailable() {
        return !isListEmpty(lstDonationRequestCard);
    }

    public ConsumerRequestVerification getInvoiceDetailsFromCard() {

        _invoiceID = getInnerText(lstInvoiceRequestCardID.get(0));
        _invoiceUserName = getInnerText(lstInvoiceRequestCardUserName.get(0));
        _invoiceUserNumber = getInnerText(lstInvoiceRequestCardUserNumber.get(0));
        _invoiceUserID = getInnerText(lstInvoiceRequestCardUserID.get(0));
        _invoiceAmount = getInnerText(lstInvoiceRequestCardAmount.get(0));

        return new ConsumerRequestVerification(driver);
    }

    public ConsumerRequestVerification getDonationDetailsFromCard() {

        _donationID = getInnerText(lstDonationRequestCardID.get(0));
        _donationUserName = getInnerText(lstDonationRequestCardUserName.get(0));
        _donationUserNumber = getInnerText(lstDonationRequestCardUserNumber.get(0));
        _donationUserID = getInnerText(lstDonationRequestCardUserID.get(0));

        return new ConsumerRequestVerification(driver);
    }

    public ConsumerRequestVerification clickOnCancelRequest() {
        pause(1);
        testStepsLog(_logStep++, "Click on Cancel button.");
        clickOn(driver, btnCancelRequest);

        return new ConsumerRequestVerification(driver);
    }

    public ConsumerRequestVerification clickOnYesCancelRequest() {
        pause(1);
        testStepsLog(_logStep++, "Click on Yes button.");
        clickOn(driver, btnCancelRequestYes);

        return new ConsumerRequestVerification(driver);
    }

    public ConsumerRequestVerification clickOnCancelCancelRequest() {
        pause(1);
        testStepsLog(_logStep++, "Click on Cancel button.");
        clickOn(driver, btnCancelRequestCancel);

        return new ConsumerRequestVerification(driver);
    }
}
