package com.incashme.consumer.indexpage;

import com.framework.init.AbstractPage;
import com.incashme.consumer.verification.ConsumerDashboardVerification;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rahul R.
 * Date: 2019-07-08
 * Time
 * Project Name: InCashMe
 */

public class ConsumerDashboardIndexPage extends AbstractPage {

    public static String _currentBalance = "";

    public static List<String> _userNameBeforeSort = new ArrayList<>();
    public static List<String> _userTxIDBeforeSort = new ArrayList<>();
    public static List<String> _userCategoryBeforeSort = new ArrayList<>();
    public static List<String> _txTypeBeforeSort = new ArrayList<>();
    public static List<LocalDateTime> _transactionDateTimeBeforeSort = new ArrayList<>();

    public static String _transactionUsername = "";
    public static int _transactionID;
    public static long _transactionUserIDNumber;
    public static String _transactionCategory = "";
    public static double _transactionAmount;
    public static String _transactionType = "";
    public static String _transactionDate = "";
    public static String _transactionTime = "";

    public static String _searchCriteria = "";
    public static int _userNumber = -1;

    public ConsumerDashboardIndexPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = ".//div[contains(@class,'user-avatar')]//img")
    private WebElement imgUserLogo;

    @FindBy(xpath = ".//div[contains(@class,'usr-dtl')]//div[contains(@class,'user-name')]")
    private WebElement lblDashboardUserName;

    @FindBy(xpath = "//ul/li[contains(@id,'li_home')]")
    private WebElement btnMenuHome;

    @FindBy(xpath = "//ul/li[contains(@id,'li_balance')]")
    private WebElement btnMenuBalance;

    @FindBy(xpath = "//ul/li[contains(@id,'li_statement')]")
    private WebElement btnMenuStatement;

    @FindBy(xpath = "//ul/li[contains(@id,'li_privacyp')]")
    private WebElement btnMenuPrivacyPolicy;

    @FindBy(xpath = "(//ul/li[contains(@id,'li_tandc')])[1]")
    private WebElement btnMenuTermsAndCondition;

    @FindBy(xpath = "(//ul/li[contains(@id,'li_tandc')])[2]")
    private WebElement btnMenuKYCAMLPolicy;

    @FindBy(xpath = "//div[contains(@class,'blnc-txt')]")
    private static WebElement lblBalance;

    @FindAll(value = {@FindBy(xpath = "//table[@id='transactionHistoryTbl']//tr//td[7]//button")})
    private List<WebElement> lstBtnMore;

    @FindBy(xpath = "//div[contains(@class,'profile-menu')]//ul/li[@id='p_home']")
    private WebElement btnPersonalInfoMenu;

    @FindBy(xpath = "//div[contains(@class,'profile-menu')]//ul/li[@id='p_docs']")
    private WebElement btnDocuments;

    @FindBy(xpath = "//div[contains(@class,'profile-menu')]//ul/li[@id='p_pwd']")
    private WebElement btnChangePassword;

    @FindBy(xpath = "//div//ul//li//span[contains(@class,'ic-logout')]")
    private WebElement btnLogout;

    @FindAll(value = {@FindBy(xpath = "//table[@id='transactionHistoryTbl']//tr//td[1]//h5")})
    private List<WebElement> lstDashboardUserName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='transactionHistoryTbl']//tr//td[1]//h5//following-sibling::div")})
    private List<WebElement> lstDashboardUserNumberID;

    @FindAll(value = {@FindBy(xpath = "//table[@id='transactionHistoryTbl']//tr//td[2]")})
    private List<WebElement> lstDashboardTxID;

    @FindAll(value = {@FindBy(xpath = "//table[@id='transactionHistoryTbl']//tr//td[3]")})
    private List<WebElement> lstDashboardUserCategory;

    @FindAll(value = {@FindBy(xpath = "//table[@id='transactionHistoryTbl']//tr//td[4]")})
    private List<WebElement> lstDashboardTxAmount;

    @FindAll(value = {@FindBy(xpath = "//table[@id='transactionHistoryTbl']//tr//td[5]")})
    private List<WebElement> lstDashboardUserType;

    @FindAll(value = {@FindBy(xpath = "//table[@id='transactionHistoryTbl']//tr//td[6]")})
    private List<WebElement> lstDashboardTransactionTime;

    @FindBy(xpath = "//table[@id='transactionHistoryTbl']//th[1]")
    private WebElement lblUserNameHeader;

    @FindBy(xpath = "//table[@id='transactionHistoryTbl']//th[2]")
    private WebElement lblTxIDHeader;

    @FindBy(xpath = "//table[@id='transactionHistoryTbl']//th[3]")
    private WebElement lblUserCategoryHeader;

    @FindBy(xpath = "//table[@id='transactionHistoryTbl']//th[5]")
    private WebElement lblUserTypeHeader;

    @FindBy(xpath = "//table[@id='transactionHistoryTbl']//th[6]")
    private WebElement lblTransactionDateTime;

    @FindBy(xpath = "//input[@type='search']")
    private WebElement txtInputSearch;

    @FindBy(xpath = "//*[text()='Transaction Detail']/ancestor::div//button[contains(@class,'close')]")
    private WebElement btnCloseTxDetailsPopup;

    @FindBy(xpath = "//input[@id='oldpassword']")
    private WebElement txtCurrentPassword;

    @FindBy(xpath = "//input[@id='password2']")
    private WebElement txtNewPassword;

    @FindBy(xpath = "//input[@id='password']")
    private WebElement txtConfirmPassword;

    @FindBy(xpath = ".//div[contains(@class,'login-form')]//button[@type='submit']")
    private WebElement btnSubmitPassword;

    @FindBy(xpath = "//*[text()='Change Password']/ancestor::div//button[contains(@class,'close')]")
    private WebElement btnClosePasswordPopup;

    public ConsumerDashboardVerification expandDashboardMenu() {
        mouseHoverTo(driver, imgUserLogo);
        testInfoLog("Logged in User Name", getText(lblDashboardUserName));

        return new ConsumerDashboardVerification(driver);
    }

    public ConsumerDashboardVerification clickOnUserDetails() {

        testStepsLog(_logStep++, "Click on User Logo.");
        clickOn(driver, imgUserLogo);
        pause(1);
        mouseHoverTo(driver, btnLogout);

        return new ConsumerDashboardVerification(driver);
    }

    public ConsumerDashboardVerification clickOnDocuments() {

        testStepsLog(_logStep++, "Click on Documents.");
        clickOn(driver, btnDocuments);

        return new ConsumerDashboardVerification(driver);
    }

    public ConsumerDashboardVerification clickOnHome() {

        testStepsLog(_logStep++, "Click on Home.");
        clickOn(driver, btnMenuHome);

        return new ConsumerDashboardVerification(driver);
    }

    public ConsumerDashboardVerification clickOnChangePassword() {

        testStepsLog(_logStep++, "Click on Change password.");
        clickOn(driver, btnChangePassword);

        return new ConsumerDashboardVerification(driver);
    }

    public ConsumerDashboardVerification clickOnBalance() {

        testStepsLog(_logStep++, "Click on Balance button.");
        clickOn(driver, btnMenuBalance);

        return new ConsumerDashboardVerification(driver);
    }

    public ConsumerDashboardVerification clickOnPrivacyPolicy() {

        testStepsLog(_logStep++, "Click on Privacy Policy button.");
        clickOn(driver, btnMenuPrivacyPolicy);

        return new ConsumerDashboardVerification(driver);
    }

    public ConsumerDashboardVerification clickOnTnC() {

        testStepsLog(_logStep++, "Click on Terms Condition button.");
        scrollToElement(driver, btnMenuTermsAndCondition);
        clickOn(driver, btnMenuTermsAndCondition);

        return new ConsumerDashboardVerification(driver);
    }

    public ConsumerDashboardVerification clickOnKYCAML() {

        testStepsLog(_logStep++, "Click on KYC & AML Policy button.");
        scrollElement(btnMenuKYCAMLPolicy);
        clickOn(driver, btnMenuKYCAMLPolicy);

        return new ConsumerDashboardVerification(driver);
    }

    public static void getCurrentBalance() {
        pause(5);
        while (getInnerText(lblBalance).isEmpty()) {
            pause(1);
        }
        _currentBalance = getInnerText(lblBalance).substring(2);
    }


    public ConsumerDashboardVerification clickOnLatestMOREButton() {

        testStepsLog(_logStep++, "Click on MORE button.");
        clickOn(driver, lstBtnMore.get(0));

        return new ConsumerDashboardVerification(driver);

    }

    public ConsumerDashboardVerification clickOnMOREButton() {

        testStepsLog(_logStep++, "Click on MORE button.");
        clickOn(driver, lstBtnMore.get(_userNumber));

        return new ConsumerDashboardVerification(driver);

    }

    public boolean isTransactionHistoryAvailable() {
        return !isListEmpty(lstDashboardUserName);
    }

    public ConsumerDashboardVerification clickOnUserNameToSort() {

        updateShowList(driver, findElementByName(driver, "transactionHistoryTbl_length"), "All");

        for (WebElement name : lstDashboardUserName) {
            _userNameBeforeSort.add(getInnerText(name));
        }

        testStepsLog(_logStep++, "Click On Name/Id to sort.");
        clickOn(driver, lblUserNameHeader);

        return new ConsumerDashboardVerification(driver);

    }

    public ConsumerDashboardVerification clickOnTransactionIDToSort() {

        updateShowList(driver, findElementByName(driver, "transactionHistoryTbl_length"), "All");

        for (WebElement name : lstDashboardTxID) {
            _userTxIDBeforeSort.add(getInnerText(name));
        }

        testStepsLog(_logStep++, "Click On Transaction ID to sort.");
        clickOn(driver, lblTxIDHeader);

        return new ConsumerDashboardVerification(driver);

    }

    public ConsumerDashboardVerification clickOnCategoryToSort() {

        updateShowList(driver, findElementByName(driver, "transactionHistoryTbl_length"), "All");

        for (WebElement name : lstDashboardUserCategory) {
            _userCategoryBeforeSort.add(getInnerText(name));
        }

        testStepsLog(_logStep++, "Click On Category to sort.");
        clickOn(driver, lblUserCategoryHeader);

        return new ConsumerDashboardVerification(driver);

    }

    public ConsumerDashboardVerification clickOnTypesToSort() {

        updateShowList(driver, findElementByName(driver, "transactionHistoryTbl_length"), "All");

        for (WebElement name : lstDashboardUserType) {
            _txTypeBeforeSort.add(getInnerText(name));
        }

        testStepsLog(_logStep++, "Click On Types to sort.");
        clickOn(driver, lblUserTypeHeader);

        return new ConsumerDashboardVerification(driver);

    }

    public ConsumerDashboardVerification clickOnDateTimeToSort() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        updateShowList(driver, findElementByName(driver, "transactionHistoryTbl_length"), "All");

        for (WebElement name : lstDashboardTransactionTime) {
            _transactionDateTimeBeforeSort.add(LocalDateTime.parse(getInnerText(name), format));
        }

        testStepsLog(_logStep++, "Click On Date & Time to sort.");
        clickOn(driver, lblTransactionDateTime);

        return new ConsumerDashboardVerification(driver);

    }

    public ConsumerDashboardVerification enterDashboardSearchCriteria(boolean isInvalid) {

        if (isInvalid) {

            scrollToElement(driver, txtInputSearch);

            String searchCriteria = getRandomCharacters(10);
            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", searchCriteria);
            type(txtInputSearch, searchCriteria);

        } else {

            scrollToElement(driver, txtInputSearch);

            _searchCriteria = getInnerText(lstDashboardTxID.get(0));

            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", _searchCriteria);
            type(txtInputSearch, _searchCriteria);
        }

        return new ConsumerDashboardVerification(driver);
    }

    public ConsumerDashboardVerification clickOnPopUpCloseButton() {

        testStepsLog(_logStep++, "Click On Close button.");
        clickOn(driver, btnCloseTxDetailsPopup);

        return new ConsumerDashboardVerification(driver);
    }

    public ConsumerDashboardVerification getDashboardTransactionDetails(String userType) {

        updateShowList(driver, findElementByName(driver, "transactionHistoryTbl_length"), "All");

        if (userType.equalsIgnoreCase("Merchant")) {
            for (int user = 0; user < sizeOf(lstDashboardUserName); user++) {
                if (getInnerText(lstDashboardUserNumberID.get(user)).contains("ID-")) {
                    _userNumber = user;
                    _transactionUsername = getInnerText(lstDashboardUserName.get(user));
                    _transactionUserIDNumber = getLongFromString(getInnerText(lstDashboardUserNumberID.get(user)));
                    _transactionID = getIntegerFromString(getInnerText(lstDashboardTxID.get(user)));
                    _transactionCategory = getInnerText(lstDashboardUserCategory.get(user));
                    _transactionAmount = getDoubleFromString(getInnerText(lstDashboardTxAmount.get(user)));
                    _transactionType = getInnerText(lstDashboardUserType.get(user));
                    _transactionDate = LocalDate.parse(getInnerText(lstDashboardTransactionTime.get(user)).split(" ")[0],
                            DateTimeFormatter.ofPattern("dd-MM-yyyy")).format(DateTimeFormatter.ofPattern("dd MMM yyyy"));
                    _transactionTime = LocalTime.parse((getInnerText(lstDashboardTransactionTime.get(user)).split(" ")[1] + " " +
                                    getInnerText(lstDashboardTransactionTime.get(user)).split(" ")[2]),
                            DateTimeFormatter.ofPattern("h:mm:ss a")).format(DateTimeFormatter.ofPattern("h:mm a"));
                    break;
                }
            }
        } else {
            for (int user = 0; user < sizeOf(lstDashboardUserName); user++) {
                if (!getInnerText(lstDashboardUserNumberID.get(user)).contains("ID")) {
                    System.out.println(getInnerText(lstDashboardUserNumberID.get(user)));
                    _userNumber = user;
                    _transactionUsername = getInnerText(lstDashboardUserName.get(user));
                    _transactionUserIDNumber = getLongFromString(getInnerText(lstDashboardUserNumberID.get(user)));
                    _transactionID = getIntegerFromString(getInnerText(lstDashboardTxID.get(user)));
                    _transactionCategory = getInnerText(lstDashboardUserCategory.get(user));
                    _transactionAmount = getDoubleFromString(getInnerText(lstDashboardTxAmount.get(user)));
                    _transactionType = getInnerText(lstDashboardUserType.get(user));
                    _transactionDate = LocalDate.parse(getInnerText(lstDashboardTransactionTime.get(user)).split(" ")[0],
                            DateTimeFormatter.ofPattern("dd-MM-yyyy")).format(DateTimeFormatter.ofPattern("dd MMM yyyy"));
                    _transactionTime = LocalTime.parse((getInnerText(lstDashboardTransactionTime.get(user)).split(" ")[1] + " " +
                                    getInnerText(lstDashboardTransactionTime.get(user)).split(" ")[2]),
                            DateTimeFormatter.ofPattern("h:mm:ss a")).format(DateTimeFormatter.ofPattern("h:mm a"));
                    break;
                }
            }
        }

        System.out.println(_transactionDate);
        System.out.println(_transactionTime);

        return new ConsumerDashboardVerification(driver);
    }

    public ConsumerDashboardVerification changeInvalidCurrentPassword() {

        String randomPassword = getRandomPassword();

        enterCurrentPassword(getRandomPassword());
        enterNewPassword(randomPassword);
        enterConfirmNewPassword(randomPassword);

        clickOnSubmitButton();

        return new ConsumerDashboardVerification(driver);
    }

    private void enterNewPassword(String newPassword) {
        testStepsLog(_logStep++, "Enter New Password.");
        testInfoLog("New Password", newPassword);
        type(txtNewPassword, newPassword);
    }

    private void enterCurrentPassword(String currentPassword) {
        testStepsLog(_logStep++, "Enter Current Password.");
        testInfoLog("Current Password", currentPassword);
        type(txtCurrentPassword, currentPassword);
    }

    private void enterConfirmNewPassword(String confirmPassword) {
        testStepsLog(_logStep++, "Enter Confirm Password.");
        testInfoLog("Confirm Password", confirmPassword);
        type(txtConfirmPassword, confirmPassword);
    }

    private void clickOnSubmitButton() {

        testStepsLog(_logStep++, "Click on Submit button.");
        clickOn(driver, btnSubmitPassword);

    }

    public ConsumerDashboardVerification clearDetails() {

        testStepsLog(_logStep++, "Remove Password details");
        clear(txtConfirmPassword);
        clear(txtNewPassword);
        clear(txtCurrentPassword);

        return new ConsumerDashboardVerification(driver);
    }

    public ConsumerDashboardVerification changeInvalidConfirmPassword() {

        enterCurrentPassword(password);
        enterNewPassword(getRandomPassword());
        enterConfirmNewPassword(getRandomPassword());

        pause(1);

        clickOnSubmitButton();

        return new ConsumerDashboardVerification(driver);

    }

    public ConsumerDashboardVerification changeAsPastPassword() {

        enterCurrentPassword(password);
        enterNewPassword(password);
        enterConfirmNewPassword(password);

        clickOnSubmitButton();

        return new ConsumerDashboardVerification(driver);

    }

    public ConsumerDashboardVerification changeInvalidNewPassword() {

        String randomPassword = String.valueOf(getRandomNumber());

        enterCurrentPassword(password);
        enterNewPassword(randomPassword);
        enterConfirmNewPassword(randomPassword);

        return new ConsumerDashboardVerification(driver);

    }
}
