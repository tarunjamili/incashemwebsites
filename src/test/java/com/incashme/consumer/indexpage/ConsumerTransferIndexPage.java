package com.incashme.consumer.indexpage;

import com.framework.init.AbstractPage;
import com.incashme.consumer.verification.ConsumerTransferVerification;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class ConsumerTransferIndexPage extends AbstractPage {

    public static String _transferMessage = "";

    public static String _transferUserName;
    public static String _transferUserNumber;
    public static double _transferAmount;
    public static double _transferReceiverBalance;
    public static double _transferSenderBalance;

    public static boolean _noConsumer;
    public static boolean _noMerchant;

    public ConsumerTransferIndexPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//div//ul//li//i[contains(@class,'ic-add-user')]")
    private WebElement btnNewTransaction;

    @FindBy(xpath = "//div[contains(@class,'searchbox')]//input[@id='payeeName']")
    private WebElement txtSearchNumber;

    @FindBy(xpath = "//div[contains(@class,'searchbox')]//button[contains(@class,'searchBtn')]")
    private WebElement btnSearchUser;

    @FindAll(value = {@FindBy(xpath = "//ul[contains(@class,'search_ul')]//li[contains(@id,'id_')]//div[contains(text(),'KYC')]")})
    private List<WebElement> lstSearchedNonKYCUser;

    @FindAll(value = {@FindBy(xpath = "//ul[contains(@class,'search_ul')]//li[contains(@id,'id_')]")})
    private List<WebElement> lstSearchedUser;

    @FindAll(value = {@FindBy(xpath = "//ul[contains(@class,'search_ul')]//li[contains(@id,'id_')]//div[2]//div[2]")})
    private List<WebElement> lstSearchedUserNumberID;

    @FindBy(xpath = "//input[@id='amount']")
    private WebElement txtTransferAmount;

    @FindBy(xpath = "//textarea[@id='message']")
    private WebElement txtTransferMessage;

    @FindBy(xpath = "//form//button[contains(@class,'btn-pink') and text()='Cancel']")
    private WebElement btnCancelTransferRequest;

    @FindBy(xpath = "//button[contains(@class,'btn-green') and text()='Send']")
    private WebElement btnSendTransfer;

    @FindBy(xpath = "//div[contains(@class,'search_user_details')]//h3")
    private WebElement lblTransferUserName;

    @FindBy(xpath = "//div[contains(@class,'search_user_details')]//p")
    private WebElement lblTransferUserNumber;

    @FindBy(xpath = "//div[contains(@class,'agnt-cnf-ftr')]//a[contains(@class,'btn-green') and contains(text(),'Home')]")
    private WebElement btnBackToHome;

    public ConsumerTransferVerification clickOnNewTransaction() {

        testStepsLog(_logStep++, "Click on New Transaction button.");
        clickOn(driver, btnNewTransaction);

        return new ConsumerTransferVerification(driver);
    }

    public ConsumerTransferVerification clickOnSendButton() {

        scrollElement(btnSendTransfer);
        testStepsLog(_logStep++, "Click on Send button.");
        clickOn(driver, btnSendTransfer);

        return new ConsumerTransferVerification(driver);
    }

    public ConsumerTransferVerification enterNonKYCUserNumber() {

        testStepsLog(_logStep++, "Enter Mobile Number to search");
        testInfoLog("Search", externalNumber);
        type(txtSearchNumber, externalNumber);
        testStepsLog(_logStep++, "Click on Search button.");
        clickOn(driver, btnSearchUser);

        return new ConsumerTransferVerification(driver);
    }

    public boolean isNonKYCUserDisplay() {
        pause(3);
        return !isListEmpty(lstSearchedNonKYCUser);
    }

    public boolean isUsersDisplay() {
        pause(3);
        return !isListEmpty(lstSearchedUser);
    }

    public ConsumerTransferVerification selectNonKYCUser() {

        testStepsLog(_logStep++, "Click on User to select.");
        clickOn(driver, lstSearchedNonKYCUser.get(0));

        return new ConsumerTransferVerification(driver);
    }

    public ConsumerTransferVerification selectValidConsumerUser() {
        for (int user = 0; user <= lastIndexOf(lstSearchedUser); user++) {
            if (!getText(lstSearchedUser.get(user)).contains("KYC is pending")) {
                if (!getText(lstSearchedUserNumberID.get(user)).contains("ID")) {
                    clickOn(driver, lstSearchedUser.get(user));
                    testStepsLog(_logStep++, "Click on user to select");
                    _noConsumer = false;
                    break;
                } else {
                    _noConsumer = true;
                }
            }
        }

        _transferUserName = getText(lblTransferUserName);
        _transferUserNumber = getText(lblTransferUserNumber);

        return new ConsumerTransferVerification(driver);
    }

    public ConsumerTransferVerification selectValidMerchantUser() {
        for (int user = 0; user <= lastIndexOf(lstSearchedUser); user++) {
            if (!getText(lstSearchedUser.get(user)).contains("KYC is pending")) {
                if (getText(lstSearchedUserNumberID.get(user)).contains("ID")) {
                    clickOn(driver, lstSearchedUser.get(user));
                    testStepsLog(_logStep++, "Click on user to select");
                    _noMerchant = false;
                    break;
                } else {
                    _noMerchant = true;
                }
            }
        }

        _transferUserName = getText(lblTransferUserName);
        _transferUserNumber = getText(lblTransferUserNumber);

        return new ConsumerTransferVerification(driver);
    }

    public ConsumerTransferVerification enterZeroAmountAsTransfer() {

        _transferAmount = 0.00d;
        scrollToElement(driver, txtTransferAmount);
        testStepsLog(_logStep++, "Enter Amount");
        testInfoLog("Amount", String.valueOf(_transferAmount));
        type(txtTransferAmount, String.valueOf(_transferAmount));

        return new ConsumerTransferVerification(driver);
    }


    public ConsumerTransferVerification enterMaxAmountAsTransfer() {

        _transferAmount = 100000.01d;
        scrollToElement(driver, txtTransferAmount);
        testStepsLog(_logStep++, "Enter Amount");
        testInfoLog("Amount", String.valueOf(_transferAmount));
        type(txtTransferAmount, String.valueOf(_transferAmount));

        return new ConsumerTransferVerification(driver);

    }

    public ConsumerTransferVerification enterValidAmount() {

        _transferAmount = formatTwoDecimal(getDoubleFromString(getRandomNumberBetween(100, 200) + "."
                + getRandomNumberBetween(10, 99)));
        scrollToElement(driver, txtTransferAmount);
        testStepsLog(_logStep++, "Enter Amount");
        testInfoLog("Amount", String.valueOf(_transferAmount));
        type(txtTransferAmount, String.valueOf(_transferAmount));

        return new ConsumerTransferVerification(driver);

    }

    public ConsumerTransferVerification enterMaxBalanceAsTransfer() {

        _transferAmount = _transferSenderBalance + 100;
        scrollToElement(driver, txtTransferAmount);
        testStepsLog(_logStep++, "Enter Amount");
        testInfoLog("Amount", String.valueOf(_transferAmount));
        type(txtTransferAmount, String.valueOf(_transferAmount));

        return new ConsumerTransferVerification(driver);

    }

    public ConsumerTransferVerification getTransferReceiverBalance() {
        ConsumerDashboardIndexPage.getCurrentBalance();
        _transferReceiverBalance = getDoubleFromString(ConsumerDashboardIndexPage._currentBalance);
        return new ConsumerTransferVerification(driver);
    }

    public ConsumerTransferVerification getTransferSenderBalance() {
        ConsumerDashboardIndexPage.getCurrentBalance();
        _transferSenderBalance = getDoubleFromString(ConsumerDashboardIndexPage._currentBalance);
        return new ConsumerTransferVerification(driver);
    }

    public boolean isBalanceLessThanAccepted() {
        return _transferSenderBalance < 100000;
    }

    public ConsumerTransferVerification enterMessageForTransfer() {
        if (getRandomBoolean()) {
            scrollToElement(driver, txtTransferMessage);
            _transferMessage = getRandomMessage();
            testStepsLog(_logStep++, "Enter Message : " + _transferMessage);
            type(txtTransferMessage, _transferMessage);
        } else {
            _transferMessage = "";
        }
        return new ConsumerTransferVerification(driver);
    }

    public ConsumerTransferVerification clickOnBackToHome() {

        testStepsLog(_logStep++, "Click on Back To Home button.");
        clickOn(driver, btnBackToHome);

        return new ConsumerTransferVerification(driver);
    }
}
