package com.incashme.consumer.indexpage;

import com.framework.init.AbstractPage;
import com.incashme.consumer.verification.ConsumerTransactionHistoryVerification;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ConsumerTransactionHistoryIndexPage extends AbstractPage {

    public ConsumerTransactionHistoryIndexPage(WebDriver driver) {
        super(driver);
    }

    public static List<String> _allTransactionDetailsCategory = new LinkedList<>();

    public static List<String> _userNameBeforeSort = new ArrayList<>();
    public static List<String> _userTxIDBeforeSort = new ArrayList<>();
    public static List<String> _userCategoryBeforeSort = new ArrayList<>();
    public static List<String> _txTypeBeforeSort = new ArrayList<>();
    public static List<LocalDateTime> _transactionDateTimeBeforeSort = new ArrayList<>();

    public static double _filterTransactionFromAmount = 0.0d;
    public static double _filterTransactionToAmount = 0.0d;

    public static String _searchCriteria = "";
    public static boolean _isRandomFilter;

    public static String _transactionUsername = "";
    public static int _transactionID;
    public static long _transactionUserIDNumber;
    public static String _transactionCategory = "";
    public static double _transactionAmount;
    public static String _transactionType = "";
    public static String _transactionDate = "";
    public static String _transactionTime = "";

    public static int _userNumber = -1;

    @FindBy(xpath = "//ul/li[contains(@id,'li_transactions')]")
    private WebElement btnMenuTransactionHistory;

    @FindAll(value = {@FindBy(xpath = "//table[@id='transactionHistoryTbl']//tr//td[1]//h5")})
    private List<WebElement> lstDashboardUserName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='transactionHistoryTbl']//tr//td[1]//h5//following-sibling::div")})
    private List<WebElement> lstDashboardUserNumberID;

    @FindAll(value = {@FindBy(xpath = "//table[@id='transactionHistoryTbl']//tr//td[2]")})
    private List<WebElement> lstDashboardTxID;

    @FindAll(value = {@FindBy(xpath = "//table[@id='transactionHistoryTbl']//tr//td[3]")})
    private List<WebElement> lstDashboardUserCategory;

    @FindAll(value = {@FindBy(xpath = "//table[@id='transactionHistoryTbl']//tr//td[4]")})
    private List<WebElement> lstDashboardTxAmount;

    @FindAll(value = {@FindBy(xpath = "//table[@id='transactionHistoryTbl']//tr//td[5]")})
    private List<WebElement> lstDashboardUserType;

    @FindAll(value = {@FindBy(xpath = "//table[@id='transactionHistoryTbl']//tr//td[6]")})
    private List<WebElement> lstDashboardTransactionTime;

    @FindBy(xpath = "//ul/li//button[@id='li_']")
    private WebElement btnAllFilterTransaction;

    @FindBy(xpath = "//ul/li//button[@id='li_C']")
    private WebElement btnIndividualFilterTransaction;

    @FindBy(xpath = "//ul/li//button[@id='li_M']")
    private WebElement btnMerchantFilterTransaction;

    @FindBy(xpath = "//ul/li//button[@id='li_A']")
    private WebElement btnAgentFilterTransaction;

    @FindBy(xpath = "//ul/li//button[@id='li_F']")
    private WebElement btnFilterTransaction;

    @FindBy(xpath = "//table[@id='transactionHistoryTbl']//th[1]")
    private WebElement lblUserNameHeader;

    @FindBy(xpath = "//table[@id='transactionHistoryTbl']//th[2]")
    private WebElement lblTxIDHeader;

    @FindBy(xpath = "//table[@id='transactionHistoryTbl']//th[3]")
    private WebElement lblUserCategoryHeader;

    @FindBy(xpath = "//table[@id='transactionHistoryTbl']//th[5]")
    private WebElement lblUserTypeHeader;

    @FindBy(xpath = "//table[@id='transactionHistoryTbl']//th[6]")
    private WebElement lblTransactionDateTime;

    @FindBy(xpath = "//input[@type='search']")
    private WebElement txtInputSearch;

    @FindBy(xpath = "//input[@formcontrolname='min_amount']")
    private WebElement txtFilterMinAmount;

    @FindBy(xpath = "//input[@formcontrolname='max_amount']")
    private WebElement txtFilterMaxAmount;

    @FindBy(xpath = "//mdb-select[@formcontrolname='usertype']")
    private WebElement btnCategoryDropdown;

    @FindAll(value = {@FindBy(xpath = "//mdb-select[@formcontrolname='usertype']//li/span")})
    private List<WebElement> lstFilterCategory;

    @FindBy(xpath = "//div[contains(@class,'filter-body')]//button[@type='submit']")
    private WebElement btnFilterSubmit;

    @FindBy(xpath = "//div[contains(@class,'filter-body')]//button[text()='Clear all']")
    private WebElement btnFilterClearAll;

    @FindAll(value = {@FindBy(xpath = "//table[@id='transactionHistoryTbl']//tr//td[7]//button")})
    private List<WebElement> lstBtnMore;

    @FindBy(xpath = "//*[text()='Transaction Detail']/ancestor::div//button[contains(@class,'close')]")
    private WebElement btnCloseTxDetailsPopup;

    public ConsumerTransactionHistoryVerification clickOnTransactionHistory() {

        testStepsLog(_logStep++, "Click on Transaction History button.");
        clickOn(driver, btnMenuTransactionHistory);

        return new ConsumerTransactionHistoryVerification(driver);
    }

    public boolean isTransactionHistoryAvailable() {
        return !isListEmpty(lstDashboardUserName);
    }

    public ConsumerTransactionHistoryVerification getCurrentConsumerCategoryDetails() {

        try {
            updateShowList(driver, findElementByName(driver, "transactionHistoryTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View Display");
        }

        _allTransactionDetailsCategory.clear();

        for (WebElement category : lstDashboardUserCategory) {
            _allTransactionDetailsCategory.add(getInnerText(category));
        }

        return new ConsumerTransactionHistoryVerification(driver);
    }

    public ConsumerTransactionHistoryVerification clickOnIndividualButton() {

        testStepsLog(_logStep++, "Click on Individual button.");
        clickOn(driver, btnIndividualFilterTransaction);
        pause(2);

        try {
            updateShowList(driver, findElementByName(driver, "transactionHistoryTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View Display");
        }

        return new ConsumerTransactionHistoryVerification(driver);

    }

    public ConsumerTransactionHistoryVerification clickOnMerchantButton() {

        testStepsLog(_logStep++, "Click on Merchant button.");
        clickOn(driver, btnMerchantFilterTransaction);
        pause(2);

        try {
            updateShowList(driver, findElementByName(driver, "transactionHistoryTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View Display");
        }

        return new ConsumerTransactionHistoryVerification(driver);

    }

    public ConsumerTransactionHistoryVerification clickOnAgentButton() {

        testStepsLog(_logStep++, "Click on Agent button.");
        clickOn(driver, btnAgentFilterTransaction);
        pause(2);

        try {
            updateShowList(driver, findElementByName(driver, "transactionHistoryTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View Display");
        }

        return new ConsumerTransactionHistoryVerification(driver);

    }

    public ConsumerTransactionHistoryVerification clickOnAllButton() {

        testStepsLog(_logStep++, "Click on Agent button.");
        clickOn(driver, btnAllFilterTransaction);
        pause(2);

        try {
            updateShowList(driver, findElementByName(driver, "transactionHistoryTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View Display");
        }

        return new ConsumerTransactionHistoryVerification(driver);

    }

    public ConsumerTransactionHistoryVerification clickOnUserNameToSort() {

        try {
            updateShowList(driver, findElementByName(driver, "transactionHistoryTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View Display");
        }

        for (WebElement name : lstDashboardUserName) {
            _userNameBeforeSort.add(getInnerText(name));
        }

        testStepsLog(_logStep++, "Click On Name/Id to sort.");
        clickOn(driver, lblUserNameHeader);

        return new ConsumerTransactionHistoryVerification(driver);

    }

    public ConsumerTransactionHistoryVerification clickOnTransactionIDToSort() {

        try {
            updateShowList(driver, findElementByName(driver, "transactionHistoryTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View Display");
        }

        for (WebElement name : lstDashboardTxID) {
            _userTxIDBeforeSort.add(getInnerText(name));
        }

        testStepsLog(_logStep++, "Click On Transaction ID to sort.");
        clickOn(driver, lblTxIDHeader);

        return new ConsumerTransactionHistoryVerification(driver);

    }

    public ConsumerTransactionHistoryVerification clickOnCategoryToSort() {

        try {
            updateShowList(driver, findElementByName(driver, "transactionHistoryTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View Display");
        }

        for (WebElement name : lstDashboardUserCategory) {
            _userCategoryBeforeSort.add(getInnerText(name));
        }

        testStepsLog(_logStep++, "Click On Category to sort.");
        clickOn(driver, lblUserCategoryHeader);

        return new ConsumerTransactionHistoryVerification(driver);

    }

    public ConsumerTransactionHistoryVerification clickOnTypesToSort() {

        try {
            updateShowList(driver, findElementByName(driver, "transactionHistoryTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View Display");
        }

        for (WebElement name : lstDashboardUserType) {
            _txTypeBeforeSort.add(getInnerText(name));
        }

        testStepsLog(_logStep++, "Click On Types to sort.");
        clickOn(driver, lblUserTypeHeader);

        return new ConsumerTransactionHistoryVerification(driver);

    }

    public ConsumerTransactionHistoryVerification clickOnDateTimeToSort() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        try {
            updateShowList(driver, findElementByName(driver, "transactionHistoryTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View Display");
        }

        for (WebElement name : lstDashboardTransactionTime) {
            _transactionDateTimeBeforeSort.add(LocalDateTime.parse(getInnerText(name), format));
        }

        testStepsLog(_logStep++, "Click On Date & Time to sort.");
        clickOn(driver, lblTransactionDateTime);

        return new ConsumerTransactionHistoryVerification(driver);

    }

    public ConsumerTransactionHistoryVerification enterDashboardSearchCriteria(boolean isInvalid) {

        if (isInvalid) {

            scrollToElement(driver, txtInputSearch);

            String searchCriteria = getRandomCharacters(10);
            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", searchCriteria);
            type(txtInputSearch, searchCriteria);

        } else {

            scrollToElement(driver, txtInputSearch);

            _searchCriteria = getInnerText(lstDashboardTxID.get(0));

            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", _searchCriteria);
            type(txtInputSearch, _searchCriteria);
        }

        return new ConsumerTransactionHistoryVerification(driver);
    }

    public ConsumerTransactionHistoryVerification clickOnFilterButton() {

        testStepsLog(_logStep++, "Click on Filter button.");
        clickOn(driver, btnFilterTransaction);

        return new ConsumerTransactionHistoryVerification(driver);

    }

    public ConsumerTransactionHistoryVerification enterTransactionAmountRange(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterTransactionFromAmount = getRandomNumberBetween(550, 999);
            _filterTransactionToAmount = getRandomNumberBetween(100, 549);

            testStepsLog(_logStep++, "Enter Min Amount Range : " + _filterTransactionFromAmount);
            type(txtFilterMinAmount, String.valueOf(_filterTransactionFromAmount));

            testStepsLog(_logStep++, "Enter Max Amount Range : " + _filterTransactionToAmount);
            type(txtFilterMaxAmount, String.valueOf(_filterTransactionToAmount));
        } else {
            _filterTransactionFromAmount = getRandomNumberBetween(10, 199);
            _filterTransactionToAmount = getRandomNumberBetween(200, 399);

            testStepsLog(_logStep++, "Enter Min Amount Range : " + _filterTransactionFromAmount);
            type(txtFilterMinAmount, String.valueOf(_filterTransactionFromAmount));

            testStepsLog(_logStep++, "Enter Max Amount Range : " + _filterTransactionToAmount);
            type(txtFilterMaxAmount, String.valueOf(_filterTransactionToAmount));
        }

        return new ConsumerTransactionHistoryVerification(driver);

    }

    public ConsumerTransactionHistoryVerification enterTransactionAmount(String field) {

        _filterTransactionFromAmount = getRandomNumberBetween(100, 549);
        _filterTransactionToAmount = getRandomNumberBetween(550, 999);

        switch (field.toLowerCase()) {
            case "from":
                testStepsLog(_logStep++, "Enter Min Amount Range : " + _filterTransactionFromAmount);
                type(txtFilterMinAmount, String.valueOf(_filterTransactionFromAmount));
                break;
            case "to":
                testStepsLog(_logStep++, "Enter Max Amount Range : " + _filterTransactionToAmount);
                type(txtFilterMaxAmount, String.valueOf(_filterTransactionToAmount));
                break;
        }

        return new ConsumerTransactionHistoryVerification(driver);

    }

    public ConsumerTransactionHistoryVerification clickOnDoneButton() {

        testStepsLog(_logStep++, "Click on Done button.");
        clickOn(driver, btnFilterSubmit);

        return new ConsumerTransactionHistoryVerification(driver);
    }

    public ConsumerTransactionHistoryVerification clickOnClearAllButton() {

        testStepsLog(_logStep++, "Click on Clear All button.");
        clickOn(driver, btnFilterClearAll);

        return new ConsumerTransactionHistoryVerification(driver);
    }

    public ConsumerTransactionHistoryVerification selectAgentCategory() {

        testStepsLog(_logStep++, "Select Agent from category.");
        clickOn(driver, btnCategoryDropdown);
        pause(1);
        clickOn(driver, lstFilterCategory.get(1));

        return new ConsumerTransactionHistoryVerification(driver);
    }

    public ConsumerTransactionHistoryVerification selectIndividualCategory() {

        testStepsLog(_logStep++, "Select Individual from category.");
        clickOn(driver, btnCategoryDropdown);
        pause(1);
        clickOn(driver, lstFilterCategory.get(2));

        return new ConsumerTransactionHistoryVerification(driver);
    }

    public ConsumerTransactionHistoryVerification selectMerchantCategory() {

        testStepsLog(_logStep++, "Select Merchant from category.");
        clickOn(driver, btnCategoryDropdown);
        pause(1);
        clickOn(driver, lstFilterCategory.get(3));

        return new ConsumerTransactionHistoryVerification(driver);
    }

    public ConsumerTransactionHistoryVerification getTransactionDetails(String userType) {

        try {
            updateShowList(driver, findElementByName(driver, "transactionHistoryTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View Display");
        }

        if (userType.equalsIgnoreCase("Merchant")) {
            for (int user = 0; user < sizeOf(lstDashboardUserName); user++) {
                System.out.println(getInnerText(lstDashboardUserNumberID.get(user)));
                if (getInnerText(lstDashboardUserNumberID.get(user)).contains("ID-")) {
                    _userNumber = user;
                    _transactionUsername = getInnerText(lstDashboardUserName.get(user));
                    _transactionUserIDNumber = getLongFromString(getInnerText(lstDashboardUserNumberID.get(user)));
                    _transactionID = getIntegerFromString(getInnerText(lstDashboardTxID.get(user)));
                    _transactionCategory = getInnerText(lstDashboardUserCategory.get(user));
                    _transactionAmount = getDoubleFromString(getInnerText(lstDashboardTxAmount.get(user)));
                    _transactionType = getInnerText(lstDashboardUserType.get(user));
                    _transactionDate = LocalDate.parse(getInnerText(lstDashboardTransactionTime.get(user)).split(" ")[0],
                            DateTimeFormatter.ofPattern("dd-MM-yyyy")).format(DateTimeFormatter.ofPattern("dd MMM yyyy"));
                    _transactionTime = LocalTime.parse((getInnerText(lstDashboardTransactionTime.get(user)).split(" ")[1] + " " +
                                    getInnerText(lstDashboardTransactionTime.get(user)).split(" ")[2]),
                            DateTimeFormatter.ofPattern("h:mm:ss a")).format(DateTimeFormatter.ofPattern("h:mm a"));
                    break;
                }
            }
        } else {
            for (int user = 0; user < sizeOf(lstDashboardUserName); user++) {
                if (!getInnerText(lstDashboardUserNumberID.get(user)).contains("ID")) {
                    System.out.println(getInnerText(lstDashboardUserNumberID.get(user)));
                    _userNumber = user;
                    _transactionUsername = getInnerText(lstDashboardUserName.get(user));
                    _transactionUserIDNumber = getLongFromString(getInnerText(lstDashboardUserNumberID.get(user)));
                    _transactionID = getIntegerFromString(getInnerText(lstDashboardTxID.get(user)));
                    _transactionCategory = getInnerText(lstDashboardUserCategory.get(user));
                    _transactionAmount = getDoubleFromString(getInnerText(lstDashboardTxAmount.get(user)));
                    _transactionType = getInnerText(lstDashboardUserType.get(user));
                    _transactionDate = LocalDate.parse(getInnerText(lstDashboardTransactionTime.get(user)).split(" ")[0],
                            DateTimeFormatter.ofPattern("dd-MM-yyyy")).format(DateTimeFormatter.ofPattern("dd MMM yyyy"));
                    _transactionTime = LocalTime.parse((getInnerText(lstDashboardTransactionTime.get(user)).split(" ")[1] + " " +
                                    getInnerText(lstDashboardTransactionTime.get(user)).split(" ")[2]),
                            DateTimeFormatter.ofPattern("h:mm:ss a")).format(DateTimeFormatter.ofPattern("h:mm a"));
                    break;
                }
            }
        }

        System.out.println(_transactionDate);
        System.out.println(_transactionTime);

        return new ConsumerTransactionHistoryVerification(driver);
    }

    public ConsumerTransactionHistoryVerification clickOnMOREButton() {

        testStepsLog(_logStep++, "Click on MORE button.");
        clickOn(driver, lstBtnMore.get(_userNumber));

        return new ConsumerTransactionHistoryVerification(driver);

    }

    public ConsumerTransactionHistoryVerification clickOnPopUpCloseButton() {

        testStepsLog(_logStep++, "Click On Close button.");
        clickOn(driver, btnCloseTxDetailsPopup);

        return new ConsumerTransactionHistoryVerification(driver);
    }
}
