package com.incashme.consumer.indexpage;

import com.framework.init.AbstractPage;
import com.incashme.consumer.verification.ConsumerAddMoneyVerification;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;

public class ConsumerAddMoneyIndexPage extends AbstractPage {

    public ConsumerAddMoneyIndexPage(WebDriver driver) {
        super(driver);
    }

    public static String _CardHolder = "";
    public static String _CardNumber = "";
    public static String _expiryDate = "";

    public static int _totalDebitCard;
    public static int _totalCreditCard;

    public static double _currentAccountBalance;
    public static double _transactionAmount;

    public static String _searchCriteria;

    public static String _bankName;

    public static List<String> _bankNamesBeforeSort = new ArrayList<>();

    @FindBy(xpath = "//ul/li[contains(@id,'li_addmoney')]")
    private WebElement btnMenuAddMoney;

    @FindAll(value = {@FindBy(xpath = "//h5[contains(text(),'Debit')]//parent::div//following-sibling::ul//li[not(contains(@class,'no_card'))]")})
    public List<WebElement> lstDebitCards;

    @FindAll(value = {@FindBy(xpath = "//h5[contains(text(),'Credit')]//parent::div//following-sibling::ul//li[not(contains(@class,'no_card'))]")})
    public List<WebElement> lstCreditCards;

    @FindBy(xpath = "//div[contains(@class,'dcc-setion')]//li//h4[contains(text(),'Select Bank')]")
    public WebElement btnSelectBank;

    @FindBy(xpath = "//h5[contains(text(),'Debit')]//following-sibling::div//a")
    private WebElement btnAddDebitCard;

    @FindBy(xpath = "//h5[contains(text(),'Credit')]//following-sibling::div//a")
    private WebElement btnAddCreditCard;

    @FindBy(xpath = "//input[@id='cardname']")
    private WebElement txtDebitCardHolderName;

    @FindBy(xpath = "//input[@id='cardnum']")
    private WebElement txtDebitCardNumber;

    @FindBy(xpath = "//input[@id='cardexpiry']")
    private WebElement txtDebitExpiryDate;

    @FindBy(xpath = "//input[@id='cardcvv']")
    private WebElement txtDebitCardCVV;

    @FindBy(xpath = "//input[@id='amount']")
    private WebElement txtTransactionAmount;

    @FindBy(xpath = "//button[contains(@class,'btn-green') and text()='Save']")
    private WebElement btnSaveCard;

    @FindBy(xpath = "//button[contains(@class,'btn-pink') and text()='Close']")
    private WebElement btnCloseForm;

    @FindBy(xpath = "//button[contains(@class,'btn-green') and text()='Pay']")
    private WebElement btnPayAddMoney;

    @FindBy(xpath = "//button[contains(@class,'btn-pink') and text()='Cancel']")
    private WebElement btnCancelAddMoney;

    @FindBy(xpath = "//h5[contains(text(),'Debit')]//parent::div//following-sibling::ul//li[not(contains(@class,'no_card'))]//a[contains(@class,'dcc-delete')]")
    private WebElement btnDeleteDebitCard;

    @FindBy(xpath = "//h5[contains(text(),'Credit')]//parent::div//following-sibling::ul//li[not(contains(@class,'no_card'))]//a[contains(@class,'dcc-delete')]")
    private WebElement btnDeleteCreditCard;

    @FindBy(xpath = "//button[contains(@class,'btn-danger') and text()='No']")
    private WebElement btnDeleteCardNo;

    @FindBy(xpath = "//button[contains(@class,'btn-success') and text()='Yes']")
    private WebElement btnDeleteCardYes;

    @FindAll(value = {@FindBy(xpath = "//h5[contains(text(),'Debit')]//parent::div//following-sibling::ul//li[not(contains(@class,'no_card'))]//span[contains(@class,'dcc-number')]")})
    private List<WebElement> lstDebitCardNumber;

    @FindAll(value = {@FindBy(xpath = "//h5[contains(text(),'Credit')]//parent::div//following-sibling::ul//li[not(contains(@class,'no_card'))]//span[contains(@class,'dcc-number')]")})
    private List<WebElement> lstCreditCardNumber;

    @FindBy(xpath = "//div//ul//li//span[contains(@class,'ic-logout')]")
    private WebElement btnLogout;

    @FindAll(value = {@FindBy(xpath = "//table[contains(@id,'banksListTable')]//td//a[text()='Select']")})
    private List<WebElement> lstSelectBankButton;

    @FindAll(value = {@FindBy(xpath = "//table[contains(@id,'banksListTable')]//td//h5")})
    private List<WebElement> lstBankName;

    @FindBy(xpath = "//table[@id='banksListTable']//th[1]")
    public WebElement lblSelectBankHeader;

    @FindBy(xpath = "//input[@type='search']")
    private WebElement txtInputSearch;

    @FindBy(xpath = "//button[contains(@class,'btn-green') and text()='Proceed']")
    private WebElement btnProceedNetBanking;

    public ConsumerAddMoneyVerification clickOnAddMoney() {

        testStepsLog(_logStep++, "Click on Add Money button.");
        clickOn(driver, btnMenuAddMoney);
        mouseHoverTo(driver, btnLogout);

        return new ConsumerAddMoneyVerification(driver);
    }

    public boolean isMaxDebitCardsAdded() {
        return sizeOf(lstDebitCards) == 4;
    }

    public boolean isMaxCreditCardsAdded() {
        return sizeOf(lstCreditCards) == 4;
    }

    public ConsumerAddMoneyVerification clickOnAddDebitCardButton() {

        mouseHoverTo(driver, btnAddDebitCard);

        testStepsLog(_logStep++, "Click on Add button.");
        clickOnJS(driver, btnAddDebitCard);

        return new ConsumerAddMoneyVerification(driver);
    }

    public ConsumerAddMoneyVerification clickOnAddCreditCardButton() {

        mouseHoverTo(driver, btnAddCreditCard);

        testStepsLog(_logStep++, "Click on Add button.");
        clickOnJS(driver, btnAddCreditCard);

        return new ConsumerAddMoneyVerification(driver);
    }

    public ConsumerAddMoneyVerification enterInvalidCardHolderName() {
        _CardHolder = getRandomFirstName() + " " + getRandomLastName();
        testStepsLog(_logStep++, "Enter Card Holder Name");
        testInfoLog("Card Holder", _CardHolder);
        type(txtDebitCardHolderName, _CardHolder);

        return new ConsumerAddMoneyVerification(driver);
    }

    public ConsumerAddMoneyVerification enterCardHolderName() {
        _CardHolder = getRandomFirstName() + getRandomLastName();
        testStepsLog(_logStep++, "Enter Card Holder Name");
        testInfoLog("Card Holder", _CardHolder);
        type(txtDebitCardHolderName, _CardHolder);

        return new ConsumerAddMoneyVerification(driver);
    }

    public ConsumerAddMoneyVerification enterDebitCardNumber() {
        _CardNumber = getRandomNumberBetween(1000, 9999) + "" + getRandomNumberBetween(1000, 9999) + ""
                + getRandomNumberBetween(1000, 9999) + "" + getRandomNumberBetween(1000, 9999);

        testStepsLog(_logStep++, "Enter Debit/Credit Card Number");
        testInfoLog("Card Number", _CardNumber);
        type(txtDebitCardNumber, _CardNumber);

        return new ConsumerAddMoneyVerification(driver);
    }

    public ConsumerAddMoneyVerification enterExpiryDate() {
        _expiryDate = String.format("%02d", getRandomNumberBetween(1, 9)) + "" + getRandomNumberBetween(25, 30);
        testStepsLog(_logStep++, "Enter Expiry Date");
        testInfoLog("Expiry Date", _expiryDate);
        type(txtDebitExpiryDate, _expiryDate);

        return new ConsumerAddMoneyVerification(driver);

    }

    public ConsumerAddMoneyVerification enterInvalidExpiryDate() {
        _expiryDate = String.format("%02d", getRandomNumberBetween(13, 20)) + "" + getRandomNumberBetween(10, 20);
        testStepsLog(_logStep++, "Enter Expiry Date");
        testInfoLog("Expiry Date", _expiryDate);
        type(txtDebitExpiryDate, _expiryDate);

        return new ConsumerAddMoneyVerification(driver);

    }

    public ConsumerAddMoneyVerification clickOnSaveButton() {
        testStepsLog(_logStep++, "Click on Save button.");
        scrollElement(btnSaveCard);
        clickOn(driver, btnSaveCard);

        return new ConsumerAddMoneyVerification(driver);
    }

    public ConsumerAddMoneyVerification openLatestAddedDebitCard() {
        testStepsLog(_logStep++, "Click on Card to open.");
        clickOn(driver, lstDebitCards.get(0));

        return new ConsumerAddMoneyVerification(driver);
    }

    public ConsumerAddMoneyVerification openLatestAddedCreditCard() {
        testStepsLog(_logStep++, "Click on Card to open.");
        clickOn(driver, lstCreditCards.get(0));

        return new ConsumerAddMoneyVerification(driver);
    }

    public boolean isNoDebitCard() {
        _totalDebitCard = sizeOf(lstDebitCards);
        return isListEmpty(lstDebitCards);
    }

    public boolean isNoCreditCard() {
        _totalCreditCard = sizeOf(lstCreditCards);
        return isListEmpty(lstCreditCards);
    }

    public ConsumerAddMoneyVerification clickOnDeleteDebitCardButton() {
        testStepsLog(_logStep++, "Click on Delete button.");
        clickOn(driver, btnDeleteDebitCard);

        return new ConsumerAddMoneyVerification(driver);
    }

    public ConsumerAddMoneyVerification clickOnDeleteCreditCardButton() {
        testStepsLog(_logStep++, "Click on Delete button.");
        clickOn(driver, btnDeleteCreditCard);

        return new ConsumerAddMoneyVerification(driver);
    }

    public ConsumerAddMoneyVerification clickOnDeleteCardNoButton() {
        testStepsLog(_logStep++, "Click on No button.");
        clickOn(driver, btnDeleteCardNo);

        return new ConsumerAddMoneyVerification(driver);
    }

    public ConsumerAddMoneyVerification clickOnDeleteCardYesButton() {
        testStepsLog(_logStep++, "Click on Yes button.");
        clickOn(driver, btnDeleteCardYes);

        return new ConsumerAddMoneyVerification(driver);
    }

    public ConsumerAddMoneyVerification getCurrentAccountBalance() {
        ConsumerDashboardIndexPage.getCurrentBalance();
        _currentAccountBalance = getDoubleFromString(ConsumerDashboardIndexPage._currentBalance);
        return new ConsumerAddMoneyVerification(driver);
    }

    public boolean isTestDebitCardDisplay() {
        boolean bool = false;

        for (WebElement card : lstDebitCardNumber) {
            if (getInnerText(card).equalsIgnoreCase(TEST_CARD_NUMBER)) {
                bool = true;
                break;
            }
        }
        return bool;
    }

    public boolean isTestCreditCardDisplay() {
        boolean bool = false;

        for (WebElement card : lstCreditCardNumber) {
            if (getInnerText(card).equalsIgnoreCase(TEST_CARD_NUMBER)) {
                bool = true;
                break;
            }
        }
        return bool;
    }

    public ConsumerAddMoneyVerification enterTestCardDetails() {
        testStepsLog(_logStep++, "Enter Card Holder Name");
        testInfoLog("Card Holder", TEST_CARD_NAME);
        type(txtDebitCardHolderName, TEST_CARD_NAME);
        pause(1);
        testStepsLog(_logStep++, "Enter Debit/Credit Card Number");
        testInfoLog("Card Number", TEST_CARD_NUMBER);
        type(txtDebitCardNumber, TEST_CARD_NUMBER);
        pause(1);
        testStepsLog(_logStep++, "Enter Expiry Date");
        testInfoLog("Expiry Date", TEST_CARD_EXP_DATE);
        type(txtDebitExpiryDate, TEST_CARD_EXP_DATE);

        return new ConsumerAddMoneyVerification(driver);
    }

    public ConsumerAddMoneyVerification openTestDebitCard() {
        for (WebElement card : lstDebitCardNumber) {
            if (getInnerText(card).equalsIgnoreCase(TEST_CARD_NUMBER)) {
                clickOn(driver, card);
                break;
            }
        }
        return new ConsumerAddMoneyVerification(driver);
    }

    public ConsumerAddMoneyVerification openTestCreditCard() {
        for (WebElement card : lstCreditCardNumber) {
            if (getInnerText(card).equalsIgnoreCase(TEST_CARD_NUMBER)) {
                clickOn(driver, card);
                break;
            }
        }
        return new ConsumerAddMoneyVerification(driver);
    }

    public ConsumerAddMoneyVerification enterCardDetails() {
        testStepsLog(_logStep++, "Enter Expiry Date");
        testInfoLog("Expiry Date", TEST_CARD_EXP_DATE);
        type(txtDebitExpiryDate, TEST_CARD_EXP_DATE);
        pause(1);
        testStepsLog(_logStep++, "Enter Card CVV");
        testInfoLog("CVV", TEST_CARD_CVV);
        type(txtDebitCardCVV, TEST_CARD_CVV);

        return new ConsumerAddMoneyVerification(driver);
    }

    public ConsumerAddMoneyVerification clickOnSelectBankButton() {
        testStepsLog(_logStep++, "Click on Select Bank button.");
        clickOn(driver, btnSelectBank);

        return new ConsumerAddMoneyVerification(driver);
    }

    public ConsumerAddMoneyVerification clickOnAddMoneyPayButton() {
        testStepsLog(_logStep++, "Click on Pay button.");
        clickOn(driver, btnPayAddMoney);

        return new ConsumerAddMoneyVerification(driver);
    }

    public ConsumerAddMoneyVerification enterMaxTransactionAmount() {
        _transactionAmount = getDoubleFromString("100000.01");
        testStepsLog(_logStep++, "Enter Transaction Amount");
        testInfoLog("Amount", String.valueOf(_transactionAmount));
        type(txtTransactionAmount, String.valueOf(_transactionAmount));

        return new ConsumerAddMoneyVerification(driver);
    }

    public ConsumerAddMoneyVerification enterValidTransactionAmount() {
        _transactionAmount = getDoubleFromString(getRandomNumberBetween(100, 999) + "." + getRandomNumberBetween(10, 99));
        testStepsLog(_logStep++, "Enter Transaction Amount");
        testInfoLog("Amount", String.valueOf(_transactionAmount));
        type(txtTransactionAmount, String.valueOf(_transactionAmount));

        return new ConsumerAddMoneyVerification(driver);
    }

    public boolean isBankListDisplay() {
        return !isListEmpty(lstSelectBankButton);
    }

    public ConsumerAddMoneyVerification clickOnBankNameToSort() {

        updateShowList(driver, findElementByName(driver, "banksListTable_length"), "All");

        for (WebElement name : lstBankName) {
            _bankNamesBeforeSort.add(getInnerText(name));
        }

        testStepsLog(_logStep++, "Click On Select Bank to sort.");
        clickOn(driver, lblSelectBankHeader);

        return new ConsumerAddMoneyVerification(driver);

    }

    public ConsumerAddMoneyVerification enterBankSearchCriteria(boolean isInvalid) {

        if (isInvalid) {

            scrollToElement(driver, txtInputSearch);

            String searchCriteria = getRandomCharacters(10);
            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", searchCriteria);
            type(txtInputSearch, searchCriteria);

        } else {

            scrollToElement(driver, txtInputSearch);

            _searchCriteria = getInnerText(lstBankName.get(getRandomNumberBetween(0, lastIndexOf(lstBankName))));

            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", _searchCriteria);
            type(txtInputSearch, _searchCriteria);
        }

        return new ConsumerAddMoneyVerification(driver);
    }

    public ConsumerAddMoneyVerification selectBankFromTheList() {

        pause(3);

        _bankName = getInnerText(lstBankName.get(getRandomNumberBetween(0, lastIndexOf(lstBankName))));

        testStepsLog(_logStep++, "Search Bank Name");
        testInfoLog("Bank Name", _bankName);
        type(txtInputSearch, _bankName);

        testStepsLog(_logStep++, "Select Bank");
        clickOn(driver, lstSelectBankButton.get(0));

        return new ConsumerAddMoneyVerification(driver);
    }

    public ConsumerAddMoneyVerification clickOnCancelButton() {
        testStepsLog(_logStep++, "Click on Cancel button.");
        clickOn(driver, btnCancelAddMoney);

        return new ConsumerAddMoneyVerification(driver);
    }

    public ConsumerAddMoneyVerification clickOnProceedButton() {
        testStepsLog(_logStep++, "Click On Proceed Button");
        clickOn(driver, btnProceedNetBanking);

        return new ConsumerAddMoneyVerification(driver);
    }
}
