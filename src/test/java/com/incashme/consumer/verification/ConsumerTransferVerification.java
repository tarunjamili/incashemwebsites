package com.incashme.consumer.verification;

import com.framework.init.AbstractPage;
import com.incashme.consumer.indexpage.ConsumerTransferIndexPage;
import com.incashme.consumer.validations.TransferValidations;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ConsumerTransferVerification extends AbstractPage implements TransferValidations {

    public ConsumerTransferVerification(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//div[contains(@class,'searchbox')]//input[@id='payeeName']")
    private WebElement txtSearchNumber;

    @FindBy(xpath = ".//div[contains(@id,'swal2-content') and contains(text(),'pending')]")
    private WebElement lblKYCVerificationPending;

    @FindBy(xpath = ".//div[contains(@id,'swal2-content') and contains(text(),'Insufficient')]")
    private WebElement lblInsufficientFund;

    @FindBy(xpath = ".//div[contains(@class,'swal2-actions')]//button[text()='OK' or text() = 'Ok']")
    private WebElement btnConfirmOK;

    @FindBy(xpath = "//div[contains(@class,'search_user_details')]//h3")
    private WebElement lblTransferUserName;

    @FindBy(xpath = "//div[contains(@class,'search_user_details')]//p")
    private WebElement lblTransferUserNumber;

    @FindBy(xpath = "//form//button[contains(@class,'btn-pink') and text()='Cancel']")
    private WebElement btnCancelTransferRequest;

    @FindBy(xpath = "//button[contains(@class,'btn-green') and text()='Send']")
    private WebElement btnSendTransfer;

    @FindBy(xpath = "//input[@id='amount']")
    private WebElement txtTransferAmount;

    @FindBy(xpath = "//textarea[@id='message']")
    private WebElement txtTransferMessage;

    @FindBy(xpath = "//div[contains(@role,'document')]//div[contains(@class,'amnt-txt')]")
    private WebElement lblTransferAmountPopUp;

    @FindBy(xpath = "//div[contains(@role,'document')]//div[contains(text(),'Money Sent')]")
    private WebElement lblTransferAmountSuccess;

    @FindBy(xpath = "//div[contains(@role,'document')]//div[contains(@class,'tppup-usr-nm')]")
    private WebElement lblTransferUserNamePopUp;

    @FindBy(xpath = "//div[contains(@role,'document')]//div[contains(@class,'tppup-usr-nm')]//following-sibling::div[1]")
    private WebElement lblTransferUserNumberPopUp;

    public boolean verifyNewTransactionScreen() {
        return isElementDisplay(txtSearchNumber);
    }

    public boolean verifyValidationForNonKYCUserTransfer() {

        boolean bool = getText(lblKYCVerificationPending).equalsIgnoreCase(KYC_PENDING);

        testValidationLog(getText(lblKYCVerificationPending));
        clickOn(driver, btnConfirmOK);

        return bool;
    }

    public boolean verifyInsufficientFundValidation() {

        boolean bool = getText(lblInsufficientFund).equalsIgnoreCase(INSUFFICIENT_FUND);

        testValidationLog(getText(lblInsufficientFund));
        clickOn(driver, btnConfirmOK);

        return bool;
    }

    public boolean verifyUserTransferScreen() {

        testInfoLog("User Name", getText(lblTransferUserName) + "<br>User Number : " + getText(lblTransferUserNumber));

        boolean bool = isElementDisplay(lblTransferUserName) && isElementDisplay(lblTransferUserNumber) &&
                isElementDisplay(txtTransferAmount) && isElementDisplay(txtTransferAmount);

        scrollToElement(driver, btnSendTransfer);

        return bool && isElementDisplay(btnSendTransfer) && isElementDisplay(btnCancelTransferRequest);

    }

    public boolean verifySendButtonIsDisabled() {
        return btnSendTransfer.getAttribute("disabled").equalsIgnoreCase("true");
    }

    public boolean verifyTransferDetailsAfterAmountSend() {

        pause(3);

        testConfirmationLog(getText(lblTransferAmountSuccess));

        System.out.println(getText(lblTransferUserNamePopUp));
        System.out.println(ConsumerTransferIndexPage._transferUserName);
        System.out.println(getText(lblTransferUserNumberPopUp).substring(getText(lblTransferUserNumberPopUp).length() - 4));
        System.out.println(ConsumerTransferIndexPage._transferUserNumber.
                substring(ConsumerTransferIndexPage._transferUserNumber.length() - 4));
        System.out.println(getDoubleFromString(getText(lblTransferAmountPopUp)));
        System.out.println(ConsumerTransferIndexPage._transferAmount);

        return getText(lblTransferUserNamePopUp).equalsIgnoreCase(ConsumerTransferIndexPage._transferUserName) &&
                getText(lblTransferUserNumberPopUp).substring(getText(lblTransferUserNumberPopUp).length() - 4).
                        equalsIgnoreCase(ConsumerTransferIndexPage._transferUserNumber.
                                substring(ConsumerTransferIndexPage._transferUserNumber.length() - 4)) &&
                getDoubleFromString(getText(lblTransferAmountPopUp)) == ConsumerTransferIndexPage._transferAmount;

    }


}
