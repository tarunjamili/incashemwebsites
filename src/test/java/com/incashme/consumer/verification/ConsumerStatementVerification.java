package com.incashme.consumer.verification;

import com.framework.init.AbstractPage;
import com.incashme.consumer.indexpage.ConsumerStatementIndexPage;
import com.incashme.consumer.validations.StatementsValidations;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class ConsumerStatementVerification extends AbstractPage implements StatementsValidations {

    public ConsumerStatementVerification(WebDriver driver) {
        super(driver);
    }

    @FindAll(value = {@FindBy(xpath = "//div[@class='stmnt-cntnt']")})
    private List<WebElement> lstStatements;

    @FindAll(value = {@FindBy(xpath = "//div[@class='pst-dt']")})
    private List<WebElement> lstStatementMonth;

    @FindBy(xpath = "//div//h2//strong[contains(text(),'No Statement')]")
    private WebElement lblNoStatement;

    @FindBy(xpath = "//li/button[contains(@class,'filter-btn')]")
    private WebElement btnFilter;

    public boolean verifyStatementsScreen() {

        if (isListEmpty(lstStatements)) {
            testInfoLog("Total Statements", String.valueOf(sizeOf(lstStatements)));
            testValidationLog(getText(lblNoStatement));
            return isElementDisplay(lblNoStatement) && isElementDisplay(btnFilter) &&
                    getText(lblNoStatement).equalsIgnoreCase(NO_STATEMENTS_FOUND);
        } else {
            testInfoLog("Total Statements", String.valueOf(sizeOf(lstStatements)));
            return sizeOf(lstStatements) > 0 && !isElementPresent(lblNoStatement) && isElementDisplay(btnFilter);
        }

    }

    public boolean verifyStatementDetails() {

        if (ConsumerStatementIndexPage._expectedStatements == 0) {
            return isElementDisplay(lblNoStatement) && isElementDisplay(btnFilter) &&
                    getText(lblNoStatement).equalsIgnoreCase(NO_STATEMENTS_FOUND);
        } else {
            testInfoLog("Total Statements", String.valueOf(sizeOf(lstStatements)));
            return sizeOf(lstStatements) == ConsumerStatementIndexPage._expectedStatements &&
                    !isElementPresent(lblNoStatement) && isElementDisplay(btnFilter);
        }
    }

    public boolean verifyStatementDownloaded() {

        String downloadedFileName = getLastFileModified(FILE_DOWNLOAD_PATH);

        System.out.println(downloadedFileName);
        System.out.println(ConsumerStatementIndexPage._statementMonthYear);

        testInfoLog("Downloaded Statement Name", downloadedFileName);

        return downloadedFileName.contains(ConsumerStatementIndexPage._statementMonthYear);

    }

    public boolean verifyStatementFilterSuccessfully() {

        String actualStatementMonthYear = getText(lstStatements.get(0));
        String expectedStatementMonthYear = ConsumerStatementIndexPage._statementMonth + " - " +
                ConsumerStatementIndexPage._statementYear;

        System.out.println(actualStatementMonthYear);
        System.out.println(expectedStatementMonthYear);

        testInfoLog("Filtered Statement", actualStatementMonthYear);

        return actualStatementMonthYear.equalsIgnoreCase(expectedStatementMonthYear) && sizeOf(lstStatements) == 1;
    }

    public boolean verifyNoStatementDisplay() {
        testValidationLog(getText(lblNoStatement));
        return isElementDisplay(lblNoStatement) &&
                getText(lblNoStatement).equalsIgnoreCase(NO_STATEMENTS_FOUND);
    }

}
