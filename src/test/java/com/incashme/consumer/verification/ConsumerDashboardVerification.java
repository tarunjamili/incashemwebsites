package com.incashme.consumer.verification;

import com.framework.init.AbstractPage;
import com.incashme.consumer.indexpage.*;
import com.incashme.consumer.validations.DashboardValidations;
import com.incashme.merchant.indexpage.MerchantDonationIndexPage;
import com.incashme.merchant.indexpage.MerchantInvoiceIndexPage;
import com.incashme.merchant.verification.MerchantLoginVerification;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Rahul R.
 * Date: 2019-07-08
 * Time
 * Project Name: InCashMe
 */

public class ConsumerDashboardVerification extends AbstractPage implements DashboardValidations {

    public ConsumerDashboardVerification(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//div//ul//li//span[contains(@class,'ic-logout')]")
    private WebElement btnLogout;

    @FindBy(xpath = "//div//ul//li//i[contains(@class,'ic-add-user')]")
    private WebElement btnNewTransaction;

    @FindBy(xpath = ".//div[contains(@class,'user-avatar')]//img")
    private WebElement imgUserLogo;

    @FindBy(xpath = "//div[contains(@class,'blnc-txt')]")
    private WebElement lblBalance;

    @FindBy(xpath = "//h5[contains(text(),'Transaction Detail')]//following-sibling::div")
    private WebElement lblTransactionIDPopup;

    @FindAll(value = {@FindBy(xpath = "//*[contains(text(),'Transaction Detail')]/../..//following-sibling::div//li//div[contains(@class,'text-right')]")})
    private List<WebElement> lstTransactionDetails;

    @FindBy(xpath = "//*[text()='Transaction Detail']/ancestor::div//button[contains(@class,'close')]")
    private WebElement btnCloseTxDetailsPopup;

    @FindAll(value = {@FindBy(xpath = "//table[@id='transactionHistoryTbl']//tr//td[7]//button")})
    private List<WebElement> lstBtnMore;

    @FindAll(value = {@FindBy(xpath = "//table[@id='transactionHistoryTbl']//tr//td[1]//h5")})
    private List<WebElement> lstUserName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='transactionHistoryTbl']//tr//td[1]//h5//following-sibling::div")})
    private List<WebElement> lstUserNumber;

    @FindAll(value = {@FindBy(xpath = "//table[@id='transactionHistoryTbl']//tr//td[4]")})
    private List<WebElement> lstUserTxAmount;

    @FindAll(value = {@FindBy(xpath = "//table[@id='transactionHistoryTbl']//tr//td[5]")})
    private List<WebElement> lstUserTxType;

    @FindAll(value = {@FindBy(xpath = "//table[@id='transactionHistoryTbl']//tr//td[3]")})
    private List<WebElement> lstUserTxCategory;

    @FindBy(xpath = "(//div[contains(@class,'user-avatar')]//following-sibling::div[contains(@class,'info_txt')]//div)[1]")
    private WebElement lblConsumerID;

    @FindBy(xpath = "(//div[contains(@class,'user-avatar')]//following-sibling::div[contains(@class,'info_txt')]//div)[2]")
    private WebElement lblConsumerSince;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@class,'pro-content')]//div[contains(@class,'row')]//div[contains(@class,'info-text')]")})
    private List<WebElement> lstConsumerDetails;

    @FindBy(xpath = "//div[contains(@class,'profile-menu')]//ul/li[@id='p_home']")
    private WebElement btnPersonalInfoMenu;

    @FindBy(xpath = "//div[contains(@class,'profile-menu')]//ul/li[@id='p_docs']")
    private WebElement btnDocuments;

    @FindBy(xpath = "//div[contains(@class,'profile-menu')]//ul/li[@id='p_pwd']")
    private WebElement btnChangePasswordMenu;

    @FindBy(xpath = "//input[@id='oldpassword']")
    private WebElement txtCurrentPassword;

    @FindBy(xpath = "//input[@id='password2']")
    private WebElement txtNewPassword;

    @FindBy(xpath = "//input[@id='password']")
    private WebElement txtConfirmPassword;

    @FindBy(xpath = ".//div[contains(@class,'login-form')]//button[@type='submit']")
    private WebElement btnChangePassword;

    @FindAll(value = {@FindBy(xpath = "//table[@id='transactionHistoryTbl']//tr//td[2]")})
    private List<WebElement> lstDashboardTxID;

    @FindAll(value = {@FindBy(xpath = "//table[@id='transactionHistoryTbl']//tr//td[3]")})
    private List<WebElement> lstDashboardUserCategory;

    @FindAll(value = {@FindBy(xpath = "//table[@id='transactionHistoryTbl']//tr//td[5]")})
    private List<WebElement> lstDashboardUserType;

    @FindAll(value = {@FindBy(xpath = "//table[@id='transactionHistoryTbl']//tr//td[6]")})
    private List<WebElement> lstDashboardTransactionTime;

    @FindBy(xpath = "//input[@type='search']")
    private WebElement txtInputSearch;

    @FindBy(xpath = "//table[@id='transactionHistoryTbl']//td")
    private WebElement lblNoRecordsFound;

    @FindBy(xpath = "//div//h2//strong[contains(text(),'No Recent Activity')]")
    private WebElement lblNoTransaction;

    @FindBy(xpath = ".//div[contains(@id,'swal2-content') and contains(text(),'old Password')]")
    private WebElement lblPastPasswordValidation;

    @FindBy(xpath = ".//div[contains(@class,'swal2-actions')]//button[text()='OK' or text() = 'Ok']")
    private WebElement btnConfirmOK;

    @FindBy(xpath = "//input[@id='password2']//parent::div//small")
    private WebElement lblInvalidNewPasswordValidation;

    @FindBy(xpath = "//input[@id='password']//parent::div//small")
    private WebElement lblInvalidConfirmPasswordValidation;

    @FindBy(xpath = ".//div[contains(@id,'swal2-content') and contains(text(),'changed successfully')]")
    private WebElement lblConfirmPasswordChange;

    @FindBy(xpath = ".//div[contains(@id,'swal2-content') and contains(text(),'Unsuccessful attempt')]")
    private WebElement lblInvalidCurrentPasswordValidation;

    public boolean verifyDashboardScreen() {
        if (isListEmpty(lstUserName)) {
            testValidationLog(getText(lblNoTransaction));
            return isElementDisplay(lblNoTransaction);
        } else {
            return isElementDisplay(btnLogout) && isElementDisplay(imgUserLogo) && isElementDisplay(btnNewTransaction);
        }
    }

    public boolean verifyBalanceScreen() {
        ConsumerDashboardIndexPage.getCurrentBalance();
        testInfoLog("Account Balance", getText(lblBalance));
        return isElementDisplay(lblBalance);
    }

    public boolean verifyPrivacyPolicyScreen() {

        switchToWindow(driver);

        String expectedTitle = getTitle(driver);
        String actualTitle = "InCashMe™";

        close(driver);

        switchToWindow(driver);

        pause(1);

        return expectedTitle.equalsIgnoreCase(actualTitle);

    }

    public boolean verifyTnCScreen() {

        switchToWindow(driver);

        String expectedTitle = getTitle(driver);
        String actualTitle = "InCashMe™";

        close(driver);

        switchToWindow(driver);

        pause(1);

        return expectedTitle.equalsIgnoreCase(actualTitle);

    }

    public boolean verifyKYCandAMLScreen() {

        switchToWindow(driver);

        String expectedTitle = getTitle(driver);
        String actualTitle = "InCashMe™";

        close(driver);

        switchToWindow(driver);

        pause(1);

        return expectedTitle.equalsIgnoreCase(actualTitle);

    }

    public boolean verifyTransactionDetails() {

        pause(3);

        boolean bool = getDoubleFromString(getText(lstTransactionDetails.get(1))) == ConsumerTransferIndexPage._transferAmount &&
                getText(lstTransactionDetails.get(2)).split("\n")[0].equalsIgnoreCase(ConsumerLoginVerification._username) &&
                getText(lstTransactionDetails.get(2)).split("\n")[1].equalsIgnoreCase(ConsumerLoginVerification._usernumber) &
                        getText(lstTransactionDetails.get(3)).equalsIgnoreCase("Pay");

        testStepsLog(_logStep++, "Click on Close button.");
        clickOn(driver, btnCloseTxDetailsPopup);

        System.out.println(getText(lstUserNumber.get(0)));
        System.out.println(ConsumerTransferIndexPage._transferUserNumber);

        bool = bool && getText(lstUserName.get(0)).equalsIgnoreCase(ConsumerTransferIndexPage._transferUserName) &&
                getText(lstUserNumber.get(0)).equalsIgnoreCase(ConsumerTransferIndexPage._transferUserNumber) &&
                getDoubleFromString(getText(lstUserTxAmount.get(0))) == ConsumerTransferIndexPage._transferAmount &&
                getText(lstUserTxType.get(0)).equalsIgnoreCase("Pay");

        return bool;
    }


    public boolean verifyAmountDeductedForTransfer() {

        pause(5);
        ConsumerDashboardIndexPage.getCurrentBalance();
        double updatedBalance = formatTwoDecimal(getDoubleFromString(ConsumerDashboardIndexPage._currentBalance));

        System.out.println(updatedBalance);
        System.out.println(ConsumerTransferIndexPage._transferSenderBalance);
        System.out.println(ConsumerTransferIndexPage._transferAmount);

        return formatTwoDecimal(ConsumerTransferIndexPage._transferSenderBalance -
                ConsumerTransferIndexPage._transferAmount) == formatTwoDecimal(updatedBalance);

    }

    public boolean verifyAmountAddedForTransfer() {

        pause(5);
        ConsumerDashboardIndexPage.getCurrentBalance();
        double updatedBalance = formatTwoDecimal(getDoubleFromString(ConsumerDashboardIndexPage._currentBalance));

        System.out.println(updatedBalance);
        System.out.println(ConsumerTransferIndexPage._transferReceiverBalance);
        System.out.println(ConsumerTransferIndexPage._transferAmount);

        return formatTwoDecimal(ConsumerTransferIndexPage._transferReceiverBalance +
                ConsumerTransferIndexPage._transferAmount) == formatTwoDecimal(updatedBalance);

    }

    public boolean verifyInvoiceTransaction() {

        testStepsLog(_logStep++, "Click on MORE button");
        clickOn(driver, lstBtnMore.get(0));

        pause(3);

        System.out.println(getDoubleFromString(getText(lstTransactionDetails.get(1))));
        System.out.println(MerchantInvoiceIndexPage._invoiceAmount);
        System.out.println(getText(lstTransactionDetails.get(2)).split("\n")[0]);
        System.out.println(MerchantInvoiceIndexPage._invoiceUserName);
        System.out.println(getText(lstTransactionDetails.get(2)).split("\n")[1]);
        System.out.println(ConsumerLoginVerification._usernumber);
        System.out.println(getText(lstTransactionDetails.get(4)));
        System.out.println(getText(lstTransactionDetails.get(3)));
        System.out.println(MerchantLoginVerification._username);

        boolean bool = getDoubleFromString(getText(lstTransactionDetails.get(1))) == MerchantInvoiceIndexPage._invoiceAmount &&
                getText(lstTransactionDetails.get(2)).split("\n")[0].equalsIgnoreCase(MerchantInvoiceIndexPage._invoiceUserName) &&
                getText(lstTransactionDetails.get(2)).split("\n")[1].equalsIgnoreCase(ConsumerLoginVerification._usernumber) &
                        getText(lstTransactionDetails.get(4)).equalsIgnoreCase("Invoice") &&
                getText(lstTransactionDetails.get(3)).equalsIgnoreCase(MerchantLoginVerification._username);

        testStepsLog(_logStep++, "Click on Close button.");
        clickOn(driver, btnCloseTxDetailsPopup);

        System.out.println(getText(lstUserName.get(0)));
        System.out.println(MerchantLoginVerification._username);
        System.out.println(getDoubleFromString(getText(lstUserTxAmount.get(0))));
        System.out.println(MerchantInvoiceIndexPage._invoiceAmount);
        System.out.println(getText(lstUserTxType.get(0)));
        System.out.println(getText(lstUserTxCategory.get(0)));

        bool = bool && getText(lstUserName.get(0)).equalsIgnoreCase(MerchantLoginVerification._username) &&
                getDoubleFromString(getText(lstUserTxAmount.get(0))) == MerchantInvoiceIndexPage._invoiceAmount &&
                getText(lstUserTxType.get(0)).equalsIgnoreCase("Invoice") &&
                getText(lstUserTxCategory.get(0)).equalsIgnoreCase("Merchant");

        return bool;

    }

    public boolean verifyConsumerInvoiceTransaction() {

        testStepsLog(_logStep++, "Click on MORE button");
        clickOn(driver, lstBtnMore.get(0));

        pause(3);

        System.out.println(getDoubleFromString(getText(lstTransactionDetails.get(1))));
        System.out.println(ConsumerRequestIndexPage._invoiceAmount);
        System.out.println(getText(lstTransactionDetails.get(2)).split("\n")[0]);
        System.out.println(ConsumerLoginVerification._username);
        System.out.println(getText(lstTransactionDetails.get(2)).split("\n")[1]);
        System.out.println(ConsumerLoginVerification._usernumber);
        System.out.println(getText(lstTransactionDetails.get(4)));
        System.out.println(getText(lstTransactionDetails.get(3)));
        System.out.println(ConsumerRequestIndexPage._invoiceUserName);

        boolean bool = getDoubleFromString(getText(lstTransactionDetails.get(1))) == getDoubleFromString(ConsumerRequestIndexPage._invoiceAmount) &&
                getText(lstTransactionDetails.get(2)).split("\n")[0].equalsIgnoreCase(ConsumerLoginVerification._username) &&
                getText(lstTransactionDetails.get(2)).split("\n")[1].equalsIgnoreCase(ConsumerLoginVerification._usernumber) &
                        getText(lstTransactionDetails.get(4)).equalsIgnoreCase("Invoice") &&
                getText(lstTransactionDetails.get(3)).equalsIgnoreCase(ConsumerRequestIndexPage._invoiceUserName);

        testStepsLog(_logStep++, "Click on Close button.");
        clickOn(driver, btnCloseTxDetailsPopup);

        System.out.println(getText(lstUserName.get(0)));
        System.out.println(ConsumerRequestIndexPage._invoiceUserName);
        System.out.println(getDoubleFromString(getText(lstUserTxAmount.get(0))));
        System.out.println(ConsumerRequestIndexPage._invoiceAmount);
        System.out.println(getText(lstUserTxType.get(0)));
        System.out.println(getText(lstUserTxCategory.get(0)));

        bool = bool && getText(lstUserName.get(0)).equalsIgnoreCase(ConsumerRequestIndexPage._invoiceUserName) &&
                getDoubleFromString(getText(lstUserTxAmount.get(0))) == getDoubleFromString(ConsumerRequestIndexPage._invoiceAmount) &&
                getText(lstUserTxType.get(0)).equalsIgnoreCase("Invoice") &&
                getText(lstUserTxCategory.get(0)).equalsIgnoreCase("Merchant");

        return bool;

    }

    public boolean verifyDonationTransaction() {

        testStepsLog(_logStep++, "Click on MORE button");
        clickOn(driver, lstBtnMore.get(0));

        pause(3);

        System.out.println(getDoubleFromString(getText(lstTransactionDetails.get(1))));
        System.out.println(ConsumerRequestIndexPage._donationAmount);
        System.out.println(getText(lstTransactionDetails.get(2)).split("\n")[0]);
        System.out.println(MerchantDonationIndexPage._donationUserName);
        System.out.println(getText(lstTransactionDetails.get(2)).split("\n")[1]);
        System.out.println(ConsumerLoginVerification._usernumber);
        System.out.println(getText(lstTransactionDetails.get(3)));

        boolean bool = getDoubleFromString(getText(lstTransactionDetails.get(1))) == ConsumerRequestIndexPage._donationAmount &&
                getText(lstTransactionDetails.get(2)).split("\n")[0].equalsIgnoreCase(MerchantDonationIndexPage._donationUserName) &&
                getText(lstTransactionDetails.get(2)).split("\n")[1].equalsIgnoreCase(ConsumerLoginVerification._usernumber) &
                        getText(lstTransactionDetails.get(3)).equalsIgnoreCase("Donation");

        testStepsLog(_logStep++, "Click on Close button.");
        clickOn(driver, btnCloseTxDetailsPopup);

        System.out.println(getText(lstUserName.get(0)));
        System.out.println(MerchantLoginVerification._username);
        System.out.println(getDoubleFromString(getText(lstUserTxAmount.get(0))));
        System.out.println(ConsumerRequestIndexPage._donationAmount);
        System.out.println(getText(lstUserTxType.get(0)));
        System.out.println(getText(lstUserTxCategory.get(0)));

        bool = bool && getText(lstUserName.get(0)).equalsIgnoreCase(MerchantLoginVerification._username) &&
                getDoubleFromString(getText(lstUserTxAmount.get(0))) == ConsumerRequestIndexPage._donationAmount &&
                getText(lstUserTxType.get(0)).equalsIgnoreCase("Donation") &&
                getText(lstUserTxCategory.get(0)).equalsIgnoreCase("Non-Profit");

        return bool;

    }

    public boolean verifyConsumerDonationTransaction() {

        testStepsLog(_logStep++, "Click on MORE button");
        clickOn(driver, lstBtnMore.get(0));

        pause(3);

        System.out.println(getDoubleFromString(getText(lstTransactionDetails.get(1))));
        System.out.println(ConsumerRequestIndexPage._donationAmount);
        System.out.println(getText(lstTransactionDetails.get(2)).split("\n")[0]);
        System.out.println(ConsumerLoginVerification._username);
        System.out.println(getText(lstTransactionDetails.get(2)).split("\n")[1]);
        System.out.println(ConsumerLoginVerification._usernumber);
        System.out.println(getText(lstTransactionDetails.get(3)));

        boolean bool = getDoubleFromString(getText(lstTransactionDetails.get(1))) == ConsumerRequestIndexPage._donationAmount &&
                getText(lstTransactionDetails.get(2)).split("\n")[0].equalsIgnoreCase(ConsumerLoginVerification._username) &&
                getText(lstTransactionDetails.get(2)).split("\n")[1].equalsIgnoreCase(ConsumerLoginVerification._usernumber) &
                        getText(lstTransactionDetails.get(3)).equalsIgnoreCase("Donation");

        testStepsLog(_logStep++, "Click on Close button.");
        clickOn(driver, btnCloseTxDetailsPopup);

        System.out.println(getText(lstUserName.get(0)));
        System.out.println(ConsumerRequestIndexPage._donationUserName);
        System.out.println(getDoubleFromString(getText(lstUserTxAmount.get(0))));
        System.out.println(ConsumerRequestIndexPage._donationAmount);
        System.out.println(getText(lstUserTxType.get(0)));
        System.out.println(getText(lstUserTxCategory.get(0)));

        bool = bool && getText(lstUserName.get(0)).equalsIgnoreCase(ConsumerRequestIndexPage._donationUserName) &&
                getDoubleFromString(getText(lstUserTxAmount.get(0))) == ConsumerRequestIndexPage._donationAmount &&
                getText(lstUserTxType.get(0)).equalsIgnoreCase("Donation") &&
                getText(lstUserTxCategory.get(0)).equalsIgnoreCase("Non-Profit");

        return bool;

    }

    public boolean verifyBalanceDeductedForPaidInvoice() {

        pause(5);
        ConsumerDashboardIndexPage.getCurrentBalance();
        double updatedBalance = formatTwoDecimal(getDoubleFromString(ConsumerDashboardIndexPage._currentBalance));

        System.out.println(updatedBalance);
        System.out.println(ConsumerRequestIndexPage._requestReceiverBalance);
        System.out.println(MerchantInvoiceIndexPage._invoiceAmount);

        return formatTwoDecimal(ConsumerRequestIndexPage._requestReceiverBalance -
                MerchantInvoiceIndexPage._invoiceAmount) == formatTwoDecimal(updatedBalance);

    }

    public boolean verifyBalanceDeductedForInvoice() {

        pause(5);
        ConsumerDashboardIndexPage.getCurrentBalance();
        double updatedBalance = formatTwoDecimal(getDoubleFromString(ConsumerDashboardIndexPage._currentBalance));

        System.out.println(updatedBalance);
        System.out.println(ConsumerRequestIndexPage._requestReceiverBalance);
        System.out.println(ConsumerRequestIndexPage._invoiceAmount);

        return formatTwoDecimal(ConsumerRequestIndexPage._requestReceiverBalance -
                getDoubleFromString(ConsumerRequestIndexPage._invoiceAmount)) == formatTwoDecimal(updatedBalance);

    }

    public boolean verifyBalanceDeductedForPaidDonation() {

        pause(5);
        ConsumerDashboardIndexPage.getCurrentBalance();
        double updatedBalance = formatTwoDecimal(getDoubleFromString(ConsumerDashboardIndexPage._currentBalance));

        System.out.println(updatedBalance);
        System.out.println(ConsumerRequestIndexPage._requestReceiverBalance);
        System.out.println(ConsumerRequestIndexPage._donationAmount);

        return formatTwoDecimal(ConsumerRequestIndexPage._requestReceiverBalance -
                ConsumerRequestIndexPage._donationAmount) == formatTwoDecimal(updatedBalance);

    }

    public boolean verifyProfileScreen() {

        boolean bool = isElementDisplay(btnPersonalInfoMenu) && isElementDisplay(btnDocuments) &&
                isElementDisplay(btnChangePasswordMenu) && btnPersonalInfoMenu.getAttribute("class").contains("active");

        testInfoLog("First Name", getText(lstConsumerDetails.get(0)) +
                "  Last Name : " + getText(lstConsumerDetails.get(1)) +
                "<br>DOB : " + getText(lstConsumerDetails.get(2)) +
                "  Cell Number : " + getText(lstConsumerDetails.get(3)) +
                "<br>Email : " + getText(lstConsumerDetails.get(4)));

        scrollElement(lstConsumerDetails.get(14));

        testInfoLog("Building", getText(lstConsumerDetails.get(5)) +
                "  Street 1 : " + getText(lstConsumerDetails.get(6)) +
                "<br>Street 2 : " + getText(lstConsumerDetails.get(7)) +
                "  Village : " + getText(lstConsumerDetails.get(8)) +
                "<br>Taluka : " + getText(lstConsumerDetails.get(9)) +
                "  City : " + getText(lstConsumerDetails.get(10)) +
                "<br>District : " + getText(lstConsumerDetails.get(11)) +
                "  State : " + getText(lstConsumerDetails.get(12)) +
                "<br>Country : " + getText(lstConsumerDetails.get(13)) +
                "  Pin Code : " + getText(lstConsumerDetails.get(14)));

        return bool;

    }

    public boolean verifyDocumentsScreenOpen() {
        return isElementDisplay(btnPersonalInfoMenu) && isElementDisplay(btnDocuments) &&
                isElementDisplay(btnChangePasswordMenu) && btnDocuments.getAttribute("class").contains("active");
    }

    public boolean verifyChangePasswordScreen() {
        return isElementDisplay(txtConfirmPassword) && isElementDisplay(txtCurrentPassword) &&
                isElementDisplay(txtNewPassword) && isElementDisplay(btnChangePassword);
    }

    public boolean verifyUserNameSorted() {

        List<String> userNameAfterSort = new ArrayList<>();
        List<String> userNameBeforeSort = ConsumerDashboardIndexPage._userNameBeforeSort;

        for (WebElement name : lstUserName) {
            userNameAfterSort.add(getInnerText(name));
        }

        userNameBeforeSort.sort(
                (String o1, String o2) -> {
                    if (o1.equalsIgnoreCase(o2)) {
                        return o2.compareTo(o1);
                    }
                    return o1.toLowerCase().compareTo(o2.toLowerCase());
                }
        );

        System.out.println(userNameAfterSort);
        System.out.println(userNameBeforeSort);

        System.out.println(userNameBeforeSort.equals(userNameAfterSort));

        return userNameBeforeSort.equals(userNameAfterSort);
    }

    public boolean verifyUserTxIDSorted() {

        List<String> txIDAfterSort = new ArrayList<>();
        List<String> txIDBeforeSort = ConsumerDashboardIndexPage._userTxIDBeforeSort;

        for (WebElement name : lstDashboardTxID) {
            txIDAfterSort.add(getInnerText(name));
        }

        txIDBeforeSort.sort(
                (String o1, String o2) -> {
                    if (o1.equalsIgnoreCase(o2)) {
                        return o2.compareTo(o1);
                    }
                    return o1.toLowerCase().compareTo(o2.toLowerCase());
                }
        );

        System.out.println(txIDAfterSort);
        System.out.println(txIDBeforeSort);

        System.out.println(txIDBeforeSort.equals(txIDAfterSort));

        return txIDBeforeSort.equals(txIDAfterSort);
    }

    public boolean verifyUserCategorySorted() {

        List<String> userCategoryAfterSort = new ArrayList<>();
        List<String> userCategoryBeforeSort = ConsumerDashboardIndexPage._userCategoryBeforeSort;

        for (WebElement name : lstDashboardUserCategory) {
            userCategoryAfterSort.add(getInnerText(name));
        }

        userCategoryBeforeSort.sort(
                (String o1, String o2) -> {
                    if (o1.equalsIgnoreCase(o2)) {
                        return o2.compareTo(o1);
                    }
                    return o1.toLowerCase().compareTo(o2.toLowerCase());
                }
        );

        System.out.println(userCategoryAfterSort);
        System.out.println(userCategoryBeforeSort);

        System.out.println(userCategoryBeforeSort.equals(userCategoryAfterSort));

        return userCategoryBeforeSort.equals(userCategoryAfterSort);
    }

    public boolean verifyUserTypeSorted() {

        List<String> userTypeAfterSort = new ArrayList<>();
        List<String> userTypeBeforeSort = ConsumerDashboardIndexPage._txTypeBeforeSort;

        for (WebElement name : lstDashboardUserType) {
            userTypeAfterSort.add(getInnerText(name));
        }

        userTypeBeforeSort.sort(
                (String o1, String o2) -> {
                    if (o1.equalsIgnoreCase(o2)) {
                        return o2.compareTo(o1);
                    }
                    return o1.toLowerCase().compareTo(o2.toLowerCase());
                }
        );

        System.out.println(userTypeAfterSort);
        System.out.println(userTypeBeforeSort);

        System.out.println(userTypeBeforeSort.equals(userTypeAfterSort));

        return userTypeBeforeSort.equals(userTypeAfterSort);
    }

    public boolean verifyTransactionDateTimeSorted() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        List<LocalDateTime> transactionDateTimeAfterSort = new ArrayList<>();
        List<LocalDateTime> transactionDateTimeBeforeSort = ConsumerDashboardIndexPage._transactionDateTimeBeforeSort;

        for (WebElement name : lstDashboardTransactionTime) {
            transactionDateTimeAfterSort.add(LocalDateTime.parse(getInnerText(name), format));
        }

        Collections.sort(transactionDateTimeBeforeSort);

        System.out.println(transactionDateTimeAfterSort);
        System.out.println(transactionDateTimeBeforeSort);

        System.out.println(transactionDateTimeBeforeSort.equals(transactionDateTimeAfterSort));

        return transactionDateTimeBeforeSort.equals(transactionDateTimeAfterSort);
    }

    public boolean verifyTableSearchSuccessful() {

        boolean bool = getInnerText(lstDashboardTxID.get(0)).contains(ConsumerDashboardIndexPage._searchCriteria) &&
                sizeOf(lstDashboardTxID) == 1;

        type(txtInputSearch, "");

        return bool;

    }

    public boolean verifyTableValidationForInvalidSearch() {

        testValidationLog(getInnerText(lblNoRecordsFound));

        boolean bool = getInnerText(lblNoRecordsFound).equalsIgnoreCase(NO_SEARCH_RECORD_FOUND);

        type(txtInputSearch, "x");
        txtInputSearch.clear();

        return bool;
    }


    public boolean verifyMerchantTransactionDetailsPopUp() {

        pause(2);

        System.err.println(getIntegerFromString(getText(lblTransactionIDPopup)));
        System.err.println(ConsumerDashboardIndexPage._transactionID);
        System.err.println(formatTwoDecimal(getDoubleFromString(getText(lstTransactionDetails.get(1)))));
        System.err.println(formatTwoDecimal(ConsumerDashboardIndexPage._transactionAmount));
        System.err.println(getText(lstTransactionDetails.get(0)));
        System.err.println(ConsumerDashboardIndexPage._transactionDate + " / " + ConsumerDashboardIndexPage._transactionTime);
        System.err.println(getText(lstTransactionDetails.get(2)).split("\n")[0]);
        System.err.println(ConsumerLoginVerification._username);
        System.err.println(getText(lstTransactionDetails.get(2)).split("\n")[1]);
        System.err.println(ConsumerLoginVerification._usernumber);

        boolean bool;

        if (ConsumerDashboardIndexPage._transactionType.equalsIgnoreCase("Invoice") ||
                ConsumerDashboardIndexPage._transactionType.equalsIgnoreCase("Transfer")) {
            System.out.println(getText(lstTransactionDetails.get(4)));
            System.out.println(ConsumerDashboardIndexPage._transactionType);
            bool = getText(lstTransactionDetails.get(4)).equalsIgnoreCase(ConsumerDashboardIndexPage._transactionType);
        } else {
            System.out.println(getText(lstTransactionDetails.get(3)));
            System.out.println(ConsumerDashboardIndexPage._transactionType);
            bool = getText(lstTransactionDetails.get(3)).equalsIgnoreCase(ConsumerDashboardIndexPage._transactionType);
        }

        return bool && getIntegerFromString(getText(lblTransactionIDPopup)) == (ConsumerDashboardIndexPage._transactionID) &&
                getText(lstTransactionDetails.get(0)).equalsIgnoreCase(ConsumerDashboardIndexPage._transactionDate
                        + " / " + ConsumerDashboardIndexPage._transactionTime) &&
                formatTwoDecimal(getDoubleFromString(getText(lstTransactionDetails.get(1)))) ==
                        formatTwoDecimal(ConsumerDashboardIndexPage._transactionAmount) &&
                getText(lstTransactionDetails.get(2)).split("\n")[0].equalsIgnoreCase(ConsumerLoginVerification._username) &&
                getText(lstTransactionDetails.get(2)).split("\n")[1].equalsIgnoreCase(ConsumerLoginVerification._usernumber);
    }

    public boolean verifyConsumerTransactionDetailsPopUp() {

        pause(3);

        for (WebElement e : lstTransactionDetails) System.out.println(getText(e));

        System.out.println(getText(lblTransactionIDPopup));
        System.out.println(ConsumerDashboardIndexPage._transactionID);
        System.out.println(ConsumerDashboardIndexPage._transactionDate + " / " + ConsumerDashboardIndexPage._transactionTime);
        System.out.println(ConsumerDashboardIndexPage._transactionAmount);
        System.out.println(ConsumerLoginVerification._username);
        System.out.println(ConsumerLoginVerification._usernumber);
        System.out.println(ConsumerDashboardIndexPage._transactionUsername);
        System.out.println(ConsumerDashboardIndexPage._transactionType);

        return getIntegerFromString(getText(lblTransactionIDPopup)) == (ConsumerDashboardIndexPage._transactionID) &&
                getText(lstTransactionDetails.get(0)).equalsIgnoreCase(ConsumerDashboardIndexPage._transactionDate
                        + " / " + ConsumerDashboardIndexPage._transactionTime) &&
                getDoubleFromString(getText(lstTransactionDetails.get(1))) == ConsumerDashboardIndexPage._transactionAmount &&
                getText(lstTransactionDetails.get(2)).split("\n")[0].equalsIgnoreCase(ConsumerLoginVerification._username) &&
                getText(lstTransactionDetails.get(2)).split("\n")[1].equalsIgnoreCase(ConsumerLoginVerification._usernumber) &&
                getText(lstTransactionDetails.get(3)).equalsIgnoreCase(ConsumerDashboardIndexPage._transactionType);
    }

    public boolean verifyBalanceUpdatedInBalanceScreen() {

        pause(3);

        return getDoubleFromString(getInnerText(lblBalance)) ==
                getDoubleFromString(ConsumerDashboardIndexPage._currentBalance) -
                        ConsumerWithdrawMoneyIndexPage._withDrawTotalAmount;

    }

    public boolean verifyInvalidCurrentPasswordValidation() {

        testValidationLog(getText(lblInvalidCurrentPasswordValidation));

        boolean bool = isElementDisplay(lblInvalidCurrentPasswordValidation) &&
                getText(lblInvalidCurrentPasswordValidation).equalsIgnoreCase(FIRST_UNSUCCESSFUL_ATTEMPTS) &&
                isElementDisplay(btnConfirmOK);

        clickOn(driver, btnConfirmOK);

        return bool;
    }

    public boolean verifyInvalidCurrentPasswordSecondTime() {

        testValidationLog(getText(lblInvalidCurrentPasswordValidation));

        boolean bool = isElementDisplay(lblInvalidCurrentPasswordValidation) &&
                getText(lblInvalidCurrentPasswordValidation).equalsIgnoreCase(SECOND_UNSUCCESSFUL_ATTEMPTS) &&
                isElementDisplay(btnConfirmOK);

        clickOn(driver, btnConfirmOK);

        return bool;
    }

    public boolean verifyAccountIsBlockedForInvalidPassword() {

        testValidationLog(getText(lblInvalidCurrentPasswordValidation));

        boolean bool = isElementDisplay(lblInvalidCurrentPasswordValidation) &&
                getText(lblInvalidCurrentPasswordValidation).equalsIgnoreCase(THIRD_UNSUCCESSFUL_ATTEMPTS) &&
                isElementDisplay(btnConfirmOK);

        clickOn(driver, btnConfirmOK);

        return bool;
    }

    public boolean verifyInvalidNewPasswordValidation() {

        testValidationLog(getText(lblInvalidNewPasswordValidation));

        return isElementDisplay(lblInvalidNewPasswordValidation) &&
                getText(lblInvalidNewPasswordValidation).equalsIgnoreCase(INVALID_NEW_PASSWORD);
    }

    public boolean verifyInvalidConfirmPasswordValidation() {

        testValidationLog(getText(lblInvalidConfirmPasswordValidation));

        return isElementDisplay(lblInvalidConfirmPasswordValidation) &&
                getText(lblInvalidConfirmPasswordValidation).equalsIgnoreCase(NEW_CONFIRM_PASSWORD_NOT_MATCHED);
    }

    public boolean verifyPastPasswordValidation() {

        testValidationLog(getText(lblPastPasswordValidation));

        boolean bool = isElementDisplay(lblPastPasswordValidation) &&
                getText(lblPastPasswordValidation).equalsIgnoreCase(PAST_PASSWORD_MESSAGE) &&
                isElementDisplay(btnConfirmOK);

        clickOn(driver, btnConfirmOK);

        return bool;

    }

    public boolean verifyPasswordChangeSuccessfully() {

        testConfirmationLog(getText(lblConfirmPasswordChange));

        boolean bool = isElementDisplay(lblConfirmPasswordChange) &&
                getText(lblConfirmPasswordChange).equalsIgnoreCase(SUCCESSFUL_PASSWORD_CHANGE_MESSAGE) &&
                isElementDisplay(btnConfirmOK);

        clickOn(driver, btnConfirmOK);

        return bool;

    }
}