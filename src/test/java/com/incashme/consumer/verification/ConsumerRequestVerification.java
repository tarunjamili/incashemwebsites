package com.incashme.consumer.verification;

import com.framework.init.AbstractPage;
import com.incashme.consumer.indexpage.ConsumerRequestIndexPage;
import com.incashme.consumer.validations.RequestValidations;
import com.incashme.merchant.indexpage.MerchantDonationIndexPage;
import com.incashme.merchant.indexpage.MerchantInvoiceIndexPage;
import com.incashme.merchant.verification.MerchantLoginVerification;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class ConsumerRequestVerification extends AbstractPage implements RequestValidations {

    public ConsumerRequestVerification(WebDriver driver) {
        super(driver);
    }

    @FindAll(value = {@FindBy(xpath = "//ul//li[contains(@class,'req')]//div[contains(@class,'dntn-id')]")})
    private List<WebElement> lstRequestIDCard;

    @FindAll(value = {@FindBy(xpath = "//ul//li[contains(@class,'req')]//div[contains(@class,'dntn-name')]")})
    private List<WebElement> lstRequesteeName;

    @FindAll(value = {@FindBy(xpath = "//ul//li[contains(@class,'req')]//div[contains(@class,'dntr-id')]")})
    private List<WebElement> lstRequesterID;

    @FindAll(value = {@FindBy(xpath = "//ul//li[contains(@class,'req')]//div[contains(@class,'dntn-amnt')]")})
    private List<WebElement> lstRequestAmount;

    @FindBy(xpath = "//div//ul//li//span[contains(@class,'ic-logout')]")
    private WebElement btnLogout;

    @FindBy(xpath = "//div//div[contains(@class,'donater-name')]")
    private WebElement lblInvoiceRequestorName;

    @FindBy(xpath = "//div//div[contains(@class,'donater-id')]")
    private WebElement lblInvoiceRequestorID;

    @FindBy(xpath = "//div//div[contains(@class,'donater-no')]")
    private WebElement lblInvoiceRequestorMobile;

    @FindBy(xpath = "//div//div[contains(@class,'donater-inv-id')]")
    private WebElement lblInvoiceID;

    @FindBy(xpath = "//button[contains(@class,'btn-green') and text()='Pay']")
    private WebElement btnPayInvoice;

    @FindBy(xpath = "//button[contains(@class,'btn-green') and text()='Confirm']")
    private WebElement btnConfirmPayInvoice;

    @FindBy(xpath = "//div//div[contains(@class,'msg-text')]")
    private WebElement lblInvoiceMessage;

    @FindBy(xpath = "//div[contains(@class,'modal-dialog')]//div[contains(@class,'amnt-txt')]")
    private WebElement lblInvoiceCardInvoiceAmount;

    @FindBy(xpath = "//div[contains(text(),'uccessfully')]//parent::div//parent::div//div[contains(@class,'amnt-txt')]")
    private WebElement lblInvoicePayAmount;

    @FindBy(xpath = "//div[contains(@class,'modal-dialog')]//div[contains(text(),'uccessfully')]")
    private WebElement lblInvoicePaySuccess;

    @FindBy(xpath = "//button[contains(@class,'btn-green') and text()='Donate']")
    private WebElement btnPayDonation;

    @FindBy(xpath = ".//div[contains(@id,'swal2-content') and contains(text(),'successfully ')]")
    private WebElement lblRequestCancelSuccess;

    @FindBy(xpath = ".//div[contains(@class,'swal2-actions')]//button[text()='OK' or text() = 'Ok']")
    private WebElement btnConfirmOK;

    @FindAll(value = {@FindBy(xpath = "//ul/li[contains(@class,'req') and contains(@id,'id')]")})
    private List<WebElement> lstRequestsCard;

    @FindBy(xpath = "//div//h2//strong[contains(text(),'No Request')]")
    private WebElement lblNoRequest;

    public boolean verifyRequestScreen() {
        pause(5);

        if (isListEmpty(lstRequestsCard)) {
            testValidationLog(getText(lblNoRequest));
            return isElementDisplay(lblNoRequest);
        } else {
            testInfoLog("Total Requests", String.valueOf(sizeOf(lstRequestsCard)));
            return sizeOf(lstRequestsCard) > 0;
        }
    }

    public boolean verifyInvoiceDetailsInScreen() {

        pause(2);

        System.out.println(getInnerText(lblInvoiceRequestorName));
        System.out.println(MerchantLoginVerification._username);

        System.out.println(getLongFromString(getInnerText(lblInvoiceID)));
        System.out.println(getLongFromString(MerchantInvoiceIndexPage._invoiceID));

        boolean bool = getInnerText(lblInvoiceRequestorName).equalsIgnoreCase(MerchantLoginVerification._username) &&
                getIntegerFromString(getInnerText(lblInvoiceID)) ==
                        getLongFromString(MerchantInvoiceIndexPage._invoiceID);

        if (!MerchantInvoiceIndexPage._invoiceMessage.isEmpty()) {
            bool = bool && getText(lblInvoiceMessage).equalsIgnoreCase(MerchantInvoiceIndexPage._invoiceMessage);
        } else {
            bool = bool && !isElementPresent(lblInvoiceMessage);
        }

        return bool;
    }

    public boolean verifyInvoiceScreenDisplay() {

        pause(2);

        System.out.println(getInnerText(lblInvoiceRequestorName));
        System.out.println(ConsumerRequestIndexPage._invoiceUserName);
        System.out.println(getInnerText(lblInvoiceID));
        System.out.println(ConsumerRequestIndexPage._invoiceID);
        System.out.println(getInnerText(lblInvoiceRequestorID));
        System.out.println(ConsumerRequestIndexPage._invoiceUserID);
        System.out.println(getInnerText(lblInvoiceRequestorMobile));
        System.out.println(ConsumerRequestIndexPage._invoiceUserNumber);

        return getInnerText(lblInvoiceRequestorName).equalsIgnoreCase(ConsumerRequestIndexPage._invoiceUserName) &&
                getInnerText(lblInvoiceID).equalsIgnoreCase(ConsumerRequestIndexPage._invoiceID) &&
                getIntegerFromString(getInnerText(lblInvoiceRequestorID)) ==
                        getIntegerFromString(ConsumerRequestIndexPage._invoiceUserID) &&
                getLongFromString(getInnerText(lblInvoiceRequestorMobile)) ==
                        getLongFromString(ConsumerRequestIndexPage._invoiceUserNumber);
    }

    public boolean verifyDonationScreenDisplay() {

        pause(2);

        System.out.println(getInnerText(lblInvoiceRequestorName));
        System.out.println(ConsumerRequestIndexPage._donationUserName);
        System.out.println(getInnerText(lblInvoiceID));
        System.out.println(ConsumerRequestIndexPage._donationID);
        System.out.println(getInnerText(lblInvoiceRequestorID));
        System.out.println(ConsumerRequestIndexPage._donationUserID);
        System.out.println(getInnerText(lblInvoiceRequestorMobile));
        System.out.println(ConsumerRequestIndexPage._donationUserNumber);

        return getInnerText(lblInvoiceRequestorName).equalsIgnoreCase(ConsumerRequestIndexPage._donationUserName) &&
                getInnerText(lblInvoiceID).equalsIgnoreCase(ConsumerRequestIndexPage._donationID) &&
                getIntegerFromString(getInnerText(lblInvoiceRequestorID)) ==
                        getIntegerFromString(ConsumerRequestIndexPage._donationUserID) &&
                getLongFromString(getInnerText(lblInvoiceRequestorMobile)) ==
                        getLongFromString(ConsumerRequestIndexPage._donationUserNumber);
    }

    public boolean verifyDonationDetailsInScreen() {

        pause(2);

        System.out.println(getInnerText(lblInvoiceRequestorName));
        System.out.println(MerchantLoginVerification._username);

        System.out.println(getLongFromString(getInnerText(lblInvoiceID)));
        System.out.println(getLongFromString(MerchantDonationIndexPage._donationID));

        boolean bool = getInnerText(lblInvoiceRequestorName).equalsIgnoreCase(MerchantLoginVerification._username) &&
                getLongFromString(getInnerText(lblInvoiceID)) ==
                        getLongFromString(MerchantDonationIndexPage._donationID);

        if (!MerchantDonationIndexPage._donationMessage.isEmpty()) {
            bool = bool && getText(lblInvoiceMessage).equalsIgnoreCase(MerchantDonationIndexPage._donationMessage);
        } else {
            bool = bool && !isElementPresent(lblInvoiceMessage);
        }

        return bool;
    }

    public boolean verifyInvoiceDetailsInCard() {

        mouseHoverTo(driver, btnLogout);

        System.out.println(getIntegerFromString(getInnerText(lstRequestIDCard.get(0))));
        System.out.println(getIntegerFromString(MerchantInvoiceIndexPage._invoiceID));

        System.out.println(getInnerText(lstRequesteeName.get(0)));
        System.out.println(MerchantLoginVerification._username);

        System.out.println(getDoubleFromString(getInnerText(lstRequestAmount.get(0))));
        System.out.println(MerchantInvoiceIndexPage._invoiceAmount + "");

        return getIntegerFromString(getInnerText(lstRequestIDCard.get(0))) ==
                getIntegerFromString(MerchantInvoiceIndexPage._invoiceID) &&
                getInnerText(lstRequesteeName.get(0)).equalsIgnoreCase(MerchantLoginVerification._username) &&
                getDoubleFromString(getInnerText(lstRequestAmount.get(0))) == MerchantInvoiceIndexPage._invoiceAmount;
    }

    public boolean verifyDonationDetailsInCard() {

        mouseHoverTo(driver, btnLogout);

        System.out.println(getIntegerFromString(getInnerText(lstRequestIDCard.get(0))));
        System.out.println(getIntegerFromString(MerchantDonationIndexPage._donationID));

        System.out.println(getInnerText(lstRequesteeName.get(0)));
        System.out.println(MerchantLoginVerification._username);


        return getIntegerFromString(getInnerText(lstRequestIDCard.get(0))) ==
                getIntegerFromString(MerchantDonationIndexPage._donationID) &&
                getInnerText(lstRequesteeName.get(0)).equalsIgnoreCase(MerchantLoginVerification._username);
    }

    public boolean verifyPayInvoiceConfirmationDetails() {

        pause(3);

        System.out.println(formatTwoDecimal(getDoubleFromString(getText(lblInvoiceCardInvoiceAmount))));
        System.out.println(MerchantInvoiceIndexPage._invoiceAmount);
        System.out.println(ConsumerRequestIndexPage._invoiceAmount);

        return (formatTwoDecimal(getDoubleFromString(getText(lblInvoiceCardInvoiceAmount))) ==
                MerchantInvoiceIndexPage._invoiceAmount) ||
                (formatTwoDecimal(getDoubleFromString(getText(lblInvoiceCardInvoiceAmount))) ==
                        getDoubleFromString(ConsumerRequestIndexPage._invoiceAmount));

    }

    public boolean verifyPayDonationConfirmationDetails() {

        pause(3);

        System.out.println(formatTwoDecimal(getDoubleFromString(getText(lblInvoiceCardInvoiceAmount))));
        System.out.println(ConsumerRequestIndexPage._donationAmount);

        return formatTwoDecimal(getDoubleFromString(getText(lblInvoiceCardInvoiceAmount))) ==
                ConsumerRequestIndexPage._donationAmount;

    }

    public boolean verifyInvoicePayCompletedPopUp() {

        pause(3);

        System.out.println(formatTwoDecimal(getDoubleFromString(getText(lblInvoicePayAmount))));
        System.out.println(MerchantInvoiceIndexPage._invoiceAmount);
        System.out.println(ConsumerRequestIndexPage._invoiceAmount);

        boolean bool = (formatTwoDecimal(getDoubleFromString(getText(lblInvoicePayAmount))) ==
                MerchantInvoiceIndexPage._invoiceAmount) ||
                (formatTwoDecimal(getDoubleFromString(getText(lblInvoicePayAmount))) ==
                        getDoubleFromString(ConsumerRequestIndexPage._invoiceAmount));

        testConfirmationLog(getText(lblInvoicePaySuccess));

        return bool;
    }

    public boolean verifyDonationPayCompletedPopUp() {

        pause(3);

        System.out.println(formatTwoDecimal(getDoubleFromString(getText(lblInvoicePayAmount))));
        System.out.println(ConsumerRequestIndexPage._donationAmount);

        boolean bool = (formatTwoDecimal(getDoubleFromString(getText(lblInvoicePayAmount))) ==
                ConsumerRequestIndexPage._donationAmount);

        testConfirmationLog(getText(lblInvoicePaySuccess));

        MerchantDonationIndexPage._donationAmount = ConsumerRequestIndexPage._donationAmount;

        return bool;
    }

    public boolean verifyInvalidAmountValidation() {
        return btnPayDonation.getAttribute("disabled").equalsIgnoreCase("true");
    }

    public boolean verifyInvoiceCancelled() {

        boolean bool = getText(lblRequestCancelSuccess).equalsIgnoreCase(INVOICE_REJECTED_SUCCESSFULLY);

        testConfirmationLog(getText(lblRequestCancelSuccess));
        clickOn(driver, btnConfirmOK);

        return bool;

    }

    public boolean verifyDonationCancelled() {

        boolean bool = getText(lblRequestCancelSuccess).equalsIgnoreCase(DONATION_REJECTED_SUCCESSFULLY);

        testConfirmationLog(getText(lblRequestCancelSuccess));
        clickOn(driver, btnConfirmOK);

        return bool;

    }

    public boolean verifyCancelledInvoiceInCards() {
        boolean bool = false;

        for (WebElement card : lstRequestIDCard) {
            if (getIntegerFromString(getInnerText(card)) == getIntegerFromString(ConsumerRequestIndexPage._invoiceID)) {
                bool = false;
                break;
            } else {
                bool = true;
            }
        }

        return bool;
    }

    public boolean verifyCancelledDonationInCards() {
        boolean bool = false;

        for (WebElement card : lstRequestIDCard) {
            if (getIntegerFromString(getInnerText(card)) == getIntegerFromString(ConsumerRequestIndexPage._donationID)) {
                bool = false;
                break;
            } else {
                bool = true;
            }
        }

        return bool;
    }
}
