package com.incashme.consumer.verification;

import com.framework.init.AbstractPage;
import com.incashme.consumer.indexpage.ConsumerAddMoneyIndexPage;
import com.incashme.consumer.validations.AddMoneyValidations;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ConsumerAddMoneyVerification extends AbstractPage implements AddMoneyValidations {

    public ConsumerAddMoneyVerification(WebDriver driver) {
        super(driver);
    }

    @FindAll(value = {@FindBy(xpath = "//ul//li[contains(@class,'dcc-items') and contains(@id,'ms')]")})
    private List<WebElement> lstDebitCreditCard;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@class,'no_card_title')]")})
    private List<WebElement> lstNoCardDisplay;

    @FindBy(xpath = "//h5[contains(text(),'Debit')]//following-sibling::div//a")
    private WebElement btnAddDebitCard;

    @FindBy(xpath = "//h5[contains(text(),'Credit')]//following-sibling::div//a")
    private WebElement btnAddCreditCard;

    @FindBy(xpath = "//input[@id='cardname']")
    private WebElement txtDebitCardHolderName;

    @FindBy(xpath = "//input[@id='cardnum']")
    private WebElement txtDebitCardNumber;

    @FindBy(xpath = "//input[@id='cardexpiry']")
    private WebElement txtDebitExpiryDate;

    @FindBy(xpath = "//button[contains(@class,'btn-green') and text()='Save']")
    private WebElement btnSaveCard;

    @FindBy(xpath = "//button[contains(@class,'btn-pink') and text()='Close']")
    private WebElement btnCloseForm;

    @FindBy(xpath = ".//div[contains(@id,'swal2-content')]")
    private WebElement lblInvalidCardDetails;

    @FindBy(xpath = ".//div[contains(@class,'swal2-actions')]//button[text()='OK' or text() = 'Ok']")
    private WebElement btnConfirmOK;

    @FindBy(xpath = "//h5[contains(text(),'Debit')]//parent::div//following-sibling::ul//li[not(contains(@class,'no_card'))]//h4")
    private WebElement lblAddedDebitCardHolderName;

    @FindBy(xpath = "//h5[contains(text(),'Debit')]//parent::div//following-sibling::ul//li[not(contains(@class,'no_card'))]//span")
    private WebElement lblAddedDebitCardNumber;

    @FindBy(xpath = "//h5[contains(text(),'Credit')]//parent::div//following-sibling::ul//li[not(contains(@class,'no_card'))]//h4")
    private WebElement lblAddedCreditCardHolderName;

    @FindBy(xpath = "//h5[contains(text(),'Credit')]//parent::div//following-sibling::ul//li[not(contains(@class,'no_card'))]//span")
    private WebElement lblAddedCreditCardNumber;

    @FindBy(xpath = "//div[contains(@class,'card_money_main')]//div[contains(@class,'m_card_dt_name')]")
    private WebElement lblCardHolderName;

    @FindBy(xpath = "//div[contains(@class,'card_money_main')]//div[contains(@class,'m_card_dt_number')]")
    private WebElement lblCardNumber;

    @FindAll(value = {@FindBy(xpath = "//h5[contains(text(),'Debit')]//parent::div//following-sibling::ul//li[not(contains(@class,'no_card'))]")})
    public List<WebElement> lstDebitCards;

    @FindAll(value = {@FindBy(xpath = "//h5[contains(text(),'Credit')]//parent::div//following-sibling::ul//li[not(contains(@class,'no_card'))]")})
    public List<WebElement> lstCreditCards;

    @FindBy(xpath = "//button[contains(@class,'btn-green') and text()='Pay']")
    private WebElement btnPayAddMoney;

    @FindBy(xpath = "//button[contains(@class,'btn-green') and text()='Proceed']")
    private WebElement btnProceedNetBanking;

    @FindBy(xpath = "//div[contains(@role,'document')]//ul//li//div")
    private WebElement lblAddedAmount;

    @FindBy(xpath = "//button[contains(@class,'btn-success') and text()='Close']")
    private WebElement btnCloseSuccessPopUp;

    @FindBy(xpath = "//div[contains(@class,'blnc-txt')]")
    private WebElement lblBalance;

    @FindBy(xpath = "//input[@type='search']")
    private WebElement txtInputSearch;

    @FindAll(value = {@FindBy(xpath = "//table[contains(@id,'banksListTable')]//td//h5")})
    private List<WebElement> lstBankName;

    @FindBy(xpath = "//table[@id='banksListTable']//td")
    private WebElement lblNoRecordsFound;

    @FindBy(xpath = "//div[contains(@class,'card_money_main')]//div[contains(@class,'m_card_dt_title')]//following-sibling::div")
    private WebElement lblSelectedBankName;


    public boolean verifyAddMoneyScreen() {
        pause(5);

        if (isListEmpty(lstDebitCreditCard)) {
            testValidationLog(getText(lstNoCardDisplay.get(0)));
            testValidationLog(getText(lstNoCardDisplay.get(1)));

            return isElementPresent(lstNoCardDisplay.get(0)) || isElementPresent(lstNoCardDisplay.get(1));
        } else {
            testInfoLog("Total Cards", String.valueOf(sizeOf(lstDebitCreditCard)));
            return sizeOf(lstDebitCreditCard) > 0;
        }
    }

    public boolean verifyAddDebitCardButton() {
        return isElementDisplay(btnAddDebitCard);
    }

    public boolean verifyAddCreditCardButton() {
        return isElementDisplay(btnAddCreditCard);
    }

    public boolean verifyAddDebitCardButtonNotAvailable() {
        return !isElementPresent(btnAddDebitCard);
    }

    public boolean verifyAddCreditCardButtonNotAvailable() {
        return !isElementPresent(btnAddCreditCard);
    }

    public boolean verifyAddDebitCardForm() {

        boolean bool = isElementPresent(txtDebitCardHolderName) && isElementPresent(txtDebitCardNumber) &&
                isElementPresent(txtDebitExpiryDate);

        scrollToElement(driver, btnSaveCard);

        return bool && isElementPresent(btnSaveCard) && isElementPresent(btnCloseForm);
    }

    public boolean verifyAddCreditCardForm() {

        boolean bool = isElementPresent(txtDebitCardHolderName) && isElementPresent(txtDebitCardNumber) &&
                isElementPresent(txtDebitExpiryDate);

        scrollToElement(driver, btnSaveCard);

        return bool && isElementPresent(btnSaveCard) && isElementPresent(btnCloseForm);
    }

    public boolean verifyValidationMessageForInvalidCardDetails() {
        boolean bool = getText(lblInvalidCardDetails).equalsIgnoreCase(BLANK_CARD_NAME);

        testValidationLog(getText(lblInvalidCardDetails));
        clickOn(driver, btnConfirmOK);

        return bool;
    }

    public boolean verifyValidationMessageForBlankDebitCardDetails() {
        boolean bool = getText(lblInvalidCardDetails).contains(INVALID_CARD_NAME + " " + INVALID_DEBIT_CARD_NUMBER + " " +
                INVALID_DEBIT_CARD_EXP_MONTH + " " + INVALID_DEBIT_CARD_EXP_YEAR);

        testValidationLog(getText(lblInvalidCardDetails));
        clickOn(driver, btnConfirmOK);

        return bool;
    }

    public boolean verifyValidationMessageForBlankCreditCardDetails() {
        boolean bool = getText(lblInvalidCardDetails).contains(INVALID_CARD_NAME + " " + INVALID_CREDIT_CARD_NUMBER + " " +
                INVALID_CREDIT_CARD_EXP_MONTH + " " + INVALID_CREDIT_CARD_EXP_YEAR);

        testValidationLog(getText(lblInvalidCardDetails));
        clickOn(driver, btnConfirmOK);

        return bool;
    }

    public boolean verifyCardAddedConfirmationMessage() {
        boolean bool = getText(lblInvalidCardDetails).equalsIgnoreCase(CARD_ADDED_SUCCESSFULLY);

        testValidationLog(getText(lblInvalidCardDetails));
        clickOn(driver, btnConfirmOK);

        return bool;
    }

    public boolean verifyCardDeletedConfirmationMessage() {

        pause(3);

        boolean bool = getText(lblInvalidCardDetails).equalsIgnoreCase(CARD_DELETED_SUCCESSFULLY);

        testValidationLog(getText(lblInvalidCardDetails));
        clickOn(driver, btnConfirmOK);

        return bool;
    }

    public boolean verifyAddedDebitCardDetailsInCard() {
        return getInnerText(lblAddedDebitCardHolderName).equalsIgnoreCase(ConsumerAddMoneyIndexPage._CardHolder) &&
                getInnerText(lblAddedDebitCardNumber).equalsIgnoreCase(ConsumerAddMoneyIndexPage._CardNumber);
    }

    public boolean verifyAddedCreditCardDetailsInCard() {
        return getInnerText(lblAddedCreditCardHolderName).equalsIgnoreCase(ConsumerAddMoneyIndexPage._CardHolder) &&
                getInnerText(lblAddedCreditCardNumber).equalsIgnoreCase(ConsumerAddMoneyIndexPage._CardNumber);
    }

    public boolean verifyDebitCardDetailsScreen() {
        return getInnerText(lblCardHolderName).equalsIgnoreCase(ConsumerAddMoneyIndexPage._CardHolder) &&
                getInnerText(lblCardNumber).equalsIgnoreCase(ConsumerAddMoneyIndexPage._CardNumber);
    }

    public boolean verifyCreditCardDetailsScreen() {
        return getInnerText(lblCardHolderName).equalsIgnoreCase(ConsumerAddMoneyIndexPage._CardHolder) &&
                getInnerText(lblCardNumber).equalsIgnoreCase(ConsumerAddMoneyIndexPage._CardNumber);
    }

    public boolean verifyDebitCardDeletedSuccessfully() {
        return sizeOf(lstDebitCards) == ConsumerAddMoneyIndexPage._totalDebitCard - 1;
    }

    public boolean verifyCreditCardDeletedSuccessfully() {
        return sizeOf(lstCreditCards) == ConsumerAddMoneyIndexPage._totalCreditCard - 1;
    }

    public boolean verifyTestDebitCardDetailsScreen() {
        return getInnerText(lblCardHolderName).equalsIgnoreCase(TEST_CARD_NAME) &&
                getInnerText(lblCardNumber).equalsIgnoreCase(TEST_CARD_NUMBER);
    }

    public boolean verifyTestCreditCardDetailsScreen() {
        return getInnerText(lblCardHolderName).equalsIgnoreCase(TEST_CARD_NAME) &&
                getInnerText(lblCardNumber).equalsIgnoreCase(TEST_CARD_NUMBER);
    }

    public boolean verifyPayButtonDisabled() {
        return btnPayAddMoney.getAttribute("disabled").equalsIgnoreCase("true");
    }

    public boolean verifyProceedButtonDisabled() {
        return btnProceedNetBanking.getAttribute("disabled").equalsIgnoreCase("true");
    }

    public boolean verifySuccessAddMoney() {
        wait = new WebDriverWait(driver, 90);
        wait.until(ExpectedConditions.visibilityOfAllElements(btnCloseSuccessPopUp));

        System.out.println(isElementDisplay(btnCloseSuccessPopUp));
        System.out.println(getDoubleFromString(getText(lblAddedAmount)));
        System.out.println(ConsumerAddMoneyIndexPage._transactionAmount);

        boolean bool = isElementDisplay(btnCloseSuccessPopUp) &&
                getDoubleFromString(getText(lblAddedAmount)) == ConsumerAddMoneyIndexPage._transactionAmount;

        testStepsLog(_logStep++, "Click on Close button");
        clickOn(driver, btnCloseSuccessPopUp);

        return bool;
    }

    public boolean verifyBalanceUpdated() {

        pause(3);
        double updatedAmount = getDoubleFromString(getInnerText(lblBalance));

        System.out.println(updatedAmount);
        System.out.println(ConsumerAddMoneyIndexPage._transactionAmount);
        System.out.println(ConsumerAddMoneyIndexPage._currentAccountBalance);

        return formatTwoDecimal(ConsumerAddMoneyIndexPage._currentAccountBalance +
                ConsumerAddMoneyIndexPage._transactionAmount) == formatTwoDecimal(updatedAmount);

    }

    public boolean verifySelectBankPopUpDisplay() {
        pause(2);
        updateShowList(driver, findElementByName(driver, "banksListTable_length"), "All");
        return isElementDisplay(txtInputSearch);
    }

    public boolean verifyBankNameSorted() {

        List<String> bankNameAfterSort = new ArrayList<>();
        List<String> bankNameBeforeSort = ConsumerAddMoneyIndexPage._bankNamesBeforeSort;

        for (WebElement name : lstBankName) {
            bankNameAfterSort.add(getInnerText(name));
        }

        Collections.sort(bankNameBeforeSort, Collections.reverseOrder());

        System.out.println(bankNameBeforeSort.equals(bankNameAfterSort));

        return !bankNameBeforeSort.equals(bankNameAfterSort);
    }

    public boolean verifyTableSearchSuccessful() {

        boolean bool = getInnerText(lstBankName.get(0)).contains(ConsumerAddMoneyIndexPage._searchCriteria);

        type(txtInputSearch, "");

        return bool;

    }

    public boolean verifyTableValidationForInvalidSearch() {

        testValidationLog(getInnerText(lblNoRecordsFound));

        boolean bool = getInnerText(lblNoRecordsFound).equalsIgnoreCase(NO_SEARCH_RECORD_FOUND);

        type(txtInputSearch, "x");
        txtInputSearch.clear();

        return bool;
    }

    public boolean verifySelectedBankInScreen() {
        return getInnerText(lblSelectedBankName).equalsIgnoreCase(ConsumerAddMoneyIndexPage._bankName);
    }

    public boolean verifyNetBankingScreenNotDisplay() {
        return !isElementPresent(lblSelectedBankName);
    }

    public boolean verifyUserRedirectsToNetBanking() {
        pause(15);
        return !driver.getCurrentUrl().contains(CONSUMER_URL);
    }
}
