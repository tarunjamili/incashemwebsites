package com.incashme.consumer.verification;

import com.framework.init.AbstractPage;
import com.incashme.consumer.indexpage.ConsumerTransactionHistoryIndexPage;
import com.incashme.consumer.validations.TransactionHistoryValidations;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class ConsumerTransactionHistoryVerification extends AbstractPage implements TransactionHistoryValidations {

    public ConsumerTransactionHistoryVerification(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//ul/li//button[@id='li_']")
    private WebElement btnAllFilterTransaction;

    @FindBy(xpath = "//ul/li//button[@id='li_C']")
    private WebElement btnIndividualFilterTransaction;

    @FindBy(xpath = "//ul/li//button[@id='li_M']")
    private WebElement btnMerchantFilterTransaction;

    @FindBy(xpath = "//ul/li//button[@id='li_A']")
    private WebElement btnAgentFilterTransaction;

    @FindBy(xpath = "//ul/li//button[@id='li_F']")
    private WebElement btnFilterTransaction;

    @FindAll(value = {@FindBy(xpath = "//table[@id='transactionHistoryTbl']//tr//td[1]//h5")})
    private List<WebElement> lstUserName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='transactionHistoryTbl']//tr//td[1]//h5//following-sibling::div")})
    private List<WebElement> lstDashboardUserNumberID;

    @FindAll(value = {@FindBy(xpath = "//table[@id='transactionHistoryTbl']//tr//td[2]")})
    private List<WebElement> lstDashboardTxID;

    @FindAll(value = {@FindBy(xpath = "//table[@id='transactionHistoryTbl']//tr//td[3]")})
    private List<WebElement> lstDashboardUserCategory;

    @FindAll(value = {@FindBy(xpath = "//table[@id='transactionHistoryTbl']//tr//td[4]")})
    private List<WebElement> lstDashboardTxAmount;

    @FindAll(value = {@FindBy(xpath = "//table[@id='transactionHistoryTbl']//tr//td[5]")})
    private List<WebElement> lstDashboardUserType;

    @FindAll(value = {@FindBy(xpath = "//table[@id='transactionHistoryTbl']//tr//td[6]")})
    private List<WebElement> lstDashboardTransactionTime;

    @FindBy(xpath = "//table[@id='transactionHistoryTbl']//td")
    private WebElement lblNoRecordsFound;

    @FindAll(value = {@FindBy(xpath = "//table[@id='transactionHistoryTbl']//tr//td[7]//button")})
    private List<WebElement> lstBtnMore;

    @FindBy(xpath = "//input[@type='search']")
    private WebElement txtInputSearch;

    @FindBy(xpath = "//input[@formcontrolname='min_amount']")
    private WebElement txtFilterMinAmount;

    @FindBy(xpath = "//input[@formcontrolname='max_amount']")
    private WebElement txtFilterMaxAmount;

    @FindBy(xpath = "//mdb-select[@formcontrolname='usertype']")
    private WebElement btnCategoryDropdown;

    @FindAll(value = {@FindBy(xpath = "//mdb-select[@formcontrolname='usertype']//li/span")})
    private List<WebElement> lstFilterCategory;

    @FindBy(xpath = "//div[contains(@class,'filter-body')]//button[@type='submit']")
    private WebElement btnFilterSubmit;

    @FindBy(xpath = "//div[contains(@class,'filter-body')]//button[text()='Clear all']")
    private WebElement btnFilterClearAll;

    @FindBy(xpath = "//div//h2//strong[contains(text(),'No Transaction')]")
    private WebElement lblNoTransaction;

    @FindBy(xpath = "//h5[contains(text(),'Transaction Detail')]//following-sibling::div")
    private WebElement lblTransactionIDPopup;

    @FindAll(value = {@FindBy(xpath = "//*[contains(text(),'Transaction Detail')]/../..//following-sibling::div//li//div[contains(@class,'text-right')]")})
    private List<WebElement> lstTransactionDetails;

    @FindBy(xpath = "//*[text()='Transaction Detail']/ancestor::div//button[contains(@class,'close')]")
    private WebElement btnCloseTxDetailsPopup;


    public boolean verifyTransactionHistoryScreen() {
        if (isListEmpty(lstUserName)) {
            testValidationLog(getText(lblNoTransaction));
            return isElementDisplay(lblNoTransaction);
        } else {
            return isElementDisplay(btnAllFilterTransaction) && isElementDisplay(btnIndividualFilterTransaction) &&
                    isElementDisplay(btnMerchantFilterTransaction) && isElementDisplay(btnAgentFilterTransaction) &&
                    isElementDisplay(btnFilterTransaction);
        }
    }

    public boolean verifyIndividualTransactionFilter() {

        if (isListEmpty(lstDashboardUserCategory)) {
            testValidationLog(getText(lblNoTransaction));
            return getText(lblNoTransaction).equalsIgnoreCase(NO_TRANSACTIONS_HISTORY);
        } else {

            List<String> lstCategories = new LinkedList<>();
            for (WebElement category : lstDashboardUserCategory) lstCategories.add(getInnerText(category));

            System.out.println(lstCategories.stream().distinct().count());
            return lstCategories.stream().distinct().count() <= 3 &&
                    sizeOf(lstCategories) == sizeOf(lstBtnMore) && (lstCategories.contains(CONSUMER_CATEGORY) ||
                    lstCategories.contains(INDIVIDUAL_CATEGORY) || lstCategories.contains(OWN_WALLET_CATEGORY));
        }
    }


    public boolean verifyMerchantTransactionFilter() {

        if (isListEmpty(lstDashboardUserCategory)) {
            testValidationLog(getText(lblNoTransaction));
            return getText(lblNoTransaction).equalsIgnoreCase(NO_TRANSACTIONS_HISTORY);
        } else {
            List<String> lstCategories = new LinkedList<>();
            for (WebElement category : lstDashboardUserCategory) lstCategories.add(getInnerText(category));

            System.out.println(lstCategories.stream().distinct().count());
            return lstCategories.stream().distinct().count() == 2 &&
                    lstCategories.contains(MERCHANT_CATEGORY) && lstCategories.contains(NON_PROFIT_CATEGORY)
                    && sizeOf(lstCategories) == sizeOf(lstBtnMore);
        }
    }

    public boolean verifyAgentTransactionFilter() {

        if (isListEmpty(lstDashboardUserCategory)) {
            testValidationLog(getText(lblNoTransaction));
            return getText(lblNoTransaction).equalsIgnoreCase(NO_TRANSACTIONS_HISTORY);
        } else {

            List<String> lstCategories = new LinkedList<>();
            for (WebElement category : lstDashboardUserCategory) lstCategories.add(getInnerText(category));

            System.out.println(lstCategories.stream().distinct().count());
            return lstCategories.stream().distinct().count() == 1 &&
                    lstCategories.contains(AGENT_CATEGORY) &&
                    sizeOf(lstCategories) == sizeOf(lstBtnMore);
        }
    }

    public boolean verifyAllTransactionFilter() {
        return sizeOf(lstUserName) == sizeOf(ConsumerTransactionHistoryIndexPage._allTransactionDetailsCategory);
    }

    public boolean verifyUserNameSorted() {

        List<String> userNameAfterSort = new ArrayList<>();
        List<String> userNameBeforeSort = ConsumerTransactionHistoryIndexPage._userNameBeforeSort;

        for (WebElement name : lstUserName) {
            userNameAfterSort.add(getInnerText(name));
        }

        userNameBeforeSort.sort(
                (String o1, String o2) -> {
                    if (o1.equalsIgnoreCase(o2)) {
                        return o2.compareTo(o1);
                    }
                    return o1.toLowerCase().compareTo(o2.toLowerCase());
                }
        );

        System.out.println(userNameBeforeSort.equals(userNameAfterSort));

        return userNameBeforeSort.equals(userNameAfterSort);
    }

    public boolean verifyUserTxIDSorted() {

        List<String> txIDAfterSort = new ArrayList<>();
        List<String> txIDBeforeSort = ConsumerTransactionHistoryIndexPage._userTxIDBeforeSort;

        for (WebElement name : lstDashboardTxID) {
            txIDAfterSort.add(getInnerText(name));
        }

        txIDBeforeSort.sort(
                (String o1, String o2) -> {
                    if (o1.equalsIgnoreCase(o2)) {
                        return o2.compareTo(o1);
                    }
                    return o1.toLowerCase().compareTo(o2.toLowerCase());
                }
        );

        System.out.println(txIDBeforeSort.equals(txIDAfterSort));

        return txIDBeforeSort.equals(txIDAfterSort);
    }

    public boolean verifyUserCategorySorted() {

        List<String> userCategoryAfterSort = new ArrayList<>();
        List<String> userCategoryBeforeSort = ConsumerTransactionHistoryIndexPage._userCategoryBeforeSort;

        for (WebElement name : lstDashboardUserCategory) {
            userCategoryAfterSort.add(getInnerText(name));
        }

        userCategoryBeforeSort.sort(
                (String o1, String o2) -> {
                    if (o1.equalsIgnoreCase(o2)) {
                        return o2.compareTo(o1);
                    }
                    return o1.toLowerCase().compareTo(o2.toLowerCase());
                }
        );

        System.out.println(userCategoryBeforeSort.equals(userCategoryAfterSort));

        return userCategoryBeforeSort.equals(userCategoryAfterSort);
    }

    public boolean verifyUserTypeSorted() {

        List<String> userTypeAfterSort = new ArrayList<>();
        List<String> userTypeBeforeSort = ConsumerTransactionHistoryIndexPage._txTypeBeforeSort;

        for (WebElement name : lstDashboardUserType) {
            userTypeAfterSort.add(getInnerText(name));
        }

        userTypeBeforeSort.sort(
                (String o1, String o2) -> {
                    if (o1.equalsIgnoreCase(o2)) {
                        return o2.compareTo(o1);
                    }
                    return o1.toLowerCase().compareTo(o2.toLowerCase());
                }
        );

        System.out.println(userTypeBeforeSort.equals(userTypeAfterSort));

        return userTypeBeforeSort.equals(userTypeAfterSort);
    }

    public boolean verifyTransactionDateTimeSorted() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        List<LocalDateTime> transactionDateTimeAfterSort = new ArrayList<>();
        List<LocalDateTime> transactionDateTimeBeforeSort = ConsumerTransactionHistoryIndexPage._transactionDateTimeBeforeSort;

        for (WebElement name : lstDashboardTransactionTime) {
            transactionDateTimeAfterSort.add(LocalDateTime.parse(getInnerText(name), format));
        }

        Collections.sort(transactionDateTimeBeforeSort);

        System.out.println(transactionDateTimeBeforeSort.equals(transactionDateTimeAfterSort));

        return transactionDateTimeBeforeSort.equals(transactionDateTimeAfterSort);
    }

    public boolean verifyTableSearchSuccessful() {

        boolean bool = getInnerText(lstDashboardTxID.get(0)).contains(ConsumerTransactionHistoryIndexPage._searchCriteria) &&
                sizeOf(lstDashboardTxID) == 1;

        type(txtInputSearch, "");

        return bool;

    }

    public boolean verifyTableValidationForInvalidSearch() {

        testValidationLog(getInnerText(lblNoRecordsFound));

        boolean bool = getInnerText(lblNoRecordsFound).equalsIgnoreCase(NO_SEARCH_RECORD_FOUND);

        type(txtInputSearch, "x");
        txtInputSearch.clear();

        return bool;
    }

    public boolean verifyFilterScreen() {
        return isElementDisplay(txtFilterMaxAmount) && isElementDisplay(txtFilterMaxAmount) &&
                isElementDisplay(btnCategoryDropdown) && isElementDisplay(btnFilterSubmit) &&
                isElementDisplay(btnFilterClearAll);
    }

    public boolean verifyNoTransactionFound() {
        return isElementDisplay(lblNoTransaction) && getText(lblNoTransaction).equalsIgnoreCase(NO_TRANSACTIONS_HISTORY);
    }

    public boolean verifyTransactionFilterByAmountRange() {

        boolean bool = true;

        if (isListEmpty(lstDashboardUserCategory)) {
            testValidationLog(getText(lblNoTransaction));
            bool = false;
        } else {

            pause(3);
            List<WebElement> lstDashboardTxAmount = driver.findElements(By.xpath("//table[@id='transactionHistoryTbl']//tr//td[4]"));

            for (WebElement amount : lstDashboardTxAmount) {
                if (Math.abs(getDoubleFromString(getText(amount))) >= ConsumerTransactionHistoryIndexPage._filterTransactionFromAmount &&
                        Math.abs(getDoubleFromString(getText(amount))) <= ConsumerTransactionHistoryIndexPage._filterTransactionToAmount) {
                    bool = true;
                } else {
                    bool = false;
                    break;
                }
            }
        }
        return bool;
    }

    public boolean verifyTransactionFilterByAgent() {

        boolean bool = true;

        if (isListEmpty(lstDashboardUserCategory)) {
            testValidationLog(getText(lblNoTransaction));
            bool = false;
        } else {
            updateShowList(driver, findElementByName(driver, "transactionHistoryTbl_length"), "All");

            pause(5);
            List<WebElement> lstDashboardTxAmount = driver.findElements(By.xpath("//table[@id='transactionHistoryTbl']//tr//td[4]"));

            for (WebElement amount : lstDashboardTxAmount) {
                if (Math.abs(getDoubleFromString(getText(amount))) >= ConsumerTransactionHistoryIndexPage._filterTransactionFromAmount &&
                        Math.abs(getDoubleFromString(getText(amount))) <= ConsumerTransactionHistoryIndexPage._filterTransactionToAmount) {
                    bool = true;
                } else {
                    System.out.println(Math.abs(getDoubleFromString(getText(amount))));
                    System.out.println(ConsumerTransactionHistoryIndexPage._filterTransactionFromAmount);
                    System.out.println(ConsumerTransactionHistoryIndexPage._filterTransactionToAmount);
                    bool = false;
                    break;
                }
            }

            List<String> lstCategories = new LinkedList<>();
            for (WebElement category : lstDashboardUserCategory) lstCategories.add(getInnerText(category));

            System.out.println(lstCategories);

            bool = bool && lstCategories.stream().distinct().count() == 1 &&
                    sizeOf(lstCategories) == sizeOf(lstBtnMore);
        }

        return bool;
    }

    public boolean verifyTransactionFilterByIndividual() {

        boolean bool = true;

        if (isListEmpty(lstDashboardUserCategory)) {
            testValidationLog(getText(lblNoTransaction));
            bool = false;
        } else {
            updateShowList(driver, findElementByName(driver, "transactionHistoryTbl_length"), "All");

            pause(5);
            List<WebElement> lstDashboardTxAmount = driver.findElements(By.xpath("//table[@id='transactionHistoryTbl']//tr//td[4]"));

            for (WebElement amount : lstDashboardTxAmount) {
                if (Math.abs(getDoubleFromString(getText(amount))) >= ConsumerTransactionHistoryIndexPage._filterTransactionFromAmount &&
                        Math.abs(getDoubleFromString(getText(amount))) <= ConsumerTransactionHistoryIndexPage._filterTransactionToAmount) {
                    bool = true;
                } else {
                    System.out.println(Math.abs(getDoubleFromString(getText(amount))));
                    System.out.println(ConsumerTransactionHistoryIndexPage._filterTransactionFromAmount);
                    System.out.println(ConsumerTransactionHistoryIndexPage._filterTransactionToAmount);
                    bool = false;
                    break;
                }
            }

            List<String> lstCategories = new LinkedList<>();
            for (WebElement category : lstDashboardUserCategory) lstCategories.add(getInnerText(category));

            System.out.println(lstCategories);

            bool = bool && lstCategories.stream().distinct().count() <= 3 &&
                    sizeOf(lstCategories) == sizeOf(lstBtnMore);
        }

        return bool;
    }

    public boolean verifyTransactionFilterByMerchant() {

        boolean bool = true;

        if (isListEmpty(lstDashboardUserCategory)) {
            testValidationLog(getText(lblNoTransaction));
            bool = false;
        } else {
            updateShowList(driver, findElementByName(driver, "transactionHistoryTbl_length"), "All");

            pause(5);
            List<WebElement> lstDashboardTxAmount = driver.findElements(By.xpath("//table[@id='transactionHistoryTbl']//tr//td[4]"));

            for (WebElement amount : lstDashboardTxAmount) {
                if (Math.abs(getDoubleFromString(getText(amount))) >= ConsumerTransactionHistoryIndexPage._filterTransactionFromAmount &&
                        Math.abs(getDoubleFromString(getText(amount))) <= ConsumerTransactionHistoryIndexPage._filterTransactionToAmount) {
                    bool = true;
                } else {
                    System.out.println(Math.abs(getDoubleFromString(getText(amount))));
                    System.out.println(ConsumerTransactionHistoryIndexPage._filterTransactionFromAmount);
                    System.out.println(ConsumerTransactionHistoryIndexPage._filterTransactionToAmount);
                    bool = false;
                    break;
                }
            }

            List<String> lstCategories = new LinkedList<>();
            for (WebElement category : lstDashboardUserCategory) lstCategories.add(getInnerText(category));

            System.out.println(lstCategories);

            bool = bool && lstCategories.stream().distinct().count() <= 2 &&
                    sizeOf(lstCategories) == sizeOf(lstBtnMore);
        }

        return bool;
    }

    public boolean verifyAllTransactionDisplay() {
        pause(5);
        updateShowList(driver, findElementByName(driver, "transactionHistoryTbl_length"), "All");
        List<WebElement> lstDashboardCategory = driver.findElements(By.xpath("//table[@id='transactionHistoryTbl']//tr//td[3]"));
        return sizeOf(lstDashboardCategory) ==
                sizeOf(ConsumerTransactionHistoryIndexPage._allTransactionDetailsCategory);
    }

    public boolean verifyMerchantTransactionDetailsPopUp() {

        pause(2);

        System.err.println(getIntegerFromString(getText(lblTransactionIDPopup)));
        System.err.println(ConsumerTransactionHistoryIndexPage._transactionID);
        System.err.println(formatTwoDecimal(getDoubleFromString(getText(lstTransactionDetails.get(1)))));
        System.err.println(formatTwoDecimal(ConsumerTransactionHistoryIndexPage._transactionAmount));
        System.err.println(getText(lstTransactionDetails.get(0)));
        System.err.println(ConsumerTransactionHistoryIndexPage._transactionDate
                + " / " + ConsumerTransactionHistoryIndexPage._transactionTime);
        System.err.println(getText(lstTransactionDetails.get(2)).split("\n")[0]);
        System.err.println(ConsumerLoginVerification._username);
        System.err.println(getText(lstTransactionDetails.get(2)).split("\n")[1]);
        System.err.println(ConsumerLoginVerification._usernumber);

        boolean bool;

        if (ConsumerTransactionHistoryIndexPage._transactionType.equalsIgnoreCase("Invoice") ||
                ConsumerTransactionHistoryIndexPage._transactionType.equalsIgnoreCase("Transfer")) {
            System.out.println(getText(lstTransactionDetails.get(4)));
            System.out.println(ConsumerTransactionHistoryIndexPage._transactionType);
            bool = getText(lstTransactionDetails.get(4)).equalsIgnoreCase(ConsumerTransactionHistoryIndexPage._transactionType);
        } else {
            System.out.println(getText(lstTransactionDetails.get(3)));
            System.out.println(ConsumerTransactionHistoryIndexPage._transactionType);
            bool = getText(lstTransactionDetails.get(3)).equalsIgnoreCase(ConsumerTransactionHistoryIndexPage._transactionType);
        }

        return bool && getIntegerFromString(getText(lblTransactionIDPopup)) == (ConsumerTransactionHistoryIndexPage._transactionID) &&
                getText(lstTransactionDetails.get(0)).equalsIgnoreCase(ConsumerTransactionHistoryIndexPage._transactionDate
                        + " / " + ConsumerTransactionHistoryIndexPage._transactionTime) &&
                formatTwoDecimal(getDoubleFromString(getText(lstTransactionDetails.get(1)))) ==
                        formatTwoDecimal(ConsumerTransactionHistoryIndexPage._transactionAmount) &&
                getText(lstTransactionDetails.get(2)).split("\n")[0].equalsIgnoreCase(ConsumerLoginVerification._username) &&
                getText(lstTransactionDetails.get(2)).split("\n")[1].equalsIgnoreCase(ConsumerLoginVerification._usernumber);

    }

    public boolean verifyConsumerTransactionDetailsPopUp() {

        pause(2);

        lstTransactionDetails.forEach(e -> System.out.println(getText(e)));
        System.out.println("-----------------------------------");
        System.out.println(getText(lblTransactionIDPopup));
        System.out.println(ConsumerTransactionHistoryIndexPage._transactionID);
        System.out.println(ConsumerTransactionHistoryIndexPage._transactionDate + " / " + ConsumerTransactionHistoryIndexPage._transactionTime);
        System.out.println(ConsumerTransactionHistoryIndexPage._transactionAmount);
        System.out.println(ConsumerLoginVerification._username);
        System.out.println(ConsumerLoginVerification._usernumber);
        System.out.println(ConsumerTransactionHistoryIndexPage._transactionUsername);
        System.out.println(ConsumerTransactionHistoryIndexPage._transactionType);

        return getIntegerFromString(getText(lblTransactionIDPopup)) == (ConsumerTransactionHistoryIndexPage._transactionID) &&
                getText(lstTransactionDetails.get(0)).equalsIgnoreCase(ConsumerTransactionHistoryIndexPage._transactionDate
                        + " / " + ConsumerTransactionHistoryIndexPage._transactionTime) &&
                formatTwoDecimal(getDoubleFromString(getText(lstTransactionDetails.get(1)))) == formatTwoDecimal(ConsumerTransactionHistoryIndexPage._transactionAmount) &&
                getText(lstTransactionDetails.get(2)).split("\n")[0].equalsIgnoreCase(ConsumerLoginVerification._username) &&
                getText(lstTransactionDetails.get(2)).split("\n")[1].equalsIgnoreCase(ConsumerLoginVerification._usernumber) &&
                getText(lstTransactionDetails.get(3)).equalsIgnoreCase(ConsumerTransactionHistoryIndexPage._transactionType);
    }

    public boolean verifyTransactionFilterByMinAmount() {

        boolean bool = true;

        if (isListEmpty(lstDashboardUserCategory)) {
            testValidationLog(getText(lblNoTransaction));
            bool = false;
        } else {

            pause(3);
            List<WebElement> lstDashboardTxAmount = driver.findElements(By.xpath("//table[@id='transactionHistoryTbl']//tr//td[4]"));

            for (WebElement amount : lstDashboardTxAmount) {
                if (Math.abs(getDoubleFromString(getText(amount))) >=
                        ConsumerTransactionHistoryIndexPage._filterTransactionFromAmount) {
                    bool = Math.abs(getDoubleFromString(getText(amount))) >=
                            ConsumerTransactionHistoryIndexPage._filterTransactionFromAmount;
                } else {
                    bool = false;
                    break;
                }
            }
        }
        return bool;
    }

    public boolean verifyTransactionFilterByMaxAmount() {

        boolean bool = true;

        if (isListEmpty(lstDashboardUserCategory)) {
            testValidationLog(getText(lblNoTransaction));
            bool = false;
        } else {

            pause(3);
            List<WebElement> lstDashboardTxAmount = driver.findElements(By.xpath("//table[@id='transactionHistoryTbl']//tr//td[4]"));

            for (WebElement amount : lstDashboardTxAmount) {
                if (Math.abs(getDoubleFromString(getText(amount))) <=
                        ConsumerTransactionHistoryIndexPage._filterTransactionToAmount) {
                    bool = Math.abs(getDoubleFromString(getText(amount))) <=
                            ConsumerTransactionHistoryIndexPage._filterTransactionToAmount;
                } else {
                    bool = false;
                    break;
                }
            }
        }
        return bool;
    }
}
