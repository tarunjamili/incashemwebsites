package com.incashme.consumer.verification;

import com.framework.init.AbstractPage;
import com.incashme.consumer.indexpage.ConsumerDashboardIndexPage;
import com.incashme.consumer.indexpage.ConsumerWithdrawMoneyIndexPage;
import com.incashme.consumer.validations.WithdrawMoneyValidations;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class ConsumerWithdrawMoneyVerification extends AbstractPage implements WithdrawMoneyValidations {

    public ConsumerWithdrawMoneyVerification(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//div[contains(@class,'blnc-lbl')]/span")
    private WebElement lblWithdrawBalance;

    @FindBy(xpath = "//ul[contains(@class,'no_money_card_list')]")
    private WebElement sectionBankList;

    @FindBy(xpath = "//input[@formcontrolname='amount']")
    private WebElement txtWithdrawAmount;

    @FindBy(xpath = "//div[contains(@class,'blnc-lbl')]/span")
    private WebElement lblBalance;

    @FindBy(xpath = "//input[@formcontrolname='display_name']")
    private WebElement txtDisplayName;

    @FindBy(xpath = "//input[@formcontrolname='holder_name']")
    private WebElement txtHolderName;

    @FindBy(xpath = "//select[@formcontrolname='bank_name']")
    private WebElement btnBankName;

    @FindBy(xpath = "//select[@formcontrolname='bank_account_type']")
    private WebElement btnBankAccountType;

    @FindBy(xpath = "//input[@formcontrolname='bank_account_no']")
    private WebElement txtBankAccountNumber;

    @FindBy(xpath = "//input[@formcontrolname='ifsc_no']")
    private WebElement txtBankIFSCNumber;

    @FindBy(xpath = "//div[contains(@class,'contentdiv')]//button[contains(@class,'btn-pink') and text()='Reset']")
    private WebElement btnResetForm;

    @FindBy(xpath = "//div[contains(@class,'contentdiv')]//button[contains(@class,'btn-pink') and text()='Cancel']")
    private WebElement btnCancelWithdraw;

    @FindBy(xpath = "//div[contains(@class,'contentdiv')]//button[contains(@class,'btn-green') and text()='Confirm']")
    private WebElement btnAddBankAccount;

    @FindBy(xpath = "//div[contains(@class,'contentdiv')]//button[contains(@class,'btn-green') and text()='Send']")
    private WebElement btnSendWithdraw;

    @FindBy(xpath = "//div//div[contains(@class,'add_bank_close')]")
    private WebElement btnCloseForm;

    @FindBy(xpath = "//div[contains(@class,'wdm-title') and contains(text(),'Add Bank Account')]")
    private WebElement sectionAddBankAccount;

    @FindBy(xpath = "//input[@formcontrolname='display_name']//following-sibling::span[contains(@class,'color-danger')]")
    private WebElement lblValidationInvalidDisplayName;

    @FindBy(xpath = "//input[@formcontrolname='holder_name']//following-sibling::span[contains(@class,'color-danger')]")
    private WebElement lblValidationInvalidHolderName;

    @FindBy(xpath = "//input[@formcontrolname='bank_account_no']//following-sibling::span[contains(@class,'color-danger')]")
    private WebElement lblValidationInvalidAccountNumber;

    @FindBy(xpath = "//input[@formcontrolname='ifsc_no']//following-sibling::span[contains(@class,'color-danger')]")
    private WebElement lblValidationInvalidIFSCNumber;

    @FindBy(xpath = "//div//h2[contains(@class,'swal2-title')]")
    private WebElement lblConfirmBankAccountAdded;

    @FindBy(xpath = "//div[contains(@class,'swal2-content')]")
    private WebElement lblDeleteBankAccountMessage;

    @FindBy(xpath = "//div[contains(@class,'swal2-content')]")
    private WebElement lblDeleteBankAccountConfirmationMessage;

    @FindBy(xpath = ".//div[contains(@class,'swal2-actions')]//button[text()='OK' or text() = 'Ok']")
    private WebElement btnConfirmOK;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@class,'no_mon_txt')]")})
    private List<WebElement> lstAddedBankAccounts;

    @FindBy(xpath = "//div[contains(@id,'swal2-content')]")
    private WebElement lblInvalidWithdrawAMount;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@class,'with_card_details')]//div[contains(@class,'with_card_d_title')]")})
    private List<WebElement> lstWithdrawDetails;

    @FindBy(xpath = "//div[contains(@class,'wt_withdrow_amt')]")
    private WebElement lblWithdrawAmount;

    @FindBy(xpath = "//div[contains(@class,'invoice_ad_amt')]")
    private WebElement lblConfirmWithdrawAmount;

    @FindBy(xpath = "//div[contains(@class,'usr-detl')]//div[contains(@class,'with_t_id')]")
    private WebElement lblConfirmDisplayName;

    @FindBy(xpath = "//div[contains(@class,'usr-detl')]//div[contains(@class,'with_t_name')]")
    private WebElement lblConfirmHolderName;

    @FindBy(xpath = "//div[contains(@class,'usr-detl')]//div[contains(@class,'with_t_bank')]")
    private WebElement lblConfirmBankName;

    @FindBy(xpath = "//div[contains(@class,'usr-detl')]//div[contains(@class,'with_t_account')]")
    private WebElement lblConfirmAccountNumber;

    @FindBy(xpath = "//div[contains(@class,'usr-detl')]//following-sibling::div[contains(@class,'color-green')]")
    private WebElement lblConfirmationWithdraw;

    @FindBy(xpath = "//div[contains(text(),'Insufficient')]")
    private WebElement lblInsufficientBalance;

    public boolean verifyWithdrawScreen() {
        if (ConsumerDashboardIndexPage._currentBalance.equalsIgnoreCase("0")) {
            testValidationLog(getText(lblInsufficientBalance));
            return isElementDisplay(lblInsufficientBalance);
        } else {
            return isElementDisplay(txtWithdrawAmount) && isElementDisplay(lblWithdrawBalance) &&
                    isElementDisplay(sectionBankList)
                    && getText(lblWithdrawBalance).equalsIgnoreCase(ConsumerDashboardIndexPage._currentBalance);
        }
    }

    public boolean verifyAddNewAccountForm() {

        boolean bool = isElementDisplay(txtDisplayName) && isElementDisplay(txtHolderName) &&
                isElementDisplay(btnBankName);

        pause(1);
        btnBankName.sendKeys(Keys.PAGE_DOWN);
        pause(1);

        return bool && isElementDisplay(btnBankAccountType) && isElementDisplay(txtBankAccountNumber) &&
                isElementDisplay(txtBankIFSCNumber) && isElementDisplay(btnResetForm) &&
                isElementDisplay(btnAddBankAccount) && isElementDisplay(btnCloseForm);

    }

    public boolean verifyCloseBankAccountScreen() {
        return !isElementPresent(sectionAddBankAccount);
    }

    public boolean verifyInvalidDisplayNameValidation() {
        testVerifyLog(getText(lblValidationInvalidDisplayName));
        return getText(lblValidationInvalidDisplayName).equalsIgnoreCase(INVALID_DISPLAY_NAME);
    }

    public boolean verifyInvalidHolderNameNameValidation() {
        testVerifyLog(getText(lblValidationInvalidHolderName));
        return getText(lblValidationInvalidHolderName).equalsIgnoreCase(INVALID_ACCOUNT_HOLDER_NAME);
    }

    public boolean verifyInvalidAccountNumberValidation() {
        testVerifyLog(getText(lblValidationInvalidAccountNumber));
        return getText(lblValidationInvalidAccountNumber).equalsIgnoreCase(INVALID_BANK_ACCOUNT_NUMBER);
    }

    public boolean verifyInvalidIFSCNumberValidation() {
        testVerifyLog(getText(lblValidationInvalidIFSCNumber));
        return getText(lblValidationInvalidIFSCNumber).equalsIgnoreCase(INVALID_BANK_IFSC_CODE);
    }

    public boolean verifyConfirmationForBankAccount() {
        pause(1);
        testConfirmationLog(getText(lblConfirmBankAccountAdded));
        boolean bool = isElementDisplay(lblConfirmBankAccountAdded);
        clickOn(driver, btnConfirmOK);
        return bool;
    }

    public boolean verifyBankAccountDetails() {

        testInfoLog("Bank Name", ConsumerWithdrawMoneyIndexPage._bankName);
        testInfoLog("Account Number", ConsumerWithdrawMoneyIndexPage._accountNumber);

        String encAccountNumber = ConsumerWithdrawMoneyIndexPage._accountNumber.
                substring(0, ConsumerWithdrawMoneyIndexPage._accountNumber.length() - 4).replaceAll("[0-9]", "X")
                + ConsumerWithdrawMoneyIndexPage._accountNumber.substring(ConsumerWithdrawMoneyIndexPage._accountNumber.length() - 4);

        return getText(lstAddedBankAccounts.get(0)).split("\n")[1].equalsIgnoreCase(encAccountNumber);

    }

    public boolean verifyValidationForDeleteBankAccount() {
        testVerifyLog(getText(lblDeleteBankAccountMessage));
        return getText(lblDeleteBankAccountMessage).contains(String.valueOf(getIntegerFromString
                (ConsumerWithdrawMoneyIndexPage._encAccountNumber)));
    }

    public boolean verifyConfirmationForDeletedBankAccount() {
        pause(3);
        boolean bool = isElementDisplay(lblDeleteBankAccountConfirmationMessage);
        testConfirmationLog(getText(lblDeleteBankAccountConfirmationMessage));
        clickOn(driver, btnConfirmOK);
        return bool;
    }

    public boolean verifyBankAccountDeletedSuccessfully() {
        return ConsumerWithdrawMoneyIndexPage._totalAddedBankAccounts - 1 == sizeOf(lstAddedBankAccounts);
    }

    public boolean verifyValidationForInvalidWithdrawAMount() {
        pause(3);
        boolean bool = isElementDisplay(lblInvalidWithdrawAMount);
        testValidationLog(getText(lblInvalidWithdrawAMount));
        clickOn(driver, btnConfirmOK);
        return bool;
    }

    public boolean verifyWithdrawDetailsScreen() {

        testInfoLog("Bank Name", getText(lstWithdrawDetails.get(0)));
        testInfoLog("Display Name", getText(lstWithdrawDetails.get(1)).split("\n")[0]);
        testInfoLog("Account Holder Name", getText(lstWithdrawDetails.get(2)).split("\n")[0]);
        testInfoLog("Account Number", getText(lstWithdrawDetails.get(3)).split("\n")[0]);

        return getText(lstWithdrawDetails.get(0)).equalsIgnoreCase(ConsumerWithdrawMoneyIndexPage._withdrawBankName) &&
                getText(lstWithdrawDetails.get(3)).split("\n")[0].substring(getText(lstWithdrawDetails.get(3)).split("\n")[0].length() - 4).
                        equalsIgnoreCase(ConsumerWithdrawMoneyIndexPage._withdrawBankAccountNumber.substring(ConsumerWithdrawMoneyIndexPage._withdrawBankAccountNumber.length() - 4)) &&
                getDoubleFromString(getText(lblWithdrawAmount)) == ConsumerWithdrawMoneyIndexPage._withdrawAmount;

    }

    public boolean verifyTotalWithdrawAmount() {
        return ConsumerWithdrawMoneyIndexPage._withdrawAmount + ConsumerWithdrawMoneyIndexPage._withdrawImpsFee
                == ConsumerWithdrawMoneyIndexPage._withDrawTotalAmount;
    }

    public boolean verifyConfirmationWithdrawDetails() {

        pause(3);

        return getDoubleFromString(getText(lblConfirmWithdrawAmount)) == ConsumerWithdrawMoneyIndexPage._withDrawTotalAmount &&
                getText(lblConfirmDisplayName).equalsIgnoreCase(ConsumerWithdrawMoneyIndexPage._withdrawDisplayName) &&
                getText(lblConfirmHolderName).equalsIgnoreCase(ConsumerWithdrawMoneyIndexPage._withdrawAccountHolder) &&
                getText(lblConfirmBankName).equalsIgnoreCase(ConsumerWithdrawMoneyIndexPage._withdrawBankName) &&
                String.valueOf(getLongFromString(getText(lblConfirmAccountNumber))).equalsIgnoreCase(ConsumerWithdrawMoneyIndexPage._withdrawBankAccountNumber);

    }

    public boolean verifyCompletedWithdrawDetails() {

        pause(5);

        testConfirmationLog(getText(lblConfirmationWithdraw));

        return getDoubleFromString(getText(lblConfirmWithdrawAmount)) == ConsumerWithdrawMoneyIndexPage._withDrawTotalAmount &&
                getText(lblConfirmDisplayName).equalsIgnoreCase(ConsumerWithdrawMoneyIndexPage._withdrawDisplayName) &&
                getText(lblConfirmHolderName).equalsIgnoreCase(ConsumerWithdrawMoneyIndexPage._withdrawAccountHolder) &&
                getText(lblConfirmBankName).equalsIgnoreCase(ConsumerWithdrawMoneyIndexPage._withdrawBankName) &&
                String.valueOf(getLongFromString(getText(lblConfirmAccountNumber))).equalsIgnoreCase(ConsumerWithdrawMoneyIndexPage._withdrawBankAccountNumber);

    }

    public boolean verifyWithdrawDeductedFromBalance() {
        pause(3);
        return getDoubleFromString(getInnerText(lblBalance)) ==
                getDoubleFromString(ConsumerDashboardIndexPage._currentBalance) -
                        ConsumerWithdrawMoneyIndexPage._withDrawTotalAmount;
    }

}
