package com.incashme.consumer.index;

import com.framework.init.SeleniumInit;
import com.incashme.consumer.indexpage.ConsumerTransactionHistoryIndexPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ConsumerTransactionHistoryIndex extends SeleniumInit {

    @Test
    public void consumer_FilterByTransactionType() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_124 :: To verify Consumer can filter the Transaction History by User Type.");
        consumerLoginIndexPage.getVersion();

        consumerLoginVerification = consumerLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (consumerLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.clickOnTransactionHistory();

        testVerifyLog("Verify user can see the Transaction History screen with details.");
        if (consumerTransactionHistoryVerification.verifyTransactionHistoryScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (consumerTransactionHistoryIndexPage.isTransactionHistoryAvailable()) {

            consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.getCurrentConsumerCategoryDetails();

            consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.clickOnIndividualButton();

            testVerifyLog("Verify user can filter transaction by Individual.");
            if (consumerTransactionHistoryVerification.verifyIndividualTransactionFilter()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.clickOnMerchantButton();

            testVerifyLog("Verify user can filter transaction by Merchant.");
            if (consumerTransactionHistoryVerification.verifyMerchantTransactionFilter()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.clickOnAgentButton();

            testVerifyLog("Verify user can filter transaction by Agent.");
            if (consumerTransactionHistoryVerification.verifyAgentTransactionFilter()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.clickOnAllButton();

            testVerifyLog("Verify user can filter transaction by All.");
            if (consumerTransactionHistoryVerification.verifyAllTransactionFilter()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void consumer_sortAndSearchTransactionHistoryDetails() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_126 :: To verify Consumer can search and sort the Transaction History details.");
        consumerLoginIndexPage.getVersion();

        consumerLoginVerification = consumerLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (consumerLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.clickOnTransactionHistory();

        testVerifyLog("Verify user can see the Transaction History screen with details.");
        if (consumerTransactionHistoryVerification.verifyTransactionHistoryScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (consumerTransactionHistoryIndexPage.isTransactionHistoryAvailable()) {

            consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.clickOnUserNameToSort();

            testVerifyLog("Verify User Name are sorted successfully.");
            if (consumerTransactionHistoryVerification.verifyUserNameSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.clickOnTransactionIDToSort();

            testVerifyLog("Verify Transaction ID are sorted successfully.");
            if (consumerTransactionHistoryVerification.verifyUserTxIDSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.clickOnCategoryToSort();

            testVerifyLog("Verify User Type are sorted successfully.");
            if (consumerTransactionHistoryVerification.verifyUserCategorySorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.clickOnTypesToSort();

            testVerifyLog("Verify User Type are sorted successfully.");
            if (consumerTransactionHistoryVerification.verifyUserTypeSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.clickOnDateTimeToSort();

            testVerifyLog("Verify Transaction Date & Time are sorted successfully.");
            if (consumerTransactionHistoryVerification.verifyTransactionDateTimeSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.enterDashboardSearchCriteria(false);

            testVerifyLog("Verify user can search details successfully.");
            if (consumerTransactionHistoryVerification.verifyTableSearchSuccessful()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            if (!isPositiveExecution) {
                consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.enterDashboardSearchCriteria(true);

                testVerifyLog("Verify validation message for the invalid search details.");
                if (consumerTransactionHistoryVerification.verifyTableValidationForInvalidSearch()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

        } else {
            testWarningLog("No Transaction History found for the Consumer, try to run with the Consumer " +
                    "who has Transaction History.");
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");

    }

    @Test
    public void consumer_filterTransactionDetails() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_125 :: To verify Consumer can filter the Transaction History.");
        consumerLoginIndexPage.getVersion();

        consumerLoginVerification = consumerLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (consumerLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.clickOnTransactionHistory();

        testVerifyLog("Verify user can see the Transaction History screen with details.");
        if (consumerTransactionHistoryVerification.verifyTransactionHistoryScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (consumerTransactionHistoryIndexPage.isTransactionHistoryAvailable()) {

            consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.getCurrentConsumerCategoryDetails();

            consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.clickOnFilterButton();

            testVerifyLog("Verify user can see the Filter screen.");
            if (consumerTransactionHistoryVerification.verifyFilterScreen()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            if (!isPositiveExecution) {
                consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.enterTransactionAmountRange(true);

                consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.clickOnDoneButton();

                testVerifyLog("Verify user can see no results for the invalid transaction search range.");
                if (consumerTransactionHistoryVerification.verifyNoTransactionFound()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.clickOnFilterButton();

            consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.enterTransactionAmountRange(false);

            consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.clickOnDoneButton();

            testVerifyLog("Verify user can filter transaction by amount range.");
            if (consumerTransactionHistoryVerification.verifyTransactionFilterByAmountRange()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.clickOnFilterButton();

            consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.selectAgentCategory();

            consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.clickOnDoneButton();

            testVerifyLog("Verify user can filter transaction by amount range with Agent.");
            if (consumerTransactionHistoryVerification.verifyTransactionFilterByAgent()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.clickOnFilterButton();

            consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.selectIndividualCategory();

            consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.clickOnDoneButton();

            testVerifyLog("Verify user can filter transaction by amount range with Individual.");
            if (consumerTransactionHistoryVerification.verifyTransactionFilterByIndividual()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.clickOnFilterButton();

            consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.selectMerchantCategory();

            consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.clickOnDoneButton();

            testVerifyLog("Verify user can filter transaction by amount range with Merchant.");
            if (consumerTransactionHistoryVerification.verifyTransactionFilterByMerchant()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.clickOnFilterButton();

            consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.clickOnClearAllButton();

            consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.clickOnDoneButton();

            testVerifyLog("Verify user can filter transaction by amount range with Merchant.");
            if (consumerTransactionHistoryVerification.verifyAllTransactionDisplay()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.clickOnFilterButton();

            consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.enterTransactionAmount("from");

            consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.clickOnDoneButton();

            testVerifyLog("Verify user can filter transaction by amount by Minimum Amount.");
            if (consumerTransactionHistoryVerification.verifyTransactionFilterByMinAmount()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.clickOnFilterButton();

            consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.clickOnClearAllButton();

            consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.enterTransactionAmount("to");

            consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.clickOnDoneButton();

            testVerifyLog("Verify user can filter transaction by amount by Maximum Amount.");
            if (consumerTransactionHistoryVerification.verifyTransactionFilterByMaxAmount()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

        } else {
            testWarningLog("No Transaction History found for the Consumer, try to run with the Consumer " +
                    "who has Transaction History.");
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");

    }

    @Test
    public void consumer_transactionHistoryDetails() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_127 :: To verify Consumer can see the Transaction History details properly.");
        consumerLoginIndexPage.getVersion();

        consumerLoginVerification = consumerLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (consumerLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.clickOnTransactionHistory();

        testVerifyLog("Verify user can see the Transaction History screen with details.");
        if (consumerTransactionHistoryVerification.verifyTransactionHistoryScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (consumerTransactionHistoryIndexPage.isTransactionHistoryAvailable()) {

            consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.getTransactionDetails("Merchant");

            if (ConsumerTransactionHistoryIndexPage._userNumber >= 0) {
                consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.clickOnMOREButton();

                testVerifyLog("Verify user can see the details properly on the Transaction Details popup.");
                if (consumerTransactionHistoryVerification.verifyMerchantTransactionDetailsPopUp()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.clickOnPopUpCloseButton();
            } else {
                testWarningLog("No Merchant Transaction found in the list");
            }

            consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.getTransactionDetails("Consumer");

            if (ConsumerTransactionHistoryIndexPage._userNumber >= 0) {

                consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.clickOnMOREButton();

                testVerifyLog("Verify user can see the details properly on the Transaction Details popup.");
                if (consumerTransactionHistoryVerification.verifyConsumerTransactionDetailsPopUp()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            } else {
                testWarningLog("No Consumer Transaction found in the list");
            }

        } else {
            testWarningLog("No Transaction History found for the Consumer, try to run with the Consumer " +
                    "who has Transaction History.");
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");

    }

}
