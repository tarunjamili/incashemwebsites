package com.incashme.consumer.index;

import com.framework.init.SeleniumInit;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ConsumerAddMoneyIndex extends SeleniumInit {

    @Test
    public void consumer_addDeleteDebitCard() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_130 :: To verify Consumer can add/delete Debit Card in Add Money.");
        consumerLoginIndexPage.getVersion();

        consumerLoginVerification = consumerLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (consumerLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerAddMoneyVerification = consumerAddMoneyIndexPage.clickOnAddMoney();

        testVerifyLog("Verify user can see the Add Money screen with details.");
        if (consumerAddMoneyVerification.verifyAddMoneyScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!consumerAddMoneyIndexPage.isMaxDebitCardsAdded()) {

            testVerifyLog("Verify user can see the '+' button to add Debit Card.");
            if (consumerAddMoneyVerification.verifyAddDebitCardButton()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerAddMoneyVerification = consumerAddMoneyIndexPage.clickOnAddDebitCardButton();

            testVerifyLog("Verify user can see Add New Debit Card form.");
            if (consumerAddMoneyVerification.verifyAddDebitCardForm()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            if (!isPositiveExecution) {
                consumerAddMoneyVerification = consumerAddMoneyIndexPage.enterInvalidCardHolderName();
                consumerAddMoneyVerification = consumerAddMoneyIndexPage.enterDebitCardNumber();
                consumerAddMoneyVerification = consumerAddMoneyIndexPage.enterInvalidExpiryDate();

                consumerAddMoneyVerification = consumerAddMoneyIndexPage.clickOnSaveButton();

                testVerifyLog("Verify user can see the validation message for invalid details");
                if (consumerAddMoneyVerification.verifyValidationMessageForInvalidCardDetails()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                consumerAddMoneyVerification = consumerAddMoneyIndexPage.clickOnSaveButton();

                testVerifyLog("Verify user can see the validation message for blank details");
                if (consumerAddMoneyVerification.verifyValidationMessageForBlankDebitCardDetails()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            consumerAddMoneyVerification = consumerAddMoneyIndexPage.enterCardHolderName();
            consumerAddMoneyVerification = consumerAddMoneyIndexPage.enterDebitCardNumber();
            consumerAddMoneyVerification = consumerAddMoneyIndexPage.enterExpiryDate();

            consumerAddMoneyVerification = consumerAddMoneyIndexPage.clickOnSaveButton();

            testVerifyLog("Verify user can see the confirmation message for added card");
            if (consumerAddMoneyVerification.verifyCardAddedConfirmationMessage()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            testVerifyLog("Verify added card details on the card.");
            if (consumerAddMoneyVerification.verifyAddedDebitCardDetailsInCard()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerAddMoneyVerification = consumerAddMoneyIndexPage.openLatestAddedDebitCard();

            testVerifyLog("Verify added card details on the screen.");
            if (consumerAddMoneyVerification.verifyDebitCardDetailsScreen()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

        } else {
            testVerifyLog("Verify user can't see the '+' button to add Debit Card.");
            if (consumerAddMoneyVerification.verifyAddDebitCardButtonNotAvailable()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            testWarningLog("Maximum debit card limit reached, can't add new debit card, Please remove any and run again.");
        }

        if (!consumerAddMoneyIndexPage.isNoDebitCard()) {

            consumerAddMoneyVerification = consumerAddMoneyIndexPage.clickOnDeleteDebitCardButton();

            consumerAddMoneyVerification = consumerAddMoneyIndexPage.clickOnDeleteCardNoButton();

            consumerAddMoneyVerification = consumerAddMoneyIndexPage.clickOnDeleteDebitCardButton();

            consumerAddMoneyVerification = consumerAddMoneyIndexPage.clickOnDeleteCardYesButton();

            testVerifyLog("Verify user can see the confirmation message for deleted card");
            if (consumerAddMoneyVerification.verifyCardDeletedConfirmationMessage()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            testVerifyLog("Verify user Card deleted successfully.");
            if (consumerAddMoneyVerification.verifyDebitCardDeletedSuccessfully()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

        } else {
            testWarningLog("No debit cards found, can't remove the debit card, Please add one and run again.");
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void consumer_addDeleteCreditCard() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_131 :: To verify Consumer can add/delete Credit Card in Add Money.");
        consumerLoginIndexPage.getVersion();

        consumerLoginVerification = consumerLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (consumerLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerAddMoneyVerification = consumerAddMoneyIndexPage.clickOnAddMoney();

        testVerifyLog("Verify user can see the Add Money screen with details.");
        if (consumerAddMoneyVerification.verifyAddMoneyScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!consumerAddMoneyIndexPage.isMaxCreditCardsAdded()) {

            testVerifyLog("Verify user can see the '+' button to add Credit Card.");
            if (consumerAddMoneyVerification.verifyAddCreditCardButton()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerAddMoneyVerification = consumerAddMoneyIndexPage.clickOnAddCreditCardButton();

            testVerifyLog("Verify user can see Add New Credit Card form.");
            if (consumerAddMoneyVerification.verifyAddCreditCardForm()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            if (!isPositiveExecution) {
                consumerAddMoneyVerification = consumerAddMoneyIndexPage.enterInvalidCardHolderName();
                consumerAddMoneyVerification = consumerAddMoneyIndexPage.enterDebitCardNumber();
                consumerAddMoneyVerification = consumerAddMoneyIndexPage.enterInvalidExpiryDate();

                consumerAddMoneyVerification = consumerAddMoneyIndexPage.clickOnSaveButton();

                testVerifyLog("Verify user can see the validation message for invalid details");
                if (consumerAddMoneyVerification.verifyValidationMessageForInvalidCardDetails()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                consumerAddMoneyVerification = consumerAddMoneyIndexPage.clickOnSaveButton();

                testVerifyLog("Verify user can see the validation message for blank details");
                if (consumerAddMoneyVerification.verifyValidationMessageForBlankCreditCardDetails()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            consumerAddMoneyVerification = consumerAddMoneyIndexPage.enterCardHolderName();
            consumerAddMoneyVerification = consumerAddMoneyIndexPage.enterDebitCardNumber();
            consumerAddMoneyVerification = consumerAddMoneyIndexPage.enterExpiryDate();

            consumerAddMoneyVerification = consumerAddMoneyIndexPage.clickOnSaveButton();

            testVerifyLog("Verify user can see the confirmation message for added card");
            if (consumerAddMoneyVerification.verifyCardAddedConfirmationMessage()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            testVerifyLog("Verify added card details on the card.");
            if (consumerAddMoneyVerification.verifyAddedCreditCardDetailsInCard()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerAddMoneyVerification = consumerAddMoneyIndexPage.openLatestAddedCreditCard();

            testVerifyLog("Verify added card details on the screen.");
            if (consumerAddMoneyVerification.verifyCreditCardDetailsScreen()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

        } else {
            testVerifyLog("Verify user can't see the '+' button to add Credit Card.");
            if (consumerAddMoneyVerification.verifyAddCreditCardButtonNotAvailable()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            testWarningLog("Maximum credit card limit reached, can't add new credit card, Please remove any and run again.");
        }

        if (!consumerAddMoneyIndexPage.isNoCreditCard()) {

            consumerAddMoneyVerification = consumerAddMoneyIndexPage.clickOnDeleteCreditCardButton();

            consumerAddMoneyVerification = consumerAddMoneyIndexPage.clickOnDeleteCardNoButton();

            consumerAddMoneyVerification = consumerAddMoneyIndexPage.clickOnDeleteCreditCardButton();

            consumerAddMoneyVerification = consumerAddMoneyIndexPage.clickOnDeleteCardYesButton();

            testVerifyLog("Verify user can see the confirmation message for deleted card");
            if (consumerAddMoneyVerification.verifyCardDeletedConfirmationMessage()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            testVerifyLog("Verify user Card deleted successfully.");
            if (consumerAddMoneyVerification.verifyCreditCardDeletedSuccessfully()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

        } else {
            testWarningLog("No credit cards found, can't remove the credit card, Please add one and run again.");
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void consumer_addMoneyFromDebitCard() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_133 :: To verify Consumer can add money via Debit Card successfully.");
        consumerLoginIndexPage.getVersion();

        consumerLoginVerification = consumerLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (consumerLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerDashboardVerification = consumerDashboardIndexPage.clickOnBalance();

        consumerAddMoneyVerification = consumerAddMoneyIndexPage.getCurrentAccountBalance();

        consumerAddMoneyVerification = consumerAddMoneyIndexPage.clickOnAddMoney();

        testVerifyLog("Verify user can see the Add Money screen with details.");
        if (consumerAddMoneyVerification.verifyAddMoneyScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (consumerAddMoneyIndexPage.isTestDebitCardDisplay()) {

            consumerAddMoneyVerification = consumerAddMoneyIndexPage.openTestDebitCard();

            testVerifyLog("Verify user can see the test card on the screen.");
            if (consumerAddMoneyVerification.verifyTestDebitCardDetailsScreen()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerAddMoneyVerification = consumerAddMoneyIndexPage.enterCardDetails();

            testVerifyLog("Verify Pay button is disabled without entering the amount.");
            if (consumerAddMoneyVerification.verifyPayButtonDisabled()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            if (!isPositiveExecution) {
                consumerAddMoneyVerification = consumerAddMoneyIndexPage.enterMaxTransactionAmount();

                testVerifyLog("Verify Pay button is disabled without entering the amount.");
                if (consumerAddMoneyVerification.verifyPayButtonDisabled()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            consumerAddMoneyVerification = consumerAddMoneyIndexPage.enterValidTransactionAmount();

            consumerAddMoneyVerification = consumerAddMoneyIndexPage.clickOnAddMoneyPayButton();

            testVerifyLog("Verify Success screen display after successful transaction.");
            if (consumerAddMoneyVerification.verifySuccessAddMoney()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerDashboardVerification = consumerDashboardIndexPage.clickOnBalance();

            testVerifyLog("Verify Balance Updated with recent added amount.");
            if (consumerAddMoneyVerification.verifyBalanceUpdated()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

        } else {
            testVerifyLog("No Test Card Display, adding a test card and running scenario.");
            if (!consumerAddMoneyIndexPage.isMaxDebitCardsAdded()) {

                consumerAddMoneyVerification = consumerAddMoneyIndexPage.clickOnAddDebitCardButton();

                consumerAddMoneyVerification = consumerAddMoneyIndexPage.enterTestCardDetails();

                consumerAddMoneyVerification = consumerAddMoneyIndexPage.clickOnSaveButton();

                testVerifyLog("Verify user can see the confirmation message for added card");
                if (consumerAddMoneyVerification.verifyCardAddedConfirmationMessage()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                consumerAddMoneyVerification = consumerAddMoneyIndexPage.openTestDebitCard();

                testVerifyLog("Verify user can see the test card on the screen.");
                if (consumerAddMoneyVerification.verifyTestDebitCardDetailsScreen()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                consumerAddMoneyVerification = consumerAddMoneyIndexPage.enterCardDetails();

                testVerifyLog("Verify Pay button is disabled without entering the amount.");
                if (consumerAddMoneyVerification.verifyPayButtonDisabled()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                if (!isPositiveExecution) {
                    consumerAddMoneyVerification = consumerAddMoneyIndexPage.enterMaxTransactionAmount();

                    testVerifyLog("Verify Pay button is disabled without entering the amount.");
                    if (consumerAddMoneyVerification.verifyPayButtonDisabled()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }
                }

                consumerAddMoneyVerification = consumerAddMoneyIndexPage.enterValidTransactionAmount();

                consumerAddMoneyVerification = consumerAddMoneyIndexPage.clickOnAddMoneyPayButton();

                testVerifyLog("Verify Success screen display after successful transaction.");
                if (consumerAddMoneyVerification.verifySuccessAddMoney()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                consumerDashboardVerification = consumerDashboardIndexPage.clickOnBalance();

                testVerifyLog("Verify Balance Updated with recent added amount.");
                if (consumerAddMoneyVerification.verifyBalanceUpdated()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

            } else {
                testWarningLog("Maximum debit card limit reached, can't add new debit card, Please remove any and run again.");
            }
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void consumer_addMoneyFromCreditCard() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_132 :: To verify Consumer can add money via Credit Card successfully.");
        consumerLoginIndexPage.getVersion();

        consumerLoginVerification = consumerLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (consumerLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerDashboardVerification = consumerDashboardIndexPage.clickOnBalance();

        consumerAddMoneyVerification = consumerAddMoneyIndexPage.getCurrentAccountBalance();

        consumerAddMoneyVerification = consumerAddMoneyIndexPage.clickOnAddMoney();

        testVerifyLog("Verify user can see the Add Money screen with details.");
        if (consumerAddMoneyVerification.verifyAddMoneyScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (consumerAddMoneyIndexPage.isTestCreditCardDisplay()) {

            consumerAddMoneyVerification = consumerAddMoneyIndexPage.openTestCreditCard();

            testVerifyLog("Verify user can see the test card on the screen.");
            if (consumerAddMoneyVerification.verifyTestCreditCardDetailsScreen()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerAddMoneyVerification = consumerAddMoneyIndexPage.enterCardDetails();

            testVerifyLog("Verify Pay button is disabled without entering the amount.");
            if (consumerAddMoneyVerification.verifyPayButtonDisabled()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            if (!isPositiveExecution) {
                consumerAddMoneyVerification = consumerAddMoneyIndexPage.enterMaxTransactionAmount();

                testVerifyLog("Verify Pay button is disabled without entering the amount.");
                if (consumerAddMoneyVerification.verifyPayButtonDisabled()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            consumerAddMoneyVerification = consumerAddMoneyIndexPage.enterValidTransactionAmount();

            consumerAddMoneyVerification = consumerAddMoneyIndexPage.clickOnAddMoneyPayButton();

            testVerifyLog("Verify Success screen display after successful transaction.");
            if (consumerAddMoneyVerification.verifySuccessAddMoney()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerDashboardVerification = consumerDashboardIndexPage.clickOnBalance();

            testVerifyLog("Verify Balance Updated with recent added amount.");
            if (consumerAddMoneyVerification.verifyBalanceUpdated()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

        } else {
            testVerifyLog("No Test Card Display, adding a test card and running scenario.");
            if (!consumerAddMoneyIndexPage.isMaxCreditCardsAdded()) {

                consumerAddMoneyVerification = consumerAddMoneyIndexPage.clickOnAddCreditCardButton();

                consumerAddMoneyVerification = consumerAddMoneyIndexPage.enterTestCardDetails();

                consumerAddMoneyVerification = consumerAddMoneyIndexPage.clickOnSaveButton();

                testVerifyLog("Verify user can see the confirmation message for added card");
                if (consumerAddMoneyVerification.verifyCardAddedConfirmationMessage()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                consumerAddMoneyVerification = consumerAddMoneyIndexPage.openTestCreditCard();

                testVerifyLog("Verify user can see the test card on the screen.");
                if (consumerAddMoneyVerification.verifyTestCreditCardDetailsScreen()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                consumerAddMoneyVerification = consumerAddMoneyIndexPage.enterCardDetails();

                testVerifyLog("Verify Pay button is disabled without entering the amount.");
                if (consumerAddMoneyVerification.verifyPayButtonDisabled()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                if (!isPositiveExecution) {
                    consumerAddMoneyVerification = consumerAddMoneyIndexPage.enterMaxTransactionAmount();

                    testVerifyLog("Verify Pay button is disabled without entering the amount.");
                    if (consumerAddMoneyVerification.verifyPayButtonDisabled()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }
                }

                consumerAddMoneyVerification = consumerAddMoneyIndexPage.enterValidTransactionAmount();

                consumerAddMoneyVerification = consumerAddMoneyIndexPage.clickOnAddMoneyPayButton();

                testVerifyLog("Verify Success screen display after successful transaction.");
                if (consumerAddMoneyVerification.verifySuccessAddMoney()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                consumerDashboardVerification = consumerDashboardIndexPage.clickOnBalance();

                testVerifyLog("Verify Balance Updated with recent added amount.");
                if (consumerAddMoneyVerification.verifyBalanceUpdated()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

            } else {
                testWarningLog("Maximum credit card limit reached, can't add new credit card, Please remove any and run again.");
            }
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void consumer_sortAndSearchBankDetails() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_134 :: To verify Consumer can sort and search Bank Accounts.");
        consumerLoginIndexPage.getVersion();

        consumerLoginVerification = consumerLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (consumerLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerAddMoneyVerification = consumerAddMoneyIndexPage.clickOnAddMoney();

        testVerifyLog("Verify user can see the Add Money screen with details.");
        if (consumerAddMoneyVerification.verifyAddMoneyScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerAddMoneyVerification = consumerAddMoneyIndexPage.clickOnSelectBankButton();

        testVerifyLog("Verify user can see Bank Popup screen.");
        if (consumerAddMoneyVerification.verifySelectBankPopUpDisplay()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (consumerAddMoneyIndexPage.isBankListDisplay()) {

            consumerAddMoneyVerification = consumerAddMoneyIndexPage.clickOnBankNameToSort();

            testVerifyLog("Verify Bank Name are sorted successfully.");
            if (consumerAddMoneyVerification.verifyBankNameSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerAddMoneyVerification = consumerAddMoneyIndexPage.enterBankSearchCriteria(false);

            testVerifyLog("Verify user can search details successfully.");
            if (consumerAddMoneyVerification.verifyTableSearchSuccessful()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            if (!isPositiveExecution) {
                consumerAddMoneyVerification = consumerAddMoneyIndexPage.enterBankSearchCriteria(true);

                testVerifyLog("Verify validation message for the invalid search details.");
                if (consumerAddMoneyVerification.verifyTableValidationForInvalidSearch()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

        } else {
            testWarningLog("No Bank Details found for the Consumer");
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");

    }

    @Test
    public void consumer_addMoneyByNetBanking() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_135 :: To verify Consumer can add money via net banking.");
        consumerLoginIndexPage.getVersion();

        consumerLoginVerification = consumerLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (consumerLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerAddMoneyVerification = consumerAddMoneyIndexPage.clickOnAddMoney();

        testVerifyLog("Verify user can see the Add Money screen with details.");
        if (consumerAddMoneyVerification.verifyAddMoneyScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerAddMoneyVerification = consumerAddMoneyIndexPage.clickOnSelectBankButton();

        testVerifyLog("Verify user can see Bank Popup screen.");
        if (consumerAddMoneyVerification.verifySelectBankPopUpDisplay()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (consumerAddMoneyIndexPage.isBankListDisplay()) {

            consumerAddMoneyVerification = consumerAddMoneyIndexPage.selectBankFromTheList();

            testVerifyLog("Verify selected Bank display on the screen.");
            if (consumerAddMoneyVerification.verifySelectedBankInScreen()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            testVerifyLog("Verify Pay button is disabled without entering the amount.");
            if (consumerAddMoneyVerification.verifyProceedButtonDisabled()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            if (!isPositiveExecution) {
                consumerAddMoneyVerification = consumerAddMoneyIndexPage.enterMaxTransactionAmount();

                testVerifyLog("Verify Pay button is disabled without entering the amount.");
                if (consumerAddMoneyVerification.verifyProceedButtonDisabled()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            consumerAddMoneyVerification = consumerAddMoneyIndexPage.enterValidTransactionAmount();

            consumerAddMoneyVerification = consumerAddMoneyIndexPage.clickOnCancelButton();

            consumerAddMoneyVerification = consumerAddMoneyIndexPage.clickOnDeleteCardNoButton();

            consumerAddMoneyVerification = consumerAddMoneyIndexPage.clickOnCancelButton();

            consumerAddMoneyVerification = consumerAddMoneyIndexPage.clickOnDeleteCardYesButton();

            testVerifyLog("Verify user can cancel NetBanking Transaction.");
            if (consumerAddMoneyVerification.verifyNetBankingScreenNotDisplay()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerAddMoneyVerification = consumerAddMoneyIndexPage.clickOnSelectBankButton();

            consumerAddMoneyVerification = consumerAddMoneyIndexPage.selectBankFromTheList();

            testVerifyLog("Verify selected Bank display on the screen.");
            if (consumerAddMoneyVerification.verifySelectedBankInScreen()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerAddMoneyVerification = consumerAddMoneyIndexPage.enterValidTransactionAmount();

            consumerAddMoneyVerification = consumerAddMoneyIndexPage.clickOnProceedButton();

            testVerifyLog("Verify user redirects to the Net Banking screen");
            if (consumerAddMoneyVerification.verifyUserRedirectsToNetBanking()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

        } else {
            testWarningLog("No Bank Details found for the Consumer");
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");

    }

}
