package com.incashme.consumer.index;

import com.framework.init.SeleniumInit;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ConsumerRequestIndex extends SeleniumInit {

    @Test
    public void consumer_PayInvoiceConsumer() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_120 :: To verify Consumer can pay for Invoices.");
        consumerLoginIndexPage.getVersion();

        consumerLoginVerification = consumerLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (consumerLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerDashboardVerification = consumerDashboardIndexPage.clickOnBalance();

        consumerRequestVerification = consumerRequestIndexPage.getRequestReceiverBalance();

        consumerRequestVerification = consumerRequestIndexPage.clickOnRequests();

        testVerifyLog("Verify request screen display properly.");
        if (consumerRequestVerification.verifyRequestScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (consumerRequestIndexPage.isInvoiceRequestAvailable()) {

            consumerRequestVerification = consumerRequestIndexPage.getInvoiceDetailsFromCard();

            consumerRequestVerification = consumerRequestIndexPage.openInvoiceRequest();

            testVerifyLog("Verify recent invoice details in the screen.");
            if (consumerRequestVerification.verifyInvoiceScreenDisplay()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerRequestVerification = consumerRequestIndexPage.clickOnPayButton();

            testVerifyLog("Verify invoice details in the Pay screen.");
            if (consumerRequestVerification.verifyPayInvoiceConfirmationDetails()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerRequestVerification = consumerRequestIndexPage.clickOnConfirmButton();

            testVerifyLog("Verify invoice details in the pay invoice completed pop up.");
            if (consumerRequestVerification.verifyInvoicePayCompletedPopUp()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerRequestVerification = consumerRequestIndexPage.clickOnBackToHomePaid();

            testVerifyLog("Verify completed transaction display in the Dashboard Transaction screen.");
            if (consumerDashboardVerification.verifyConsumerInvoiceTransaction()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerDashboardVerification = consumerDashboardIndexPage.clickOnBalance();

            testVerifyLog("Verify Merchant balance is deducted for the paid invoice amount.");
            if (consumerDashboardVerification.verifyBalanceDeductedForInvoice()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        } else {
            testWarningLog("No invoices found for the user, try to create the invoices for " + username + ", and " +
                    "rerun the test again.");
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void consumer_CancelInvoiceConsumer() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_121 :: To verify Consumer can cancel the Invoice.");
        consumerLoginIndexPage.getVersion();

        consumerLoginVerification = consumerLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (consumerLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerRequestVerification = consumerRequestIndexPage.clickOnRequests();

        testVerifyLog("Verify request screen display properly.");
        if (consumerRequestVerification.verifyRequestScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (consumerRequestIndexPage.isInvoiceRequestAvailable()) {

            consumerRequestVerification = consumerRequestIndexPage.getInvoiceDetailsFromCard();

            consumerRequestVerification = consumerRequestIndexPage.openInvoiceRequest();

            testVerifyLog("Verify recent invoice details in the screen.");
            if (consumerRequestVerification.verifyInvoiceScreenDisplay()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerRequestVerification = consumerRequestIndexPage.clickOnCancelRequest();

            consumerRequestVerification = consumerRequestIndexPage.clickOnCancelCancelRequest();

            consumerRequestVerification = consumerRequestIndexPage.clickOnCancelRequest();

            consumerRequestVerification = consumerRequestIndexPage.clickOnYesCancelRequest();

            testVerifyLog("Verify invoice cancelled successfully.");
            if (consumerRequestVerification.verifyInvoiceCancelled()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            testVerifyLog("Verify cancelled invoice not display in cards.");
            if (consumerRequestVerification.verifyCancelledInvoiceInCards()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

        } else {
            testWarningLog("No invoices found for the user, try to create the invoices for " + username + ", and " +
                    "rerun the test again.");
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void consumer_CancelDonationConsumer() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_122 :: To verify Consumer can cancel the Donation.");
        consumerLoginIndexPage.getVersion();

        consumerLoginVerification = consumerLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (consumerLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerRequestVerification = consumerRequestIndexPage.clickOnRequests();

        testVerifyLog("Verify request screen display properly.");
        if (consumerRequestVerification.verifyRequestScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (consumerRequestIndexPage.isDonationRequestAvailable()) {

            consumerRequestVerification = consumerRequestIndexPage.getDonationDetailsFromCard();

            consumerRequestVerification = consumerRequestIndexPage.openDonationRequest();

            testVerifyLog("Verify recent donation details in the screen.");
            if (consumerRequestVerification.verifyDonationScreenDisplay()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerRequestVerification = consumerRequestIndexPage.clickOnCancelRequest();

            consumerRequestVerification = consumerRequestIndexPage.clickOnCancelCancelRequest();

            consumerRequestVerification = consumerRequestIndexPage.clickOnCancelRequest();

            consumerRequestVerification = consumerRequestIndexPage.clickOnYesCancelRequest();

            testVerifyLog("Verify donation cancelled successfully.");
            if (consumerRequestVerification.verifyDonationCancelled()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            testVerifyLog("Verify cancelled donation not display in cards.");
            if (consumerRequestVerification.verifyCancelledDonationInCards()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

        } else {
            testWarningLog("No invoices found for the user, try to create the invoices for " + username + ", and " +
                    "rerun the test again.");
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void consumer_PayDonationConsumer() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_123 :: To verify Consumer can pay for donation.");
        consumerLoginIndexPage.getVersion();

        consumerLoginVerification = consumerLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (consumerLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerDashboardVerification = consumerDashboardIndexPage.clickOnBalance();

        consumerRequestVerification = consumerRequestIndexPage.getRequestReceiverBalance();

        consumerRequestVerification = consumerRequestIndexPage.clickOnRequests();

        testVerifyLog("Verify request screen display properly.");
        if (consumerRequestVerification.verifyRequestScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (consumerRequestIndexPage.isDonationRequestAvailable()) {

            consumerRequestVerification = consumerRequestIndexPage.getDonationDetailsFromCard();

            consumerRequestVerification = consumerRequestIndexPage.openDonationRequest();

            testVerifyLog("Verify recent donation details in the screen.");
            if (consumerRequestVerification.verifyDonationScreenDisplay()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerRequestVerification = consumerRequestIndexPage.enterValidAmount();

            consumerRequestVerification = consumerRequestIndexPage.clickOnDonateButton();

            testVerifyLog("Verify donation details in the Pay screen.");
            if (consumerRequestVerification.verifyPayDonationConfirmationDetails()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerRequestVerification = consumerRequestIndexPage.clickOnConfirmButton();

            testVerifyLog("Verify donation details in the pay donation completed pop up.");
            if (consumerRequestVerification.verifyDonationPayCompletedPopUp()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerRequestVerification = consumerRequestIndexPage.clickOnBackToHomePaid();

            testVerifyLog("Verify completed transaction display in the Dashboard Transaction screen.");
            if (consumerDashboardVerification.verifyConsumerDonationTransaction()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerDashboardVerification = consumerDashboardIndexPage.clickOnBalance();

            testVerifyLog("Verify Consumer balance is deducted for the paid donation amount.");
            if (consumerDashboardVerification.verifyBalanceDeductedForPaidDonation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        } else {
            testWarningLog("No donations found for the user, try to create the donations for " + username + ", and" +
                    "rerun the test again.");
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

}
