package com.incashme.consumer.index;

import com.framework.init.SeleniumInit;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ConsumerStatementIndex extends SeleniumInit {

    @Test
    public void consumer_StatementDownload() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_128 :: To verify Consumer can download the Monthly Statement.");
        consumerLoginIndexPage.getVersion();

        consumerLoginVerification = consumerLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (consumerLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        createDownloadDirectory();

        consumerDashboardVerification = consumerDashboardIndexPage.expandDashboardMenu();
        consumerDashboardVerification = consumerDashboardIndexPage.clickOnUserDetails();

        consumerStatementVerification = consumerStatementIndexPage.getProfileCreatedMonth();

        consumerDashboardVerification = consumerDashboardIndexPage.clickOnHome();

        consumerStatementVerification = consumerStatementIndexPage.clickOnStatementMenu();

        testVerifyLog("Verify Monthly statements display since user started the account.");
        if (consumerStatementVerification.verifyStatementDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (consumerStatementIndexPage.isStatementDisplay()) {

            consumerStatementVerification = consumerStatementIndexPage.clickOnViewButton();

            testVerifyLog("Verify Statement downloaded successfully.");
            if (consumerStatementVerification.verifyStatementDownloaded()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");

    }

    @Test
    public void consumer_StatementFilter() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_129 :: To verify Consumer can filter the Statements.");
        consumerLoginIndexPage.getVersion();

        consumerLoginVerification = consumerLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (consumerLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerStatementVerification = consumerStatementIndexPage.clickOnStatementMenu();

        testVerifyLog("Verify user can see the Statement Screen with list of statements.");
        if (consumerStatementVerification.verifyStatementsScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (consumerStatementIndexPage.isStatementDisplay()) {

            if (!isPositiveExecution) {
                consumerStatementVerification = consumerStatementIndexPage.selectStatementToFilter(true);

                testVerifyLog("Verify No Statement display for current month.");
                if (consumerStatementVerification.verifyNoStatementDisplay()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            consumerStatementVerification = consumerStatementIndexPage.clickOnFilterButton();

            consumerStatementVerification = consumerStatementIndexPage.selectStatementToFilter(false);

            testVerifyLog("Verify Statement filtered successfully.");
            if (consumerStatementVerification.verifyStatementFilterSuccessfully()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");

    }

}
