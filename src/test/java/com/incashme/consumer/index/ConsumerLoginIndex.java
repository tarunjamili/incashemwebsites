package com.incashme.consumer.index;

import com.framework.init.SeleniumInit;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Rahul R.
 * Date: 2019-07-08
 * Time
 * Project Name: InCashMe
 */

public class ConsumerLoginIndex extends SeleniumInit {

    @Test
    public void consumer_LoginLogout() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_109 :: To verify Consumer can Login and Logout with the valid email credentials.");
        consumerLoginIndexPage.getVersion();

        if (!isPositiveExecution) {
            consumerLoginVerification = consumerLoginIndexPage.invalidLoginAs(getInvalidEmail(), getRandomPassword());

            testVerifyLog("Verify validation message for the invalid email address.");
            if (consumerLoginVerification.verifyInvalidEmailValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerLoginVerification = consumerLoginIndexPage.loginAs(getUnRegisteredEmail(), getRandomPassword());

            testVerifyLog("Verify validation message for the blank credentials.");
            if (consumerLoginVerification.verifyUnregisteredEmailValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        consumerLoginVerification = consumerLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (consumerLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerLoginVerification = consumerLoginIndexPage.clickOnLogout();

        testVerifyLog("Verify user logout successfully from the Application.");
        if (consumerLoginVerification.verifyUserLogoutSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerLoginVerification = consumerLoginIndexPage.clickOnPrivacyPolicy();

        testVerifyLog("Verify user can see the Privacy Policy screen.");
        if (consumerLoginVerification.verifyPrivacyPolicyScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerLoginVerification = consumerLoginIndexPage.clickOnTermsCondition();

        testVerifyLog("Verify user can see the Terms & Condition screen.");
        if (consumerLoginVerification.verifyTnCScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");

    }

    //Need Mailinator account with active mobile number
    @Test(enabled = false)
    public void consumer_ForgotPassword() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_110 :: To verify Forgot Password functionality.");
        consumerLoginIndexPage.getVersion();

        consumerLoginVerification = consumerLoginIndexPage.clickOnForgotPassword();

        consumerLoginVerification = consumerLoginIndexPage.clickOnBackToLogin();

        consumerLoginVerification = consumerLoginIndexPage.clickOnForgotPassword();

        testVerifyLog("Verify user can see the Forgot Password screen.");
        if (consumerLoginVerification.verifyForgotPasswordScreenDisplay()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerLoginVerification = consumerLoginIndexPage.forgotPasswordAs(getInvalidEmail());

        testVerifyLog("Verify validation message for the invalid email address.");
        if (consumerLoginVerification.verifyInvalidEmailValidation()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerLoginVerification = consumerLoginIndexPage.forgotPasswordAs(username);
        consumerLoginVerification = consumerLoginIndexPage.clickOnSubmitButton();

        testVerifyLog("Verify OTP Screen display for entering the OTP.");
        if (consumerLoginVerification.verifyStep1OTPScreenDisplay()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerLoginVerification = consumerLoginIndexPage.enterOTP(getRandomNumber() + "");
        consumerLoginVerification = consumerLoginIndexPage.clickOnSubmitButton();

        testVerifyLog("Verify invalid entered OTP validation message.");
        if (consumerLoginVerification.verifyInvalidOTPValidation()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerLoginVerification = consumerLoginIndexPage.waitAndClickResendButton();

        testVerifyLog("Verify OTP Screen display for entering the OTP.");
        if (consumerLoginVerification.verifyStep1OTPScreenDisplay()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerLoginVerification = consumerLoginIndexPage.enterStep1OTP();
        consumerLoginVerification = consumerLoginIndexPage.clickOnSubmitButton();

        testVerifyLog("Verify OTP Screen display after entering Step 1 OTP.");
        if (consumerLoginVerification.verifyStep2OTPScreenDisplay()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerLoginVerification = consumerLoginIndexPage.openResetPasswordLink(username);

        testVerifyLog("Verify Reset Password screen display.");
        if (consumerLoginVerification.verifyResetPasswordScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerLoginVerification = consumerLoginIndexPage.enterOTP(getRandomNumber() + "1");

        testVerifyLog("Verify Invalid OTP digits message.");
        if (consumerLoginVerification.verifyInvalidOTPDigitMessage()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerLoginVerification = consumerLoginIndexPage.enterStep2OTP();

        consumerLoginVerification = consumerLoginIndexPage.changeInvalidNewPassword();

        testVerifyLog("Verify validation message for the invalid new password.");
        if (consumerLoginVerification.verifyInvalidNewPasswordValidation()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerLoginVerification = consumerLoginIndexPage.changeAsPastPassword();

        testVerifyLog("Verify validation message for the past password as new password.");
        if (consumerLoginVerification.verifyPastPasswordValidation()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerLoginVerification = consumerLoginIndexPage.changeConsumerNewPassword();

        testVerifyLog("Verify confirmation message for the Password changed successfully.");
        if (consumerLoginVerification.verifyPasswordChangeSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerLoginVerification = consumerLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (consumerLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void consumer_NonKYCLogin() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_108 :: To verify Non KYC Consumer tries to access the application with Login and Forgot Password.");
        consumerLoginIndexPage.getVersion();

        consumerLoginVerification = consumerLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can see the KYC Validation message for the non kyc users.");
        if (consumerLoginVerification.verifyLoginKYCValidationMessage()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerLoginVerification = consumerLoginIndexPage.clickOnForgotPassword();

        testVerifyLog("Verify user can see the Forgot Password screen.");
        if (consumerLoginVerification.verifyForgotPasswordScreenDisplay()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerLoginVerification = consumerLoginIndexPage.forgotPasswordAs(username);
        consumerLoginVerification = consumerLoginIndexPage.clickOnSubmitButton();

        testVerifyLog("Verify user can see the KYC Validation message for the non kyc users.");
        if (consumerLoginVerification.verifyForgotPasswordKYCValidationMessage()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }
}