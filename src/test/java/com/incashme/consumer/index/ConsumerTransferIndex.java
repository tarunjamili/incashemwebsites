package com.incashme.consumer.index;

import com.framework.init.SeleniumInit;
import com.incashme.consumer.indexpage.ConsumerTransferIndexPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ConsumerTransferIndex extends SeleniumInit {

    @Test
    public void consumer_NonKYCTransfer() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_115 :: To verify Consumer try to send money to Non KYC user.");
        consumerLoginIndexPage.getVersion();

        consumerLoginVerification = consumerLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (consumerLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerTransferVerification = consumerTransferIndexPage.clickOnNewTransaction();

        testVerifyLog("Verify user can see the New Transaction screen properly..");
        if (consumerTransferVerification.verifyNewTransactionScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerTransferVerification = consumerTransferIndexPage.enterNonKYCUserNumber();

        if (consumerTransferIndexPage.isNonKYCUserDisplay()) {

            consumerTransferVerification = consumerTransferIndexPage.selectNonKYCUser();

            testVerifyLog("Verify user can see the validation message for the Non KYC user transfer.");
            if (consumerTransferVerification.verifyValidationForNonKYCUserTransfer()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        } else {
            testWarningLog("No Non KYC user found, please reenter new user and rerun the scenario again.");
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void consumer_consumerTransfer() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_116 :: To verify Consumer can send money to other consumer.");
        consumerLoginIndexPage.getVersion();

        consumerLoginVerification = consumerLoginIndexPage.loginAs(externalEmail, externalPassword);

        consumerDashboardVerification = consumerDashboardIndexPage.clickOnBalance();

        consumerTransferVerification = consumerTransferIndexPage.getTransferReceiverBalance();

        consumerLoginVerification = consumerLoginIndexPage.clickOnLogout();

        consumerLoginVerification = consumerLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (consumerLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerDashboardVerification = consumerDashboardIndexPage.clickOnBalance();

        consumerTransferVerification = consumerTransferIndexPage.getTransferSenderBalance();

        consumerTransferVerification = consumerTransferIndexPage.clickOnNewTransaction();

        testVerifyLog("Verify user can see the New Transaction screen properly..");
        if (consumerTransferVerification.verifyNewTransactionScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerTransferVerification = consumerTransferIndexPage.enterNonKYCUserNumber();

        if (consumerTransferIndexPage.isUsersDisplay()) {

            consumerTransferVerification = consumerTransferIndexPage.selectValidConsumerUser();

            if (!ConsumerTransferIndexPage._noConsumer) {

                testVerifyLog("Verify user can see transfer screen to enter data.");
                if (consumerTransferVerification.verifyUserTransferScreen()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                if (!isPositiveExecution) {
                    consumerTransferVerification = consumerTransferIndexPage.enterZeroAmountAsTransfer();

                    testVerifyLog("Verify send button is disabled for the invalid amount.");
                    if (consumerTransferVerification.verifySendButtonIsDisabled()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }

                    consumerTransferVerification = consumerTransferIndexPage.enterMaxAmountAsTransfer();

                    testVerifyLog("Verify send button is disabled for the invalid amount.");
                    if (consumerTransferVerification.verifySendButtonIsDisabled()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }

                    if (consumerTransferIndexPage.isBalanceLessThanAccepted()) {

                        consumerTransferVerification = consumerTransferIndexPage.enterMaxBalanceAsTransfer();

                        consumerTransferVerification = consumerTransferIndexPage.clickOnSendButton();

                        testVerifyLog("Verify validation message for the insufficient fund in account.");
                        if (consumerTransferVerification.verifyInsufficientFundValidation()) {
                            stepPassed();
                        } else {
                            stepFailure(driver);
                            numOfFailedSteps++;
                        }

                    }
                }

                consumerTransferVerification = consumerTransferIndexPage.enterValidAmount();

                consumerTransferVerification = consumerTransferIndexPage.enterMessageForTransfer();

                consumerTransferVerification = consumerTransferIndexPage.clickOnSendButton();

                testVerifyLog("Verify user can see successful pop up after amount send.");
                if (consumerTransferVerification.verifyTransferDetailsAfterAmountSend()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                consumerTransferVerification = consumerTransferIndexPage.clickOnBackToHome();

                consumerDashboardVerification = consumerDashboardIndexPage.clickOnLatestMOREButton();

                testVerifyLog("Verify user can see transaction details in popup properly.");
                if (consumerDashboardVerification.verifyTransactionDetails()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                consumerDashboardVerification = consumerDashboardIndexPage.clickOnBalance();

                testVerifyLog("Verify amount deducted for the transferred amount.");
                if (consumerDashboardVerification.verifyAmountDeductedForTransfer()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                consumerLoginVerification = consumerLoginIndexPage.clickOnLogout();

                consumerLoginVerification = consumerLoginIndexPage.loginAs(externalEmail, externalPassword);

                consumerDashboardVerification = consumerDashboardIndexPage.clickOnBalance();

                testVerifyLog("Verify amount added for the transferred amount.");
                if (consumerDashboardVerification.verifyAmountAddedForTransfer()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            } else {
                testWarningLog("No Consumer found for the transfer scenario");
            }
        } else {
            testWarningLog("No user found, please reenter registered user and rerun the scenario again.");
        }
        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void consumer_merchantTransfer() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_117 :: To verify Consumer can send money to Merchant from Transaction.");
        consumerLoginIndexPage.getVersion();

        openURL(driver, MERCHANT_URL);

        merchantLoginVerification = merchantLoginIndexPage.loginAs(externalEmail, externalPassword);

        merchantDashboardVerification = merchantDashboardIndexPage.getTransferReceiverBalance();

        merchantLoginVerification = merchantLoginIndexPage.clickOnLogout();

        openURL(driver, CONSUMER_URL);

        consumerLoginVerification = consumerLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (consumerLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerDashboardVerification = consumerDashboardIndexPage.clickOnBalance();

        consumerTransferVerification = consumerTransferIndexPage.getTransferSenderBalance();

        consumerTransferVerification = consumerTransferIndexPage.clickOnNewTransaction();

        testVerifyLog("Verify user can see the New Transaction screen properly..");
        if (consumerTransferVerification.verifyNewTransactionScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerTransferVerification = consumerTransferIndexPage.enterNonKYCUserNumber();

        if (consumerTransferIndexPage.isUsersDisplay()) {

            consumerTransferVerification = consumerTransferIndexPage.selectValidMerchantUser();

            if (!ConsumerTransferIndexPage._noMerchant) {

                testVerifyLog("Verify user can see transfer screen to enter data.");
                if (consumerTransferVerification.verifyUserTransferScreen()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                if (!isPositiveExecution) {
                    consumerTransferVerification = consumerTransferIndexPage.enterZeroAmountAsTransfer();

                    testVerifyLog("Verify send button is disabled for the invalid amount.");
                    if (consumerTransferVerification.verifySendButtonIsDisabled()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }

                    consumerTransferVerification = consumerTransferIndexPage.enterMaxAmountAsTransfer();

                    testVerifyLog("Verify send button is disabled for the invalid amount.");
                    if (consumerTransferVerification.verifySendButtonIsDisabled()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }
                }

                if (consumerTransferIndexPage.isBalanceLessThanAccepted()) {

                    if (!isPositiveExecution) {
                        consumerTransferVerification = consumerTransferIndexPage.enterMaxBalanceAsTransfer();

                        consumerTransferVerification = consumerTransferIndexPage.clickOnSendButton();

                        testVerifyLog("Verify validation message for the insufficient fund in account.");
                        if (consumerTransferVerification.verifyInsufficientFundValidation()) {
                            stepPassed();
                        } else {
                            stepFailure(driver);
                            numOfFailedSteps++;
                        }
                    }

                }

                consumerTransferVerification = consumerTransferIndexPage.enterValidAmount();

                consumerTransferVerification = consumerTransferIndexPage.enterMessageForTransfer();

                consumerTransferVerification = consumerTransferIndexPage.clickOnSendButton();

                testVerifyLog("Verify user can see successful pop up after amount send.");
                if (consumerTransferVerification.verifyTransferDetailsAfterAmountSend()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                consumerTransferVerification = consumerTransferIndexPage.clickOnBackToHome();

                consumerDashboardVerification = consumerDashboardIndexPage.clickOnLatestMOREButton();

                testVerifyLog("Verify user can see transaction details in popup properly.");
                if (consumerDashboardVerification.verifyTransactionDetails()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                consumerDashboardVerification = consumerDashboardIndexPage.clickOnBalance();

                testVerifyLog("Verify amount deducted for the transferred amount.");
                if (consumerDashboardVerification.verifyAmountDeductedForTransfer()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                consumerLoginVerification = consumerLoginIndexPage.clickOnLogout();

                openURL(driver, MERCHANT_URL);

                merchantLoginVerification = merchantLoginIndexPage.loginAs(externalEmail, externalPassword);

                testVerifyLog("Verify amount added for the transferred amount.");
                if (merchantDashboardVerification.verifyAmountAddedForTransfer()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            } else {
                testWarningLog("No Merchant found for the transfer scenario");
            }
        } else {
            testWarningLog("No user found, please reenter registered user and rerun the scenario again.");
        }
        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

}
