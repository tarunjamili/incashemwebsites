package com.incashme.consumer.index;

import com.framework.init.SeleniumInit;
import com.incashme.consumer.indexpage.ConsumerDashboardIndexPage;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Rahul R.
 * Date: 2019-07-08
 * Time
 * Project Name: InCashMe
 */

public class ConsumerDashboardIndex extends SeleniumInit {

    @Test
    public void consumer_DashboardMenuVerification() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_112 :: To verify Dashboard Screen and Menu Links.  <br>" +
                "ICM_SC_119 :: To verify available balance in the Balance screen");
        consumerLoginIndexPage.getVersion();

        consumerLoginVerification = consumerLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (consumerLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify user can see the Dashboard screen with details.");
        if (consumerDashboardVerification.verifyDashboardScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerDashboardVerification = consumerDashboardIndexPage.expandDashboardMenu();
        consumerRequestVerification = consumerRequestIndexPage.clickOnRequests();

        testVerifyLog("Verify user can see the Request screen with details.");
        if (consumerRequestVerification.verifyRequestScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerTransactionHistoryVerification = consumerTransactionHistoryIndexPage.clickOnTransactionHistory();

        testVerifyLog("Verify user can see the Transaction History screen with details.");
        if (consumerTransactionHistoryVerification.verifyTransactionHistoryScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerStatementVerification = consumerStatementIndexPage.clickOnStatementMenu();

        testVerifyLog("Verify user can see the Statement screen with details.");
        if (consumerStatementVerification.verifyStatementsScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerDashboardVerification = consumerDashboardIndexPage.clickOnBalance();

        testVerifyLog("Verify user can see the Balance screen with balance amount.");
        if (consumerDashboardVerification.verifyBalanceScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerAddMoneyVerification = consumerAddMoneyIndexPage.clickOnAddMoney();

        testVerifyLog("Verify user can see the Add Money screen with details.");
        if (consumerAddMoneyVerification.verifyAddMoneyScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerWithdrawMoneyVerification = consumerWithdrawMoneyIndexPage.clickOnWithdrawMoney();

        testVerifyLog("Verify user can see the Withdraw Money screen with details.");
        if (consumerWithdrawMoneyVerification.verifyWithdrawScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerDashboardVerification = consumerDashboardIndexPage.clickOnPrivacyPolicy();

        testVerifyLog("Verify user can see the Privacy Policy screen.");
        if (consumerDashboardVerification.verifyPrivacyPolicyScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerDashboardVerification = consumerDashboardIndexPage.clickOnTnC();

        testVerifyLog("Verify user can see the Terms & Condition screen.");
        if (consumerDashboardVerification.verifyTnCScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerDashboardVerification = consumerDashboardIndexPage.clickOnKYCAML();

        testVerifyLog("Verify user can see the KYC & AML Policy screen.");
        if (consumerDashboardVerification.verifyKYCandAMLScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void consumer_DashboardProfileVerification() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_111 :: To verify Consumer profile details and profile links.");
        consumerLoginIndexPage.getVersion();

        consumerLoginVerification = consumerLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (consumerLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify user can see the Dashboard screen with details.");
        if (consumerDashboardVerification.verifyDashboardScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerDashboardVerification = consumerDashboardIndexPage.expandDashboardMenu();
        consumerDashboardVerification = consumerDashboardIndexPage.clickOnUserDetails();

        testVerifyLog("Verify user can see the Profile screen with details.");
        if (consumerDashboardVerification.verifyProfileScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerDashboardVerification = consumerDashboardIndexPage.clickOnDocuments();

        testVerifyLog("Verify Documents screen open.");
        if (consumerDashboardVerification.verifyDocumentsScreenOpen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerDashboardVerification = consumerDashboardIndexPage.clickOnChangePassword();

        testVerifyLog("Verify Change Password Screen display.");
        if (consumerDashboardVerification.verifyChangePasswordScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void consumer_sortAndSearchTransactionDetails() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_113 :: To verify Consumer can search and sort the Dashboard Transaction history.");
        consumerLoginIndexPage.getVersion();

        consumerLoginVerification = consumerLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (consumerLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (consumerDashboardIndexPage.isTransactionHistoryAvailable()) {

            consumerDashboardVerification = consumerDashboardIndexPage.clickOnUserNameToSort();

            testVerifyLog("Verify User Name are sorted successfully.");
            if (consumerDashboardVerification.verifyUserNameSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerDashboardVerification = consumerDashboardIndexPage.clickOnTransactionIDToSort();

            testVerifyLog("Verify Transaction ID are sorted successfully.");
            if (consumerDashboardVerification.verifyUserTxIDSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerDashboardVerification = consumerDashboardIndexPage.clickOnCategoryToSort();

            testVerifyLog("Verify User Type are sorted successfully.");
            if (consumerDashboardVerification.verifyUserCategorySorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerDashboardVerification = consumerDashboardIndexPage.clickOnTypesToSort();

            testVerifyLog("Verify User Type are sorted successfully.");
            if (consumerDashboardVerification.verifyUserTypeSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerDashboardVerification = consumerDashboardIndexPage.clickOnDateTimeToSort();

            testVerifyLog("Verify Transaction Date & Time are sorted successfully.");
            if (consumerDashboardVerification.verifyTransactionDateTimeSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerDashboardVerification = consumerDashboardIndexPage.enterDashboardSearchCriteria(false);

            testVerifyLog("Verify user can search details successfully.");
            if (consumerDashboardVerification.verifyTableSearchSuccessful()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            if (!isPositiveExecution) {
                consumerDashboardVerification = consumerDashboardIndexPage.enterDashboardSearchCriteria(true);

                testVerifyLog("Verify validation message for the invalid search details.");
                if (consumerDashboardVerification.verifyTableValidationForInvalidSearch()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

        } else {
            testWarningLog("No Transaction History found for the Consumer, try to run with the Consumer " +
                    "who has Transaction History for today.");
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");

    }

    @Test
    public void consumer_transactionHistoryDetails() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_114 :: To verify Consumer can see the details of the Transaction on the home screen.");
        consumerLoginIndexPage.getVersion();

        consumerLoginVerification = consumerLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (consumerLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (consumerDashboardIndexPage.isTransactionHistoryAvailable()) {

            consumerDashboardVerification = consumerDashboardIndexPage.getDashboardTransactionDetails("Merchant");

            if (ConsumerDashboardIndexPage._userNumber >= 0) {
                consumerDashboardVerification = consumerDashboardIndexPage.clickOnMOREButton();

                testVerifyLog("Verify user can see the details properly on the Transaction Details popup.");
                if (consumerDashboardVerification.verifyMerchantTransactionDetailsPopUp()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                consumerDashboardVerification = consumerDashboardIndexPage.clickOnPopUpCloseButton();
            } else {
                testWarningLog("No Merchant Transaction found in the list");
            }

            consumerDashboardVerification = consumerDashboardIndexPage.getDashboardTransactionDetails("Consumer");

            if (ConsumerDashboardIndexPage._userNumber >= 0) {

                consumerDashboardVerification = consumerDashboardIndexPage.clickOnMOREButton();

                testVerifyLog("Verify user can see the details properly on the Transaction Details popup.");
                if (consumerDashboardVerification.verifyConsumerTransactionDetailsPopUp()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            } else {
                testWarningLog("No Consumer Transaction found in the list");
            }

        } else {
            testWarningLog("No Transaction History found for the Consumer, try to run with the Consumer " +
                    "who has Transaction History for today.");
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");

    }

    @Test
    public void consumer_ChangePassword() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_118 :: To verify Consumer can change password successfully.");
        consumerLoginIndexPage.getVersion();

        consumerLoginVerification = consumerLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (consumerLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerDashboardVerification = consumerDashboardIndexPage.expandDashboardMenu();
        consumerDashboardVerification = consumerDashboardIndexPage.clickOnUserDetails();

        testVerifyLog("Verify user can see the Profile screen with details.");
        if (consumerDashboardVerification.verifyProfileScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerDashboardVerification = consumerDashboardIndexPage.clickOnChangePassword();

        testVerifyLog("Verify user can see the Change Password screen.");
        if (consumerDashboardVerification.verifyChangePasswordScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerDashboardVerification = consumerDashboardIndexPage.changeInvalidCurrentPassword();

        testVerifyLog("Verify validation message for the invalid current password.");
        if (consumerDashboardVerification.verifyInvalidCurrentPasswordValidation()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerDashboardVerification = consumerDashboardIndexPage.changeInvalidNewPassword();

        testVerifyLog("Verify validation message for the invalid new password.");
        if (consumerDashboardVerification.verifyInvalidNewPasswordValidation()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerDashboardVerification = consumerDashboardIndexPage.clearDetails();
        consumerDashboardVerification = consumerDashboardIndexPage.changeInvalidConfirmPassword();

        testVerifyLog("Verify validation message for the different new and confirm password.");
        if (consumerDashboardVerification.verifyInvalidConfirmPasswordValidation()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerDashboardVerification = consumerDashboardIndexPage.changeAsPastPassword();

        testVerifyLog("Verify validation message for the past password as new password.");
        if (consumerDashboardVerification.verifyPastPasswordValidation()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerDashboardVerification = consumerDashboardIndexPage.changeInvalidCurrentPassword();

        testVerifyLog("Verify validation message for the invalid current password.");
        if (consumerDashboardVerification.verifyInvalidCurrentPasswordSecondTime()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerDashboardVerification = consumerDashboardIndexPage.changeInvalidCurrentPassword();

        testVerifyLog("Verify user account blocked after three invalid attempts.");
        if (consumerDashboardVerification.verifyAccountIsBlockedForInvalidPassword()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify user logout successfully from the Application.");
        if (consumerLoginVerification.verifyUserLogoutSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerLoginVerification = consumerLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can see the account locked validation.");
        if (consumerLoginVerification.verifyUserLockedSuccessfullyForWrongPassword()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

}