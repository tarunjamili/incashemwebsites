package com.incashme.consumer.index;

import com.framework.init.SeleniumInit;
import com.incashme.consumer.indexpage.ConsumerDashboardIndexPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ConsumerWithdrawMoneyIndex extends SeleniumInit {

    @Test
    public void consumer_AddNewBankAccount() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_136 :: To verify Consumer can add bank account to withdraw money.");
        consumerLoginIndexPage.getVersion();

        consumerLoginVerification = consumerLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (consumerLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerDashboardVerification = consumerDashboardIndexPage.clickOnBalance();

        ConsumerDashboardIndexPage.getCurrentBalance();

        consumerWithdrawMoneyVerification = consumerWithdrawMoneyIndexPage.clickOnWithdrawMoney();

        if (!ConsumerDashboardIndexPage._currentBalance.equalsIgnoreCase("0")) {
            testVerifyLog("Verify user can withdraw screen properly.");
            if (consumerWithdrawMoneyVerification.verifyWithdrawScreen()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            if (!consumerWithdrawMoneyIndexPage.isAllBankAccountAdded()) {

                consumerWithdrawMoneyVerification = consumerWithdrawMoneyIndexPage.clickOnAddBankAccount();

                testVerifyLog("Verify user can add new bank account form screen.");
                if (consumerWithdrawMoneyVerification.verifyAddNewAccountForm()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                consumerWithdrawMoneyVerification = consumerWithdrawMoneyIndexPage.clickOnCloseAddBankForm();

                testVerifyLog("Verify user close add new bank account form screen.");
                if (consumerWithdrawMoneyVerification.verifyCloseBankAccountScreen()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                consumerWithdrawMoneyVerification = consumerWithdrawMoneyIndexPage.clickOnAddBankAccount();

                if (!isPositiveExecution) {
                    consumerWithdrawMoneyVerification = consumerWithdrawMoneyIndexPage.enterDisplayName(true);

                    testVerifyLog("Verify validation message for invalid display name.");
                    if (consumerWithdrawMoneyVerification.verifyInvalidDisplayNameValidation()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }
                }

                consumerWithdrawMoneyVerification = consumerWithdrawMoneyIndexPage.enterDisplayName(false);

                if (!isPositiveExecution) {
                    consumerWithdrawMoneyVerification = consumerWithdrawMoneyIndexPage.enterAccountHolderName(true);

                    testVerifyLog("Verify validation message for invalid account holder name.");
                    if (consumerWithdrawMoneyVerification.verifyInvalidHolderNameNameValidation()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }
                }

                consumerWithdrawMoneyVerification = consumerWithdrawMoneyIndexPage.enterAccountHolderName(false);

                consumerWithdrawMoneyVerification = consumerWithdrawMoneyIndexPage.selectAccountType();

                consumerWithdrawMoneyVerification = consumerWithdrawMoneyIndexPage.enterAccountNumberWithSpace();

                testVerifyLog("Verify validation message for invalid account account number.");
                if (consumerWithdrawMoneyVerification.verifyInvalidAccountNumberValidation()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                if (!isPositiveExecution) {
                    consumerWithdrawMoneyVerification = consumerWithdrawMoneyIndexPage.enterAccountNumber(true);

                    testVerifyLog("Verify validation message for invalid account account number.");
                    if (consumerWithdrawMoneyVerification.verifyInvalidAccountNumberValidation()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }
                }

                consumerWithdrawMoneyVerification = consumerWithdrawMoneyIndexPage.enterAccountNumber(false);

                if (!isPositiveExecution) {
                    consumerWithdrawMoneyVerification = consumerWithdrawMoneyIndexPage.enterIFSCCode(true);

                    testVerifyLog("Verify validation message for invalid IFSC Code.");
                    if (consumerWithdrawMoneyVerification.verifyInvalidIFSCNumberValidation()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }
                }

                consumerWithdrawMoneyVerification = consumerWithdrawMoneyIndexPage.enterIFSCCode(false);

                consumerWithdrawMoneyVerification = consumerWithdrawMoneyIndexPage.clickOnAddNewBankAccount();

                consumerWithdrawMoneyVerification = consumerWithdrawMoneyIndexPage.clickOKAddBankAccountButton();

                testVerifyLog("Verify confirmation message for the Bank Account added.");
                if (consumerWithdrawMoneyVerification.verifyConfirmationForBankAccount()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                testVerifyLog("Verify Added Bank Account details display in Money Source.");
                if (consumerWithdrawMoneyVerification.verifyBankAccountDetails()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            } else {
                testWarningLog("Failed to add a new bank account, currently max bank account added, either remove one or try to run the scenario with different account.");
            }
        } else {
            testWarningLog("Insufficient Balance : " + ConsumerDashboardIndexPage._currentBalance + " , please add balance to withdraw money.");
            testVerifyLog("Verify user can withdraw screen properly.");
            if (consumerWithdrawMoneyVerification.verifyWithdrawScreen()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }
        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void consumer_DeleteBankAccount() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_137 :: To verify Consumer can delete bank account from withdraw money.");
        consumerLoginIndexPage.getVersion();

        consumerLoginVerification = consumerLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (consumerLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerDashboardVerification = consumerDashboardIndexPage.clickOnBalance();
        ConsumerDashboardIndexPage.getCurrentBalance();

        consumerWithdrawMoneyVerification = consumerWithdrawMoneyIndexPage.clickOnWithdrawMoney();

        if (!ConsumerDashboardIndexPage._currentBalance.equalsIgnoreCase("0")) {
            testVerifyLog("Verify user can withdraw screen properly.");
            if (consumerWithdrawMoneyVerification.verifyWithdrawScreen()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            if (consumerWithdrawMoneyIndexPage.isBankAccountAvailable()) {

                consumerWithdrawMoneyVerification = consumerWithdrawMoneyIndexPage.clickOnDeleteButton();

                testVerifyLog("Verify validation message for the Delete Bank Account.");
                if (consumerWithdrawMoneyVerification.verifyValidationForDeleteBankAccount()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                consumerWithdrawMoneyVerification = consumerWithdrawMoneyIndexPage.clickCancelAddBankAccountButton();

                consumerWithdrawMoneyVerification = consumerWithdrawMoneyIndexPage.clickOnDeleteButton();

                consumerWithdrawMoneyVerification = consumerWithdrawMoneyIndexPage.clickOnYesButton();

                testVerifyLog("Verify confirmation message for the Bank Account deleted.");
                if (consumerWithdrawMoneyVerification.verifyConfirmationForDeletedBankAccount()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                testVerifyLog("Verify deleted account not display on screen.");
                if (consumerWithdrawMoneyVerification.verifyBankAccountDeletedSuccessfully()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

            } else {
                testWarningLog("Failed to delete bank account, Please add bank account to delete it.");
            }
        } else {
            testWarningLog("Insufficient Balance : " + ConsumerDashboardIndexPage._currentBalance + " , please add balance to withdraw money.");
            testVerifyLog("Verify user can withdraw screen properly.");
            if (consumerWithdrawMoneyVerification.verifyWithdrawScreen()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void consumer_WithdrawMoney() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_138 :: To verify Consumer can withdraw money from the selected account.");
        consumerLoginIndexPage.getVersion();

        consumerLoginVerification = consumerLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (consumerLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerDashboardVerification = consumerDashboardIndexPage.clickOnBalance();
        ConsumerDashboardIndexPage.getCurrentBalance();

        consumerWithdrawMoneyVerification = consumerWithdrawMoneyIndexPage.clickOnWithdrawMoney();

        if (!ConsumerDashboardIndexPage._currentBalance.equalsIgnoreCase("0")) {
            testVerifyLog("Verify user can withdraw screen properly.");
            if (consumerWithdrawMoneyVerification.verifyWithdrawScreen()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            if (consumerWithdrawMoneyIndexPage.isBankAccountAvailable()) {

                consumerWithdrawMoneyVerification = consumerWithdrawMoneyIndexPage.clickOnBankToWithdraw();

                testVerifyLog("Verify validation message for invalid withdraw amount.");
                if (consumerWithdrawMoneyVerification.verifyValidationForInvalidWithdrawAMount()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                if (!isPositiveExecution) {
                    consumerWithdrawMoneyVerification = consumerWithdrawMoneyIndexPage.enterWithdrawAmount(0);

                    consumerWithdrawMoneyVerification = consumerWithdrawMoneyIndexPage.clickOnBankToWithdraw();

                    testVerifyLog("Verify validation message for invalid withdraw amount.");
                    if (consumerWithdrawMoneyVerification.verifyValidationForInvalidWithdrawAMount()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }

                    consumerWithdrawMoneyVerification = consumerWithdrawMoneyIndexPage.enterWithdrawAmount(999999);

                    consumerWithdrawMoneyVerification = consumerWithdrawMoneyIndexPage.clickOnBankToWithdraw();

                    testVerifyLog("Verify validation message for invalid withdraw amount.");
                    if (consumerWithdrawMoneyVerification.verifyValidationForInvalidWithdrawAMount()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }
                }

                consumerWithdrawMoneyVerification = consumerWithdrawMoneyIndexPage.enterWithdrawAmount();

                consumerWithdrawMoneyVerification = consumerWithdrawMoneyIndexPage.clickOnBankToWithdraw();

                consumerWithdrawMoneyVerification = consumerWithdrawMoneyIndexPage.clickOnBackToWithdrawScreen();

                consumerWithdrawMoneyVerification = consumerWithdrawMoneyIndexPage.clickOnCancelWithdrawOK();

                consumerWithdrawMoneyVerification = consumerWithdrawMoneyIndexPage.enterWithdrawAmount();

                consumerWithdrawMoneyVerification = consumerWithdrawMoneyIndexPage.clickOnBankToWithdraw();

                testVerifyLog("Verify withdraw details in the screen.");
                if (consumerWithdrawMoneyVerification.verifyWithdrawDetailsScreen()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                consumerWithdrawMoneyVerification = consumerWithdrawMoneyIndexPage.getWithdrawDetails();

                testVerifyLog("Verify total withdraw amount display properly.");
                if (consumerWithdrawMoneyVerification.verifyTotalWithdrawAmount()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                consumerWithdrawMoneyVerification = consumerWithdrawMoneyIndexPage.clickOnConfirmToWithdraw();

                testVerifyLog("Verify withdraw details in confirmation window.");
                if (consumerWithdrawMoneyVerification.verifyConfirmationWithdrawDetails()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                consumerWithdrawMoneyVerification = consumerWithdrawMoneyIndexPage.clickOnCancelWithdraw();

                consumerWithdrawMoneyVerification = consumerWithdrawMoneyIndexPage.clickOnConfirmToWithdraw();

                consumerWithdrawMoneyVerification = consumerWithdrawMoneyIndexPage.clickOnSendWithdraw();

                testVerifyLog("Verify withdraw details in completed withdraw window.");
                if (consumerWithdrawMoneyVerification.verifyCompletedWithdrawDetails()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                consumerWithdrawMoneyVerification = consumerWithdrawMoneyIndexPage.clickOnBackToHome();

                consumerWithdrawMoneyVerification = consumerWithdrawMoneyIndexPage.clickOnWithdrawMoney();

                testVerifyLog("Verify withdraw amount deducted from the balance.");
                if (consumerWithdrawMoneyVerification.verifyWithdrawDeductedFromBalance()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                consumerDashboardVerification = consumerDashboardIndexPage.clickOnBalance();

                testVerifyLog("Verify withdraw amount deducted from the balance.");
                if (consumerDashboardVerification.verifyBalanceUpdatedInBalanceScreen()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }


            } else {
                testWarningLog("No Bank Accounts found for the Withdraw, please add bank accounts first to withdraw the money.");
            }
        } else {
            testWarningLog("Insufficient Balance : " + ConsumerDashboardIndexPage._currentBalance + " , please add balance to withdraw money.");

            testVerifyLog("Verify user can withdraw screen properly.");
            if (consumerWithdrawMoneyVerification.verifyWithdrawScreen()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }
        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }
}
