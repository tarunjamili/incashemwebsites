package com.incashme.consumer.validations;

public interface TransferValidations {
    String KYC_PENDING = "Kyc Verification of recipient is pending. You cannot send money now.";
    String INSUFFICIENT_FUND = "Insufficient Funds in account.";
}
