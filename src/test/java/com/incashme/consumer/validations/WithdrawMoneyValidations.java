package com.incashme.consumer.validations;

public interface WithdrawMoneyValidations {

    String INVALID_DISPLAY_NAME = "Display Name must be contains letter.";
    String INVALID_ACCOUNT_HOLDER_NAME = "Account Holder Name must be contains letter.";
    String INVALID_BANK_ACCOUNT_NUMBER = "Enter valid bank Account number (Length 9 to 18)";
    String INVALID_BANK_IFSC_CODE = "Enter valid IFSC Code";

}
