package com.incashme.consumer.validations;

public interface AddMoneyValidations {

    String BLANK_CARD_NAME = "The holder name may only contain letters.";
    String INVALID_CARD_NAME = "The holder name field is required.";

    String INVALID_DEBIT_CARD_NUMBER = "The cc num field is required when type is debit_card.";
    String INVALID_DEBIT_CARD_EXP_MONTH = "The cc exp month field is required when type is debit_card.";
    String INVALID_DEBIT_CARD_EXP_YEAR = "The cc exp year field is required when type is debit_card.";

    String INVALID_CREDIT_CARD_NUMBER = "The cc num field is required when type is credit_card.";
    String INVALID_CREDIT_CARD_EXP_MONTH = "The cc exp month field is required when type is credit_card.";
    String INVALID_CREDIT_CARD_EXP_YEAR = "The cc exp year field is required when type is credit_card.";

    String CARD_ADDED_SUCCESSFULLY = "Money Source added";
    String CARD_DELETED_SUCCESSFULLY = "Card successfully deleted.";

    String NO_SEARCH_RECORD_FOUND = "No matching records found";

}
