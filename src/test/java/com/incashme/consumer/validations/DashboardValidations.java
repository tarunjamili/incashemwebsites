package com.incashme.consumer.validations;

/**
 * Created by Rahul R.
 * Date: 2019-07-08
 * Time
 * Project Name: InCashMe
 */

public interface DashboardValidations {

    String NO_SEARCH_RECORD_FOUND = "No matching records found";

    String FIRST_UNSUCCESSFUL_ATTEMPTS = "Unsuccessful attempt 1/3. Account locked temporarily after 3 attempts";
    String SECOND_UNSUCCESSFUL_ATTEMPTS = "Unsuccessful attempt 2/3. Account locked temporarily after 3 attempts";
    String THIRD_UNSUCCESSFUL_ATTEMPTS = "Unsuccessful attempt 3/3. Account locked temporarily for 40 mins.";
    String INVALID_NEW_PASSWORD = "Your Password must contain 1 Capital letter & 1 Small letter & 1 Numerical & 1 Special Character.";
    String NEW_CONFIRM_PASSWORD_NOT_MATCHED = "New Password and Confirm Password does not match.";
    String PAST_PASSWORD_MESSAGE = "New Password cannot be same as the old Password.";
    String SUCCESSFUL_PASSWORD_CHANGE_MESSAGE = "Your password was changed successfully. Please login with the new password.";
}
