package com.incashme.consumer.validations;

/**
 * Created by Rahul R.
 * Date: 2019-04-11
 * Time: 12:15
 * Project Name: InCashMe
 */
public interface TransactionHistoryValidations {

    String CONSUMER_CATEGORY = "Consumer";
    String OWN_WALLET_CATEGORY = "Own wallet";
    String INDIVIDUAL_CATEGORY = "Individual";
    String MERCHANT_CATEGORY = "Merchant";
    String NON_PROFIT_CATEGORY = "Non-Profit";
    String AGENT_CATEGORY = "Agent";

    String NO_SEARCH_RECORD_FOUND = "No matching records found";

    String NO_TRANSACTIONS_HISTORY = "No Transaction History!";


}
