package com.incashme.consumer.validations;

/**
 * Created by Rahul R.
 * Date: 2019-03-25
 * Time: 14:31
 * Project Name: InCashMe
 */
public interface StatementsValidations {

    String NO_STATEMENTS_FOUND = "No Statement!";

}
