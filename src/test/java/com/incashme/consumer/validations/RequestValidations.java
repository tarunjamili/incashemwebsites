package com.incashme.consumer.validations;

public interface RequestValidations {


    String INVOICE_REJECTED_SUCCESSFULLY = "Invoice successfully rejected.";
    String DONATION_REJECTED_SUCCESSFULLY = "Donation successfully rejected.";

}
