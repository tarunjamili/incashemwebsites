package com.incashme.merchant.validations;

/**
 * Created by Rahul R.
 * Date: 2019-04-11
 * Time: 12:15
 * Project Name: InCashMe
 */
public interface TransactionHistoryValidations {

    String NO_SEARCH_RECORD_FOUND = "No matching records found";

    String NO_FILTER_DATA_AVAILABLE = "Sorry we couldn’t find any matches for your search criteria";

    String INVALID_AMOUNT_RANGE = "Amount Range is Invalid!";

    String REFUND_SUCCESSFUL = "Refund Money Successfully.";

    String TYPE_REFUNDED = "Refunded";
    String TYPE_REFUND = "Refund";
    String TYPE_WITHDRAW = "Withdraw";
    String TYPE_DEPOSIT = "Deposit";

    String ADD_BALANCE_IN_ADDITIONAL_DEVICE = "Continue to add balance in additional device";
    String ADDED_BALANCE_IN_ADDITIONAL_DEVICE = "Added balance in additional device";

    String INVALID_ADDITIONAL_DEVICE_REFUND_AMOUNT = "Amount should be, minimum 1 and maximum 100000.";


}
