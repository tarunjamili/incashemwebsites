package com.incashme.merchant.validations;

public interface DashboardValidations {

    String NO_TRANSACTION_HISTORY_FOUND = "Here is no data alive.";

    String INVALID_RECEIVE_AMOUNT_VALIDATION = "Amount should be, minimum 1 and maximum 100000.";

    String NO_SEARCH_RECORD_FOUND = "No matching records found";

    String FIRST_UNSUCCESSFUL_ATTEMPTS = "Unsuccessful attempt 1/3. Account locked temporarily after 3 attempts";
    String SECOND_UNSUCCESSFUL_ATTEMPTS = "Unsuccessful attempt 2/3. Account locked temporarily after 3 attempts";
    String THIRD_UNSUCCESSFUL_ATTEMPTS = "Unsuccessful attempt 3/3. Account locked temporarily for 40 mins.";
    String INVALID_NEW_PASSWORD = "Entered New Password Is Not Valid!";
    String NEW_CONFIRM_PASSWORD_NOT_MATCHED = "New password and Confirm password must be match!";
    String PAST_PASSWORD_MESSAGE = "Cannot use a past password";
    String SUCCESSFUL_PASSWORD_CHANGE_MESSAGE = "Your password was changed successfully. Please login with the new password.";

    String INVALID_NEW_PIN = "New Pin Must Be Positive 4 Digit Number";
    String INVALID_CONFIRM_PIN = "Confirm Pin Must Be Positive 4 Digit Number";
    String NEW_CONFIRM_PIN_NOT_MATCHED = "PIN and confirm new PIN does not match. Please try again.";
    String PAST_PIN_MESSAGE = "New PIN cannot be same as the old PIN.";

    String INVALID_DISPLAY_NAME = "Display Name must be contains letter.";
    String INVALID_ACCOUNT_HOLDER_NAME = "Account Holder Name must be contains letter.";
    String INVALID_BANK_ACCOUNT_NUMBER = "Enter valid bank Account number (Length 9 to 18)";
    String INVALID_BANK_IFSC_CODE = "Enter valid IFSC Code";

}
