package com.incashme.merchant.validations;

public interface InvoiceValidations {

    String NO_INVOICE_HISTORY_FOUND = "Here is no data alive.";

    String NO_SEARCH_RECORD_FOUND = "No matching records found";

    String NO_FILTER_DATA_AVAILABLE = "Sorry we couldn’t find any matches for no search result";

    String INVOICE_PAYMENT_PENDING = "Payment Pending";
    String INVOICE_CANCELLED = "Cancelled";
    String INVOICE_PAYMENT_COMPLETED = "Payment Received";
    String INVOICE_PAYMENT_PAID = "Invoice Paid";

    String INVOICE_PENDING = "Invoice Pending";
    String INVOICE_REQUESTED = "Invoice Requested";

    String INVALID_AMOUNT_RANGE = "Amount Range is Invalid!";

    String NO_RECEIVED_INVOICES = "No any Received invoice.";
    String NO_SENT_INVOICES = "No any Sent invoice.";

    String INVALID_MOBILE_NUMBER = "Enter Mobile Number must be 10 digit number";
    String NO_USERS_FOUND = "No Users Found";
    String INVALID_INVOICE_AMOUNT = "Amount should be, minimum 1 and maximum 100000.";
}
