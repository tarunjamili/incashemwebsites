package com.incashme.merchant.validations;

public interface DonationValidations {

    String NO_SEARCH_RECORD_FOUND = "No matching records found";

    String NO_DONATION_FOUND = "No any Requested Donation.";

    String NO_FILTER_DATA_AVAILABLE = "Sorry we couldn’t find any matches for no search result";

    String DONATION_PENDING = "Pending";
    String DONATION_CANCELLED = "Cancelled";
    String DONATION_COMPLETED = "Donated";
    String DONATION_RECEIVED = "Donation Received";

    String DONATION_PENDING_CARD = "Donation Pending";

    String INVALID_AMOUNT_RANGE = "Amount Range is Invalid!";

    String NO_REQUESTED_DONATION = "No any Requested Donation.";
    String NO_SENT_DONATION = "No any Sent Donation.";

    String INVALID_MOBILE_NUMBER = "Enter Mobile Number must be 10 digit number";
    String NO_USERS_FOUND = "No Users Found";
    String INVALID_DONATION_AMOUNT = "Amount should be, minimum 1 and maximum 100000.";

    String INSUFFICIENT_FUND_MESSAGE = "Insufficient Funds in account.";
}
