package com.incashme.merchant.verification;

import com.framework.init.AbstractPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class MerchantAdditionalDeviceVerification extends AbstractPage {

    public MerchantAdditionalDeviceVerification(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//input[@formcontrolname='profile_fname']")
    private WebElement txtAddDeviceFName;

    @FindBy(xpath = "//input[@formcontrolname='profile_lname']")
    private WebElement txtAddDeviceLName;

    @FindBy(xpath = "//input[@formcontrolname='username']")
    private WebElement txtAddDeviceUsername;

    @FindBy(xpath = "//input[@formcontrolname='cellnum']")
    private WebElement txtAddDeviceCellNumber;

    @FindBy(xpath = "//input[@formcontrolname='create_pin']")
    private WebElement txtAddDeviceCreatePIN;

    @FindBy(xpath = "//input[@formcontrolname='confirm_pin']")
    private WebElement txtAddDeviceConfirmPIN;

    @FindBy(xpath = "//input[@formcontrolname='pospicture']")
    private WebElement txtAddDeviceProfilePicture;

    @FindBy(xpath = "//div[contains(@class,'contentdiv')]//button[contains(@class,'btn-pink') and text()='Reset']")
    private WebElement btnResetForm;

    @FindBy(xpath = "//div[contains(@class,'contentdiv')]//button[contains(@class,'btn-green') and text()='Add']")
    private WebElement btnAddDevice;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@id,'addi_active')]//div[contains(@class,'dntn-name')]")})
    private List<WebElement> lstActiveAdditionalDevices;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@id,'addi_locked')]//div[contains(@class,'dntn-name')]")})
    private List<WebElement> lstLockedAdditionalDevices;

    @FindBy(xpath = "//ul//li/a[contains(text(),'Active')]")
    private WebElement btnActiveTab;

    @FindBy(xpath = "//ul//li/a[contains(text(),'Locked')]")
    private WebElement btnLockedTab;

    public boolean verifyAddDeviceScreen() {

        wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfAllElements(txtAddDeviceFName, txtAddDeviceLName,
                txtAddDeviceConfirmPIN, txtAddDeviceUsername, txtAddDeviceProfilePicture,
                txtAddDeviceCellNumber, txtAddDeviceCreatePIN));

        boolean bool = isElementDisplay(txtAddDeviceFName) && isElementDisplay(txtAddDeviceLName) &&
                isElementDisplay(txtAddDeviceCellNumber) && isElementDisplay(txtAddDeviceConfirmPIN) &&
                isElementDisplay(txtAddDeviceUsername) && isElementDisplay(txtAddDeviceCreatePIN) &&
                isElementDisplay(txtAddDeviceProfilePicture);

        scrollToElement(driver, btnAddDevice);

        bool = bool && isElementDisplay(btnResetForm) && isElementDisplay(btnAddDevice);

        testInfoLog("Total Active Devices", String.valueOf(sizeOf(lstActiveAdditionalDevices)));

        clickOn(driver, btnLockedTab);

        testInfoLog("Total Locked Devices", String.valueOf(sizeOf(lstLockedAdditionalDevices)));

        return bool;
    }
}
