package com.incashme.merchant.verification;

import com.framework.init.AbstractPage;
import com.incashme.merchant.indexpage.MerchantDashboardIndexPage;
import com.incashme.merchant.indexpage.MerchantInvoiceIndexPage;
import com.incashme.merchant.validations.InvoiceValidations;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MerchantInvoiceVerification extends AbstractPage implements InvoiceValidations {

    public MerchantInvoiceVerification(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//table//tr//td[contains(text(),'matching')]")
    private WebElement lblNoRecordsFound;

    @FindBy(xpath = "//input[@type='search']")
    private WebElement txtInputSearch;

    @FindBy(xpath = "//div//p[contains(text(),'no data alive')]")
    private WebElement lblNoInvoiceHistory;

    @FindBy(xpath = "//table[@id='invoice-history-table']")
    private WebElement invoiceHistoryTable;

    @FindBy(xpath = "//li/button[contains(@class,'filterBtn')]")
    private WebElement btnFilter;

    @FindBy(xpath = "//div[contains(@id,'Received')]//a[contains(text(),'New Invoice')]")
    private WebElement btnCreateNewInvoice;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//button[contains(@class,'searchbtn')]")
    private WebElement btnFilterSearch;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//a[contains(text(),'Clear All')]")
    private WebElement btnFilterClearAll;

    @FindAll(value = {@FindBy(xpath = "//table[@id='invoice-history-table']//tr//td[2]")})
    private List<WebElement> lstInvoiceID;

    @FindAll(value = {@FindBy(xpath = "//table[@id='invoice-history-table']//tr//td[4]//div")})
    private List<WebElement> lstInvoiceStatus;

    @FindBy(xpath = "//div//p[contains(text(),'find any matches')]")
    private WebElement lblNoFilterInvoicesAvailable;

    @FindAll(value = {@FindBy(xpath = "//table[@id='invoice-history-table']//tr//td[1]//h5")})
    private List<WebElement> lstUserName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='invoice-history-table']//tr//td[1]//following-sibling::div")})
    private List<WebElement> lstUserIdNumber;

    @FindAll(value = {@FindBy(xpath = "//table[@id='invoice-history-table']//tr//td[3]")})
    private List<WebElement> lstInvoiceAmount;

    @FindAll(value = {@FindBy(xpath = "//table[@id='invoice-history-table']//tr//td[6]//button")})
    private List<WebElement> lstBtnMore;

    @FindBy(xpath = ".//div[contains(@id,'swal2-content') and contains(text(),'Invalid')]")
    private WebElement lblInvalidAmountValidation;

    @FindBy(xpath = ".//div[contains(@class,'swal2-actions')]//button[text()='OK' or text() = 'Ok']")
    private WebElement btnConfirmOK;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@id,'Received')]//ul/li//div[contains(@class,'dntn-id')]")})
    private List<WebElement> lstReceivedInvoicesId;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@id,'Received')]//ul/li//div[contains(@class,'dntn-name')]")})
    private List<WebElement> lstReceivedInvoicesSenderName;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@id,'Received')]//ul/li//div[contains(@class,'dntr-id') or contains(@class,'dntr-no')]")})
    private List<WebElement> lstReceivedInvoicesSenderID;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@id,'Received')]//ul/li//div[contains(@class,'dntr-sts')]")})
    private List<WebElement> lstReceivedInvoicesStatus;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@id,'Received')]//ul/li//div[contains(@class,'dntn-date')]")})
    private List<WebElement> lstReceivedInvoicesDate;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@id,'Sent')]//ul/li//div[contains(@class,'dntn-id')]")})
    private List<WebElement> lstSentInvoicesId;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@id,'Sent')]//ul/li//div[contains(@class,'dntn-name')]")})
    private List<WebElement> lstSentInvoicesSenderName;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@id,'Sent')]//ul/li//div[contains(@class,'dntr-id') or contains(@class,'dntr-no')]")})
    private List<WebElement> lstSentInvoicesSenderID;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@id,'Sent')]//ul/li//div[contains(@class,'dntr-sts')]")})
    private List<WebElement> lstSentInvoicesStatus;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@id,'Sent')]//ul/li//div[contains(@class,'dntn-date')]")})
    private List<WebElement> lstSentInvoicesDate;

    @FindBy(xpath = "//div[contains(@id,'Received')]//ul/li//p")
    private WebElement lblNoReceivedInvoices;

    @FindBy(xpath = "//div[contains(@id,'Sent')]//ul/li//p")
    private WebElement lblNoSentInvoices;

    @FindBy(xpath = "//button[text()='Back']")
    private WebElement btnBack;

    @FindBy(xpath = "//input[@formcontrolname='invoiceUser']")
    private WebElement txtSenderNumber;

    @FindBy(xpath = "//input[@formcontrolname='invoiceAmount']")
    private WebElement txtInvoiceAmount;

    @FindBy(xpath = "//input[@formcontrolname='invoiceMessage']")
    private WebElement txtInvoiceMessage;

    @FindBy(xpath = "//div[contains(@class,'contentdiv')]//button[contains(@class,'btn-pink') and text()='Cancel']")
    private WebElement btnCancelInvoice;

    @FindBy(xpath = "//div[contains(@class,'contentdiv')]//button[contains(@class,'btn-green') and text()='Next']")
    private WebElement btnNextInvoiceCreate;

    @FindBy(xpath = "//form//span[contains(@class,'color-danger') and contains(text(),'Mobile')]")
    private WebElement lblInvalidMobileNumber;

    @FindBy(xpath = "//form//span[contains(@class,'color-danger') and contains(text(),'Amount')]")
    private WebElement lblInvalidAmount;

    @FindBy(xpath = "//div[contains(@id,'pay_merchant')]//div[contains(@class,'tpup-pnk-txt')]")
    private WebElement lblConfirmInvoiceDelete;

    @FindBy(xpath = "//div[contains(@class,'filter-suggetion')]//div[contains(@class,'trans-sub-title')]")
    private WebElement lblNoUsersFound;

    @FindBy(xpath = "//div[contains(@id,'create-confirm')]//div[contains(@class,'invoice_ad_amt')]")
    private WebElement lblInvoiceAmount;

    @FindBy(xpath = "//div[contains(@id,'create-confirm')]//div[contains(@class,'tppup-usr-nm')]")
    private WebElement lblInvoiceUsername;

    @FindBy(xpath = "//div[contains(@id,'create-confirm')]//div[contains(@class,'tppup-usr-nm')]//following-sibling::div")
    private WebElement lblInvoiceMobileNumber;

    @FindBy(xpath = "//div[contains(@class,'row')]//div[contains(text(),'Message')]//following-sibling::div")
    private WebElement lblInvoiceMessage;

    @FindBy(xpath = "//div[contains(@class,'tpup-mnyscs-txt') and contains(text(),'sent success')]")
    private WebElement lblInvoiceSentSuccess;

    @FindBy(xpath = "//div[contains(@class,'tpup-mnyscs-txt') and contains(text(),'Successfully canceled')]")
    private WebElement lblInvoiceDeleteSuccess;

    @FindBy(xpath = "//div[contains(@class,'tpup-mnyscs-txt') and contains(text(),'Successfully Pay')]")
    private WebElement lblInvoicePaySuccess;

    @FindBy(xpath = "//div[contains(@id,'create-confirm')]//div[contains(@class,'time-txt')]")
    private WebElement lblInvoiceSentTime;

    @FindBy(xpath = "//div[contains(@id,'showInvoiceReceived')]//div[contains(@class,'donater-name')]")
    private WebElement lblInvoiceCardUserName;

    @FindBy(xpath = "//div[contains(@id,'showInvoiceReceived')]//div[contains(@class,'donater-id')]")
    private WebElement lblInvoiceCardUserID;

    @FindBy(xpath = "//div[contains(@id,'showInvoiceReceived')]//div[contains(@class,'donater-no')]")
    private List<WebElement> lblInvoiceCardID;

    @FindBy(xpath = "//div[contains(@id,'showInvoiceReceived')]//div[contains(@class,'msg-text')]")
    private WebElement lblInvoiceCardMessage;

    @FindBy(xpath = "//div[contains(@id,'showInvoiceReceived')]//div[contains(@class,'dntn-pdng-txt')]")
    private WebElement lblInvoiceCardInvoiceAmount;

    @FindBy(xpath = "//div[contains(@id,'pay_merchant')]//button[contains(@class,'btn-green') and contains(text(),'Home')]")
    private WebElement btnBackToHomeDeleted;

    @FindBy(xpath = "//div[contains(@id,'pay_merchant')]//div[contains(@class,'amnt-txt')]")
    private WebElement lblInvoiceDeleteAmount;

    @FindBy(xpath = "//div[contains(@id,'pay_merchant')]//div[contains(@class,'amnt-txt')]")
    private WebElement lblInvoicePayAmount;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@id,'pay_merchant')]//div[contains(@class,'tppup-usr-nm')]")})
    private List<WebElement> lblInvoiceDeleteUserName;

    public boolean verifyInvoiceScreen() {

        pause(5);

        if (isElementPresent(lblNoInvoiceHistory)) {

            scrollToElement(driver, lblNoInvoiceHistory);
            testValidationLog(getText(lblNoInvoiceHistory));
            return isElementDisplay(lblNoInvoiceHistory) &&
                    getText(lblNoInvoiceHistory).equalsIgnoreCase(NO_INVOICE_HISTORY_FOUND);

        } else {
            return isElementDisplay(invoiceHistoryTable) && isElementDisplay(btnFilter) && isElementDisplay(btnCreateNewInvoice);
        }

    }

    public boolean verifySearchSuccessful() {

        boolean bool = true;

        for (WebElement e : lstInvoiceID) {
            bool = bool && getInnerText(e).contains(MerchantInvoiceIndexPage._searchCriteria);
        }

        return bool;

    }

    public boolean verifyValidationForInvalidSearch() {

        testValidationLog(getInnerText(lblNoRecordsFound));

        boolean bool = getInnerText(lblNoRecordsFound).equalsIgnoreCase(NO_SEARCH_RECORD_FOUND);

        type(txtInputSearch, "x");
        txtInputSearch.clear();

        return bool;
    }

    public boolean verifyInvoiceFilterScreenDisplay() {

        try {
            updateShowList(driver, findElementByName(driver, "invoice-history-table_length"), "100");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        return isElementDisplay(btnFilterSearch) && isElementDisplay(btnFilterClearAll);
    }

    public boolean verifyRandomDataFiltered() {
        return getText(lblNoFilterInvoicesAvailable).equalsIgnoreCase(NO_FILTER_DATA_AVAILABLE);
    }

    public boolean verifyFirstNameFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement name : lstUserName) {
            finalFilterResult.add(getInnerText(name).split(" ")[0].trim());
        }

        System.out.println(finalFilterResult);
        System.out.println(MerchantInvoiceIndexPage._filterFirstName);

        return finalFilterResult.contains(MerchantInvoiceIndexPage._filterFirstName);
    }

    public boolean verifyLastNameFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement name : lstUserName) {
            finalFilterResult.add(getInnerText(name).replace(MerchantInvoiceIndexPage._filterFirstName, "").trim());
        }

        System.out.println(finalFilterResult);
        System.out.println(MerchantInvoiceIndexPage._filterLastName);

        return finalFilterResult.contains(MerchantInvoiceIndexPage._filterLastName);
    }

    public boolean verifyPhoneNumberFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement number : lstUserIdNumber) {
            finalFilterResult.add(getInnerText(number));
        }

        System.out.println(finalFilterResult);
        System.out.println(MerchantInvoiceIndexPage._filterPhone);

        return finalFilterResult.contains(MerchantInvoiceIndexPage._filterPhone);
    }

    public boolean verifyInvoiceIDFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement number : lstInvoiceID) {
            finalFilterResult.add(String.valueOf(getIntegerFromString(getInnerText(number))));
        }

        System.out.println(finalFilterResult);
        System.out.println(MerchantInvoiceIndexPage._filterInvoiceId);

        return finalFilterResult.contains(MerchantInvoiceIndexPage._filterInvoiceId) &&
                sizeOf(finalFilterResult) == 1;
    }

    public boolean verifyStatusFiltered() {

        List<String> finalFilterResult = new ArrayList<>();
        boolean bool = false;

        pause(2);

        for (WebElement status : lstInvoiceStatus) {
            finalFilterResult.add(getInnerText(status));
        }

        System.out.println(finalFilterResult);
        System.out.println(MerchantInvoiceIndexPage._filterInvoiceStatusType);

        switch (MerchantInvoiceIndexPage._filterInvoiceStatusType.toLowerCase()) {
            case "pending":
                if (isListEmpty(finalFilterResult)) {
                    testValidationLog(getText(lblNoFilterInvoicesAvailable));
                    bool = getText(lblNoFilterInvoicesAvailable).equalsIgnoreCase(NO_FILTER_DATA_AVAILABLE);
                } else {
                    System.out.println(finalFilterResult.stream().distinct().limit(2).count());
                    bool = finalFilterResult.stream().distinct().limit(2).count() == 1 &&
                            finalFilterResult.contains(INVOICE_PAYMENT_PENDING) &&
                            sizeOf(lstBtnMore) == sizeOf(finalFilterResult);
                }
                break;
            case "cancelled":
                if (isListEmpty(finalFilterResult)) {
                    testValidationLog(getText(lblNoFilterInvoicesAvailable));
                    bool = getText(lblNoFilterInvoicesAvailable).equalsIgnoreCase(NO_FILTER_DATA_AVAILABLE);
                } else {
                    System.out.println(finalFilterResult.stream().distinct().limit(2).count());
                    bool = finalFilterResult.stream().distinct().limit(2).count() == 1 &&
                            finalFilterResult.contains(INVOICE_CANCELLED);
                }
                break;
            case "paid":
                if (isListEmpty(finalFilterResult)) {
                    testValidationLog(getText(lblNoFilterInvoicesAvailable));
                    bool = getText(lblNoFilterInvoicesAvailable).equalsIgnoreCase(NO_FILTER_DATA_AVAILABLE);
                } else {
                    System.out.println(finalFilterResult.stream().distinct().limit(2).count());
                    bool = finalFilterResult.stream().distinct().limit(2).count() == 1 &&
                            finalFilterResult.contains(INVOICE_PAYMENT_COMPLETED);
                }
                break;
        }

        return bool;
    }

    public boolean verifyMaxMinAmountValidation() {

        boolean bool = getText(lblInvalidAmountValidation).equalsIgnoreCase(INVALID_AMOUNT_RANGE);

        testValidationLog(getText(lblInvalidAmountValidation));
        clickOn(driver, btnConfirmOK);

        return bool;
    }

    public boolean verifyInvoiceAmountRangeFilter() {

        List<Double> finalFilterResult = new ArrayList<>();

        for (WebElement name : lstInvoiceAmount) {
            finalFilterResult.add(getDoubleFromString(getInnerText(name)));
        }

        System.out.println(finalFilterResult);
        System.out.println(MerchantInvoiceIndexPage._filterTransactionFromAmount);
        System.out.println(MerchantInvoiceIndexPage._filterTransactionToAmount);

        return findMin(finalFilterResult) >= MerchantInvoiceIndexPage._filterTransactionFromAmount &&
                findMax(finalFilterResult) <= MerchantInvoiceIndexPage._filterTransactionToAmount;

    }

    public boolean verifyInvoiceAmountFromFieldFilter() {

        List<Double> finalFilterResult = new ArrayList<>();

        for (WebElement name : lstInvoiceAmount) {
            finalFilterResult.add(getDoubleFromString(getInnerText(name)));
        }

        System.out.println(finalFilterResult);
        System.out.println(MerchantInvoiceIndexPage._filterTransactionFromAmount);

        return !isListEmpty(finalFilterResult) &&
                findMin(finalFilterResult) >= MerchantInvoiceIndexPage._filterTransactionFromAmount;
    }

    public boolean verifyInvoiceAmountToFieldFilter() {

        List<Double> finalFilterResult = new ArrayList<>();

        for (WebElement name : lstInvoiceAmount) {
            finalFilterResult.add(getDoubleFromString(getInnerText(name)));
        }

        System.out.println(finalFilterResult);
        System.out.println(MerchantInvoiceIndexPage._filterTransactionToAmount);

        return !isListEmpty(finalFilterResult) &&
                findMax(finalFilterResult) <= MerchantInvoiceIndexPage._filterTransactionToAmount;
    }

    public boolean verifyShopNameFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement name : lstUserName) {
            finalFilterResult.add(getInnerText(name));
        }

        System.out.println(finalFilterResult);
        System.out.println(MerchantInvoiceIndexPage._filterShopName);

        return finalFilterResult.contains(MerchantInvoiceIndexPage._filterShopName);
    }

    public boolean verifyMerchantIDFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement number : lstUserIdNumber) {
            finalFilterResult.add(getInnerText(number));
        }

        System.out.println(finalFilterResult);
        System.out.println(MerchantInvoiceIndexPage._filterMerchantId);

        return finalFilterResult.contains(MerchantInvoiceIndexPage._filterMerchantId);
    }

    public boolean verifyFilteredInvoiceTypesForSent() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement status : lstInvoiceStatus) {
            finalFilterResult.add(getInnerText(status));
        }

        if (!isListEmpty(finalFilterResult)) {
            Set<String> status = new HashSet<>(finalFilterResult);
            System.out.println(status);

            status.remove(INVOICE_PAYMENT_COMPLETED);
            status.remove(INVOICE_CANCELLED);
            status.remove(INVOICE_PAYMENT_PENDING);

            return status.isEmpty();
        } else {
            testValidationLog(getText(lblNoFilterInvoicesAvailable));
            return getText(lblNoFilterInvoicesAvailable).equalsIgnoreCase(NO_FILTER_DATA_AVAILABLE);
        }

    }

    public boolean verifyFilteredInvoiceTypesForReceiveConsumer() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement status : lstInvoiceStatus) {
            finalFilterResult.add(getInnerText(status));
        }

        if (!isListEmpty(finalFilterResult)) {
            Set<String> status = new HashSet<>(finalFilterResult);
            System.out.println(status);

            status.remove(INVOICE_PAYMENT_COMPLETED);
            status.remove(INVOICE_CANCELLED);
            status.remove(INVOICE_PAYMENT_PENDING);

            return status.isEmpty();
        } else {
            testValidationLog(getText(lblNoFilterInvoicesAvailable));
            return getText(lblNoFilterInvoicesAvailable).equalsIgnoreCase(NO_FILTER_DATA_AVAILABLE);
        }

    }

    public boolean verifyFilteredInvoiceTypesForReceiveMerchant() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement status : lstInvoiceStatus) {
            finalFilterResult.add(getInnerText(status));
        }

        if (!isListEmpty(finalFilterResult)) {
            Set<String> status = new HashSet<>(finalFilterResult);
            System.out.println(status);

            status.remove(INVOICE_PAYMENT_PAID);
            status.remove(INVOICE_CANCELLED);
            status.remove(INVOICE_PENDING);
            System.out.println(status);
            return status.isEmpty();
        } else {
            testValidationLog(getText(lblNoFilterInvoicesAvailable));
            return getText(lblNoFilterInvoicesAvailable).equalsIgnoreCase(NO_FILTER_DATA_AVAILABLE);
        }

    }

    public boolean verifyReceivedInvoiceDetails() {
        if (!isListEmpty(lstReceivedInvoicesId)) {
            boolean bool = true;
            testInfoLog("Total Received Invoices", String.valueOf(sizeOf(lstReceivedInvoicesId)));
            for (int invoice = 0; invoice < sizeOf(lstReceivedInvoicesId); invoice++) {
                testInfoLog("Invoice Details", "\nInvoice ID : " + getInnerText(lstReceivedInvoicesId.get(invoice)) +
                        "\nInvoice Sender Name & ID : " + getInnerText(lstReceivedInvoicesSenderName.get(invoice)) + "  " +
                        getInnerText(lstReceivedInvoicesSenderID.get(invoice)) +
                        "\nInvoice Status : " + getInnerText(lstReceivedInvoicesStatus.get(invoice)) +
                        "\nInvoice Date : " + getInnerText(lstReceivedInvoicesDate.get(invoice)));

                bool = bool && getInnerText(lstReceivedInvoicesStatus.get(invoice)).equalsIgnoreCase(INVOICE_PENDING);
            }
            return bool;
        } else {
            testValidationLog(getInnerText(lblNoReceivedInvoices));
            return isElementDisplay(lblNoReceivedInvoices) &&
                    getInnerText(lblNoReceivedInvoices).equalsIgnoreCase(NO_RECEIVED_INVOICES);
        }
    }

    public boolean verifySentInvoiceDetails() {
        if (!isListEmpty(lstSentInvoicesId)) {
            boolean bool = true;
            testInfoLog("Total Sent Invoices", String.valueOf(sizeOf(lstSentInvoicesId)));
            for (int invoice = 0; invoice < sizeOf(lstSentInvoicesId); invoice++) {
                testInfoLog("Invoice Details", "\nInvoice ID : " + getInnerText(lstSentInvoicesId.get(invoice)) +
                        "\nInvoice Sender Name & ID : " + getInnerText(lstSentInvoicesSenderName.get(invoice)) + "  " +
                        getInnerText(lstSentInvoicesSenderID.get(invoice)) +
                        "\nInvoice Status : " + getInnerText(lstSentInvoicesStatus.get(invoice)) +
                        "\nInvoice Date : " + getInnerText(lstSentInvoicesDate.get(invoice)));

                bool = bool && getInnerText(lstSentInvoicesStatus.get(invoice)).equalsIgnoreCase(INVOICE_REQUESTED);
            }
            return bool;
        } else {
            testValidationLog(getInnerText(lblNoSentInvoices));
            return isElementDisplay(lblNoSentInvoices) &&
                    getInnerText(lblNoSentInvoices).equalsIgnoreCase(NO_SENT_INVOICES);
        }
    }

    public boolean verifyCreateNewInvoiceScreen() {

        pause(1);

        boolean bool = isElementDisplay(btnBack) && isElementDisplay(txtSenderNumber) && isElementDisplay(txtInvoiceAmount);

        scrollToElement(driver, btnNextInvoiceCreate);

        return bool && isElementDisplay(txtInvoiceMessage) && isElementDisplay(btnCancelInvoice) &&
                isElementDisplay(btnNextInvoiceCreate) && !isElementPresent(btnCreateNewInvoice);
    }

    public boolean verifyCreateInvoiceScreenIsClosed() {
        pause(1);
        return isElementDisplay(btnCreateNewInvoice) && !isElementPresent(btnBack);
    }

    public boolean verifyValidationMessageForInvalidMobileNumber() {
        testValidationLog(getText(lblInvalidMobileNumber));
        return isElementDisplay(lblInvalidMobileNumber) &&
                getText(lblInvalidMobileNumber).equalsIgnoreCase(INVALID_MOBILE_NUMBER);
    }

    public boolean verifyValidationForUserNotFound() {
        pause(3);
        testValidationLog(getText(lblNoUsersFound));
        return isElementDisplay(lblNoUsersFound) &&
                getText(lblNoUsersFound).equalsIgnoreCase(NO_USERS_FOUND);
    }

    public boolean verifyInvalidAmountValidation() {
        testValidationLog(getText(lblInvalidAmount));
        return isElementDisplay(lblInvalidAmount) &&
                getText(lblInvalidAmount).equalsIgnoreCase(INVALID_INVOICE_AMOUNT);
    }

    public boolean verifyInvoiceDetailsForMerchantBeforeSubmitInInvoice() {

        pause(3);

        System.out.println(getText(lblInvoiceUsername));
        System.out.println(MerchantInvoiceIndexPage._invoiceUserName);
        System.out.println(getIntegerFromString(getText(lblInvoiceMobileNumber)));
        System.out.println(getIntegerFromString(MerchantInvoiceIndexPage._invoiceUserNumber));
        System.out.println(formatTwoDecimal(getDoubleFromString(getText(lblInvoiceAmount))));
        System.out.println(MerchantInvoiceIndexPage._invoiceAmount);

        boolean bool = getText(lblInvoiceUsername).equalsIgnoreCase(MerchantInvoiceIndexPage._invoiceUserName) &&
                getIntegerFromString(getText(lblInvoiceMobileNumber)) ==
                        getIntegerFromString(MerchantInvoiceIndexPage._invoiceUserNumber) &&
                formatTwoDecimal(getDoubleFromString(getText(lblInvoiceAmount))) ==
                        MerchantInvoiceIndexPage._invoiceAmount;

        System.out.println(MerchantInvoiceIndexPage._invoiceMessage);
        System.out.println(!isElementPresent(lblInvoiceMessage));

        if (!MerchantInvoiceIndexPage._invoiceMessage.isEmpty()) {
            System.out.println(getText(lblInvoiceMessage));
            bool = bool && getText(lblInvoiceMessage).equalsIgnoreCase(MerchantInvoiceIndexPage._invoiceMessage);
        } else {
            bool = bool && !isElementPresent(lblInvoiceMessage);
        }
        return bool;
    }

    public boolean verifyInvoiceDetailsForConsumerBeforeSubmitInInvoice() {

        pause(3);

        System.out.println(getText(lblInvoiceUsername));
        System.out.println(MerchantInvoiceIndexPage._invoiceUserName);
        System.out.println(getText(lblInvoiceMobileNumber).substring(getText(lblInvoiceMobileNumber).length() - 10));
        System.out.println(MerchantInvoiceIndexPage._invoiceUserNumber);
        System.out.println(formatTwoDecimal(getDoubleFromString(getText(lblInvoiceAmount))));
        System.out.println(MerchantInvoiceIndexPage._invoiceAmount);

        String expectedNumber = getText(lblInvoiceMobileNumber).substring(getText(lblInvoiceMobileNumber).length() - 10);
        System.out.println(expectedNumber);
        String actualNumber = MerchantInvoiceIndexPage._invoiceUserNumber.
                substring(MerchantInvoiceIndexPage._invoiceUserNumber.length() - 10);
        System.out.println(actualNumber);

        boolean bool = getText(lblInvoiceUsername).equalsIgnoreCase(MerchantInvoiceIndexPage._invoiceUserName) &&
                expectedNumber.equalsIgnoreCase(actualNumber) &&
                formatTwoDecimal(getDoubleFromString(getText(lblInvoiceAmount))) ==
                        MerchantInvoiceIndexPage._invoiceAmount;

        System.out.println(MerchantInvoiceIndexPage._invoiceMessage);

        if (!MerchantInvoiceIndexPage._invoiceMessage.isEmpty()) {
            System.out.println(getText(lblInvoiceMessage));
            System.out.println(MerchantInvoiceIndexPage._invoiceMessage);
            bool = bool && getText(lblInvoiceMessage).equalsIgnoreCase(MerchantInvoiceIndexPage._invoiceMessage);
        } else {
            System.out.println(!isElementPresent(lblInvoiceMessage));
            bool = bool && !isElementPresent(lblInvoiceMessage);
        }
        return bool;
    }

    public boolean verifyInvoicePopUpClosed() {
        pause(3);
        return isElementDisplay(btnBack) && isElementDisplay(btnNextInvoiceCreate);
    }

    public boolean verifyInvoiceDetailsForMerchantAfterSent() {

        pause(3);

        System.out.println(getText(lblInvoiceUsername));
        System.out.println(MerchantInvoiceIndexPage._invoiceUserName);
        System.out.println(getText(lblInvoiceMobileNumber));
        System.out.println(MerchantInvoiceIndexPage._invoiceUserNumber);
        System.out.println(formatTwoDecimal(getDoubleFromString(getText(lblInvoiceAmount))));
        System.out.println(MerchantInvoiceIndexPage._invoiceAmount);

        boolean bool = getText(lblInvoiceUsername).equalsIgnoreCase(MerchantInvoiceIndexPage._invoiceUserName) &&
                getIntegerFromString(getText(lblInvoiceMobileNumber)) ==
                        getIntegerFromString(MerchantInvoiceIndexPage._invoiceUserNumber) &&
                formatTwoDecimal(getDoubleFromString(getText(lblInvoiceAmount))) ==
                        MerchantInvoiceIndexPage._invoiceAmount;

        testConfirmationLog(getText(lblInvoiceSentSuccess));
        testInfoLog("Invoice Sent Time", getText(lblInvoiceSentTime));

        return bool;
    }

    public boolean verifyInvoiceDetailsForConsumerAfterSent() {

        pause(3);

        System.out.println(getText(lblInvoiceUsername));
        System.out.println(MerchantInvoiceIndexPage._invoiceUserName);
        System.out.println(getText(lblInvoiceMobileNumber).substring(getText(lblInvoiceMobileNumber).length() - 10));
        System.out.println(MerchantInvoiceIndexPage._invoiceUserNumber);
        System.out.println(formatTwoDecimal(getDoubleFromString(getText(lblInvoiceAmount))));
        System.out.println(MerchantInvoiceIndexPage._invoiceAmount);

        String expectedNumber = getText(lblInvoiceMobileNumber).substring(getText(lblInvoiceMobileNumber).length() - 10);
        System.out.println(expectedNumber);
        String actualNumber = MerchantInvoiceIndexPage._invoiceUserNumber.
                substring(MerchantInvoiceIndexPage._invoiceUserNumber.length() - 10);
        System.out.println(actualNumber);

        boolean bool = getText(lblInvoiceUsername).equalsIgnoreCase(MerchantInvoiceIndexPage._invoiceUserName) &&
                expectedNumber.equalsIgnoreCase(actualNumber) &&
                formatTwoDecimal(getDoubleFromString(getText(lblInvoiceAmount))) ==
                        MerchantInvoiceIndexPage._invoiceAmount;

        testConfirmationLog(getText(lblInvoiceSentSuccess));
        testInfoLog("Invoice Sent Time", getText(lblInvoiceSentTime));

        return bool;
    }

    public boolean verifyCreatedInvoiceDetailsForMerchant() {

        pause(3);

        System.out.println(getText(lblInvoiceCardUserID));
        System.out.println(MerchantInvoiceIndexPage._invoiceUserNumber);
        System.out.println(formatTwoDecimal(getDoubleFromString(getText(lblInvoiceCardInvoiceAmount))));
        System.out.println(MerchantInvoiceIndexPage._invoiceAmount);

        boolean bool = getIntegerFromString(getText(lblInvoiceCardUserID)) ==
                getIntegerFromString(MerchantInvoiceIndexPage._invoiceUserNumber) &&
                formatTwoDecimal(getDoubleFromString(getText(lblInvoiceCardInvoiceAmount))) ==
                        MerchantInvoiceIndexPage._invoiceAmount;

        if (!MerchantInvoiceIndexPage._invoiceMessage.isEmpty()) {
            bool = bool && getText(lblInvoiceCardMessage).equalsIgnoreCase(MerchantInvoiceIndexPage._invoiceMessage);
        } else {
            bool = bool && !isElementPresent(lblInvoiceMessage);
        }

        testInfoLog("Invoice Number", getText(lblInvoiceCardID.get(0)));

        return bool;
    }

    public boolean verifyCreatedInvoiceDetailsForConsumer() {

        pause(3);

        System.out.println(getText(lblInvoiceCardID.get(0)));
        System.out.println(MerchantInvoiceIndexPage._invoiceUserNumber);
        System.out.println(formatTwoDecimal(getDoubleFromString(getText(lblInvoiceCardInvoiceAmount))));
        System.out.println(MerchantInvoiceIndexPage._invoiceAmount);

        String expectedNumber = getText(lblInvoiceCardID.get(0)).
                substring(getText(lblInvoiceCardID.get(0)).length() - 10);
        System.out.println(expectedNumber);
        String actualNumber = MerchantInvoiceIndexPage._invoiceUserNumber.
                substring(MerchantInvoiceIndexPage._invoiceUserNumber.length() - 10);
        System.out.println(actualNumber);


        boolean bool = actualNumber.equalsIgnoreCase(expectedNumber) &&
                formatTwoDecimal(getDoubleFromString(getText(lblInvoiceCardInvoiceAmount))) ==
                        MerchantInvoiceIndexPage._invoiceAmount;

        if (!MerchantInvoiceIndexPage._invoiceMessage.isEmpty()) {
            bool = bool && getText(lblInvoiceCardMessage).equalsIgnoreCase(MerchantInvoiceIndexPage._invoiceMessage);
        } else {
            bool = bool && !isElementPresent(lblInvoiceMessage);
        }

        testInfoLog("Invoice Number", getText(lblInvoiceCardID.get(1)));

        MerchantInvoiceIndexPage._invoiceID = getText(lblInvoiceCardID.get(1));

        return bool;
    }


    public boolean verifyInvoiceDeleteConfirmationPopUp() {

        pause(3);

        System.out.println(getText(lblInvoiceDeleteUserName.get(1)));
        System.out.println(MerchantInvoiceIndexPage._invoiceUserNumber);
        System.out.println(formatTwoDecimal(getDoubleFromString(getText(lblInvoiceDeleteAmount))));
        System.out.println(MerchantInvoiceIndexPage._invoiceAmount);

        boolean bool = getIntegerFromString(getText(lblInvoiceDeleteUserName.get(1))) ==
                getIntegerFromString(MerchantInvoiceIndexPage._invoiceUserNumber) &&
                formatTwoDecimal(getDoubleFromString(getText(lblInvoiceDeleteAmount))) ==
                        MerchantInvoiceIndexPage._invoiceAmount;

        testConfirmationLog(getText(lblConfirmInvoiceDelete));

        return bool;
    }

    public boolean verifyInvoiceDeleteCompletedPopUp() {

        pause(3);

        System.out.println(getText(lblInvoiceDeleteUserName.get(1)));
        System.out.println(MerchantInvoiceIndexPage._invoiceUserNumber);
        System.out.println(formatTwoDecimal(getDoubleFromString(getText(lblInvoiceDeleteAmount))));
        System.out.println(MerchantInvoiceIndexPage._invoiceAmount);

        boolean bool = getIntegerFromString(getText(lblInvoiceDeleteUserName.get(1))) ==
                getIntegerFromString(MerchantInvoiceIndexPage._invoiceUserNumber) &&
                formatTwoDecimal(getDoubleFromString(getText(lblInvoiceDeleteAmount))) ==
                        MerchantInvoiceIndexPage._invoiceAmount;

        testConfirmationLog(getText(lblInvoiceDeleteSuccess));

        return bool;
    }

    public boolean verifyInvoiceDeleteByReceiver() {

        pause(3);

        testConfirmationLog(getText(lblInvoiceDeleteSuccess));

        return isElementDisplay(btnBackToHomeDeleted);
    }

    public boolean verifyDeletedInvoiceCancelled() {

        pause(3);

        return sizeOf(lstUserIdNumber) == 1 && getText(lstInvoiceStatus.get(0)).equalsIgnoreCase(INVOICE_CANCELLED);
    }

    public boolean verifyReceivedMerchantInvoiceDetails() {

        pause(3);

        System.out.println(formatTwoDecimal(getDoubleFromString(getText(lblInvoiceCardInvoiceAmount))));
        System.out.println(MerchantInvoiceIndexPage._invoiceAmount);

        boolean bool = formatTwoDecimal(getDoubleFromString(getText(lblInvoiceCardInvoiceAmount))) ==
                MerchantInvoiceIndexPage._invoiceAmount;

        if (!MerchantInvoiceIndexPage._invoiceMessage.isEmpty()) {
            bool = bool && getText(lblInvoiceCardMessage).equalsIgnoreCase(MerchantInvoiceIndexPage._invoiceMessage);
        } else {
            bool = bool && !isElementPresent(lblInvoiceMessage);
        }

        return bool;
    }

    public boolean verifyPayInvoiceConfirmationDetails() {

        pause(3);

        System.out.println(formatTwoDecimal(getDoubleFromString(getText(lblInvoiceCardInvoiceAmount))));
        System.out.println(MerchantInvoiceIndexPage._invoiceAmount);

        return formatTwoDecimal(getDoubleFromString(getText(lblInvoiceCardInvoiceAmount))) ==
                MerchantInvoiceIndexPage._invoiceAmount;

    }

    public boolean verifyInvoicePayCompletedPopUp() {

        pause(3);

        System.out.println(formatTwoDecimal(getDoubleFromString(getText(lblInvoicePayAmount))));
        System.out.println(MerchantInvoiceIndexPage._invoiceAmount);

        boolean bool = formatTwoDecimal(getDoubleFromString(getText(lblInvoicePayAmount))) ==
                MerchantInvoiceIndexPage._invoiceAmount;

        testConfirmationLog(getText(lblInvoicePaySuccess));

        return bool;
    }

    public boolean verifyInvoiceIDNotDisplayInReceived() {
        boolean bool = true;

        for (int invoice = 0; invoice < sizeOf(lstReceivedInvoicesId); invoice++) {
            if (getInnerText(lstReceivedInvoicesId.get(invoice)).equalsIgnoreCase(MerchantInvoiceIndexPage._invoiceID)) {
                bool = false;
                break;
            }
        }
        return bool;
    }

    public boolean verifyBalanceDeductedForPaidInvoice() {

        pause(5);
        MerchantDashboardIndexPage.getCurrentBalance();
        double updatedBalance = formatTwoDecimal(getDoubleFromString(MerchantDashboardIndexPage._currentBalance));

        System.out.println(updatedBalance);
        System.out.println(MerchantInvoiceIndexPage._invoiceReceiverBalance);
        System.out.println(MerchantInvoiceIndexPage._invoiceAmount);

        return formatTwoDecimal(MerchantInvoiceIndexPage._invoiceReceiverBalance -
                MerchantInvoiceIndexPage._invoiceAmount) == formatTwoDecimal(updatedBalance);

    }

    public boolean verifyBalanceAddedForPaidInvoice() {

        pause(5);
        MerchantDashboardIndexPage.getCurrentBalance();
        double updatedBalance = formatTwoDecimal(getDoubleFromString(MerchantDashboardIndexPage._currentBalance));

        System.out.println(updatedBalance);
        System.out.println(MerchantInvoiceIndexPage._invoiceSenderBalance);
        System.out.println(MerchantInvoiceIndexPage._invoiceAmount);

        return formatTwoDecimal(MerchantInvoiceIndexPage._invoiceSenderBalance +
                MerchantInvoiceIndexPage._invoiceAmount) == formatTwoDecimal(updatedBalance);

    }

    public boolean verifyPaidInvoiceStatus() {
        pause(3);
        return sizeOf(lstUserIdNumber) == 1 && getText(lstInvoiceStatus.get(0)).equalsIgnoreCase(INVOICE_PAYMENT_PAID);
    }

    public boolean verifyReceivedInvoiceStatus() {
        pause(3);
        return sizeOf(lstUserIdNumber) == 1 && getText(lstInvoiceStatus.get(0)).equalsIgnoreCase(INVOICE_PAYMENT_COMPLETED);
    }

    public boolean verifyInvoiceIDNotDisplayInSent() {
        boolean bool = true;

        for (int invoice = 0; invoice < sizeOf(lstSentInvoicesId); invoice++) {
            if (getInnerText(lstSentInvoicesId.get(invoice)).equalsIgnoreCase(MerchantInvoiceIndexPage._invoiceID)) {
                bool = false;
                break;
            }
        }
        return bool;
    }
}
