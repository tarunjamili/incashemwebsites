package com.incashme.merchant.verification;

import com.framework.init.AbstractPage;
import com.incashme.merchant.indexpage.MerchantStatementIndexPage;
import com.incashme.merchant.validations.StatementsValidations;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class MerchantStatementVerification extends AbstractPage implements StatementsValidations {

    public MerchantStatementVerification(WebDriver driver) {
        super(driver);
    }

    @FindAll(value = {@FindBy(xpath = "//div[@class='stmnt-cntnt']")})
    private List<WebElement> lstStatements;

    @FindBy(xpath = "//div//p[contains(text(),'no data alive')]")
    private WebElement lblNoStatement;

    @FindBy(xpath = "//li/button[contains(@class,'filterBtn')]")
    private WebElement btnFilter;

    public boolean verifyStatementsScreen() {

        if (isListEmpty(lstStatements)) {
            testInfoLog("Total Statements", String.valueOf(sizeOf(lstStatements)));
            testValidationLog(getText(lblNoStatement));
            return isElementDisplay(lblNoStatement) &&
                    getText(lblNoStatement).equalsIgnoreCase(NO_STATEMENTS_AVAILABLE);
        } else {
            testInfoLog("Total Statements", String.valueOf(sizeOf(lstStatements)));
            return sizeOf(lstStatements) > 0 && !isElementPresent(lblNoStatement) && isElementDisplay(btnFilter);
        }

    }

    public boolean verifyStatementDetails() {

        if (isListEmpty(lstStatements)) {
            return isElementDisplay(lblNoStatement) && isElementDisplay(btnFilter) &&
                    getText(lblNoStatement).equalsIgnoreCase(NO_STATEMENTS_FOUND);
        } else {
            testInfoLog("Total Statements", String.valueOf(sizeOf(lstStatements)));
            return sizeOf(lstStatements) > 0 &&
                    !isElementPresent(lblNoStatement) && isElementDisplay(btnFilter);
        }

    }

    public boolean verifyStatementDownloaded() {

        String downloadedFileName = getLastFileModified(FILE_DOWNLOAD_PATH);

        System.out.println(downloadedFileName);
        System.out.println(MerchantStatementIndexPage._statementMonthYear);

        testInfoLog("Downloaded Statement Name", downloadedFileName);

        return downloadedFileName.contains(MerchantStatementIndexPage._statementMonthYear);

    }

    public boolean verifyStatementFilterSuccessfully() {

        String actualStatementMonthYear = getText(lstStatements.get(0));
        String expectedStatementMonthYear = MerchantStatementIndexPage._statementMonth + " " +
                MerchantStatementIndexPage._statementYear;

        System.out.println(actualStatementMonthYear);
        System.out.println(expectedStatementMonthYear);
        System.out.println(actualStatementMonthYear.equalsIgnoreCase(expectedStatementMonthYear));

        testInfoLog("Filtered Statement", actualStatementMonthYear);

        return actualStatementMonthYear.equalsIgnoreCase(expectedStatementMonthYear);
    }

    public boolean verifyNoStatementDisplay() {
        return sizeOf(lstStatements) == MerchantStatementIndexPage._totalStatements;
    }
}