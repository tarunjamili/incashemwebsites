package com.incashme.merchant.verification;

import com.framework.init.AbstractPage;
import com.incashme.consumer.indexpage.ConsumerTransferIndexPage;
import com.incashme.merchant.indexpage.MerchantDashboardIndexPage;
import com.incashme.merchant.indexpage.MerchantLoginIndexPage;
import com.incashme.merchant.indexpage.MerchantTransactionHistoryIndexPage;
import com.incashme.merchant.indexpage.MerchantWithdrawIndexPage;
import com.incashme.merchant.validations.DashboardValidations;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MerchantDashboardVerification extends AbstractPage implements DashboardValidations {

    public MerchantDashboardVerification(WebDriver driver) {
        super(driver);
    }

    @FindAll(value = {@FindBy(xpath = "//h4[contains(text(),'Personal')]//following-sibling::form/div/div")})
    private List<WebElement> lstUserAccountDetails;

    @FindAll(value = {@FindBy(xpath = "//h4[contains(text(),'Business')]//following-sibling::form/div/div")})
    private List<WebElement> lstUserBusinessDetails;

    @FindAll(value = {@FindBy(xpath = "//h4[contains(text(),'Documents')]//following-sibling::form//span/span")})
    private List<WebElement> lstUserVerificationStatus;

    @FindAll(value = {@FindBy(xpath = "//h4[contains(text(),'Documents')]//following-sibling::form//input")})
    private List<WebElement> lstUserDocumentsDetails;

    @FindBy(xpath = "//a[contains(@id,'v-pills-business-info-tab')]")
    private WebElement btnBusinessInfoTab;

    @FindBy(xpath = "//a[contains(@id,'v-pills-documents-tab')]")
    private WebElement btnDocumentsInfoTab;

    @FindBy(xpath = "//div[contains(@class,'document_txt')]//following-sibling::div//img")
    private WebElement documentImages;

    @FindBy(xpath = "//h5[contains(text(),'Documents')]//following-sibling::button[contains(@class,'close')]")
    private WebElement btnClosePopup;

    @FindBy(xpath = "//input[@id='oldpassword']")
    private WebElement txtCurrentPassword;

    @FindBy(xpath = "//input[@id='password']")
    private WebElement txtNewPassword;

    @FindBy(xpath = "//input[@id='cnfpwd']")
    private WebElement txtConfirmPassword;

    @FindBy(xpath = "//input[@formcontrolname='oldpin']")
    private WebElement txtCurrentPIN;

    @FindBy(xpath = "//input[@formcontrolname='pin']")
    private WebElement txtNewPIN;

    @FindBy(xpath = "//input[@formcontrolname='pin2']")
    private WebElement txtConfirmPIN;

    @FindAll(value = {@FindBy(xpath = ".//div[contains(@class,'login-form')]//button[@type='submit']")})
    private List<WebElement> btnSubmit;

    @FindBy(xpath = "//table//tr[1]/td[4]")
    private WebElement lblLastLoginTime;

    @FindBy(xpath = "//table//tr[1]/td[3]")
    private WebElement lblUserAgent;

    @FindBy(xpath = "//table//tr[1]/td[2]")
    private WebElement lblIpAddress;

    @FindBy(xpath = "//table//tr[1]/td[1]")
    private WebElement lblSources;

    @FindBy(xpath = "//table[@id='trans-history-table']")
    private WebElement loginHistoryTable;

    @FindBy(xpath = "//input[@formcontrolname='amount']")
    private WebElement txtTopUpAmount;

    @FindBy(xpath = "//div[contains(@class,'blnc-val')]")
    private WebElement lblBalance;

    @FindBy(xpath = "//div[contains(@class,'blnc-txt')]")
    private WebElement lblBalanceMenu;

    @FindBy(xpath = "//div[contains(@id,'dash-column')]")
    private WebElement homeScreenChart;

    @FindBy(xpath = "//label[@id='p_week']")
    private WebElement btnChartWeek;

    @FindBy(xpath = "//label[@id='p_month']")
    private WebElement btnChartMonth;

    @FindBy(xpath = "//label[@id='p_year']")
    private WebElement btnChartYear;

    @FindBy(xpath = "//label[@id='p_total']")
    private WebElement btnChartTotal;

    @FindBy(xpath = "//div[contains(@class,'merchant-count')]//span[contains(@class,'ml-auto')]")
    private WebElement lblSalesAmount;

    @FindBy(xpath = "//div//p[contains(text(),'no data alive')]")
    private WebElement lblNoTransactionHistory;

    @FindBy(xpath = "//div//p[contains(text(),'any Requested Donation')]")
    private WebElement lblNoDonationHistory;

    @FindBy(xpath = "//li/button[contains(@class,'filterBtn')]")
    private WebElement btnFilter;

    @FindBy(xpath = "//table[@id='trans-history-table']")
    private WebElement transactionHistoryTable;

    @FindBy(xpath = "//div[contains(@class,'column-chart-div')]")
    private WebElement donationChart;

    @FindBy(xpath = "//div[contains(@class,'trfr-tpup-row')]//span[contains(@class,'color-danger')]")
    private WebElement lblInvalidReceiveAMountValidation;

    @FindBy(xpath = "//div[contains(@class,'trfr-tpup-card')]//form//button")
    private WebElement btnStartReceiveMoney;

    @FindBy(xpath = "//div[contains(@class,'merc_barcode')]/img")
    private WebElement imgQRCode;

    @FindBy(xpath = "//div[contains(@class,'merc_h_amount')]")
    private WebElement lblReceiveAmount;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dash-trans-history-table']//tr//td[1]//h5[last()]")})
    private List<WebElement> lstDashboardUserName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dash-trans-history-table']//tr//td[1]//following-sibling::div[last()]")})
    private List<WebElement> lstDashboardUserIDNumber;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dash-trans-history-table']//tr//td[3]//div")})
    private List<WebElement> lstDashboardUserType;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dash-trans-history-table']//tr//td[2]//span")})
    private List<WebElement> lstDashboardTransactionID;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dash-trans-history-table']//tr//td[6]")})
    private List<WebElement> lstDashboardTransactionTime;

    @FindBy(xpath = "//input[@type='search']")
    private WebElement txtInputSearch;

    @FindBy(xpath = "//table//tr//td[contains(text(),'matching')]")
    private WebElement lblNoRecordsFound;

    @FindBy(xpath = ".//div[contains(@id,'swal2-content') and contains(text(),'Unsuccessful attempt')]")
    private WebElement lblInvalidCurrentPINValidation;

    @FindBy(xpath = ".//div[contains(@id,'swal2-content') and contains(text(),'past password')]")
    private WebElement lblPastPasswordValidation;

    @FindBy(xpath = ".//div[contains(@class,'swal2-actions')]//button[text()='Ok']")
    private WebElement btnConfirmOK;

    @FindBy(xpath = ".//div[contains(@class,'login-form')]//*[@class='err-msg']")
    private WebElement lblInvalidNewPasswordValidation;

    @FindBy(xpath = ".//div[contains(@id,'swal2-content') and contains(text(),'must be match')]")
    private WebElement lblInvalidConfirmPasswordValidation;

    @FindBy(xpath = ".//div[contains(@id,'swal2-content') and contains(text(),'changed successfully')]")
    private WebElement lblConfirmPasswordChange;

    @FindBy(xpath = ".//div[contains(@id,'swal2-content') and contains(text(),'Unsuccessful attempt')]")
    private WebElement lblInvalidCurrentPasswordValidation;

    @FindAll(value = {@FindBy(xpath = ".//div[contains(@class,'login-form')]//*[@class='err-msg']")})
    private List<WebElement> lblInvalidNewPINValidation;

    @FindBy(xpath = ".//div[contains(@id,'swal2-content') and contains(text(),'does not match')]")
    private WebElement lblInvalidConfirmPinValidation;

    @FindBy(xpath = ".//div[contains(@id,'swal2-content') and contains(text(),'old PIN')]")
    private WebElement lblPastPinValidation;


    public boolean verifyUserProfileDetails() {

        pause(3);

        boolean bool;

        bool = !isListEmpty(lstUserAccountDetails);

        clickOn(driver, btnBusinessInfoTab);

        bool = bool && !isListEmpty(lstUserBusinessDetails);

        clickOn(driver, btnDocumentsInfoTab);

        for (int doc = 0; doc < sizeOf(lstUserDocumentsDetails); doc++) {
            testInfoLog("Verification Status", getText(lstUserVerificationStatus.get(doc)));
            if (!getText(lstUserVerificationStatus.get(doc)).equalsIgnoreCase("Document Not Available")) {
                clickOn(driver, lstUserDocumentsDetails.get(doc));
                bool = bool && isElementDisplay(documentImages);
                clickOn(driver, btnClosePopup);
                pause(1);
            } else {
                testWarningLog("Document is missing in the Merchant Profile Documents.");
            }
        }

        return bool;
    }

    public boolean verifyChangePasswordScreen() {

        pause(1);

        return isElementDisplay(txtCurrentPassword) && isElementDisplay(txtNewPassword) &&
                isElementDisplay(txtConfirmPassword) && isElementDisplay(btnSubmit.get(0));

    }

    public boolean verifyChangePINScreen() {

        pause(1);

        return isElementDisplay(txtCurrentPIN) && isElementDisplay(txtNewPIN) &&
                isElementDisplay(txtConfirmPIN) && isElementDisplay(btnSubmit.get(1));

    }

    public boolean verifyPrivacyPolicyScreen() {

        switchToWindow(driver);

        String expectedTitle = getTitle(driver);
        String actualTitle = "InCashMe™";

        close(driver);

        switchToWindow(driver);

        return expectedTitle.equalsIgnoreCase(actualTitle);

    }

    public boolean verifyTnCScreen() {

        switchToWindow(driver);

        String expectedTitle = getTitle(driver);
        String actualTitle = "InCashMe™";

        close(driver);

        switchToWindow(driver);

        return expectedTitle.equalsIgnoreCase(actualTitle);

    }

    public boolean verifyKYCandAMLScreen() {

        switchToWindow(driver);

        String expectedTitle = getTitle(driver);
        String actualTitle = "InCashMe™";

        close(driver);

        switchToWindow(driver);

        return expectedTitle.equalsIgnoreCase(actualTitle);

    }

    public boolean verifyLoginHistory() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");

        pause(5);

        String actualTime = getText(lblLastLoginTime).trim();

        testVerifyLog("<br>Source : " + getText(lblSources) +
                "<br>IP : " + getText(lblIpAddress) +
                "<br>User Agent : " + getText(lblUserAgent) +
                "<br>Login Time : " + getText(lblLastLoginTime));

        List<String> lstExpectedTimes = new ArrayList<>();

        for (int sec = 0; sec <= 14; sec++) {
            lstExpectedTimes.add((LocalDateTime.parse(MerchantLoginIndexPage._loginTime, format)
                    .plusSeconds(sec)).format(format).trim());
            lstExpectedTimes.add((LocalDateTime.parse(MerchantLoginIndexPage._loginTime, format)
                    .minusSeconds(sec)).format(format).trim());
        }

        System.out.println(lstExpectedTimes);
        System.out.println(actualTime);

        return isElementDisplay(loginHistoryTable) && lstExpectedTimes.contains(actualTime);

    }

    public boolean verifyDashboardScreen() {

        pause(5);

        boolean bool = isElementDisplay(lblBalance) && isElementDisplay(txtTopUpAmount);
        testInfoLog("Balance", getText(lblBalance));

        bool = bool && isElementDisplay(homeScreenChart);

        scrollToElement(driver, btnChartTotal);

        System.out.println(bool);

        testStepsLog(_logStep++, "Click on Total button.");
        clickOn(driver, btnChartTotal);

        pause(1);

        testInfoLog("Total Sales", getInnerText(lblSalesAmount));

        scrollToElement(driver, btnChartWeek);
        testStepsLog(_logStep++, "Click on Week button.");
        clickOn(driver, btnChartWeek);

        pause(1);

        testInfoLog("Total Week Sales", getInnerText(lblSalesAmount));

        scrollToElement(driver, btnChartMonth);
        testStepsLog(_logStep++, "Click on Month button.");
        clickOn(driver, btnChartMonth);

        pause(1);

        testInfoLog("Total Month Sales", getInnerText(lblSalesAmount));

        scrollToElement(driver, btnChartYear);
        testStepsLog(_logStep++, "Click on Year button.");
        clickOn(driver, btnChartYear);

        pause(1);

        testInfoLog("Total Year Sales", getInnerText(lblSalesAmount));

        if (!isListEmpty(lstDashboardUserName)) {
            System.out.println(bool);
            updateShowList(driver, findElementByName(driver, "dash-trans-history-table_length"), "100");
            for (int user = 0; user < sizeOf(lstDashboardUserName); user++) {
                if (getInnerText(lstDashboardUserIDNumber.get(user)).contains("ID")) {
                    bool = bool && (getInnerText(lstDashboardUserType.get(user)).equalsIgnoreCase("Merchant")
                            || getInnerText(lstDashboardUserType.get(user)).equalsIgnoreCase("Agent"));
                } else {
                    bool = bool && getInnerText(lstDashboardUserType.get(user)).equalsIgnoreCase("Consumer");
                }
            }
        } else {
            testVerifyLog("No Transaction done for today for the current Merchant.");
        }

        return bool;
    }

    public boolean verifyTxHistoryScreen() {
        pause(5);

        if (isElementPresent(lblNoTransactionHistory)) {

            scrollToElement(driver, lblNoTransactionHistory);
            testValidationLog(getText(lblNoTransactionHistory));
            return isElementDisplay(lblNoTransactionHistory) &&
                    getText(lblNoTransactionHistory).equalsIgnoreCase(NO_TRANSACTION_HISTORY_FOUND);

        } else {
            return isElementDisplay(transactionHistoryTable) && isElementDisplay(btnFilter);
        }
    }

    public boolean verifyBalanceScreen() {
        System.out.println(getText(lblBalanceMenu));
        System.out.println(MerchantDashboardIndexPage._currentBalance);
        testInfoLog("Current Account Balance", getText(lblBalanceMenu));
        return isElementDisplay(lblBalanceMenu) && getText(lblBalanceMenu).equalsIgnoreCase(MerchantDashboardIndexPage._currentBalance);
    }

    public boolean verifyZeroAmountValidation() {

        testVerifyLog(getText(lblInvalidReceiveAMountValidation));

        return getText(lblInvalidReceiveAMountValidation).equalsIgnoreCase(INVALID_RECEIVE_AMOUNT_VALIDATION) &&
                btnStartReceiveMoney.getAttribute("disabled").equalsIgnoreCase("true");
    }

    public boolean verifyMaxAmountValidation1() {

        testVerifyLog(getText(lblInvalidReceiveAMountValidation));

        return getText(lblInvalidReceiveAMountValidation).equalsIgnoreCase(INVALID_RECEIVE_AMOUNT_VALIDATION) &&
                btnStartReceiveMoney.getAttribute("disabled").equalsIgnoreCase("true");

    }

    public boolean verifyReceiveMoneyQRScreen() {

        pause(3);

        System.out.println(getText(lblReceiveAmount));
        System.out.println(MerchantDashboardIndexPage._receiveMoney);

        return isElementDisplay(imgQRCode);

    }

    public boolean verifyPopUpScreenClosed() {
        pause(3);
        return !isElementPresent(imgQRCode) && !isElementPresent(lblReceiveAmount);
    }

    public boolean verifyUserNameSorted() {

        List<String> userNameAfterSort = new ArrayList<>();
        List<String> userNameBeforeSort = MerchantDashboardIndexPage._userNameBeforeSort;

        for (WebElement name : lstDashboardUserName) {
            userNameAfterSort.add(getInnerText(name));
        }
        System.out.println(userNameAfterSort);
        System.out.println(userNameBeforeSort);

        userNameBeforeSort.sort(
                (String o1, String o2) -> {
                    if (o1.equalsIgnoreCase(o2)) {
                        return o2.compareTo(o1);
                    }
                    return o1.toLowerCase().compareTo(o2.toLowerCase());
                }
        );

        System.out.println(userNameAfterSort);
        System.out.println(userNameBeforeSort);

        System.out.println(userNameBeforeSort.equals(userNameAfterSort));

        return userNameBeforeSort.equals(userNameAfterSort);
    }

    public boolean verifyUserTxIDSorted() {

        List<String> txIDAfterSort = new ArrayList<>();
        List<String> txIDBeforeSort = MerchantDashboardIndexPage._userTxIDBeforeSort;

        for (WebElement name : lstDashboardTransactionID) {
            txIDAfterSort.add(getInnerText(name));
        }

        txIDBeforeSort.sort(
                (String o1, String o2) -> {
                    if (o1.equalsIgnoreCase(o2)) {
                        return o2.compareTo(o1);
                    }
                    return o1.toLowerCase().compareTo(o2.toLowerCase());
                }
        );

        System.out.println(txIDAfterSort);
        System.out.println(txIDBeforeSort);

        System.out.println(txIDBeforeSort.equals(txIDAfterSort));

        return txIDBeforeSort.equals(txIDAfterSort);
    }

    public boolean verifyUserTypeSorted() {

        List<String> userTypeAfterSort = new ArrayList<>();
        List<String> userTypeBeforeSort = MerchantDashboardIndexPage._userTypesBeforeSort;

        for (WebElement name : lstDashboardUserType) {
            userTypeAfterSort.add(getInnerText(name));
        }

        userTypeBeforeSort.sort(
                (String o1, String o2) -> {
                    if (o1.equalsIgnoreCase(o2)) {
                        return o2.compareTo(o1);
                    }
                    return o1.toLowerCase().compareTo(o2.toLowerCase());
                }
        );

        System.out.println(userTypeAfterSort);
        System.out.println(userTypeBeforeSort);

        System.out.println(userTypeBeforeSort.equals(userTypeAfterSort));

        return userTypeBeforeSort.equals(userTypeAfterSort);
    }

    public boolean verifyTransactionDateTimeSorted() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        List<LocalDateTime> transactionDateTimeAfterSort = new ArrayList<>();
        List<LocalDateTime> transactionDateTimeBeforeSort = MerchantDashboardIndexPage._transactionDateTimeBeforeSort;

        for (WebElement name : lstDashboardTransactionTime) {
            transactionDateTimeAfterSort.add(LocalDateTime.parse(getInnerText(name), format));
        }

        Collections.sort(transactionDateTimeBeforeSort);

        System.out.println(transactionDateTimeAfterSort);
        System.out.println(transactionDateTimeBeforeSort);

        System.out.println(transactionDateTimeBeforeSort.equals(transactionDateTimeAfterSort));

        return transactionDateTimeBeforeSort.equals(transactionDateTimeAfterSort);
    }

    public boolean verifyTableSearchSuccessful() {

        boolean bool = getInnerText(lstDashboardTransactionID.get(0)).contains(MerchantDashboardIndexPage._searchCriteria) &&
                sizeOf(lstDashboardTransactionID) == 1;

        type(txtInputSearch, "");

        return bool;

    }

    public boolean verifyTableValidationForInvalidSearch() {

        testValidationLog(getInnerText(lblNoRecordsFound));

        boolean bool = getInnerText(lblNoRecordsFound).equalsIgnoreCase(NO_SEARCH_RECORD_FOUND);

        type(txtInputSearch, "x");
        txtInputSearch.clear();

        return bool;
    }

    public boolean verifyInvalidCurrentPasswordValidation() {

        testValidationLog(getText(lblInvalidCurrentPasswordValidation));

        boolean bool = isElementDisplay(lblInvalidCurrentPasswordValidation) &&
                getText(lblInvalidCurrentPasswordValidation).equalsIgnoreCase(FIRST_UNSUCCESSFUL_ATTEMPTS) &&
                isElementDisplay(btnConfirmOK);

        clickOn(driver, btnConfirmOK);

        return bool;
    }

    public boolean verifyInvalidCurrentPasswordSecondTime() {

        testValidationLog(getText(lblInvalidCurrentPasswordValidation));

        boolean bool = isElementDisplay(lblInvalidCurrentPasswordValidation) &&
                getText(lblInvalidCurrentPasswordValidation).equalsIgnoreCase(SECOND_UNSUCCESSFUL_ATTEMPTS) &&
                isElementDisplay(btnConfirmOK);

        clickOn(driver, btnConfirmOK);

        return bool;
    }

    public boolean verifyAccountIsBlockedForInvalidPassword() {

        testValidationLog(getText(lblInvalidCurrentPasswordValidation));

        boolean bool = isElementDisplay(lblInvalidCurrentPasswordValidation) &&
                getText(lblInvalidCurrentPasswordValidation).equalsIgnoreCase(THIRD_UNSUCCESSFUL_ATTEMPTS) &&
                isElementDisplay(btnConfirmOK);

        clickOn(driver, btnConfirmOK);

        return bool;
    }

    public boolean verifyInvalidNewPasswordValidation() {

        testValidationLog(getText(lblInvalidNewPasswordValidation));

        return isElementDisplay(lblInvalidNewPasswordValidation) &&
                getText(lblInvalidNewPasswordValidation).equalsIgnoreCase(INVALID_NEW_PASSWORD);
    }

    public boolean verifyInvalidConfirmPasswordValidation() {

        testValidationLog(getText(lblInvalidConfirmPasswordValidation));

        boolean bool = isElementDisplay(lblInvalidConfirmPasswordValidation) &&
                getText(lblInvalidConfirmPasswordValidation).equalsIgnoreCase(NEW_CONFIRM_PASSWORD_NOT_MATCHED) &&
                isElementDisplay(btnConfirmOK);

        clickOn(driver, btnConfirmOK);

        return bool;

    }

    public boolean verifyPastPasswordValidation() {

        testValidationLog(getText(lblPastPasswordValidation));

        boolean bool = isElementDisplay(lblPastPasswordValidation) &&
                getText(lblPastPasswordValidation).equalsIgnoreCase(PAST_PASSWORD_MESSAGE) &&
                isElementDisplay(btnConfirmOK);

        clickOn(driver, btnConfirmOK);

        return bool;

    }

    public boolean verifyPasswordChangeSuccessfully() {

        testConfirmationLog(getText(lblConfirmPasswordChange));

        boolean bool = isElementDisplay(lblConfirmPasswordChange) &&
                getText(lblConfirmPasswordChange).equalsIgnoreCase(SUCCESSFUL_PASSWORD_CHANGE_MESSAGE) &&
                isElementDisplay(btnConfirmOK);

        clickOn(driver, btnConfirmOK);

        return bool;

    }

    public boolean verifyInvalidCurrentPINValidation() {

        testValidationLog(getText(lblInvalidCurrentPINValidation));

        boolean bool = isElementDisplay(lblInvalidCurrentPINValidation) &&
                getText(lblInvalidCurrentPINValidation).equalsIgnoreCase(FIRST_UNSUCCESSFUL_ATTEMPTS) &&
                isElementDisplay(btnConfirmOK);

        clickOn(driver, btnConfirmOK);

        return bool;
    }

    public boolean verifyInvalidNewPINValidation() {

        testValidationLog(getText(lblInvalidNewPINValidation.get(0)));
        testValidationLog(getText(lblInvalidNewPINValidation.get(1)));

        return isElementDisplay(lblInvalidNewPINValidation.get(0)) &&
                getText(lblInvalidNewPINValidation.get(0)).equalsIgnoreCase(INVALID_NEW_PIN) &&
                isElementDisplay(lblInvalidNewPINValidation.get(1)) &&
                getText(lblInvalidNewPINValidation.get(1)).equalsIgnoreCase(INVALID_CONFIRM_PIN);
    }

    public boolean verifyInvalidConfirmPinValidation() {

        testValidationLog(getText(lblInvalidConfirmPinValidation));

        boolean bool = isElementDisplay(lblInvalidConfirmPinValidation) &&
                getText(lblInvalidConfirmPinValidation).equalsIgnoreCase(NEW_CONFIRM_PIN_NOT_MATCHED) &&
                isElementDisplay(btnConfirmOK);

        clickOn(driver, btnConfirmOK);

        return bool;

    }

    public boolean verifyPastPinValidation() {

        testValidationLog(getText(lblPastPinValidation));

        boolean bool = isElementDisplay(lblPastPinValidation) &&
                getText(lblPastPinValidation).equalsIgnoreCase(PAST_PIN_MESSAGE) &&
                isElementDisplay(btnConfirmOK);

        clickOn(driver, btnConfirmOK);

        return bool;

    }

    public boolean verifyInvalidCurrentPINSecondTime() {

        testValidationLog(getText(lblInvalidCurrentPINValidation));

        boolean bool = isElementDisplay(lblInvalidCurrentPINValidation) &&
                getText(lblInvalidCurrentPINValidation).equalsIgnoreCase(SECOND_UNSUCCESSFUL_ATTEMPTS) &&
                isElementDisplay(btnConfirmOK);

        clickOn(driver, btnConfirmOK);

        return bool;
    }

    public boolean verifyAccountIsBlockedForInvalidPIN() {

        testValidationLog(getText(lblInvalidCurrentPINValidation));

        boolean bool = isElementDisplay(lblInvalidCurrentPINValidation) &&
                getText(lblInvalidCurrentPINValidation).equalsIgnoreCase(THIRD_UNSUCCESSFUL_ATTEMPTS) &&
                isElementDisplay(btnConfirmOK);

        clickOn(driver, btnConfirmOK);

        return bool;
    }

    public boolean verifyBalanceUpdatedInHomeScreen() {

        pause(3);

        return getDoubleFromString(getText(lblBalance)) ==
                getDoubleFromString(MerchantDashboardIndexPage._currentBalance) -
                        MerchantWithdrawIndexPage._withDrawTotalAmount;

    }

    public boolean verifyBalanceAfterRefund() {

        pause(5);

        System.out.println(formatTwoDecimal(getDoubleFromString(MerchantDashboardIndexPage._currentBalance) -
                MerchantTransactionHistoryIndexPage._latestTransactionAmount));
        System.out.println(formatTwoDecimal(getDoubleFromString(getText(lblBalance))));
        System.out.println(formatTwoDecimal(getDoubleFromString(MerchantDashboardIndexPage._currentBalance) -
                MerchantTransactionHistoryIndexPage._latestTransactionAmount) ==
                formatTwoDecimal(getDoubleFromString(getText(lblBalance))));

        testInfoLog("Previous Balance", MerchantDashboardIndexPage._currentBalance);
        testInfoLog("Refund Amount", String.valueOf(MerchantTransactionHistoryIndexPage._latestTransactionAmount));
        testInfoLog("Updated Balance", getText(lblBalance));

        double updatedBalance = formatTwoDecimal(getDoubleFromString(MerchantDashboardIndexPage._currentBalance) -
                MerchantTransactionHistoryIndexPage._latestTransactionAmount);

        return updatedBalance == formatTwoDecimal(getDoubleFromString(getText(lblBalance)));
    }

    public boolean verifyAmountAddedForTransfer() {

        pause(5);
        MerchantDashboardIndexPage.getCurrentBalance();
        double updatedBalance = formatTwoDecimal(getDoubleFromString(MerchantDashboardIndexPage._currentBalance));

        System.out.println(updatedBalance);
        System.out.println(MerchantDashboardIndexPage._transferReceiverBalance);
        System.out.println(ConsumerTransferIndexPage._transferAmount);

        return formatTwoDecimal(MerchantDashboardIndexPage._transferReceiverBalance +
                ConsumerTransferIndexPage._transferAmount) == formatTwoDecimal(updatedBalance);

    }
}
