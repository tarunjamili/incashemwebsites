package com.incashme.merchant.verification;

import com.framework.init.AbstractPage;
import com.incashme.merchant.indexpage.MerchantTransactionHistoryIndexPage;
import com.incashme.merchant.validations.TransactionHistoryValidations;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class MerchantTransactionHistoryVerification extends AbstractPage implements TransactionHistoryValidations {

    public MerchantTransactionHistoryVerification(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//table//tr//td[contains(text(),'matching')]")
    private WebElement lblNoRecordsFound;

    @FindBy(xpath = "//input[@type='search']")
    private WebElement txtInputSearch;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//button[contains(@class,'searchbtn')]")
    private WebElement btnFilterSearch;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//a[contains(text(),'Clear All')]")
    private WebElement btnFilterClearAll;

    @FindBy(xpath = "//div//p[contains(text(),'find any matches')]")
    private WebElement lblNoFilterConsumerAvailable;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[1]//h5")})
    private List<WebElement> lstUserName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[1]//following-sibling::div")})
    private List<WebElement> lstUserIdNumber;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[2]/span")})
    private List<WebElement> lstUserTxID;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[3]//div")})
    private List<WebElement> lstUserType;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[4]//div")})
    private List<WebElement> lstTransactionAmount;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[6]")})
    private List<WebElement> lstTransactionTime;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[7]//button")})
    private List<WebElement> lstBtnMore;

    @FindBy(xpath = ".//div[contains(@id,'swal2-content') and contains(text(),'Invalid')]")
    private WebElement lblInvalidAmountValidation;

    @FindBy(xpath = ".//div[contains(@class,'swal2-actions')]//button[text()='OK' or text() = 'Ok']")
    private WebElement btnConfirmOK;

    @FindBy(xpath = "//div[contains(@class,'csmer-trans-detail')]//div[contains(text(),' Transaction Type ')]//following-sibling::div")
    private WebElement lblTransactionType;

    @FindBy(xpath = "//*[text()='Transaction Details']/ancestor::div//button[contains(@class,'close')]")
    private WebElement btnCloseTxDetailsPopup;

    @FindBy(xpath = "//div[@role='dialog']//div[contains(@class,'transfer-id')]")
    private WebElement lblPopUpTransactionID;

    @FindAll(value = {@FindBy(xpath = "//*[contains(text(),'Transaction Details')]/../..//following-sibling::div//li//div[contains(@class,'text-right')]")})
    private List<WebElement> lstTransactionDetails;

    @FindBy(xpath = "(//*[contains(text(),'Transaction Details')]/../..//following-sibling::div//li//div[contains(@class,'details-title')])[3]")
    private WebElement lblAddtionalDevice;

    @FindBy(xpath = "//div[contains(@class,'csmer-trans-detail')]//button[contains(@class,'btn-green') and text()='Refund Money']")
    private WebElement btnRefund;

    @FindBy(xpath = "//div[contains(@class,'amt_s_details')]//div[contains(@class,'merc_d_amount')]")
    private WebElement lblRefundAmountPopUp;

    @FindBy(xpath = "//div[contains(@class,'amt_s_details')]//div[contains(@class,'merc_d_name')]")
    private WebElement lblRefundReceiverNameNumber;

    @FindBy(xpath = "//div[contains(@class,'agnt-cnf-ftr')]//button[contains(@id,'send-mny')]")
    private WebElement btnSendRefund;

    @FindBy(xpath = "//div[contains(@class,'agnt-cnf-ftr')]//button[contains(@class,'btn-pink')]")
    private WebElement btnCancelRefund;

    @FindBy(xpath = ".//div[contains(@id,'swal2-content') and contains(text(),'Refund')]")
    private WebElement lblRefundMoneySuccessful;

    @FindBy(xpath = "//input[@formcontrolname='amount']")
    private WebElement txtTransferRefundAmount;

    @FindBy(xpath = "//div[contains(@class,'agnt-cnf-ftr')]//button[contains(@class,'btn-green') and text()='Add Money']")
    private WebElement btnAddMoney;

    @FindBy(xpath = "//div[contains(@class,'merc_h_amount')]")
    private WebElement lblDeviceBalanceError;

    @FindBy(xpath = "//div[contains(@class,'entr-amnt')]//span[contains(@class,'color-danger')]")
    private WebElement lblInvalidRefundAmountValidation;

    @FindBy(xpath = "//h5[contains(text(),'Successfully')]")
    private WebElement lblSuccessAdditionalDeviceRefund;

    @FindBy(xpath = "//div[contains(@class,'agnt-cnf-ftr')]//button[contains(@class,'btn-green') and text()='Ok']")
    private WebElement btnRefundCompleteOK;

    public boolean verifySearchSuccessful() {

        boolean bool = true;

        for (WebElement e : lstUserTxID) {
            bool = bool && getInnerText(e).contains(MerchantTransactionHistoryIndexPage._searchCriteria);
        }

        return bool;

    }

    public boolean verifyValidationForInvalidSearch() {

        testValidationLog(getInnerText(lblNoRecordsFound));

        boolean bool = getInnerText(lblNoRecordsFound).equalsIgnoreCase(NO_SEARCH_RECORD_FOUND);

        type(txtInputSearch, "x");
        txtInputSearch.clear();

        return bool;
    }

    public boolean verifyTransactionFilterScreenDisplay() {

        try {
            updateShowList(driver, findElementByName(driver, "trans-history-table_length"), "100");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        return isElementDisplay(btnFilterSearch) && isElementDisplay(btnFilterClearAll);
    }

    public boolean verifyRandomDataFiltered() {
        return getText(lblNoFilterConsumerAvailable).equalsIgnoreCase(NO_FILTER_DATA_AVAILABLE);
    }

    public boolean verifyFirstNameFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement name : lstUserName) {
            finalFilterResult.add(getInnerText(name).split(" ")[0].trim());
        }

        System.out.println(finalFilterResult);
        System.out.println(MerchantTransactionHistoryIndexPage._filterFirstName);

        return finalFilterResult.contains(MerchantTransactionHistoryIndexPage._filterFirstName);
    }

    public boolean verifyLastNameFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement name : lstUserName) {
            finalFilterResult.add(getInnerText(name).replace(MerchantTransactionHistoryIndexPage._filterFirstName, "").trim());
        }

        System.out.println(finalFilterResult);
        System.out.println(MerchantTransactionHistoryIndexPage._filterLastName);

        return finalFilterResult.contains(MerchantTransactionHistoryIndexPage._filterLastName);
    }

    public boolean verifyPhoneNumberFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement number : lstUserIdNumber) {
            finalFilterResult.add(getInnerText(number));
        }

        System.out.println(finalFilterResult);
        System.out.println(MerchantTransactionHistoryIndexPage._filterPhone);

        return finalFilterResult.contains(MerchantTransactionHistoryIndexPage._filterPhone);
    }

    public boolean verifyTxIDFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement number : lstUserTxID) {
            finalFilterResult.add(String.valueOf(getIntegerFromString(getInnerText(number))));
        }

        System.out.println(finalFilterResult);
        System.out.println(MerchantTransactionHistoryIndexPage._filterTransactionId);

        return finalFilterResult.contains(MerchantTransactionHistoryIndexPage._filterTransactionId) &&
                sizeOf(finalFilterResult) == 1;
    }

    public boolean verifyMaxMinTransactionValidation() {

        boolean bool = getText(lblInvalidAmountValidation).equalsIgnoreCase(INVALID_AMOUNT_RANGE);

        testValidationLog(getText(lblInvalidAmountValidation));
        clickOn(driver, btnConfirmOK);

        return bool;
    }

    public boolean verifyTransactionRangeFilter() {

        List<Double> finalFilterResult = new ArrayList<>();

        for (WebElement name : lstTransactionAmount) {
            finalFilterResult.add(getDoubleFromString(getInnerText(name)));
        }

        System.out.println(finalFilterResult);
        System.out.println(MerchantTransactionHistoryIndexPage._filterTransactionFromAmount);
        System.out.println(MerchantTransactionHistoryIndexPage._filterTransactionToAmount);

        return findMin(finalFilterResult) >= MerchantTransactionHistoryIndexPage._filterTransactionFromAmount &&
                findMax(finalFilterResult) <= MerchantTransactionHistoryIndexPage._filterTransactionToAmount;

    }

    public boolean verifyTransactionFromFieldFilter() {

        List<Double> finalFilterResult = new ArrayList<>();

        for (WebElement name : lstTransactionAmount) {
            finalFilterResult.add(getDoubleFromString(getInnerText(name)));
        }

        System.out.println(finalFilterResult);
        System.out.println(MerchantTransactionHistoryIndexPage._filterTransactionFromAmount);

        return !isListEmpty(finalFilterResult) &&
                findMin(finalFilterResult) >= MerchantTransactionHistoryIndexPage._filterTransactionFromAmount;
    }

    public boolean verifyTransactionToFieldFilter() {

        List<Double> finalFilterResult = new ArrayList<>();

        for (WebElement name : lstTransactionAmount) {
            finalFilterResult.add(getDoubleFromString(getInnerText(name)));
        }

        System.out.println(finalFilterResult);
        System.out.println(MerchantTransactionHistoryIndexPage._filterTransactionToAmount);

        return !isListEmpty(finalFilterResult) &&
                findMax(finalFilterResult) <= MerchantTransactionHistoryIndexPage._filterTransactionToAmount;
    }

    public boolean verifyTransactionTypeFilterSuccessfully() {

        if (!isListEmpty(lstTransactionAmount)) {
            clickOn(driver, lstBtnMore.get(getRandomNumberBetween(0, lastIndexOf(lstBtnMore))));

            pause(2);

            boolean bool = getText(lblTransactionType).equalsIgnoreCase(MerchantTransactionHistoryIndexPage._filterTransactionType);

            clickOn(driver, btnCloseTxDetailsPopup);

            return bool;
        } else {
            testValidationLog(getText(lblNoFilterConsumerAvailable));
            return true;
        }

    }

    public boolean verifyShopNameFiltered() {

        pause(5);

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement name : lstUserName) {
            finalFilterResult.add(getInnerText(name));
        }

        System.out.println(finalFilterResult);
        System.out.println(MerchantTransactionHistoryIndexPage._filterShopName);

        return finalFilterResult.contains(MerchantTransactionHistoryIndexPage._filterShopName);
    }

    public boolean verifyMerchantIDFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement number : lstUserIdNumber) {
            finalFilterResult.add(getInnerText(number));
        }

        System.out.println(finalFilterResult);
        System.out.println(MerchantTransactionHistoryIndexPage._filterID);

        return finalFilterResult.contains(MerchantTransactionHistoryIndexPage._filterID);
    }

    public boolean verifyTransactionDetails(String userType) {

        pause(2);

        System.out.println(getText(lblPopUpTransactionID));

        boolean bool = getText(lblPopUpTransactionID).equalsIgnoreCase(
                userType + " Transfer Id- " + MerchantTransactionHistoryIndexPage._filterTransactionId);

        String date = MerchantTransactionHistoryIndexPage._transactionDateAndTime.substring(0, 10);
        String time = LocalTime.parse(MerchantTransactionHistoryIndexPage._transactionDateAndTime.substring(11),
                DateTimeFormatter.ofPattern("hh:mm:ss a")).format(DateTimeFormatter.ofPattern("hh:mm a"));

        System.out.println(date);
        System.out.println(time);

        bool = bool && getText(lstTransactionDetails.get(0)).
                equalsIgnoreCase(date + " & " + time);

        if (!getText(lblAddtionalDevice).equalsIgnoreCase("Additional Device")) {
            System.out.println(getText(lstTransactionDetails.get(2)));
            System.out.println(MerchantTransactionHistoryIndexPage._filterFirstName);
            System.out.println(getLongFromString(MerchantTransactionHistoryIndexPage._filterPhone));

            bool = bool && getText(lstTransactionDetails.get(2)).split("\n")[0].equalsIgnoreCase(MerchantTransactionHistoryIndexPage._filterFirstName)
                    && String.valueOf(getLongFromString(getText(lstTransactionDetails.get(2)).split("\n")[1])).equalsIgnoreCase
                    (String.valueOf(getLongFromString(MerchantTransactionHistoryIndexPage._filterPhone)));
        } else {
            System.out.println(getText(lstTransactionDetails.get(4)));
            //  bool = bool && getIntegerFromString(getText(driver,lstTransactionDetails.get(4))) != 0;

        }

        System.out.println(getText(lstTransactionDetails.get(3)));
        System.out.println(MerchantLoginVerification._username);

        bool = bool && getText(lstTransactionDetails.get(3)).contains(MerchantLoginVerification._username);

        System.out.println(getText(lstTransactionDetails.get(1)));
        System.out.println(MerchantTransactionHistoryIndexPage._transactionTotalAmount);

        bool = bool && getText(lstTransactionDetails.get(1)).equalsIgnoreCase(MerchantTransactionHistoryIndexPage._transactionTotalAmount);

        return bool;
    }

    public boolean verifyRefundButtonDisplay() {
        pause(3);
        return isElementPresent(btnRefund);
    }

    public boolean verifyRefundButtonNotDisplay() {
        pause(3);
        return !isElementPresent(btnRefund);
    }

    public boolean verifyRefundConfirmation() {
        pause(3);
        testConfirmationLog(getText(lblRefundMoneySuccessful));

        boolean bool = getText(lblRefundMoneySuccessful).equalsIgnoreCase(REFUND_SUCCESSFUL);

        clickOn(driver, btnConfirmOK);

        return bool;

    }

    public boolean verifyConfirmationRefundDetails() {

        pause(3);

        System.out.println(getDoubleFromString(getText(lblRefundAmountPopUp)));
        System.out.println(MerchantTransactionHistoryIndexPage._refundAmount);
        System.out.println(getText(lblRefundReceiverNameNumber));
        System.out.println(MerchantTransactionHistoryIndexPage._refundReceiverName);

        return getDoubleFromString(getText(lblRefundAmountPopUp)) == MerchantTransactionHistoryIndexPage._refundAmount &&
                getText(lblRefundReceiverNameNumber).split("\n")[0].
                        equalsIgnoreCase(MerchantTransactionHistoryIndexPage._refundReceiverName.split("\n")[0]) &&
                getText(lblRefundReceiverNameNumber).split("\n")[1].
                        contains(MerchantTransactionHistoryIndexPage._refundReceiverName.split("\n")[1]) &&
                isElementDisplay(btnSendRefund) && isElementDisplay(btnCancelRefund);


    }

    public boolean verifyConfirmationRefundDetailsForMerchantSell() {

        pause(3);

        System.out.println(getDoubleFromString(getText(lblRefundAmountPopUp)));
        System.out.println(MerchantTransactionHistoryIndexPage._refundAmount);

        testInfoLog("Merchant Name", getText(lblRefundReceiverNameNumber).split("\n")[0]);
        testInfoLog("Merchant Number", getText(lblRefundReceiverNameNumber).split("\n")[1]);

        return getDoubleFromString(getText(lblRefundAmountPopUp)) == MerchantTransactionHistoryIndexPage._refundAmount &&
                isElementDisplay(btnSendRefund) && isElementDisplay(btnCancelRefund);

    }

    public boolean verifyConfirmationRefundDetailsForAddDevice() {

        pause(3);

        System.out.println(getDoubleFromString(getText(lblRefundAmountPopUp)));
        System.out.println(MerchantTransactionHistoryIndexPage._refundAmount);

        return getDoubleFromString(getText(lblRefundAmountPopUp)) == MerchantTransactionHistoryIndexPage._refundAmount;

    }

    public boolean verifyTransactionRefunded() {
        pause(3);
        return getText(lstTransactionDetails.get(5)).equalsIgnoreCase(TYPE_REFUNDED);
    }

    public boolean verifyCompletedRefundDetails() {

        System.out.println(MerchantTransactionHistoryIndexPage._refundAmount);
        System.out.println(MerchantTransactionHistoryIndexPage._latestTransactionAmount);
        System.out.println(MerchantTransactionHistoryIndexPage._refundReceiverName);
        System.out.println(MerchantTransactionHistoryIndexPage._latestRefundReceiverName);
        System.out.println(MerchantTransactionHistoryIndexPage._refundSenderName);
        System.out.println(MerchantTransactionHistoryIndexPage._latestRefundSenderName);

        System.out.println(MerchantTransactionHistoryIndexPage._latestTransactionType);

        return MerchantTransactionHistoryIndexPage._refundAmount == MerchantTransactionHistoryIndexPage._latestTransactionAmount &&
                MerchantTransactionHistoryIndexPage._refundReceiverName.equalsIgnoreCase(MerchantTransactionHistoryIndexPage._latestRefundReceiverName) &&
                MerchantTransactionHistoryIndexPage._refundSenderName.equalsIgnoreCase(MerchantTransactionHistoryIndexPage._latestRefundSenderName) &&
                MerchantTransactionHistoryIndexPage._latestTransactionType.equalsIgnoreCase(TYPE_REFUND);

    }

    public boolean verifyRefundInsufficient() {
        pause(3);

        testValidationLog(getText(lblDeviceBalanceError));
        return isElementDisplay(txtTransferRefundAmount) && isElementDisplay(btnCancelRefund) && isElementDisplay(btnAddMoney) &&
                getText(lblDeviceBalanceError).equalsIgnoreCase(ADD_BALANCE_IN_ADDITIONAL_DEVICE + " - " +
                        MerchantTransactionHistoryIndexPage._refundAdditionalDeviceName);

    }

    public boolean verifyZeroAmountValidation() {

        testVerifyLog(getText(lblInvalidRefundAmountValidation));

        return getText(lblInvalidRefundAmountValidation).equalsIgnoreCase(INVALID_ADDITIONAL_DEVICE_REFUND_AMOUNT) &&
                btnAddMoney.getAttribute("disabled").equalsIgnoreCase("true");
    }

    public boolean verifyMaxAmountValidation() {

        testVerifyLog(getText(lblInvalidRefundAmountValidation));

        return getText(lblInvalidRefundAmountValidation).equalsIgnoreCase(INVALID_ADDITIONAL_DEVICE_REFUND_AMOUNT) &&
                btnAddMoney.getAttribute("disabled").equalsIgnoreCase("true");

    }

    public boolean verifyConfirmationForRefundAddedInAdditionalDevice() {

        pause(3);

        testConfirmationLog(getText(lblDeviceBalanceError));
        System.out.println(ADDED_BALANCE_IN_ADDITIONAL_DEVICE + " - " +
                MerchantTransactionHistoryIndexPage._refundAdditionalDeviceName);

        boolean bool = getText(lblDeviceBalanceError).contains(ADDED_BALANCE_IN_ADDITIONAL_DEVICE + " - " +
                MerchantTransactionHistoryIndexPage._refundAdditionalDeviceName) && isElementDisplay(btnRefundCompleteOK);

        clickOn(driver, btnRefundCompleteOK);

        return bool;

    }

    public boolean verifyAmountWithdrawFromMerchantToAddDevice() {

        pause(3);

        return MerchantTransactionHistoryIndexPage._refundAmount == getDoubleFromString(getText(lstTransactionDetails.get(1))) &&
                getText(lstTransactionDetails.get(lastIndexOf(lstTransactionDetails))).equalsIgnoreCase(TYPE_WITHDRAW);

    }

    public boolean verifyAmountDepositFromMerchantToAddDevice() {

        pause(3);

        return MerchantTransactionHistoryIndexPage._refundAmount == getDoubleFromString(getText(lstTransactionDetails.get(1))) &&
                MerchantTransactionHistoryIndexPage._refundReceiverName.equalsIgnoreCase(getText(lstTransactionDetails.get(2))) &&
                MerchantTransactionHistoryIndexPage._refundSenderName.equalsIgnoreCase(getText(lstTransactionDetails.get(3))) &&
                getText(lstTransactionDetails.get(lastIndexOf(lstTransactionDetails))).equalsIgnoreCase(TYPE_DEPOSIT);

    }
}
