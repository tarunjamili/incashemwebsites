package com.incashme.merchant.verification;

import com.framework.init.AbstractPage;
import com.incashme.merchant.indexpage.MerchantDashboardIndexPage;
import com.incashme.merchant.indexpage.MerchantDonationIndexPage;
import com.incashme.merchant.validations.DonationValidations;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MerchantDonationVerification extends AbstractPage implements DonationValidations {

    public MerchantDonationVerification(WebDriver driver) {
        super(driver);
    }

    @FindAll(value = {@FindBy(xpath = "//table[@id='donation-transaction-table']//tr//td[2]")})
    private List<WebElement> lstDonationID;

    @FindBy(xpath = "//table//tr//td[contains(text(),'matching')]")
    private WebElement lblNoRecordsFound;

    @FindBy(xpath = "//input[@type='search']")
    private WebElement txtInputSearch;

    @FindBy(xpath = "//div//p[contains(text(),'any Requested Donation')]")
    private WebElement lblNoDonationHistory;

    @FindBy(xpath = "//li/button[contains(@class,'filterBtn')]")
    private WebElement btnFilter;

    @FindBy(xpath = "//div[contains(@class,'column-chart-div')]")
    private WebElement donationChart;

    @FindBy(xpath = "//label[@id='p_week']")
    private WebElement btnChartWeek;

    @FindBy(xpath = "//label[@id='p_month']")
    private WebElement btnChartMonth;

    @FindBy(xpath = "//label[@id='p_year']")
    private WebElement btnChartYear;

    @FindBy(xpath = "//label[@id='p_total']")
    private WebElement btnChartTotal;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//button[contains(@class,'searchbtn')]")
    private WebElement btnFilterSearch;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//a[contains(text(),'Clear All')]")
    private WebElement btnFilterClearAll;

    @FindBy(xpath = "//select[@formcontrolname='otype']")
    private WebElement btnUserDropdown;

    @FindBy(xpath = "//select[@formcontrolname='type']")
    private WebElement btnTypeDropdown;

    @FindBy(xpath = "//div//p[contains(text(),'find any matches')]")
    private WebElement lblNoFilterDonationAvailable;

    @FindAll(value = {@FindBy(xpath = "//table[@id='donation-transaction-table']//tr//td[1]//h5")})
    private List<WebElement> lstUserName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='donation-transaction-table']//tr//td[1]//h5//following-sibling::div")})
    private List<WebElement> lstUserIdNumber;

    @FindAll(value = {@FindBy(xpath = "//table[@id='donation-transaction-table']//tr//td[4]")})
    private List<WebElement> lstDonationType;

    @FindAll(value = {@FindBy(xpath = "//table[@id='donation-transaction-table']//tr//td[6]//button")})
    private List<WebElement> lstBtnMore;

    @FindBy(xpath = ".//div[contains(@id,'swal2-content') and contains(text(),'Invalid')]")
    private WebElement lblInvalidAmountValidation;

    @FindBy(xpath = ".//div[contains(@class,'swal2-actions')]//button[text()='OK' or text() = 'Ok']")
    private WebElement btnConfirmOK;

    @FindAll(value = {@FindBy(xpath = "//table[@id='donation-transaction-table']//tr//td[3]")})
    private List<WebElement> lstDonationAmount;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@id,'Requested')]//ul/li//div[contains(@class,'dntn-id')]")})
    private List<WebElement> lstRequestedDonationId;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@id,'Requested')]//ul/li//div[contains(@class,'dntn-name')]")})
    private List<WebElement> lstRequestedDonatorName;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@id,'Requested')]//ul/li//div[contains(@class,'dntr-id') or contains(@class,'dntr-no')]")})
    private List<WebElement> lstRequestedDonatorID;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@id,'Requested')]//ul/li//div[contains(@class,'dntr-sts')]")})
    private List<WebElement> lstRequestedDonationStatus;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@id,'Requested')]//ul/li//div[contains(@class,'dntn-date')]")})
    private List<WebElement> lstRequestedDonationDate;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@id,'Sent')]//ul/li//div[contains(@class,'dntn-id')]")})
    private List<WebElement> lstSentDonationId;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@id,'Sent')]//ul/li//div[contains(@class,'dntn-name')]")})
    private List<WebElement> lstSentDonatorName;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@id,'Sent')]//ul/li//div[contains(@class,'dntr-id') or contains(@class,'dntr-no')]")})
    private List<WebElement> lstSentDonatorID;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@id,'Sent')]//ul/li//div[contains(@class,'dntr-sts')]")})
    private List<WebElement> lstSentDonationStatus;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@id,'Sent')]//ul/li//div[contains(@class,'dntn-date')]")})
    private List<WebElement> lstSentDonationDate;

    @FindBy(xpath = "//div[contains(@id,'Requested')]//ul/li//p")
    private WebElement lblNoRequestedDonation;

    @FindBy(xpath = "//div[contains(@id,'Sent')]//ul/li//p")
    private WebElement lblNoSentDonation;

    @FindBy(xpath = "//a[text()='Back']")
    private WebElement btnBack;

    @FindBy(xpath = "//input[@formcontrolname='donationUser']")
    private WebElement txtSenderNumber;

    @FindBy(xpath = "//input[@formcontrolname='donationMessage']")
    private WebElement txtDonationMessage;

    @FindBy(xpath = "//div[contains(@class,'contentdiv')]//button[contains(@class,'btn-pink') and text()='Cancel']")
    private WebElement btnCancelDonationRequest;

    @FindBy(xpath = "//div[contains(@class,'contentdiv')]//button[contains(@class,'btn-green') and text()='Send']")
    private WebElement btnSendDonationRequest;

    @FindBy(xpath = "//div[contains(@class,'contentdiv')]//button[contains(@class,'btn-green') and text()='Pending']")
    private WebElement btnPendingDonationRequest;

    @FindBy(xpath = "//div[contains(@id,'Requested')]//a[contains(text(),'New Donation')]")
    private WebElement btnCreateNewDonation;

    @FindBy(xpath = "//form//span[contains(@class,'color-danger') and contains(text(),'Mobile')]")
    private WebElement lblInvalidMobileNumber;

    @FindBy(xpath = "//div[contains(@class,'filter-suggetion')]//div[contains(@class,'trans-sub-title')]")
    private WebElement lblNoUsersFound;

    @FindBy(xpath = "//div[contains(@id,'mrcnt-make-donation')]//div[contains(@class,'tppup-usr-nm')]")
    private WebElement lblDonationUsername;

    @FindBy(xpath = "//div[contains(@id,'mrcnt-make-donation')]//div[contains(@class,'tppup-usr-nm')]//following-sibling::div")
    private WebElement lblDonationIDNumber;

    @FindBy(xpath = "//div[contains(@class,'donation-qrcd')]/img")
    private WebElement imgQRCode;

    @FindBy(xpath = "//div[contains(@class,'tpup-mnyscs-txt') and contains(text(),'sent success')]")
    private WebElement lblDonationSentSuccess;

    @FindBy(xpath = "//div[contains(@class,'tpup-mnyscs-txt') and contains(text(),'cancelled success')]")
    private WebElement lblDonationCancelSuccess;

    @FindBy(xpath = "//div[contains(@id,'mrcnt-make-donation')]//div[contains(@class,'time-txt')]")
    private WebElement lblDonationSentTime;

    @FindBy(xpath = "//div[contains(@id,'mrcnt-conf')]//div[contains(@class,'time-txt')]")
    private WebElement lblDonationCancelTime;

    @FindBy(xpath = "//div[contains(@class,'dnte-rgt-dtl')]//div[contains(@class,'donater-name')]")
    private WebElement lblDonationCardUserName;

    @FindBy(xpath = "//div[contains(@class,'dnte-rgt-dtl')]//div[contains(@class,'donater-id')]")
    private WebElement lblDonationCardUserID;

    @FindBy(xpath = "//div[contains(@class,'dnte-rgt-dtl')]//div[contains(@class,'donater-no')]")
    private List<WebElement> lblDonationCardID;

    @FindBy(xpath = "//div//div[contains(@class,'msg-text')]")
    private WebElement lblDonationCardMessage;

    @FindBy(xpath = "//input[@formcontrolname='donationAmount']//following-sibling::div[contains(text(),'Amount')]")
    private WebElement lblInvalidAmount;

    @FindBy(xpath = "//div[contains(@id,'mrcnt-conf')]//div[contains(@class,'tppup-usr-nm')]")
    private WebElement lblDonationCancelUsername;

    @FindBy(xpath = "//div[contains(@id,'mrcnt-conf')]//div[contains(@class,'tppup-usr-nm')]//following-sibling::div")
    private WebElement lblDonationCancelIDNumber;

    @FindBy(xpath = "//div[contains(@id,'mrcnt-conf')]//div[contains(@class,'amnt-txt')]")
    private WebElement lblDonationAmountPopUp;

    @FindBy(xpath = "//div[contains(@id,'mrcnt-conf')]//div[contains(@class,'amnt-txt')]")
    private WebElement lblDonationPayAmount;

    @FindBy(xpath = "//div[contains(@id,'mrcnt-conf')]//div[contains(@class,'color-green') and contains(text(),'Congratulation')]")
    private WebElement lblDonationPaySuccess;

    @FindBy(xpath = "//div[contains(@class,'merchant-count')]//span[contains(@class,'ml-auto')]")
    private WebElement lblDonationAmountChart;

    @FindBy(xpath = "//div[contains(@id,'mrcnt-conf')]//div[contains(text(),'Insufficient')]")
    private WebElement lblInsufficientFundMessage;

    public boolean verifyDonationScreen() {

        boolean bool;

        pause(3);
        System.out.println(isElementPresent(lblNoDonationHistory));
        if (isElementPresent(lblNoDonationHistory)) {
            scrollToElement(driver, lblNoDonationHistory);
            testValidationLog(getInnerText(lblNoDonationHistory));
            return isElementDisplay(lblNoDonationHistory) &&
                    getInnerText(lblNoDonationHistory).equalsIgnoreCase(NO_DONATION_FOUND);
        } else {
            bool = isElementDisplay(donationChart) && isElementDisplay(btnFilter);
            scrollToElement(driver, btnChartWeek);
            return bool && isElementDisplay(btnChartWeek) && isElementDisplay(btnChartMonth) &&
                    isElementDisplay(btnChartYear) && isElementDisplay(btnChartTotal);
        }
    }

    public boolean verifySearchSuccessful() {
        boolean bool = true;
        for (WebElement e : lstDonationID) {
            bool = bool && getInnerText(e).contains(MerchantDonationIndexPage._searchCriteria);
        }
        return bool;
    }

    public boolean verifyValidationForInvalidSearch() {

        testValidationLog(getInnerText(lblNoRecordsFound));

        boolean bool = getInnerText(lblNoRecordsFound).equalsIgnoreCase(NO_SEARCH_RECORD_FOUND);

        type(txtInputSearch, "x");
        txtInputSearch.clear();

        return bool;
    }

    public boolean verifyDonationFilterScreenDisplay() {

        try {
            updateShowList(driver, findElementByName(driver, "donation-transaction-table_length"), "100");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        return isElementDisplay(btnFilterSearch) && isElementDisplay(btnFilterClearAll);
    }

    public boolean verifyProfitMerchantFilterType() {
        Select users = new Select(btnUserDropdown);
        Select types = new Select(btnTypeDropdown);
        return sizeOf(users.getOptions()) == 1 && sizeOf(types.getOptions()) == 1;
    }

    public boolean verifyRandomDataFiltered() {
        return getText(lblNoFilterDonationAvailable).equalsIgnoreCase(NO_FILTER_DATA_AVAILABLE);
    }

    public boolean verifyShopNameFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement name : lstUserName) {
            finalFilterResult.add(getInnerText(name));
        }

        System.out.println(finalFilterResult);
        System.out.println(MerchantDonationIndexPage._filterShopName);

        return finalFilterResult.contains(MerchantDonationIndexPage._filterShopName);
    }

    public boolean verifyMerchantIDFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement number : lstUserIdNumber) {
            finalFilterResult.add(getInnerText(number));
        }

        System.out.println(finalFilterResult);
        System.out.println(MerchantDonationIndexPage._filterMerchantId);

        return finalFilterResult.contains(MerchantDonationIndexPage._filterMerchantId);
    }

    public boolean verifyDonationIDFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement number : lstDonationID) {
            finalFilterResult.add(String.valueOf(getIntegerFromString(getInnerText(number))));
        }

        System.out.println(finalFilterResult);
        System.out.println(MerchantDonationIndexPage._filterDonationId);

        return finalFilterResult.contains(MerchantDonationIndexPage._filterDonationId) &&
                sizeOf(finalFilterResult) == 1;
    }

    public boolean verifyStatusFiltered() {

        List<String> finalFilterResult = new ArrayList<>();
        boolean bool = false;

        pause(2);

        for (WebElement status : lstDonationType) {
            finalFilterResult.add(getInnerText(status));
        }

        System.out.println(finalFilterResult);
        System.out.println(MerchantDonationIndexPage._filterDonationStatusType);

        switch (MerchantDonationIndexPage._filterDonationStatusType.toLowerCase()) {
            case "pending":
                if (isListEmpty(finalFilterResult)) {
                    testValidationLog(getText(lblNoFilterDonationAvailable));
                    bool = getText(lblNoFilterDonationAvailable).equalsIgnoreCase(NO_FILTER_DATA_AVAILABLE);
                } else {
                    System.out.println(finalFilterResult.stream().distinct().limit(2).count());
                    bool = finalFilterResult.stream().distinct().limit(2).count() == 1 &&
                            finalFilterResult.contains(DONATION_PENDING) &&
                            sizeOf(lstBtnMore) == sizeOf(finalFilterResult);
                }
                break;
            case "cancelled":
                if (isListEmpty(finalFilterResult)) {
                    testValidationLog(getText(lblNoFilterDonationAvailable));
                    bool = getText(lblNoFilterDonationAvailable).equalsIgnoreCase(NO_FILTER_DATA_AVAILABLE);
                } else {
                    System.out.println(finalFilterResult.stream().distinct().limit(2).count());
                    bool = finalFilterResult.stream().distinct().limit(2).count() == 1 &&
                            finalFilterResult.contains(DONATION_CANCELLED);
                }
                break;
            case "donated":
                if (isListEmpty(finalFilterResult)) {
                    testValidationLog(getText(lblNoFilterDonationAvailable));
                    bool = getText(lblNoFilterDonationAvailable).equalsIgnoreCase(NO_FILTER_DATA_AVAILABLE);
                } else {
                    System.out.println(finalFilterResult.stream().distinct().limit(2).count());
                    bool = finalFilterResult.stream().distinct().limit(2).count() == 1 &&
                            finalFilterResult.contains(DONATION_COMPLETED);
                }
                break;
            case "donation received":
                if (isListEmpty(finalFilterResult)) {
                    testValidationLog(getText(lblNoFilterDonationAvailable));
                    bool = getText(lblNoFilterDonationAvailable).equalsIgnoreCase(NO_FILTER_DATA_AVAILABLE);
                } else {
                    System.out.println(finalFilterResult.stream().distinct().limit(2).count());
                    bool = finalFilterResult.stream().distinct().limit(2).count() == 1 &&
                            finalFilterResult.contains(DONATION_RECEIVED);
                }
                break;
        }

        return bool;
    }

    public boolean verifyMaxMinAmountValidation() {

        boolean bool = getText(lblInvalidAmountValidation).equalsIgnoreCase(INVALID_AMOUNT_RANGE);

        testValidationLog(getText(lblInvalidAmountValidation));
        clickOn(driver, btnConfirmOK);

        return bool;
    }

    public boolean verifyDonationAmountRangeFilter() {

        List<Double> finalFilterResult = new ArrayList<>();

        for (WebElement name : lstDonationAmount) {
            finalFilterResult.add(getDoubleFromString(getInnerText(name)));
        }

        System.out.println(finalFilterResult);
        System.out.println(MerchantDonationIndexPage._filterDonationFromAmount);
        System.out.println(MerchantDonationIndexPage._filterDonationToAmount);

        return findMin(finalFilterResult) >= MerchantDonationIndexPage._filterDonationFromAmount &&
                findMax(finalFilterResult) <= MerchantDonationIndexPage._filterDonationToAmount;

    }

    public boolean verifyDonationAmountFromFieldFilter() {

        List<Double> finalFilterResult = new ArrayList<>();

        for (WebElement name : lstDonationAmount) {
            finalFilterResult.add(getDoubleFromString(getInnerText(name)));
        }

        System.out.println(finalFilterResult);
        System.out.println(MerchantDonationIndexPage._filterDonationFromAmount);

        return !isListEmpty(finalFilterResult) &&
                findMin(finalFilterResult) >= MerchantDonationIndexPage._filterDonationFromAmount;
    }

    public boolean verifyDonationAmountToFieldFilter() {

        List<Double> finalFilterResult = new ArrayList<>();

        for (WebElement name : lstDonationAmount) {
            finalFilterResult.add(getDoubleFromString(getInnerText(name)));
        }

        System.out.println(finalFilterResult);
        System.out.println(MerchantDonationIndexPage._filterDonationToAmount);

        return !isListEmpty(finalFilterResult) &&
                findMax(finalFilterResult) <= MerchantDonationIndexPage._filterDonationToAmount;
    }

    public boolean verifyFirstNameFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement name : lstUserName) {
            finalFilterResult.add(getInnerText(name).split(" ")[0].trim());
        }

        System.out.println(finalFilterResult);
        System.out.println(MerchantDonationIndexPage._filterFirstName);

        return finalFilterResult.contains(MerchantDonationIndexPage._filterFirstName);
    }

    public boolean verifyLastNameFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement name : lstUserName) {
            finalFilterResult.add(getInnerText(name).replace(MerchantDonationIndexPage._filterFirstName, "").trim());
        }

        System.out.println(finalFilterResult);
        System.out.println(MerchantDonationIndexPage._filterLastName);

        return finalFilterResult.contains(MerchantDonationIndexPage._filterLastName);
    }

    public boolean verifyPhoneNumberFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement number : lstUserIdNumber) {
            finalFilterResult.add(getInnerText(number));
        }

        System.out.println(finalFilterResult);
        System.out.println(MerchantDonationIndexPage._filterPhone);

        return finalFilterResult.contains(MerchantDonationIndexPage._filterPhone);
    }

    public boolean verifyFilteredDonationTypesForSent() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement status : lstDonationType) {
            finalFilterResult.add(getInnerText(status));
        }

        if (!isListEmpty(finalFilterResult)) {
            Set<String> status = new HashSet<>(finalFilterResult);
            System.out.println(status);

            status.remove(DONATION_RECEIVED);
            status.remove(DONATION_CANCELLED);
            status.remove(DONATION_PENDING);

            return status.isEmpty();
        } else {
            testValidationLog(getText(lblNoFilterDonationAvailable));
            return getText(lblNoFilterDonationAvailable).equalsIgnoreCase(NO_FILTER_DATA_AVAILABLE);
        }

    }

    public boolean verifyFilteredDonationTypesForRequestedConsumer() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement status : lstDonationType) {
            finalFilterResult.add(getInnerText(status));
        }

        if (!isListEmpty(finalFilterResult)) {
            Set<String> status = new HashSet<>(finalFilterResult);
            System.out.println(status);

            status.remove(DONATION_COMPLETED);
            status.remove(DONATION_CANCELLED);
            status.remove(DONATION_PENDING);

            return status.isEmpty();
        } else {
            testValidationLog(getText(lblNoFilterDonationAvailable));
            return getText(lblNoFilterDonationAvailable).equalsIgnoreCase(NO_FILTER_DATA_AVAILABLE);
        }

    }

    public boolean verifyRequestedDonationDetails() {
        if (!isListEmpty(lstRequestedDonationId)) {
            boolean bool = true;
            testInfoLog("Total Received Donations", String.valueOf(sizeOf(lstRequestedDonationId)));
            for (int donation = 0; donation < sizeOf(lstRequestedDonationId); donation++) {
                testInfoLog("Donation Details", "\nDonation ID : " + getInnerText(lstRequestedDonationId.get(donation)) +
                        "\nDonation Sender Name & ID : " + getInnerText(lstRequestedDonatorName.get(donation)) + "  " +
                        getInnerText(lstRequestedDonatorID.get(donation)) +
                        "\nDonation Status : " + getInnerText(lstRequestedDonationStatus.get(donation)) +
                        "\nDonation Date : " + getInnerText(lstRequestedDonationDate.get(donation)));

                bool = bool && getInnerText(lstRequestedDonationStatus.get(donation)).equalsIgnoreCase(DONATION_PENDING_CARD);
            }
            return bool;
        } else {
            testValidationLog(getInnerText(lblNoRequestedDonation));
            return isElementDisplay(lblNoRequestedDonation) &&
                    getInnerText(lblNoRequestedDonation).equalsIgnoreCase(NO_REQUESTED_DONATION);
        }
    }

    public boolean verifySentDonationDetails() {
        if (!isListEmpty(lstSentDonationId)) {
            boolean bool = true;
            testInfoLog("Total Sent Donations", String.valueOf(sizeOf(lstSentDonationId)));
            for (int donation = 0; donation < sizeOf(lstSentDonationId); donation++) {
                testInfoLog("Donation Details", "\nDonation ID : " + getInnerText(lstSentDonationId.get(donation)) +
                        "\nDonation Sender Name & ID : " + getInnerText(lstSentDonatorName.get(donation)) + "  " +
                        getInnerText(lstSentDonatorID.get(donation)) +
                        "\nDonation Status : " + getInnerText(lstSentDonationStatus.get(donation)) +
                        "\nDonation Date : " + getInnerText(lstSentDonationDate.get(donation)));

                bool = bool && getInnerText(lstSentDonationStatus.get(donation)).equalsIgnoreCase(DONATION_PENDING_CARD);
            }
            return bool;
        } else {
            testValidationLog(getInnerText(lblNoSentDonation));
            return isElementDisplay(lblNoSentDonation) &&
                    getInnerText(lblNoSentDonation).equalsIgnoreCase(NO_SENT_DONATION);
        }
    }

    public boolean verifyCreateNewDonationScreen() {

        pause(3);

        boolean bool = isElementDisplay(btnBack) && isElementDisplay(txtSenderNumber);

        scrollToElement(driver, btnSendDonationRequest);

        return bool && isElementDisplay(txtDonationMessage) && isElementDisplay(btnCancelDonationRequest) &&
                isElementDisplay(btnSendDonationRequest) && !isElementPresent(btnCreateNewDonation);
    }

    public boolean verifyCreateDonationScreenIsClosed() {
        pause(1);
        return isElementDisplay(btnCreateNewDonation) && !isElementPresent(btnBack);
    }

    public boolean verifyValidationMessageForInvalidMobileNumber() {
        testValidationLog(getText(lblInvalidMobileNumber));
        return isElementDisplay(lblInvalidMobileNumber) &&
                getText(lblInvalidMobileNumber).equalsIgnoreCase(INVALID_MOBILE_NUMBER);
    }

    public boolean verifyValidationForUserNotFound() {
        pause(3);
        testValidationLog(getText(lblNoUsersFound));
        return isElementDisplay(lblNoUsersFound) &&
                getText(lblNoUsersFound).equalsIgnoreCase(NO_USERS_FOUND);
    }

    public boolean verifyDonationPopUpClosed() {
        pause(5);
        return isElementDisplay(btnBack);
    }


    public boolean verifyDonationDetailsForMerchantBeforeSubmitDonation() {

        pause(3);

        System.out.println(getText(lblDonationUsername));
        System.out.println(MerchantDonationIndexPage._donationUserName);
        System.out.println(getText(lblDonationIDNumber));
        System.out.println(MerchantDonationIndexPage._donationUserNumber);

        return getText(lblDonationUsername).equalsIgnoreCase(MerchantDonationIndexPage._donationUserName) &&
                getIntegerFromString(getText(lblDonationIDNumber)) ==
                        getIntegerFromString(MerchantDonationIndexPage._donationUserNumber) &&
                isElementDisplay(imgQRCode);

    }

    public boolean verifyDonationDetailsForMerchantAfterSent() {

        pause(3);

        System.out.println(getText(lblDonationUsername));
        System.out.println(MerchantDonationIndexPage._donationUserName);
        System.out.println(getText(lblDonationIDNumber));
        System.out.println(MerchantDonationIndexPage._donationUserNumber);

        boolean bool = getText(lblDonationUsername).equalsIgnoreCase(MerchantDonationIndexPage._donationUserName) &&
                getIntegerFromString(getText(lblDonationIDNumber)) ==
                        getIntegerFromString(MerchantDonationIndexPage._donationUserNumber) &&
                isElementDisplay(imgQRCode);

        testConfirmationLog(getText(lblDonationSentSuccess));
        testInfoLog("Donation Sent Time", getText(lblDonationSentTime));

        return bool;
    }

    public boolean verifyDonationDetailsForMerchantAfterCancelledFromReceiver() {

        pause(3);

        System.out.println(getText(lblDonationCancelIDNumber));
        System.out.println(MerchantDonationIndexPage._donationUserNumber);

        boolean bool = getIntegerFromString(getText(lblDonationCancelIDNumber)) ==
                getIntegerFromString(MerchantDonationIndexPage._donationUserNumber);

        testConfirmationLog(getText(lblDonationCancelSuccess));
        testInfoLog("Donation Cancel Time", getText(lblDonationCancelTime));

        return bool;
    }

    public boolean verifyDonationDetailsForMerchantAfterCancelledFromSender() {

        pause(3);

        testConfirmationLog(getText(lblDonationCancelSuccess));
        testInfoLog("Donation Cancel Time", getText(lblDonationCancelTime));

        return isElementDisplay(lblDonationCancelSuccess);
    }

    public boolean verifyCreatedDonationDetailsForMerchant() {

        pause(3);

        System.out.println(getText(lblDonationCardUserID));
        System.out.println(MerchantDonationIndexPage._donationUserNumber);

        boolean bool = getIntegerFromString(getText(lblDonationCardUserID)) ==
                getIntegerFromString(MerchantDonationIndexPage._donationUserNumber);


        if (!MerchantDonationIndexPage._donationMessage.isEmpty()) {
            bool = bool && getText(lblDonationCardMessage).equalsIgnoreCase(MerchantDonationIndexPage._donationMessage);
        } else {
            bool = bool && !isElementPresent(lblDonationCardMessage);
        }

        testInfoLog("Donation ID", getText(lstSentDonationId.get(0)));

        MerchantDonationIndexPage._donationID = getText(lstSentDonationId.get(0));

        return bool;
    }

    public boolean verifyCreatedDonationDetailsForConsumer() {

        pause(3);

        String expectedNumber = getText(lblDonationCardID.get(0)).
                substring(getText(lblDonationCardID.get(0)).length() - 10);
        System.out.println(expectedNumber);
        String actualNumber = MerchantDonationIndexPage._donationUserNumber.
                substring(MerchantDonationIndexPage._donationUserNumber.length() - 10);
        System.out.println(actualNumber);

        boolean bool = actualNumber.equalsIgnoreCase(expectedNumber);

        if (!MerchantDonationIndexPage._donationMessage.isEmpty()) {
            bool = bool && getText(lblDonationCardMessage).equalsIgnoreCase(MerchantDonationIndexPage._donationMessage);
        } else {
            bool = bool && !isElementPresent(lblDonationCardMessage);
        }

        testInfoLog("Donation ID", getText(lstSentDonationId.get(0)));

        return bool;
    }

    public boolean verifyReceivedMerchantDonationDetails() {

        pause(3);

        if (!MerchantDonationIndexPage._donationMessage.isEmpty()) {
            return getText(lblDonationCardMessage).equalsIgnoreCase(MerchantDonationIndexPage._donationMessage);
        } else {
            return !isElementPresent(lblDonationCardMessage);
        }
    }

    public boolean verifyInvalidAmountValidation() {
        testValidationLog(getText(lblInvalidAmount));
        return isElementDisplay(lblInvalidAmount) &&
                getText(lblInvalidAmount).equalsIgnoreCase(INVALID_DONATION_AMOUNT);
    }

    public boolean verifyPayDonationConfirmationDetails() {

        pause(3);

        System.out.println(formatTwoDecimal(getDoubleFromString(getText(lblDonationAmountPopUp))));
        System.out.println(MerchantDonationIndexPage._donationAmount);

        return formatTwoDecimal(getDoubleFromString(getText(lblDonationAmountPopUp))) ==
                MerchantDonationIndexPage._donationAmount;

    }

    public boolean verifyDonationPayCompletedPopUp() {

        pause(3);

        System.out.println(formatTwoDecimal(getDoubleFromString(getText(lblDonationPayAmount))));
        System.out.println(MerchantDonationIndexPage._donationAmount);

        boolean bool = formatTwoDecimal(getDoubleFromString(getText(lblDonationPayAmount))) ==
                MerchantDonationIndexPage._donationAmount;

        testConfirmationLog(getText(lblDonationPaySuccess));

        return bool;
    }

    public boolean verifyDonationIDNotDisplayInRequested() {
        boolean bool = true;

        for (int donation = 0; donation < sizeOf(lstRequestedDonationId); donation++) {
            if (getInnerText(lstRequestedDonationId.get(donation)).
                    equalsIgnoreCase(MerchantDonationIndexPage._donationID)) {
                bool = false;
                break;
            }
        }
        return bool;
    }

    public boolean verifyDonatedDonationStatus() {
        pause(3);
        return sizeOf(lstUserIdNumber) == 1 && getText(lstDonationType.get(0)).equalsIgnoreCase(DONATION_COMPLETED);
    }

    public boolean verifyBalanceDeductedForPaidDonation() {

        pause(5);
        MerchantDashboardIndexPage.getCurrentBalance();
        double updatedBalance = formatTwoDecimal(getDoubleFromString(MerchantDashboardIndexPage._currentBalance));

        System.out.println(updatedBalance);
        System.out.println(MerchantDonationIndexPage._donationSenderBalance);
        System.out.println(MerchantDonationIndexPage._donationAmount);

        return formatTwoDecimal(MerchantDonationIndexPage._donationSenderBalance -
                MerchantDonationIndexPage._donationAmount) == formatTwoDecimal(updatedBalance);

    }

    public boolean verifyDonationAmountAddedInDonatedChartForPaid() {

        pause(3);
        double updatedAmount = getDoubleFromString(getInnerText(lblDonationAmountChart).split(",")[0]);

        System.out.println(updatedAmount);
        System.out.println(MerchantDonationIndexPage._payDonationPaidChart);
        System.out.println(MerchantDonationIndexPage._donationAmount);

        return formatTwoDecimal(MerchantDonationIndexPage._payDonationPaidChart +
                MerchantDonationIndexPage._donationAmount) == formatTwoDecimal(updatedAmount);

    }

    public boolean verifyBalanceAddedForPaidDonation() {

        pause(5);
        MerchantDashboardIndexPage.getCurrentBalance();
        double updatedBalance = formatTwoDecimal(getDoubleFromString(MerchantDashboardIndexPage._currentBalance));

        System.out.println(updatedBalance);
        System.out.println(MerchantDonationIndexPage._donationRequesterBalance);
        System.out.println(MerchantDonationIndexPage._donationAmount);

        return formatTwoDecimal(MerchantDonationIndexPage._donationRequesterBalance +
                MerchantDonationIndexPage._donationAmount) == formatTwoDecimal(updatedBalance);

    }

    public boolean verifyDonationAmountAddedInDonatedChartForReceived() {

        pause(3);
        double updatedAmount = getDoubleFromString(getInnerText(lblDonationAmountChart).split(",")[0]);

        System.out.println(updatedAmount);
        System.out.println(MerchantDonationIndexPage._receiverDonationReceivedChart);
        System.out.println(MerchantDonationIndexPage._donationAmount);

        return formatTwoDecimal(MerchantDonationIndexPage._receiverDonationReceivedChart +
                MerchantDonationIndexPage._donationAmount) == formatTwoDecimal(updatedAmount);

    }

    public boolean verifyReceivedDonationStatus() {
        pause(3);
        System.out.println(getText(lstDonationType.get(0)));
        System.out.println(DONATION_RECEIVED);
        return sizeOf(lstUserIdNumber) == 1 &&
                getText(lstDonationType.get(0)).equalsIgnoreCase(DONATION_RECEIVED);
    }

    public boolean verifyDonationIDNotDisplayInSent() {
        boolean bool = true;

        for (int donation = 0; donation < sizeOf(lstSentDonationId); donation++) {
            if (getInnerText(lstSentDonationId.get(donation)).equalsIgnoreCase(MerchantDonationIndexPage._donationID)) {
                bool = false;
                break;
            }
        }
        return bool;
    }

    public boolean verifyDeletedDonationCancelled() {
        pause(3);
        return sizeOf(lstUserIdNumber) == 1 &&
                getText(lstDonationType.get(0)).equalsIgnoreCase(DONATION_CANCELLED);
    }

    public boolean verifyDonationDetailsForConsumerBeforeSubmitInInvoice() {

        pause(3);

        System.out.println(getText(lblDonationUsername));
        System.out.println(MerchantDonationIndexPage._donationUserName);
        System.out.println(getText(lblDonationIDNumber).substring(getText(lblDonationIDNumber).length() - 10));
        System.out.println(MerchantDonationIndexPage._donationUserNumber);

        String expectedNumber = getText(lblDonationIDNumber).substring(getText(lblDonationIDNumber).length() - 10);
        System.out.println(expectedNumber);
        String actualNumber = MerchantDonationIndexPage._donationUserNumber.
                substring(MerchantDonationIndexPage._donationUserNumber.length() - 10);
        System.out.println(actualNumber);

        return getText(lblDonationUsername).equalsIgnoreCase(MerchantDonationIndexPage._donationUserName) &&
                expectedNumber.equalsIgnoreCase(actualNumber);
    }

    public boolean verifyDonationDetailsForConsumerAfterSent() {

        pause(3);

        System.out.println(getText(lblDonationUsername));
        System.out.println(MerchantDonationIndexPage._donationUserName);
        System.out.println(getText(lblDonationIDNumber).substring(getText(lblDonationIDNumber).length() - 10));
        System.out.println(MerchantDonationIndexPage._donationUserNumber);

        String expectedNumber = getText(lblDonationIDNumber).substring(getText(lblDonationIDNumber).length() - 10);
        System.out.println(expectedNumber);
        String actualNumber = MerchantDonationIndexPage._donationUserNumber.
                substring(MerchantDonationIndexPage._donationUserNumber.length() - 10);
        System.out.println(actualNumber);

        boolean bool = getText(lblDonationUsername).equalsIgnoreCase(MerchantDonationIndexPage._donationUserName) &&
                expectedNumber.equalsIgnoreCase(actualNumber);

        testConfirmationLog(getText(lblDonationSentSuccess));
        testInfoLog("Donation Sent Time", getText(lblDonationSentTime));

        return bool;
    }

    public boolean verifyInsufficientFundMessage() {
        testValidationLog(getText(lblInsufficientFundMessage));
        return isElementDisplay(lblInsufficientFundMessage) &&
                getText(lblInsufficientFundMessage).equalsIgnoreCase(INSUFFICIENT_FUND_MESSAGE);
    }
}
