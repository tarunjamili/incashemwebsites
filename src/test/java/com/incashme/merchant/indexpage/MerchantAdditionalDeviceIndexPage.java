package com.incashme.merchant.indexpage;

import com.framework.init.AbstractPage;
import com.incashme.merchant.verification.MerchantAdditionalDeviceVerification;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class MerchantAdditionalDeviceIndexPage extends AbstractPage {

    public MerchantAdditionalDeviceIndexPage(WebDriver driver) {
        super(driver);
    }

    public static double _additionalDeviceBalance = 0.0d;

    @FindBy(xpath = "//ul/li//a[@title='Additional Device']")
    private WebElement btnMenuAddDevice;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@id,'addi_active')]//div[contains(@class,'dntr-id')]")})
    private List<WebElement> lstActiveDeviceID;

    @FindBy(xpath = "//div[contains(@class,'addi_pro_det')]//p[contains(@class,'addi_p_dt_d')]")
    private WebElement lblAdditionalDeviceBalance;

    @FindBy(xpath = "//ul//li/a[contains(text(),'Active')]")
    private WebElement btnActiveTab;

    public MerchantAdditionalDeviceVerification clickOnAdditionalDeviceMenu() {

        testStepsLog(_logStep++, "Click on Additional Device button.");
        clickOn(driver, btnMenuAddDevice);

        return new MerchantAdditionalDeviceVerification(driver);
    }

    public MerchantAdditionalDeviceVerification openAdditionalDeviceInNewWindow() {
        testStepsLog(_logStep++, "Click on Additional Device button.");
        openLinkInNewWindow(driver, btnMenuAddDevice.getAttribute("href"));
        return new MerchantAdditionalDeviceVerification(driver);
    }

    public MerchantAdditionalDeviceVerification openAndGetAdditionalDeviceBalance() {
        clickOn(driver, btnActiveTab);
        for (WebElement device : lstActiveDeviceID) {
            if (getInnerText(device).contains(MerchantTransactionHistoryIndexPage._refundReceiverName.split("\n")[0])) {
                testStepsLog(_logStep++, "Click on Additional Device to open.");
                scrollToElement(driver, device);
                clickOn(driver, device);
                pause(5);
                _additionalDeviceBalance = getDoubleFromString(getInnerText(lblAdditionalDeviceBalance));
                break;
            }
        }
        return new MerchantAdditionalDeviceVerification(driver);
    }

    public MerchantAdditionalDeviceVerification closeAdditionalDeviceWindow() {
        closeCurrentWindow(driver);
        return new MerchantAdditionalDeviceVerification(driver);
    }
}
