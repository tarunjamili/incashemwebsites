package com.incashme.merchant.indexpage;

import com.framework.init.AbstractPage;
import com.incashme.merchant.verification.MerchantStatementVerification;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class MerchantStatementIndexPage extends AbstractPage {

    public MerchantStatementIndexPage(WebDriver driver) {
        super(driver);
    }

    public static String _statementMonthYear = "";
    public static String _statementMonth = "";
    public static String _statementYear = "";

    public static int _totalStatements = 0;

    @FindBy(xpath = "//div[contains(@class,'profile-details')]//following-sibling::div[contains(text(),'Since')]")
    private WebElement lblProfileSince;

    @FindBy(xpath = "//li/button[contains(@class,'filterBtn')]")
    private WebElement btnFilter;

    @FindBy(xpath = "//p[contains(@class,'m_reset_btn')]")
    private WebElement btnFilterOK;

    @FindBy(xpath = "//button[@title]")
    private List<WebElement> lstBtnYear;

    @FindBy(xpath = "//div[contains(@class,'dp-months-row')]//button")
    private List<WebElement> lstBtnMonth;

    @FindAll(value = {@FindBy(xpath = "//div[@class='stmnt-cntnt']")})
    private List<WebElement> lstStatements;

    @FindAll(value = {@FindBy(xpath = "//div[@class='stmnt-view']//button")})
    private List<WebElement> lstStatementDownload;

    @FindBy(xpath = "//ul/li//a[@title='Statement']")
    private WebElement btnMenuStatement;

    public MerchantStatementVerification clickOnStatementMenu() {

        testStepsLog(_logStep++, "Click on Statement button.");
        clickOn(driver, btnMenuStatement);

        return new MerchantStatementVerification(driver);
    }

    public MerchantStatementVerification clickOnViewButton() {

        _statementMonthYear = getText(lstStatements.get(0)).split(" ")[1] + "-" +
                getText(lstStatements.get(0)).split(" ")[0].substring(0, 3);

        testStepsLog(_logStep++, "Click on View Button.");
        clickOnJS(driver, lstStatementDownload.get(0));

        pause(20);

        return new MerchantStatementVerification(driver);
    }

    public boolean isStatementDisplay() {
        _totalStatements = sizeOf(lstStatements);
        return !isListEmpty(lstStatements);
    }

    public MerchantStatementVerification clickOnFilterButton() {

        testStepsLog(_logStep++, "Click on Filter Button.");
        clickOn(driver, btnFilter);

        return new MerchantStatementVerification(driver);
    }

    public MerchantStatementVerification selectStatementToFilter(boolean isInvalid) {

        clickOnFilterButton();

        if (isInvalid) {
            DateTimeFormatter format = DateTimeFormatter.ofPattern("MMM");
            _statementMonth = format.format(LocalDate.now());
            _statementYear = getText(lstStatements.get(0)).split(" ")[1].trim();
        } else {
            _statementMonth = getText(lstStatements.get(0)).split(" ")[0].trim();
            _statementYear = getText(lstStatements.get(0)).split(" ")[1].trim();
        }

        for (int month = 0; month < sizeOf(lstBtnMonth); month++) {
            if (getText(lstBtnMonth.get(month)).equalsIgnoreCase(_statementMonth)) {
                clickOn(driver, lstBtnMonth.get(month));
            }
        }

        clickOnFilterOKButton();

        return new MerchantStatementVerification(driver);

    }

    private void clickOnFilterOKButton() {
        testStepsLog(_logStep++, "Click on OK Button.");
        clickOn(driver, btnFilterOK);
    }
}
