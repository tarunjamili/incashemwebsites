package com.incashme.merchant.indexpage;

import com.framework.init.AbstractPage;
import com.incashme.merchant.verification.MerchantInvoiceVerification;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MerchantInvoiceIndexPage extends AbstractPage {

    public MerchantInvoiceIndexPage(WebDriver driver) {
        super(driver);
    }

    public static String _searchCriteria = "";
    public static String _filterRandomValue = "";

    public static boolean _isRandomFilter;

    public static String _filterFirstName = "";
    public static String _filterLastName = "";
    public static String _filterShopName = "";
    public static String _filterMerchantId = "";
    public static String _filterPhone = "";
    public static String _filterInvoiceId = "";
    public static String _filterInvoiceStatusType = "";
    public static double _filterTransactionFromAmount = 0.0d;
    public static double _filterTransactionToAmount = 0.0d;

    public static String _invoiceUserName = "";
    public static String _invoiceUserNumber = "";
    public static double _invoiceAmount = 0.0d;
    public static String _invoiceMessage = "";
    public static String _invoiceID = "";

    public static double _invoiceSenderBalance = 0.0d;
    public static double _invoiceReceiverBalance = 0.0d;

    public static List<Double> _transactionAmount = new ArrayList<>();

    @FindBy(xpath = "//ul/li//a[@title='Invoice']")
    private WebElement btnMenuInvoice;

    @FindBy(xpath = "//div//p[contains(text(),'no data alive')]")
    private WebElement lblNoInvoices;

    @FindBy(xpath = "//input[@type='search']")
    private WebElement txtInputSearch;

    @FindBy(xpath = "//li/button[contains(@class,'filterBtn')]")
    private WebElement btnFilter;

    @FindAll(value = {@FindBy(xpath = "//table[@id='invoice-history-table']//tr//td[2]")})
    private List<WebElement> lstInvoiceID;

    @FindAll(value = {@FindBy(xpath = "//table[@id='invoice-history-table']//tr//td[1]//h5")})
    private List<WebElement> lstUserName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='invoice-history-table']//tr//td[1]//following-sibling::div")})
    private List<WebElement> lstUserIdNumber;

    @FindAll(value = {@FindBy(xpath = "//table[@id='invoice-history-table']//tr//td[3]")})
    private List<WebElement> lstInvoiceAmount;

    @FindBy(xpath = "//select[@formcontrolname='otype']")
    private WebElement btnUserDropdown;

    @FindBy(xpath = "//select[@formcontrolname='type']")
    private WebElement btnTypeDropdown;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//button[contains(@class,'searchbtn')]")
    private WebElement btnFilterSearch;

    @FindBy(xpath = "//input[@formcontrolname='fname']")
    private WebElement txtFilterFirstName;

    @FindBy(xpath = "//input[@formcontrolname='lname']")
    private WebElement txtFilterLastName;

    @FindBy(xpath = "//input[@formcontrolname='mib_phone']")
    private WebElement txtFilterConsumerMobile;

    @FindBy(xpath = "//input[@formcontrolname='id']")
    private WebElement txtFilterInvoiceID;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//a[contains(text(),'Clear All')]")
    private WebElement btnFilterClearAll;

    @FindBy(xpath = "//select[@formcontrolname='status']")
    private WebElement btnInvoiceStatusTypeDropDown;

    @FindBy(xpath = "//input[@formcontrolname='from_amount']")
    private WebElement txtFilterInvoiceFromAmount;

    @FindBy(xpath = "//input[@formcontrolname='to_amount']")
    private WebElement txtFilterInvoiceToAmount;

    @FindBy(xpath = "//input[@formcontrolname='mia_companyname']")
    private WebElement txtFilterShopName;

    @FindAll(value = {@FindBy(xpath = "//*[contains(text(),'Invoice History Search Filter')]//following-sibling::form//input[not(contains(@class,'selection'))]")})
    private List<WebElement> lstFilterFields;

    @FindBy(xpath = "//input[@formcontrolname='other_user_id']")
    private WebElement txtFilterID;

    @FindBy(xpath = "//input[@formcontrolname='other_user_id']//following-sibling::div//a")
    private WebElement btnClearMerchantID;

    @FindBy(xpath = "//input[@formcontrolname='id']//following-sibling::div//a")
    private WebElement btnClearInvoiceID;

    @FindBy(xpath = "(//input[@formcontrolname='mia_companyname']//following-sibling::div//a)[1]")
    private WebElement btnClearShopName;

    @FindBy(xpath = "//select[@formcontrolname='status']/../..//following-sibling::div//a")
    private WebElement btnClearInvoiceStaus;

    @FindBy(xpath = "//input[@formcontrolname='to_amount']//following-sibling::div//a")
    private WebElement btnClearAmountRange;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@class,'donation-sldr')]/div/ul/li")})
    private List<WebElement> lstInvoiceTabButtons;

    @FindBy(xpath = "//div[contains(@id,'Received')]//a[contains(text(),'New Invoice')]")
    private WebElement btnCreateNewInvoice;

    @FindBy(xpath = "//button[text()='Back']")
    private WebElement btnBack;

    @FindBy(xpath = "//input[@formcontrolname='invoiceUser']")
    private WebElement txtInvoiceMobileNumber;

    @FindBy(xpath = "//input[@formcontrolname='invoiceAmount']")
    private WebElement txtInvoiceAmount;

    @FindBy(xpath = "//input[@formcontrolname='invoiceMessage']")
    private WebElement txtInvoiceMessage;

    @FindBy(xpath = "//div[contains(@class,'agnt-cnf-ftr')]//button[contains(@class,'btn-pink') and text()='Cancel']")
    private WebElement btnCancelInvoice;

    @FindBy(xpath = "//div[contains(@class,'contentdiv')]//button[contains(@class,'btn-green') and text()='Next']")
    private WebElement btnNextInvoiceCreate;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@class,'filter-suggetion')]//div[contains(@class,'badge')]")})
    private List<WebElement> lstUserList;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@class,'filter-suggetion')]//h5[contains(@class,'user-name')]")})
    private List<WebElement> lstInvoiceUserName;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@class,'filter-suggetion')]//div[contains(@class,'trans-sub-title')]")})
    private List<WebElement> lstInvoiceUserMobileID;

    @FindBy(xpath = "//button[contains(@class,'btn-green') and text()='Send']")
    private WebElement btnSendInvoice;

    @FindBy(xpath = "//button[contains(@class,'btn-green') and text()='Confirm']")
    private WebElement btnConfirmDeleteInvoice;

    @FindBy(xpath = "//button[contains(@class,'btn-green') and text()='Confirm']")
    private WebElement btnConfirmPayInvoice;

    @FindBy(xpath = "//div[contains(@id,'create-confirm')]//button[contains(@class,'btn-green') and contains(text(),'Home')]")
    private WebElement btnBackToHomeCreated;

    @FindBy(xpath = "//div[contains(@id,'pay_merchant')]//button[contains(@class,'btn-green') and contains(text(),'Home')]")
    private WebElement btnBackToHomeDeleted;

    @FindBy(xpath = "//div[contains(@id,'pay_merchant')]//button[contains(@class,'btn-green') and contains(text(),'Home')]")
    private WebElement btnBackToHomePay;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@id,'Sent')]//ul/li//div[contains(@class,'dntn-id')]")})
    private List<WebElement> lstSentInvoicesId;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@id,'Received')]//ul/li//div[contains(@class,'dntn-id')]")})
    private List<WebElement> lstReceivedInvoicesId;

    @FindBy(xpath = "//div[contains(@id,'showInvoiceReceived')]//button[contains(@class,'btn-pink') and text()='Cancel']")
    private WebElement btnCancelCreatedInvoice;

    @FindBy(xpath = "//div[contains(@id,'pay_merchant')]//button[contains(@class,'btn-pink') and text()='Cancel']")
    private WebElement getBtnCancelCancelCreatedInvoice;

    @FindBy(xpath = "//div[contains(@id,'showInvoiceReceived')]//div[contains(@class,'donater-no')]")
    private WebElement lblInvoiceCardID;

    @FindBy(xpath = "//div[contains(@class,'contentdiv')]//button[contains(@class,'btn-green') and text()='Pay']")
    private WebElement btnPayInvoice;


    public MerchantInvoiceVerification clickOnInvoiceMenu() {

        testStepsLog(_logStep++, "Click on Invoice button.");
        clickOn(driver, btnMenuInvoice);

        return new MerchantInvoiceVerification(driver);
    }

    public MerchantInvoiceVerification clickOnCreateNewInvoice() {

        pause(3);

        testStepsLog(_logStep++, "Click on Create New Invoice button.");
        clickOn(driver, btnCreateNewInvoice);

        return new MerchantInvoiceVerification(driver);
    }

    public boolean isInvoiceTransactionDisplay() {

        try {
            updateShowList(driver, findElementByName(driver, "invoice-history-table_length"), "100");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        return !isElementPresent(lblNoInvoices);
    }

    public MerchantInvoiceVerification enterInvoiceSearchIDCriteria(boolean isInvalid) {

        if (isInvalid) {

            scrollToElement(driver, txtInputSearch);

            String searchCriteria = String.valueOf(getRandomNumber());
            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", searchCriteria);
            type(txtInputSearch, searchCriteria);

        } else {

            scrollToElement(driver, txtInputSearch);

            _searchCriteria = getInnerText(lstInvoiceID.get(getRandomNumberBetween(0, lastIndexOf(lstInvoiceID)))).split("-")[1];

            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", _searchCriteria);
            type(txtInputSearch, _searchCriteria);
        }

        return new MerchantInvoiceVerification(driver);
    }

    public MerchantInvoiceVerification clickOnFilterIcon() {

        scrollToElement(driver, btnFilter);

        testStepsLog(_logStep++, "Click on Filter Button.");
        clickOn(driver, btnFilter);

        pause(2);

        return new MerchantInvoiceVerification(driver);

    }

    public MerchantInvoiceVerification getConsumerDetailsForFilter() {

        pause(2);

        try {
            updateShowList(driver, findElementByName(driver, "invoice-history-table_length"), "100");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        for (int user = 0; user < sizeOf(lstUserName); user++) {
            if (!getInnerText(lstUserIdNumber.get(user)).contains("ID-")) {
                _filterFirstName = getInnerText(lstUserName.get(user)).split(" ")[0].trim();
                _filterLastName = getInnerText(lstUserName.get(user)).replace(_filterFirstName, "").trim();
                _filterPhone = getInnerText(lstUserIdNumber.get(user));
                _filterInvoiceId = String.valueOf(getIntegerFromString(getInnerText(lstInvoiceID.get(user))));
                System.out.println(_filterFirstName);
                System.out.println(_filterLastName);
                for (int num = 0; num < sizeOf(lstInvoiceAmount); num++) {
                    _transactionAmount.add(getDoubleFromString(getInnerText(lstInvoiceAmount.get(num))));
                }
                Collections.sort(_transactionAmount);
                break;
            }
        }
        return new MerchantInvoiceVerification(driver);
    }


    public MerchantInvoiceVerification changeFilterUserTo(String user) {

        scrollToElement(driver, btnUserDropdown);

        Select select = new Select(btnUserDropdown);

        switch (user.toLowerCase()) {
            case "consumer":
                select.selectByValue("user");
                break;
            case "merchant":
                select.selectByValue("merchant");
                break;
        }

        testStepsLog(_logStep++, "Select User : " + user);

        return new MerchantInvoiceVerification(driver);

    }

    public MerchantInvoiceVerification changeInvoiceTypeTo(String type) {

        scrollToElement(driver, btnTypeDropdown);

        Select select = new Select(btnTypeDropdown);

        switch (type.toLowerCase()) {
            case "sent":
                select.selectByValue("sent");
                break;
            case "received":
                select.selectByValue("received");
                break;
        }

        testStepsLog(_logStep++, "Select Type : " + type);

        return new MerchantInvoiceVerification(driver);

    }

    public MerchantInvoiceVerification filterFirstName(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomCharacters(7);
            testStepsLog(_logStep++, "Enter First Name : " + _filterRandomValue);
            type(txtFilterFirstName, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter First Name : " + _filterFirstName);
            type(txtFilterFirstName, _filterFirstName);
        }

        return new MerchantInvoiceVerification(driver);
    }

    public MerchantInvoiceVerification clickConsumerOnFilterSearchButton() {

        testStepsLog(_logStep++, "Click on Search button.");
        clickOn(driver, btnFilterSearch);

        pause(2);

        try {
            updateShowList(driver, findElementByName(driver, "invoice-history-table_length"), "100");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        pause(1);

        return new MerchantInvoiceVerification(driver);

    }

    public MerchantInvoiceVerification clearAllConsumerFilterDetails() {

        testStepsLog(_logStep++, "Click on Clear All button.");
        clickOn(driver, btnFilterClearAll);

        pause(2);

        try {
            updateShowList(driver, findElementByName(driver, "invoice-history-table_length"), "100");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        pause(1);

        return new MerchantInvoiceVerification(driver);

    }

    public MerchantInvoiceVerification filterLastName(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomCharacters(7);
            testStepsLog(_logStep++, "Enter Last Name : " + _filterRandomValue);
            type(txtFilterLastName, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter Last Name : " + _filterLastName);
            type(txtFilterLastName, _filterLastName);
        }

        return new MerchantInvoiceVerification(driver);
    }

    public MerchantInvoiceVerification filterConsumerNumber(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomNumber() + "" + getRandomNumber();
            testStepsLog(_logStep++, "Enter Mobile Number : " + _filterRandomValue);
            type(txtFilterConsumerMobile, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter Mobile Number : " + _filterPhone);
            type(txtFilterConsumerMobile, _filterPhone);
        }

        return new MerchantInvoiceVerification(driver);
    }

    public MerchantInvoiceVerification filterInvoiceId(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        scrollToElement(driver, txtFilterInvoiceID);

        if (isInvalid) {
            _filterRandomValue = "ID- " + getRandomNumber() + "" + getRandomNumberBetween(1, 9);
            testStepsLog(_logStep++, "Enter Invoice ID Number : " + _filterRandomValue);
            type(txtFilterInvoiceID, _filterRandomValue);
        } else {
            scrollToElement(driver, txtFilterInvoiceID);
            testStepsLog(_logStep++, "Enter Invoice ID Number : " + _filterInvoiceId);
            type(txtFilterInvoiceID, _filterInvoiceId);
        }

        return new MerchantInvoiceVerification(driver);
    }

    public MerchantInvoiceVerification filterInvoiceStatus(String status) {

        scrollToElement(driver, btnInvoiceStatusTypeDropDown);

        clickOn(driver, btnInvoiceStatusTypeDropDown);

        Select type = new Select(btnInvoiceStatusTypeDropDown);

        _filterInvoiceStatusType = status;

        switch (status.toLowerCase()) {
            case "pending":
                type.selectByIndex(1);
                break;
            case "cancelled":
                type.selectByIndex(2);
                break;
            case "paid":
                type.selectByIndex(3);
                break;
            default:
                type.selectByIndex(0);
                break;
        }

        testStepsLog(_logStep++, "Select Status : " + status);

        return new MerchantInvoiceVerification(driver);

    }

    public MerchantInvoiceVerification enterInvoiceAmountRange(boolean isInvalid) {

        _isRandomFilter = isInvalid;
        scrollToElement(driver, txtFilterInvoiceToAmount);

        if (isInvalid) {
            double randomNumber = getRandomNumber();
            _filterTransactionFromAmount = randomNumber + 10;
            _filterTransactionToAmount = randomNumber;

            testStepsLog(_logStep++, "Enter From Amount Range : " + _filterTransactionFromAmount);
            type(txtFilterInvoiceFromAmount, String.valueOf((int) _filterTransactionFromAmount));

            testStepsLog(_logStep++, "Enter To Amount Range : " + _filterTransactionToAmount);
            type(txtFilterInvoiceToAmount, String.valueOf((int) _filterTransactionToAmount));
        } else {
            if (!isListEmpty(_transactionAmount)) {
                if ((formatTwoDecimal(findMin(_transactionAmount))) == 0 &&
                        (int) (formatTwoDecimal(findMin(_transactionAmount))) ==
                                (int) (formatTwoDecimal(findMax(_transactionAmount)))) {
                    _filterTransactionFromAmount = (int) (formatTwoDecimal(findMin(_transactionAmount)));
                    _filterTransactionToAmount = (int) (formatTwoDecimal(findMax(_transactionAmount)));
                } else if (sizeOf(_transactionAmount) == 1) {
                    _filterTransactionFromAmount = (int) (formatTwoDecimal(findMin(_transactionAmount)));
                    _filterTransactionToAmount = (int) (formatTwoDecimal(findMax(_transactionAmount))) + 1;
                } else {
                    _filterTransactionFromAmount = (int) (formatTwoDecimal(findMin(_transactionAmount)) + 1);
                    _filterTransactionToAmount = (int) (formatTwoDecimal(findMax(_transactionAmount)) - 1);
                }
            } else {
                _filterTransactionFromAmount = 0;
                _filterTransactionToAmount = 0;
            }

            testStepsLog(_logStep++, "Enter From Amount Range : " + _filterTransactionFromAmount);
            type(txtFilterInvoiceFromAmount, String.valueOf((int) _filterTransactionFromAmount));

            testStepsLog(_logStep++, "Enter To Amount Range : " + _filterTransactionToAmount);
            type(txtFilterInvoiceToAmount, String.valueOf((int) _filterTransactionToAmount));
        }

        return new MerchantInvoiceVerification(driver);

    }

    public MerchantInvoiceVerification enterInvoiceAmount(String field) {

        scrollToElement(driver, txtFilterInvoiceToAmount);

        if (sizeOf(_transactionAmount) > 2) {
            _filterTransactionFromAmount = (int) (formatTwoDecimal(_transactionAmount.get(1) + 1));
            _filterTransactionToAmount = (int) (formatTwoDecimal(_transactionAmount.get(_transactionAmount.size() - 1) - 1));
        } else {
            if (!isListEmpty(_transactionAmount)) {
                _filterTransactionFromAmount = (int) (formatTwoDecimal(_transactionAmount.get(0)));
                _filterTransactionToAmount = (int) (formatTwoDecimal(_transactionAmount.get(_transactionAmount.size() - 1)));
            } else if (sizeOf(_transactionAmount) == 1) {
                _filterTransactionFromAmount = (int) (formatTwoDecimal(_transactionAmount.get(0)));
                _filterTransactionToAmount = (int) (formatTwoDecimal(_transactionAmount.get(_transactionAmount.size() - 1))) + 1;
            }
        }

        switch (field.toLowerCase()) {
            case "from":
                testStepsLog(_logStep++, "Enter From Amount Range : " + _filterTransactionFromAmount);
                type(txtFilterInvoiceFromAmount, String.valueOf((int) _filterTransactionFromAmount));
                break;
            case "to":
                testStepsLog(_logStep++, "Enter To Amount Range : " + _filterTransactionToAmount);
                type(txtFilterInvoiceToAmount, String.valueOf((int) _filterTransactionToAmount));
                break;
        }

        return new MerchantInvoiceVerification(driver);

    }

    public MerchantInvoiceVerification getMerchantDetailsForFilter() {

        pause(2);

        try {
            updateShowList(driver, findElementByName(driver, "invoice-history-table_length"), "100");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        _transactionAmount.clear();

        for (int user = 0; user < sizeOf(lstUserName); user++) {
            if (getText(lstUserIdNumber.get(user)).contains("ID-")) {
                _filterShopName = getInnerText(lstUserName.get(user));
                _filterMerchantId = getInnerText(lstUserIdNumber.get(user));
                _filterInvoiceId = String.valueOf(getIntegerFromString(getInnerText(lstInvoiceID.get(user))));

                for (int num = 0; num < sizeOf(lstInvoiceAmount); num++) {
                    _transactionAmount.add(getDoubleFromString(getInnerText(lstInvoiceAmount.get(num))));
                }
                Collections.sort(_transactionAmount);
                break;
            }
        }
        return new MerchantInvoiceVerification(driver);
    }

    public MerchantInvoiceVerification filterShopName(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomCharacters(7);
            testStepsLog(_logStep++, "Enter Shop Name : " + _filterRandomValue);
            type(txtFilterShopName, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter Shop Name : " + _filterShopName);
            type(txtFilterShopName, _filterShopName);
        }

        return new MerchantInvoiceVerification(driver);
    }


    public MerchantInvoiceVerification filterMerchantID(boolean isInvalid) {

        if (isInvalid) {
            _filterRandomValue = getRandomNumberBetween(10000, 99999) + "" + getRandomNumberBetween(10000, 99999);
            testStepsLog(_logStep++, "Enter Merchant ID");
            testInfoLog("Merchant ID", _filterRandomValue);
            type(txtFilterID, _filterRandomValue);

        } else {
            testStepsLog(_logStep++, "Enter Merchant ID");
            testInfoLog("Merchant ID", _filterMerchantId);
            type(txtFilterID, _filterMerchantId);
        }

        return new MerchantInvoiceVerification(driver);
    }

    public MerchantInvoiceVerification clearMerchantIDField() {

        testStepsLog(_logStep++, "Click on Clear button.");
        clickOn(driver, btnClearMerchantID);

        return new MerchantInvoiceVerification(driver);
    }

    public MerchantInvoiceVerification clearInvoiceID() {

        testStepsLog(_logStep++, "Click on Clear button.");
        clickOn(driver, btnClearInvoiceID);

        return new MerchantInvoiceVerification(driver);
    }

    public MerchantInvoiceVerification clearInvoiceStatus() {

        testStepsLog(_logStep++, "Click on Clear button.");
        clickOn(driver, btnClearInvoiceStaus);

        return new MerchantInvoiceVerification(driver);
    }

    public MerchantInvoiceVerification clearShopName() {

        testStepsLog(_logStep++, "Click on Clear button.");
        clickOn(driver, btnClearShopName);

        return new MerchantInvoiceVerification(driver);
    }

    public MerchantInvoiceVerification clearAmountRange() {

        testStepsLog(_logStep++, "Click on Clear Amount Range button.");
        clickOn(driver, btnClearAmountRange);

        return new MerchantInvoiceVerification(driver);
    }

    public MerchantInvoiceVerification clickOnReceivedTab() {

        pause(2);

        testStepsLog(_logStep++, "Click on Received button.");
        clickOn(driver, lstInvoiceTabButtons.get(0));

        return new MerchantInvoiceVerification(driver);

    }

    public MerchantInvoiceVerification clickOnSentTab() {

        pause(2);

        testStepsLog(_logStep++, "Click on Sent button.");
        clickOn(driver, lstInvoiceTabButtons.get(1));

        return new MerchantInvoiceVerification(driver);
    }

    public MerchantInvoiceVerification clickOnBackButton() {

        testStepsLog(_logStep++, "Click on Back button.");
        clickOn(driver, btnBack);

        return new MerchantInvoiceVerification(driver);

    }

    public MerchantInvoiceVerification enterAlphabeticalUserNumber() {

        _invoiceUserNumber = getRandomCharacters(10);
        testStepsLog(_logStep++, "Enter User Number : " + _invoiceUserNumber);
        type(txtInvoiceMobileNumber, _invoiceUserNumber);

        return new MerchantInvoiceVerification(driver);
    }

    public MerchantInvoiceVerification enterNumberMoreThan10Digits() {

        _invoiceUserNumber = getRandomNumberBetween(10000, 99999) + "" + getRandomNumberBetween(10000, 99999) + ""
                + getRandomNumberBetween(100, 999);
        testStepsLog(_logStep++, "Enter User Number : " + _invoiceUserNumber);
        type(txtInvoiceMobileNumber, _invoiceUserNumber);

        return new MerchantInvoiceVerification(driver);

    }

    public MerchantInvoiceVerification enterWrongMobileNumber() {

        _invoiceUserNumber = getRandomNumberBetween(10000, 99999) + "" + getRandomNumberBetween(10000, 99999);
        testStepsLog(_logStep++, "Enter User Number : " + _invoiceUserNumber);
        type(txtInvoiceMobileNumber, _invoiceUserNumber);

        return new MerchantInvoiceVerification(driver);

    }

    public MerchantInvoiceVerification enterMerchantMobileNumber() {

        boolean isMerchantAvailable = false;
        _invoiceUserNumber = externalNumber;
        testStepsLog(_logStep++, "Enter User Number : " + _invoiceUserNumber);
        type(txtInvoiceMobileNumber, _invoiceUserNumber);

        testStepsLog(_logStep++, "Select Merchant from list");

        for (int user = 0; user < sizeOf(lstUserList); user++) {
            if (!getText(lstUserList.get(user)).equalsIgnoreCase("consumer")) {
                _invoiceUserNumber = getInnerText(lstInvoiceUserMobileID.get(user));
                _invoiceUserName = getInnerText(lstInvoiceUserName.get(user));
                lstUserList.get(user).click();
                isMerchantAvailable = true;
                break;
            }
        }

        if (!isMerchantAvailable)
            testWarningLog("No Merchant found registered with this number, please enter valid number and re-run the scenario.");

        return new MerchantInvoiceVerification(driver);
    }

    public MerchantInvoiceVerification enterConsumerMobileNumber() {

        boolean isConsumerAvailable = false;
        _invoiceUserNumber = externalNumber;
        testStepsLog(_logStep++, "Enter User Number : " + _invoiceUserNumber);
        type(txtInvoiceMobileNumber, _invoiceUserNumber);

        testStepsLog(_logStep++, "Select Merchant from list");

        for (int user = 0; user < sizeOf(lstUserList); user++) {
            if (getText(lstUserList.get(user)).equalsIgnoreCase("consumer")) {
                _invoiceUserNumber = getInnerText(lstInvoiceUserMobileID.get(user));
                _invoiceUserName = getInnerText(lstInvoiceUserName.get(user));
                lstUserList.get(user).click();
                isConsumerAvailable = true;
                break;
            }
        }

        if (!isConsumerAvailable)
            testWarningLog("No Consumer found registered with this number, please enter valid number and re-run the scenario.");

        return new MerchantInvoiceVerification(driver);
    }

    public MerchantInvoiceVerification enterMinAmount() {
        _invoiceAmount = 0.01;
        testStepsLog(_logStep++, "Enter Amount : " + _invoiceAmount);
        type(txtInvoiceAmount, String.valueOf(_invoiceAmount));
        return new MerchantInvoiceVerification(driver);
    }

    public MerchantInvoiceVerification enterMaxAmount() {
        _invoiceAmount = 9999999;
        testStepsLog(_logStep++, "Enter Amount : " + _invoiceAmount);
        type(txtInvoiceAmount, String.valueOf(_invoiceAmount));
        return new MerchantInvoiceVerification(driver);
    }

    public MerchantInvoiceVerification enterValidAmount() {
        _invoiceAmount = formatTwoDecimal(getDoubleFromString(getRandomNumberBetween(1, 2) + "."
                + getRandomNumberBetween(10, 99)));
        testStepsLog(_logStep++, "Enter Amount : " + _invoiceAmount);
        type(txtInvoiceAmount, String.valueOf(_invoiceAmount));
        return new MerchantInvoiceVerification(driver);
    }

    public MerchantInvoiceVerification enterMessageForInvoice() {
        if (getRandomBoolean()) {
            _invoiceMessage = getRandomMessage().replaceAll("\\.", "");
            testStepsLog(_logStep++, "Enter Message : " + _invoiceMessage);
            type(txtInvoiceMessage, _invoiceMessage);
        } else {
            _invoiceMessage = "";
        }
        return new MerchantInvoiceVerification(driver);
    }

    public MerchantInvoiceVerification clickOnNextButton() {
        pause(2);
        testStepsLog(_logStep++, "Click on Next button.");
        clickOn(driver, btnNextInvoiceCreate);

        return new MerchantInvoiceVerification(driver);
    }

    public MerchantInvoiceVerification clickOnCancelButton() {
        pause(2);
        testStepsLog(_logStep++, "Click on Cancel button.");
        clickOn(driver, btnCancelInvoice);

        return new MerchantInvoiceVerification(driver);
    }

    public MerchantInvoiceVerification clickOnSendButton() {

        pause(2);
        scrollToElement(driver, btnSendInvoice);
        testStepsLog(_logStep++, "Click on Send button.");
        clickOn(driver, btnSendInvoice);
        if (isElementPresent(btnSendInvoice)) {
            try {
                if (isElementPresent(btnCancelInvoice)) {
                    System.out.println("Clicked on it");
                } else {
                    scrollToElement(driver, btnSendInvoice);
                    clickOn(driver, btnSendInvoice);
                }
            } catch (Exception e) {
                try {
                    scrollToElement(driver, btnSendInvoice);
                    clickOn(driver, btnSendInvoice);
                } catch (Exception e2) {
                    System.out.println("Already Send clicked");
                }
            }
        }

        return new MerchantInvoiceVerification(driver);
    }

    public MerchantInvoiceVerification clickOnBackToHome() {

        testStepsLog(_logStep++, "Click on Back To Home button.");
        clickOn(driver, btnBackToHomeCreated);

        return new MerchantInvoiceVerification(driver);

    }

    public MerchantInvoiceVerification clickOnBackToHomePaid() {

        testStepsLog(_logStep++, "Click on Back To Home button.");
        clickOn(driver, btnBackToHomePay);

        return new MerchantInvoiceVerification(driver);

    }

    public MerchantInvoiceVerification selectLatestCreatedInvoice() {

        testStepsLog(_logStep++, "Select Latest created Invoice from Sent section.");
        clickOn(driver, lstSentInvoicesId.get(0));

        _invoiceID = getText(lblInvoiceCardID);

        return new MerchantInvoiceVerification(driver);

    }

    public MerchantInvoiceVerification selectLatestReceivedInvoice() {

        testStepsLog(_logStep++, "Select Latest received Invoice from Received section.");
        clickOn(driver, lstReceivedInvoicesId.get(0));

        _invoiceID = getText(lblInvoiceCardID);

        return new MerchantInvoiceVerification(driver);

    }

    public MerchantInvoiceVerification cancelCreatedInvoice() {

        testStepsLog(_logStep++, "Click on Cancel button.");
        clickOn(driver, btnCancelCreatedInvoice);

        return new MerchantInvoiceVerification(driver);

    }

    public MerchantInvoiceVerification cancelReceivedInvoice() {

        testStepsLog(_logStep++, "Click on Cancel button.");
        clickOn(driver, btnCancelCreatedInvoice);

        return new MerchantInvoiceVerification(driver);

    }

    public MerchantInvoiceVerification clickOnCancelCancelCreatedInvoice() {

        testStepsLog(_logStep++, "Click on Cancel button.");
        clickOn(driver, getBtnCancelCancelCreatedInvoice);

        return new MerchantInvoiceVerification(driver);

    }

    public MerchantInvoiceVerification clickOnConfirmCancelCreatedInvoice() {

        testStepsLog(_logStep++, "Click on Confirm button.");
        clickOn(driver, btnConfirmDeleteInvoice);

        return new MerchantInvoiceVerification(driver);

    }

    public MerchantInvoiceVerification clickOnBackToHomeAfterDeleteInvoice() {

        testStepsLog(_logStep++, "Click on Back To Home button.");
        clickOn(driver, btnBackToHomeDeleted);

        return new MerchantInvoiceVerification(driver);

    }

    public MerchantInvoiceVerification searchDeletedInvoice() {

        pause(3);

        scrollToElement(driver, txtInputSearch);

        _searchCriteria = String.valueOf(getIntegerFromString(_invoiceID));

        testStepsLog(_logStep++, "Enter Search Criteria");
        testInfoLog("Search", _searchCriteria);
        type(txtInputSearch, _searchCriteria);

        return new MerchantInvoiceVerification(driver);
    }

    public MerchantInvoiceVerification searchPaidInvoice() {

        pause(3);

        scrollToElement(driver, txtInputSearch);

        _searchCriteria = String.valueOf(getIntegerFromString(_invoiceID));

        testStepsLog(_logStep++, "Enter Search Criteria");
        testInfoLog("Search", _searchCriteria);
        type(txtInputSearch, _searchCriteria);

        return new MerchantInvoiceVerification(driver);
    }

    public MerchantInvoiceVerification filterDeletedInvoiceId() {

        scrollToElement(driver, txtFilterInvoiceID);

        scrollToElement(driver, txtFilterInvoiceID);
        testStepsLog(_logStep++, "Enter Invoice ID Number : " + _invoiceID);
        type(txtFilterInvoiceID, _invoiceID);

        return new MerchantInvoiceVerification(driver);
    }

    public MerchantInvoiceVerification getCurrentSenderBalance() {
        MerchantDashboardIndexPage.getCurrentBalance();
        _invoiceSenderBalance = getDoubleFromString(MerchantDashboardIndexPage._currentBalance);
        return new MerchantInvoiceVerification(driver);
    }

    public MerchantInvoiceVerification getCurrentReceiverBalance() {
        MerchantDashboardIndexPage.getCurrentBalance();
        _invoiceReceiverBalance = getDoubleFromString(MerchantDashboardIndexPage._currentBalance);
        return new MerchantInvoiceVerification(driver);
    }

    public MerchantInvoiceVerification clickOnPayButton() {
        testStepsLog(_logStep++, "Click on Pay button.");
        clickOn(driver, btnPayInvoice);
        return new MerchantInvoiceVerification(driver);
    }

    public MerchantInvoiceVerification clickOnConfirmButton() {
        testStepsLog(_logStep++, "Click on Confirm button.");
        clickOn(driver, btnConfirmPayInvoice);
        return new MerchantInvoiceVerification(driver);
    }

}
