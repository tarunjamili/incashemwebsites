package com.incashme.merchant.indexpage;

import com.framework.init.AbstractPage;
import com.incashme.merchant.verification.MerchantWithdrawVerification;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class MerchantWithdrawIndexPage extends AbstractPage {

    public MerchantWithdrawIndexPage(WebDriver driver) {
        super(driver);
    }

    public static String _displayName = "";
    public static String _holderName = "";
    public static String _bankName = "";
    public static String _accountType = "";
    public static String _accountNumber = "";
    public static String _ifscCode = "";

    public static int _totalAddedBankAccounts = 0;

    public static String _encAccountNumber = "";

    public static double _withdrawAmount = 0.0d;
    public static String _withdrawBankName = "";
    public static String _withdrawBankAccountNumber = "";
    public static String _withdrawDisplayName = "";
    public static String _withdrawAccountHolder = "";
    public static double _withdrawImpsFee = 0.0d;
    public static double _withDrawTotalAmount = 0.0d;

    @FindBy(xpath = "//div//ul//li//a[contains(text(),'Withdraw')]")
    private WebElement btnWithdrawMoney;

    @FindBy(xpath = "//input[@formcontrolname='amount']")
    private WebElement txtWithdrawAmount;

    @FindBy(xpath = "//div[contains(@class,'blnc-lbl')]")
    private WebElement lblBalance;

    @FindBy(xpath = "//ul[contains(@class,'no_money_card_list')]")
    private WebElement sectionBankList;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@class,'no_mon_txt')]")})
    private List<WebElement> lstAddedBankAccounts;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@class,'no_close_icon')]//span")})
    private List<WebElement> lstBtnDeleteAccount;

    @FindAll(value = {@FindBy(xpath = "//ul[contains(@class,'no_money_card_list')]//li//span[contains(@class,'no_list')]")})
    private List<WebElement> lstAddNewBankAccount;

    @FindBy(xpath = "//input[@formcontrolname='display_name']")
    private WebElement txtDisplayName;

    @FindBy(xpath = "//input[@formcontrolname='holder_name']")
    private WebElement txtHolderName;

    @FindBy(xpath = "//select[@formcontrolname='bank_name']")
    private WebElement btnBankName;

    @FindBy(xpath = "//select[@formcontrolname='bank_account_type']")
    private WebElement btnBankAccountType;

    @FindBy(xpath = "//input[@formcontrolname='bank_account_no']")
    private WebElement txtBankAccountNumber;

    @FindBy(xpath = "//input[@formcontrolname='ifsc_no']")
    private WebElement txtBankIFSCNumber;

    @FindBy(xpath = "//div[contains(@class,'contentdiv')]//button[contains(@class,'btn-pink') and text()='Reset']")
    private WebElement btnResetForm;

    @FindBy(xpath = "//div[contains(@class,'contentdiv')]//button[contains(@class,'btn-green') and text()='Confirm']")
    private WebElement btnAddBankAccount;

    @FindBy(xpath = "//div//div[contains(@class,'add_bank_close')]")
    private WebElement btnCloseForm;

    @FindBy(xpath = ".//div[contains(@class,'swal2-actions')]//button[text()='Ok']")
    private WebElement btnOKAddBankAccount;

    @FindBy(xpath = ".//div[contains(@class,'swal2-actions')]//button[text()='Cancel']")
    private WebElement btnCancelAddBankAccount;

    @FindBy(xpath = "//div[contains(@class,'contentdiv')]//button[contains(@class,'btn-pink') and text()='Back']")
    private WebElement btnBack;

    @FindBy(xpath = "//div[contains(@class,'contentdiv')]//button[contains(@class,'btn-green') and text()='Confirm']")
    private WebElement btnConfirmWithdraw;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@class,'with_card_details')]//div[contains(@class,'with_card_d_title')]")})
    private List<WebElement> lstWithdrawDetails;

    @FindBy(xpath = "//div[contains(@class,'wt_imps_amt')]")
    private WebElement lblIMPSAmount;

    @FindBy(xpath = "//div[contains(@class,'wt_total_amt')]")
    private WebElement lblTotalWithdrawAmount;

    @FindBy(xpath = "//button[contains(@class,'btn-pink') and text()='Cancel']")
    private WebElement btnCancelWithdraw;

    @FindBy(xpath = "//button[contains(@class,'btn-green') and text()='Send']")
    private WebElement btnSendWithdraw;

    @FindBy(xpath = ".//div[contains(@class,'swal2-actions')]//button[text()='OK' or text() = 'Ok']")
    private WebElement btnConfirmOK;

    @FindBy(xpath = "//button[contains(@class,'btn-green') and contains(text(),'Home')]")
    private WebElement btnBackToHome;

    public MerchantWithdrawVerification clickOnWithdrawMenu() {

        testStepsLog(_logStep++, "Click on Withdraw Money button.");
        clickOn(driver, btnWithdrawMoney);

        return new MerchantWithdrawVerification(driver);

    }

    public boolean isAllBankAccountAdded() {
        testInfoLog("Total Added Bank Accounts", String.valueOf(sizeOf(lstAddedBankAccounts)));
        return sizeOf(lstAddedBankAccounts) == 4;
    }

    public boolean isBankAccountAvailable() {
        testInfoLog("Total Added Bank Accounts", String.valueOf(sizeOf(lstAddedBankAccounts)));
        _totalAddedBankAccounts = sizeOf(lstAddedBankAccounts);
        return sizeOf(lstAddedBankAccounts) != 0;
    }

    public MerchantWithdrawVerification clickOnAddBankAccount() {

        testStepsLog(_logStep++, "Click on Add (+) button.");
        clickOn(driver, lstAddNewBankAccount.get(getRandomNumberBetween(0, lastIndexOf(lstAddNewBankAccount))));

        return new MerchantWithdrawVerification(driver);
    }

    public MerchantWithdrawVerification clickOnCloseAddBankForm() {

        testStepsLog(_logStep++, "Click on Close (X) button.");
        clickOn(driver, btnCloseForm);

        return new MerchantWithdrawVerification(driver);
    }

    public MerchantWithdrawVerification enterDisplayName(boolean isInvalid) {

        if (isInvalid) {

            scrollToElement(driver, txtDisplayName);

            String searchCriteria = String.valueOf(getRandomNumber());
            testStepsLog(_logStep++, "Enter Display Name");
            testInfoLog("Display Name", searchCriteria);
            type(txtDisplayName, searchCriteria);

        } else {

            scrollToElement(driver, txtDisplayName);

            _displayName = getRandomFirstName();

            testStepsLog(_logStep++, "Enter Display Name");
            testInfoLog("Display Name", _displayName);
            type(txtDisplayName, _displayName);
        }

        return new MerchantWithdrawVerification(driver);
    }

    public MerchantWithdrawVerification enterAccountHolderName(boolean isInvalid) {

        if (isInvalid) {

            scrollToElement(driver, txtHolderName);

            String searchCriteria = String.valueOf(getRandomNumber());
            testStepsLog(_logStep++, "Enter Account Holder Name");
            testInfoLog("Account Holder Name", searchCriteria);
            type(txtHolderName, searchCriteria);

        } else {

            scrollToElement(driver, txtHolderName);

            _holderName = getRandomFirstName() + "" + getRandomLastName();

            testStepsLog(_logStep++, "Enter Account Holder Name");
            testInfoLog("Account Holder Name", _holderName);
            type(txtHolderName, _holderName);
        }

        return new MerchantWithdrawVerification(driver);
    }

    public MerchantWithdrawVerification selectBank() {

        Select bank = new Select(btnBankName);
        int index = getRandomNumberBetween(1, bank.getOptions().size() - 1);

        testStepsLog(_logStep++, "Select Bank");
        bank.selectByIndex(index);
        _bankName = getText(bank.getFirstSelectedOption());
        testInfoLog("Bank", _bankName);

        pause(3);

        return new MerchantWithdrawVerification(driver);
    }

    public MerchantWithdrawVerification selectAccountType() {

        Select accountType = new Select(btnBankAccountType);
        int index = getRandomNumberBetween(1, accountType.getOptions().size() - 1);

        testStepsLog(_logStep++, "Select Account Type");
        accountType.selectByIndex(index);
        _accountType = getText(accountType.getFirstSelectedOption());
        testInfoLog("Bank Account Type", _accountType);

        pause(3);

        return new MerchantWithdrawVerification(driver);
    }

    public MerchantWithdrawVerification enterAccountNumber(boolean isInvalid) {

        if (isInvalid) {

            scrollToElement(driver, txtBankAccountNumber);

            String searchCriteria = String.valueOf(getRandomNumber());
            testStepsLog(_logStep++, "Enter Account Number");
            testInfoLog("Account Number", searchCriteria);
            type(txtBankAccountNumber, searchCriteria);

        } else {

            scrollToElement(driver, txtBankAccountNumber);

            _accountNumber = getRandomNumberBetween(1000, 9999) + "" + getRandomNumberBetween(1000, 9999) + ""
                    + getRandomNumberBetween(1000, 9999) + "" + getRandomNumberBetween(1000, 9999);

            testStepsLog(_logStep++, "Enter Account Number");
            testInfoLog("Account Number", _accountNumber);
            type(txtBankAccountNumber, _accountNumber);
        }

        return new MerchantWithdrawVerification(driver);
    }

    public MerchantWithdrawVerification enterAccountNumberWithSpace() {


        scrollToElement(driver, txtBankAccountNumber);

        String searchCriteria = getRandomNumberBetween(1000, 9999) + " " + getRandomNumberBetween(1000, 9999) + " " +
                getRandomNumberBetween(1000, 9999) + " " + getRandomNumberBetween(1000, 9999);
        testStepsLog(_logStep++, "Enter Account Number");
        testInfoLog("Account Number", searchCriteria);
        type(txtBankAccountNumber, searchCriteria);


        return new MerchantWithdrawVerification(driver);
    }

    public MerchantWithdrawVerification enterIFSCCode(boolean isInvalid) {

        if (isInvalid) {

            scrollToElement(driver, txtBankIFSCNumber);

            String searchCriteria = String.valueOf(getRandomNumber());
            testStepsLog(_logStep++, "Enter IFSC Code");
            testInfoLog("IFSC Number", searchCriteria);
            type(txtBankIFSCNumber, searchCriteria);

        } else {

            scrollToElement(driver, txtBankIFSCNumber);

            _ifscCode = "SBIN0600300";

            testStepsLog(_logStep++, "Enter IFSC Code");
            testInfoLog("IFSC Number", _ifscCode);
            type(txtBankIFSCNumber, _ifscCode);
        }

        return new MerchantWithdrawVerification(driver);
    }

    public MerchantWithdrawVerification clickOKAddBankAccountButton() {

        testStepsLog(_logStep++, "Click on Ok button.");
        clickOn(driver, btnOKAddBankAccount);

        return new MerchantWithdrawVerification(driver);
    }

    public MerchantWithdrawVerification clickCancelAddBankAccountButton() {

        testStepsLog(_logStep++, "Click on Cancel button.");
        clickOn(driver, btnCancelAddBankAccount);

        return new MerchantWithdrawVerification(driver);
    }

    public MerchantWithdrawVerification clickOnAddNewBankAccount() {

        btnBankName.sendKeys(Keys.PAGE_DOWN);

        testStepsLog(_logStep++, "Click on Confirm button.");
        clickOn(driver, btnAddBankAccount);

        return new MerchantWithdrawVerification(driver);
    }

    public MerchantWithdrawVerification clickOnDeleteButton() {

        _encAccountNumber = getText(lstAddedBankAccounts.get(0)).split("\n")[1];

        testStepsLog(_logStep++, "Click on Delete (X) button.");
        clickOn(driver, lstBtnDeleteAccount.get(0));

        return new MerchantWithdrawVerification(driver);

    }

    public MerchantWithdrawVerification clickOnBankToWithdraw() {

        _withdrawBankName = getText(lstAddedBankAccounts.get(0)).split("\n")[0];
        _withdrawBankAccountNumber = getText(lstAddedBankAccounts.get(0)).split("\n")[1];

        testStepsLog(_logStep++, "Click on Bank Account.");
        clickOn(driver, lstAddedBankAccounts.get(0));

        return new MerchantWithdrawVerification(driver);

    }

    public MerchantWithdrawVerification enterWithdrawAmount(double... withdrawAmount) {

        if (withdrawAmount.length == 0) {
            _withdrawAmount = getDoubleFromString(getRandomNumberBetween(100, 200) + "." + getRandomNumberBetween(10, 99));

            testStepsLog(_logStep++, "Enter Withdraw Amount");
            testInfoLog("Withdraw Amount", String.valueOf(_withdrawAmount));
            type(txtWithdrawAmount, String.valueOf(_withdrawAmount));
        } else {
            _withdrawAmount = withdrawAmount[0];

            testStepsLog(_logStep++, "Enter Withdraw Amount");
            testInfoLog("Withdraw Amount", String.valueOf(_withdrawAmount));
            type(txtWithdrawAmount, String.valueOf(_withdrawAmount));
        }

        return new MerchantWithdrawVerification(driver);

    }

    public MerchantWithdrawVerification clickOnBackToWithdrawScreen() {

        scrollToElement(driver, btnBack);

        testStepsLog(_logStep++, "Click on Back button.");
        clickOn(driver, btnBack);

        return new MerchantWithdrawVerification(driver);

    }

    public MerchantWithdrawVerification clickOnConfirmToWithdraw() {

        scrollToElement(driver, btnBack);

        testStepsLog(_logStep++, "Click on Confirm button.");
        clickOn(driver, btnConfirmWithdraw);

        return new MerchantWithdrawVerification(driver);

    }

    public MerchantWithdrawVerification getWithdrawDetails() {

        _withdrawDisplayName = getText(lstWithdrawDetails.get(1)).split("\n")[0];
        _withdrawAccountHolder = getText(lstWithdrawDetails.get(2)).split("\n")[0];
        _withdrawBankAccountNumber = getText(lstWithdrawDetails.get(3)).split("\n")[0];
        _withdrawImpsFee = getDoubleFromString(getText(lblIMPSAmount));
        _withDrawTotalAmount = getDoubleFromString(getText(lblTotalWithdrawAmount));

        return new MerchantWithdrawVerification(driver);

    }

    public MerchantWithdrawVerification clickOnCancelWithdraw() {

        testStepsLog(_logStep++, "Click on Cancel button.");
        clickOn(driver, btnCancelWithdraw);

        return new MerchantWithdrawVerification(driver);

    }

    public MerchantWithdrawVerification clickOnSendWithdraw() {

        testStepsLog(_logStep++, "Click on Send button.");
        clickOn(driver, btnSendWithdraw);

        return new MerchantWithdrawVerification(driver);

    }

    public MerchantWithdrawVerification clickOnCancelWithdrawOK() {

        testStepsLog(_logStep++, "Click on Ok button.");
        clickOn(driver, btnConfirmOK);

        return new MerchantWithdrawVerification(driver);

    }

    public MerchantWithdrawVerification clickOnBackToHome() {

        testStepsLog(_logStep++, "Click on Back To Home button.");
        clickOn(driver, btnBackToHome);

        return new MerchantWithdrawVerification(driver);

    }
}
