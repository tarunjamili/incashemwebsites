package com.incashme.merchant.indexpage;

import com.framework.init.AbstractPage;
import com.incashme.merchant.verification.MerchantDonationVerification;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MerchantDonationIndexPage extends AbstractPage {

    public MerchantDonationIndexPage(WebDriver driver) {
        super(driver);
    }

    public static String _searchCriteria = "";
    public static String _filterRandomValue = "";

    public static boolean _isRandomFilter;

    public static String _filterFirstName = "";
    public static String _filterLastName = "";
    public static String _filterShopName = "";
    public static String _filterMerchantId = "";
    public static String _filterPhone = "";
    public static String _filterDonationId = "";
    public static String _filterDonationStatusType = "";
    public static double _filterDonationFromAmount = 0.0d;
    public static double _filterDonationToAmount = 0.0d;

    public static String _donationUserName = "";
    public static String _donationUserNumber = "";
    public static String _donationMessage = "";
    public static String _donationID = "";
    public static double _donationAmount = 0.0d;

    public static double _donationRequesterBalance = 0.0d;
    public static double _donationSenderBalance = 0.0d;

    public static double _receiverDonationReceivedChart = 0.0d;
    public static double _receiverDonationPaidChart = 0.0d;
    public static double _payDonationPaidChart = 0.0d;

    public static List<Double> _transactionAmount = new ArrayList<>();

    public static String userType = "";

    @FindBy(xpath = "//ul/li//a[@title='Donation']")
    private WebElement btnMenuDonation;

    @FindBy(xpath = "//div//p[contains(text(),'no data alive')]")
    private WebElement lblNoDonations;

    @FindBy(xpath = "//input[@type='search']")
    private WebElement txtInputSearch;

    @FindAll(value = {@FindBy(xpath = "//table[@id='donation-transaction-table']//tr//td[2]")})
    private List<WebElement> lstDonationID;

    @FindAll(value = {@FindBy(xpath = "//table[@id='donation-transaction-table']//tr//td[1]//h5")})
    private List<WebElement> lstUserName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='donation-transaction-table']//tr//h5//following-sibling::div")})
    private List<WebElement> lstUserIdNumber;

    @FindAll(value = {@FindBy(xpath = "//table[@id='donation-transaction-table']//tr//td[3]")})
    private List<WebElement> lstDonationAmount;

    @FindAll(value = {@FindBy(xpath = "//table[@id='donation-transaction-table']//tr//td[4]")})
    private List<WebElement> lstDonationType;

    @FindAll(value = {@FindBy(xpath = "//table[@id='donation-transaction-table']//tr//td[5]")})
    private List<WebElement> lstDonationDateTime;

    @FindBy(xpath = "//div[contains(@id,'Requested')]//a[contains(text(),'Donation')]")
    private WebElement btnNewDonation;

    @FindBy(xpath = "//li/button[contains(@class,'filterBtn')]")
    private WebElement btnFilter;

    @FindBy(xpath = "//select[@formcontrolname='otype']")
    private WebElement btnUserDropdown;

    @FindBy(xpath = "//select[@formcontrolname='type']")
    private WebElement btnTypeDropdown;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//button[contains(@class,'searchbtn')]")
    private WebElement btnFilterSearch;

    @FindBy(xpath = "//input[@formcontrolname='mia_companyname']")
    private WebElement txtFilterShopName;

    @FindBy(xpath = "//input[@formcontrolname='other_user_id']")
    private WebElement txtFilterID;

    @FindBy(xpath = "//input[@formcontrolname='other_user_id']//following-sibling::div//a")
    private WebElement btnClearMerchantID;

    @FindBy(xpath = "(//input[@formcontrolname='mia_companyname']//following-sibling::div//a)[1]")
    private WebElement btnClearShopName;

    @FindBy(xpath = "//select[@formcontrolname='status']/../..//following-sibling::div//a")
    private WebElement btnClearDonationStaus;

    @FindBy(xpath = "//input[@formcontrolname='to_amount']//following-sibling::div//a")
    private WebElement btnClearAmountRange;

    @FindBy(xpath = "//input[@formcontrolname='id']")
    private WebElement txtFilterDonationID;

    @FindBy(xpath = "//input[@formcontrolname='id']//following-sibling::div//a")
    private WebElement btnClearDonationID;

    @FindBy(xpath = "//select[@formcontrolname='status']")
    private WebElement btnDonationStatusTypeDropDown;

    @FindBy(xpath = "//input[@formcontrolname='from_amount']")
    private WebElement txtFilterDonationFromAmount;

    @FindBy(xpath = "//input[@formcontrolname='to_amount']")
    private WebElement txtFilterDonationToAmount;

    @FindBy(xpath = "//input[@formcontrolname='fname']")
    private WebElement txtFilterFirstName;

    @FindBy(xpath = "//input[@formcontrolname='lname']")
    private WebElement txtFilterLastName;

    @FindBy(xpath = "//input[@formcontrolname='mib_phone']")
    private WebElement txtFilterConsumerMobile;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//a[contains(text(),'Clear All')]")
    private WebElement btnFilterClearAll;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@class,'donation-sldr')]/div/ul/li")})
    private List<WebElement> lstDonationTabButtons;

    @FindBy(xpath = "//div[contains(@id,'Requested')]//a[contains(text(),'New Donation')]")
    private WebElement btnCreateNewDonation;

    @FindBy(xpath = "//a[text()='Back']")
    private WebElement btnBack;

    @FindBy(xpath = "//input[@formcontrolname='donationUser']")
    private WebElement txDonationMobileNumber;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@class,'filter-suggetion')]//div[contains(@class,'badge')]")})
    private List<WebElement> lstUserList;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@class,'filter-suggetion')]//h5[contains(@class,'user-name')]")})
    private List<WebElement> lstDonationUserName;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@class,'filter-suggetion')]//div[contains(@class,'trans-sub-title')]")})
    private List<WebElement> lstDonationUserMobileID;

    @FindBy(xpath = "//input[@formcontrolname='donationMessage']")
    private WebElement txtDonationMessage;

    @FindBy(xpath = "//div[contains(@class,'agnt-cnf-ftr')]//button[contains(@class,'btn-pink') and text()='Cancel']")
    private WebElement btnCancelSendDonationRequest;

    @FindBy(xpath = "//form//button[contains(@class,'btn-pink') and text()='Cancel']")
    private WebElement btnCancelDonationRequest;

    @FindBy(xpath = "//div[contains(@class,'contentdiv')]//button[contains(@class,'btn-green') and text()='Send']")
    private WebElement btnSendDonationRequest;

    @FindBy(xpath = "//button[contains(@class,'btn-green') and text()='Confirm']")
    private WebElement btnConfirmDonation;

    @FindBy(xpath = "//div[contains(@id,'mrcnt-make-donation')]//button[contains(@class,'btn-green') and contains(text(),'Home')]")
    private WebElement btnBackToHomeCreated;

    @FindBy(xpath = "//div//div[contains(@class,'dntn-id')]")
    private WebElement lblDonationCardID;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@id,'Sent')]//ul/li//div[contains(@class,'dntn-id')]")})
    private List<WebElement> lstSentDonationId;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@id,'Requested')]//ul/li//div[contains(@class,'dntn-id')]")})
    private List<WebElement> lstRequestedDonationId;

    @FindBy(xpath = "//input[@formcontrolname='donationAmount']")
    private WebElement txtDonationAmount;

    @FindBy(xpath = "//div[contains(@class,'contentdiv')]//button[contains(@class,'btn-green') and text()='Pay']")
    private WebElement btnPayDonation;

    @FindBy(xpath = "//div[contains(@id,'mrcnt-conf')]//button[contains(@class,'btn-green') and contains(text(),'Home')]")
    private WebElement btnBackToHomePay;

    @FindBy(xpath = "//div[contains(@id,'mrcnt-conf')]//button[contains(@class,'btn-green') and contains(text(),'Home')]")
    private WebElement btnBackToHomeDeleted;

    @FindBy(xpath = "//div[contains(@class,'merchant-count')]//span[contains(@class,'ml-auto')]")
    private WebElement lblDonationAmountChart;

    @FindBy(xpath = "//div[contains(@id,'mrcnt-make-donation')]//div[contains(@class,'tppup-usr-nm')]")
    private WebElement lblDonationUsername;

    public MerchantDonationVerification clickOnDonationMenu() {

        testStepsLog(_logStep++, "Click on Donation button.");
        clickOn(driver, btnMenuDonation);

        return new MerchantDonationVerification(driver);
    }

    public boolean isDonationTransactionDisplay() {
        try {
            updateShowList(driver, findElementByName(driver, "donation-transaction-table_length"), "100");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }
        return !isElementPresent(lblNoDonations);
    }

    public MerchantDonationVerification enterDonationSearchIDCriteria(boolean isInvalid) {

        scrollElement(txtInputSearch);

        if (isInvalid) {

            String searchCriteria = String.valueOf(getRandomNumber());
            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", searchCriteria);
            type(txtInputSearch, searchCriteria);

        } else {

            _searchCriteria = getInnerText(lstDonationID.get(getRandomNumberBetween(0, lastIndexOf(lstDonationID)))).split("-")[1];

            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", _searchCriteria);
            type(txtInputSearch, _searchCriteria);
        }

        return new MerchantDonationVerification(driver);
    }

    public boolean isNonProfitMerchant() {
        boolean isNonProfit = isElementPresent(btnNewDonation);
        if (isNonProfit) {
            userType = "Non Profit";
        } else {
            userType = "Profit";
        }
        return isNonProfit;
    }

    public MerchantDonationVerification clickOnFilterIcon() {
        scrollToElement(driver, btnFilter);
        testStepsLog(_logStep++, "Click on Filter Button.");
        clickOn(driver, btnFilter);
        pause(2);
        return new MerchantDonationVerification(driver);
    }

    public MerchantDonationVerification changeFilterUserTo(String user) {

        scrollToElement(driver, btnUserDropdown);

        Select select = new Select(btnUserDropdown);

        switch (user.toLowerCase()) {
            case "consumer":
                select.selectByValue("user");
                break;
            case "merchant":
                select.selectByValue("merchant");
                break;
        }

        testStepsLog(_logStep++, "Select User : " + user);

        return new MerchantDonationVerification(driver);

    }

    public MerchantDonationVerification changeDonationTypeTo(String type) {

        scrollToElement(driver, btnTypeDropdown);

        Select select = new Select(btnTypeDropdown);

        switch (type.toLowerCase()) {
            case "sent":
                select.selectByValue("sent");
                break;
            case "requested":
                select.selectByValue("received");
                break;
        }

        testStepsLog(_logStep++, "Select Type : " + type);

        return new MerchantDonationVerification(driver);

    }

    public MerchantDonationVerification clickConsumerOnFilterSearchButton() {

        testStepsLog(_logStep++, "Click on Search button.");
        clickOn(driver, btnFilterSearch);

        pause(2);

        try {
            updateShowList(driver, findElementByName(driver, "donation-transaction-table_length"), "100");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        pause(1);

        return new MerchantDonationVerification(driver);

    }

    public MerchantDonationVerification getMerchantDetailsForFilter() {

        pause(2);

        try {
            updateShowList(driver, findElementByName(driver, "donation-transaction-table_length"), "100");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        _transactionAmount.clear();

        for (int user = 0; user < sizeOf(lstUserName); user++) {
            if (getText(lstUserIdNumber.get(user)).contains("ID-")) {
                _filterShopName = getInnerText(lstUserName.get(user));
                _filterMerchantId = getInnerText(lstUserIdNumber.get(user));
                _filterDonationId = String.valueOf(getIntegerFromString(getInnerText(lstDonationID.get(user))));

                for (int num = 0; num < sizeOf(lstDonationAmount); num++) {
                    _transactionAmount.add(getDoubleFromString(getInnerText(lstDonationAmount.get(num))));
                }
                Collections.sort(_transactionAmount);
                break;
            }
        }
        return new MerchantDonationVerification(driver);
    }

    public MerchantDonationVerification filterShopName(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomCharacters(7);
            testStepsLog(_logStep++, "Enter Shop Name : " + _filterRandomValue);
            type(txtFilterShopName, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter Shop Name : " + _filterShopName);
            type(txtFilterShopName, _filterShopName);
        }

        return new MerchantDonationVerification(driver);
    }


    public MerchantDonationVerification filterMerchantID(boolean isInvalid) {

        if (isInvalid) {
            _filterRandomValue = getRandomNumberBetween(10000, 99999) + "" + getRandomNumberBetween(10000, 99999);
            testStepsLog(_logStep++, "Enter Merchant ID");
            testInfoLog("Merchant ID", _filterRandomValue);
            type(txtFilterID, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter Merchant ID");
            testInfoLog("Merchant ID", _filterMerchantId);
            type(txtFilterID, _filterMerchantId);
        }

        return new MerchantDonationVerification(driver);
    }

    public MerchantDonationVerification clearShopName() {

        testStepsLog(_logStep++, "Click on Clear button.");
        clickOn(driver, btnClearShopName);

        pause(2);

        return new MerchantDonationVerification(driver);
    }

    public MerchantDonationVerification clearMerchantIDField() {

        testStepsLog(_logStep++, "Click on Clear button.");
        clickOn(driver, btnClearMerchantID);

        pause(2);

        return new MerchantDonationVerification(driver);
    }

    public MerchantDonationVerification filterDonationId(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        scrollToElement(driver, txtFilterDonationID);

        if (isInvalid) {
            _filterRandomValue = "ID- " + getRandomNumber() + "" + getRandomNumberBetween(1, 9);
            testStepsLog(_logStep++, "Enter Donation ID Number : " + _filterRandomValue);
            type(txtFilterDonationID, _filterRandomValue);
        } else {
            scrollToElement(driver, txtFilterDonationID);
            testStepsLog(_logStep++, "Enter Donation ID Number : " + _filterDonationId);
            type(txtFilterDonationID, _filterDonationId);
        }

        return new MerchantDonationVerification(driver);
    }

    public MerchantDonationVerification clearDonationID() {

        testStepsLog(_logStep++, "Click on Clear button.");
        clickOn(driver, btnClearDonationID);

        pause(2);

        return new MerchantDonationVerification(driver);
    }

    public MerchantDonationVerification filterDonationStatus(String status) {

        scrollToElement(driver, btnDonationStatusTypeDropDown);

        clickOn(driver, btnDonationStatusTypeDropDown);

        Select type = new Select(btnDonationStatusTypeDropDown);

        if (status.equalsIgnoreCase("donated") &&
                userType.equalsIgnoreCase("Non Profit")) {
            _filterDonationStatusType = "Donation Received";
        } else {
            _filterDonationStatusType = status;
        }

        switch (status.toLowerCase()) {
            case "pending":
                type.selectByIndex(1);
                break;
            case "cancelled":
                type.selectByIndex(2);
                break;
            case "donated":
                type.selectByIndex(3);
                break;
            default:
                type.selectByIndex(0);
                break;
        }

        testStepsLog(_logStep++, "Select Status : " + status);

        return new MerchantDonationVerification(driver);

    }

    public MerchantDonationVerification clearDonationStatus() {

        testStepsLog(_logStep++, "Click on Clear button.");
        clickOn(driver, btnClearDonationStaus);

        pause(2);

        return new MerchantDonationVerification(driver);
    }

    public MerchantDonationVerification enterDonationAmountRange(boolean isInvalid) {

        _isRandomFilter = isInvalid;
        scrollToElement(driver, txtFilterDonationToAmount);

        if (isInvalid) {
            double randomNumber = getRandomNumber();
            _filterDonationFromAmount = randomNumber + 10;
            _filterDonationToAmount = randomNumber;

            testStepsLog(_logStep++, "Enter From Amount Range : " + _filterDonationFromAmount);
            type(txtFilterDonationFromAmount, String.valueOf((int) _filterDonationFromAmount));

            testStepsLog(_logStep++, "Enter To Amount Range : " + _filterDonationToAmount);
            type(txtFilterDonationToAmount, String.valueOf((int) _filterDonationToAmount));
        } else {
            if (!isListEmpty(_transactionAmount)) {
                if ((formatTwoDecimal(findMin(_transactionAmount))) == 0 &&
                        (int) (formatTwoDecimal(findMin(_transactionAmount))) ==
                                (int) (formatTwoDecimal(findMax(_transactionAmount)))) {
                    _filterDonationFromAmount = (int) (formatTwoDecimal(findMin(_transactionAmount)));
                    _filterDonationToAmount = (int) (formatTwoDecimal(findMax(_transactionAmount)));
                } else if (sizeOf(_transactionAmount) == 1) {
                    _filterDonationFromAmount = (int) (formatTwoDecimal(findMin(_transactionAmount)));
                    _filterDonationToAmount = (int) (formatTwoDecimal(findMax(_transactionAmount))) + 1;
                } else {
                    _filterDonationFromAmount = (int) (formatTwoDecimal(findMin(_transactionAmount)) + 1);
                    _filterDonationToAmount = (int) (formatTwoDecimal(findMax(_transactionAmount)) - 1);
                }
            } else {
                _filterDonationFromAmount = 0;
                _filterDonationToAmount = 0;
            }

            testStepsLog(_logStep++, "Enter From Amount Range : " + _filterDonationFromAmount);
            type(txtFilterDonationFromAmount, String.valueOf((int) _filterDonationFromAmount));

            testStepsLog(_logStep++, "Enter To Amount Range : " + _filterDonationToAmount);
            type(txtFilterDonationToAmount, String.valueOf((int) _filterDonationToAmount));
        }

        return new MerchantDonationVerification(driver);

    }

    public MerchantDonationVerification enterDonationAmount(String field) {

        scrollToElement(driver, txtFilterDonationToAmount);

        if (sizeOf(_transactionAmount) > 2) {
            _filterDonationFromAmount = (int) (formatTwoDecimal(_transactionAmount.get(1) + 1));
            _filterDonationToAmount = (int) (formatTwoDecimal(_transactionAmount.get(_transactionAmount.size() - 1) - 1));
        } else {
            if (!isListEmpty(_transactionAmount)) {
                _filterDonationFromAmount = (int) (formatTwoDecimal(_transactionAmount.get(0)));
                _filterDonationToAmount = (int) (formatTwoDecimal(_transactionAmount.get(_transactionAmount.size() - 1)));
            } else if (sizeOf(_transactionAmount) == 1) {
                _filterDonationFromAmount = (int) (formatTwoDecimal(_transactionAmount.get(0)));
                _filterDonationToAmount = (int) (formatTwoDecimal(_transactionAmount.get(_transactionAmount.size() - 1))) + 1;
            }
        }

        switch (field.toLowerCase()) {
            case "from":
                testStepsLog(_logStep++, "Enter From Amount Range : " + _filterDonationFromAmount);
                type(txtFilterDonationFromAmount, String.valueOf((int) _filterDonationFromAmount));
                break;
            case "to":
                testStepsLog(_logStep++, "Enter To Amount Range : " + _filterDonationToAmount);
                type(txtFilterDonationToAmount, String.valueOf((int) _filterDonationToAmount));
                break;
        }

        return new MerchantDonationVerification(driver);

    }

    public MerchantDonationVerification clearAmountRange() {

        testStepsLog(_logStep++, "Click on Clear Amount Range button.");
        clickOn(driver, btnClearAmountRange);

        pause(2);

        return new MerchantDonationVerification(driver);
    }

    public MerchantDonationVerification getConsumerDetailsForFilter() {

        pause(2);

        try {
            updateShowList(driver, findElementByName(driver, "donation-transaction-table_length"), "100");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        for (int user = 0; user < sizeOf(lstUserName); user++) {
            if (!getInnerText(lstUserIdNumber.get(user)).contains("ID- ")) {
                _filterFirstName = getInnerText(lstUserName.get(user)).split(" ")[0].trim();
                _filterLastName = getInnerText(lstUserName.get(user)).replace(_filterFirstName, "").trim();
                _filterPhone = getInnerText(lstUserIdNumber.get(user));
                _filterDonationId = String.valueOf(getIntegerFromString(getInnerText(lstDonationID.get(user))));
                System.out.println(_filterFirstName);
                System.out.println(_filterLastName);
                for (int num = 0; num < sizeOf(lstDonationAmount); num++) {
                    _transactionAmount.add(getDoubleFromString(getInnerText(lstDonationAmount.get(num))));
                }
                Collections.sort(_transactionAmount);
                break;
            }
        }
        return new MerchantDonationVerification(driver);
    }

    public MerchantDonationVerification filterFirstName(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomCharacters(7);
            testStepsLog(_logStep++, "Enter First Name : " + _filterRandomValue);
            type(txtFilterFirstName, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter First Name : " + _filterFirstName);
            type(txtFilterFirstName, _filterFirstName);
        }

        return new MerchantDonationVerification(driver);
    }

    public MerchantDonationVerification filterLastName(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomCharacters(7);
            testStepsLog(_logStep++, "Enter Last Name : " + _filterRandomValue);
            type(txtFilterLastName, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter Last Name : " + _filterLastName);
            type(txtFilterLastName, _filterLastName);
        }

        return new MerchantDonationVerification(driver);
    }

    public MerchantDonationVerification clearAllConsumerFilterDetails() {

        testStepsLog(_logStep++, "Click on Clear All button.");
        clickOn(driver, btnFilterClearAll);

        pause(2);

        try {
            updateShowList(driver, findElementByName(driver, "donation-transaction-table_length"), "100");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        pause(1);

        return new MerchantDonationVerification(driver);

    }

    public MerchantDonationVerification filterConsumerNumber(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomNumber() + "" + getRandomNumber();
            testStepsLog(_logStep++, "Enter Mobile Number : " + _filterRandomValue);
            type(txtFilterConsumerMobile, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter Mobile Number : " + _filterPhone);
            type(txtFilterConsumerMobile, _filterPhone);
        }

        return new MerchantDonationVerification(driver);
    }

    public MerchantDonationVerification clickOnRequestedTab() {

        wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfAllElements(lstDonationTabButtons.get(0)));

        pause(5);

        testStepsLog(_logStep++, "Click on Requested button.");
        clickOn(driver, lstDonationTabButtons.get(0));

        return new MerchantDonationVerification(driver);

    }

    public MerchantDonationVerification clickOnSentTab() {

        wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfAllElements(lstDonationTabButtons.get(1)));

        pause(5);

        testStepsLog(_logStep++, "Click on Sent button.");
        clickOn(driver, lstDonationTabButtons.get(1));

        return new MerchantDonationVerification(driver);
    }

    public MerchantDonationVerification clickOnCreateNewDonation() {

        pause(3);

        testStepsLog(_logStep++, "Click on Create New Donation button.");
        clickOn(driver, btnCreateNewDonation);

        return new MerchantDonationVerification(driver);
    }

    public MerchantDonationVerification clickOnBackButton() {

        testStepsLog(_logStep++, "Click on Back button.");
        clickOn(driver, btnBack);

        return new MerchantDonationVerification(driver);

    }

    public MerchantDonationVerification enterAlphabeticalUserNumber() {

        _donationUserNumber = getRandomCharacters(10);
        testStepsLog(_logStep++, "Enter User Number : " + _donationUserNumber);
        type(txDonationMobileNumber, _donationUserNumber);

        return new MerchantDonationVerification(driver);
    }

    public MerchantDonationVerification enterNumberMoreThan10Digits() {

        _donationUserNumber = getRandomNumberBetween(10000, 99999) + "" + getRandomNumberBetween(10000, 99999) + ""
                + getRandomNumberBetween(100, 999);
        testStepsLog(_logStep++, "Enter User Number : " + _donationUserNumber);
        type(txDonationMobileNumber, _donationUserNumber);

        return new MerchantDonationVerification(driver);

    }

    public MerchantDonationVerification enterWrongMobileNumber() {

        _donationUserNumber = getRandomNumberBetween(10000, 99999) + "" + getRandomNumberBetween(10000, 99999);
        testStepsLog(_logStep++, "Enter User Number : " + _donationUserNumber);
        type(txDonationMobileNumber, _donationUserNumber);

        return new MerchantDonationVerification(driver);

    }

    public MerchantDonationVerification enterMerchantMobileNumber() {

        boolean isMerchantAvailable = false;
        _donationUserNumber = externalNumber;
        testStepsLog(_logStep++, "Enter User Number : " + _donationUserNumber);
        type(txDonationMobileNumber, _donationUserNumber);

        testStepsLog(_logStep++, "Select Merchant from list");

        for (int user = 0; user < sizeOf(lstUserList); user++) {
            if (!getText(lstUserList.get(user)).equalsIgnoreCase("consumer")) {
                _donationUserNumber = getInnerText(lstDonationUserMobileID.get(user));
                _donationUserName = getInnerText(lstDonationUserName.get(user));
                clickOn(driver, lstDonationUserName.get(user));
                isMerchantAvailable = true;
                break;
            }
        }

        if (!isMerchantAvailable)
            testWarningLog("No Merchant found registered with this number, please enter valid number and re-run the scenario.");

        return new MerchantDonationVerification(driver);
    }

    public MerchantDonationVerification enterConsumerMobileNumber() {

        boolean isConsumerAvailable = false;
        _donationUserNumber = externalNumber;
        testStepsLog(_logStep++, "Enter User Number : " + _donationUserNumber);
        type(txDonationMobileNumber, _donationUserNumber);

        testStepsLog(_logStep++, "Select Consumer from list");

        for (int user = 0; user < sizeOf(lstUserList); user++) {
            if (getText(lstUserList.get(user)).equalsIgnoreCase("consumer")) {
                _donationUserNumber = getInnerText(lstDonationUserMobileID.get(user));
                _donationUserName = getInnerText(lstDonationUserName.get(user));
                clickOn(driver, lstDonationUserName.get(user));
                isConsumerAvailable = true;
                break;
            }
        }

        if (!isConsumerAvailable)
            testWarningLog("No Consumer found registered with this number, please enter valid number and re-run the scenario.");

        return new MerchantDonationVerification(driver);
    }

    public MerchantDonationVerification enterMessageForDonation() {
        if (getRandomBoolean()) {
            scrollToElement(driver, txtDonationMessage);
            _donationMessage = getRandomMessage().replaceAll("\\.", "");
            testStepsLog(_logStep++, "Enter Message : " + _donationMessage);
            type(txtDonationMessage, _donationMessage);
        } else {
            _donationMessage = "";
        }
        return new MerchantDonationVerification(driver);
    }

    public MerchantDonationVerification clickOnSendButton() {

        pause(2);
        scrollToElement(driver, btnSendDonationRequest);
        pause(2);
        testStepsLog(_logStep++, "Click on Send button.");
        clickOn(driver, btnSendDonationRequest);
        pause(2);
        System.out.println("button send : " + isElementDisplay(btnSendDonationRequest));
        try {
            if (isElementPresent(btnCancelSendDonationRequest)) {
                System.out.println("Clicked on it");
            } else {
                scrollToElement(driver, btnSendDonationRequest);
                clickOn(driver, btnSendDonationRequest);
            }
        } catch (Exception e) {
            try {
                scrollToElement(driver, btnSendDonationRequest);
                clickOn(driver, btnSendDonationRequest);
            } catch (Exception e2) {
                System.out.println("Alread Send clicked");
            }
        }

        return new MerchantDonationVerification(driver);
    }

    public MerchantDonationVerification clickOnCancelButton() {
        pause(2);
        testStepsLog(_logStep++, "Click on Cancel button.");
        clickOn(driver, btnCancelSendDonationRequest);

        return new MerchantDonationVerification(driver);
    }

    public MerchantDonationVerification clickOnCancelDonationButton() {
        pause(2);
        testStepsLog(_logStep++, "Click on Cancel button.");
        clickOn(driver, btnCancelDonationRequest);

        return new MerchantDonationVerification(driver);
    }

    public MerchantDonationVerification clickOnConfirmButton() {
        pause(2);
        testStepsLog(_logStep++, "Click on Confirm button.");
        clickOn(driver, btnConfirmDonation);

        return new MerchantDonationVerification(driver);
    }

    public MerchantDonationVerification clickOnBackToHome() {

        testStepsLog(_logStep++, "Click on Back To Home button.");
        clickOn(driver, btnBackToHomeCreated);

        return new MerchantDonationVerification(driver);

    }

    public MerchantDonationVerification selectLatestSentDonation() {

        testStepsLog(_logStep++, "Select Latest sent Donation from Sent section.");
        clickOn(driver, lstSentDonationId.get(0));

        _donationID = getText(lblDonationCardID);

        return new MerchantDonationVerification(driver);

    }

    public MerchantDonationVerification getCurrentRequesterBalance() {
        pause(5);
        MerchantDashboardIndexPage.getCurrentBalance();
        _donationRequesterBalance = getDoubleFromString(MerchantDashboardIndexPage._currentBalance);
        return new MerchantDonationVerification(driver);
    }

    public MerchantDonationVerification getCurrentSenderBalance() {
        MerchantDashboardIndexPage.getCurrentBalance();
        _donationSenderBalance = getDoubleFromString(MerchantDashboardIndexPage._currentBalance);

        return new MerchantDonationVerification(driver);
    }

    public MerchantDonationVerification selectLatestRequestedDonation() {

        testStepsLog(_logStep++, "Select Latest requested Donation from Requested section.");
        clickOn(driver, lstRequestedDonationId.get(0));

        _donationID = getText(lblDonationCardID);

        return new MerchantDonationVerification(driver);
    }

    public MerchantDonationVerification enterMinAmount() {
        _donationAmount = 0.01;
        testStepsLog(_logStep++, "Enter Amount : " + _donationAmount);
        type(txtDonationAmount, String.valueOf(_donationAmount));
        return new MerchantDonationVerification(driver);
    }

    public MerchantDonationVerification enterMaxAmount() {
        _donationAmount = 9999999;
        testStepsLog(_logStep++, "Enter Amount : " + _donationAmount);
        type(txtDonationAmount, String.valueOf(_donationAmount));
        return new MerchantDonationVerification(driver);
    }

    public MerchantDonationVerification enterValidAmount() {
        _donationAmount = formatTwoDecimal(getDoubleFromString(getRandomNumberBetween(1, 2) + "."
                + getRandomNumberBetween(10, 99)));
        testStepsLog(_logStep++, "Enter Amount : " + _donationAmount);
        type(txtDonationAmount, String.valueOf(_donationAmount));
        return new MerchantDonationVerification(driver);
    }

    public MerchantDonationVerification clickOnPayButton() {
        scrollToElement(driver, btnPayDonation);
        testStepsLog(_logStep++, "Click on Pay button.");
        clickOn(driver, btnPayDonation);
        return new MerchantDonationVerification(driver);
    }

    public MerchantDonationVerification clickOnConfirmPayButton() {
        testStepsLog(_logStep++, "Click on Confirm button.");
        clickOn(driver, btnConfirmDonation);
        return new MerchantDonationVerification(driver);
    }

    public MerchantDonationVerification clickOnBackToHomePaid() {

        testStepsLog(_logStep++, "Click on Back To Home button.");
        clickOn(driver, btnBackToHomePay);

        return new MerchantDonationVerification(driver);

    }

    public MerchantDonationVerification clickOnBackToHomeDeleted() {

        testStepsLog(_logStep++, "Click on Back To Home button.");
        clickOn(driver, btnBackToHomeDeleted);

        return new MerchantDonationVerification(driver);

    }

    public MerchantDonationVerification searchPaidDonation() {

        pause(5);

        scrollToElement(driver, txtInputSearch);

        _searchCriteria = String.valueOf(getIntegerFromString(_donationID));

        testStepsLog(_logStep++, "Enter Search Criteria");
        testInfoLog("Search", _searchCriteria);
        type(txtInputSearch, _searchCriteria);

        return new MerchantDonationVerification(driver);
    }

    public MerchantDonationVerification getReceiverDonationAmounts() {
        _receiverDonationReceivedChart = getDoubleFromString(getInnerText(lblDonationAmountChart).split(",")[0]);
        _receiverDonationPaidChart = getDoubleFromString(getInnerText(lblDonationAmountChart).split(",")[1]);

        return new MerchantDonationVerification(driver);
    }

    public MerchantDonationVerification getSenderDonationAmounts() {
        _payDonationPaidChart = getDoubleFromString(getInnerText(lblDonationAmountChart).split(",")[0]);

        return new MerchantDonationVerification(driver);
    }

    public MerchantDonationVerification searchDeletedDonation() {

        pause(3);

        scrollToElement(driver, txtInputSearch);

        _searchCriteria = String.valueOf(getIntegerFromString(_donationID));

        testStepsLog(_logStep++, "Enter Search Criteria");
        testInfoLog("Search", _searchCriteria);
        type(txtInputSearch, _searchCriteria);

        return new MerchantDonationVerification(driver);
    }

    public MerchantDonationVerification filterDeletedDonationId() {

        scrollToElement(driver, txtFilterDonationID);

        scrollToElement(driver, txtFilterDonationID);
        testStepsLog(_logStep++, "Enter Donation ID Number : " + _donationID);
        type(txtFilterDonationID, _donationID);

        return new MerchantDonationVerification(driver);
    }

    public boolean isMerchantFilterRecordsDisplay() {
        return !isListEmpty(lstUserName);

    }

    public MerchantDonationVerification enterAmountMoreThanFunds() {
        _donationAmount = _donationSenderBalance + 100;
        testStepsLog(_logStep++, "Enter Amount : " + _donationAmount);
        type(txtDonationAmount, String.valueOf(_donationAmount));
        return new MerchantDonationVerification(driver);
    }
}
