package com.incashme.merchant.indexpage;

import com.framework.init.AbstractPage;
import com.incashme.merchant.verification.MerchantDashboardVerification;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class MerchantDashboardIndexPage extends AbstractPage {

    public MerchantDashboardIndexPage(WebDriver driver) {
        super(driver);
    }

    public static String _currentBalance = "";
    public static double _receiveMoney = 0.0d;

    public static String _searchCriteria = "";

    public static List<String> _userNameBeforeSort = new ArrayList<>();
    public static List<String> _userTxIDBeforeSort = new ArrayList<>();
    public static List<String> _userTypesBeforeSort = new ArrayList<>();
    public static List<LocalDateTime> _transactionDateTimeBeforeSort = new ArrayList<>();

    public static double _transferReceiverBalance;

    @FindBy(xpath = "//div//ul//li//a[contains(text(),'Change Password')]")
    private WebElement btnChangePassword;

    @FindBy(xpath = "//div//ul//li//a[contains(text(),'Change Pin')]")
    private WebElement btnChangePIN;

    @FindBy(xpath = "//div//ul//li//span[contains(text(),'Privacy')]")
    private WebElement btnPrivacyPolicy;

    @FindBy(xpath = "//div//ul//li//a[contains(text(),'Terms')]")
    private WebElement btnTermsConditions;

    @FindBy(xpath = "//div//ul//li//a[contains(text(),'KYC')]")
    private WebElement btnKYCandAML;

    @FindBy(xpath = "//div//ul//li//a[contains(text(),'Login')]")
    private WebElement btnLoginHistory;

    @FindBy(xpath = ".//div[contains(@class,'user-avatar')]//a")
    private WebElement btnUserProfile;

    @FindBy(xpath = "//*[text()='Change Password']/ancestor::div//button[contains(@class,'close')]")
    private WebElement btnClosePasswordPopup;

    @FindBy(xpath = "//*[text()='Change Pin']/ancestor::div//button[contains(@class,'close')]")
    private WebElement btnClosePINPopup;

    @FindBy(xpath = "//div/button/i[contains(@class,'mnu-opn')]")
    private WebElement btnExpandMenu;

    @FindBy(xpath = "//ul/li//a[@title='Home']")
    private WebElement btnMenuHome;

    @FindBy(xpath = "//ul/li//a[@title='Balance']")
    private WebElement btnMenuBalance;

    @FindBy(xpath = "//div[contains(@class,'blnc-val')]")
    private static WebElement lblBalance;

    @FindBy(xpath = "//input[@formcontrolname='amount']")
    private WebElement txtTopUpAmount;

    @FindBy(xpath = "//*[text()='Receive Money']/ancestor::div//button[contains(@class,'close')]")
    private WebElement btnCloseReceiveMoney;

    @FindBy(xpath = "//div[contains(@class,'trfr-tpup-card')]//form//button")
    private WebElement btnStartReceiveMoney;

    @FindBy(xpath = "//div[contains(@class,'merc_barcode')]/img")
    private WebElement imgQRCode;

    @FindBy(xpath = "//div[contains(@class,'merc_barcode')]/following-sibling::div[contains(@class,'merc_scan_time')]")
    private WebElement lblRemainingTime;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dash-trans-history-table']//tr//td[1]//h5[last()]")})
    private List<WebElement> lstDashboardUserName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dash-trans-history-table']//tr//td[1]//following-sibling::div[last()]")})
    private List<WebElement> lstDashboardUserIDNumber;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dash-trans-history-table']//tr//td[3]//div")})
    private List<WebElement> lstDashboardUserType;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dash-trans-history-table']//tr//td[2]//span")})
    private List<WebElement> lstDashboardTransactionID;

    @FindBy(xpath = "//table[@id='dash-trans-history-table']//th[1]")
    private WebElement lblUserNameHeader;

    @FindBy(xpath = "//table[@id='dash-trans-history-table']//th[2]")
    private WebElement lblTxIDHeader;

    @FindBy(xpath = "//table[@id='dash-trans-history-table']//th[3]")
    private WebElement lblUserTypeHeader;

    @FindBy(xpath = "//table[@id='dash-trans-history-table']//th[6]")
    private WebElement lblTransactionDateTime;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dash-trans-history-table']//tr//td[6]")})
    private List<WebElement> lstDashboardTransactionTime;

    @FindBy(xpath = "//input[@type='search']")
    private WebElement txtInputSearch;

    @FindBy(xpath = "//input[@id='oldpassword']")
    private WebElement txtCurrentPassword;

    @FindBy(xpath = "//input[@id='password']")
    private WebElement txtNewPassword;

    @FindBy(xpath = "//input[@id='cnfpwd']")
    private WebElement txtConfirmPassword;

    @FindBy(xpath = "//input[@formcontrolname='oldpin']")
    private WebElement txtCurrentPIN;

    @FindBy(xpath = "//input[@formcontrolname='pin']")
    private WebElement txtNewPIN;

    @FindBy(xpath = "//input[@formcontrolname='pin2']")
    private WebElement txtConfirmPIN;

    @FindAll(value = {@FindBy(xpath = ".//div[contains(@class,'login-form')]//button[@type='submit']")})
    private List<WebElement> btnSubmit;

    public static void getCurrentBalance() {
        pause(5);
        while (getInnerText(lblBalance).isEmpty()) {
            pause(1);
        }
        _currentBalance = getInnerText(lblBalance);
    }

    public MerchantDashboardVerification openUserProfile() {

        testStepsLog(_logStep++, "Click on User Avatar icon.");
        clickOn(driver, btnUserProfile);

        return new MerchantDashboardVerification(driver);
    }

    public MerchantDashboardVerification clickOnHomeMenu() {

        testStepsLog(_logStep++, "Click on Home icon.");
        clickOn(driver, btnMenuHome);

        return new MerchantDashboardVerification(driver);
    }

    public MerchantDashboardVerification clickOnChangePassword() {

        testStepsLog(_logStep++, "Click on Change Password button.");
        clickOn(driver, btnChangePassword);

        return new MerchantDashboardVerification(driver);
    }

    public MerchantDashboardVerification clickOnChangePIN() {

        testStepsLog(_logStep++, "Click on Change PIN button.");
        pause(1);
        clickOn(driver, btnChangePIN);

        return new MerchantDashboardVerification(driver);
    }

    public MerchantDashboardVerification closePasswordPopUp() {

        testStepsLog(_logStep++, "Click on Close button.");
        clickOn(driver, btnClosePasswordPopup);

        return new MerchantDashboardVerification(driver);
    }

    public MerchantDashboardVerification closePINPopUp() {

        testStepsLog(_logStep++, "Click on Close button.");
        clickOn(driver, btnClosePINPopup);

        return new MerchantDashboardVerification(driver);
    }

    public MerchantDashboardVerification clickOnPrivacyPolicy() {

        testStepsLog(_logStep++, "Click on Privacy Policy button.");
        clickOn(driver, btnPrivacyPolicy);

        return new MerchantDashboardVerification(driver);
    }

    public MerchantDashboardVerification clickOnTermsCondition() {

        testStepsLog(_logStep++, "Click on Terms & Condition button.");
        clickOn(driver, btnTermsConditions);

        return new MerchantDashboardVerification(driver);
    }

    public MerchantDashboardVerification clickOnKYCAMLPolicy() {

        testStepsLog(_logStep++, "Click on KYC & AML Policy button.");
        clickOn(driver, btnKYCandAML);

        return new MerchantDashboardVerification(driver);
    }

    public MerchantDashboardVerification clickOnLoginHistory() {

        testStepsLog(_logStep++, "Click on Login History button.");
        clickOn(driver, btnLoginHistory);

        return new MerchantDashboardVerification(driver);
    }

    public MerchantDashboardVerification expandDashboardMenu() {

        testStepsLog(_logStep++, "Click on Expand Menu icon.");
        clickOn(driver, btnExpandMenu);

        return new MerchantDashboardVerification(driver);
    }

    public MerchantDashboardVerification clickOnBalanceMenu() {

        testStepsLog(_logStep++, "Click on Balance button.");
        clickOn(driver, btnMenuBalance);

        return new MerchantDashboardVerification(driver);
    }

    public MerchantDashboardVerification enterZeroAmount() {

        _receiveMoney = 0.00;

        testStepsLog(_logStep++, "Enter Amount to Receive");
        testInfoLog("Amount", String.valueOf(_receiveMoney));
        type(txtTopUpAmount, String.valueOf(_receiveMoney));

        return new MerchantDashboardVerification(driver);
    }

    public MerchantDashboardVerification enterMaximumAmount() {

        _receiveMoney = 100000.22;

        testStepsLog(_logStep++, "Enter Amount to Receive");
        testInfoLog("Amount", String.valueOf(_receiveMoney));
        type(txtTopUpAmount, String.valueOf(_receiveMoney));

        return new MerchantDashboardVerification(driver);
    }

    public MerchantDashboardVerification enterValidAmount() {

        _receiveMoney = getDoubleFromString(getRandomNumberBetween(100, 200) + "." + getRandomNumberBetween(10, 99));

        testStepsLog(_logStep++, "Enter Amount to Receive");
        testInfoLog("Amount", String.valueOf(_receiveMoney));
        type(txtTopUpAmount, String.valueOf(_receiveMoney));

        clickOnSendButton();

        return new MerchantDashboardVerification(driver);
    }

    public MerchantDashboardVerification clickOnCloseReceiveMoneyPopUp() {

        testStepsLog(_logStep++, "Click on Close Receive Money Popup.");
        clickOn(driver, btnCloseReceiveMoney);

        return new MerchantDashboardVerification(driver);
    }

    public MerchantDashboardVerification clickOnSendButton() {

        testStepsLog(_logStep++, "Click on Send button.");
        clickOn(driver, btnStartReceiveMoney);

        return new MerchantDashboardVerification(driver);
    }

    public MerchantDashboardVerification waitForPopUpExpire() {

        pause(3);

        while (isElementPresent(imgQRCode)) {
            testInfoLog("Popup Timer", getText(lblRemainingTime));
            pause(30);
        }

        return new MerchantDashboardVerification(driver);
    }

    public boolean isSalesHistoryAvailable() {
        return !isListEmpty(lstDashboardUserName);
    }

    public MerchantDashboardVerification clickOnUserNameToSort() {

        updateShowList(driver, findElementByName(driver, "dash-trans-history-table_length"), "100");

        for (WebElement name : lstDashboardUserName) {
            _userNameBeforeSort.add(getInnerText(name));
        }

        testStepsLog(_logStep++, "Click On Name/Id to sort.");
        clickOn(driver, lblUserNameHeader);

        return new MerchantDashboardVerification(driver);

    }

    public MerchantDashboardVerification clickOnTransactionIDToSort() {

        updateShowList(driver, findElementByName(driver, "dash-trans-history-table_length"), "100");

        for (WebElement name : lstDashboardTransactionID) {
            _userTxIDBeforeSort.add(getInnerText(name));
        }

        testStepsLog(_logStep++, "Click On Transaction ID to sort.");
        clickOn(driver, lblTxIDHeader);

        return new MerchantDashboardVerification(driver);

    }

    public MerchantDashboardVerification clickOnTypesToSort() {

        updateShowList(driver, findElementByName(driver, "dash-trans-history-table_length"), "100");

        for (WebElement name : lstDashboardUserType) {
            _userTypesBeforeSort.add(getInnerText(name));
        }

        testStepsLog(_logStep++, "Click On Types to sort.");
        clickOn(driver, lblUserTypeHeader);
        clickOn(driver, lblUserTypeHeader);

        return new MerchantDashboardVerification(driver);

    }

    public MerchantDashboardVerification clickOnDateTimeToSort() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        updateShowList(driver, findElementByName(driver, "dash-trans-history-table_length"), "100");

        for (WebElement name : lstDashboardTransactionTime) {
            _transactionDateTimeBeforeSort.add(LocalDateTime.parse(getInnerText(name), format));
        }

        testStepsLog(_logStep++, "Click On Date & Time to sort.");
        clickOn(driver, lblTransactionDateTime);

        return new MerchantDashboardVerification(driver);

    }

    public MerchantDashboardVerification enterDashboardSearchCriteria(boolean isInvalid) {

        if (isInvalid) {

            scrollToElement(driver, txtInputSearch);

            String searchCriteria = getRandomCharacters(10);
            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", searchCriteria);
            type(txtInputSearch, searchCriteria);

        } else {

            scrollToElement(driver, txtInputSearch);

            _searchCriteria = getInnerText(lstDashboardTransactionID.get(0));

            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", _searchCriteria);
            type(txtInputSearch, _searchCriteria);
        }

        return new MerchantDashboardVerification(driver);
    }

    public MerchantDashboardVerification changeInvalidCurrentPassword() {

        String randomPassword = getRandomPassword();

        enterCurrentPassword(getRandomPassword());
        enterNewPassword(randomPassword);
        enterConfirmNewPassword(randomPassword);

        clickOnSubmitButton();

        return new MerchantDashboardVerification(driver);
    }

    private void enterNewPassword(String newPassword) {
        testStepsLog(_logStep++, "Enter New Password.");
        testInfoLog("New Password", newPassword);
        type(txtNewPassword, newPassword);
    }

    private void enterCurrentPassword(String currentPassword) {
        testStepsLog(_logStep++, "Enter Current Password.");
        testInfoLog("Current Password", currentPassword);
        type(txtCurrentPassword, currentPassword);
    }

    private void enterConfirmNewPassword(String confirmPassword) {
        testStepsLog(_logStep++, "Enter Confirm Password.");
        testInfoLog("Confirm Password", confirmPassword);
        type(txtConfirmPassword, confirmPassword);
    }

    private void clickOnSubmitButton() {

        testStepsLog(_logStep++, "Click on Submit button.");
        clickOn(driver, btnSubmit.get(0));

    }

    private void clickOnSubmitPINButton() {

        testStepsLog(_logStep++, "Click on Submit button.");
        clickOn(driver, btnSubmit.get(1));

    }

    public MerchantDashboardVerification changeInvalidNewPassword() {

        String randomPassword = String.valueOf(getRandomNumber());

        enterCurrentPassword(password);
        enterNewPassword(randomPassword);
        enterConfirmNewPassword(randomPassword);

        return new MerchantDashboardVerification(driver);

    }

    public MerchantDashboardVerification changeInvalidConfirmPassword() {

        enterCurrentPassword(password);
        enterNewPassword(getRandomPassword());
        enterConfirmNewPassword(getRandomPassword());

        pause(1);

        clickOnSubmitButton();

        return new MerchantDashboardVerification(driver);

    }

    public MerchantDashboardVerification changeAsPastPassword() {

        enterCurrentPassword(password);
        enterNewPassword(password);
        enterConfirmNewPassword(password);

        clickOnSubmitButton();

        return new MerchantDashboardVerification(driver);

    }

    public MerchantDashboardVerification changeInvalidCurrentPIN() {

        int randomPIN = getRandomPIN();

        enterCurrentPIN(String.valueOf(getRandomPIN()));
        enterNewPIN(String.valueOf(randomPIN));
        enterConfirmNewPIN(String.valueOf(randomPIN));

        clickOnSubmitPINButton();

        return new MerchantDashboardVerification(driver);
    }

    private void enterNewPIN(String newPassword) {
        testStepsLog(_logStep++, "Enter New PIN.");
        testInfoLog("New PIN", newPassword);
        type(txtNewPIN, newPassword);
    }

    private void enterCurrentPIN(String currentPassword) {
        testStepsLog(_logStep++, "Enter Current PIN.");
        testInfoLog("Current PIN", currentPassword);
        type(txtCurrentPIN, currentPassword);
    }

    private void enterConfirmNewPIN(String confirmPassword) {
        testStepsLog(_logStep++, "Enter Confirm PIN.");
        testInfoLog("Confirm PIN", confirmPassword);
        type(txtConfirmPIN, confirmPassword);
    }

    public MerchantDashboardVerification changeInvalidNewPIN() {

        String randomPassword = getRandomNumberBetween(100, 999) + getRandomCharacters(3);

        enterCurrentPIN(testData);
        enterNewPIN(randomPassword);
        enterConfirmNewPIN(randomPassword);

        return new MerchantDashboardVerification(driver);

    }

    public MerchantDashboardVerification changeInvalidConfirmPIN() {

        enterCurrentPIN(testData);
        enterNewPIN(String.valueOf(getRandomPIN()));
        enterConfirmNewPIN(String.valueOf(getRandomPIN()));

        pause(1);

        clickOnSubmitPINButton();

        return new MerchantDashboardVerification(driver);

    }

    public MerchantDashboardVerification changeAsPastPin() {

        enterCurrentPIN(testData);
        enterNewPIN(testData);
        enterConfirmNewPIN(testData);

        clickOnSubmitPINButton();

        return new MerchantDashboardVerification(driver);

    }

    public MerchantDashboardVerification getTransferReceiverBalance() {
        MerchantDashboardIndexPage.getCurrentBalance();
        _transferReceiverBalance = getDoubleFromString(MerchantDashboardIndexPage._currentBalance);
        return new MerchantDashboardVerification(driver);
    }
}
