package com.incashme.merchant.indexpage;

import com.framework.init.AbstractPage;
import com.incashme.merchant.verification.MerchantTransactionHistoryVerification;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MerchantTransactionHistoryIndexPage extends AbstractPage {

    public MerchantTransactionHistoryIndexPage(WebDriver driver) {
        super(driver);
    }

    public static String _searchCriteria = "";
    public static String _filterRandomValue = "";

    public static boolean _isRandomFilter;

    public static double _additionalDeviceRefund = 0.0d;
    private static boolean isBalanceDeducted = true;

    public static String _filterFirstName = "";
    public static String _filterLastName = "";
    public static String _filterShopName = "";
    public static String _filterPhone = "";
    public static String _filterID = "";
    public static String _filterTransactionId = "";
    public static double _filterTransactionFromAmount = 0.0d;
    public static double _filterTransactionToAmount = 0.0d;
    public static String _filterTransactionType = "";
    public static String _transactionDateAndTime = "";
    public static String _transactionTotalAmount = "";

    public static String _refundTransactionID = "";
    public static double _refundAmount = 0.0d;
    public static String _refundReceiverName = "";
    public static String _refundSenderName = "";
    public static String _refundTransactionType = "";
    public static String _refundCompleteTime = "";
    public static String _latestTransactionID = "";
    public static double _latestTransactionAmount = 0.0d;
    public static String _latestRefundReceiverName = "";
    public static String _latestRefundSenderName = "";
    public static String _latestTransactionType = "";
    public static String _refundAdditionalDeviceName = "";
    public static String _refundAdditionalDeviceNumber = "";

    public static List<Double> _transactionAmount = new ArrayList<>();

    private String[] transactionTypes = {"Take Transfer", "Sell", "Take Invoice", "Take Donation", "Donate", "Topup Commission",
            "Refund", "Take Refund", "Deposit", "Withdraw", "Refunded", "Buy", "Invoice", "Transfer"};

    @FindBy(xpath = "//ul/li//a[@title='Transaction History']")
    private WebElement btnMenuTxHistory;

    @FindBy(xpath = "//div//p[contains(text(),'no data alive')]")
    private WebElement lblNoTransaction;

    @FindBy(xpath = "//input[@type='search']")
    private WebElement txtInputSearch;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[1]//h5")})
    private List<WebElement> lstUserName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[1]//following-sibling::div")})
    private List<WebElement> lstUserIdNumber;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[2]/span")})
    private List<WebElement> lstUserTxID;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[3]//div")})
    private List<WebElement> lstUserType;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[4]//div")})
    private List<WebElement> lstTransactionAmount;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[6]")})
    private List<WebElement> lstTransactionTime;

    @FindBy(xpath = "//li/button[contains(@class,'filterBtn')]")
    private WebElement btnFilter;

    @FindBy(xpath = "//select[@formcontrolname='o_utype']")
    private WebElement btnUserDropdown;

    @FindBy(xpath = "//select[@formcontrolname='type']")
    private WebElement btnTransactionTypeDropDown;

    @FindBy(xpath = "//input[@formcontrolname='o_fname']")
    private WebElement txtFilterFirstName;

    @FindBy(xpath = "//input[@formcontrolname='o_lname']")
    private WebElement txtFilterLastName;

    @FindBy(xpath = "//input[@formcontrolname='o_alt_cellnum']")
    private WebElement txtFilterConsumerMobile;

    @FindBy(xpath = "//input[@formcontrolname='id']")
    private WebElement txtFilterTransactionID;

    @FindBy(xpath = "//input[@formcontrolname='from_amount']")
    private WebElement txtFilterTransactionFromAmount;

    @FindBy(xpath = "//input[@formcontrolname='to_amount']")
    private WebElement txtFilterTransactionToAmount;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//button[contains(@class,'searchbtn')]")
    private WebElement btnFilterSearch;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//a[contains(text(),'Clear All')]")
    private WebElement btnFilterClearAll;

    @FindBy(xpath = "//input[@formcontrolname='mi_companyname']")
    private WebElement txtFilterShopName;

    @FindBy(xpath = "//input[@formcontrolname='other_user_id']")
    private WebElement txtFilterID;

    @FindBy(xpath = "//input[@formcontrolname='other_user_id']//following-sibling::div//a")
    private WebElement btnClearMerchantID;

    @FindBy(xpath = "//input[@formcontrolname='to_amount']//following-sibling::div//a")
    private WebElement btnClearAmountRange;

    @FindBy(xpath = "//input[@formcontrolname='id']//following-sibling::div//a")
    private WebElement btnClearTransactionID;

    @FindBy(xpath = "//select[@formcontrolname='type']/../../following-sibling::div//a")
    private WebElement btnClearTransactionType;

    @FindAll(value = {@FindBy(xpath = "//*[contains(text(),'Transaction History Search Filter')]//following-sibling::form//input[not(contains(@class,'selection'))]")})
    private List<WebElement> lstFilterFields;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[7]//button")})
    private List<WebElement> lstTransactionMoreButton;

    @FindBy(xpath = "//h5[contains(text(),'Transaction Detail')]//following-sibling::button")
    private WebElement btnCloseTransactionPopUp;

    @FindBy(xpath = "//div//p[contains(text(),'find any matches')]")
    private WebElement lblNoFilterConsumerAvailable;

    @FindBy(xpath = "//div[@role='dialog']//div[contains(@class,'transfer-id')]")
    private WebElement lblPopUpTransactionID;

    @FindAll(value = {@FindBy(xpath = "//*[contains(text(),'Transaction Details')]/../..//following-sibling::div//li//div[contains(@class,'text-right')]")})
    private List<WebElement> lstTransactionDetails;

    @FindBy(xpath = "//div[contains(@class,'csmer-trans-detail')]//button[contains(@class,'btn-green') and text()='Refund Money']")
    private WebElement btnRefund;

    @FindBy(xpath = "//div[contains(@class,'agnt-cnf-ftr')]//button[contains(@id,'send-mny')]")
    private WebElement btnSendRefund;

    @FindBy(xpath = "//div[contains(@class,'agnt-cnf-ftr')]//button[contains(@class,'btn-pink')]")
    private WebElement btnCancelRefund;

    @FindBy(xpath = "(//*[contains(text(),'Transaction Details')]/../..//following-sibling::div//li//div[contains(@class,'details-title')])[3]")
    private WebElement lblAddtionalDevice;

    @FindBy(xpath = "//input[@formcontrolname='amount']")
    private WebElement txtTransferRefundAmount;

    @FindBy(xpath = "//div[contains(@class,'agnt-cnf-ftr')]//button[contains(@class,'btn-green') and text()='Add Money']")
    private WebElement btnAddMoney;

    public MerchantTransactionHistoryVerification clickOnTxHistoryMenu() {

        testStepsLog(_logStep++, "Click on Transaction History button.");
        clickOn(driver, btnMenuTxHistory);

        return new MerchantTransactionHistoryVerification(driver);
    }

    public boolean isTransactionDataDisplay() {

        try {
            updateShowList(driver, findElementByName(driver, "trans-history-table_length"), "100");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        return !isElementPresent(lblNoTransaction);
    }

    public MerchantTransactionHistoryVerification enterTransactionSearchIDCriteria(boolean isInvalid) {

        if (isInvalid) {

            scrollToElement(driver, txtInputSearch);

            String searchCriteria = String.valueOf(getRandomNumber());
            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", searchCriteria);
            type(txtInputSearch, searchCriteria);

        } else {

            scrollToElement(driver, txtInputSearch);

            _searchCriteria = getInnerText(lstUserTxID.get(getRandomNumberBetween(0, lastIndexOf(lstUserTxID)))).split("-")[1];

            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", _searchCriteria);
            type(txtInputSearch, _searchCriteria);
        }

        return new MerchantTransactionHistoryVerification(driver);
    }

    public MerchantTransactionHistoryVerification searchRefundTransaction() {

        scrollToElement(driver, txtInputSearch);

        _searchCriteria = String.valueOf(getIntegerFromString(_refundTransactionID));
        _filterRandomValue = "0";

        testStepsLog(_logStep++, "Enter Search Criteria");
        testInfoLog("Search", _searchCriteria);
        type(txtInputSearch, _searchCriteria);

        return new MerchantTransactionHistoryVerification(driver);
    }

    public MerchantTransactionHistoryVerification clearSearchDetails() {

        scrollToElement(driver, txtInputSearch);

        testStepsLog(_logStep++, "Remove Search Criteria");
        clear(txtInputSearch);

        return new MerchantTransactionHistoryVerification(driver);
    }


    public MerchantTransactionHistoryVerification clickOnFilterIcon() {

        scrollToElement(driver, btnFilter);

        testStepsLog(_logStep++, "Click on Filter Button.");
        clickOn(driver, btnFilter);

        pause(2);

        return new MerchantTransactionHistoryVerification(driver);

    }

    public MerchantTransactionHistoryVerification getConsumerDetailsForFilter() {

        pause(2);

        try {
            updateShowList(driver, findElementByName(driver, "trans-history-table_length"), "100");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        for (int user = 0; user < sizeOf(lstUserType); user++) {
            if (getText(lstUserType.get(user)).equalsIgnoreCase("Consumer")) {
                _filterFirstName = getInnerText(lstUserName.get(user)).split(" ")[0].trim();
                _filterLastName = getInnerText(lstUserName.get(user)).replace(_filterFirstName, "").trim();
                _filterPhone = getInnerText(lstUserIdNumber.get(user));
                _filterTransactionId = String.valueOf(getIntegerFromString(getInnerText(lstUserTxID.get(user))));

                for (int num = 0; num < sizeOf(lstTransactionAmount); num++) {
                    _transactionAmount.add(getDoubleFromString(getInnerText(lstTransactionAmount.get(num))));
                }

                Collections.sort(_transactionAmount);

                break;
            }

        }

        return new MerchantTransactionHistoryVerification(driver);

    }

    public MerchantTransactionHistoryVerification changeFilterUserTo(String user) {

        scrollToElement(driver, btnUserDropdown);

        Select select = new Select(btnUserDropdown);

        switch (user.toLowerCase()) {
            case "consumer":
                select.selectByValue("user");
                break;
            case "merchant":
                select.selectByValue("merchant");
                break;
        }

        testStepsLog(_logStep++, "Select User : " + user);

        return new MerchantTransactionHistoryVerification(driver);

    }

    public MerchantTransactionHistoryVerification filterFirstName(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomCharacters(7);
            testStepsLog(_logStep++, "Enter First Name : " + _filterRandomValue);
            type(txtFilterFirstName, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter First Name : " + _filterFirstName);
            type(txtFilterFirstName, _filterFirstName);
        }

        return new MerchantTransactionHistoryVerification(driver);
    }

    public MerchantTransactionHistoryVerification clickConsumerOnFilterSearchButton() {

        testStepsLog(_logStep++, "Click on Search button.");
        clickOn(driver, btnFilterSearch);

        pause(2);

        try {
            updateShowList(driver, findElementByName(driver, "trans-history-table_length"), "100");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        pause(1);

        return new MerchantTransactionHistoryVerification(driver);

    }

    public MerchantTransactionHistoryVerification clearAllConsumerFilterDetails() {

        testStepsLog(_logStep++, "Click on Clear All button.");
        clickOn(driver, btnFilterClearAll);

        pause(2);

        try {
            updateShowList(driver, findElementByName(driver, "trans-history-table_length"), "100");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        pause(1);

        return new MerchantTransactionHistoryVerification(driver);

    }

    public MerchantTransactionHistoryVerification filterLastName(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomCharacters(7);
            testStepsLog(_logStep++, "Enter Last Name : " + _filterRandomValue);
            type(txtFilterLastName, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter Last Name : " + _filterLastName);
            type(txtFilterLastName, _filterLastName);
        }

        return new MerchantTransactionHistoryVerification(driver);
    }

    public MerchantTransactionHistoryVerification filterConsumerNumber(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomNumber() + "" + getRandomNumber();
            testStepsLog(_logStep++, "Enter Mobile Number : " + _filterRandomValue);
            type(txtFilterConsumerMobile, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter Mobile Number : " + _filterPhone);
            type(txtFilterConsumerMobile, _filterPhone);
        }

        return new MerchantTransactionHistoryVerification(driver);
    }

    public MerchantTransactionHistoryVerification filterTransactionId(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        scrollToElement(driver, txtFilterTransactionID);

        if (isInvalid) {
            _filterRandomValue = "Transaction ID - " + getRandomNumber() + "" + getRandomNumberBetween(1, 9);
            testStepsLog(_logStep++, "Enter Tx ID Number : " + _filterRandomValue);
            type(txtFilterTransactionID, _filterRandomValue);
        } else {
            scrollToElement(driver, txtFilterTransactionID);
            testStepsLog(_logStep++, "Enter Tx ID Number : " + _filterTransactionId);
            type(txtFilterTransactionID, _filterTransactionId);
        }

        return new MerchantTransactionHistoryVerification(driver);
    }

    public MerchantTransactionHistoryVerification enterTransactionAmountRange(boolean isInvalid) {

        _isRandomFilter = isInvalid;
        scrollToElement(driver, txtFilterTransactionToAmount);

        if (isInvalid) {
            double randomNumber = getRandomNumber();
            _filterTransactionFromAmount = randomNumber + 10;
            _filterTransactionToAmount = randomNumber;

            testStepsLog(_logStep++, "Enter From Amount Range : " + _filterTransactionFromAmount);
            type(txtFilterTransactionFromAmount, String.valueOf(_filterTransactionFromAmount));

            testStepsLog(_logStep++, "Enter To Amount Range : " + _filterTransactionToAmount);
            type(txtFilterTransactionToAmount, String.valueOf(_filterTransactionToAmount));
        } else {
            _filterTransactionFromAmount = formatTwoDecimal(findMin(_transactionAmount)) + 100;
            _filterTransactionToAmount = formatTwoDecimal(findMax(_transactionAmount)) - 100;

            testStepsLog(_logStep++, "Enter From Amount Range : " + _filterTransactionFromAmount);
            type(txtFilterTransactionFromAmount, String.valueOf(_filterTransactionFromAmount));

            testStepsLog(_logStep++, "Enter To Amount Range : " + _filterTransactionToAmount);
            type(txtFilterTransactionToAmount, String.valueOf(_filterTransactionToAmount));
        }

        return new MerchantTransactionHistoryVerification(driver);

    }

    public MerchantTransactionHistoryVerification enterTransactionAmount(String field) {

        scrollToElement(driver, txtFilterTransactionToAmount);

        _filterTransactionFromAmount = formatTwoDecimal(_transactionAmount.get(2));
        _filterTransactionToAmount = formatTwoDecimal(_transactionAmount.get(_transactionAmount.size() - 2));

        switch (field.toLowerCase()) {
            case "from":
                testStepsLog(_logStep++, "Enter From Amount Range : " + _filterTransactionFromAmount);
                type(txtFilterTransactionFromAmount, String.valueOf(_filterTransactionFromAmount));
                break;
            case "to":
                testStepsLog(_logStep++, "Enter To Amount Range : " + _filterTransactionToAmount);
                type(txtFilterTransactionToAmount, String.valueOf(_filterTransactionToAmount));
                break;
        }

        return new MerchantTransactionHistoryVerification(driver);

    }

    public MerchantTransactionHistoryVerification filterTransactionType() {

        scrollToElement(driver, btnTransactionTypeDropDown);

        Select select = new Select(btnTransactionTypeDropDown);

        int selection = getRandomNumberBetween(1, select.getOptions().size() - 1);
        select.selectByIndex(selection);

        _filterTransactionType = getText(select.getFirstSelectedOption());
        testStepsLog(_logStep++, "Select Transaction Type : " + _filterTransactionType);

        return new MerchantTransactionHistoryVerification(driver);
    }

    public MerchantTransactionHistoryVerification filterTransactionType(int index) {

        scrollToElement(driver, btnTransactionTypeDropDown);

        Select select = new Select(btnTransactionTypeDropDown);

        select.selectByVisibleText(transactionTypes[index]);

        _filterTransactionType = getText(select.getFirstSelectedOption());
        testStepsLog(_logStep++, "Select Transaction Type : " + _filterTransactionType);

        return new MerchantTransactionHistoryVerification(driver);
    }

    public MerchantTransactionHistoryVerification getMerchantDetailsForFilter() {

        pause(2);

        try {
            updateShowList(driver, findElementByName(driver, "trans-history-table_length"), "100");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        _transactionAmount.clear();

        for (int user = 0; user < sizeOf(lstUserType); user++) {
            if (getText(lstUserType.get(user)).equalsIgnoreCase("Merchant")) {
                _filterShopName = getInnerText(lstUserName.get(user));
                _filterID = getInnerText(lstUserIdNumber.get(user));
                _filterTransactionId = String.valueOf(getIntegerFromString(getInnerText(lstUserTxID.get(user))));

                for (int num = 0; num < sizeOf(lstTransactionAmount); num++) {
                    _transactionAmount.add(getDoubleFromString(getInnerText(lstTransactionAmount.get(num))));
                }

                Collections.sort(_transactionAmount);

                break;
            }

        }

        return new MerchantTransactionHistoryVerification(driver);

    }

    public MerchantTransactionHistoryVerification filterShopName(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomCharacters(7);
            testStepsLog(_logStep++, "Enter Shop Name : " + _filterRandomValue);
            type(txtFilterShopName, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter Shop Name : " + _filterShopName);
            type(txtFilterShopName, _filterShopName);
        }

        return new MerchantTransactionHistoryVerification(driver);
    }

    public MerchantTransactionHistoryVerification filterMerchantID(boolean isInvalid) {

        if (isInvalid) {
            _filterRandomValue = getRandomNumberBetween(10000, 99999) + "" + getRandomNumberBetween(10000, 99999);
            testStepsLog(_logStep++, "Enter Merchant ID");
            testInfoLog("Merchant ID", _filterRandomValue);
            type(txtFilterID, _filterRandomValue);

        } else {
            testStepsLog(_logStep++, "Enter Merchant ID");
            testInfoLog("Merchant ID", _filterID);
            type(txtFilterID, _filterID);
        }

        return new MerchantTransactionHistoryVerification(driver);
    }

    public MerchantTransactionHistoryVerification clearAllMerchantFilterDetails() {

        testStepsLog(_logStep++, "Click on Clear All button.");
        clickOn(driver, btnFilterClearAll);

        pause(2);

        try {
            updateShowList(driver, findElementByName(driver, "trans-history-table_length"), "100");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        for (int i = 0; i < sizeOf(lstFilterFields); i++) {
            if (isElementPresent(lstFilterFields.get(i))) {
                clickOn(driver, lstFilterFields.get(i));
                break;
            }
        }

        changeFilterUserTo("Merchant");

        pause(1);

        return new MerchantTransactionHistoryVerification(driver);

    }

    public MerchantTransactionHistoryVerification clearMerchantIDField() {

        testStepsLog(_logStep++, "Click on Clear button.");
        clickOn(driver, btnClearMerchantID);

        return new MerchantTransactionHistoryVerification(driver);
    }

    public MerchantTransactionHistoryVerification clearAmountRange() {

        testStepsLog(_logStep++, "Click on Clear Amount Range button.");
        clickOn(driver, btnClearAmountRange);

        return new MerchantTransactionHistoryVerification(driver);
    }

    public MerchantTransactionHistoryVerification clearTransactionID() {

        testStepsLog(_logStep++, "Click on Clear button.");
        clickOn(driver, btnClearTransactionID);

        return new MerchantTransactionHistoryVerification(driver);
    }

    public MerchantTransactionHistoryVerification clearTransactionType() {

        testStepsLog(_logStep++, "Click on Clear button.");
        clickOn(driver, btnClearTransactionType);

        return new MerchantTransactionHistoryVerification(driver);
    }

    public MerchantTransactionHistoryVerification getAnyConsumerDetails() {

        _filterRandomValue = String.valueOf(getRandomNumberBetween(0, sizeOf(lstUserName) - 1));

        _filterFirstName = getInnerText(lstUserName.get(getIntegerFromString(_filterRandomValue)));
        _filterPhone = getInnerText(lstUserIdNumber.get(getIntegerFromString(_filterRandomValue)));
        _filterTransactionId = String.valueOf(getIntegerFromString(getInnerText(lstUserTxID.get(getIntegerFromString(_filterRandomValue)))));
        _transactionDateAndTime = getInnerText(lstTransactionTime.get(getIntegerFromString(_filterRandomValue)));

        _transactionTotalAmount = getInnerText(lstTransactionAmount.get(getIntegerFromString(_filterRandomValue)));

        return new MerchantTransactionHistoryVerification(driver);
    }

    public MerchantTransactionHistoryVerification clickOnMOREButton() {

        scrollToElement(driver, lstTransactionMoreButton.get(getIntegerFromString(_filterRandomValue)));

        testStepsLog(_logStep++, "Click on MORE button.");
        clickOn(driver, lstTransactionMoreButton.get(getIntegerFromString(_filterRandomValue)));

        return new MerchantTransactionHistoryVerification(driver);

    }

    public MerchantTransactionHistoryVerification clickOnMOREButton(int number) {

        scrollToElement(driver, lstTransactionMoreButton.get(number));

        testStepsLog(_logStep++, "Click on MORE button.");
        clickOn(driver, lstTransactionMoreButton.get(number));

        return new MerchantTransactionHistoryVerification(driver);

    }

    public MerchantTransactionHistoryVerification clickOnClosePopUp() {

        pause(2);

        testStepsLog(_logStep++, "Click on Close button.");
        clickOn(driver, btnCloseTransactionPopUp);

        return new MerchantTransactionHistoryVerification(driver);

    }

    public boolean isFilterResultDisplay() {
        return !isListEmpty(lstTransactionMoreButton);
    }

    public MerchantTransactionHistoryVerification selectAnyTransaction() {

        try {
            updateShowList(driver, findElementByName(driver, "trans-history-table_length"), "100");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        _filterRandomValue = String.valueOf(getRandomNumberBetween(0, sizeOf(lstUserName) - 1));
        return new MerchantTransactionHistoryVerification(driver);
    }


    public MerchantTransactionHistoryVerification getTransactionDetails() {

        pause(3);

        _refundTransactionID = getText(lblPopUpTransactionID);
        testInfoLog("Selected Transaction", _refundTransactionID);
        _refundAmount = getDoubleFromString(getText(lstTransactionDetails.get(1)));
        _refundReceiverName = getText(lstTransactionDetails.get(2));
        _refundSenderName = getText(lstTransactionDetails.get(3));
        _refundTransactionType = getText(lstTransactionDetails.get(5));

        return new MerchantTransactionHistoryVerification(driver);
    }

    public MerchantTransactionHistoryVerification clickOnRefundButton() {

        testStepsLog(_logStep++, "Click on Refund Money button.");
        clickOn(driver, btnRefund);

        return new MerchantTransactionHistoryVerification(driver);

    }

    public MerchantTransactionHistoryVerification clickOnCancelRefundButton() {

        testStepsLog(_logStep++, "Click on Cancel button.");
        clickOn(driver, btnCancelRefund);

        return new MerchantTransactionHistoryVerification(driver);
    }

    public MerchantTransactionHistoryVerification clickOnSendRefundButton() {

        pause(2);
        scrollToElement(driver, btnSendRefund);
        pause(2);
        testStepsLog(_logStep++, "Click on Send button.");
        clickOn(driver, btnSendRefund);
        pause(2);
        System.out.println("button send : " + isElementPresent(btnSendRefund));
        try {
            if (isElementPresent(btnCancelRefund)) {
                System.out.println("Clicked on it");
            } else {
                scrollToElement(driver, btnSendRefund);
                clickOn(driver, btnSendRefund);
            }
        } catch (Exception e) {
            try {
                scrollToElement(driver, btnSendRefund);
                clickOn(driver, btnSendRefund);
            } catch (Exception e2) {
                System.out.println("Already Send clicked");
            }
        }

        return new MerchantTransactionHistoryVerification(driver);
    }

    public MerchantTransactionHistoryVerification getLatestTransactionDetails() {

        pause(3);

        _latestTransactionID = getText(lblPopUpTransactionID);
        _latestTransactionAmount = getDoubleFromString(getText(lstTransactionDetails.get(1)));
        _latestRefundReceiverName = getText(lstTransactionDetails.get(2));
        _latestRefundSenderName = getText(lstTransactionDetails.get(3));
        _latestTransactionType = getText(lstTransactionDetails.get(5));

        return new MerchantTransactionHistoryVerification(driver);
    }

    public boolean isAdditionalDeviceTransfer() {
        return getText(lblAddtionalDevice).equalsIgnoreCase("Additional Device");
    }

    public MerchantTransactionHistoryVerification getAdditionalDeviceIDName() {

        _refundAdditionalDeviceName = getText(lstTransactionDetails.get(2)).split("\n")[0];
        _refundAdditionalDeviceNumber = getText(lstTransactionDetails.get(2)).split("\n")[1];

        testStepsLog(_logStep++, "Additional Device Transaction found.");
        testInfoLog("Device ID", _refundAdditionalDeviceName);
        testInfoLog("Device Number", _refundAdditionalDeviceNumber);

        return new MerchantTransactionHistoryVerification(driver);

    }

    public static boolean isAdditionalDeviceBalanceLessThanRefundAmount() {
        return MerchantAdditionalDeviceIndexPage._additionalDeviceBalance < _refundAmount;
    }

    public MerchantTransactionHistoryVerification enterZeroRefundAmountToAddDevice() {

        _additionalDeviceRefund = 0.00;

        testStepsLog(_logStep++, "Enter Amount to add in Additional Device");
        testInfoLog("Amount", String.valueOf(_additionalDeviceRefund));
        type(txtTransferRefundAmount, String.valueOf(_additionalDeviceRefund));

        return new MerchantTransactionHistoryVerification(driver);
    }

    public MerchantTransactionHistoryVerification enterMaxRefundAmountToAddDevice() {

        _additionalDeviceRefund = 100000.22;

        testStepsLog(_logStep++, "Enter Amount to add in Additional Device");
        testInfoLog("Amount", String.valueOf(_additionalDeviceRefund));
        type(txtTransferRefundAmount, String.valueOf(_additionalDeviceRefund));

        return new MerchantTransactionHistoryVerification(driver);
    }

    public MerchantTransactionHistoryVerification enterRefundAmountToAddDevice() {

        _additionalDeviceRefund = _refundAmount;

        testStepsLog(_logStep++, "Enter Amount to add in Additional Device");
        testInfoLog("Amount", String.valueOf(_additionalDeviceRefund));
        type(txtTransferRefundAmount, String.valueOf(_additionalDeviceRefund));

        return new MerchantTransactionHistoryVerification(driver);
    }

    public MerchantTransactionHistoryVerification clickOnAddMoney() {

        testStepsLog(_logStep++, "Click on Add Money button.");
        clickOn(driver, btnAddMoney);

        return new MerchantTransactionHistoryVerification(driver);

    }

    public MerchantTransactionHistoryVerification setNoBalanceDeduction() {
        isBalanceDeducted = false;
        return new MerchantTransactionHistoryVerification(driver);
    }

    public boolean getIsBalanceDeducted() {
        return isBalanceDeducted;
    }
}
