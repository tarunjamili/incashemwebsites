package com.incashme.merchant.index;

import com.framework.init.SeleniumInit;
import com.incashme.merchant.indexpage.MerchantDashboardIndexPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class MerchantDashboardIndex extends SeleniumInit {

    @Test
    public void merchant_ProfileDetailsVerification() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_068 :: To verify Merchant profile details and profile links.");
        merchantLoginIndexPage.getVersion();

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        merchantLoginIndexPage.getLoginTime();

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (merchantLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantLoginVerification = merchantLoginIndexPage.clickOnUserName();

        merchantDashboardVerification = merchantDashboardIndexPage.clickOnLoginHistory();

        testVerifyLog("Verify user can see the Login History details.");
        if (merchantDashboardVerification.verifyLoginHistory()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantDashboardVerification = merchantDashboardIndexPage.openUserProfile();

        testVerifyLog("Verify user can see the profile details on the screen.");
        if (merchantDashboardVerification.verifyUserProfileDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantLoginVerification = merchantLoginIndexPage.clickOnUserName();

        merchantDashboardVerification = merchantDashboardIndexPage.clickOnChangePassword();

        testVerifyLog("Verify user can see the Change Password screen.");
        if (merchantDashboardVerification.verifyChangePasswordScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantDashboardVerification = merchantDashboardIndexPage.closePasswordPopUp();

        merchantDashboardVerification = merchantDashboardIndexPage.clickOnChangePIN();

        testVerifyLog("Verify user can see the Change Password screen.");
        if (merchantDashboardVerification.verifyChangePINScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantDashboardVerification = merchantDashboardIndexPage.closePINPopUp();

        merchantDashboardVerification = merchantDashboardIndexPage.clickOnPrivacyPolicy();

        testVerifyLog("Verify user can see the Privacy Policy screen.");
        if (merchantDashboardVerification.verifyPrivacyPolicyScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantDashboardVerification = merchantDashboardIndexPage.clickOnTermsCondition();

        testVerifyLog("Verify user can see the Terms & Condition screen.");
        if (merchantDashboardVerification.verifyTnCScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantDashboardVerification = merchantDashboardIndexPage.clickOnKYCAMLPolicy();

        testVerifyLog("Verify user can see the KYC & AML Policy screen.");
        if (merchantDashboardVerification.verifyKYCandAMLScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantLoginVerification = merchantLoginIndexPage.clickOnUserName();

        merchantLoginVerification = merchantLoginIndexPage.clickOnLogout();

        testVerifyLog("Verify user logout successfully from the Application.");
        if (merchantLoginVerification.verifyUserLogoutSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void merchant_DashboardLinksAndDetails() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_072 :: To verify Dashboard Screen and Menu Links. <br>" +
                "ICM_SC_080 :: To verify Merchant can see the Balance on the screen.");
        merchantLoginIndexPage.getVersion();

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (merchantLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify user can see the Dashboard screen properly.");
        if (merchantDashboardVerification.verifyDashboardScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        MerchantDashboardIndexPage.getCurrentBalance();

        merchantDashboardVerification = merchantDashboardIndexPage.expandDashboardMenu();

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnInvoiceMenu();

        testVerifyLog("Verify user can see the Invoice screen properly.");
        if (merchantInvoiceVerification.verifyInvoiceScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnTxHistoryMenu();

        testVerifyLog("Verify user can see the Transaction History screen properly.");
        if (merchantDashboardVerification.verifyTxHistoryScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantStatementVerification = merchantStatementIndexPage.clickOnStatementMenu();

        testVerifyLog("Verify user can see the Statement screen properly.");
        if (merchantStatementVerification.verifyStatementsScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantDashboardVerification = merchantDashboardIndexPage.clickOnBalanceMenu();

        testVerifyLog("Verify user can see the Balance screen properly.");
        if (merchantDashboardVerification.verifyBalanceScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantDonationVerification = merchantDonationIndexPage.clickOnDonationMenu();

        testVerifyLog("Verify user can see the Donation screen properly.");
        if (merchantDonationVerification.verifyDonationScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantAdditionalDeviceVerification = merchantAdditionalDeviceIndexPage.clickOnAdditionalDeviceMenu();

        testVerifyLog("Verify user can see the Additional Device screen properly.");
        if (merchantAdditionalDeviceVerification.verifyAddDeviceScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void merchant_MerchantReceiveMoneyByQR() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_073 :: To verify Merchant can generate QR Code for accepting the payment.");
        merchantLoginIndexPage.getVersion();

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (merchantLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            merchantDashboardVerification = merchantDashboardIndexPage.enterZeroAmount();

            testVerifyLog("Verify validation message for the Zero Amount.");
            if (merchantDashboardVerification.verifyZeroAmountValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantDashboardVerification = merchantDashboardIndexPage.enterMaximumAmount();

            testVerifyLog("Verify validation message amount more than 100000.");
            if (merchantDashboardVerification.verifyMaxAmountValidation1()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        merchantDashboardVerification = merchantDashboardIndexPage.enterValidAmount();

        testVerifyLog("Verify QR Code display with the amount to receive.");
        if (merchantDashboardVerification.verifyReceiveMoneyQRScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantDashboardVerification = merchantDashboardIndexPage.clickOnCloseReceiveMoneyPopUp();

        testVerifyLog("Verify Receive Money Popup screen closed.");
        if (merchantDashboardVerification.verifyPopUpScreenClosed()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantDashboardVerification = merchantDashboardIndexPage.clickOnSendButton();

        merchantDashboardVerification = merchantDashboardIndexPage.waitForPopUpExpire();

        testVerifyLog("Verify Receive Money Popup screen closed after it expires.");
        if (merchantDashboardVerification.verifyPopUpScreenClosed()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");

    }

    @Test
    public void merchant_sortAndSearchForSalesTable() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_074 :: To verify Merchant can search and sort the Dashboard Sales history.");
        merchantLoginIndexPage.getVersion();

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (merchantLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (merchantDashboardIndexPage.isSalesHistoryAvailable()) {

            merchantDashboardVerification = merchantDashboardIndexPage.clickOnUserNameToSort();

            testVerifyLog("Verify User Name are sorted successfully.");
            if (merchantDashboardVerification.verifyUserNameSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantDashboardVerification = merchantDashboardIndexPage.clickOnTransactionIDToSort();

            testVerifyLog("Verify Transaction ID are sorted successfully.");
            if (merchantDashboardVerification.verifyUserTxIDSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantDashboardVerification = merchantDashboardIndexPage.clickOnTypesToSort();

            testVerifyLog("Verify User Type are sorted successfully.");
            if (merchantDashboardVerification.verifyUserTypeSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantDashboardVerification = merchantDashboardIndexPage.clickOnDateTimeToSort();

            testVerifyLog("Verify Transaction Date & Time are sorted successfully.");
            if (merchantDashboardVerification.verifyTransactionDateTimeSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantDashboardVerification = merchantDashboardIndexPage.enterDashboardSearchCriteria(false);

            testVerifyLog("Verify user can search details successfully.");
            if (merchantDashboardVerification.verifyTableSearchSuccessful()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            if (!isPositiveExecution) {
                merchantDashboardVerification = merchantDashboardIndexPage.enterDashboardSearchCriteria(true);

                testVerifyLog("Verify validation message for the invalid search details.");
                if (merchantDashboardVerification.verifyTableValidationForInvalidSearch()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

        } else {
            testWarningLog("No Sales History found for the Merchant, try to run with the Merchant who has Sales History for today.");
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");

    }

    @Test(enabled = false)
    public void merchant_ChangePassword() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_069 :: To verify Merchant can change password successfully.");
        merchantLoginIndexPage.getVersion();

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (merchantLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantLoginVerification = merchantLoginIndexPage.clickOnUserName();
        merchantDashboardVerification = merchantDashboardIndexPage.clickOnChangePassword();

        testVerifyLog("Verify user can see the Change Password screen.");
        if (merchantDashboardVerification.verifyChangePasswordScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantDashboardVerification = merchantDashboardIndexPage.changeInvalidCurrentPassword();

        testVerifyLog("Verify validation message for the invalid current password.");
        if (merchantDashboardVerification.verifyInvalidCurrentPasswordValidation()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantDashboardVerification = merchantDashboardIndexPage.changeInvalidNewPassword();

        testVerifyLog("Verify validation message for the invalid new password.");
        if (merchantDashboardVerification.verifyInvalidNewPasswordValidation()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantDashboardVerification = merchantDashboardIndexPage.closePasswordPopUp();
        merchantDashboardVerification = merchantDashboardIndexPage.clickOnChangePassword();
        merchantDashboardVerification = merchantDashboardIndexPage.changeInvalidConfirmPassword();

        testVerifyLog("Verify validation message for the different new and confirm password.");
        if (merchantDashboardVerification.verifyInvalidConfirmPasswordValidation()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantDashboardVerification = merchantDashboardIndexPage.changeAsPastPassword();

        testVerifyLog("Verify validation message for the past password as new password.");
        if (merchantDashboardVerification.verifyPastPasswordValidation()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }


        merchantDashboardVerification = merchantDashboardIndexPage.changeInvalidCurrentPassword();

        testVerifyLog("Verify validation message for the invalid current password.");
        if (merchantDashboardVerification.verifyInvalidCurrentPasswordSecondTime()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantDashboardVerification = merchantDashboardIndexPage.changeInvalidCurrentPassword();

        testVerifyLog("Verify user account blocked after three invalid attempts.");
        if (merchantDashboardVerification.verifyAccountIsBlockedForInvalidPassword()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify user logout successfully from the Application.");
        if (merchantLoginVerification.verifyUserLogoutSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can see the account locked validation.");
        if (merchantLoginVerification.verifyUserLockedSuccessfullyForWrongPassword()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test(enabled = false)
    public void merchant_ChangePIN() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_070 :: To verify Merchant can change PIN successfully.");
        merchantLoginIndexPage.getVersion();

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (merchantLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantLoginVerification = merchantLoginIndexPage.clickOnUserName();
        merchantDashboardVerification = merchantDashboardIndexPage.clickOnChangePIN();

        testVerifyLog("Verify user can see the Change PIN screen.");
        if (merchantDashboardVerification.verifyChangePINScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantDashboardVerification = merchantDashboardIndexPage.changeInvalidCurrentPIN();

        testVerifyLog("Verify validation message for the invalid current PIN.");
        if (merchantDashboardVerification.verifyInvalidCurrentPINValidation()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantDashboardVerification = merchantDashboardIndexPage.changeInvalidNewPIN();

        testVerifyLog("Verify validation message for the invalid new PIN.");
        if (merchantDashboardVerification.verifyInvalidNewPINValidation()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantDashboardVerification = merchantDashboardIndexPage.changeInvalidConfirmPIN();

        testVerifyLog("Verify validation message for the different new and confirm testData.");
        if (merchantDashboardVerification.verifyInvalidConfirmPinValidation()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantDashboardVerification = merchantDashboardIndexPage.changeAsPastPin();

        testVerifyLog("Verify validation message for the past testData as new testData.");
        if (merchantDashboardVerification.verifyPastPinValidation()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantDashboardVerification = merchantDashboardIndexPage.changeInvalidCurrentPIN();

        testVerifyLog("Verify validation message for the invalid current PIN.");
        if (merchantDashboardVerification.verifyInvalidCurrentPINSecondTime()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantDashboardVerification = merchantDashboardIndexPage.changeInvalidCurrentPIN();

        testVerifyLog("Verify user account blocked after three invalid attempts.");
        if (merchantDashboardVerification.verifyAccountIsBlockedForInvalidPIN()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify user logout successfully from the Application.");
        if (merchantLoginVerification.verifyUserLogoutSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can see the account locked validation.");
        if (merchantLoginVerification.verifyUserLockedSuccessfullyForWrongPIN()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void merchant_AddNewBankAccount() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_081 :: To verify Merchant can add a new bank account to withdraw money");
        merchantLoginIndexPage.getVersion();

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (merchantLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        MerchantDashboardIndexPage.getCurrentBalance();

        merchantLoginVerification = merchantLoginIndexPage.clickOnUserName();

        merchantWithdrawVerification = merchantWithdrawIndexPage.clickOnWithdrawMenu();

        testVerifyLog("Verify user can withdraw screen properly.");
        if (merchantWithdrawVerification.verifyWithdrawScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!merchantWithdrawIndexPage.isAllBankAccountAdded()) {

            merchantLoginVerification = merchantLoginIndexPage.clickOnUserName();

            merchantWithdrawVerification = merchantWithdrawIndexPage.clickOnAddBankAccount();

            testVerifyLog("Verify user can add new bank account form screen.");
            if (merchantWithdrawVerification.verifyAddNewAccountForm()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantWithdrawVerification = merchantWithdrawIndexPage.clickOnCloseAddBankForm();

            testVerifyLog("Verify user close add new bank account form screen.");
            if (merchantWithdrawVerification.verifyCloseBankAccountScreen()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantWithdrawVerification = merchantWithdrawIndexPage.clickOnAddBankAccount();

            if (!isPositiveExecution) {
                merchantWithdrawVerification = merchantWithdrawIndexPage.enterDisplayName(true);

                testVerifyLog("Verify validation message for invalid display name.");
                if (merchantWithdrawVerification.verifyInvalidDisplayNameValidation()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            merchantWithdrawVerification = merchantWithdrawIndexPage.enterDisplayName(false);

            if (!isPositiveExecution) {
                merchantWithdrawVerification = merchantWithdrawIndexPage.enterAccountHolderName(true);

                testVerifyLog("Verify validation message for invalid account holder name.");
                if (merchantWithdrawVerification.verifyInvalidHolderNameNameValidation()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            merchantWithdrawVerification = merchantWithdrawIndexPage.enterAccountHolderName(false);

            merchantWithdrawVerification = merchantWithdrawIndexPage.selectBank();

            merchantWithdrawVerification = merchantWithdrawIndexPage.selectAccountType();

            merchantWithdrawVerification = merchantWithdrawIndexPage.enterAccountNumberWithSpace();

            testVerifyLog("Verify validation message for invalid account account number.");
            if (merchantWithdrawVerification.verifyInvalidAccountNumberValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            if (!isPositiveExecution) {
                merchantWithdrawVerification = merchantWithdrawIndexPage.enterAccountNumber(true);

                testVerifyLog("Verify validation message for invalid account account number.");
                if (merchantWithdrawVerification.verifyInvalidAccountNumberValidation()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            merchantWithdrawVerification = merchantWithdrawIndexPage.enterAccountNumber(false);

            if (!isPositiveExecution) {
                merchantWithdrawVerification = merchantWithdrawIndexPage.enterIFSCCode(true);

                testVerifyLog("Verify validation message for invalid IFSC Code.");
                if (merchantWithdrawVerification.verifyInvalidIFSCNumberValidation()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            merchantWithdrawVerification = merchantWithdrawIndexPage.enterIFSCCode(false);

            merchantWithdrawVerification = merchantWithdrawIndexPage.clickOnAddNewBankAccount();

            merchantWithdrawVerification = merchantWithdrawIndexPage.clickCancelAddBankAccountButton();

            merchantWithdrawVerification = merchantWithdrawIndexPage.clickOnAddNewBankAccount();

            merchantWithdrawVerification = merchantWithdrawIndexPage.clickOKAddBankAccountButton();

            testVerifyLog("Verify confirmation message for the Bank Account added.");
            if (merchantWithdrawVerification.verifyConfirmationForBankAccount()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            testVerifyLog("Verify Added Bank Account details display in Money Source.");
            if (merchantWithdrawVerification.verifyBankAccountDetails()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

        } else {
            testWarningLog("Failed to add a new bank account, currently max bank account added, either remove one or try to run the scenario with different account.");
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void merchant_DeleteBankAccount() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_082 :: To verify Merchant can delete a bank account from list.");
        merchantLoginIndexPage.getVersion();

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (merchantLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        MerchantDashboardIndexPage.getCurrentBalance();

        merchantLoginVerification = merchantLoginIndexPage.clickOnUserName();

        merchantWithdrawVerification = merchantWithdrawIndexPage.clickOnWithdrawMenu();

        testVerifyLog("Verify user can withdraw screen properly.");
        if (merchantWithdrawVerification.verifyWithdrawScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (merchantWithdrawIndexPage.isBankAccountAvailable()) {

            merchantWithdrawVerification = merchantWithdrawIndexPage.clickOnDeleteButton();

            testVerifyLog("Verify validation message for the Delete Bank Account.");
            if (merchantWithdrawVerification.verifyValidationForDeleteBankAccount()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantWithdrawVerification = merchantWithdrawIndexPage.clickCancelAddBankAccountButton();

            merchantWithdrawVerification = merchantWithdrawIndexPage.clickOnDeleteButton();

            merchantWithdrawVerification = merchantWithdrawIndexPage.clickOKAddBankAccountButton();

            testVerifyLog("Verify confirmation message for the Bank Account deleted.");
            if (merchantWithdrawVerification.verifyConfirmationForDeletedBankAccount()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            testVerifyLog("Verify deleted account not display on screen.");
            if (merchantWithdrawVerification.verifyBankAccountDeletedSuccessfully()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

        } else {
            testWarningLog("Failed to delete bank account, Please add bank account to delete it.");
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void merchant_WithdrawMoney() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_083 :: To verify Merchant can withdraw money from the selected bank account.");
        merchantLoginIndexPage.getVersion();

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (merchantLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        MerchantDashboardIndexPage.getCurrentBalance();

        merchantLoginVerification = merchantLoginIndexPage.clickOnUserName();

        merchantWithdrawVerification = merchantWithdrawIndexPage.clickOnWithdrawMenu();

        testVerifyLog("Verify user can withdraw screen properly.");
        if (merchantWithdrawVerification.verifyWithdrawScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (merchantWithdrawIndexPage.isBankAccountAvailable()) {

            merchantWithdrawVerification = merchantWithdrawIndexPage.clickOnBankToWithdraw();

            testVerifyLog("Verify validation message for invalid withdraw amount.");
            if (merchantWithdrawVerification.verifyValidationForInvalidWithdrawAMount()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            if (!isPositiveExecution) {
                merchantWithdrawVerification = merchantWithdrawIndexPage.enterWithdrawAmount(0);

                merchantWithdrawVerification = merchantWithdrawIndexPage.clickOnBankToWithdraw();

                testVerifyLog("Verify validation message for invalid withdraw amount.");
                if (merchantWithdrawVerification.verifyValidationForInvalidWithdrawAMount()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantWithdrawVerification = merchantWithdrawIndexPage.enterWithdrawAmount(999999);

                merchantWithdrawVerification = merchantWithdrawIndexPage.clickOnBankToWithdraw();

                testVerifyLog("Verify validation message for invalid withdraw amount.");
                if (merchantWithdrawVerification.verifyValidationForInvalidWithdrawAMount()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            merchantWithdrawVerification = merchantWithdrawIndexPage.enterWithdrawAmount();

            merchantWithdrawVerification = merchantWithdrawIndexPage.clickOnBankToWithdraw();

            merchantWithdrawVerification = merchantWithdrawIndexPage.clickOnBackToWithdrawScreen();

            merchantWithdrawVerification = merchantWithdrawIndexPage.clickOnCancelWithdrawOK();

            merchantWithdrawVerification = merchantWithdrawIndexPage.enterWithdrawAmount();

            merchantWithdrawVerification = merchantWithdrawIndexPage.clickOnBankToWithdraw();

            testVerifyLog("Verify withdraw details in the screen.");
            if (merchantWithdrawVerification.verifyWithdrawDetailsScreen()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantWithdrawVerification = merchantWithdrawIndexPage.getWithdrawDetails();

            testVerifyLog("Verify total withdraw amount display properly.");
            if (merchantWithdrawVerification.verifyTotalWithdrawAmount()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantWithdrawVerification = merchantWithdrawIndexPage.clickOnConfirmToWithdraw();

            testVerifyLog("Verify withdraw details in confirmation window.");
            if (merchantWithdrawVerification.verifyConfirmationWithdrawDetails()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantWithdrawVerification = merchantWithdrawIndexPage.clickOnCancelWithdraw();

            merchantWithdrawVerification = merchantWithdrawIndexPage.clickOnConfirmToWithdraw();

            merchantWithdrawVerification = merchantWithdrawIndexPage.clickOnSendWithdraw();

            testVerifyLog("Verify withdraw details in completed withdraw window.");
            if (merchantWithdrawVerification.verifyCompletedWithdrawDetails()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantWithdrawVerification = merchantWithdrawIndexPage.clickOnBackToHome();

            testVerifyLog("Verify withdraw amount deducted from the balance.");
            if (merchantWithdrawVerification.verifyWithdrawDeductedFromBalance()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantDashboardVerification = merchantDashboardIndexPage.clickOnHomeMenu();

            testVerifyLog("Verify withdraw amount deducted from the balance.");
            if (merchantDashboardVerification.verifyBalanceUpdatedInHomeScreen()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }


        } else {
            testWarningLog("No Bank Accounts found for the Withdraw, please add bank accounts first to withdraw the money.");
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }
}
