package com.incashme.merchant.index;

import com.framework.init.SeleniumInit;
import org.testng.Assert;
import org.testng.annotations.Test;

public class MerchantInvoiceIndex extends SeleniumInit {

    @Test
    public void merchant_InvoiceSearchForTables() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_091 :: To verify Merchant can search the Invoice Details.");
        merchantLoginIndexPage.getVersion();

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (merchantLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnInvoiceMenu();

        if (merchantInvoiceIndexPage.isInvoiceTransactionDisplay()) {

            merchantInvoiceVerification = merchantInvoiceIndexPage.enterInvoiceSearchIDCriteria(false);

            testVerifyLog("Verify search details display properly.");
            if (merchantInvoiceVerification.verifySearchSuccessful()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            if (!isPositiveExecution) {
                merchantInvoiceVerification = merchantInvoiceIndexPage.enterInvoiceSearchIDCriteria(true);

                testVerifyLog("Verify validation message display for the invalid search criteria.");
                if (merchantInvoiceVerification.verifyValidationForInvalidSearch()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }
        }
        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void merchant_InvoiceDetailsFilter() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_092 :: To verify Merchant can filter the invoice transaction history.");
        merchantLoginIndexPage.getVersion();

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (merchantLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnInvoiceMenu();

        if (merchantInvoiceIndexPage.isInvoiceTransactionDisplay()) {

            merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnFilterIcon();

            testVerifyLog("Verify user can see the Filter screen.");
            if (merchantInvoiceVerification.verifyInvoiceFilterScreenDisplay()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantInvoiceVerification = merchantInvoiceIndexPage.changeFilterUserTo("Consumer");

            merchantInvoiceVerification = merchantInvoiceIndexPage.changeInvoiceTypeTo("Sent");

            merchantInvoiceVerification = merchantInvoiceIndexPage.clickConsumerOnFilterSearchButton();

            merchantInvoiceVerification = merchantInvoiceIndexPage.getConsumerDetailsForFilter();

            if (!isPositiveExecution) {
                merchantInvoiceVerification = merchantInvoiceIndexPage.filterFirstName(true);

                merchantInvoiceVerification = merchantInvoiceIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("User can see validation message for invalid filtered data.");
                if (merchantInvoiceVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            merchantInvoiceVerification = merchantInvoiceIndexPage.filterFirstName(false);

            merchantInvoiceVerification = merchantInvoiceIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify First Name filter successfully.");
            if (merchantInvoiceVerification.verifyFirstNameFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantInvoiceVerification = merchantInvoiceIndexPage.clearAllConsumerFilterDetails();

            if (!isPositiveExecution) {
                merchantInvoiceVerification = merchantInvoiceIndexPage.filterLastName(true);

                merchantInvoiceVerification = merchantInvoiceIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("User can see validation message for invalid filtered data.");
                if (merchantInvoiceVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            merchantInvoiceVerification = merchantInvoiceIndexPage.filterLastName(false);

            merchantInvoiceVerification = merchantInvoiceIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify Last Name filter successfully.");
            if (merchantInvoiceVerification.verifyLastNameFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantInvoiceVerification = merchantInvoiceIndexPage.clearAllConsumerFilterDetails();

            if (!isPositiveExecution) {
                merchantInvoiceVerification = merchantInvoiceIndexPage.filterConsumerNumber(true);

                merchantInvoiceVerification = merchantInvoiceIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("User can see validation message for invalid filtered data.");
                if (merchantInvoiceVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            merchantInvoiceVerification = merchantInvoiceIndexPage.filterConsumerNumber(false);

            merchantInvoiceVerification = merchantInvoiceIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify Mobile Number filter successfully.");
            if (merchantInvoiceVerification.verifyPhoneNumberFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantInvoiceVerification = merchantInvoiceIndexPage.clearAllConsumerFilterDetails();

            if (!isPositiveExecution) {
                merchantInvoiceVerification = merchantInvoiceIndexPage.filterInvoiceId(true);

                merchantInvoiceVerification = merchantInvoiceIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("User can see validation message for invalid filtered data.");
                if (merchantInvoiceVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            merchantInvoiceVerification = merchantInvoiceIndexPage.filterInvoiceId(false);

            merchantInvoiceVerification = merchantInvoiceIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify Invoice ID filter successfully.");
            if (merchantInvoiceVerification.verifyInvoiceIDFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantInvoiceVerification = merchantInvoiceIndexPage.clearAllConsumerFilterDetails();

            merchantInvoiceVerification = merchantInvoiceIndexPage.filterInvoiceStatus("Pending");

            merchantInvoiceVerification = merchantInvoiceIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify Invoice Type filter successfully.");
            if (merchantInvoiceVerification.verifyStatusFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantInvoiceVerification = merchantInvoiceIndexPage.clearAllConsumerFilterDetails();

            merchantInvoiceVerification = merchantInvoiceIndexPage.filterInvoiceStatus("Cancelled");

            merchantInvoiceVerification = merchantInvoiceIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify Invoice Type filter successfully.");
            if (merchantInvoiceVerification.verifyStatusFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantInvoiceVerification = merchantInvoiceIndexPage.clearAllConsumerFilterDetails();

            merchantInvoiceVerification = merchantInvoiceIndexPage.filterInvoiceStatus("Paid");

            merchantInvoiceVerification = merchantInvoiceIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify Invoice Type filter successfully.");
            if (merchantInvoiceVerification.verifyStatusFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantInvoiceVerification = merchantInvoiceIndexPage.clearAllConsumerFilterDetails();

            if (!isPositiveExecution) {
                merchantInvoiceVerification = merchantInvoiceIndexPage.enterInvoiceAmountRange(true);

                merchantInvoiceVerification = merchantInvoiceIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("Verify validation message for Max amount is less than Min amount.");
                if (merchantInvoiceVerification.verifyMaxMinAmountValidation()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            merchantInvoiceVerification = merchantInvoiceIndexPage.clearAllConsumerFilterDetails();

            merchantInvoiceVerification = merchantInvoiceIndexPage.enterInvoiceAmount("from");

            merchantInvoiceVerification = merchantInvoiceIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify user can filter by min amount.");
            if (merchantInvoiceVerification.verifyInvoiceAmountFromFieldFilter()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantInvoiceVerification = merchantInvoiceIndexPage.clearAllConsumerFilterDetails();

            merchantInvoiceVerification = merchantInvoiceIndexPage.enterInvoiceAmount("to");

            merchantInvoiceVerification = merchantInvoiceIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify user can filter by max amount.");
            if (merchantInvoiceVerification.verifyInvoiceAmountToFieldFilter()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantInvoiceVerification = merchantInvoiceIndexPage.clearAllConsumerFilterDetails();

            merchantInvoiceVerification = merchantInvoiceIndexPage.enterInvoiceAmountRange(false);

            merchantInvoiceVerification = merchantInvoiceIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify user can filter with From and To Amount.");
            if (merchantInvoiceVerification.verifyInvoiceAmountRangeFilter()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantInvoiceVerification = merchantInvoiceIndexPage.clearAllConsumerFilterDetails();

            merchantDashboardVerification = merchantDashboardIndexPage.clickOnHomeMenu();

            merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnInvoiceMenu();

            merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnFilterIcon();

            merchantInvoiceVerification = merchantInvoiceIndexPage.changeFilterUserTo("Merchant");

            merchantInvoiceVerification = merchantInvoiceIndexPage.changeInvoiceTypeTo("Sent");

            merchantInvoiceVerification = merchantInvoiceIndexPage.clickConsumerOnFilterSearchButton();

            merchantInvoiceVerification = merchantInvoiceIndexPage.getMerchantDetailsForFilter();

            if (!isPositiveExecution) {
                merchantInvoiceVerification = merchantInvoiceIndexPage.filterShopName(true);

                merchantInvoiceVerification = merchantInvoiceIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("User can see validation message for invalid filtered data.");
                if (merchantInvoiceVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            merchantInvoiceVerification = merchantInvoiceIndexPage.filterShopName(false);

            merchantInvoiceVerification = merchantInvoiceIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify Shop Name filter successfully.");
            if (merchantInvoiceVerification.verifyShopNameFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantInvoiceVerification = merchantInvoiceIndexPage.clearShopName();

            if (!isPositiveExecution) {
                merchantInvoiceVerification = merchantInvoiceIndexPage.filterMerchantID(true);

                merchantInvoiceVerification = merchantInvoiceIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("User can see validation message for invalid filtered data.");
                if (merchantInvoiceVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            merchantInvoiceVerification = merchantInvoiceIndexPage.filterMerchantID(false);

            merchantInvoiceVerification = merchantInvoiceIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify Merchant ID filter successfully.");
            if (merchantInvoiceVerification.verifyMerchantIDFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantInvoiceVerification = merchantInvoiceIndexPage.clearMerchantIDField();

            if (!isPositiveExecution) {
                merchantInvoiceVerification = merchantInvoiceIndexPage.filterInvoiceId(true);

                merchantInvoiceVerification = merchantInvoiceIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("User can see validation message for invalid filtered data.");
                if (merchantInvoiceVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            merchantInvoiceVerification = merchantInvoiceIndexPage.filterInvoiceId(false);

            merchantInvoiceVerification = merchantInvoiceIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify Transaction ID filter successfully.");
            if (merchantInvoiceVerification.verifyInvoiceIDFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantInvoiceVerification = merchantInvoiceIndexPage.clearInvoiceID();

            merchantInvoiceVerification = merchantInvoiceIndexPage.filterInvoiceStatus("Pending");

            merchantInvoiceVerification = merchantInvoiceIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify Invoice Type filter successfully.");
            if (merchantInvoiceVerification.verifyStatusFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantInvoiceVerification = merchantInvoiceIndexPage.filterInvoiceStatus("Cancelled");

            merchantInvoiceVerification = merchantInvoiceIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify Invoice Type filter successfully.");
            if (merchantInvoiceVerification.verifyStatusFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantInvoiceVerification = merchantInvoiceIndexPage.filterInvoiceStatus("Paid");

            merchantInvoiceVerification = merchantInvoiceIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify Invoice Type filter successfully.");
            if (merchantInvoiceVerification.verifyStatusFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantInvoiceVerification = merchantInvoiceIndexPage.clearInvoiceStatus();

            if (!isPositiveExecution) {
                merchantInvoiceVerification = merchantInvoiceIndexPage.enterInvoiceAmountRange(true);

                merchantInvoiceVerification = merchantInvoiceIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("Verify validation message for Max amount is less than Min amount.");
                if (merchantInvoiceVerification.verifyMaxMinAmountValidation()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            merchantInvoiceVerification = merchantInvoiceIndexPage.clearAmountRange();

            merchantInvoiceVerification = merchantInvoiceIndexPage.enterInvoiceAmount("from");

            merchantInvoiceVerification = merchantInvoiceIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify user can filter by min amount.");
            if (merchantInvoiceVerification.verifyInvoiceAmountFromFieldFilter()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantInvoiceVerification = merchantInvoiceIndexPage.clearAmountRange();

            merchantInvoiceVerification = merchantInvoiceIndexPage.enterInvoiceAmount("to");

            merchantInvoiceVerification = merchantInvoiceIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify user can filter by max amount.");
            if (merchantInvoiceVerification.verifyInvoiceAmountToFieldFilter()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantInvoiceVerification = merchantInvoiceIndexPage.clearAmountRange();

            merchantInvoiceVerification = merchantInvoiceIndexPage.enterInvoiceAmountRange(false);

            merchantInvoiceVerification = merchantInvoiceIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify user can filter with From and To Amount.");
            if (merchantInvoiceVerification.verifyInvoiceAmountRangeFilter()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantInvoiceVerification = merchantInvoiceIndexPage.clearAmountRange();

        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void merchant_InvoiceTypesFilter() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_093 :: To verify Merchant can filter the invoice types by users.");
        merchantLoginIndexPage.getVersion();

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (merchantLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnInvoiceMenu();

        if (merchantInvoiceIndexPage.isInvoiceTransactionDisplay()) {

            merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnFilterIcon();

            testVerifyLog("Verify user can see the Filter screen.");
            if (merchantInvoiceVerification.verifyInvoiceFilterScreenDisplay()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantInvoiceVerification = merchantInvoiceIndexPage.changeFilterUserTo("Consumer");

            merchantInvoiceVerification = merchantInvoiceIndexPage.changeInvoiceTypeTo("Sent");

            merchantInvoiceVerification = merchantInvoiceIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify invoice status for the Consumer Sent filter.");
            if (merchantInvoiceVerification.verifyFilteredInvoiceTypesForSent()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantInvoiceVerification = merchantInvoiceIndexPage.changeInvoiceTypeTo("Received");

            merchantInvoiceVerification = merchantInvoiceIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify invoice status for the Consumer Receive filter.");
            if (merchantInvoiceVerification.verifyFilteredInvoiceTypesForReceiveConsumer()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantInvoiceVerification = merchantInvoiceIndexPage.changeFilterUserTo("Merchant");

            merchantInvoiceVerification = merchantInvoiceIndexPage.changeInvoiceTypeTo("Sent");

            merchantInvoiceVerification = merchantInvoiceIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify invoice status for the Merchant Sent filter.");
            if (merchantInvoiceVerification.verifyFilteredInvoiceTypesForSent()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantInvoiceVerification = merchantInvoiceIndexPage.changeInvoiceTypeTo("Received");

            merchantInvoiceVerification = merchantInvoiceIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify invoice status for the Merchant Receive filter.");
            if (merchantInvoiceVerification.verifyFilteredInvoiceTypesForReceiveMerchant()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }
        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void merchant_InvoiceDetailsInTable() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_094 :: To verify Merchant can see the received and sent invoice in the table.");
        merchantLoginIndexPage.getVersion();

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (merchantLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnInvoiceMenu();

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnReceivedTab();

        testVerifyLog("Verify received invoice details display on the screen.");
        if (merchantInvoiceVerification.verifyReceivedInvoiceDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnSentTab();

        testVerifyLog("Verify Sent invoice details display on the screen.");
        if (merchantInvoiceVerification.verifySentInvoiceDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void merchant_CancelInvoiceFromSender() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_095 :: To verify Merchant can cancel a new created invoice.");
        merchantLoginIndexPage.getVersion();

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (merchantLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnInvoiceMenu();

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnCreateNewInvoice();

        testVerifyLog("Verify Create New Invoice Screen is display.");
        if (merchantInvoiceVerification.verifyCreateNewInvoiceScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantInvoiceVerification = merchantInvoiceIndexPage.enterMerchantMobileNumber();

        merchantInvoiceVerification = merchantInvoiceIndexPage.enterValidAmount();

        merchantInvoiceVerification = merchantInvoiceIndexPage.enterMessageForInvoice();

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnNextButton();

        testVerifyLog("Verify invoice details before submitting.");
        if (merchantInvoiceVerification.verifyInvoiceDetailsForMerchantBeforeSubmitInInvoice()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnSendButton();

        testVerifyLog("Verify invoice details after Invoice sent successfully.");
        if (merchantInvoiceVerification.verifyInvoiceDetailsForMerchantAfterSent()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnBackToHome();

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnSentTab();

        merchantInvoiceVerification = merchantInvoiceIndexPage.selectLatestCreatedInvoice();

        testVerifyLog("Verify invoice details in the Invoice screen.");
        if (merchantInvoiceVerification.verifyCreatedInvoiceDetailsForMerchant()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantInvoiceVerification = merchantInvoiceIndexPage.cancelCreatedInvoice();

        testVerifyLog("Verify invoice details in the delete invoice confirmation pop up.");
        if (merchantInvoiceVerification.verifyInvoiceDeleteConfirmationPopUp()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnCancelCancelCreatedInvoice();

        merchantInvoiceVerification = merchantInvoiceIndexPage.cancelCreatedInvoice();

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnConfirmCancelCreatedInvoice();

        testVerifyLog("Verify invoice details in the delete invoice completed pop up.");
        if (merchantInvoiceVerification.verifyInvoiceDeleteCompletedPopUp()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnBackToHomeAfterDeleteInvoice();

        merchantInvoiceVerification = merchantInvoiceIndexPage.searchDeletedInvoice();

        testVerifyLog("Verify deleted invoice not display in Invoice Transaction.");
        if (merchantInvoiceVerification.verifyValidationForInvalidSearch()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnFilterIcon();

        merchantInvoiceVerification = merchantInvoiceIndexPage.changeFilterUserTo("Merchant");

        merchantInvoiceVerification = merchantInvoiceIndexPage.changeInvoiceTypeTo("Sent");

        merchantInvoiceVerification = merchantInvoiceIndexPage.filterDeletedInvoiceId();

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickConsumerOnFilterSearchButton();

        testVerifyLog("Verify deleted invoice display as Cancelled.");
        if (merchantInvoiceVerification.verifyDeletedInvoiceCancelled()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void merchant_CreateAndPayInvoiceMerchant() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_096 :: To verify Merchant can create a new invoice for other merchant.");
        merchantLoginIndexPage.getVersion();

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (merchantLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantDashboardVerification = merchantDashboardIndexPage.openUserProfile();

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnInvoiceMenu();

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnCreateNewInvoice();

        testVerifyLog("Verify Create New Invoice Screen is display.");
        if (merchantInvoiceVerification.verifyCreateNewInvoiceScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnBackButton();

        testVerifyLog("Verify user can close the create new invoice screen.");
        if (merchantInvoiceVerification.verifyCreateInvoiceScreenIsClosed()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnCreateNewInvoice();

        if (!isPositiveExecution) {
            merchantInvoiceVerification = merchantInvoiceIndexPage.enterAlphabeticalUserNumber();

            testVerifyLog("Verify user can see validation message for the invalid mobile number.");
            if (merchantInvoiceVerification.verifyValidationMessageForInvalidMobileNumber()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantInvoiceVerification = merchantInvoiceIndexPage.enterNumberMoreThan10Digits();

            testVerifyLog("Verify user can see validation message for the invalid mobile number.");
            if (merchantInvoiceVerification.verifyValidationMessageForInvalidMobileNumber()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantInvoiceVerification = merchantInvoiceIndexPage.enterWrongMobileNumber();

            testVerifyLog("Verify user can see validation message for User not found.");
            if (merchantInvoiceVerification.verifyValidationForUserNotFound()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

      /*  merchantInvoiceVerification = merchantInvoiceIndexPage.enterOwnNumber();

        testVerifyLog("Verify user can see validation message when user enters own number for Invoice.");
        if (merchantInvoiceVerification.verifyValidationForUserNotFound()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }*/
        }

        merchantInvoiceVerification = merchantInvoiceIndexPage.enterMerchantMobileNumber();

        if (!isPositiveExecution) {
            merchantInvoiceVerification = merchantInvoiceIndexPage.enterMinAmount();

            testVerifyLog("Verify user can see validation message for amount less than minimum amount.");
            if (merchantInvoiceVerification.verifyInvalidAmountValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantInvoiceVerification = merchantInvoiceIndexPage.enterMaxAmount();

            testVerifyLog("Verify user can see validation message for amount more than maximum amount.");
            if (merchantInvoiceVerification.verifyInvalidAmountValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        merchantInvoiceVerification = merchantInvoiceIndexPage.enterValidAmount();

        merchantInvoiceVerification = merchantInvoiceIndexPage.enterMessageForInvoice();

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnNextButton();

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnCancelButton();

        testVerifyLog("Verify invoice confirmation pop up is closed.");
        if (merchantInvoiceVerification.verifyInvoicePopUpClosed()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnNextButton();

        testVerifyLog("Verify invoice details before submitting.");
        if (merchantInvoiceVerification.verifyInvoiceDetailsForMerchantBeforeSubmitInInvoice()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnSendButton();

        testVerifyLog("Verify invoice details after Invoice sent successfully.");
        if (merchantInvoiceVerification.verifyInvoiceDetailsForMerchantAfterSent()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnBackToHome();

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnSentTab();

        merchantInvoiceVerification = merchantInvoiceIndexPage.selectLatestCreatedInvoice();

        testVerifyLog("Verify invoice details in the Invoice screen.");
        if (merchantInvoiceVerification.verifyCreatedInvoiceDetailsForMerchant()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantDashboardVerification = merchantDashboardIndexPage.clickOnHomeMenu();
        merchantInvoiceVerification = merchantInvoiceIndexPage.getCurrentSenderBalance();

        merchantLoginVerification = merchantLoginIndexPage.clickOnLogout();

        merchantLoginVerification = merchantLoginIndexPage.loginAs(externalEmail, externalPassword);

        merchantInvoiceVerification = merchantInvoiceIndexPage.getCurrentReceiverBalance();

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnInvoiceMenu();

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnReceivedTab();

        merchantInvoiceVerification = merchantInvoiceIndexPage.selectLatestReceivedInvoice();

        testVerifyLog("Verify invoice details in the Invoice screen.");
        if (merchantInvoiceVerification.verifyReceivedMerchantInvoiceDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnPayButton();

        testVerifyLog("Verify invoice details in the Pay screen.");
        if (merchantInvoiceVerification.verifyPayInvoiceConfirmationDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }
        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnConfirmButton();

        testVerifyLog("Verify invoice details in the pay invoice completed pop up.");
        if (merchantInvoiceVerification.verifyInvoicePayCompletedPopUp()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnBackToHomePaid();

        testVerifyLog("Verify paid invoice not display in received section.");
        if (merchantInvoiceVerification.verifyInvoiceIDNotDisplayInReceived()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantInvoiceVerification = merchantInvoiceIndexPage.searchPaidInvoice();

        testVerifyLog("Verify invoice status changed to paid.");
        if (merchantInvoiceVerification.verifyPaidInvoiceStatus()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantDashboardVerification = merchantDashboardIndexPage.clickOnHomeMenu();

        testVerifyLog("Verify Merchant balance is deducted for the paid invoice amount.");
        if (merchantInvoiceVerification.verifyBalanceDeductedForPaidInvoice()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantLoginVerification = merchantLoginIndexPage.clickOnLogout();

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify Merchant balance is added for the received invoice amount.");
        if (merchantInvoiceVerification.verifyBalanceAddedForPaidInvoice()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnInvoiceMenu();

        merchantInvoiceVerification = merchantInvoiceIndexPage.searchPaidInvoice();

        testVerifyLog("Verify invoice status changed to paid.");
        if (merchantInvoiceVerification.verifyReceivedInvoiceStatus()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnSentTab();

        testVerifyLog("Verify paid invoice not display in sent section.");
        if (merchantInvoiceVerification.verifyInvoiceIDNotDisplayInSent()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void merchant_CreateAndPayInvoiceConsumer() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_097 :: To verify Merchant can create a new invoice for Consumer.");
        merchantLoginIndexPage.getVersion();

        openURL(driver, CONSUMER_URL);

        consumerLoginVerification = consumerLoginIndexPage.loginAs(externalEmail, externalPassword);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (consumerLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerDashboardVerification = consumerDashboardIndexPage.clickOnBalance();

        consumerRequestVerification = consumerRequestIndexPage.getRequestReceiverBalance();

        consumerLoginVerification = consumerLoginIndexPage.clickOnLogout();

        openURL(driver, MERCHANT_URL);

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (merchantLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantDashboardVerification = merchantDashboardIndexPage.openUserProfile();

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnInvoiceMenu();

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnCreateNewInvoice();

        testVerifyLog("Verify Create New Invoice Screen is display.");
        if (merchantInvoiceVerification.verifyCreateNewInvoiceScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantInvoiceVerification = merchantInvoiceIndexPage.enterConsumerMobileNumber();

        if (!isPositiveExecution) {
            merchantInvoiceVerification = merchantInvoiceIndexPage.enterMinAmount();

            testVerifyLog("Verify user can see validation message for amount less than minimum amount.");
            if (merchantInvoiceVerification.verifyInvalidAmountValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantInvoiceVerification = merchantInvoiceIndexPage.enterMaxAmount();

            testVerifyLog("Verify user can see validation message for amount more than maximum amount.");
            if (merchantInvoiceVerification.verifyInvalidAmountValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        merchantInvoiceVerification = merchantInvoiceIndexPage.enterValidAmount();

        merchantInvoiceVerification = merchantInvoiceIndexPage.enterMessageForInvoice();

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnNextButton();

        testVerifyLog("Verify invoice details before submitting.");
        if (merchantInvoiceVerification.verifyInvoiceDetailsForConsumerBeforeSubmitInInvoice()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnSendButton();

        testVerifyLog("Verify invoice details after Invoice sent successfully.");
        if (merchantInvoiceVerification.verifyInvoiceDetailsForConsumerAfterSent()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnBackToHome();

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnSentTab();

        merchantInvoiceVerification = merchantInvoiceIndexPage.selectLatestCreatedInvoice();

        testVerifyLog("Verify invoice details in the Invoice screen.");
        if (merchantInvoiceVerification.verifyCreatedInvoiceDetailsForConsumer()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantDashboardVerification = merchantDashboardIndexPage.clickOnHomeMenu();
        merchantInvoiceVerification = merchantInvoiceIndexPage.getCurrentSenderBalance();

        merchantLoginVerification = merchantLoginIndexPage.clickOnLogout();

        openURL(driver, CONSUMER_URL);

        consumerLoginVerification = consumerLoginIndexPage.loginAs(externalEmail, externalPassword);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (consumerLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerRequestVerification = consumerRequestIndexPage.clickOnRequests();

        testVerifyLog("Verify request screen display properly.");
        if (consumerRequestVerification.verifyRequestScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify recent invoice details in the card.");
        if (consumerRequestVerification.verifyInvoiceDetailsInCard()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerRequestVerification = consumerRequestIndexPage.openRecentRequest();

        testVerifyLog("Verify recent invoice details in the screen.");
        if (consumerRequestVerification.verifyInvoiceDetailsInScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerRequestVerification = consumerRequestIndexPage.clickOnPayButton();

        testVerifyLog("Verify invoice details in the Pay screen.");
        if (consumerRequestVerification.verifyPayInvoiceConfirmationDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerRequestVerification = consumerRequestIndexPage.clickOnConfirmButton();

        testVerifyLog("Verify invoice details in the pay invoice completed pop up.");
        if (consumerRequestVerification.verifyInvoicePayCompletedPopUp()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerRequestVerification = consumerRequestIndexPage.clickOnBackToHomePaid();

        testVerifyLog("Verify completed transaction display in the Dashboard Transaction screen.");
        if (consumerDashboardVerification.verifyInvoiceTransaction()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerDashboardVerification = consumerDashboardIndexPage.clickOnBalance();

        testVerifyLog("Verify Merchant balance is deducted for the paid invoice amount.");
        if (consumerDashboardVerification.verifyBalanceDeductedForPaidInvoice()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerLoginVerification = consumerLoginIndexPage.clickOnLogout();

        openURL(driver, MERCHANT_URL);

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify Merchant balance is added for the received invoice amount.");
        if (merchantInvoiceVerification.verifyBalanceAddedForPaidInvoice()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnInvoiceMenu();

        merchantInvoiceVerification = merchantInvoiceIndexPage.searchPaidInvoice();

        testVerifyLog("Verify invoice status changed to paid.");
        if (merchantInvoiceVerification.verifyReceivedInvoiceStatus()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnSentTab();

        testVerifyLog("Verify paid invoice not display in sent section.");
        if (merchantInvoiceVerification.verifyInvoiceIDNotDisplayInSent()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void merchant_CancelInvoiceFromReceiver() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_098 :: To verify Merchant can cancel received invoice.");
        merchantLoginIndexPage.getVersion();

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (merchantLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantDashboardVerification = merchantDashboardIndexPage.openUserProfile();

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnInvoiceMenu();

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnCreateNewInvoice();

        testVerifyLog("Verify Create New Invoice Screen is display.");
        if (merchantInvoiceVerification.verifyCreateNewInvoiceScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnBackButton();

        testVerifyLog("Verify user can close the create new invoice screen.");
        if (merchantInvoiceVerification.verifyCreateInvoiceScreenIsClosed()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnCreateNewInvoice();

        merchantInvoiceVerification = merchantInvoiceIndexPage.enterMerchantMobileNumber();

        merchantInvoiceVerification = merchantInvoiceIndexPage.enterValidAmount();

        merchantInvoiceVerification = merchantInvoiceIndexPage.enterMessageForInvoice();

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnNextButton();

        testVerifyLog("Verify invoice details before submitting.");
        if (merchantInvoiceVerification.verifyInvoiceDetailsForMerchantBeforeSubmitInInvoice()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnSendButton();

        testVerifyLog("Verify invoice details after Invoice sent successfully.");
        if (merchantInvoiceVerification.verifyInvoiceDetailsForMerchantAfterSent()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnBackToHome();

        merchantLoginVerification = merchantLoginIndexPage.clickOnLogout();

        merchantLoginVerification = merchantLoginIndexPage.loginAs(externalEmail, externalPassword);

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnInvoiceMenu();

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnReceivedTab();

        merchantInvoiceVerification = merchantInvoiceIndexPage.selectLatestReceivedInvoice();

        testVerifyLog("Verify invoice details in the Invoice screen.");
        if (merchantInvoiceVerification.verifyReceivedMerchantInvoiceDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantInvoiceVerification = merchantInvoiceIndexPage.cancelReceivedInvoice();

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnCancelCancelCreatedInvoice();

        merchantInvoiceVerification = merchantInvoiceIndexPage.cancelReceivedInvoice();

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnConfirmCancelCreatedInvoice();

        testVerifyLog("Verify invoice details in the delete invoice completed pop up.");
        if (merchantInvoiceVerification.verifyInvoiceDeleteByReceiver()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnBackToHomeAfterDeleteInvoice();

        merchantInvoiceVerification = merchantInvoiceIndexPage.searchDeletedInvoice();

        testVerifyLog("Verify deleted invoice not display in Invoice Transaction.");
        if (merchantInvoiceVerification.verifyValidationForInvalidSearch()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickOnFilterIcon();

        merchantInvoiceVerification = merchantInvoiceIndexPage.changeFilterUserTo("Merchant");

        merchantInvoiceVerification = merchantInvoiceIndexPage.changeInvoiceTypeTo("Received");

        merchantInvoiceVerification = merchantInvoiceIndexPage.filterDeletedInvoiceId();

        merchantInvoiceVerification = merchantInvoiceIndexPage.clickConsumerOnFilterSearchButton();

        testVerifyLog("Verify deleted invoice display as Cancelled.");
        if (merchantInvoiceVerification.verifyDeletedInvoiceCancelled()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }


        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }
}