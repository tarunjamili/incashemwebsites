package com.incashme.merchant.index;

import com.framework.init.SeleniumInit;
import com.incashme.merchant.indexpage.MerchantDonationIndexPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class MerchantDonationIndex extends SeleniumInit {

    @Test
    public void merchant_DonationSearchForTables() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_099 :: To verify Merchant can search the Donation Details.");
        merchantLoginIndexPage.getVersion();

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (merchantLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantDonationVerification = merchantDonationIndexPage.clickOnDonationMenu();

        if (merchantDonationIndexPage.isDonationTransactionDisplay()) {

            merchantDonationVerification = merchantDonationIndexPage.enterDonationSearchIDCriteria(false);

            testVerifyLog("Verify search details display properly.");
            if (merchantDonationVerification.verifySearchSuccessful()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            if (!isPositiveExecution) {
                merchantDonationVerification = merchantDonationIndexPage.enterDonationSearchIDCriteria(true);

                testVerifyLog("Verify validation message display for the invalid search criteria.");
                if (merchantDonationVerification.verifyValidationForInvalidSearch()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }
        }
        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void merchant_ProfitDonationDetailsFilter() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_100 :: To verify Profit Type Merchant can filter the donation transaction history.");
        merchantLoginIndexPage.getVersion();

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (merchantLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantDonationVerification = merchantDonationIndexPage.clickOnDonationMenu();

        if (!merchantDonationIndexPage.isNonProfitMerchant()) {

            merchantDonationVerification = merchantDonationIndexPage.clickOnFilterIcon();

            testVerifyLog("Verify user can see the Filter screen.");
            if (merchantDonationVerification.verifyDonationFilterScreenDisplay()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            testVerifyLog("Verify for Profit Merchant, filter display Merchant Received filter by default.");
            if (merchantDonationVerification.verifyProfitMerchantFilterType()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

            if (merchantDonationIndexPage.isMerchantFilterRecordsDisplay()) {

                merchantDonationVerification = merchantDonationIndexPage.getMerchantDetailsForFilter();

                if (!isPositiveExecution) {
                    merchantDonationVerification = merchantDonationIndexPage.filterShopName(true);

                    merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                    testVerifyLog("User can see validation message for invalid filtered data.");
                    if (merchantDonationVerification.verifyRandomDataFiltered()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }
                }

                merchantDonationVerification = merchantDonationIndexPage.filterShopName(false);

                merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("Verify Shop Name filter successfully.");
                if (merchantDonationVerification.verifyShopNameFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantDonationVerification = merchantDonationIndexPage.clearShopName();

                if (!isPositiveExecution) {
                    merchantDonationVerification = merchantDonationIndexPage.filterMerchantID(true);

                    merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                    testVerifyLog("User can see validation message for invalid filtered data.");
                    if (merchantDonationVerification.verifyRandomDataFiltered()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }
                }

                merchantDonationVerification = merchantDonationIndexPage.filterMerchantID(false);

                merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("Verify Merchant ID filter successfully.");
                if (merchantDonationVerification.verifyMerchantIDFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantDonationVerification = merchantDonationIndexPage.clearMerchantIDField();

                if (!isPositiveExecution) {
                    merchantDonationVerification = merchantDonationIndexPage.filterDonationId(true);

                    merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                    testVerifyLog("User can see validation message for invalid filtered data.");
                    if (merchantDonationVerification.verifyRandomDataFiltered()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }
                }

                merchantDonationVerification = merchantDonationIndexPage.filterDonationId(false);

                merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("Verify Donation ID filter successfully.");
                if (merchantDonationVerification.verifyDonationIDFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantDonationVerification = merchantDonationIndexPage.clearDonationID();

                merchantDonationVerification = merchantDonationIndexPage.filterDonationStatus("Pending");

                merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("Verify Donation Type filter successfully.");
                if (merchantDonationVerification.verifyStatusFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantDonationVerification = merchantDonationIndexPage.filterDonationStatus("Cancelled");

                merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("Verify Donation Type filter successfully.");
                if (merchantDonationVerification.verifyStatusFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantDonationVerification = merchantDonationIndexPage.filterDonationStatus("Donated");

                merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("Verify Donation Type filter successfully.");
                if (merchantDonationVerification.verifyStatusFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantDonationVerification = merchantDonationIndexPage.clearDonationStatus();

                if (!isPositiveExecution) {
                    merchantDonationVerification = merchantDonationIndexPage.enterDonationAmountRange(true);

                    merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                    testVerifyLog("Verify validation message for Max amount is less than Min amount.");
                    if (merchantDonationVerification.verifyMaxMinAmountValidation()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }
                }

                merchantDonationVerification = merchantDonationIndexPage.clearAmountRange();

                merchantDonationVerification = merchantDonationIndexPage.enterDonationAmount("from");

                merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("Verify user can filter by min amount.");
                if (merchantDonationVerification.verifyDonationAmountFromFieldFilter()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantDonationVerification = merchantDonationIndexPage.clearAmountRange();

                merchantDonationVerification = merchantDonationIndexPage.enterDonationAmount("to");

                merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("Verify user can filter by max amount.");
                if (merchantDonationVerification.verifyDonationAmountToFieldFilter()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantDonationVerification = merchantDonationIndexPage.clearAmountRange();

                merchantDonationVerification = merchantDonationIndexPage.enterDonationAmountRange(false);

                merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("Verify user can filter with From and To Amount.");
                if (merchantDonationVerification.verifyDonationAmountRangeFilter()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantDonationVerification = merchantDonationIndexPage.clearAmountRange();
            } else {
                testWarningLog("No Records available to run the test scenario, please add records for the filter" +
                        " and re run again.");
            }
        } else {
            testWarningLog("Looks like you selected a Non Profit Merchant type, please enter email id an password " +
                    "of Profit Merchant type and rerun the scenario again.");
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void merchant_NonProfitDonationDetailsFilter() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_101 :: To verify Non Profit Type Merchant can filter the donation transaction history.");
        merchantLoginIndexPage.getVersion();

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (merchantLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantDonationVerification = merchantDonationIndexPage.clickOnDonationMenu();

        if (merchantDonationIndexPage.isNonProfitMerchant()) {
            if (merchantDonationIndexPage.isDonationTransactionDisplay()) {

                merchantDonationVerification = merchantDonationIndexPage.clickOnFilterIcon();

                testVerifyLog("Verify user can see the Filter screen.");
                if (merchantDonationVerification.verifyDonationFilterScreenDisplay()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantDonationVerification = merchantDonationIndexPage.changeFilterUserTo("Consumer");

                merchantDonationVerification = merchantDonationIndexPage.changeDonationTypeTo("Sent");

                merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                if (merchantDonationIndexPage.isMerchantFilterRecordsDisplay()) {

                    merchantDonationVerification = merchantDonationIndexPage.getConsumerDetailsForFilter();

                    if (!isPositiveExecution) {
                        merchantDonationVerification = merchantDonationIndexPage.filterFirstName(true);

                        merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                        testVerifyLog("User can see validation message for invalid filtered data.");
                        if (merchantDonationVerification.verifyRandomDataFiltered()) {
                            stepPassed();
                        } else {
                            stepFailure(driver);
                            numOfFailedSteps++;
                        }
                    }

                    merchantDonationVerification = merchantDonationIndexPage.filterFirstName(false);

                    merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                    testVerifyLog("Verify First Name filter successfully.");
                    if (merchantDonationVerification.verifyFirstNameFiltered()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }

                    merchantDonationVerification = merchantDonationIndexPage.clearAllConsumerFilterDetails();

                    if (!isPositiveExecution) {
                        merchantDonationVerification = merchantDonationIndexPage.filterLastName(true);

                        merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                        testVerifyLog("User can see validation message for invalid filtered data.");
                        if (merchantDonationVerification.verifyRandomDataFiltered()) {
                            stepPassed();
                        } else {
                            stepFailure(driver);
                            numOfFailedSteps++;
                        }
                    }

                    merchantDonationVerification = merchantDonationIndexPage.filterLastName(false);

                    merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                    testVerifyLog("Verify Last Name filter successfully.");
                    if (merchantDonationVerification.verifyLastNameFiltered()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }

                    merchantDonationVerification = merchantDonationIndexPage.clearAllConsumerFilterDetails();

                    if (!isPositiveExecution) {
                        merchantDonationVerification = merchantDonationIndexPage.filterConsumerNumber(true);

                        merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                        testVerifyLog("User can see validation message for invalid filtered data.");
                        if (merchantDonationVerification.verifyRandomDataFiltered()) {
                            stepPassed();
                        } else {
                            stepFailure(driver);
                            numOfFailedSteps++;
                        }
                    }

                    merchantDonationVerification = merchantDonationIndexPage.filterConsumerNumber(false);

                    merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                    testVerifyLog("Verify Mobile Number filter successfully.");
                    if (merchantDonationVerification.verifyPhoneNumberFiltered()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }

                    merchantDonationVerification = merchantDonationIndexPage.clearAllConsumerFilterDetails();

                    if (!isPositiveExecution) {
                        merchantDonationVerification = merchantDonationIndexPage.filterDonationId(true);

                        merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                        testVerifyLog("User can see validation message for invalid filtered data.");
                        if (merchantDonationVerification.verifyRandomDataFiltered()) {
                            stepPassed();
                        } else {
                            stepFailure(driver);
                            numOfFailedSteps++;
                        }
                    }

                    merchantDonationVerification = merchantDonationIndexPage.filterDonationId(false);

                    merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                    testVerifyLog("Verify Donation ID filter successfully.");
                    if (merchantDonationVerification.verifyDonationIDFiltered()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }

                    merchantDonationVerification = merchantDonationIndexPage.clearAllConsumerFilterDetails();

                    merchantDonationVerification = merchantDonationIndexPage.filterDonationStatus("Pending");

                    merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                    testVerifyLog("Verify Donation Type filter successfully.");
                    if (merchantDonationVerification.verifyStatusFiltered()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }

                    merchantDonationVerification = merchantDonationIndexPage.clearAllConsumerFilterDetails();

                    merchantDonationVerification = merchantDonationIndexPage.filterDonationStatus("Cancelled");

                    merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                    testVerifyLog("Verify Donation Type filter successfully.");
                    if (merchantDonationVerification.verifyStatusFiltered()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }

                    merchantDonationVerification = merchantDonationIndexPage.clearAllConsumerFilterDetails();

                    merchantDonationVerification = merchantDonationIndexPage.filterDonationStatus("Donated");

                    merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                    testVerifyLog("Verify Donation Type filter successfully.");
                    if (merchantDonationVerification.verifyStatusFiltered()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }

                    merchantDonationVerification = merchantDonationIndexPage.clearAllConsumerFilterDetails();

                    if (!isPositiveExecution) {
                        merchantDonationVerification = merchantDonationIndexPage.enterDonationAmountRange(true);

                        merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                        testVerifyLog("Verify validation message for Max amount is less than Min amount.");
                        if (merchantDonationVerification.verifyMaxMinAmountValidation()) {
                            stepPassed();
                        } else {
                            stepFailure(driver);
                            numOfFailedSteps++;
                        }
                    }

                    merchantDonationVerification = merchantDonationIndexPage.clearAllConsumerFilterDetails();

                    merchantDonationVerification = merchantDonationIndexPage.enterDonationAmount("from");

                    merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                    testVerifyLog("Verify user can filter by min amount.");
                    if (merchantDonationVerification.verifyDonationAmountFromFieldFilter()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }

                    merchantDonationVerification = merchantDonationIndexPage.clearAllConsumerFilterDetails();

                    merchantDonationVerification = merchantDonationIndexPage.enterDonationAmount("to");

                    merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                    testVerifyLog("Verify user can filter by max amount.");
                    if (merchantDonationVerification.verifyDonationAmountToFieldFilter()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }

                    merchantDonationVerification = merchantDonationIndexPage.clearAllConsumerFilterDetails();

                    merchantDonationVerification = merchantDonationIndexPage.enterDonationAmountRange(false);

                    merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                    testVerifyLog("Verify user can filter with From and To Amount.");
                    if (merchantDonationVerification.verifyDonationAmountRangeFilter()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }

                    merchantDonationVerification = merchantDonationIndexPage.clearAllConsumerFilterDetails();
                } else {
                    testWarningLog("No Records available to run the test scenario, please add records for the filter" +
                            " and re run again.");
                }

                merchantDashboardVerification = merchantDashboardIndexPage.clickOnHomeMenu();

                merchantDonationVerification = merchantDonationIndexPage.clickOnDonationMenu();

                merchantDonationVerification = merchantDonationIndexPage.clickOnFilterIcon();

                merchantDonationVerification = merchantDonationIndexPage.changeFilterUserTo("Merchant");

                merchantDonationVerification = merchantDonationIndexPage.changeDonationTypeTo("Sent");

                merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                if (merchantDonationIndexPage.isMerchantFilterRecordsDisplay()) {
                    merchantDonationVerification = merchantDonationIndexPage.getMerchantDetailsForFilter();

                    if (!isPositiveExecution) {
                        merchantDonationVerification = merchantDonationIndexPage.filterShopName(true);

                        merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                        testVerifyLog("User can see validation message for invalid filtered data.");
                        if (merchantDonationVerification.verifyRandomDataFiltered()) {
                            stepPassed();
                        } else {
                            stepFailure(driver);
                            numOfFailedSteps++;
                        }
                    }

                    merchantDonationVerification = merchantDonationIndexPage.filterShopName(false);

                    merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                    testVerifyLog("Verify Shop Name filter successfully.");
                    if (merchantDonationVerification.verifyShopNameFiltered()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }

                    merchantDonationVerification = merchantDonationIndexPage.clearShopName();

                    if (!isPositiveExecution) {
                        merchantDonationVerification = merchantDonationIndexPage.filterMerchantID(true);

                        merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                        testVerifyLog("User can see validation message for invalid filtered data.");
                        if (merchantDonationVerification.verifyRandomDataFiltered()) {
                            stepPassed();
                        } else {
                            stepFailure(driver);
                            numOfFailedSteps++;
                        }
                    }

                    merchantDonationVerification = merchantDonationIndexPage.filterMerchantID(false);

                    merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                    testVerifyLog("Verify Merchant ID filter successfully.");
                    if (merchantDonationVerification.verifyMerchantIDFiltered()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }

                    merchantDonationVerification = merchantDonationIndexPage.clearMerchantIDField();

                    if (!isPositiveExecution) {
                        merchantDonationVerification = merchantDonationIndexPage.filterDonationId(true);

                        merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                        testVerifyLog("User can see validation message for invalid filtered data.");
                        if (merchantDonationVerification.verifyRandomDataFiltered()) {
                            stepPassed();
                        } else {
                            stepFailure(driver);
                            numOfFailedSteps++;
                        }
                    }

                    merchantDonationVerification = merchantDonationIndexPage.filterDonationId(false);

                    merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                    testVerifyLog("Verify Transaction ID filter successfully.");
                    if (merchantDonationVerification.verifyDonationIDFiltered()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }

                    merchantDonationVerification = merchantDonationIndexPage.clearDonationID();

                    merchantDonationVerification = merchantDonationIndexPage.filterDonationStatus("Pending");

                    merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                    testVerifyLog("Verify Donation Type filter successfully.");
                    if (merchantDonationVerification.verifyStatusFiltered()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }

                    merchantDonationVerification = merchantDonationIndexPage.filterDonationStatus("Cancelled");

                    merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                    testVerifyLog("Verify Donation Type filter successfully.");
                    if (merchantDonationVerification.verifyStatusFiltered()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }

                    merchantDonationVerification = merchantDonationIndexPage.filterDonationStatus("Donated");

                    merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                    testVerifyLog("Verify Donation Type filter successfully.");
                    if (merchantDonationVerification.verifyStatusFiltered()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }

                    merchantDonationVerification = merchantDonationIndexPage.clearDonationStatus();

                    if (!isPositiveExecution) {
                        merchantDonationVerification = merchantDonationIndexPage.enterDonationAmountRange(true);

                        merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                        testVerifyLog("Verify validation message for Max amount is less than Min amount.");
                        if (merchantDonationVerification.verifyMaxMinAmountValidation()) {
                            stepPassed();
                        } else {
                            stepFailure(driver);
                            numOfFailedSteps++;
                        }
                    }

                    merchantDonationVerification = merchantDonationIndexPage.clearAmountRange();

                    merchantDonationVerification = merchantDonationIndexPage.enterDonationAmount("from");

                    merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                    testVerifyLog("Verify user can filter by min amount.");
                    if (merchantDonationVerification.verifyDonationAmountFromFieldFilter()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }

                    merchantDonationVerification = merchantDonationIndexPage.clearAmountRange();

                    merchantDonationVerification = merchantDonationIndexPage.enterDonationAmount("to");

                    merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                    testVerifyLog("Verify user can filter by max amount.");
                    if (merchantDonationVerification.verifyDonationAmountToFieldFilter()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }

                    merchantDonationVerification = merchantDonationIndexPage.clearAmountRange();

                    merchantDonationVerification = merchantDonationIndexPage.enterDonationAmountRange(false);

                    merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                    testVerifyLog("Verify user can filter with From and To Amount.");
                    if (merchantDonationVerification.verifyDonationAmountRangeFilter()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }

                    merchantDonationVerification = merchantDonationIndexPage.clearAmountRange();
                } else {
                    testWarningLog("No Records available to run the test scenario, please add records for the filter" +
                            " and re run again.");
                }
            }
        } else {
            testWarningLog("Looks like you selected a Profit Merchant type, please enter email id an password " +
                    "of Non Profit Merchant type and rerun the scenario again.");
        }
        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void merchant_DonationTypesFilter() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_102 :: To verify Merchant can filter the donation types by users.");
        merchantLoginIndexPage.getVersion();

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (merchantLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantDonationVerification = merchantDonationIndexPage.clickOnDonationMenu();
        if (merchantDonationIndexPage.isNonProfitMerchant()) {
            if (merchantDonationIndexPage.isDonationTransactionDisplay()) {
                merchantDonationVerification = merchantDonationIndexPage.clickOnFilterIcon();

                testVerifyLog("Verify user can see the Filter screen.");
                if (merchantDonationVerification.verifyDonationFilterScreenDisplay()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantDonationVerification = merchantDonationIndexPage.changeFilterUserTo("Consumer");

                merchantDonationVerification = merchantDonationIndexPage.changeDonationTypeTo("Sent");

                merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("Verify donation status for the Consumer Sent filter.");
                if (merchantDonationVerification.verifyFilteredDonationTypesForSent()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantDonationVerification = merchantDonationIndexPage.changeDonationTypeTo("Requested");

                merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("Verify donation status for the Consumer Requested filter.");
                if (merchantDonationVerification.verifyFilteredDonationTypesForRequestedConsumer()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantDonationVerification = merchantDonationIndexPage.changeFilterUserTo("Merchant");

                merchantDonationVerification = merchantDonationIndexPage.changeDonationTypeTo("Sent");

                merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("Verify donation status for the Merchant Sent filter.");
                if (merchantDonationVerification.verifyFilteredDonationTypesForSent()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantDonationVerification = merchantDonationIndexPage.changeDonationTypeTo("Requested");

                merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("Verify donation status for the Merchant Requested filter.");
                if (merchantDonationVerification.verifyFilteredDonationTypesForRequestedConsumer()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }
        } else {
            testWarningLog("Looks like you selected a Profit Merchant type, please enter email id an password " +
                    "of Non Profit Merchant type and rerun the scenario again.");
        }
        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void merchant_DonationDetailsInTable() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_103 :: To verify Merchant can see the requested and sent donation receipt in the table.");
        merchantLoginIndexPage.getVersion();

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (merchantLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantDonationVerification = merchantDonationIndexPage.clickOnDonationMenu();

        merchantDonationVerification = merchantDonationIndexPage.clickOnRequestedTab();

        testVerifyLog("Verify requested donation receipt details display on the screen.");
        if (merchantDonationVerification.verifyRequestedDonationDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (merchantDonationIndexPage.isNonProfitMerchant()) {
            merchantDonationVerification = merchantDonationIndexPage.clickOnSentTab();

            testVerifyLog("Verify Sent donation receipt details display on the screen.");
            if (merchantDonationVerification.verifySentDonationDetails()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        } else {
            testWarningLog("Looks like you selected a Profit Merchant type, please enter email id an password " +
                    "of Non Profit Merchant type and rerun the scenario again for checking sent type donations.");
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void merchant_CreateAndPayDonationMerchant() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_104 :: To verify Non Profit Merchant can create a donation for other merchants.");
        merchantLoginIndexPage.getVersion();

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (merchantLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantDonationVerification = merchantDonationIndexPage.clickOnDonationMenu();

        if (merchantDonationIndexPage.isNonProfitMerchant()) {

            merchantDonationVerification = merchantDonationIndexPage.clickOnCreateNewDonation();

            testVerifyLog("Verify Create New Donation Screen is display.");
            if (merchantDonationVerification.verifyCreateNewDonationScreen()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantDonationVerification = merchantDonationIndexPage.clickOnBackButton();

            testVerifyLog("Verify user can close the create new donation screen.");
            if (merchantDonationVerification.verifyCreateDonationScreenIsClosed()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantDonationVerification = merchantDonationIndexPage.getReceiverDonationAmounts();

            merchantDonationVerification = merchantDonationIndexPage.clickOnCreateNewDonation();

            if (!isPositiveExecution) {
                merchantDonationVerification = merchantDonationIndexPage.enterAlphabeticalUserNumber();

                testVerifyLog("Verify user can see validation message for the invalid mobile number.");
                if (merchantDonationVerification.verifyValidationMessageForInvalidMobileNumber()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantDonationVerification = merchantDonationIndexPage.enterNumberMoreThan10Digits();

                testVerifyLog("Verify user can see validation message for the invalid mobile number.");
                if (merchantDonationVerification.verifyValidationMessageForInvalidMobileNumber()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantDonationVerification = merchantDonationIndexPage.enterWrongMobileNumber();

                testVerifyLog("Verify user can see validation message for User not found.");
                if (merchantDonationVerification.verifyValidationForUserNotFound()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

               /* merchantInvoiceVerification = merchantInvoiceIndexPage.enterOwnNumber();

                testVerifyLog("Verify user can see validation message when user enters own number for Invoice.");
                if (merchantInvoiceVerification.verifyValidationForUserNotFound()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }*/
            }

            merchantDonationVerification = merchantDonationIndexPage.enterMerchantMobileNumber();

            merchantDonationVerification = merchantDonationIndexPage.enterMessageForDonation();

            merchantDonationVerification = merchantDonationIndexPage.clickOnSendButton();

            merchantDonationVerification = merchantDonationIndexPage.clickOnCancelButton();

            testVerifyLog("Verify donation confirmation pop up is closed.");
            if (merchantDonationVerification.verifyDonationPopUpClosed()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantDonationVerification = merchantDonationIndexPage.clickOnSendButton();

            testVerifyLog("Verify donation details before submitting.");
            if (merchantDonationVerification.verifyDonationDetailsForMerchantBeforeSubmitDonation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantDonationVerification = merchantDonationIndexPage.clickOnConfirmButton();

            testVerifyLog("Verify donation details after Donation sent successfully.");
            if (merchantDonationVerification.verifyDonationDetailsForMerchantAfterSent()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantDonationVerification = merchantDonationIndexPage.clickOnBackToHome();

            merchantDonationVerification = merchantDonationIndexPage.clickOnSentTab();

            merchantDonationVerification = merchantDonationIndexPage.selectLatestSentDonation();

            testVerifyLog("Verify donation details in the Donation screen.");
            if (merchantDonationVerification.verifyCreatedDonationDetailsForMerchant()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantDashboardVerification = merchantDashboardIndexPage.clickOnHomeMenu();
            merchantDonationVerification = merchantDonationIndexPage.getCurrentRequesterBalance();

            merchantLoginVerification = merchantLoginIndexPage.clickOnLogout();

            merchantLoginVerification = merchantLoginIndexPage.loginAs(externalEmail, externalPassword);

            merchantDonationVerification = merchantDonationIndexPage.getCurrentSenderBalance();

            if (MerchantDonationIndexPage._donationSenderBalance > 10) {

                merchantDonationVerification = merchantDonationIndexPage.clickOnDonationMenu();

                merchantDonationVerification = merchantDonationIndexPage.getSenderDonationAmounts();

                merchantDonationVerification = merchantDonationIndexPage.clickOnRequestedTab();

                merchantDonationVerification = merchantDonationIndexPage.selectLatestRequestedDonation();

                testVerifyLog("Verify donation details in the Donation screen.");
                if (merchantDonationVerification.verifyReceivedMerchantDonationDetails()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                if (!isPositiveExecution) {
                    merchantDonationVerification = merchantDonationIndexPage.enterMinAmount();

                    testVerifyLog("Verify user can see validation message for amount less than minimum amount.");
                    if (merchantDonationVerification.verifyInvalidAmountValidation()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }

                    merchantDonationVerification = merchantDonationIndexPage.enterMaxAmount();

                    testVerifyLog("Verify user can see validation message for amount more than maximum amount.");
                    if (merchantDonationVerification.verifyInvalidAmountValidation()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }

                    merchantDonationVerification = merchantDonationIndexPage.enterAmountMoreThanFunds();

                    merchantDonationVerification = merchantDonationIndexPage.clickOnPayButton();

                    merchantDonationVerification = merchantDonationIndexPage.clickOnConfirmPayButton();

                    testVerifyLog("Verify user can see validation message for amount more than available funds.");
                    if (merchantDonationVerification.verifyInsufficientFundMessage()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }

                    merchantDonationVerification = merchantDonationIndexPage.clickOnCancelButton();
                }

                merchantDonationVerification = merchantDonationIndexPage.enterValidAmount();

                merchantDonationVerification = merchantDonationIndexPage.clickOnPayButton();

                testVerifyLog("Verify donation details in the Pay screen.");
                if (merchantDonationVerification.verifyPayDonationConfirmationDetails()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
                merchantDonationVerification = merchantDonationIndexPage.clickOnConfirmPayButton();

                testVerifyLog("Verify donation details in the pay donation completed pop up.");
                if (merchantDonationVerification.verifyDonationPayCompletedPopUp()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantDonationVerification = merchantDonationIndexPage.clickOnBackToHomePaid();

                testVerifyLog("Verify paid donation not display in requested section.");
                if (merchantDonationVerification.verifyDonationIDNotDisplayInRequested()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantDonationVerification = merchantDonationIndexPage.searchPaidDonation();

                testVerifyLog("Verify donation status changed to donated.");
                if (merchantDonationVerification.verifyDonatedDonationStatus()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                testVerifyLog("Verify donated amount updated after donation paid.");
                if (merchantDonationVerification.verifyDonationAmountAddedInDonatedChartForPaid()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantDashboardVerification = merchantDashboardIndexPage.clickOnHomeMenu();

                testVerifyLog("Verify Merchant balance is deducted for the paid donation amount.");
                if (merchantDonationVerification.verifyBalanceDeductedForPaidDonation()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantLoginVerification = merchantLoginIndexPage.clickOnLogout();

                merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

                testVerifyLog("Verify Merchant balance is added for the received donation amount.");
                if (merchantDonationVerification.verifyBalanceAddedForPaidDonation()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantDonationVerification = merchantDonationIndexPage.clickOnDonationMenu();

                testVerifyLog("Verify donation received amount updated after donation received.");
                if (merchantDonationVerification.verifyDonationAmountAddedInDonatedChartForReceived()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantDonationVerification = merchantDonationIndexPage.searchPaidDonation();

                testVerifyLog("Verify donation status changed to received.");
                if (merchantDonationVerification.verifyReceivedDonationStatus()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantDonationVerification = merchantDonationIndexPage.clickOnSentTab();

                testVerifyLog("Verify received donation not display in sent section.");
                if (merchantDonationVerification.verifyDonationIDNotDisplayInSent()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            } else {
                testWarningLog("Merchant should have balance more than 500 INR for executing this scenario, please " +
                        "add balance in " + externalEmail + " account to run this scenario.");
            }
        } else {
            testWarningLog("Looks like you selected a Profit Merchant type, please enter email id an password " +
                    "of Non Profit Merchant type and rerun the scenario again for checking sent type donations.");
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void merchant_CancelDonationFromSender() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_105 :: To verify Non Profit Merchant can create a donation and cancel the donation invoice.");
        merchantLoginIndexPage.getVersion();

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (merchantLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantDonationVerification = merchantDonationIndexPage.clickOnDonationMenu();

        if (merchantDonationIndexPage.isNonProfitMerchant()) {

            merchantDonationVerification = merchantDonationIndexPage.clickOnCreateNewDonation();

            testVerifyLog("Verify Create New Donation Screen is display.");
            if (merchantDonationVerification.verifyCreateNewDonationScreen()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantDonationVerification = merchantDonationIndexPage.enterMerchantMobileNumber();

            merchantDonationVerification = merchantDonationIndexPage.enterMessageForDonation();

            merchantDonationVerification = merchantDonationIndexPage.clickOnSendButton();

            testVerifyLog("Verify donation details before submitting.");
            if (merchantDonationVerification.verifyDonationDetailsForMerchantBeforeSubmitDonation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantDonationVerification = merchantDonationIndexPage.clickOnConfirmButton();

            testVerifyLog("Verify donation details after Donation sent successfully.");
            if (merchantDonationVerification.verifyDonationDetailsForMerchantAfterSent()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantDonationVerification = merchantDonationIndexPage.clickOnBackToHome();

            merchantDonationVerification = merchantDonationIndexPage.clickOnSentTab();

            merchantDonationVerification = merchantDonationIndexPage.selectLatestSentDonation();

            testVerifyLog("Verify donation details in the Donation screen.");
            if (merchantDonationVerification.verifyCreatedDonationDetailsForMerchant()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantDonationVerification = merchantDonationIndexPage.clickOnCancelDonationButton();

            merchantDonationVerification = merchantDonationIndexPage.clickOnConfirmButton();

            testVerifyLog("Verify donation details after Donation cancelled successfully.");
            if (merchantDonationVerification.verifyDonationDetailsForMerchantAfterCancelledFromReceiver()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantDonationVerification = merchantDonationIndexPage.clickOnBackToHomeDeleted();

            merchantDonationVerification = merchantDonationIndexPage.searchDeletedDonation();

            testVerifyLog("Verify deleted donation not display in Donation Transaction.");
            if (merchantDonationVerification.verifyValidationForInvalidSearch()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantDonationVerification = merchantDonationIndexPage.clickOnFilterIcon();

            merchantDonationVerification = merchantDonationIndexPage.changeFilterUserTo("Merchant");

            merchantDonationVerification = merchantDonationIndexPage.changeDonationTypeTo("Sent");

            merchantDonationVerification = merchantDonationIndexPage.filterDeletedDonationId();

            merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify deleted donation display as Cancelled.");
            if (merchantDonationVerification.verifyDeletedDonationCancelled()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

        } else {
            testWarningLog("Looks like you selected a Profit Merchant type, please enter email id an password " +
                    "of Non Profit Merchant type and rerun the scenario again for checking sent type donations.");
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void merchant_CancelDonationFromReceiver() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_106 :: To verify Non Profit Merchant can create a donation and receiver can cancel it.");
        merchantLoginIndexPage.getVersion();

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (merchantLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantDonationVerification = merchantDonationIndexPage.clickOnDonationMenu();

        if (merchantDonationIndexPage.isNonProfitMerchant()) {

            merchantDonationVerification = merchantDonationIndexPage.clickOnCreateNewDonation();

            testVerifyLog("Verify Create New Donation Screen is display.");
            if (merchantDonationVerification.verifyCreateNewDonationScreen()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantDonationVerification = merchantDonationIndexPage.enterMerchantMobileNumber();

            merchantDonationVerification = merchantDonationIndexPage.enterMessageForDonation();

            merchantDonationVerification = merchantDonationIndexPage.clickOnSendButton();

            testVerifyLog("Verify donation details before submitting.");
            if (merchantDonationVerification.verifyDonationDetailsForMerchantBeforeSubmitDonation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantDonationVerification = merchantDonationIndexPage.clickOnConfirmButton();

            testVerifyLog("Verify donation details after Donation sent successfully.");
            if (merchantDonationVerification.verifyDonationDetailsForMerchantAfterSent()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantDonationVerification = merchantDonationIndexPage.clickOnBackToHome();

            merchantLoginVerification = merchantLoginIndexPage.clickOnLogout();

            merchantLoginVerification = merchantLoginIndexPage.loginAs(externalEmail, externalPassword);

            merchantDonationVerification = merchantDonationIndexPage.clickOnDonationMenu();

            merchantDonationVerification = merchantDonationIndexPage.clickOnRequestedTab();

            merchantDonationVerification = merchantDonationIndexPage.selectLatestRequestedDonation();

            testVerifyLog("Verify donation details in the Donation screen.");
            if (merchantDonationVerification.verifyReceivedMerchantDonationDetails()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantDonationVerification = merchantDonationIndexPage.clickOnCancelDonationButton();

            merchantDonationVerification = merchantDonationIndexPage.clickOnConfirmButton();

            testVerifyLog("Verify donation details in the delete donation pop up.");
            if (merchantDonationVerification.verifyDonationDetailsForMerchantAfterCancelledFromSender()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantDonationVerification = merchantDonationIndexPage.clickOnBackToHomeDeleted();

            merchantDonationVerification = merchantDonationIndexPage.searchDeletedDonation();

            testVerifyLog("Verify deleted donation not display in Donation Transaction.");
            if (merchantDonationVerification.verifyValidationForInvalidSearch()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantDonationVerification = merchantDonationIndexPage.clickOnFilterIcon();

            merchantDonationVerification = merchantDonationIndexPage.changeFilterUserTo("Merchant");

            merchantDonationVerification = merchantDonationIndexPage.changeDonationTypeTo("Received");

            merchantDonationVerification = merchantDonationIndexPage.filterDeletedDonationId();

            merchantDonationVerification = merchantDonationIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify deleted donation display as Cancelled.");
            if (merchantDonationVerification.verifyDeletedDonationCancelled()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        } else {
            testWarningLog("Looks like you selected a Profit Merchant type, please enter email id an password " +
                    "of Non Profit Merchant type and rerun the scenario again for checking sent type donations.");
        }
        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void merchant_CreateAndPayDonationConsumer() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_107 :: To verify Non Profit Merchant can create a donation for consumers.");
        merchantLoginIndexPage.getVersion();

        openURL(driver, CONSUMER_URL);

        consumerLoginVerification = consumerLoginIndexPage.loginAs(externalEmail, externalPassword);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (consumerLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        consumerDashboardVerification = consumerDashboardIndexPage.clickOnBalance();

        consumerRequestVerification = consumerRequestIndexPage.getRequestReceiverBalance();

        consumerLoginVerification = consumerLoginIndexPage.clickOnLogout();

        openURL(driver, MERCHANT_URL);

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (merchantLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantDonationVerification = merchantDonationIndexPage.clickOnDonationMenu();

        if (merchantDonationIndexPage.isNonProfitMerchant()) {

            merchantDonationVerification = merchantDonationIndexPage.clickOnCreateNewDonation();

            testVerifyLog("Verify Create New Donation Screen is display.");
            if (merchantDonationVerification.verifyCreateNewDonationScreen()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantDonationVerification = merchantDonationIndexPage.clickOnBackButton();

            testVerifyLog("Verify user can close the create new donation screen.");
            if (merchantDonationVerification.verifyCreateDonationScreenIsClosed()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantDonationVerification = merchantDonationIndexPage.getReceiverDonationAmounts();

            merchantDonationVerification = merchantDonationIndexPage.clickOnCreateNewDonation();

            if (!isPositiveExecution) {
                merchantDonationVerification = merchantDonationIndexPage.enterAlphabeticalUserNumber();

                testVerifyLog("Verify user can see validation message for the invalid mobile number.");
                if (merchantDonationVerification.verifyValidationMessageForInvalidMobileNumber()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantDonationVerification = merchantDonationIndexPage.enterNumberMoreThan10Digits();

                testVerifyLog("Verify user can see validation message for the invalid mobile number.");
                if (merchantDonationVerification.verifyValidationMessageForInvalidMobileNumber()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantDonationVerification = merchantDonationIndexPage.enterWrongMobileNumber();

                testVerifyLog("Verify user can see validation message for User not found.");
                if (merchantDonationVerification.verifyValidationForUserNotFound()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

      /*  merchantInvoiceVerification = merchantInvoiceIndexPage.enterOwnNumber();

        testVerifyLog("Verify user can see validation message when user enters own number for Invoice.");
        if (merchantInvoiceVerification.verifyValidationForUserNotFound()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }*/
            }

            merchantDonationVerification = merchantDonationIndexPage.enterConsumerMobileNumber();

            merchantDonationVerification = merchantDonationIndexPage.enterMessageForDonation();

            merchantDonationVerification = merchantDonationIndexPage.clickOnSendButton();

            merchantDonationVerification = merchantDonationIndexPage.clickOnCancelButton();

            testVerifyLog("Verify donation confirmation pop up is closed.");
            if (merchantDonationVerification.verifyDonationPopUpClosed()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantDonationVerification = merchantDonationIndexPage.clickOnSendButton();

            testVerifyLog("Verify donation details before submitting.");
            if (merchantDonationVerification.verifyDonationDetailsForConsumerBeforeSubmitInInvoice()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantDonationVerification = merchantDonationIndexPage.clickOnConfirmButton();

            testVerifyLog("Verify donation details after Donation sent successfully.");
            if (merchantDonationVerification.verifyDonationDetailsForConsumerAfterSent()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantDonationVerification = merchantDonationIndexPage.clickOnBackToHome();

            merchantDonationVerification = merchantDonationIndexPage.clickOnSentTab();

            merchantDonationVerification = merchantDonationIndexPage.selectLatestSentDonation();

            testVerifyLog("Verify donation details in the Donation screen.");
            if (merchantDonationVerification.verifyCreatedDonationDetailsForConsumer()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantDashboardVerification = merchantDashboardIndexPage.clickOnHomeMenu();

            merchantDonationVerification = merchantDonationIndexPage.getCurrentRequesterBalance();

            merchantLoginVerification = merchantLoginIndexPage.clickOnLogout();

            openURL(driver, CONSUMER_URL);

            consumerLoginVerification = consumerLoginIndexPage.loginAs(externalEmail, externalPassword);

            testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
            if (consumerLoginVerification.verifyUserLoginSuccessfully()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerRequestVerification = consumerRequestIndexPage.clickOnRequests();

            testVerifyLog("Verify request screen display properly.");
            if (consumerRequestVerification.verifyRequestScreen()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            testVerifyLog("Verify recent donation details in the card.");
            if (consumerRequestVerification.verifyDonationDetailsInCard()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerRequestVerification = consumerRequestIndexPage.openRecentRequest();

            testVerifyLog("Verify recent donation details in the screen.");
            if (consumerRequestVerification.verifyDonationDetailsInScreen()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            if (!isPositiveExecution) {
                consumerRequestVerification = consumerRequestIndexPage.enterMinAmount();

                testVerifyLog("Verify user can't pay the min donation amount.");
                if (consumerRequestVerification.verifyInvalidAmountValidation()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                consumerRequestVerification = consumerRequestIndexPage.enterMaxAmount();

                testVerifyLog("Verify user can't pay the existed donation amount.");
                if (consumerRequestVerification.verifyInvalidAmountValidation()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            if (consumerRequestIndexPage.isBalanceLessThanAccepted()) {

                if (!isPositiveExecution) {
                    consumerRequestVerification = consumerRequestIndexPage.enterAmountMoreThanFunds();

                    consumerRequestVerification = consumerRequestIndexPage.clickOnDonateButton();

                    consumerRequestVerification = consumerRequestIndexPage.clickOnConfirmButton();

                    testVerifyLog("Verify user can see validation message for amount more than available funds.");
                    if (merchantDonationVerification.verifyInsufficientFundMessage()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }

                    consumerRequestVerification = consumerRequestIndexPage.clickOnCancelDonationButton();
                }
            }

            consumerRequestVerification = consumerRequestIndexPage.enterValidAmount();

            consumerRequestVerification = consumerRequestIndexPage.clickOnDonateButton();

            testVerifyLog("Verify donation details in the Pay screen.");
            if (consumerRequestVerification.verifyPayDonationConfirmationDetails()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
            consumerRequestVerification = consumerRequestIndexPage.clickOnConfirmButton();

            testVerifyLog("Verify donation details in the pay donation completed pop up.");
            if (consumerRequestVerification.verifyDonationPayCompletedPopUp()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerRequestVerification = consumerRequestIndexPage.clickOnBackToHomePaid();

            testVerifyLog("Verify completed transaction display in the Dashboard Transaction screen.");
            if (consumerDashboardVerification.verifyDonationTransaction()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerDashboardVerification = consumerDashboardIndexPage.clickOnBalance();

            testVerifyLog("Verify Merchant balance is deducted for the paid donation amount.");
            if (consumerDashboardVerification.verifyBalanceDeductedForPaidDonation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            consumerLoginVerification = consumerLoginIndexPage.clickOnLogout();

            openURL(driver, MERCHANT_URL);

            merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

            testVerifyLog("Verify Merchant balance is added for the received donation amount.");
            if (merchantDonationVerification.verifyBalanceAddedForPaidDonation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantDonationVerification = merchantDonationIndexPage.clickOnDonationMenu();

            testVerifyLog("Verify donation received amount updated after donation received.");
            if (merchantDonationVerification.verifyDonationAmountAddedInDonatedChartForReceived()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantDonationVerification = merchantDonationIndexPage.searchPaidDonation();

            testVerifyLog("Verify donation status changed to received.");
            if (merchantDonationVerification.verifyReceivedDonationStatus()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantDonationVerification = merchantDonationIndexPage.clickOnSentTab();

            testVerifyLog("Verify received donation not display in sent section.");
            if (merchantDonationVerification.verifyDonationIDNotDisplayInSent()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

        } else {
            testWarningLog("Looks like you selected a Profit Merchant type, please enter email id an password " +
                    "of Non Profit Merchant type and rerun the scenario again for checking sent type donations.");
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

}