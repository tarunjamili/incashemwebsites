package com.incashme.merchant.index;

import com.framework.init.SeleniumInit;
import org.testng.Assert;
import org.testng.annotations.Test;

public class MerchantStatementIndex extends SeleniumInit {

    @Test
    public void merchant_StatementDownload() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_078 :: To verify Merchant can download the Monthly Statement.");
        merchantLoginIndexPage.getVersion();

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (merchantLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        createDownloadDirectory();

        merchantDashboardVerification = merchantDashboardIndexPage.openUserProfile();

        merchantStatementVerification = merchantStatementIndexPage.clickOnStatementMenu();

        testVerifyLog("Verify Monthly statements display since user started the account.");
        if (merchantStatementVerification.verifyStatementDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (merchantStatementIndexPage.isStatementDisplay()) {

            merchantStatementVerification = merchantStatementIndexPage.clickOnViewButton();

            testVerifyLog("Verify Statement downloaded successfully.");
            if (merchantStatementVerification.verifyStatementDownloaded()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");

    }

    @Test
    public void merchant_StatementFilter() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_079 :: To verify Merchant can filter the Statements.");
        merchantLoginIndexPage.getVersion();

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (merchantLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantStatementVerification = merchantStatementIndexPage.clickOnStatementMenu();

        testVerifyLog("Verify user can see the Statement Screen with list of statements.");
        if (merchantStatementVerification.verifyStatementsScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (merchantStatementIndexPage.isStatementDisplay()) {

            if (!isPositiveExecution) {
                merchantStatementVerification = merchantStatementIndexPage.selectStatementToFilter(true);

                testVerifyLog("Verify No Statement display for current month.");
                if (merchantStatementVerification.verifyNoStatementDisplay()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            merchantStatementVerification = merchantStatementIndexPage.clickOnFilterButton();

            merchantStatementVerification = merchantStatementIndexPage.selectStatementToFilter(false);

            testVerifyLog("Verify Statement filtered successfully.");
            if (merchantStatementVerification.verifyStatementFilterSuccessfully()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");

    }

}
