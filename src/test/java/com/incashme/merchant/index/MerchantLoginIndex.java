package com.incashme.merchant.index;

import com.framework.init.SeleniumInit;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Rahul R.
 * Date: 2019-04-09
 * Time
 * Project Name: InCashMe
 */

public class MerchantLoginIndex extends SeleniumInit {

    @Test
    public void merchant_LoginLogout() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_066 :: To verify Merchant can Login and Logout with the valid email credentials.");
        merchantLoginIndexPage.getVersion();

        if (!isPositiveExecution) {
            merchantLoginVerification = merchantLoginIndexPage.invalidLoginAs(getInvalidEmail(), getRandomPassword());

            testVerifyLog("Verify validation message for the invalid email address.");
            if (merchantLoginVerification.verifyInvalidEmailValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantLoginVerification = merchantLoginIndexPage.loginAs(getUnRegisteredEmail(), getRandomPassword());

            testVerifyLog("Verify validation message for the blank credentials.");
            if (merchantLoginVerification.verifyUnregisteredEmailValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (merchantLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantLoginVerification = merchantLoginIndexPage.clickOnLogout();

        testVerifyLog("Verify user logout successfully from the Application.");
        if (merchantLoginVerification.verifyUserLogoutSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantLoginVerification = merchantLoginIndexPage.clickOnPrivacyPolicy();

        testVerifyLog("Verify user can see the Privacy Policy screen.");
        if (merchantLoginVerification.verifyPrivacyPolicyScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantLoginVerification = merchantLoginIndexPage.clickOnTermsCondition();

        testVerifyLog("Verify user can see the Terms & Condition screen.");
        if (merchantLoginVerification.verifyTnCScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");

    }

    //Need Mailinator account with active mobile number
    @Test(enabled = false)
    public void merchant_ForgotPassword() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_067 :: To verify Forgot Password functionality.");
        merchantLoginIndexPage.getVersion();

        merchantLoginVerification = merchantLoginIndexPage.clickOnForgotPassword();

        merchantLoginVerification = merchantLoginIndexPage.clickOnBackToLogin();

        merchantLoginVerification = merchantLoginIndexPage.clickOnForgotPassword();

        testVerifyLog("Verify user can see the Forgot Password screen.");
        if (merchantLoginVerification.verifyForgotPasswordScreenDisplay()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantLoginVerification = merchantLoginIndexPage.forgotPasswordAs(getInvalidEmail());

        testVerifyLog("Verify validation message for the invalid email address.");
        if (merchantLoginVerification.verifyInvalidEmailValidation()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantLoginVerification = merchantLoginIndexPage.forgotPasswordAs(username);
        merchantLoginVerification = merchantLoginIndexPage.clickOnSubmitButton();

        testVerifyLog("Verify OTP Screen display for entering the OTP.");
        if (merchantLoginVerification.verifyStep1OTPScreenDisplay()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantLoginVerification = merchantLoginIndexPage.enterOTP(getRandomNumber() + "");
        merchantLoginVerification = merchantLoginIndexPage.clickOnSubmitButton();

        testVerifyLog("Verify invalid entered OTP validation message.");
        if (merchantLoginVerification.verifyInvalidOTPValidation()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantLoginVerification = merchantLoginIndexPage.waitAndClickResendButton();

        testVerifyLog("Verify OTP Screen display for entering the OTP.");
        if (merchantLoginVerification.verifyStep1OTPScreenDisplay()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantLoginVerification = merchantLoginIndexPage.enterStep1OTP();
        merchantLoginVerification = merchantLoginIndexPage.clickOnSubmitButton();

        testVerifyLog("Verify OTP Screen display after entering Step 1 OTP.");
        if (merchantLoginVerification.verifyStep2OTPScreenDisplay()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantLoginVerification = merchantLoginIndexPage.openResetPasswordLink(username);

        testVerifyLog("Verify Reset Password screen display.");
        if (merchantLoginVerification.verifyResetPasswordScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantLoginVerification = merchantLoginIndexPage.enterOTP(getRandomNumber() + "1");

        testVerifyLog("Verify Invalid OTP digits message.");
        if (merchantLoginVerification.verifyInvalidOTPDigitMessage()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantLoginVerification = merchantLoginIndexPage.enterStep2OTP();

        merchantLoginVerification = merchantLoginIndexPage.changeInvalidNewPassword();

        testVerifyLog("Verify validation message for the invalid new password.");
        if (merchantLoginVerification.verifyInvalidNewPasswordValidation()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantLoginVerification = merchantLoginIndexPage.changeAsPastPassword();

        testVerifyLog("Verify validation message for the past password as new password.");
        if (merchantLoginVerification.verifyPastPasswordValidation()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantLoginVerification = merchantLoginIndexPage.changeAgentNewPassword();

        testVerifyLog("Verify confirmation message for the Password changed successfully.");
        if (merchantLoginVerification.verifyPasswordChangeSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (merchantLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void merchant_NonKYCLogin() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_065 :: To verify Non KYC Merchant tries to access the application with Login and Forgot Password.");
        merchantLoginIndexPage.getVersion();

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can see the KYC Validation message for the non kyc users.");
        if (merchantLoginVerification.verifyLoginKYCValidationMessage()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantLoginVerification = merchantLoginIndexPage.clickOnForgotPassword();

        testVerifyLog("Verify user can see the Forgot Password screen.");
        if (merchantLoginVerification.verifyForgotPasswordScreenDisplay()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantLoginVerification = merchantLoginIndexPage.forgotPasswordAs(username);
        merchantLoginVerification = merchantLoginIndexPage.clickOnSubmitButton();

        testVerifyLog("Verify user can see the KYC Validation message for the non kyc users.");
        if (merchantLoginVerification.verifyForgotPasswordKYCValidationMessage()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }
}