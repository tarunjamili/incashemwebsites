package com.incashme.merchant.index;

import com.framework.init.SeleniumInit;
import com.incashme.merchant.indexpage.MerchantDashboardIndexPage;
import com.incashme.merchant.indexpage.MerchantTransactionHistoryIndexPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class MerchantTransactionHistoryIndex extends SeleniumInit {

    @Test
    public void merchant_TransactionSearchForTables() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_077 :: To verify Merchant can search the Transaction History.");
        merchantLoginIndexPage.getVersion();

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (merchantLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnTxHistoryMenu();

        if (merchantTransactionHistoryIndexPage.isTransactionDataDisplay()) {

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.enterTransactionSearchIDCriteria(false);

            testVerifyLog("Verify search details display properly.");
            if (merchantTransactionHistoryVerification.verifySearchSuccessful()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            if (!isPositiveExecution) {
                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.enterTransactionSearchIDCriteria(true);

                testVerifyLog("Verify validation message display for the invalid search criteria.");
                if (merchantTransactionHistoryVerification.verifyValidationForInvalidSearch()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");

    }

    @Test
    public void merchant_TransactionFilter() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_076 :: To verify Merchant can filter the transaction history.");
        merchantLoginIndexPage.getVersion();

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (merchantLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnTxHistoryMenu();

        if (merchantTransactionHistoryIndexPage.isTransactionDataDisplay()) {

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnFilterIcon();

            testVerifyLog("Verify user can see the Filter screen.");
            if (merchantTransactionHistoryVerification.verifyTransactionFilterScreenDisplay()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.getConsumerDetailsForFilter();

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.changeFilterUserTo("Consumer");

            if (!isPositiveExecution) {
                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.filterFirstName(true);

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("User can see validation message for invalid filtered data.");
                if (merchantTransactionHistoryVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.filterFirstName(false);

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify First Name filter successfully.");
            if (merchantTransactionHistoryVerification.verifyFirstNameFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clearAllConsumerFilterDetails();

            if (!isPositiveExecution) {
                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.filterLastName(true);

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("User can see validation message for invalid filtered data.");
                if (merchantTransactionHistoryVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.filterLastName(false);

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify Last Name filter successfully.");
            if (merchantTransactionHistoryVerification.verifyLastNameFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clearAllConsumerFilterDetails();

            if (!isPositiveExecution) {
                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.filterConsumerNumber(true);

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("User can see validation message for invalid filtered data.");
                if (merchantTransactionHistoryVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.filterConsumerNumber(false);

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify Mobile Number filter successfully.");
            if (merchantTransactionHistoryVerification.verifyPhoneNumberFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clearAllConsumerFilterDetails();

            if (!isPositiveExecution) {
                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.filterTransactionId(true);

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("User can see validation message for invalid filtered data.");
                if (merchantTransactionHistoryVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.filterTransactionId(false);

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify Transaction ID filter successfully.");
            if (merchantTransactionHistoryVerification.verifyTxIDFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clearAllConsumerFilterDetails();

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.filterTransactionType();

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify Transaction Type filter successfully.");
            if (merchantTransactionHistoryVerification.verifyTransactionTypeFilterSuccessfully()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clearAllConsumerFilterDetails();

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.enterTransactionAmountRange(true);

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify validation message for Max amount is less than Min amount.");
            if (merchantTransactionHistoryVerification.verifyMaxMinTransactionValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clearAllConsumerFilterDetails();

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.enterTransactionAmount("from");

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify user can filter by min amount.");
            if (merchantTransactionHistoryVerification.verifyTransactionFromFieldFilter()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clearAllConsumerFilterDetails();

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.enterTransactionAmount("to");

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify user can filter by max amount.");
            if (merchantTransactionHistoryVerification.verifyTransactionToFieldFilter()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clearAllConsumerFilterDetails();

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.enterTransactionAmountRange(false);

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify user can filter with From and To Amount.");
            if (merchantTransactionHistoryVerification.verifyTransactionRangeFilter()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clearAllConsumerFilterDetails();

            merchantDashboardVerification = merchantDashboardIndexPage.clickOnHomeMenu();

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnTxHistoryMenu();

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.getMerchantDetailsForFilter();

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnFilterIcon();

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.changeFilterUserTo("Merchant");

            if (!isPositiveExecution) {
                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.filterShopName(true);

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("User can see validation message for invalid filtered data.");
                if (merchantTransactionHistoryVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.filterShopName(false);

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify Shop Name filter successfully.");
            if (merchantTransactionHistoryVerification.verifyShopNameFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clearAllMerchantFilterDetails();

            if (!isPositiveExecution) {
                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.filterMerchantID(true);

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("User can see validation message for invalid filtered data.");
                if (merchantTransactionHistoryVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.filterMerchantID(false);

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify Merchant ID filter successfully.");
            if (merchantTransactionHistoryVerification.verifyMerchantIDFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clearMerchantIDField();

            if (!isPositiveExecution) {
                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.filterTransactionId(true);

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("User can see validation message for invalid filtered data.");
                if (merchantTransactionHistoryVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.filterTransactionId(false);

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify Transaction ID filter successfully.");
            if (merchantTransactionHistoryVerification.verifyTxIDFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clearTransactionID();

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.filterTransactionType();

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify Transaction Type filter successfully.");
            if (merchantTransactionHistoryVerification.verifyTransactionTypeFilterSuccessfully()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clearTransactionType();

            if (!isPositiveExecution) {
                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.enterTransactionAmountRange(true);

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("Verify validation message for Max amount is less than Min amount.");
                if (merchantTransactionHistoryVerification.verifyMaxMinTransactionValidation()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clearAmountRange();

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.enterTransactionAmount("from");

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify user can filter by min amount.");
            if (merchantTransactionHistoryVerification.verifyTransactionFromFieldFilter()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clearAmountRange();

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.enterTransactionAmount("to");

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify user can filter by max amount.");
            if (merchantTransactionHistoryVerification.verifyTransactionToFieldFilter()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clearAmountRange();

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.enterTransactionAmountRange(false);

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify user can filter with From and To Amount.");
            if (merchantTransactionHistoryVerification.verifyTransactionRangeFilter()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clearAmountRange();

        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void merchant_TransactionDetailsVerification() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_075 :: To verify Merchant can see the Transaction History in the screen properly.");
        merchantLoginIndexPage.getVersion();

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (merchantLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnTxHistoryMenu();

        if (merchantTransactionHistoryIndexPage.isTransactionDataDisplay()) {

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnFilterIcon();

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.changeFilterUserTo("Consumer");

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.getAnyConsumerDetails();

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnMOREButton();

            testVerifyLog("Verify Transaction details display same in pop up screen.");
            if (merchantTransactionHistoryVerification.verifyTransactionDetails("Consumer")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnClosePopUp();

            merchantDashboardVerification = merchantDashboardIndexPage.clickOnHomeMenu();

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnTxHistoryMenu();

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnFilterIcon();

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.changeFilterUserTo("Merchant");

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.getAnyConsumerDetails();

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnMOREButton();

            testVerifyLog("Verify Transaction details display same in pop up screen.");
            if (merchantTransactionHistoryVerification.verifyTransactionDetails("Merchant")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void merchant_TransactionDetailsRefundButton() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_084 :: To verify Merchant can see Refund button for only Refund Transaction Types.");
        merchantLoginIndexPage.getVersion();

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (merchantLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnTxHistoryMenu();

        if (merchantTransactionHistoryIndexPage.isTransactionDataDisplay()) {

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnFilterIcon();

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.changeFilterUserTo("Consumer");

            for (int index = 0; index < 3; index++) {
                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.filterTransactionType(index);

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

                if (merchantTransactionHistoryIndexPage.isFilterResultDisplay()) {

                    merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.selectAnyTransaction();

                    merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnMOREButton();

                    testVerifyLog("Verify Refund button display for the selected Transaction Type.");
                    if (merchantTransactionHistoryVerification.verifyRefundButtonDisplay()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }

                    merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnClosePopUp();
                } else {
                    testValidationLog("Sorry we couldn’t find any matches for your search criteria");
                }
            }

            for (int index = 4; index < 14; index++) {
                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.filterTransactionType(index);

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

                if (merchantTransactionHistoryIndexPage.isFilterResultDisplay()) {

                    merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.selectAnyTransaction();

                    merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnMOREButton();

                    testVerifyLog("Verify Refund button not display for the selected Transaction Type.");
                    if (merchantTransactionHistoryVerification.verifyRefundButtonNotDisplay()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }

                    merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnClosePopUp();
                } else {
                    testValidationLog("Sorry we couldn’t find any matches for your search criteria");
                }
            }

        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void merchant_TransactionDetailsRefundProcessForSellConsumer() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_085 :: To verify Merchant can refund the amount to user for Consumer Sell Transaction Type.");
        merchantLoginIndexPage.getVersion();

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (merchantLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        MerchantDashboardIndexPage.getCurrentBalance();

        merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnTxHistoryMenu();

        if (merchantTransactionHistoryIndexPage.isTransactionDataDisplay()) {

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnFilterIcon();

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.changeFilterUserTo("Consumer");

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.filterTransactionType(1);

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            if (merchantTransactionHistoryIndexPage.isFilterResultDisplay()) {

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.selectAnyTransaction();

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnMOREButton();

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.getTransactionDetails();

                testVerifyLog("Verify Refund button display for the selected Transaction Type.");
                if (merchantTransactionHistoryVerification.verifyRefundButtonDisplay()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                if (merchantTransactionHistoryIndexPage.isAdditionalDeviceTransfer()) {

                    merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.getAdditionalDeviceIDName();

                    merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnRefundButton();

                    merchantAdditionalDeviceVerification = merchantAdditionalDeviceIndexPage.openAdditionalDeviceInNewWindow();

                    testVerifyLog("Verify user can see the Additional Device screen properly.");
                    if (merchantAdditionalDeviceVerification.verifyAddDeviceScreen()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }

                    merchantAdditionalDeviceVerification = merchantAdditionalDeviceIndexPage.openAndGetAdditionalDeviceBalance();

                    if (MerchantTransactionHistoryIndexPage.isAdditionalDeviceBalanceLessThanRefundAmount()) {

                        merchantAdditionalDeviceVerification = merchantAdditionalDeviceIndexPage.closeAdditionalDeviceWindow();

                        if (!isPositiveExecution) {
                            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnSendRefundButton();

                            testVerifyLog("Verify validation message for insufficient amount for refund.");
                            if (merchantTransactionHistoryVerification.verifyRefundInsufficient()) {
                                stepPassed();
                            } else {
                                stepFailure(driver);
                                numOfFailedSteps++;
                            }

                            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.enterZeroRefundAmountToAddDevice();

                            testVerifyLog("Verify validation message for the zero refund amount.");
                            if (merchantTransactionHistoryVerification.verifyZeroAmountValidation()) {
                                stepPassed();
                            } else {
                                stepFailure(driver);
                                numOfFailedSteps++;
                            }

                            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.enterMaxRefundAmountToAddDevice();

                            testVerifyLog("Verify validation message for the max refund amount.");
                            if (merchantTransactionHistoryVerification.verifyMaxAmountValidation()) {
                                stepPassed();
                            } else {
                                stepFailure(driver);
                                numOfFailedSteps++;
                            }
                        }
                        merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.enterRefundAmountToAddDevice();

                        merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnAddMoney();

                        testVerifyLog("Verify confirmation message for amount added in additional device.");
                        if (merchantTransactionHistoryVerification.verifyConfirmationForRefundAddedInAdditionalDevice()) {
                            stepPassed();
                        } else {
                            stepFailure(driver);
                            numOfFailedSteps++;
                        }

                        merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnClosePopUp();

                        merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnFilterIcon();

                        refresh(driver);

                        merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnMOREButton(0);

                        testVerifyLog("Verify Amount Transfer from Merchant to Additional Device.");
                        if (merchantTransactionHistoryVerification.verifyAmountWithdrawFromMerchantToAddDevice()) {
                            stepPassed();
                        } else {
                            stepFailure(driver);
                            numOfFailedSteps++;
                        }

                        merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnClosePopUp();

                        merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnMOREButton(1);

                        testVerifyLog("Verify Amount Transfer from Merchant to Additional Device.");
                        if (merchantTransactionHistoryVerification.verifyAmountDepositFromMerchantToAddDevice()) {
                            stepPassed();
                        } else {
                            stepFailure(driver);
                            numOfFailedSteps++;
                        }

                        merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnClosePopUp();

                        merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnFilterIcon();

                        merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.filterTransactionType(1);

                        merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

                        merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnMOREButton();

                        merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnRefundButton();

                        testVerifyLog("Verify Refund details in confirmation popup screen.");
                        if (merchantTransactionHistoryVerification.verifyConfirmationRefundDetailsForAddDevice()) {
                            stepPassed();
                        } else {
                            stepFailure(driver);
                            numOfFailedSteps++;
                        }

                    } else {
                        merchantAdditionalDeviceVerification = merchantAdditionalDeviceIndexPage.closeAdditionalDeviceWindow();
                        merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.setNoBalanceDeduction();
                    }

                } else {
                    merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnRefundButton();

                    testVerifyLog("Verify Refund details in confirmation popup screen.");
                    if (merchantTransactionHistoryVerification.verifyConfirmationRefundDetails()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }
                }

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnCancelRefundButton();

                testVerifyLog("Verify Refund button display for the selected Transaction Type.");
                if (merchantTransactionHistoryVerification.verifyRefundButtonDisplay()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnRefundButton();

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnSendRefundButton();

                testVerifyLog("Verify Confirmation Message for Refund successful.");
                if (merchantTransactionHistoryVerification.verifyRefundConfirmation()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                refresh(driver);

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.searchRefundTransaction();

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnMOREButton(0);

                testVerifyLog("Verify Transaction status updated to Refunded.");
                if (merchantTransactionHistoryVerification.verifyTransactionRefunded()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnClosePopUp();

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clearSearchDetails();

                refresh(driver);

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnMOREButton(0);

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.getLatestTransactionDetails();

                testVerifyLog("Verify Refund Details is similar to Transaction.");
                if (merchantTransactionHistoryVerification.verifyCompletedRefundDetails()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnClosePopUp();

                merchantDashboardVerification = merchantDashboardIndexPage.clickOnHomeMenu();

                if (merchantTransactionHistoryIndexPage.getIsBalanceDeducted()) {
                    testVerifyLog("Verify Balance updated after Refund completed.");
                    if (merchantDashboardVerification.verifyBalanceAfterRefund()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }
                }
            } else {
                testValidationLog("Sorry we couldn’t find any matches for your search criteria");
            }
        }
        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void merchant_TransactionDetailsRefundProcessForTakeTransferConsumer() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_086 :: To verify Merchant can refund the amount to user for Take Transfer Transaction Type.");
        merchantLoginIndexPage.getVersion();

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (merchantLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        MerchantDashboardIndexPage.getCurrentBalance();

        merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnTxHistoryMenu();

        if (merchantTransactionHistoryIndexPage.isTransactionDataDisplay()) {

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnFilterIcon();

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.changeFilterUserTo("Consumer");

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.filterTransactionType(0);

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            if (merchantTransactionHistoryIndexPage.isFilterResultDisplay()) {

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.selectAnyTransaction();

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnMOREButton();

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.getTransactionDetails();

                testVerifyLog("Verify Refund button display for the selected Transaction Type.");
                if (merchantTransactionHistoryVerification.verifyRefundButtonDisplay()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnRefundButton();

                testVerifyLog("Verify Refund details in confirmation popup screen.");
                if (merchantTransactionHistoryVerification.verifyConfirmationRefundDetails()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnCancelRefundButton();

                testVerifyLog("Verify Refund button display for the selected Transaction Type.");
                if (merchantTransactionHistoryVerification.verifyRefundButtonDisplay()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnRefundButton();

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnSendRefundButton();

                testVerifyLog("Verify Confirmation Message for Refund successful.");
                if (merchantTransactionHistoryVerification.verifyRefundConfirmation()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                refresh(driver);

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.searchRefundTransaction();

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnMOREButton(0);

                testVerifyLog("Verify Transaction status updated to Refunded.");
                if (merchantTransactionHistoryVerification.verifyTransactionRefunded()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnClosePopUp();

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clearSearchDetails();

                refresh(driver);

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnMOREButton(0);

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.getLatestTransactionDetails();

                testVerifyLog("Verify Refund Details is similar to Transaction.");
                if (merchantTransactionHistoryVerification.verifyCompletedRefundDetails()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnClosePopUp();

                merchantDashboardVerification = merchantDashboardIndexPage.clickOnHomeMenu();

                testVerifyLog("Verify Balance updated after Refund completed.");
                if (merchantDashboardVerification.verifyBalanceAfterRefund()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            } else {
                testValidationLog("Sorry we couldn’t find any matches for your search criteria");
            }
        }
        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void merchant_TransactionDetailsRefundProcessForTakeInvoiceConsumer() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_087 :: To verify Merchant can refund the amount to user for Take Invoice Transaction Type.");
        merchantLoginIndexPage.getVersion();

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (merchantLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        MerchantDashboardIndexPage.getCurrentBalance();

        merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnTxHistoryMenu();

        if (merchantTransactionHistoryIndexPage.isTransactionDataDisplay()) {

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnFilterIcon();

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.changeFilterUserTo("Consumer");

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.filterTransactionType(2);

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            if (merchantTransactionHistoryIndexPage.isFilterResultDisplay()) {

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.selectAnyTransaction();

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnMOREButton();

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.getTransactionDetails();

                testVerifyLog("Verify Refund button display for the selected Transaction Type.");
                if (merchantTransactionHistoryVerification.verifyRefundButtonDisplay()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnRefundButton();

                testVerifyLog("Verify Refund details in confirmation popup screen.");
                if (merchantTransactionHistoryVerification.verifyConfirmationRefundDetails()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnCancelRefundButton();

                testVerifyLog("Verify Refund button display for the selected Transaction Type.");
                if (merchantTransactionHistoryVerification.verifyRefundButtonDisplay()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnRefundButton();

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnSendRefundButton();

                testVerifyLog("Verify Confirmation Message for Refund successful.");
                if (merchantTransactionHistoryVerification.verifyRefundConfirmation()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                refresh(driver);

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.searchRefundTransaction();

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnMOREButton(0);

                testVerifyLog("Verify Transaction status updated to Refunded.");
                if (merchantTransactionHistoryVerification.verifyTransactionRefunded()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnClosePopUp();

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clearSearchDetails();

                refresh(driver);

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnMOREButton(0);

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.getLatestTransactionDetails();

                testVerifyLog("Verify Refund Details is similar to Transaction.");
                if (merchantTransactionHistoryVerification.verifyCompletedRefundDetails()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnClosePopUp();

                merchantDashboardVerification = merchantDashboardIndexPage.clickOnHomeMenu();

                testVerifyLog("Verify Balance updated after Refund completed.");
                if (merchantDashboardVerification.verifyBalanceAfterRefund()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

            } else {
                testValidationLog("Sorry we couldn’t find any matches for your search criteria");
            }
        }
        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void merchant_TransactionDetailsRefundProcessForSellMerchant() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_088 :: To verify Merchant can refund the amount to user for Merchant Sell Transaction Type.");
        merchantLoginIndexPage.getVersion();

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (merchantLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        MerchantDashboardIndexPage.getCurrentBalance();

        merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnTxHistoryMenu();

        if (merchantTransactionHistoryIndexPage.isTransactionDataDisplay()) {

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnFilterIcon();

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.changeFilterUserTo("Merchant");

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.filterTransactionType(1);

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            if (merchantTransactionHistoryIndexPage.isFilterResultDisplay()) {

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.selectAnyTransaction();

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnMOREButton();

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.getTransactionDetails();

                testVerifyLog("Verify Refund button display for the selected Transaction Type.");
                if (merchantTransactionHistoryVerification.verifyRefundButtonDisplay()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                if (merchantTransactionHistoryIndexPage.isAdditionalDeviceTransfer()) {

                    merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.getAdditionalDeviceIDName();

                    merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnRefundButton();

                    merchantAdditionalDeviceVerification = merchantAdditionalDeviceIndexPage.openAdditionalDeviceInNewWindow();

                    testVerifyLog("Verify user can see the Additional Device screen properly.");
                    if (merchantAdditionalDeviceVerification.verifyAddDeviceScreen()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }

                    merchantAdditionalDeviceVerification = merchantAdditionalDeviceIndexPage.openAndGetAdditionalDeviceBalance();

                    if (MerchantTransactionHistoryIndexPage.isAdditionalDeviceBalanceLessThanRefundAmount()) {

                        merchantAdditionalDeviceVerification = merchantAdditionalDeviceIndexPage.closeAdditionalDeviceWindow();

                        if (!isPositiveExecution) {
                            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnSendRefundButton();

                            testVerifyLog("Verify validation message for insufficient amount for refund.");
                            if (merchantTransactionHistoryVerification.verifyRefundInsufficient()) {
                                stepPassed();
                            } else {
                                stepFailure(driver);
                                numOfFailedSteps++;
                            }

                            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.enterZeroRefundAmountToAddDevice();

                            testVerifyLog("Verify validation message for the zero refund amount.");
                            if (merchantTransactionHistoryVerification.verifyZeroAmountValidation()) {
                                stepPassed();
                            } else {
                                stepFailure(driver);
                                numOfFailedSteps++;
                            }

                            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.enterMaxRefundAmountToAddDevice();

                            testVerifyLog("Verify validation message for the max refund amount.");
                            if (merchantTransactionHistoryVerification.verifyMaxAmountValidation()) {
                                stepPassed();
                            } else {
                                stepFailure(driver);
                                numOfFailedSteps++;
                            }
                        }

                        merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.enterRefundAmountToAddDevice();

                        merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnAddMoney();

                        testVerifyLog("Verify confirmation message for amount added in additional device.");
                        if (merchantTransactionHistoryVerification.verifyConfirmationForRefundAddedInAdditionalDevice()) {
                            stepPassed();
                        } else {
                            stepFailure(driver);
                            numOfFailedSteps++;
                        }

                        merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnClosePopUp();

                        merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnFilterIcon();

                        refresh(driver);

                        merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnMOREButton(0);

                        testVerifyLog("Verify Amount Transfer from Merchant to Additional Device.");
                        if (merchantTransactionHistoryVerification.verifyAmountWithdrawFromMerchantToAddDevice()) {
                            stepPassed();
                        } else {
                            stepFailure(driver);
                            numOfFailedSteps++;
                        }

                        merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnClosePopUp();

                        merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnMOREButton(1);

                        testVerifyLog("Verify Amount Transfer from Merchant to Additional Device.");
                        if (merchantTransactionHistoryVerification.verifyAmountDepositFromMerchantToAddDevice()) {
                            stepPassed();
                        } else {
                            stepFailure(driver);
                            numOfFailedSteps++;
                        }

                        merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnClosePopUp();

                        merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnFilterIcon();

                        merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.filterTransactionType(1);

                        merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

                        merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnMOREButton();

                        merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnRefundButton();

                        testVerifyLog("Verify Refund details in confirmation popup screen.");
                        if (merchantTransactionHistoryVerification.verifyConfirmationRefundDetailsForAddDevice()) {
                            stepPassed();
                        } else {
                            stepFailure(driver);
                            numOfFailedSteps++;
                        }

                    } else {
                        merchantAdditionalDeviceVerification = merchantAdditionalDeviceIndexPage.closeAdditionalDeviceWindow();
                        merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.setNoBalanceDeduction();
                    }

                } else {
                    merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnRefundButton();

                    testVerifyLog("Verify Refund details in confirmation popup screen.");
                    if (merchantTransactionHistoryVerification.verifyConfirmationRefundDetailsForMerchantSell()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }
                }

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnCancelRefundButton();

                testVerifyLog("Verify Refund button display for the selected Transaction Type.");
                if (merchantTransactionHistoryVerification.verifyRefundButtonDisplay()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnRefundButton();

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnSendRefundButton();

                testVerifyLog("Verify Confirmation Message for Refund successful.");
                if (merchantTransactionHistoryVerification.verifyRefundConfirmation()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                refresh(driver);

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.searchRefundTransaction();

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnMOREButton(0);

                testVerifyLog("Verify Transaction status updated to Refunded.");
                if (merchantTransactionHistoryVerification.verifyTransactionRefunded()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnClosePopUp();

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clearSearchDetails();

                refresh(driver);

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnMOREButton(0);

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.getLatestTransactionDetails();

                testVerifyLog("Verify Refund Details is similar to Transaction.");
                if (merchantTransactionHistoryVerification.verifyCompletedRefundDetails()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnClosePopUp();

                merchantDashboardVerification = merchantDashboardIndexPage.clickOnHomeMenu();

                if (merchantTransactionHistoryIndexPage.getIsBalanceDeducted()) {
                    testVerifyLog("Verify Balance updated after Refund completed.");
                    if (merchantDashboardVerification.verifyBalanceAfterRefund()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }
                }

            } else {
                testValidationLog("Sorry we couldn’t find any matches for your search criteria");
            }
        }
        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void merchant_TransactionDetailsRefundProcessForTakeTransferMerchant() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_089 :: To verify Merchant can refund the amount to user for Merchant Take Transfer Transaction Type.");
        merchantLoginIndexPage.getVersion();

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (merchantLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        MerchantDashboardIndexPage.getCurrentBalance();

        merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnTxHistoryMenu();

        if (merchantTransactionHistoryIndexPage.isTransactionDataDisplay()) {

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnFilterIcon();

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.changeFilterUserTo("Merchant");

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.filterTransactionType(0);

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            if (merchantTransactionHistoryIndexPage.isFilterResultDisplay()) {

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.selectAnyTransaction();

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnMOREButton();

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.getTransactionDetails();

                testVerifyLog("Verify Refund button display for the selected Transaction Type.");
                if (merchantTransactionHistoryVerification.verifyRefundButtonDisplay()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnRefundButton();

                testVerifyLog("Verify Refund details in confirmation popup screen.");
                if (merchantTransactionHistoryVerification.verifyConfirmationRefundDetails()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnCancelRefundButton();

                testVerifyLog("Verify Refund button display for the selected Transaction Type.");
                if (merchantTransactionHistoryVerification.verifyRefundButtonDisplay()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnRefundButton();

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnSendRefundButton();

                testVerifyLog("Verify Confirmation Message for Refund successful.");
                if (merchantTransactionHistoryVerification.verifyRefundConfirmation()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                refresh(driver);

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.searchRefundTransaction();

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnMOREButton(0);

                testVerifyLog("Verify Transaction status updated to Refunded.");
                if (merchantTransactionHistoryVerification.verifyTransactionRefunded()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnClosePopUp();

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clearSearchDetails();

                refresh(driver);

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnMOREButton(0);

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.getLatestTransactionDetails();

                testVerifyLog("Verify Refund Details is similar to Transaction.");
                if (merchantTransactionHistoryVerification.verifyCompletedRefundDetails()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnClosePopUp();

                merchantDashboardVerification = merchantDashboardIndexPage.clickOnHomeMenu();

                testVerifyLog("Verify Balance updated after Refund completed.");
                if (merchantDashboardVerification.verifyBalanceAfterRefund()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

            } else {
                testValidationLog("Sorry we couldn’t find any matches for your search criteria");
            }
        }
        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void merchant_TransactionDetailsRefundProcessForTakeInvoiceMerchant() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_090 :: To verify Merchant can refund the amount to user for Merchant Take Invoice Transaction Type.");
        merchantLoginIndexPage.getVersion();

        merchantLoginVerification = merchantLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (merchantLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        MerchantDashboardIndexPage.getCurrentBalance();

        merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnTxHistoryMenu();

        if (merchantTransactionHistoryIndexPage.isTransactionDataDisplay()) {

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnFilterIcon();

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.changeFilterUserTo("Merchant");

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.filterTransactionType(2);

            merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            if (merchantTransactionHistoryIndexPage.isFilterResultDisplay()) {

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.selectAnyTransaction();

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnMOREButton();

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.getTransactionDetails();

                testVerifyLog("Verify Refund button display for the selected Transaction Type.");
                if (merchantTransactionHistoryVerification.verifyRefundButtonDisplay()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnRefundButton();

                testVerifyLog("Verify Refund details in confirmation popup screen.");
                if (merchantTransactionHistoryVerification.verifyConfirmationRefundDetailsForMerchantSell()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnCancelRefundButton();

                testVerifyLog("Verify Refund button display for the selected Transaction Type.");
                if (merchantTransactionHistoryVerification.verifyRefundButtonDisplay()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnRefundButton();

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnSendRefundButton();

                testVerifyLog("Verify Confirmation Message for Refund successful.");
                if (merchantTransactionHistoryVerification.verifyRefundConfirmation()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                refresh(driver);

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.searchRefundTransaction();

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnMOREButton(0);

                testVerifyLog("Verify Transaction status updated to Refunded.");
                if (merchantTransactionHistoryVerification.verifyTransactionRefunded()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnClosePopUp();

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clearSearchDetails();

                refresh(driver);

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnMOREButton(0);

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.getLatestTransactionDetails();

                testVerifyLog("Verify Refund Details is similar to Transaction.");
                if (merchantTransactionHistoryVerification.verifyCompletedRefundDetails()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                merchantTransactionHistoryVerification = merchantTransactionHistoryIndexPage.clickOnClosePopUp();

                merchantDashboardVerification = merchantDashboardIndexPage.clickOnHomeMenu();

                testVerifyLog("Verify Balance updated after Refund completed.");
                if (merchantDashboardVerification.verifyBalanceAfterRefund()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            } else {
                testValidationLog("Sorry we couldn’t find any matches for your search criteria");
            }
        }
        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }
}