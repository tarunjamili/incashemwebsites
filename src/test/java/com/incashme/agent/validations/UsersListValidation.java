package com.incashme.agent.validations;

/**
 * Created by Rahul R.
 * Date: 2019-04-11
 * Time: 12:50
 * Project Name: InCashMe
 */
public interface UsersListValidation {

    String NO_CONSUMER_FOUND = "No consumers found! Start adding Consumers today to earn Money.";
    String NO_MERCHANT_FOUND = "No merchants found! Start adding Merchants today to earn Money.";

    String FIRST_NAME_MORE_THAN_1_CHAR = "First Name should be greater than 1 characters long.";
    String FIRST_NAME_NUMERIC = "First Name should be Minimum 2 characters long and Maximum 20 characters and can only contain letters.";
    String FIRST_NAME_LESS_THAN_20_CHAR = "First Name should not be greater than 20 characters long.";
    String LAST_NAME_MORE_THAN_1_CHAR = "Last Name should be greater than 1 characters long.";
    String LAST_NAME_NUMERIC = "Last Name should be Minimum 2 characters long and Maximum 20 characters and can only contain letters.";
    String LAST_NAME_LESS_THAN_20_CHAR = "Last Name should not be greater than 20 characters long.";
    String EMAIL_ADDRESS_INVALID = "Please provide a valid email address.";
    String MOBILE_NUMBER_VALIDATION = "Phone Number should be 10 Digit";
    String INVALID_FILE_FORMAT_UPLOAD = "Please Upload files in png, jpg, jpeg format.";
    String USER_NAME_VALIDATION_MESSAGE = "User Name should be minimum 8 characters and contain atleast 1 of the special character (.@#_)";

    String OTP_MISMATCH = "OTP Mismatch";
    String INVALID_OTP_MESSAGE = "The otp must be between 1 and 5 digits.";

    String ADDRESS_MORE_THAN_50_CHAR = "Address should not be greater than 50 characters long.";
    String VILLAGE_MORE_THAN_50_CHAR = "Village should not be greater than 50 characters long.";
    String TEHSIL_MORE_THAN_50_CHAR = "Tehsil should not be greater than 50 characters long.";
    String DISTRICT_MORE_THAN_50_CHAR = "District should not be greater than 50 characters long.";
    String COUNTRY_NUMERIC_VALUES = "Country can only contain letter and spaces";
    String COUNTRY_LESS_THAN_1_CHAR = "Country should be Minimum 2 characters long";
    String COUNTRY_MORE_THAN_30_CHAR = "Country should be Maximum 30 character long.";
    String PIN_CODE_VALIDATION = "Pin Code should be 6 Digit";

    String BUSINESS_NAME_LESS_THAN_4_CHAR = "Company Name should be minimum 4 characters long.";
    String BUSINESS_NAME_MORE_THAN_50_CHAR = "Company Name should be Maximum 50 characters long.";
    String DBA_NAME_LESS_THAN_4_CHAR = "DBA should be minimum 4 characters long.";
    String DBA_NAME_MORE_THAN_50_CHAR = "DBA should be Maximum 50 characters long.";
    String GST_NUMBER_VALIDATION = "GST Number should be 15 characters long.";

    String USER_CREATED_SUCCESSFULLY = "Successfully Created !";
    String USER_CONFIRMATION_DETAILS = "Temporary Password sent to registered Email & Mobile Number.";

    String NO_TRANSACTION_FOUND = "No Transactions found !";
    String NO_SEARCH_RECORD_FOUND = "No matching records found";

    String NO_FILTER_DATA_AVAILABLE = "Wakeup !";
}

