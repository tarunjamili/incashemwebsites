package com.incashme.agent.validations;

/**
 * Created by Rahul R.
 * Date: 2019-04-11
 * Time: 12:15
 * Project Name: InCashMe
 */
public interface TransactionHistoryValidations {

    String NO_TRANSACTION_HISTORY_FOUND = "Here is no data alive. Start adding target to earn Money.";

    String NO_SEARCH_RECORD_FOUND = "No matching records found";

    String NO_FILTER_DATA_AVAILABLE = "Wakeup !";

    String INVALID_TOPUP_AMOUNT_RANGE = "TopUp Amount Range is Invalid!";
    String INVALID_COMMISSION_AMOUNT_RANGE = "Commission Amount Range is Invalid!";


}
