package com.incashme.agent.validations;

/**
 * Created by Rahul R.
 * Date: 2019-03-25
 * Time: 14:31
 * Project Name: InCashMe
 */
public interface StatementsValidations {

    String NO_STATEMENTS_FOUND = "Sorry we couldn’t find any matches for your search result.";

}
