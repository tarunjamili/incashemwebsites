package com.incashme.agent.validations;

/**
 * Created by Rahul R.
 * Date: 2019-03-25
 * Time: 14:29
 * Project Name: InCashMe
 */
public interface CommissionValidations {

    String NO_COMMISSION_FOUND = "No TopUp Transactions from Agents.";

    String INVALID_TOPUP_AMOUNT_RANGE = "TopUp Amount Range is Invalid!";
    String INVALID_COMMISSION_AMOUNT_RANGE = "Commission Amount Range is Invalid!";

    String NO_SEARCH_RECORD_FOUND = "No matching records found";

    String NO_FILTER_DATA_AVAILABLE = "Wakeup !";

}
