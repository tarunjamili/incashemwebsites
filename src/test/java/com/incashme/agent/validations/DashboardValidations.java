package com.incashme.agent.validations;

/**
 * Created by Rahul R.
 * Date: 2019-03-29
 * Time: 14:40
 * Project Name: InCashMe
 */
public interface DashboardValidations {

    String FIRST_UNSUCCESSFUL_ATTEMPTS = "Unsuccessful attempt 1/3. Account locked temporarily after 3 attempts";
    String SECOND_UNSUCCESSFUL_ATTEMPTS = "Unsuccessful attempt 2/3. Account locked temporarily after 3 attempts";
    String THIRD_UNSUCCESSFUL_ATTEMPTS = "Unsuccessful attempt 3/3. Account locked temporarily for 40 mins.";
    String INVALID_NEW_PASSWORD = "Password should be minimum 8 characters. Must contain atleast 1 lowercase alphabetical character." +
            " Must contain atleast 1 uppercase alphabetical character. Must contain at least one special character." +
            " Must contain at least one number.";
    String NEW_CONFIRM_PASSWORD_NOT_MATCHED = "New Password and Confirm Password does not match.";
    String PAST_PASSWORD_MESSAGE = "New Password cannot be same as the old Password.";
    String SUCCESSFUL_PASSWORD_CHANGE_MESSAGE = "Your password was changed successfully. Please login with the new password.";

    String NO_TOPUP_COMMISSION_TODAY_MESSAGE = "Here is no data alive. Start adding target to earn Money.";
    String NO_TODAY_COMPLETED_TOPUP_MESSAGE = "No Topups found!";
    String NO_TODAY_ADDED_CONSUMERS = "No consumers found! Start adding Consumers today to earn Money.";
    String NO_TODAY_ADDED_MERCHANTS = "No merchants found! Start adding Merchants today to earn Money.";
    String NO_TOTAL_ADDED_CONSUMER_MESSAGE = "No data available in table";
    String NO_TOTAL_ADDED_MERCHANT_MESSAGE = "No data available in table";
    String NO_CREATED_CONSUMER_LIST_MESSAGE = "find any matches for your search result.";
    String NO_CREATED_MERCHANT_LIST_MESSAGE = "find any matches for your search result.";

    String NO_SEARCH_RECORD_FOUND = "No matching records found";

    String INVALID_NEW_PIN = "Pin should be 4 digit.";
    String INVALID_CONFIRM_PIN = "Confirm Pin should be 4 digit. ";
    String NEW_CONFIRM_PIN_NOT_MATCHED = "PIN and confirm PIN does not match.";
    String PAST_PIN_MESSAGE = "Cannot use a past testData";

    String KYC_PENDING_FOR_TOPUP = "Kyc Verification of recipient is pending. You cannot Top Up money now.";

    String INSUFFICIENT_TOPUP_BALANCE = "Insufficient Funds in account.";
    String MINIMUM_TOPUP_AMOUNT_VALIDATION = "Amount should be greater than or equal to INR 200.00";
    String NO_USER_FOUND = "No User found.";

    String NO_FILTER_DATA_AVAILABLE = "Wakeup !";

    String CONSUMER_TOPUP_VALIDATION = "Topup Amount should be Greater than";
    String CONSUMER_INSUFFICIENT_BALANCE_TOPUP = "Insufficient Funds in account.";

}
