package com.incashme.agent.index;

import com.framework.init.SeleniumInit;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Rahul R.
 * Date: 2019-04-26
 * Time: 14:47
 * Project Name: InCashMe
 */
public class AgentTransactionHistoryIndex extends SeleniumInit {

    @Test
    public void agent_TransactionSearchForTables() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_061 :: To verify Agent can sort the Transaction Details.");
        agentLoginIndexPage.getVersion();

        agentLoginVerification = agentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (agentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickOnTransactionHistory();

        if (agentTransactionHistoryIndexPage.isTransactionDataDisplay()) {

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.enterTransactionSearchIDCriteria(false);

            testVerifyLog("Verify search details display properly.");
            if (agentTransactionHistoryVerification.verifySearchSuccessful()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            if (!isPositiveExecution) {
                agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.enterTransactionSearchIDCriteria(true);

                testVerifyLog("Verify validation message display for the invalid search criteria.");
                if (agentTransactionHistoryVerification.verifyValidationForInvalidSearch()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");

    }

    @Test
    public void agent_TransactionSortAndFilter() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_035 :: To verify Agent can sort and filter the transaction history.");
        agentLoginIndexPage.getVersion();

        agentLoginVerification = agentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (agentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickOnTransactionHistory();

        if (agentTransactionHistoryIndexPage.isTransactionDataDisplay()) {

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickTransactionTopUpToSort();

            testVerifyLog("Verify Top ups are sorted successfully.");
            if (agentTransactionHistoryVerification.verifyTopUpsSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickCommissionToSort();

            testVerifyLog("Verify Commission are sorted successfully.");
            if (agentTransactionHistoryVerification.verifyCommissionSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickOnCommissionDateTimeToSort();

            testVerifyLog("Verify Date & Time are sorted successfully.");
            if (agentTransactionHistoryVerification.verifyCommissionDateTimeSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickOnFilterIcon();

            testVerifyLog("Verify user can see the Filter screen.");
            if (agentTransactionHistoryVerification.verifyTransactionFilterScreenDisplay()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.getConsumerDetailsForFilter();

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.changeFilterUserTo("Consumer");

            if (!isPositiveExecution) {
                agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.filterFirstName(true);

                agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("User can see validation message for invalid filtered data.");
                if (agentTransactionHistoryVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.filterFirstName(false);

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify First Name filter successfully.");
            if (agentTransactionHistoryVerification.verifyFirstNameFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clearAllConsumerFilterDetails();

            if (!isPositiveExecution) {
                agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.filterLastName(true);

                agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("User can see validation message for invalid filtered data.");
                if (agentTransactionHistoryVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.filterLastName(false);

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify Last Name filter successfully.");
            if (agentTransactionHistoryVerification.verifyLastNameFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clearAllConsumerFilterDetails();

            if (!isPositiveExecution) {
                agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.filterConsumerNumber(true);

                agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("User can see validation message for invalid filtered data.");
                if (agentTransactionHistoryVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.filterConsumerNumber(false);

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify Mobile Number filter successfully.");
            if (agentTransactionHistoryVerification.verifyPhoneNumberFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clearAllConsumerFilterDetails();

            if (!isPositiveExecution) {
                agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.filterTransactionId(true);

                agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("User can see validation message for invalid filtered data.");
                if (agentTransactionHistoryVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.filterTransactionId(false);

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify Transaction ID filter successfully.");
            if (agentTransactionHistoryVerification.verifyTxIDFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clearAllConsumerFilterDetails();

            if (!isPositiveExecution) {
                agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.enterTopUpAmountRange(true);

                agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("Verify validation message for Max amount is less than Min amount.");
                if (agentTransactionHistoryVerification.verifyMaxMinTopupValidation()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clearAllConsumerFilterDetails();

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.enterTopUpAmount("from");

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify user can filter by min amount.");
            if (agentTransactionHistoryVerification.verifyTopUpFromFieldFilter()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clearAllConsumerFilterDetails();

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.enterTopUpAmount("to");

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify user can filter by max amount.");
            if (agentTransactionHistoryVerification.verifyTopUpToFieldFilter()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clearAllConsumerFilterDetails();

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.enterTopUpAmountRange(false);

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify user can filter with From and To Amount.");
            if (agentTransactionHistoryVerification.verifyTopUpMaxMinFilter()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clearAllConsumerFilterDetails();

            if (!isPositiveExecution) {
                agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.enterCommissionAmountRange(true);

                agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("Verify validation message for Max amount is less than Min amount.");
                if (agentTransactionHistoryVerification.verifyMaxMinCommissionValidation()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clearAllConsumerFilterDetails();

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.enterCommissionAmount("from");

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify user can filter by min amount.");
            if (agentTransactionHistoryVerification.verifyCommissionFromFieldFilter()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clearAllConsumerFilterDetails();

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.enterCommissionAmount("to");

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify user can filter by max amount.");
            if (agentTransactionHistoryVerification.verifyCommissionToFieldFilter()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clearAllConsumerFilterDetails();

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.enterCommissionAmountRange(false);

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify user can filter with From and To Amount.");
            if (agentTransactionHistoryVerification.verifyCommissionMaxMinFilter()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clearAllConsumerFilterDetails();

            agentDashboardVerification = agentDashboardIndexPage.clickOnDashboardIcon();

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickOnTransactionHistory();

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.getMerchantDetailsForFilter();

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickOnFilterIcon();

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.changeFilterUserTo("Merchant");

            if (!isPositiveExecution) {
                agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.filterShopName(true);

                agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("User can see validation message for invalid filtered data.");
                if (agentTransactionHistoryVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.filterShopName(false);

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify Shop Name filter successfully.");
            if (agentTransactionHistoryVerification.verifyShopNameFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clearAllMerchantFilterDetails();

            if (!isPositiveExecution) {
                agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.filterMerchantID(true);

                agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("User can see validation message for invalid filtered data.");
                if (agentTransactionHistoryVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.filterMerchantID(false);

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify Merchant ID filter successfully.");
            if (agentTransactionHistoryVerification.verifyMerchantIDFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clearMerchantIDField();

            if (!isPositiveExecution) {
                agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.enterTopUpAmountRange(true);

                agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("Verify validation message for Max amount is less than Min amount.");
                if (agentTransactionHistoryVerification.verifyMaxMinTopupValidation()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clearMerchantTopupAmountRange();

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.enterTopUpAmount("from");

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify user can filter by min amount.");
            if (agentTransactionHistoryVerification.verifyTopUpFromFieldFilter()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clearMerchantTopupAmountRange();

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.enterTopUpAmount("to");

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify user can filter by max amount.");
            if (agentTransactionHistoryVerification.verifyTopUpToFieldFilter()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clearMerchantTopupAmountRange();

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.enterTopUpAmountRange(false);

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify user can filter with From and To Amount.");
            if (agentTransactionHistoryVerification.verifyTopUpMaxMinFilter()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clearMerchantTopupAmountRange();

            if (!isPositiveExecution) {
                agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.enterCommissionAmountRange(true);

                agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("Verify validation message for Max amount is less than Min amount.");
                if (agentTransactionHistoryVerification.verifyMaxMinCommissionValidation()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clearMerchantCommissionAmountRange();

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.enterCommissionAmount("from");

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify user can filter by min amount.");
            if (agentTransactionHistoryVerification.verifyCommissionFromFieldFilter()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clearMerchantCommissionAmountRange();

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.enterCommissionAmount("to");

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify user can filter by max amount.");
            if (agentTransactionHistoryVerification.verifyCommissionToFieldFilter()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clearMerchantCommissionAmountRange();

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.enterCommissionAmountRange(false);

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify user can filter with From and To Amount.");
            if (agentTransactionHistoryVerification.verifyCommissionMaxMinFilter()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void agent_TransactionDetailsVerification() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_034 :: To verify Agent can see the Transaction History in the screen properly.");
        agentLoginIndexPage.getVersion();

        agentLoginVerification = agentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (agentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickOnTransactionHistory();

        if (agentTransactionHistoryIndexPage.isTransactionDataDisplay()) {

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.searchUser("Consumer");

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.getAnyConsumerDetails();

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickOnMOREButton();

            testVerifyLog("Verify Transaction details display same in pop up screen.");
            if (agentTransactionHistoryVerification.verifyTransactionDetails("Consumer")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickOnClosePopUp();

            agentDashboardVerification = agentDashboardIndexPage.clickOnDashboardIcon();

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickOnTransactionHistory();

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.searchUser("Merchant");

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.getAnyConsumerDetails();

            agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickOnMOREButton();

            testVerifyLog("Verify Transaction details display same in pop up screen.");
            if (agentTransactionHistoryVerification.verifyTransactionDetails("Merchant")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

}
