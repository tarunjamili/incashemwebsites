package com.incashme.agent.index;

import com.framework.init.SeleniumInit;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Rahul R.
 * Date: 2019-04-26
 * Time: 16:20
 * Project Name: InCashMe
 */
public class AgentCommissionIndex extends SeleniumInit {

    @Test
    public void agent_TotalCommissionCalculation() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_039 :: To verify Agent can see the list of received commission on the screen with valid commission amount for Merchant" +
                "<br>ICM_SC_041 :: To verify Agent can see the list of received commission on the screen with valid commission amount for Consumer");

        agentLoginIndexPage.getVersion();

        agentLoginVerification = agentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (agentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify user can see the Dashboard screen with details.");
        if (agentDashboardVerification.verifyDashboardDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.clickOnCommissionMenu();

        testVerifyLog("Verify user can see the Commission Screen.");
        if (agentCommissionVerification.verifyCommissionScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentCommissionVerification = agentCommissionIndexPage.clickOnTotalButton();

        agentCommissionVerification = agentCommissionIndexPage.getTotalCommissionAmount();

        agentCommissionVerification = agentCommissionIndexPage.clickOnFilterIcon();

        if (agentCommissionIndexPage.isConsumerCommissionDataDisplay()) {
            testVerifyLog("Verify consumer commission calculated properly for the top up.");
            if (agentCommissionVerification.verifyConsumerCommissionCalculation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentCommissionVerification = agentCommissionIndexPage.clickOnMerchantTab();

        if (agentCommissionIndexPage.isMerchantCommissionDataDisplay()) {
            testVerifyLog("Verify merchant commission calculated properly for the top up.");
            if (agentCommissionVerification.verifyMerchantCommissionCalculation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        testVerifyLog("Verify user can see the commission amount total properly.");
        if (agentCommissionVerification.verifyTotalConsumerCommissionAmount()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void agent_CommissionSortAndFilter() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_040 :: To verify Agent can sort and Filter the Commission details for Merchant.<br>" +
                "ICM_SC_042 :: To verify Agent can sort and Filter the Commission details for Consumer.");

        agentLoginIndexPage.getVersion();

        agentLoginVerification = agentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (agentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.clickOnCommissionMenu();

        if (agentCommissionIndexPage.isConsumerCommissionDataDisplay()) {

            agentCommissionVerification = agentCommissionIndexPage.clickConsumerTopUpToSort();

            testVerifyLog("Verify Top ups are sorted successfully.");
            if (agentCommissionVerification.verifyTopUpsSorted("Consumer")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentCommissionVerification = agentCommissionIndexPage.clickConsumerCommissionToSort();

            testVerifyLog("Verify Commission are sorted successfully.");
            if (agentCommissionVerification.verifyCommissionSorted("Consumer")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentCommissionVerification = agentCommissionIndexPage.clickOnConsumerCommissionDateTimeToSort();

            testVerifyLog("Verify Date & Time are sorted successfully.");
            if (agentCommissionVerification.verifyCommissionDateTimeSorted("Consumer")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentDashboardVerification = agentDashboardIndexPage.clickOnDashboardIcon();

            agentDashboardVerification = agentDashboardIndexPage.clickOnCommissionMenu();

            agentCommissionVerification = agentCommissionIndexPage.clickOnFilterIcon();

            testVerifyLog("Verify user can see the Filter screen.");
            if (agentCommissionVerification.verifyConsumerFilterScreenDisplay()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentCommissionVerification = agentCommissionIndexPage.getConsumerDetailsForFilter();

            if (!isPositiveExecution) {
                agentCommissionVerification = agentCommissionIndexPage.filterFirstName(true);

                agentCommissionVerification = agentCommissionIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("User can see validation message for invalid filtered data.");
                if (agentCommissionVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            agentCommissionVerification = agentCommissionIndexPage.filterFirstName(false);

            agentCommissionVerification = agentCommissionIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify First Name filter successfully.");
            if (agentCommissionVerification.verifyFirstNameFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            if (!isPositiveExecution) {
                agentCommissionVerification = agentCommissionIndexPage.clearAllConsumerFilterDetails();

                agentCommissionVerification = agentCommissionIndexPage.filterLastName(true);

                agentCommissionVerification = agentCommissionIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("User can see validation message for invalid filtered data.");
                if (agentCommissionVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            agentCommissionVerification = agentCommissionIndexPage.filterLastName(false);

            agentCommissionVerification = agentCommissionIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify Last Name filter successfully.");
            if (agentCommissionVerification.verifyLastNameFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentCommissionVerification = agentCommissionIndexPage.clearAllConsumerFilterDetails();

            if (!isPositiveExecution) {
                agentCommissionVerification = agentCommissionIndexPage.filterConsumerNumber(true);

                agentCommissionVerification = agentCommissionIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("User can see validation message for invalid filtered data.");
                if (agentCommissionVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            agentCommissionVerification = agentCommissionIndexPage.filterConsumerNumber(false);

            agentCommissionVerification = agentCommissionIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify Mobile Number filter successfully.");
            if (agentCommissionVerification.verifyPhoneNumberFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentCommissionVerification = agentCommissionIndexPage.clearAllConsumerFilterDetails();

            if (!isPositiveExecution) {
                agentCommissionVerification = agentCommissionIndexPage.enterTopUpAmountRange(true);

                agentCommissionVerification = agentCommissionIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("Verify validation message for Max amount is less than Min amount.");
                if (agentCommissionVerification.verifyMaxMinTopupValidation()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            agentCommissionVerification = agentCommissionIndexPage.clearAllConsumerFilterDetails();

            agentCommissionVerification = agentCommissionIndexPage.enterTopUpAmount("from");

            agentCommissionVerification = agentCommissionIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify user can filter by min amount.");
            if (agentCommissionVerification.verifyTopUpFromFieldFilter("Consumer")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentCommissionVerification = agentCommissionIndexPage.clearAllConsumerFilterDetails();

            agentCommissionVerification = agentCommissionIndexPage.enterTopUpAmount("to");

            agentCommissionVerification = agentCommissionIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify user can filter by max amount.");
            if (agentCommissionVerification.verifyTopUpToFieldFilter("Consumer")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentCommissionVerification = agentCommissionIndexPage.clearAllConsumerFilterDetails();

            agentCommissionVerification = agentCommissionIndexPage.enterTopUpAmountRange(false);

            agentCommissionVerification = agentCommissionIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify user can filter with From and To Amount.");
            if (agentCommissionVerification.verifyTopUpMaxMinFilter("Consumer")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentCommissionVerification = agentCommissionIndexPage.clearAllConsumerFilterDetails();

            if (!isPositiveExecution) {
                agentCommissionVerification = agentCommissionIndexPage.enterCommissionAmountRange(true);

                agentCommissionVerification = agentCommissionIndexPage.clickConsumerOnFilterSearchButton();

                testVerifyLog("Verify validation message for Max amount is less than Min amount.");
                if (agentCommissionVerification.verifyMaxMinCommissionValidation()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            agentCommissionVerification = agentCommissionIndexPage.clearAllConsumerFilterDetails();

            agentCommissionVerification = agentCommissionIndexPage.enterCommissionAmount("from");

            agentCommissionVerification = agentCommissionIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify user can filter by min amount.");
            if (agentCommissionVerification.verifyCommissionFromFieldFilter("Consumer")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentCommissionVerification = agentCommissionIndexPage.clearAllConsumerFilterDetails();

            agentCommissionVerification = agentCommissionIndexPage.enterCommissionAmount("to");

            agentCommissionVerification = agentCommissionIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify user can filter by max amount.");
            if (agentCommissionVerification.verifyCommissionToFieldFilter("Consumer")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentCommissionVerification = agentCommissionIndexPage.clearAllConsumerFilterDetails();

            agentCommissionVerification = agentCommissionIndexPage.enterCommissionAmountRange(false);

            agentCommissionVerification = agentCommissionIndexPage.clickConsumerOnFilterSearchButton();

            testVerifyLog("Verify user can filter with From and To Amount.");
            if (agentCommissionVerification.verifyCommissionMaxMinFilter("Consumer")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentCommissionVerification = agentCommissionIndexPage.clickOnMerchantTab();

        if (agentCommissionIndexPage.isMerchantCommissionDataDisplay()) {

            agentCommissionVerification = agentCommissionIndexPage.clickMerchantTopUpToSort();

            testVerifyLog("Verify Top ups are sorted successfully.");
            if (agentCommissionVerification.verifyTopUpsSorted("Merchant")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentCommissionVerification = agentCommissionIndexPage.clickMerchantCommissionToSort();

            testVerifyLog("Verify Commission are sorted successfully.");
            if (agentCommissionVerification.verifyCommissionSorted("Merchant")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentCommissionVerification = agentCommissionIndexPage.clickOnMerchantCommissionDateTimeToSort();

            testVerifyLog("Verify Date & Time are sorted successfully.");
            if (agentCommissionVerification.verifyCommissionDateTimeSorted("Merchant")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentDashboardVerification = agentDashboardIndexPage.clickOnDashboardIcon();

            agentDashboardVerification = agentDashboardIndexPage.clickOnCommissionMenu();

            agentCommissionVerification = agentCommissionIndexPage.clickOnFilterIcon();

            agentCommissionVerification = agentCommissionIndexPage.clickOnMerchantTab();

            testVerifyLog("Verify user can see the Filter screen.");
            if (agentCommissionVerification.verifyMerchantFilterScreenDisplay()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentCommissionVerification = agentCommissionIndexPage.getMerchantDetailsForFilter();

            if (!isPositiveExecution) {
                agentCommissionVerification = agentCommissionIndexPage.filterShopName(true);

                agentCommissionVerification = agentCommissionIndexPage.clickMerchantOnFilterSearchButton();

                testVerifyLog("User can see validation message for invalid filtered data.");
                if (agentCommissionVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            agentCommissionVerification = agentCommissionIndexPage.filterShopName(false);

            agentCommissionVerification = agentCommissionIndexPage.clickMerchantOnFilterSearchButton();

            testVerifyLog("Verify First Name filter successfully.");
            if (agentCommissionVerification.verifyShopNameFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentCommissionVerification = agentCommissionIndexPage.clearAllMerchantFilterDetails();

            if (!isPositiveExecution) {
                agentCommissionVerification = agentCommissionIndexPage.filterMerchantID(true);

                agentCommissionVerification = agentCommissionIndexPage.clickMerchantOnFilterSearchButton();

                testVerifyLog("User can see validation message for invalid filtered data.");
                if (agentCommissionVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            agentCommissionVerification = agentCommissionIndexPage.filterMerchantID(false);

            agentCommissionVerification = agentCommissionIndexPage.clickMerchantOnFilterSearchButton();

            testVerifyLog("Verify Mobile Number filter successfully.");
            if (agentCommissionVerification.verifyMerchantIDFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentCommissionVerification = agentCommissionIndexPage.clearAllMerchantFilterDetails();

            if (!isPositiveExecution) {
                agentCommissionVerification = agentCommissionIndexPage.enterTopUpAmountRange(true);

                agentCommissionVerification = agentCommissionIndexPage.clickMerchantOnFilterSearchButton();

                testVerifyLog("Verify validation message for Max amount is less than Min amount.");
                if (agentCommissionVerification.verifyMaxMinTopupValidation()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            agentCommissionVerification = agentCommissionIndexPage.clearAllMerchantFilterDetails();

            agentCommissionVerification = agentCommissionIndexPage.enterTopUpAmount("from");

            agentCommissionVerification = agentCommissionIndexPage.clickMerchantOnFilterSearchButton();

            testVerifyLog("Verify user can filter by min amount.");
            if (agentCommissionVerification.verifyTopUpFromFieldFilter("Merchant")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentCommissionVerification = agentCommissionIndexPage.clearAllMerchantFilterDetails();

            agentCommissionVerification = agentCommissionIndexPage.enterTopUpAmount("to");

            agentCommissionVerification = agentCommissionIndexPage.clickMerchantOnFilterSearchButton();

            testVerifyLog("Verify user can filter by max amount.");
            if (agentCommissionVerification.verifyTopUpToFieldFilter("Merchant")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentCommissionVerification = agentCommissionIndexPage.clearAllMerchantFilterDetails();

            agentCommissionVerification = agentCommissionIndexPage.enterTopUpAmountRange(false);

            agentCommissionVerification = agentCommissionIndexPage.clickMerchantOnFilterSearchButton();

            testVerifyLog("Verify user can filter with From and To Amount.");
            if (agentCommissionVerification.verifyTopUpMaxMinFilter("Merchant")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentCommissionVerification = agentCommissionIndexPage.clearAllMerchantFilterDetails();

            if (!isPositiveExecution) {
                agentCommissionVerification = agentCommissionIndexPage.enterCommissionAmountRange(true);

                agentCommissionVerification = agentCommissionIndexPage.clickMerchantOnFilterSearchButton();

                testVerifyLog("Verify validation message for Max amount is less than Min amount.");
                if (agentCommissionVerification.verifyMaxMinCommissionValidation()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            agentCommissionVerification = agentCommissionIndexPage.clearAllMerchantFilterDetails();

            agentCommissionVerification = agentCommissionIndexPage.enterCommissionAmount("from");

            agentCommissionVerification = agentCommissionIndexPage.clickMerchantOnFilterSearchButton();

            testVerifyLog("Verify user can filter by min amount.");
            if (agentCommissionVerification.verifyCommissionFromFieldFilter("Merchant")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentCommissionVerification = agentCommissionIndexPage.clearAllMerchantFilterDetails();

            agentCommissionVerification = agentCommissionIndexPage.enterCommissionAmount("to");

            agentCommissionVerification = agentCommissionIndexPage.clickMerchantOnFilterSearchButton();

            testVerifyLog("Verify user can filter by max amount.");
            if (agentCommissionVerification.verifyCommissionToFieldFilter("Merchant")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentCommissionVerification = agentCommissionIndexPage.clearAllMerchantFilterDetails();

            agentCommissionVerification = agentCommissionIndexPage.enterCommissionAmountRange(false);

            agentCommissionVerification = agentCommissionIndexPage.clickMerchantOnFilterSearchButton();

            testVerifyLog("Verify user can filter with From and To Amount.");
            if (agentCommissionVerification.verifyCommissionMaxMinFilter("Merchant")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void agent_CommissionSearchForTables() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_062 :: To verify Agent can search the Commission Details.");
        agentLoginIndexPage.getVersion();

        agentLoginVerification = agentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (agentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.clickOnCommissionMenu();

        if (agentCommissionIndexPage.isConsumerCommissionDataDisplay()) {

            agentCommissionVerification = agentCommissionIndexPage.enterConsumerSearchIDCriteria(false);

            testVerifyLog("Verify search details display properly.");
            if (agentCommissionVerification.verifySearchSuccessful("Consumer")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            if (!isPositiveExecution) {
                agentCommissionVerification = agentCommissionIndexPage.enterConsumerSearchIDCriteria(true);

                testVerifyLog("Verify validation message display for the invalid search criteria.");
                if (agentCommissionVerification.verifyValidationForConsumerInvalidSearch()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

        }

        agentCommissionVerification = agentCommissionIndexPage.clickOnMerchantTab();

        if (agentCommissionIndexPage.isMerchantCommissionDataDisplay()) {

            agentCommissionVerification = agentCommissionIndexPage.enterMerchantSearchIDCriteria(false);

            testVerifyLog("Verify search details display properly.");
            if (agentCommissionVerification.verifySearchSuccessful("Merchant")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            if (!isPositiveExecution) {
                agentCommissionVerification = agentCommissionIndexPage.enterMerchantSearchIDCriteria(true);

                testVerifyLog("Verify validation message display for the invalid search criteria.");
                if (agentCommissionVerification.verifyValidationForMerchantInvalidSearch()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");

    }

}
