package com.incashme.agent.index;

import com.framework.init.SeleniumInit;
import com.incashme.agent.indexpage.AgentDashboardIndexPage;
import com.incashme.agent.indexpage.AgentLoginIndexPage;
import com.incashme.agent.indexpage.AgentTargetTopUpDetailsIndexPage;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Rahul R.
 * Date: 2019-04-11
 * Time: 11:36
 * Project Name: InCashMe
 */
public class AgentDashboardIndex extends SeleniumInit {

    @Test
    public void agent_DashboardMenuVerification() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_026 :: To verify Dashboard Screen and Menu Links.<br> " +
                "ICM_SC_030 :: To verify available balance is same as balance. <br>" +
                "ICM_SC_038 :: To verify Agent can see the Balance on the screen.");
        agentLoginIndexPage.getVersion();

        agentLoginVerification = agentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (agentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify user can see the Dashboard screen with details.");
        if (agentDashboardVerification.verifyDashboardDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.getCurrentBalance();

        testVerifyLog("Verify user can see the Dashboard screen buttons.");
        if (agentDashboardVerification.verifyDashboardButtons()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.expandDashboardMenu();
        agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickOnTransactionHistory();

        testVerifyLog("Verify user can see the Transaction History Screen.");
        if (agentTransactionHistoryVerification.verifyTransactionHistoryScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.clickOnStatementMenu();

        testVerifyLog("Verify user can see the Statement Screen with list of statements.");
        if (agentStatementVerification.verifyStatementsScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.clickOnBalanceMenu();

        testVerifyLog("Verify user can see the Balance Screen with current balance.");
        if (agentDashboardVerification.verifyBalanceScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.clickOnCommissionMenu();

        testVerifyLog("Verify user can see the Commission Screen.");
        if (agentCommissionVerification.verifyCommissionScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.clickOnUsersListMenu();

        testVerifyLog("Verify user can see the Users List Screen.");
        if (agentUserListVerification.verifyUsersListScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.clickOnDashboardIcon();
        agentDashboardVerification = agentDashboardIndexPage.clickOnCreateConsumer();

        testVerifyLog("Verify user can see the Create Consumer Step 1.");
        if (agentUserListVerification.verifyCreateConsumerScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.clickOnDashboardIcon();
        agentDashboardVerification = agentDashboardIndexPage.clickOnCreateMerchant();

        testVerifyLog("Verify user can see the Create Merchant Step 1.");
        if (agentUserListVerification.verifyCreateMerchantScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");

    }

    @Test
    public void agent_ProfileDetailsVerification() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_027 :: To verify Agent profile details and profile links.");
        agentLoginIndexPage.getVersion();

        agentLoginVerification = agentLoginIndexPage.loginAs(username, password);

        AgentLoginIndexPage.getLoginTime();

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (agentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.openUserProfile();

        testVerifyLog("Verify user can see the profile details on the screen.");
        if (agentDashboardVerification.verifyUserProfileDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.closeProfilePopUp();
        agentLoginVerification = agentLoginIndexPage.clickOnUserName();
        agentDashboardVerification = agentDashboardIndexPage.clickOnChangePassword();

        testVerifyLog("Verify user can see the Change Password screen.");
        if (agentDashboardVerification.verifyChangePasswordScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.closePasswordPopUp();
        agentDashboardVerification = agentDashboardIndexPage.clickOnPrivacyPolicy();

        testVerifyLog("Verify user can see the Privacy Policy screen.");
        if (agentDashboardVerification.verifyPrivacyPolicyScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.clickOnTermsCondition();

        testVerifyLog("Verify user can see the Terms & Condition screen.");
        if (agentDashboardVerification.verifyTnCScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.clickOnKYCAMLPolicy();

        testVerifyLog("Verify user can see the KYC & AML Policy screen.");
        if (agentDashboardVerification.verifyKYCandAMLScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.clickOnLoginHistory();

        testVerifyLog("Verify user can see the Login History details.");
        if (agentDashboardVerification.verifyLoginHistory()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentLoginVerification = agentLoginIndexPage.clickOnLogout();

        testVerifyLog("Verify user logout successfully from the Application.");
        if (agentLoginVerification.verifyUserLogoutSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }


        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void agent_TodayActivityDetails() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_032 :: To verify today's activity details display properly.");
        agentLoginIndexPage.getVersion();

        agentLoginVerification = agentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (agentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify user can see the Dashboard screen with details.");
        if (agentDashboardVerification.verifyDashboardDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.getTodayAgentDetails();

        testVerifyLog("Verify agent can see the Topup Commission Details on the Dashboard.");
        if (agentDashboardVerification.verifyCommissionDetailsOnDashboard()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (agentDashboardIndexPage.isCommissionDataDisplay()) {
            testVerifyLog("Verify user can see the commission amount total properly.");
            if (agentDashboardVerification.verifyTotalCommissionAmount()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            testVerifyLog("Verify commission calculated properly for the top up.");
            if (agentDashboardVerification.verifyCommissionCalculation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        } else {
            testWarningLog("No commission display on the screen to verify Calculation, skipping calculation part.");
        }

        agentDashboardVerification = agentDashboardIndexPage.openTopUpUserList();

        testVerifyLog("Verify list of completed topups display on the screen.");
        if (agentDashboardVerification.verifyTodayCompletedTopupsDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify topus in popup are display same to Dashboard.");
        if (agentDashboardVerification.verifyTopUpDetailsSameAsDashboard()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.clickOnCloseTopUpList();

        agentDashboardVerification = agentDashboardIndexPage.openAddedConsumerList();

        testVerifyLog("Verify list of today added consumers display on the screen.");
        if (agentDashboardVerification.verifyTodayAddedConsumerDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.clickOnCloseConsumersList();

        agentDashboardVerification = agentDashboardIndexPage.openAddedMerchantList();

        testVerifyLog("Verify list of today added merchants display on the screen.");
        if (agentDashboardVerification.verifyTodayAddedMerchantDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.clickOnCloseMerchantsList();


        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    //Change Password remaining only
    @Test(enabled = false)
    public void agent_ChangePassword() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_028 :: To verify Agent can change password successfully.");
        agentLoginIndexPage.getVersion();

        agentLoginVerification = agentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (agentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentLoginVerification = agentLoginIndexPage.clickOnUserName();
        agentDashboardVerification = agentDashboardIndexPage.clickOnChangePassword();

        testVerifyLog("Verify user can see the Change Password screen.");
        if (agentDashboardVerification.verifyChangePasswordScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.changeInvalidCurrentPassword();

        testVerifyLog("Verify validation message for the invalid current password.");
        if (agentDashboardVerification.verifyInvalidCurrentPasswordValidation()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.changeInvalidNewPassword();

        testVerifyLog("Verify validation message for the invalid new password.");
        if (agentDashboardVerification.verifyInvalidNewPasswordValidation()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.closePasswordPopUp();
        agentDashboardVerification = agentDashboardIndexPage.clickOnChangePassword();
        agentDashboardVerification = agentDashboardIndexPage.changeInvalidConfirmPassword();

        testVerifyLog("Verify validation message for the different new and confirm password.");
        if (agentDashboardVerification.verifyInvalidConfirmPasswordValidation()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.changeAsPastPassword();

        testVerifyLog("Verify validation message for the past password as new password.");
        if (agentDashboardVerification.verifyPastPasswordValidation()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

       /* superAgentDashboardVerification = superAgentDashboardIndexPage.changeNewPassword();

        testVerifyLog("Verify confirmation message for the Password changed successfully.");
        if (superAgentDashboardVerification.verifyPasswordChangeSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify user logout successfully from the Application.");
        if (superAgentLoginVerification.verifyUserLogoutSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentLoginVerification = superAgentLoginIndexPage.loginAs("testsuperagent121@mailinator.com", "Nagesh@2");

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (superAgentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentLoginVerification = superAgentLoginIndexPage.clickOnUserName();
        superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnChangePassword();

        testVerifyLog("Verify user can see the Change Password screen.");
        if (superAgentDashboardVerification.verifyChangePasswordScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }*/

        agentDashboardVerification = agentDashboardIndexPage.changeInvalidCurrentPassword();

        testVerifyLog("Verify validation message for the invalid current password.");
        if (agentDashboardVerification.verifyInvalidCurrentPasswordSecondTime()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.changeInvalidCurrentPassword();

        testVerifyLog("Verify user account blocked after three invalid attempts.");
        if (agentDashboardVerification.verifyAccountIsBlockedForInvalidPassword()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify user logout successfully from the Application.");
        if (agentLoginVerification.verifyUserLogoutSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentLoginVerification = agentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can see the account locked validation.");
        if (agentLoginVerification.verifyUserLockedSuccessfullyForWrongPassword()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    //PIN Change remaining only
    @Test(enabled = false)
    public void agent_ChangePIN() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_029 :: To verify Agent can change PIN successfully.");
        agentLoginIndexPage.getVersion();

        agentLoginVerification = agentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (agentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentLoginVerification = agentLoginIndexPage.clickOnUserName();
        agentDashboardVerification = agentDashboardIndexPage.clickOnChangePIN();

        testVerifyLog("Verify user can see the Change PIN screen.");
        if (agentDashboardVerification.verifyChangePINScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.changeInvalidCurrentPIN();

        testVerifyLog("Verify validation message for the invalid current PIN.");
        if (agentDashboardVerification.verifyInvalidCurrentPINValidation()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.changeInvalidNewPIN();

        testVerifyLog("Verify validation message for the invalid new PIN.");
        if (agentDashboardVerification.verifyInvalidNewPINValidation()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.changeInvalidConfirmPIN();

        testVerifyLog("Verify validation message for the different new and confirm testData.");
        if (agentDashboardVerification.verifyInvalidConfirmPinValidation()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.changeAsPastPin();

        testVerifyLog("Verify validation message for the past testData as new testData.");
        if (agentDashboardVerification.verifyPastPinValidation()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

       /* superAgentDashboardVerification = superAgentDashboardIndexPage.changeNewPassword();

        testVerifyLog("Verify confirmation message for the Password changed successfully.");
        if (superAgentDashboardVerification.verifyPasswordChangeSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify user logout successfully from the Application.");
        if (superAgentLoginVerification.verifyUserLogoutSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentLoginVerification = superAgentLoginIndexPage.loginAs("testsuperagent121@mailinator.com", "Nagesh@2");

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (superAgentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentLoginVerification = superAgentLoginIndexPage.clickOnUserName();
        superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnChangePassword();

        testVerifyLog("Verify user can see the Change Password screen.");
        if (superAgentDashboardVerification.verifyChangePasswordScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }*/

        agentDashboardVerification = agentDashboardIndexPage.changeInvalidCurrentPIN();

        testVerifyLog("Verify validation message for the invalid current PIN.");
        if (agentDashboardVerification.verifyInvalidCurrentPINSecondTime()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.changeInvalidCurrentPIN();

        testVerifyLog("Verify user account blocked after three invalid attempts.");
        if (agentDashboardVerification.verifyAccountIsBlockedForInvalidPIN()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify user logout successfully from the Application.");
        if (agentLoginVerification.verifyUserLogoutSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentLoginVerification = agentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can see the account locked validation.");
        if (agentLoginVerification.verifyUserLockedSuccessfullyForWrongPIN()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void agent_TopUpDetailsVerification() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_031 :: To verify Agent can see the Consumer TopUp status details.");
        agentLoginIndexPage.getVersion();

        agentLoginVerification = agentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (agentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify user can see the Dashboard screen with details.");
        if (agentDashboardVerification.verifyDashboardDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.getConsumerAndMerchantCount();

        agentDashboardVerification = agentDashboardIndexPage.getTodayAgentDetails();

        agentDashboardVerification = agentDashboardIndexPage.clickOnAddUserIcon();

        agentDashboardVerification = agentDashboardIndexPage.getConsumersListFromAddUsers();

        agentDashboardVerification = agentDashboardIndexPage.clickOnCloseAddUser();

        agentDashboardVerification = agentDashboardIndexPage.clickOnTopUpSection();

        agentTargetTopUpDetailsVerification = agentTargetTopUpDetailsIndexPage.getTotalTargetTopupDetails();

        testVerifyLog("Verify Topup Target Details display properly.");
        if (agentTargetTopUpDetailsVerification.verifyTopupAgentTargetDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify Agent Target Details Screen Display.");
        if (agentTargetTopUpDetailsVerification.verifyConsumerDetailsList()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void agent_ConsumerDetailsVerification() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_049 :: To verify Agent can see the Consumer TopUp status details.");
        agentLoginIndexPage.getVersion();

        agentLoginVerification = agentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (agentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify user can see the Dashboard screen with details.");
        if (agentDashboardVerification.verifyDashboardDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.getConsumerAndMerchantCount();

        agentDashboardVerification = agentDashboardIndexPage.getTodayAgentDetails();

        agentDashboardVerification = agentDashboardIndexPage.clickOnConsumerSection();

        agentConsumerTargetDetailsVerification = agentConsumerTargetDetailsIndexPage.getConsumerDetails();

        testVerifyLog("Verify Consumer Target Details Screen Display.");
        if (agentConsumerTargetDetailsVerification.verifyConsumerTargetDetailsScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentConsumerTargetDetailsVerification = agentConsumerTargetDetailsIndexPage.getTotalConsumerFromChart();

        testVerifyLog("Verify Consumer List count is display properly.");
        if (agentConsumerTargetDetailsVerification.verifyConsumerListCount()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentConsumerTargetDetailsVerification = agentConsumerTargetDetailsIndexPage.getTotalConsumersDetails();

        if (agentConsumerTargetDetailsIndexPage.isConsumerListDisplay()) {

            agentConsumerTargetDetailsVerification = agentConsumerTargetDetailsIndexPage.clickOnConsumerMOREButton();

            testVerifyLog("Verify consumer profile details screen open.");
            if (agentUserListVerification.verifyConsumerDetailsScreenDisplay()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.clickOnBackButton();

            agentConsumerTargetDetailsVerification = agentConsumerTargetDetailsIndexPage.clickOnConsumerListButton();

            testVerifyLog("Verify list of added consumer display on the screen.");
            if (agentConsumerTargetDetailsVerification.verifyTotalAddedConsumerDetails()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void agent_MerchantDetailsVerification() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_050 :: To verify Agent can see the Merchant TopUp status details.");
        agentLoginIndexPage.getVersion();

        agentLoginVerification = agentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (agentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify user can see the Dashboard screen with details.");
        if (agentDashboardVerification.verifyDashboardDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.getConsumerAndMerchantCount();

        agentDashboardVerification = agentDashboardIndexPage.getTodayAgentDetails();

        agentDashboardVerification = agentDashboardIndexPage.clickOnMerchantSection();

        agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.getMerchantDetails();

        testVerifyLog("Verify Merchant Target Details Screen Display.");
        if (agentMerchantTargetDetailsVerification.verifyMerchantTargetDetailsScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.getTotalMerchantFromChart();

        testVerifyLog("Verify Merchant List count is display properly.");
        if (agentMerchantTargetDetailsVerification.verifyMerchantListCount()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.getTotalMerchantDetails();

        if (agentMerchantTargetDetailsIndexPage.isMerchantListDisplay()) {

            agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.clickOnMerchantMOREButton();

            testVerifyLog("Verify merchant profile details screen open.");
            if (agentUserListVerification.verifyMerchantDetailsScreenDisplay()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.clickOnBackButton();

            agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.clickOnMerchantListButton();

            testVerifyLog("Verify list of added merchants display on the screen.");
            if (agentMerchantTargetDetailsVerification.verifyTotalAddedMerchantDetails()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void agent_DashboardTableActions() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_051 :: To verify Agent can search and sort the table details in the Dashboard.");
        agentLoginIndexPage.getVersion();

        agentLoginVerification = agentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (agentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify user can see the Dashboard screen with details.");
        if (agentDashboardVerification.verifyDashboardDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.getConsumerAndMerchantCount();

        agentDashboardVerification = agentDashboardIndexPage.getTodayAgentDetails();

        testVerifyLog("Verify agent can see the Topup Commission Details on the Dashboard.");
        if (agentDashboardVerification.verifyCommissionDetailsOnDashboard()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (agentDashboardIndexPage.isCommissionDataDisplay()) {

            agentDashboardVerification = agentDashboardIndexPage.clickTopUpToSort();

            testVerifyLog("Verify Top ups are sorted successfully.");
            if (agentDashboardVerification.verifyTopUpsSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentDashboardVerification = agentDashboardIndexPage.clickCommissionToSort();

            testVerifyLog("Verify Commission are sorted successfully.");
            if (agentDashboardVerification.verifyCommissionSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentDashboardVerification = agentDashboardIndexPage.clickOnCommissionDateTimeToSort();

            testVerifyLog("Verify Date & Time are sorted successfully.");
            if (agentDashboardVerification.verifyCommissionDateTimeSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentDashboardVerification = agentDashboardIndexPage.enterSearchCriteria(false);

            testVerifyLog("Verify search details display properly.");
            if (agentDashboardVerification.verifySearchSuccessful()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            if (!isPositiveExecution) {
                agentDashboardVerification = agentDashboardIndexPage.enterSearchCriteria(true);

                testVerifyLog("Verify validation message display for the invalid search criteria.");
                if (agentDashboardVerification.verifyValidationForInvalidSearch()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void agent_TopUpDetailsTableActions() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_052 :: To verify Agent can search and sort the table details in the TopUp Details screen.");
        agentLoginIndexPage.getVersion();

        agentLoginVerification = agentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (agentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify user can see the Dashboard screen with details.");
        if (agentDashboardVerification.verifyDashboardDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.getConsumerAndMerchantCount();

        agentDashboardVerification = agentDashboardIndexPage.clickOnAddUserIcon();

        agentDashboardVerification = agentDashboardIndexPage.getConsumersListFromAddUsers();

        agentDashboardVerification = agentDashboardIndexPage.clickOnCloseAddUser();

        agentDashboardVerification = agentDashboardIndexPage.clickOnTopUpSection();

        if (agentConsumerTargetDetailsIndexPage.isConsumerListDisplay()) {

            agentTargetTopUpDetailsVerification = agentTargetTopUpDetailsIndexPage.clickTopUpConsumerNameToSort();

            testVerifyLog("Verify Consumer Name are sorted successfully.");
            if (agentTargetTopUpDetailsVerification.verifyTopUpConsumerNameSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentTargetTopUpDetailsVerification = agentTargetTopUpDetailsIndexPage.clickTopUpConsumerNumberToSort();

            testVerifyLog("Verify Consumer Mobile Number are sorted successfully.");
            if (agentTargetTopUpDetailsVerification.verifyTopUpConsumerNumberSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentTargetTopUpDetailsVerification = agentTargetTopUpDetailsIndexPage.enterTopUpConsumerSearchCriteria(false);

            testVerifyLog("Verify search details display properly.");
            if (agentTargetTopUpDetailsVerification.verifyTopUpConsumerSearchSuccessful()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            if (!isPositiveExecution) {
                agentTargetTopUpDetailsVerification = agentTargetTopUpDetailsIndexPage.enterTopUpConsumerSearchCriteria(true);

                testVerifyLog("Verify validation message display for the invalid search criteria.");
                if (agentTargetTopUpDetailsVerification.verifyTopUpValidationForInvalidSearch()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

        }


        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void agent_ConsumerDetailsTableActions() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_053 :: To verify Agent can search and sort the table details in the Consumer Target Details screen.");
        agentLoginIndexPage.getVersion();

        agentLoginVerification = agentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (agentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify user can see the Dashboard screen with details.");
        if (agentDashboardVerification.verifyDashboardDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.getConsumerAndMerchantCount();

        agentDashboardVerification = agentDashboardIndexPage.clickOnConsumerSection();

        agentConsumerTargetDetailsVerification = agentConsumerTargetDetailsIndexPage.getTotalConsumerFromChart();

        if (agentConsumerTargetDetailsIndexPage.isConsumerListDisplay()) {

            agentConsumerTargetDetailsVerification = agentConsumerTargetDetailsIndexPage.clickConsumerNameToSort();

            testVerifyLog("Verify Consumer Name are sorted successfully.");
            if (agentConsumerTargetDetailsVerification.verifyConsumerNameSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentConsumerTargetDetailsVerification = agentConsumerTargetDetailsIndexPage.clickConsumerNumberToSort();

            testVerifyLog("Verify Consumer Mobile Number are sorted successfully.");
            if (agentConsumerTargetDetailsVerification.verifyConsumerNumberSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentConsumerTargetDetailsVerification = agentConsumerTargetDetailsIndexPage.clickOnConsumerStatusToSort();

            testVerifyLog("Verify Consumer Status are sorted successfully.");
            if (agentConsumerTargetDetailsVerification.verifyConsumerStatusSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentConsumerTargetDetailsVerification = agentConsumerTargetDetailsIndexPage.clickOnConsumerDateTimeToSort();

            testVerifyLog("Verify Date & Time are sorted successfully.");
            if (agentConsumerTargetDetailsVerification.verifyConsumerDateTimeSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentConsumerTargetDetailsVerification = agentConsumerTargetDetailsIndexPage.enterConsumerSearchCriteria(false);

            testVerifyLog("Verify search details display properly.");
            if (agentConsumerTargetDetailsVerification.verifyConsumerSearchSuccessful()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            if (!isPositiveExecution) {
                agentConsumerTargetDetailsVerification = agentConsumerTargetDetailsIndexPage.enterConsumerSearchCriteria(true);

                testVerifyLog("Verify validation message display for the invalid search criteria.");
                if (agentConsumerTargetDetailsVerification.verifyConsumerValidationForInvalidSearch()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }
        }

        agentConsumerTargetDetailsVerification = agentConsumerTargetDetailsIndexPage.clickOnConsumerListButton();

        if (agentConsumerTargetDetailsIndexPage.isConsumerListDisplay()) {

            agentConsumerTargetDetailsVerification = agentConsumerTargetDetailsIndexPage.enterConsumerListSearchCriteria(false);

            testVerifyLog("Verify search details display properly.");
            if (agentConsumerTargetDetailsVerification.verifyListConsumerSearchSuccessful()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            if (!isPositiveExecution) {
                agentConsumerTargetDetailsVerification = agentConsumerTargetDetailsIndexPage.enterConsumerListSearchCriteria(true);

                testVerifyLog("Verify validation message display for the invalid search criteria.");
                if (agentConsumerTargetDetailsVerification.verifyValidationForListInvalidSearch()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }
        }


        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void agent_MerchantDetailsTableActions() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_054 :: To verify Agent can search and sort the table details in the Merchant Target Details screen.");
        agentLoginIndexPage.getVersion();

        agentLoginVerification = agentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (agentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify user can see the Dashboard screen with details.");
        if (agentDashboardVerification.verifyDashboardDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.getConsumerAndMerchantCount();

        agentDashboardVerification = agentDashboardIndexPage.getTodayAgentDetails();

        agentDashboardVerification = agentDashboardIndexPage.clickOnMerchantSection();

        agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.getMerchantDetails();

        testVerifyLog("Verify Merchant Target Details Screen Display.");
        if (agentMerchantTargetDetailsVerification.verifyMerchantTargetDetailsScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.getTotalMerchantFromChart();

        testVerifyLog("Verify Merchant List count is display properly.");
        if (agentMerchantTargetDetailsVerification.verifyMerchantListCount()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (agentMerchantTargetDetailsIndexPage.isMerchantListDisplay()) {

            agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.clickMerchantNameToSort();

            testVerifyLog("Verify Merchant Name are sorted successfully.");
            if (agentMerchantTargetDetailsVerification.verifyMerchantNameSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.clickMerchantNumberToSort();

            testVerifyLog("Verify Merchant Phone Number are sorted successfully.");
            if (agentMerchantTargetDetailsVerification.verifyMerchantNumberSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.clickOnMerchantStatusToSort();

            testVerifyLog("Verify Merchant Status are sorted successfully.");
            if (agentMerchantTargetDetailsVerification.verifyMerchantStatusSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.clickOnMerchantDateTimeToSort();

            testVerifyLog("Verify Date & Time are sorted successfully.");
            if (agentMerchantTargetDetailsVerification.verifyMerchantDateTimeSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.enterMerchantSearchCriteria(false);

            testVerifyLog("Verify search details display properly.");
            if (agentMerchantTargetDetailsVerification.verifyMerchantSearchSuccessful()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            if (!isPositiveExecution) {
                agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.enterMerchantSearchCriteria(true);

                testVerifyLog("Verify validation message display for the invalid search criteria.");
                if (agentMerchantTargetDetailsVerification.verifyMerchantValidationForInvalidSearch()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }
        }

        agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.clickOnMerchantListButton();

        if (agentMerchantTargetDetailsIndexPage.isMerchantListDisplay()) {

            agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.enterMerchantListSearchCriteria(false);

            testVerifyLog("Verify search details display properly.");
            if (agentMerchantTargetDetailsVerification.verifyListMerchantSearchSuccessful()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            if (!isPositiveExecution) {
                agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.enterMerchantListSearchCriteria(true);

                testVerifyLog("Verify validation message display for the invalid search criteria.");
                if (agentMerchantTargetDetailsVerification.verifyValidationForListInvalidSearch()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }
        }


        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void agent_DashboardAddUserTableActions() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_055 :: To verify Agent can search and sort the table details in the Add Users Details screen.");
        agentLoginIndexPage.getVersion();

        agentLoginVerification = agentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (agentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify user can see the Dashboard screen with details.");
        if (agentDashboardVerification.verifyDashboardDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.getConsumerAndMerchantCount();

        agentDashboardVerification = agentDashboardIndexPage.getTodayAgentDetails();

        agentDashboardVerification = agentDashboardIndexPage.clickOnAddUserIcon();

        agentDashboardVerification = agentDashboardIndexPage.getConsumersListFromAddUsers();

        if (agentConsumerTargetDetailsIndexPage.isConsumerListDisplay()) {

            agentDashboardVerification = agentDashboardIndexPage.enterAddUserConsumerListSearchCriteria(false);

            testVerifyLog("Verify search details display properly.");
            if (agentDashboardVerification.verifyConsumerListSearchSuccessful()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            if (!isPositiveExecution) {
                agentDashboardVerification = agentDashboardIndexPage.enterAddUserConsumerListSearchCriteria(true);

                testVerifyLog("Verify validation message display for the invalid search criteria.");
                if (agentDashboardVerification.verifyValidationForAddUserInvalidSearch()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }
        }

        agentDashboardVerification = agentDashboardIndexPage.clickOnCloseAddUser();

        agentDashboardVerification = agentDashboardIndexPage.clickOnAddUserIcon();

        agentDashboardVerification = agentDashboardIndexPage.clickOnMerchantTab();

        if (agentDashboardIndexPage.isAddUserMerchantListDisplay()) {

            agentDashboardVerification = agentDashboardIndexPage.enterAddUserMerchantListSearchCriteria(false);

            testVerifyLog("Verify search details display properly.");
            if (agentDashboardVerification.verifyMerchantListSearchSuccessful()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            if (!isPositiveExecution) {
                agentDashboardVerification = agentDashboardIndexPage.enterAddUserMerchantListSearchCriteria(true);

                testVerifyLog("Verify validation message display for the invalid search criteria.");
                if (agentDashboardVerification.verifyValidationForAddUserInvalidSearch()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }
        }


        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void agent_TopUpSuccessful() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_033 :: To verify Agent completes top up and receive commission.");
        agentLoginIndexPage.getVersion();

        agentLoginVerification = agentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (agentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify user can see the Dashboard screen with details.");
        if (agentDashboardVerification.verifyDashboardDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.getTopUpDetailsBeforeTopUp();

        agentDashboardVerification = agentDashboardIndexPage.clickOnAddUserIcon();

        if (agentDashboardIndexPage.kycPendingAddConsumerDisplay()) {

            agentDashboardVerification = agentDashboardIndexPage.clickOnNonKYCConsumer();

            testVerifyLog("Verify user see the Non KYC User TopUp Validation Message.");
            if (agentDashboardVerification.verifyNonKYCConsumerTopUpValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }


        } else {
            testWarningLog("No KYC Pending Consumer display in the list.");
        }

        agentDashboardVerification = agentDashboardIndexPage.clickOnMerchantTab();

        if (agentDashboardIndexPage.kycPendingAddMerchantDisplay()) {

            agentDashboardVerification = agentDashboardIndexPage.clickOnNonKYCMerchant();

            testVerifyLog("Verify user see the Non KYC User TopUp Validation Message.");
            if (agentDashboardVerification.verifyNonKYCConsumerTopUpValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

        } else {
            testWarningLog("No KYC Pending Merchant display in the list.");
        }

        agentDashboardVerification = agentDashboardIndexPage.clickOnCloseAddUser();

        agentDashboardVerification = agentDashboardIndexPage.enterUserMobileNumberForTopUp();

        testVerifyLog("Verify validation message for no user found.");
        if (agentDashboardVerification.verifyNoUserFoundForTopUp()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.clickOnAddUserIcon();

        agentDashboardVerification = agentDashboardIndexPage.clickOnConsumerTab();

        agentDashboardVerification = agentDashboardIndexPage.selectConsumerForTopUp();

        if (!isPositiveExecution) {
            agentDashboardVerification = agentDashboardIndexPage.enterZeroTopUpAmount();

            testVerifyLog("Verify validation message for zero topup amount.");
            if (agentDashboardVerification.verifyZeroTopUpAmountValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentDashboardVerification = agentDashboardIndexPage.enterInvalidTopUpAmount();

            agentDashboardVerification = agentDashboardIndexPage.clickOnCompleteTopUpButton();

            testVerifyLog("Verify validation message for invalid topup amount.");
            if (agentDashboardVerification.verifyInvalidTopUpAmountValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentDashboardVerification = agentDashboardIndexPage.enterMaxBalanceTopUpAmount();

            testVerifyLog("Verify validation message for insufficient topup amount.");
            if (agentDashboardVerification.verifyInsufficientBalanceTopUpValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }
        agentDashboardVerification = agentDashboardIndexPage.enterValidTopUpAmount();

        agentDashboardVerification = agentDashboardIndexPage.clickOnCompleteTopUpButton();

        agentDashboardVerification = agentDashboardIndexPage.clickOnCancelTopUpButton();

        agentDashboardVerification = agentDashboardIndexPage.clickOnCompleteTopUpButton();

        testVerifyLog("Verify confirmation message before completing topup.");
        if (agentDashboardVerification.verifyConsumerTopUpDetailsBeforeComplete()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.clickOnSendTopUpButton();

        testVerifyLog("Verify confirmation message after completing topup.");
        if (agentDashboardVerification.verifyConsumerTopUpDetailsAfterComplete()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.clickOnCloseTopUpButton();

        testVerifyLog("Verify completed topup display in the transaction history.");
        if (agentDashboardVerification.verifyCompletedConsumerTopUpInTodayTransactionHistory()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify balance updated after topup completed.");
        if (agentDashboardVerification.verifyBalanceAndTodayConsumerTopUpDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickOnTransactionHistory();

        agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.searchRecentTransaction();

        testVerifyLog("Verify completed topup display in today's topup table.");
        if (agentTransactionHistoryVerification.verifyCompletedTopUpDisplayOnScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.clickOnCommissionMenu();

        agentCommissionVerification = agentCommissionIndexPage.clickOnFilterIcon();

        agentCommissionVerification = agentCommissionIndexPage.searchRecentConsumerTransaction();

        testVerifyLog("Verify completed topup commission display in the commission screen.");
        if (agentCommissionVerification.verifyConsumerCommissionDisplay()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.clickOnDashboardIcon();

        agentDashboardVerification = agentDashboardIndexPage.getTopUpDetailsBeforeTopUp();

        agentDashboardVerification = agentDashboardIndexPage.clickOnAddUserIcon();

        agentDashboardVerification = agentDashboardIndexPage.clickOnMerchantTab();

        agentDashboardVerification = agentDashboardIndexPage.selectMerchantForTopUp();

        agentDashboardVerification = agentDashboardIndexPage.enterValidTopUpAmount();

        agentDashboardVerification = agentDashboardIndexPage.clickOnCompleteTopUpButton();

        agentDashboardVerification = agentDashboardIndexPage.clickOnCancelTopUpButton();

        agentDashboardVerification = agentDashboardIndexPage.clickOnCompleteTopUpButton();

        testVerifyLog("Verify confirmation message before completing topup.");
        if (agentDashboardVerification.verifyMerchantTopUpDetailsBeforeComplete()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.clickOnSendTopUpButton();

        testVerifyLog("Verify confirmation message after completing topup.");
        if (agentDashboardVerification.verifyMerchantTopUpDetailsAfterComplete()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.clickOnCloseTopUpButton();

        testVerifyLog("Verify completed topup display in the transaction history.");
        if (agentDashboardVerification.verifyCompletedMerchantTopUpInTodayTransactionHistory()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify balance updated after topup completed.");
        if (agentDashboardVerification.verifyBalanceAndTodayMerchantTopUpDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.clickOnTransactionHistory();

        agentTransactionHistoryVerification = agentTransactionHistoryIndexPage.searchRecentTransaction();

        testVerifyLog("Verify completed topup display in today's topup table.");
        if (agentTransactionHistoryVerification.verifyCompletedTopUpDisplayOnScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.clickOnCommissionMenu();

        agentCommissionVerification = agentCommissionIndexPage.clickOnFilterIcon();

        agentCommissionVerification = agentCommissionIndexPage.clickOnMerchantTab();

        agentCommissionVerification = agentCommissionIndexPage.searchRecentMerchantTransaction();

        testVerifyLog("Verify completed topup commission display in the commission screen.");
        if (agentCommissionVerification.verifyMerchantCommissionDisplay()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void agent_CancelCreateConsumer() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_056 :: To verify Agent can cancel Create Consumer scenario.");
        agentLoginIndexPage.getVersion();

        agentLoginVerification = agentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (agentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.getAddedConsumerCounts();

        agentDashboardVerification = agentDashboardIndexPage.clickOnCreateConsumer();

        testVerifyLog("Verify user can see the Create Consumer Step 1 Screen.");
        if (agentUserListVerification.verifyCreateConsumerScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentUserListVerification = agentUserListIndexPage.enterName("first name");

        agentUserListVerification = agentUserListIndexPage.enterName("last name");

        agentUserListVerification = agentUserListIndexPage.enterRandomDOB();

        testVerifyLog("Verify DOB selected successfully.");
        if (agentUserListVerification.verifyDOBSelected()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentUserListVerification = agentUserListIndexPage.enterEmail(false);

        agentUserListVerification = agentUserListIndexPage.enterMobileNumber();

        agentUserListVerification = agentUserListIndexPage.uploadProfilePicture(false);

        testVerifyLog("Verify profile pic selected successfully.");
        if (agentUserListVerification.verifyProfilePicSelected()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentUserListVerification = agentUserListIndexPage.selectDocumentType();
        agentUserListVerification = agentUserListIndexPage.uploadFrontSide(false);

        testVerifyLog("Verify Adhaar Front selected successfully.");
        if (agentUserListVerification.verifyAdhaarFrontSelected()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentUserListVerification = agentUserListIndexPage.uploadBackSide(false);

        testVerifyLog("Verify Adhaar Back selected successfully.");
        if (agentUserListVerification.verifyAdhaarBackSelected()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentUserListVerification = agentUserListIndexPage.clickStep1OnNextButton();

        testVerifyLog("Verify Step 2 OTP screen display.");
        if (agentUserListVerification.verifyStep2OTPScreenDisplay()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentUserListVerification = agentUserListIndexPage.enterStep2OTP();
        agentUserListVerification = agentUserListIndexPage.clickOnStep2NextButton();

        testVerifyLog("Verify Step 3 screen display.");
        if (agentUserListVerification.verifyConsumerStep3ScreenDisplay()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentUserListVerification = agentUserListIndexPage.enterHomeAddress(false);

        agentUserListVerification = agentUserListIndexPage.enterStreet1Address(false);

        agentUserListVerification = agentUserListIndexPage.enterStreet2Address(false);

        agentUserListVerification = agentUserListIndexPage.enterVillageAddress(false);

        agentUserListVerification = agentUserListIndexPage.enterTehsilAddress(false);

        agentUserListVerification = agentUserListIndexPage.enterDistrictAddress(false);

        agentUserListVerification = agentUserListIndexPage.selectState();

        pause(3);

        agentUserListVerification = agentUserListIndexPage.selectCity();

        agentUserListVerification = agentUserListIndexPage.enterCountry();

        agentUserListVerification = agentUserListIndexPage.enterPinCode();

        agentUserListVerification = agentUserListIndexPage.clickOnConsumerCancelButton();
        pause(1);
        agentUserListVerification = agentUserListIndexPage.clickOnOKCancelButton();

        testVerifyLog("Verify Create Consumer cancelled successfully.");
        if (agentUserListVerification.verifyCancelCreateConsumer()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void agent_CancelCreateMerchant() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_057 :: To verify Agent can cancel Create Merchant scenario.");
        agentLoginIndexPage.getVersion();

        agentLoginVerification = agentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (agentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.getAddedMerchantCounts();

        agentDashboardVerification = agentDashboardIndexPage.clickOnCreateMerchant();

        testVerifyLog("Verify user can see the Create Merchant Step 1 Screen.");
        if (agentUserListVerification.verifyCreateMerchantScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentUserListVerification = agentUserListIndexPage.enterName("first name");

        agentUserListVerification = agentUserListIndexPage.enterName("last name");

        agentUserListVerification = agentUserListIndexPage.enterRandomDOB();

        testVerifyLog("Verify DOB selected successfully.");
        if (agentUserListVerification.verifyDOBSelected()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentUserListVerification = agentUserListIndexPage.enterUsername(false);

        agentUserListVerification = agentUserListIndexPage.enterEmail(false);

        agentUserListVerification = agentUserListIndexPage.enterMobileNumber();

        agentUserListVerification = agentUserListIndexPage.uploadProfilePicture(false);

        testVerifyLog("Verify profile pic selected successfully.");
        if (agentUserListVerification.verifyProfilePicSelected()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentUserListVerification = agentUserListIndexPage.selectDocumentType();
        agentUserListVerification = agentUserListIndexPage.uploadFrontSide(false);

        testVerifyLog("Verify Adhaar Front selected successfully.");
        if (agentUserListVerification.verifyAdhaarFrontSelected()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentUserListVerification = agentUserListIndexPage.uploadBackSide(false);

        testVerifyLog("Verify Adhaar Back selected successfully.");
        if (agentUserListVerification.verifyAdhaarBackSelected()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentUserListVerification = agentUserListIndexPage.clickStep1OnNextButton();

        testVerifyLog("Verify Step 2 OTP screen display.");
        if (agentUserListVerification.verifyStep2OTPScreenDisplay()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentUserListVerification = agentUserListIndexPage.enterStep2OTP();
        agentUserListVerification = agentUserListIndexPage.clickOnStep2NextButton();

        testVerifyLog("Verify Step 3 screen display.");
        if (agentUserListVerification.verifyMerchantStep3ScreenDisplay()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentUserListVerification = agentUserListIndexPage.enterBusinessName();

        agentUserListVerification = agentUserListIndexPage.selectBusinessType();

        agentUserListVerification = agentUserListIndexPage.enterDBAName();

        agentUserListVerification = agentUserListIndexPage.enterBusinessNumber(false);

        agentUserListVerification = agentUserListIndexPage.enterGSTNumber();

        agentUserListVerification = agentUserListIndexPage.enterBusinessURL();

        agentUserListVerification = agentUserListIndexPage.uploadBusinessPANCard(false);

        testVerifyLog("Verify Business PAN Card selected successfully.");
        if (superAgentAgentListVerification.verifyBusinessPANSelected()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentUserListVerification = agentUserListIndexPage.uploadBusinessAddressProof(false);

        testVerifyLog("Verify Business Address Proof selected successfully.");
        if (superAgentAgentListVerification.verifyBusinessAddressProofSelected()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentUserListVerification = agentUserListIndexPage.clickOnStep3NextButton();

        testVerifyLog("Verify Step 4 screen display.");
        if (agentUserListVerification.verifyMerchantStep4ScreenDisplay()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentUserListVerification = agentUserListIndexPage.enterHomeAddress(false);

        agentUserListVerification = agentUserListIndexPage.enterStreet1Address(false);

        agentUserListVerification = agentUserListIndexPage.enterStreet2Address(false);

        agentUserListVerification = agentUserListIndexPage.enterVillageAddress(false);

        agentUserListVerification = agentUserListIndexPage.enterTehsilAddress(false);

        agentUserListVerification = agentUserListIndexPage.enterDistrictAddress(false);

        agentUserListVerification = agentUserListIndexPage.selectState();

        pause(3);

        agentUserListVerification = agentUserListIndexPage.selectCity();

        agentUserListVerification = agentUserListIndexPage.enterCountry();

        agentUserListVerification = agentUserListIndexPage.enterPinCode();

        agentUserListVerification = agentUserListIndexPage.clickOnMerchantCancelButton();
        pause(1);
        agentUserListVerification = agentUserListIndexPage.clickOnOKCancelButton();

        testVerifyLog("Verify Create Merchant cancelled successfully.");
        if (agentUserListVerification.verifyCancelCreateMerchant()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void agent_CreateConsumer() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_047 :: To verify Agent can Create Consumer.");
        agentLoginIndexPage.getVersion();

        agentLoginVerification = agentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (agentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.getAddedConsumerCounts();

        agentDashboardVerification = agentDashboardIndexPage.clickOnCreateConsumer();

        testVerifyLog("Verify user can see the Create Consumer Step 1 Screen.");
        if (agentUserListVerification.verifyCreateConsumerScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            agentUserListVerification = agentUserListIndexPage.enterNameMinChar("first name");

            testVerifyLog("Verify validation message for first name with minimum characters.");
            if (agentUserListVerification.verifyMinCharName("first name")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.enterNameNumber("first name");

            testVerifyLog("Verify validation message for numeric first name.");
            if (agentUserListVerification.verifyNumericName("first name")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.enterNameMaxChar("first name");

            testVerifyLog("Verify validation message for first name with max characters.");
            if (agentUserListVerification.verifyMaxCharName("first name")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentUserListVerification = agentUserListIndexPage.enterName("first name");

        testVerifyLog("Verify no validation message display for valid name.");
        if (agentUserListVerification.verifyNoValidationName("first name")) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            agentUserListVerification = agentUserListIndexPage.enterNameMinChar("last name");

            testVerifyLog("Verify validation message for last name for minimum characters.");
            if (agentUserListVerification.verifyMinCharName("last name")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.enterNameNumber("last name");

            testVerifyLog("Verify validation message for numeric last name.");
            if (agentUserListVerification.verifyNumericName("last name")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.enterNameMaxChar("last name");

            testVerifyLog("Verify validation message for last name with maximum characters.");
            if (agentUserListVerification.verifyMaxCharName("last name")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentUserListVerification = agentUserListIndexPage.enterName("last name");

        testVerifyLog("Verify no validation message display for valid name.");
        if (agentUserListVerification.verifyNoValidationName("last name")) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentUserListVerification = agentUserListIndexPage.enterRandomDOB();

        testVerifyLog("Verify DOB selected successfully.");
        if (agentUserListVerification.verifyDOBSelected()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            agentUserListVerification = agentUserListIndexPage.enterEmail(true);

            testVerifyLog("Verify validation message for invalid email address.");
            if (agentUserListVerification.verifyInvalidEmailValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentUserListVerification = agentUserListIndexPage.enterEmail(false);

        testVerifyLog("Verify no validation message display for valid email.");
        if (agentUserListVerification.verifyNoValidationEmail()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            agentUserListVerification = agentUserListIndexPage.enterMinMobileNumber();

            testVerifyLog("Verify validation message display for mobile number with minimum numbers.");
            if (agentUserListVerification.verifyInvalidMobileNumberValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.enterMaxMobileNumber();

            testVerifyLog("Verify validation message display for mobile number with maximum numbers.");
            if (agentUserListVerification.verifyInvalidMobileNumberValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.enterInvalidMobileNumber();

            testVerifyLog("Verify validation message display for invalid mobile number.");
            if (agentUserListVerification.verifyInvalidMobileNumberValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentUserListVerification = agentUserListIndexPage.enterMobileNumber();

        testVerifyLog("Verify no validation message display for valid Mobile Number.");
        if (agentUserListVerification.verifyNoValidationMobile()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            agentUserListVerification = agentUserListIndexPage.uploadProfilePicture(true);

            testVerifyLog("Verify validation message for invalid file format upload.");
            if (agentUserListVerification.verifyInvalidFileUpload()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentUserListVerification = agentUserListIndexPage.uploadProfilePicture(false);

        testVerifyLog("Verify profile pic selected successfully.");
        if (agentUserListVerification.verifyProfilePicSelected()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentUserListVerification = agentUserListIndexPage.selectDocumentType();

        if (!isPositiveExecution) {
            agentUserListVerification = agentUserListIndexPage.uploadFrontSide(true);

            testVerifyLog("Verify validation message for invalid file format upload.");
            if (agentUserListVerification.verifyInvalidFileUpload()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentUserListVerification = agentUserListIndexPage.uploadFrontSide(false);

        testVerifyLog("Verify Adhaar Front selected successfully.");
        if (agentUserListVerification.verifyAdhaarFrontSelected()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            agentUserListVerification = agentUserListIndexPage.uploadBackSide(true);

            testVerifyLog("Verify validation message for invalid file format upload.");
            if (agentUserListVerification.verifyInvalidFileUpload()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentUserListVerification = agentUserListIndexPage.uploadBackSide(false);

        testVerifyLog("Verify Adhaar Back selected successfully.");
        if (agentUserListVerification.verifyAdhaarBackSelected()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentUserListVerification = agentUserListIndexPage.clickStep1OnNextButton();

        testVerifyLog("Verify Step 2 OTP screen display.");
        if (agentUserListVerification.verifyStep2OTPScreenDisplay()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            agentUserListVerification = agentUserListIndexPage.enterOTP(getRandomNumber() + "");
            agentUserListVerification = agentUserListIndexPage.clickOnStep2NextButton();

            testVerifyLog("Verify validation message for wrong OTP.");
            if (agentUserListVerification.verifyOTPMisMatchValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.enterOTP(getRandomCharacters(5));
            agentUserListVerification = agentUserListIndexPage.clickOnStep2NextButton();

            testVerifyLog("Verify validation message for invalid OTP.");
            if (agentUserListVerification.verifyInvalidOTPValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.enterOTP(getRandomNumber() + "1");
            agentUserListVerification = agentUserListIndexPage.clickOnStep2NextButton();

            testVerifyLog("Verify validation message for OTP with maximum numbers.");
            if (agentUserListVerification.verifyInvalidOTPValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.enterOTP("");
            agentUserListVerification = agentUserListIndexPage.clickOnStep2NextButton();

            testVerifyLog("Verify validation message for blank OTP.");
            if (agentUserListVerification.verifyOTPMisMatchValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentUserListVerification = agentUserListIndexPage.enterStep2OTP();
        agentUserListVerification = agentUserListIndexPage.clickOnStep2NextButton();

        testVerifyLog("Verify Step 3 screen display.");
        if (agentUserListVerification.verifyConsumerStep3ScreenDisplay()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            agentUserListVerification = agentUserListIndexPage.enterHomeAddress(true);

            testVerifyLog("Verify validation message for invalid address details.");
            if (agentUserListVerification.verifyInvalidAddress()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentUserListVerification = agentUserListIndexPage.enterHomeAddress(false);

        testVerifyLog("Verify no validation message display for valid address.");
        if (agentUserListVerification.verifyNoValidationAddress()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            agentUserListVerification = agentUserListIndexPage.enterStreet1Address(true);

            testVerifyLog("Verify validation message for invalid Street 1 details.");
            if (agentUserListVerification.verifyInvalidAddress()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentUserListVerification = agentUserListIndexPage.enterStreet1Address(false);

        testVerifyLog("Verify no validation message display for valid Street 1 address.");
        if (agentUserListVerification.verifyNoValidationAddress()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            agentUserListVerification = agentUserListIndexPage.enterStreet2Address(true);

            testVerifyLog("Verify validation message for invalid Street 2 details.");
            if (agentUserListVerification.verifyInvalidAddress()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentUserListVerification = agentUserListIndexPage.enterStreet2Address(false);

        testVerifyLog("Verify no validation message display for valid Street 2 address.");
        if (agentUserListVerification.verifyNoValidationAddress()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            agentUserListVerification = agentUserListIndexPage.enterVillageAddress(true);

            testVerifyLog("Verify validation message for invalid village details.");
            if (agentUserListVerification.verifyInvalidVillageAddress()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentUserListVerification = agentUserListIndexPage.enterVillageAddress(false);

        testVerifyLog("Verify no validation message display for valid village.");
        if (agentUserListVerification.verifyNoValidationAddress()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            agentUserListVerification = agentUserListIndexPage.enterTehsilAddress(true);

            testVerifyLog("Verify validation message for invalid tehsil details.");
            if (agentUserListVerification.verifyInvalidTehsilAddress()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentUserListVerification = agentUserListIndexPage.enterTehsilAddress(false);

        testVerifyLog("Verify no validation message display for valid tehsil.");
        if (agentUserListVerification.verifyNoValidationAddress()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            agentUserListVerification = agentUserListIndexPage.enterDistrictAddress(true);

            testVerifyLog("Verify validation message for invalid district details.");
            if (agentUserListVerification.verifyInvalidDistrictAddress()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentUserListVerification = agentUserListIndexPage.enterDistrictAddress(false);

        testVerifyLog("Verify no validation message display for valid district.");
        if (agentUserListVerification.verifyNoValidationAddress()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentUserListVerification = agentUserListIndexPage.selectState();

        agentUserListVerification = agentUserListIndexPage.selectCity();

        if (!isPositiveExecution) {
            agentUserListVerification = agentUserListIndexPage.enterNumberCountry();

            testVerifyLog("Verify validation message for numeric values in country.");
            if (agentUserListVerification.verifyNumericCountryValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.enterMinCharCountry();

            testVerifyLog("Verify validation message for minimum characters in country.");
            if (agentUserListVerification.verifyCountryMinCharValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.enterMaxCharCountry();

            testVerifyLog("Verify validation message for maximum characters in country.");
            if (agentUserListVerification.verifyCountryMaxCharValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentUserListVerification = agentUserListIndexPage.enterCountry();

        testVerifyLog("Verify no validation message display for valid country.");
        if (agentUserListVerification.verifyNoValidationCountry()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            agentUserListVerification = agentUserListIndexPage.enterMinPinCode();

            testVerifyLog("Verify validation message display for minimum numbers testData code.");
            if (agentUserListVerification.verifyInvalidPinCodeValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.enterMaxPinCode();

            testVerifyLog("Verify validation message display for maximum numbers testData code.");
            if (agentUserListVerification.verifyInvalidPinCodeValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.enterInvalidPinCode();

            testVerifyLog("Verify validation message display for invalid testData code.");
            if (agentUserListVerification.verifyInvalidPinCodeValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }
        agentUserListVerification = agentUserListIndexPage.enterPinCode();

        testVerifyLog("Verify no validation message display for valid Pin Code.");
        if (agentUserListVerification.verifyNoValidationPinCode()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentUserListVerification = agentUserListIndexPage.clickOnConsumerCancelButton();
        pause(1);
        agentUserListVerification = agentUserListIndexPage.clickOnCancelCancelButton();
        pause(1);
        agentUserListVerification = agentUserListIndexPage.clickOnCreateConsumerButton();

        testVerifyLog("Verify user see the confirmation message for Consumer created successfully.");
        if (agentUserListVerification.verifyUserCreatedSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify Created Consumer count updated after consumer created.");
        if (agentUserListVerification.verifyCreatedConsumerCount()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.clickOnAddUserIcon();

        agentDashboardVerification = agentDashboardIndexPage.searchCreatedConsumerInAddUserList();

        testVerifyLog("Verify recent created consumer display in Consumer List.");
        if (agentDashboardVerification.verifyConsumerListSearchSuccessful()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.clickOnCloseAddUser();

        agentDashboardVerification = agentDashboardIndexPage.clickOnConsumerSection();

        agentConsumerTargetDetailsVerification = agentConsumerTargetDetailsIndexPage.clickOnFilterButton();

        agentConsumerTargetDetailsVerification = agentConsumerTargetDetailsIndexPage.enterConsumerSearchCriteria();

        testVerifyLog("Verify created consumer display in Consumer Section.");
        if (agentConsumerTargetDetailsVerification.verifyCreatedConsumerSearchSuccessful()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.clickOnUsersListMenu();

        agentUserListVerification = agentUserListIndexPage.enterConsumerSearchCriteria();

        testVerifyLog("Verify Created Consumer display in the Users List screen.");
        if (agentUserListVerification.verifyCreatedConsumerSearchSuccessful()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void agent_CreateMerchant() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_048 :: To verify Agent can Create Merchant.");
        agentLoginIndexPage.getVersion();

        agentLoginVerification = agentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (agentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.getAddedMerchantCounts();

        agentDashboardVerification = agentDashboardIndexPage.clickOnCreateMerchant();

        testVerifyLog("Verify user can see the Create Merchant Step 1 Screen.");
        if (agentUserListVerification.verifyCreateMerchantScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            agentUserListVerification = agentUserListIndexPage.enterNameMinChar("first name");

            testVerifyLog("Verify validation message for first name with minimum characters.");
            if (agentUserListVerification.verifyMinCharName("first name")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.enterNameNumber("first name");

            testVerifyLog("Verify validation message for numeric first name.");
            if (agentUserListVerification.verifyNumericName("first name")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.enterNameMaxChar("first name");

            testVerifyLog("Verify validation message for first name with max characters.");
            if (agentUserListVerification.verifyMaxCharName("first name")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentUserListVerification = agentUserListIndexPage.enterName("first name");

        testVerifyLog("Verify no validation message display for valid name.");
        if (agentUserListVerification.verifyNoValidationName("first name")) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            agentUserListVerification = agentUserListIndexPage.enterNameMinChar("last name");

            testVerifyLog("Verify validation message for last name for minimum characters.");
            if (agentUserListVerification.verifyMinCharName("last name")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.enterNameNumber("last name");

            testVerifyLog("Verify validation message for numeric last name.");
            if (agentUserListVerification.verifyNumericName("last name")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.enterNameMaxChar("last name");

            testVerifyLog("Verify validation message for last name with maximum characters.");
            if (agentUserListVerification.verifyMaxCharName("last name")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentUserListVerification = agentUserListIndexPage.enterName("last name");

        testVerifyLog("Verify no validation message display for valid name.");
        if (agentUserListVerification.verifyNoValidationName("last name")) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentUserListVerification = agentUserListIndexPage.enterUsernameLessThan8Char();

        testVerifyLog("Verify validation message for invalid username.");
        if (agentUserListVerification.verifyInvalidUsername()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            agentUserListVerification = agentUserListIndexPage.enterUsername(true);

            testVerifyLog("Verify no validation message display for invalid username.");
            if (agentUserListVerification.verifyInvalidUsername()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentUserListVerification = agentUserListIndexPage.enterUsername(false);

        testVerifyLog("Verify no validation message display for valid username.");
        if (agentUserListVerification.verifyNoValidationUserName()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentUserListVerification = agentUserListIndexPage.enterRandomDOB();

        testVerifyLog("Verify DOB selected successfully.");
        if (agentUserListVerification.verifyDOBSelected()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            agentUserListVerification = agentUserListIndexPage.enterEmail(true);

            testVerifyLog("Verify validation message for invalid email address.");
            if (agentUserListVerification.verifyInvalidEmailValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentUserListVerification = agentUserListIndexPage.enterEmail(false);

        testVerifyLog("Verify no validation message display for valid email.");
        if (agentUserListVerification.verifyNoValidationEmail()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            agentUserListVerification = agentUserListIndexPage.enterMinMobileNumber();

            testVerifyLog("Verify validation message display for mobile number with minimum numbers.");
            if (agentUserListVerification.verifyInvalidMobileNumberValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.enterMaxMobileNumber();

            testVerifyLog("Verify validation message display for mobile number with maximum numbers.");
            if (agentUserListVerification.verifyInvalidMobileNumberValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.enterInvalidMobileNumber();

            testVerifyLog("Verify validation message display for invalid mobile number.");
            if (agentUserListVerification.verifyInvalidMobileNumberValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentUserListVerification = agentUserListIndexPage.enterMobileNumber();

        testVerifyLog("Verify no validation message display for valid Mobile Number.");
        if (agentUserListVerification.verifyNoValidationMobile()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            agentUserListVerification = agentUserListIndexPage.uploadProfilePicture(true);

            testVerifyLog("Verify validation message for invalid file format upload.");
            if (agentUserListVerification.verifyInvalidFileUpload()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentUserListVerification = agentUserListIndexPage.uploadProfilePicture(false);

        testVerifyLog("Verify profile pic selected successfully.");
        if (agentUserListVerification.verifyProfilePicSelected()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentUserListVerification = agentUserListIndexPage.selectDocumentType();

        if (!isPositiveExecution) {
            agentUserListVerification = agentUserListIndexPage.uploadFrontSide(true);

            testVerifyLog("Verify validation message for invalid file format upload.");
            if (agentUserListVerification.verifyInvalidFileUpload()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentUserListVerification = agentUserListIndexPage.uploadFrontSide(false);

        testVerifyLog("Verify Document Front Side selected successfully.");
        if (agentUserListVerification.verifyAdhaarFrontSelected()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            agentUserListVerification = agentUserListIndexPage.uploadBackSide(true);

            testVerifyLog("Verify validation message for invalid file format upload.");
            if (agentUserListVerification.verifyInvalidFileUpload()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentUserListVerification = agentUserListIndexPage.uploadBackSide(false);

        testVerifyLog("Verify Document Back Side selected successfully.");
        if (agentUserListVerification.verifyAdhaarBackSelected()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentUserListVerification = agentUserListIndexPage.clickStep1OnNextButton();

        testVerifyLog("Verify Step 2 OTP screen display.");
        if (agentUserListVerification.verifyStep2OTPScreenDisplay()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            agentUserListVerification = agentUserListIndexPage.enterOTP(getRandomNumber() + "");
            agentUserListVerification = agentUserListIndexPage.clickOnStep2NextButton();

            testVerifyLog("Verify validation message for wrong OTP.");
            if (agentUserListVerification.verifyOTPMisMatchValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.enterOTP(getRandomCharacters(5));
            agentUserListVerification = agentUserListIndexPage.clickOnStep2NextButton();

            testVerifyLog("Verify validation message for invalid OTP.");
            if (agentUserListVerification.verifyInvalidOTPValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.enterOTP(getRandomNumber() + "1");
            agentUserListVerification = agentUserListIndexPage.clickOnStep2NextButton();

            testVerifyLog("Verify validation message for OTP with maximum numbers.");
            if (agentUserListVerification.verifyInvalidOTPValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.enterOTP("");
            agentUserListVerification = agentUserListIndexPage.clickOnStep2NextButton();

            testVerifyLog("Verify validation message for blank OTP.");
            if (agentUserListVerification.verifyOTPMisMatchValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentUserListVerification = agentUserListIndexPage.enterStep2OTP();
        agentUserListVerification = agentUserListIndexPage.clickOnStep2NextButton();

        testVerifyLog("Verify Step 3 screen display.");
        if (agentUserListVerification.verifyMerchantStep3ScreenDisplay()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            agentUserListVerification = agentUserListIndexPage.enterInvalidBusinessName("min");

            testVerifyLog("Verify validation message for invalid Business Name.");
            if (agentUserListVerification.verifyInvalidBusinessName()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.enterInvalidBusinessName("max");

            testVerifyLog("Verify validation message for invalid Business Name.");
            if (agentUserListVerification.verifyInvalidBusinessName()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentUserListVerification = agentUserListIndexPage.enterBusinessName();

        testVerifyLog("Verify no validation message for valid Business Name.");
        if (agentUserListVerification.verifyNoValidationBusinessName()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentUserListVerification = agentUserListIndexPage.selectBusinessType();

        if (!isPositiveExecution) {
            agentUserListVerification = agentUserListIndexPage.enterInvalidDBAName("min");

            testVerifyLog("Verify validation message for invalid DBA Name.");
            if (agentUserListVerification.verifyInvalidDBAName()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.enterInvalidDBAName("max");

            testVerifyLog("Verify validation message for invalid DBA Name.");
            if (agentUserListVerification.verifyInvalidDBAName()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentUserListVerification = agentUserListIndexPage.enterDBAName();

        testVerifyLog("Verify no validation message for valid DBA Name.");
        if (agentUserListVerification.verifyNoValidationDBAName()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            agentUserListVerification = agentUserListIndexPage.enterBusinessNumber(true);

            testVerifyLog("Verify validation message for invalid Business Number.");
            if (agentUserListVerification.verifyInvalidMobileNumberValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentUserListVerification = agentUserListIndexPage.enterBusinessNumber(false);

        testVerifyLog("Verify no validation message display for valid Business Number.");
        if (agentUserListVerification.verifyNoValidationMobile()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            agentUserListVerification = agentUserListIndexPage.enterGSTNumber(true);

            testVerifyLog("Verify validation message for invalid GST Number.");
            if (agentUserListVerification.verifyInvalidGSTNumber()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentUserListVerification = agentUserListIndexPage.enterGSTNumber(false);

        testVerifyLog("Verify no validation message display for valid GST Number.");
        if (agentUserListVerification.verifyNoValidationGST()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentUserListVerification = agentUserListIndexPage.enterBusinessURL();

        if (!isPositiveExecution) {
            agentUserListVerification = agentUserListIndexPage.uploadBusinessPANCard(true);

            testVerifyLog("Verify validation message for invalid file format upload.");
            if (agentUserListVerification.verifyInvalidFileUpload()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentUserListVerification = agentUserListIndexPage.uploadBusinessPANCard(false);

        testVerifyLog("Verify Business PAN selected successfully.");
        if (agentUserListVerification.verifyBusinessPANSelected()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            agentUserListVerification = agentUserListIndexPage.uploadBusinessAddressProof(true);

            testVerifyLog("Verify validation message for invalid file format upload.");
            if (agentUserListVerification.verifyInvalidFileUpload()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentUserListVerification = agentUserListIndexPage.uploadBusinessAddressProof(false);

        testVerifyLog("Verify Business Address Proof selected successfully.");
        if (agentUserListVerification.verifyBusinessAddressProofSelected()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentUserListVerification = agentUserListIndexPage.clickOnStep3NextButton();

        testVerifyLog("Verify Step 4 screen display.");
        if (agentUserListVerification.verifyMerchantStep4ScreenDisplay()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            agentUserListVerification = agentUserListIndexPage.enterHomeAddress(true);

            testVerifyLog("Verify validation message for invalid address details.");
            if (agentUserListVerification.verifyInvalidAddress()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentUserListVerification = agentUserListIndexPage.enterHomeAddress(false);

        testVerifyLog("Verify no validation message display for valid address.");
        if (agentUserListVerification.verifyNoValidationAddress()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            agentUserListVerification = agentUserListIndexPage.enterStreet1Address(true);

            testVerifyLog("Verify validation message for invalid Street 1 details.");
            if (agentUserListVerification.verifyInvalidAddress()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentUserListVerification = agentUserListIndexPage.enterStreet1Address(false);

        testVerifyLog("Verify no validation message display for valid Street 1.");
        if (agentUserListVerification.verifyNoValidationAddress()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            agentUserListVerification = agentUserListIndexPage.enterStreet2Address(true);

            testVerifyLog("Verify validation message for invalid Street 2 details.");
            if (agentUserListVerification.verifyInvalidAddress()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentUserListVerification = agentUserListIndexPage.enterStreet2Address(false);

        testVerifyLog("Verify no validation message display for valid Street 2.");
        if (agentUserListVerification.verifyNoValidationAddress()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            agentUserListVerification = agentUserListIndexPage.enterVillageAddress(true);

            testVerifyLog("Verify validation message for invalid village details.");
            if (agentUserListVerification.verifyInvalidVillageAddress()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentUserListVerification = agentUserListIndexPage.enterVillageAddress(false);

        testVerifyLog("Verify no validation message display for valid village.");
        if (agentUserListVerification.verifyNoValidationAddress()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            agentUserListVerification = agentUserListIndexPage.enterTehsilAddress(true);

            testVerifyLog("Verify validation message for invalid tehsil details.");
            if (agentUserListVerification.verifyInvalidTehsilAddress()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentUserListVerification = agentUserListIndexPage.enterTehsilAddress(false);

        testVerifyLog("Verify no validation message display for valid tehsil.");
        if (agentUserListVerification.verifyNoValidationAddress()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            agentUserListVerification = agentUserListIndexPage.enterDistrictAddress(true);

            testVerifyLog("Verify validation message for invalid district details.");
            if (agentUserListVerification.verifyInvalidDistrictAddress()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentUserListVerification = agentUserListIndexPage.enterDistrictAddress(false);

        testVerifyLog("Verify no validation message display for valid district.");
        if (agentUserListVerification.verifyNoValidationAddress()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentUserListVerification = agentUserListIndexPage.selectState();

        agentUserListVerification = agentUserListIndexPage.selectCity();

        agentUserListVerification = agentUserListIndexPage.enterNumberCountry();

        testVerifyLog("Verify validation message for numeric values in country.");
        if (agentUserListVerification.verifyNumericCountryValidation()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            agentUserListVerification = agentUserListIndexPage.enterMinCharCountry();

            testVerifyLog("Verify validation message for minimum characters in country.");
            if (agentUserListVerification.verifyCountryMinCharValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.enterMaxCharCountry();

            testVerifyLog("Verify validation message for maximum characters in country.");
            if (agentUserListVerification.verifyCountryMaxCharValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentUserListVerification = agentUserListIndexPage.enterCountry();

        testVerifyLog("Verify no validation message display for valid country.");
        if (agentUserListVerification.verifyNoValidationCountry()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            agentUserListVerification = agentUserListIndexPage.enterMinPinCode();

            testVerifyLog("Verify validation message display for minimum numbers testData code.");
            if (agentUserListVerification.verifyInvalidPinCodeValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.enterMaxPinCode();

            testVerifyLog("Verify validation message display for maximum numbers testData code.");
            if (agentUserListVerification.verifyInvalidPinCodeValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.enterInvalidPinCode();

            testVerifyLog("Verify validation message display for invalid testData code.");
            if (agentUserListVerification.verifyInvalidPinCodeValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentUserListVerification = agentUserListIndexPage.enterPinCode();

        testVerifyLog("Verify no validation message display for valid Pin Code.");
        if (agentUserListVerification.verifyNoValidationPinCode()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentUserListVerification = agentUserListIndexPage.clickOnMerchantCancelButton();
        pause(1);
        agentUserListVerification = agentUserListIndexPage.clickOnCancelCancelButton();
        pause(1);
        agentUserListVerification = agentUserListIndexPage.clickOnCreateMerchantButton();

        testVerifyLog("Verify user see the confirmation message for Merchant created successfully.");
        if (agentUserListVerification.verifyUserCreatedSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify Created Merchant count updated after merchant created.");
        if (agentUserListVerification.verifyCreatedMerchantCount()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.clickOnAddUserIcon();

        agentDashboardVerification = agentDashboardIndexPage.clickOnMerchantTab();

        agentDashboardVerification = agentDashboardIndexPage.searchCreatedConsumerInAddUserList();

        testVerifyLog("Verify recent created merchant display in Merchant List.");
        if (agentDashboardVerification.verifyMerchantListSearchSuccessful()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.clickOnCloseAddUser();

        agentDashboardVerification = agentDashboardIndexPage.clickOnMerchantSection();

        agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.clickOnFilterButton();

        agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.enterMerchantSearchCriteria();

        testVerifyLog("Verify created merchant display in Merchant Section.");
        if (agentMerchantTargetDetailsVerification.verifyCreatedMerchantSearchSuccessful()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.clickOnUsersListMenu();

        agentUserListVerification = agentUserListIndexPage.clickOnMerchantTab();

        agentUserListVerification = agentUserListIndexPage.enterMerchantSearchCriteria();

        testVerifyLog("Verify Created Merchant display in the Users List screen.");
        if (agentUserListVerification.verifyCreatedMerchantSearchSuccessful()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }


        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void agent_ConsumerDetailsFilter() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_058 :: To verify Agent can Filter the Consumer details in the Consumer Target screen.");
        agentLoginIndexPage.getVersion();

        agentLoginVerification = agentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (agentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.getConsumerAndMerchantCount();

        agentDashboardVerification = agentDashboardIndexPage.clickOnConsumerSection();

        agentConsumerTargetDetailsVerification = agentConsumerTargetDetailsIndexPage.clickOnFilterButton();

        if (agentConsumerTargetDetailsIndexPage.isConsumerListDisplay()) {

            testVerifyLog("Verify user can see the Filter screen.");
            if (agentConsumerTargetDetailsVerification.verifyConsumerFilterScreenDisplay()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentConsumerTargetDetailsVerification = agentConsumerTargetDetailsIndexPage.getConsumerDetailsForFilter();

            if (!isPositiveExecution) {
                agentConsumerTargetDetailsVerification = agentConsumerTargetDetailsIndexPage.filterFirstName(true);

                agentConsumerTargetDetailsVerification = agentConsumerTargetDetailsIndexPage.clickOnFilterSearchButton();

                testVerifyLog("User can see validation message for invalid filtered data.");
                if (agentConsumerTargetDetailsVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            agentConsumerTargetDetailsVerification = agentConsumerTargetDetailsIndexPage.filterFirstName(false);

            agentConsumerTargetDetailsVerification = agentConsumerTargetDetailsIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify First Name filter successfully.");
            if (agentConsumerTargetDetailsVerification.verifyNameFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentConsumerTargetDetailsVerification = agentConsumerTargetDetailsIndexPage.clearAllFilterDetails();

            if (!isPositiveExecution) {
                agentConsumerTargetDetailsVerification = agentConsumerTargetDetailsIndexPage.filterLastName(true);

                agentConsumerTargetDetailsVerification = agentConsumerTargetDetailsIndexPage.clickOnFilterSearchButton();

                testVerifyLog("User can see validation message for invalid filtered data.");
                if (agentConsumerTargetDetailsVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            agentConsumerTargetDetailsVerification = agentConsumerTargetDetailsIndexPage.filterLastName(false);

            agentConsumerTargetDetailsVerification = agentConsumerTargetDetailsIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify Last Name filter successfully.");
            if (agentConsumerTargetDetailsVerification.verifyNameFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentConsumerTargetDetailsVerification = agentConsumerTargetDetailsIndexPage.clearAllFilterDetails();

            if (!isPositiveExecution) {
                agentConsumerTargetDetailsVerification = agentConsumerTargetDetailsIndexPage.filterConsumerNumber(true);

                agentConsumerTargetDetailsVerification = agentConsumerTargetDetailsIndexPage.clickOnFilterSearchButton();

                testVerifyLog("User can see validation message for invalid filtered data.");
                if (agentConsumerTargetDetailsVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            agentConsumerTargetDetailsVerification = agentConsumerTargetDetailsIndexPage.filterConsumerNumber(false);

            agentConsumerTargetDetailsVerification = agentConsumerTargetDetailsIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify Mobile Number filter successfully.");
            if (agentConsumerTargetDetailsVerification.verifyPhoneNumberFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentConsumerTargetDetailsVerification = agentConsumerTargetDetailsIndexPage.clearAllFilterDetails();

            agentConsumerTargetDetailsVerification = agentConsumerTargetDetailsIndexPage.getConsumerStatus();

            agentConsumerTargetDetailsVerification = agentConsumerTargetDetailsIndexPage.filterStatus("Active");

            agentConsumerTargetDetailsVerification = agentConsumerTargetDetailsIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify Status filter successfully.");
            if (agentConsumerTargetDetailsVerification.verifyStatusFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentConsumerTargetDetailsVerification = agentConsumerTargetDetailsIndexPage.filterStatus("Kyc Pending");

            agentConsumerTargetDetailsVerification = agentConsumerTargetDetailsIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify Status filter successfully.");
            if (agentConsumerTargetDetailsVerification.verifyStatusFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentConsumerTargetDetailsVerification = agentConsumerTargetDetailsIndexPage.filterStatus("Login Pending");

            agentConsumerTargetDetailsVerification = agentConsumerTargetDetailsIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify Status filter successfully.");
            if (agentConsumerTargetDetailsVerification.verifyStatusFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");

    }

    @Test
    public void agent_TopUpsDetailsFilter() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_060 :: To verify Agent can Filter the Consumer details in the Top up Target screen.");
        agentLoginIndexPage.getVersion();

        agentLoginVerification = agentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (agentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.getConsumerAndMerchantCount();

        agentDashboardVerification = agentDashboardIndexPage.clickOnTopUpSection();

        agentTargetTopUpDetailsVerification = agentTargetTopUpDetailsIndexPage.clickOnFilterButton();

        if (agentConsumerTargetDetailsIndexPage.isConsumerListDisplay()) {

            testVerifyLog("Verify user can see the Filter screen.");
            if (agentTargetTopUpDetailsVerification.verifyConsumerFilterScreenDisplay()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentTargetTopUpDetailsVerification = agentTargetTopUpDetailsIndexPage.getConsumerDetailsForFilter();

            if (!isPositiveExecution) {
                agentTargetTopUpDetailsVerification = agentTargetTopUpDetailsIndexPage.filterFirstName(true);

                agentTargetTopUpDetailsVerification = agentTargetTopUpDetailsIndexPage.clickOnFilterSearchButton();

                testVerifyLog("User can see validation message for invalid filtered data.");
                if (agentConsumerTargetDetailsVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            agentTargetTopUpDetailsVerification = agentTargetTopUpDetailsIndexPage.filterFirstName(false);

            agentTargetTopUpDetailsVerification = agentTargetTopUpDetailsIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify First Name filter successfully.");
            if (agentTargetTopUpDetailsVerification.verifyNameFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentTargetTopUpDetailsVerification = agentTargetTopUpDetailsIndexPage.clearAllFilterDetails();

            if (!isPositiveExecution) {
                agentTargetTopUpDetailsVerification = agentTargetTopUpDetailsIndexPage.filterLastName(true);

                agentTargetTopUpDetailsVerification = agentTargetTopUpDetailsIndexPage.clickOnFilterSearchButton();

                testVerifyLog("User can see validation message for invalid filtered data.");
                if (agentTargetTopUpDetailsVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            agentTargetTopUpDetailsVerification = agentTargetTopUpDetailsIndexPage.filterLastName(false);

            agentTargetTopUpDetailsVerification = agentTargetTopUpDetailsIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify Last Name filter successfully.");
            if (agentTargetTopUpDetailsVerification.verifyNameFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentTargetTopUpDetailsVerification = agentTargetTopUpDetailsIndexPage.clearAllFilterDetails();

            if (!isPositiveExecution) {
                agentTargetTopUpDetailsVerification = agentTargetTopUpDetailsIndexPage.filterConsumerNumber(true);

                agentTargetTopUpDetailsVerification = agentTargetTopUpDetailsIndexPage.clickOnFilterSearchButton();

                testVerifyLog("User can see validation message for invalid filtered data.");
                if (agentTargetTopUpDetailsVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            agentTargetTopUpDetailsVerification = agentTargetTopUpDetailsIndexPage.filterConsumerNumber(false);

            agentTargetTopUpDetailsVerification = agentTargetTopUpDetailsIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify Mobile Number filter successfully.");
            if (agentTargetTopUpDetailsVerification.verifyPhoneNumberFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentTargetTopUpDetailsVerification = agentTargetTopUpDetailsIndexPage.clearAllFilterDetails();
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");

    }

    @Test
    public void agent_MerchantDetailsFilter() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_059 :: To verify Agent can Filter the Merchant details in the Merchant Target screen.");
        agentLoginIndexPage.getVersion();

        agentLoginVerification = agentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (agentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.getConsumerAndMerchantCount();

        agentDashboardVerification = agentDashboardIndexPage.clickOnMerchantSection();

        agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.clickOnFilterButton();

        if (agentMerchantTargetDetailsIndexPage.isMerchantListDisplay()) {

            testVerifyLog("Verify user can see the Filter screen.");
            if (agentMerchantTargetDetailsVerification.verifyMerchantFilterScreenDisplay()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.getMerchantDetailsForFilter();

            if (!isPositiveExecution) {
                agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.filterShopName(true);

                agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.clickOnFilterSearchButton();

                testVerifyLog("User can see validation message for invalid filtered data.");
                if (agentMerchantTargetDetailsVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.filterShopName(false);

            agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify Shop Name filter successfully.");
            if (agentMerchantTargetDetailsVerification.verifyShopNameFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.clearAllFilterDetails();

            if (!isPositiveExecution) {
                agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.filterMerchantID(true);

                agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.clickOnFilterSearchButton();

                testVerifyLog("User can see validation message for invalid filtered data.");
                if (agentMerchantTargetDetailsVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.filterMerchantID(false);

            agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify Merchant ID filter successfully.");
            if (agentMerchantTargetDetailsVerification.verifyMerchantIDFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.clearAllFilterDetails();

            if (!isPositiveExecution) {
                agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.filterMerchantNumber(true);

                agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.clickOnFilterSearchButton();

                testVerifyLog("User can see validation message for invalid filtered data.");
                if (agentMerchantTargetDetailsVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.filterMerchantNumber(false);

            agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify Mobile Number filter successfully.");
            if (agentMerchantTargetDetailsVerification.verifyPhoneNumberFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.clearAllFilterDetails();

            agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.getMerchantStatus();

            agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.filterStatus("Active");

            agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify Status filter successfully.");
            if (agentMerchantTargetDetailsVerification.verifyStatusFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.filterStatus("Kyc Pending");

            agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify Status filter successfully.");
            if (agentConsumerTargetDetailsVerification.verifyStatusFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.filterStatus("Login Pending");

            agentMerchantTargetDetailsVerification = agentMerchantTargetDetailsIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify Status filter successfully.");
            if (agentConsumerTargetDetailsVerification.verifyStatusFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");

    }

    @Test
    public void agent_TopUpUserFromTargetScreen() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_064 :: To verify Agent can topup from the Topup Target screen.");
        agentLoginIndexPage.getVersion();

        agentLoginVerification = agentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (agentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify user can see the Dashboard screen with details.");
        if (agentDashboardVerification.verifyDashboardDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.getConsumerAndMerchantCount();

        agentDashboardVerification = agentDashboardIndexPage.clickOnAddUserIcon();

        agentDashboardVerification = agentDashboardIndexPage.getConsumersListFromAddUsers();

        agentDashboardVerification = agentDashboardIndexPage.clickOnCloseAddUser();

        agentDashboardVerification = agentDashboardIndexPage.getTopUpDetailsBeforeTopUp();

        agentDashboardVerification = agentDashboardIndexPage.clickOnTopUpSection();

        if (agentConsumerTargetDetailsIndexPage.isConsumerListDisplay()) {

            if (agentTargetTopUpDetailsIndexPage.isFirstTimeConsumerRemaining() || agentTargetTopUpDetailsIndexPage.isSecondTimeConsumerRemaining()) {

                agentTargetTopUpDetailsVerification = agentTargetTopUpDetailsIndexPage.clickOnRechargeButton();

                agentTargetTopUpDetailsVerification = agentTargetTopUpDetailsIndexPage.enterZeroTopUpAmount();

                agentTargetTopUpDetailsVerification = agentTargetTopUpDetailsIndexPage.getConsumerTopUpAmount();

                testVerifyLog("Verify user can see the Dashboard screen with details.");
                if (agentTargetTopUpDetailsVerification.verifyValidationMessageForZeroTopUpAmount()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                agentTargetTopUpDetailsVerification = agentTargetTopUpDetailsIndexPage.enterInvalidTopUpAmount();

                testVerifyLog("Verify user can see the Dashboard screen with details.");
                if (agentTargetTopUpDetailsVerification.verifyValidationMessageForZeroTopUpAmount()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                if (AgentDashboardIndexPage.balanceBeforeTopUp < 100000 &&
                        AgentTargetTopUpDetailsIndexPage._maxTopUpAmount > AgentDashboardIndexPage.balanceBeforeTopUp) {

                    agentTargetTopUpDetailsVerification = agentTargetTopUpDetailsIndexPage.enterMaxBalanceTopUpAmount();

                    agentTargetTopUpDetailsVerification = agentTargetTopUpDetailsIndexPage.clickOnCompleteTopUpButton();

                    testVerifyLog("Verify user can see the Dashboard screen with details.");
                    if (agentTargetTopUpDetailsVerification.verifyValidationMessageForInsufficientBalanceTopUp()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }
                }

                agentTargetTopUpDetailsVerification = agentTargetTopUpDetailsIndexPage.enterValidTopUpAmount();

                agentTargetTopUpDetailsVerification = agentTargetTopUpDetailsIndexPage.clickOnCompleteTopUpButton();

                testVerifyLog("Verify user can see the Dashboard screen with details.");
                if (agentTargetTopUpDetailsVerification.verifyConsumerTopUpDetailsAfterComplete()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                agentTargetTopUpDetailsVerification = agentTargetTopUpDetailsIndexPage.clickOnCloseTopUpButton();

                testVerifyLog("Verify balance updated after topup completed.");
                if (agentDashboardVerification.verifyBalanceAndTodayConsumerTopUpDetails()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                agentDashboardVerification = agentDashboardIndexPage.getTopUpDetailsBeforeTopUp();

                agentDashboardVerification = agentDashboardIndexPage.clickOnTopUpSection();

                if (agentTargetTopUpDetailsIndexPage.isSecondTimeConsumerRemaining()) {

                    agentTargetTopUpDetailsVerification = agentTargetTopUpDetailsIndexPage.clickOnRechargeButton();

                    agentTargetTopUpDetailsVerification = agentTargetTopUpDetailsIndexPage.enterZeroTopUpAmount();

                    agentTargetTopUpDetailsVerification = agentTargetTopUpDetailsIndexPage.getConsumerTopUpAmount();

                    testVerifyLog("Verify user can see the Dashboard screen with details.");
                    if (agentTargetTopUpDetailsVerification.verifyValidationMessageForZeroTopUpAmount()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }

                    agentTargetTopUpDetailsVerification = agentTargetTopUpDetailsIndexPage.enterInvalidTopUpAmount();

                    testVerifyLog("Verify user can see the Dashboard screen with details.");
                    if (agentTargetTopUpDetailsVerification.verifyValidationMessageForZeroTopUpAmount()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }

                    if (AgentDashboardIndexPage.balanceBeforeTopUp < 100000 &&
                            AgentTargetTopUpDetailsIndexPage._maxTopUpAmount > AgentDashboardIndexPage.balanceBeforeTopUp) {
                        agentTargetTopUpDetailsVerification = agentTargetTopUpDetailsIndexPage.enterMaxBalanceTopUpAmount();

                        agentTargetTopUpDetailsVerification = agentTargetTopUpDetailsIndexPage.clickOnCompleteTopUpButton();

                        testVerifyLog("Verify user can see the Dashboard screen with details.");
                        if (agentTargetTopUpDetailsVerification.verifyValidationMessageForInsufficientBalanceTopUp()) {
                            stepPassed();
                        } else {
                            stepFailure(driver);
                            numOfFailedSteps++;
                        }
                    }

                    agentTargetTopUpDetailsVerification = agentTargetTopUpDetailsIndexPage.enterValidTopUpAmount();

                    agentTargetTopUpDetailsVerification = agentTargetTopUpDetailsIndexPage.clickOnCompleteTopUpButton();

                    testVerifyLog("Verify user can see the Dashboard screen with details.");
                    if (agentTargetTopUpDetailsVerification.verifyConsumerTopUpDetailsAfterComplete()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }

                    agentTargetTopUpDetailsVerification = agentTargetTopUpDetailsIndexPage.clickOnCloseTopUpButton();

                    testVerifyLog("Verify balance updated after topup completed.");
                    if (agentDashboardVerification.verifyBalanceAndTodayConsumerTopUpDetails()) {
                        stepPassed();
                    } else {
                        stepFailure(driver);
                        numOfFailedSteps++;
                    }
                }
            } else {
                testWarningLog("No Consumer found with the pending recharges");
            }
        }


        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }
}