package com.incashme.agent.index;

import com.framework.init.SeleniumInit;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Rahul R.
 * Date: 2019-04-09
 * Time
 * Project Name: InCashMe
 */

public class AgentLoginIndex extends SeleniumInit {

    @Test
    public void agent_LoginLogout() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_024 :: To verify Agent can Login and Logout with the valid email credentials.");
        agentLoginIndexPage.getVersion();

        if (!isPositiveExecution) {
            agentLoginVerification = agentLoginIndexPage.invalidLoginAs(getInvalidEmail(), getRandomPassword());

            testVerifyLog("Verify validation message for the invalid email address.");
            if (agentLoginVerification.verifyInvalidEmailValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentLoginVerification = agentLoginIndexPage.loginAs(getUnRegisteredEmail(), getRandomPassword());

            testVerifyLog("Verify validation message for the blank credentials.");
            if (agentLoginVerification.verifyUnregisteredEmailValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentLoginVerification = agentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (agentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentLoginVerification = agentLoginIndexPage.clickOnLogout();

        testVerifyLog("Verify user logout successfully from the Application.");
        if (agentLoginVerification.verifyUserLogoutSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentLoginVerification = agentLoginIndexPage.clickOnPrivacyPolicy();

        testVerifyLog("Verify user can see the Privacy Policy screen.");
        if (agentLoginVerification.verifyPrivacyPolicyScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentLoginVerification = agentLoginIndexPage.clickOnTermsCondition();

        testVerifyLog("Verify user can see the Terms & Condition screen.");
        if (agentLoginVerification.verifyTnCScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");

    }

    //Need Mailinator account with active mobile number
    @Test(enabled = false)
    public void agent_ForgotPassword() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_025 :: To verify Forgot Password functionality.");
        agentLoginIndexPage.getVersion();

        agentLoginVerification = agentLoginIndexPage.clickOnForgotPassword();

        agentLoginVerification = agentLoginIndexPage.clickOnBackToLogin();

        agentLoginVerification = agentLoginIndexPage.clickOnForgotPassword();

        testVerifyLog("Verify user can see the Forgot Password screen.");
        if (agentLoginVerification.verifyForgotPasswordScreenDisplay()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentLoginVerification = agentLoginIndexPage.forgotPasswordAs(getInvalidEmail());

        testVerifyLog("Verify validation message for the invalid email address.");
        if (agentLoginVerification.verifyInvalidEmailValidation()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentLoginVerification = agentLoginIndexPage.forgotPasswordAs(username);
        agentLoginVerification = agentLoginIndexPage.clickOnSubmitButton();

        testVerifyLog("Verify OTP Screen display for entering the OTP.");
        if (agentLoginVerification.verifyStep1OTPScreenDisplay()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentLoginVerification = agentLoginIndexPage.enterOTP(getRandomNumber() + "");
        agentLoginVerification = agentLoginIndexPage.clickOnSubmitButton();

        testVerifyLog("Verify invalid entered OTP validation message.");
        if (agentLoginVerification.verifyInvalidOTPValidation()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentLoginVerification = agentLoginIndexPage.waitAndClickResendButton();

        testVerifyLog("Verify OTP Screen display for entering the OTP.");
        if (agentLoginVerification.verifyStep1OTPScreenDisplay()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentLoginVerification = agentLoginIndexPage.enterStep1OTP();
        agentLoginVerification = agentLoginIndexPage.clickOnSubmitButton();

        testVerifyLog("Verify OTP Screen display after entering Step 1 OTP.");
        if (agentLoginVerification.verifyStep2OTPScreenDisplay()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentLoginVerification = agentLoginIndexPage.openResetPasswordLink(username);

        testVerifyLog("Verify Reset Password screen display.");
        if (agentLoginVerification.verifyResetPasswordScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentLoginVerification = agentLoginIndexPage.enterOTP(getRandomNumber() + "1");

        testVerifyLog("Verify Invalid OTP digits message.");
        if (agentLoginVerification.verifyInvalidOTPDigitMessage()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentLoginVerification = agentLoginIndexPage.enterStep2OTP();

        agentLoginVerification = agentLoginIndexPage.changeInvalidNewPassword();

        testVerifyLog("Verify validation message for the invalid new password.");
        if (agentLoginVerification.verifyInvalidNewPasswordValidation()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentLoginVerification = agentLoginIndexPage.changeAsPastPassword();

        testVerifyLog("Verify validation message for the past password as new password.");
        if (agentLoginVerification.verifyPastPasswordValidation()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentLoginVerification = agentLoginIndexPage.changeAgentNewPassword();

        testVerifyLog("Verify confirmation message for the Password changed successfully.");
        if (agentLoginVerification.verifyPasswordChangeSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentLoginVerification = agentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (agentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void agent_NonKYCLogin() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_023 :: To verify Non KYC Agent tries to access the application with Login and Forgot Password.");
        agentLoginIndexPage.getVersion();

        agentLoginVerification = agentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can see the KYC Validation message for the non kyc users.");
        if (agentLoginVerification.verifyLoginKYCValidationMessage()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentLoginVerification = agentLoginIndexPage.clickOnForgotPassword();

        testVerifyLog("Verify user can see the Forgot Password screen.");
        if (agentLoginVerification.verifyForgotPasswordScreenDisplay()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentLoginVerification = agentLoginIndexPage.forgotPasswordAs(username);
        agentLoginVerification = agentLoginIndexPage.clickOnSubmitButton();

        testVerifyLog("Verify user can see the KYC Validation message for the non kyc users.");
        if (agentLoginVerification.verifyForgotPasswordKYCValidationMessage()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }
}