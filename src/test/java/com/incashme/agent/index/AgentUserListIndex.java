package com.incashme.agent.index;

import com.framework.init.SeleniumInit;
import org.testng.Assert;
import org.testng.annotations.Test;

public class AgentUserListIndex extends SeleniumInit {

    @Test
    public void agent_UserListDetails() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_043 :: To verify Agent can see the Consumers in the User List.<br>" +
                "ICM_SC_044 :: To verify Agent can see the Merchants in the User List.");

        agentLoginIndexPage.getVersion();

        agentLoginVerification = agentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (agentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify user can see the Dashboard screen with details.");
        if (agentDashboardVerification.verifyDashboardDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.getConsumerAndMerchantCount();

        agentDashboardVerification = agentDashboardIndexPage.clickOnUsersListMenu();

        testVerifyLog("Verify user can see the Agents List Screen.");
        if (agentUserListVerification.verifyUsersListScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentUserListVerification = agentUserListIndexPage.clickOnConsumerTab();

        testVerifyLog("Verify Total Added agents are same as display on Dashboard.");
        if (agentUserListVerification.verifyUserCountSimilarToDashboard()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentUserListVerification = agentUserListIndexPage.clickOnConsumerTab();

        if (agentUserListIndexPage.isConsumerDataDisplay()) {

            agentUserListVerification = agentUserListIndexPage.getAnyConsumerDetails();
            agentUserListVerification = agentUserListIndexPage.clickOnConsumerMOREButton();

            testVerifyLog("Verify user can see the Agent Personal Info in Profile.");
            if (agentUserListVerification.verifyUserPersonalInfo()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.clickOnHistory();

            testVerifyLog("Verify user can see the Agent History Info in Profile.");
            if (agentUserListVerification.verifyUserHistoryInfo()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.clickOnBackButton();
        }

        agentUserListVerification = agentUserListIndexPage.clickOnMerchantTab();

        if (agentUserListIndexPage.isMerchantDataDisplay()) {

            agentUserListVerification = agentUserListIndexPage.getAnyMerchantDetails();
            agentUserListVerification = agentUserListIndexPage.clickOnMerchantMOREButton();

            testVerifyLog("Verify user can see the Agent Personal Info in Profile.");
            if (agentUserListVerification.verifyUserPersonalInfo()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.clickOnHistory();

            testVerifyLog("Verify user can see the Agent History Info in Profile.");
            if (agentUserListVerification.verifyUserHistoryInfo()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

        }


        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void agent_UserListFilter() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_045 :: To verify Agent can sort and Filter the consumer list.<br>" +
                "ICM_SC_046 :: To verify Agent can sort and filter the Merchant list.");

        agentLoginIndexPage.getVersion();

        agentLoginVerification = agentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (agentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.clickOnUsersListMenu();

        testVerifyLog("Verify user can see the Agents List Screen.");
        if (agentUserListVerification.verifyUsersListScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentUserListVerification = agentUserListIndexPage.clickOnConsumerTab();

        agentUserListVerification = agentUserListIndexPage.clickOnFilterIcon();

        if (agentUserListIndexPage.isConsumerDataDisplay()) {

            testVerifyLog("Verify user can see the Filter screen.");
            if (agentUserListVerification.verifyConsumerFilterScreenDisplay()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.getConsumerDetailsForFilter();

            if (!isPositiveExecution) {
                agentUserListVerification = agentUserListIndexPage.filterFirstName(true);

                agentUserListVerification = agentUserListIndexPage.clickOnConsumerFilterSearchButton();

                testVerifyLog("User can see validation message for invalid filtered data.");
                if (agentUserListVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            agentUserListVerification = agentUserListIndexPage.filterFirstName(false);

            agentUserListVerification = agentUserListIndexPage.clickOnConsumerFilterSearchButton();

            testVerifyLog("Verify First Name filter successfully.");
            if (agentUserListVerification.verifyFirstNameFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.clearAllConsumerFilterDetails();

            if (!isPositiveExecution) {
                agentUserListVerification = agentUserListIndexPage.filterLastName(true);

                agentUserListVerification = agentUserListIndexPage.clickOnConsumerFilterSearchButton();

                testVerifyLog("User can see validation message for invalid filtered data.");
                if (agentUserListVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            agentUserListVerification = agentUserListIndexPage.filterLastName(false);

            agentUserListVerification = agentUserListIndexPage.clickOnConsumerFilterSearchButton();

            testVerifyLog("Verify Last Name filter successfully.");
            if (agentUserListVerification.verifyLastNameFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.clearAllConsumerFilterDetails();

            if (!isPositiveExecution) {
                agentUserListVerification = agentUserListIndexPage.filterNumber(true);

                agentUserListVerification = agentUserListIndexPage.clickOnConsumerFilterSearchButton();

                testVerifyLog("User can see validation message for invalid filtered data.");
                if (agentUserListVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            agentUserListVerification = agentUserListIndexPage.filterNumber(false);

            agentUserListVerification = agentUserListIndexPage.clickOnConsumerFilterSearchButton();

            testVerifyLog("Verify Mobile Number filter successfully.");
            if (agentUserListVerification.verifyPhoneNumberFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.clearAllConsumerFilterDetails();

            agentUserListVerification = agentUserListIndexPage.getConsumerStatus();

            agentUserListVerification = agentUserListIndexPage.filterStatus("Active");

            agentUserListVerification = agentUserListIndexPage.clickOnConsumerFilterSearchButton();

            testVerifyLog("Verify Status filter successfully.");
            if (agentUserListVerification.verifyStatusFiltered("Consumer")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.filterStatus("Kyc Pending");

            agentUserListVerification = agentUserListIndexPage.clickOnConsumerFilterSearchButton();

            testVerifyLog("Verify Status filter successfully.");
            if (agentUserListVerification.verifyStatusFiltered("Consumer")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.filterStatus("Login Pending");

            agentUserListVerification = agentUserListIndexPage.clickOnConsumerFilterSearchButton();

            testVerifyLog("Verify Status filter successfully.");
            if (agentUserListVerification.verifyStatusFiltered("Consumer")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

        }

        agentUserListVerification = agentUserListIndexPage.clickOnMerchantTab();

        testVerifyLog("Verify user can see the Filter screen.");
        if (agentCommissionVerification.verifyMerchantFilterScreenDisplay()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentUserListVerification = agentUserListIndexPage.clickOnFilterIcon();

        if (agentUserListIndexPage.isMerchantDataDisplay()) {

            agentUserListVerification = agentUserListIndexPage.getMerchantDetailsForFilter();

            agentUserListVerification = agentUserListIndexPage.clickOnFilterIcon();

            if (!isPositiveExecution) {
                agentUserListVerification = agentUserListIndexPage.filterShopName(true);

                agentUserListVerification = agentUserListIndexPage.clickOnMerchantFilterSearchButton();

                testVerifyLog("User can see validation message for invalid filtered data.");
                if (agentUserListVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            agentUserListVerification = agentUserListIndexPage.filterShopName(false);

            agentUserListVerification = agentUserListIndexPage.clickOnMerchantFilterSearchButton();

            testVerifyLog("Verify Shop Name filter successfully.");
            if (agentUserListVerification.verifyShopNameFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.clearAllMerchantFilterDetails();

            if (!isPositiveExecution) {
                agentUserListVerification = agentUserListIndexPage.filterNumber(true);

                agentUserListVerification = agentUserListIndexPage.clickOnMerchantFilterSearchButton();

                testVerifyLog("User can see validation message for invalid filtered data.");
                if (agentUserListVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            agentUserListVerification = agentUserListIndexPage.filterNumber(false);

            agentUserListVerification = agentUserListIndexPage.clickOnMerchantFilterSearchButton();

            testVerifyLog("Verify Mobile Number filter successfully.");
            if (agentUserListVerification.verifyMerchantNumberFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.clearAllMerchantFilterDetails();

            agentUserListVerification = agentUserListIndexPage.geMerchantStatus();

            agentUserListVerification = agentUserListIndexPage.filterStatus("Active");

            agentUserListVerification = agentUserListIndexPage.clickOnMerchantFilterSearchButton();

            testVerifyLog("Verify Status filter successfully.");
            if (agentUserListVerification.verifyStatusFiltered("Merchant")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.filterStatus("Kyc Pending");

            agentUserListVerification = agentUserListIndexPage.clickOnMerchantFilterSearchButton();

            testVerifyLog("Verify Status filter successfully.");
            if (agentUserListVerification.verifyStatusFiltered("Merchant")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.filterStatus("Login Pending");

            agentUserListVerification = agentUserListIndexPage.clickOnMerchantFilterSearchButton();

            testVerifyLog("Verify Status filter successfully.");
            if (agentUserListVerification.verifyStatusFiltered("Merchant")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");

    }

    @Test
    public void agent_UserListTableActions() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_063 :: To verify Agent can search the Consumers & Merchants Details.");
        agentLoginIndexPage.getVersion();

        agentLoginVerification = agentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (agentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentDashboardVerification = agentDashboardIndexPage.clickOnUsersListMenu();

        testVerifyLog("Verify user can see the Agents List Screen.");
        if (agentUserListVerification.verifyUsersListScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentUserListVerification = agentUserListIndexPage.clickOnConsumerTab();

        if (agentUserListIndexPage.isConsumerDataDisplay()) {

            agentUserListVerification = agentUserListIndexPage.clickConsumerNameToSort();

            testVerifyLog("Verify Consumer Name are sorted successfully.");
            if (agentUserListVerification.verifyConsumerNameSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.clickConsumerNumberToSort();

            testVerifyLog("Verify Consumer Mobile Number are sorted successfully.");
            if (agentUserListVerification.verifyConsumerNumberSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.clickOnConsumerStatusToSort();

            testVerifyLog("Verify Consumer Status are sorted successfully.");
            if (agentUserListVerification.verifyConsumerStatusSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.clickOnConsumerDateTimeToSort();

            testVerifyLog("Verify Date & Time are sorted successfully.");
            if (agentUserListVerification.verifyConsumerDateTimeSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.enterConsumerSearchCriteria(false);

            testVerifyLog("Verify search details display properly.");
            if (agentUserListVerification.verifyConsumerSearchSuccessful()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.enterConsumerSearchCriteria(true);

            testVerifyLog("Verify validation message display for the invalid search criteria.");
            if (agentUserListVerification.verifyConsumerValidationForInvalidSearch()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        agentUserListVerification = agentUserListIndexPage.clickOnMerchantTab();

        if (agentUserListIndexPage.isMerchantDataDisplay()) {

            agentUserListVerification = agentUserListIndexPage.clickMerchantNameToSort();

            testVerifyLog("Verify Merchant Name are sorted successfully.");
            if (agentUserListVerification.verifyMerchantNameSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.clickMerchantNumberToSort();

            testVerifyLog("Verify Merchant Mobile Number are sorted successfully.");
            if (agentUserListVerification.verifyMerchantNumberSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.clickOnMerchantStatusToSort();

            testVerifyLog("Verify Merchant Status are sorted successfully.");
            if (agentUserListVerification.verifyMerchantStatusSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.clickOnMerchantDateTimeToSort();

            testVerifyLog("Verify Date & Time are sorted successfully.");
            if (agentUserListVerification.verifyMerchantDateTimeSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            agentUserListVerification = agentUserListIndexPage.enterMerchantSearchCriteria(false);

            testVerifyLog("Verify search details display properly.");
            if (agentUserListVerification.verifyMerchantSearchSuccessful()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            if (!isPositiveExecution) {
                agentUserListVerification = agentUserListIndexPage.enterMerchantSearchCriteria(true);

                testVerifyLog("Verify validation message display for the invalid search criteria.");
                if (agentUserListVerification.verifyMerchantValidationForInvalidSearch()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }
        }


        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");

    }

}
