package com.incashme.agent.index;

import com.framework.init.SeleniumInit;
import org.testng.Assert;
import org.testng.annotations.Test;

public class AgentStatementIndex extends SeleniumInit {

    @Test
    public void agent_StatementDownload() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_036 :: To verify Agent can download the Monthly Statement.");
        agentLoginIndexPage.getVersion();

        agentLoginVerification = agentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (agentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        createDownloadDirectory();

        agentDashboardVerification = agentDashboardIndexPage.openUserProfile();

        agentStatementVerification = agentStatementIndexPage.getProfileCreatedMonth();

        agentDashboardVerification = agentDashboardIndexPage.closeProfilePopUp();

        agentStatementVerification = agentStatementIndexPage.clickOnStatementMenu();

        testVerifyLog("Verify Monthly statements display since user started the account.");
        if (agentStatementVerification.verifyStatementDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (agentStatementIndexPage.isStatementDisplay()) {

            agentStatementVerification = agentStatementIndexPage.clickOnViewButton();

            testVerifyLog("Verify Statement downloaded successfully.");
            if (agentStatementVerification.verifyStatementDownloaded()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");

    }

    @Test
    public void agent_StatementFilter() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_037 :: To verify Agent can filter the Statements.");
        agentLoginIndexPage.getVersion();

        agentLoginVerification = agentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (agentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        agentStatementVerification = agentStatementIndexPage.clickOnStatementMenu();

        testVerifyLog("Verify user can see the Statement Screen with list of statements.");
        if (agentStatementVerification.verifyStatementsScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (agentStatementIndexPage.isStatementDisplay()) {

            if (!isPositiveExecution) {
                agentStatementVerification = agentStatementIndexPage.selectStatementToFilter(true);

                testVerifyLog("Verify No Statement display for current month.");
                if (agentStatementVerification.verifyNoStatementDisplay()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            agentStatementVerification = agentStatementIndexPage.clickOnFilterButton();

            agentStatementVerification = agentStatementIndexPage.selectStatementToFilter(false);

            testVerifyLog("Verify Statement filtered successfully.");
            if (agentStatementVerification.verifyStatementFilterSuccessfully()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");

    }

}
