package com.incashme.agent.indexpage;

import com.framework.init.AbstractPage;
import com.incashme.agent.verification.AgentTargetTopUpDetailsVerification;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Rahul R.
 * Date: 2019-04-25
 * Time: 10:37
 * Project Name: InCashMe
 */
public class AgentTargetTopUpDetailsIndexPage extends AbstractPage {

    public AgentTargetTopUpDetailsIndexPage(WebDriver driver) {
        super(driver);
    }

    public static int _targetAchievedInTopUpDetails;
    public static int _targetLeftInTopUpDetails;
    public static int _daysCompletedInTopUpDetails;
    public static int _daysLeftInTopUpDetails;
    public static int _totalDaysInTopUpDetails;
    public static int _totalTargetForTopUp;

    public static String _searchCriteria;

    public static List<String> _consumerNameBeforeSort = new ArrayList<>();
    public static List<String> _consumerNumberBeforeSort = new ArrayList<>();

    public static String _filterConsumerFirstName = "";
    public static String _filterConsumerLastName = "";
    public static String _filterConsumerPhone = "";
    public static String _filterStatus = "";

    public static String _filterRandomValue = "";
    public static boolean _isRandomFilter;

    Map<WebElement, String> mapRechargeStatus = new HashMap<>();
    public static int _minTopUpAmount;
    public static int _maxTopUpAmount;

    @FindBy(xpath = "//div[contains(text(),'Top-Up')]//following-sibling::div//span[contains(@class,'chart-velue')]")
    private WebElement lblComletedTopups;

    @FindBy(xpath = "//div[contains(text(),'User')]//following-sibling::div//span[contains(@class,'chart-velue')]")
    private WebElement lblAddedUsersInChart;

    @FindBy(xpath = "//div[contains(text(),'Consumers')]//following-sibling::div//span[contains(@class,'chart-velue')]")
    private WebElement lblAddedConsumersInChart;

    @FindBy(xpath = "//div[contains(text(),'Merchants')]//following-sibling::div//span[contains(@class,'chart-velue')]")
    private WebElement lblAddedMerchantsInChart;

    @FindBy(xpath = "//div[contains(text(),'Target Left')]/following-sibling::div")
    private WebElement lblTaregtLeftInChart;

    @FindBy(xpath = "//div[contains(text(),'Total Target') or contains(text(),'Target Achieved')]/following-sibling::div")
    private WebElement lblTotalTargetInChart;

    @FindBy(xpath = "//div[contains(text(),'Time')]//following-sibling::div//span[contains(@class,'chart-velue')]")
    private WebElement lblDaysCompletedInChart;

    @FindBy(xpath = "//div[contains(text(),'Days Left')]/following-sibling::div")
    private WebElement lblDaysLeftInChart;

    @FindBy(xpath = "//div[text()='Target']/following-sibling::div")
    private WebElement lblTargetDaysInChart;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[1]//h5")})
    private List<WebElement> lstTopUpConsumerName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[2]//h5")})
    private List<WebElement> lstTopUpConsumerNumber;

    @FindBy(xpath = "//table[@id='dataTbl']//th[1]")
    private WebElement lblConsumerNameHeader;

    @FindBy(xpath = "//table[@id='dataTbl']//th[2]")
    private WebElement lblConsumerNumberHeader;

    @FindBy(xpath = "//table[@id='dataTbl']//th[3]")
    private WebElement lblConsumerStatusHeader;

    @FindBy(xpath = "//table[@id='dataTbl']//th[4]")
    private WebElement lblConsumerDateTimeHeader;

    @FindBy(xpath = "//input[@type='search']")
    private WebElement txtInputSearch;

    @FindBy(xpath = "//li/button[contains(@class,'filterBtn')]")
    private WebElement btnFilter;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//button[contains(@class,'searchbtn')]")
    private WebElement btnFilterSearch;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//a[contains(text(),'Clear All')]")
    private WebElement btnFilterClearAll;

    @FindBy(xpath = "//input[@formcontrolname='fname']")
    private WebElement txtFilterFirstName;

    @FindBy(xpath = "//input[@formcontrolname='lname']")
    private WebElement txtFilterLastName;

    @FindBy(xpath = "//input[@formcontrolname='phone']")
    private WebElement txtFilterConsumerMobile;

    @FindBy(xpath = "//div//a[contains(text(),'Clear Date Range')]")
    private WebElement btnClearDateRange;

    @FindAll(value = {@FindBy(xpath = "//mdb-select[@formcontrolname='status']//li/span")})
    private List<WebElement> lstStatuses;

    @FindBy(xpath = "//mdb-select[@formcontrolname='status']")
    private WebElement btnStatusDropdown;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[3]//div")})
    private List<WebElement> lstConsumerStatus;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[1]//h5")})
    private List<WebElement> lstConsumerName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[2]/h5")})
    private List<WebElement> lstConsumerNumber;

    @FindAll(value = {@FindBy(xpath = "//table[contains(@id,'dataTbl')]//tr/td[3]//div[contains(@class,'status-txt')]")})
    private List<WebElement> lstFirstTimeRecharge;

    @FindAll(value = {@FindBy(xpath = "//table[contains(@id,'dataTbl')]//tr/td[4]//div[contains(@class,'status-txt')]")})
    private List<WebElement> lstSecondTimeRecharge;

    @FindBy(xpath = "//input[@id='amount']")
    private WebElement txtTopUpAmount;

    @FindBy(xpath = "//div[contains(@class,'modal-content')]//button[contains(@class,'btn-green') and text()='SEND']")
    private WebElement btnSendTopUp;

    @FindBy(xpath = "//div[contains(@class,'modal-content')]//button[contains(@class,'btn-green') and text()='Close']")
    private WebElement btnCloseTopUpPopUp;

    @FindBy(xpath = "//div[contains(@class,'tpup-pnk-txt') and contains(text(),'Greater than')]")
    private WebElement lblInvalidTopUpValidation;

    public AgentTargetTopUpDetailsVerification getTotalTargetTopupDetails() {

        pause(2);

        _targetAchievedInTopUpDetails = getIntegerFromString(getInnerText(lblAddedUsersInChart));
        _targetLeftInTopUpDetails = getIntegerFromString(getInnerText(lblTaregtLeftInChart));
        _totalTargetForTopUp = getIntegerFromString(getInnerText(lblTotalTargetInChart));
        _daysCompletedInTopUpDetails = getIntegerFromString(getInnerText(lblDaysCompletedInChart));
        _daysLeftInTopUpDetails = getIntegerFromString(getInnerText(lblDaysLeftInChart));
        _totalDaysInTopUpDetails = getIntegerFromString(getInnerText(lblTargetDaysInChart));

        testInfoLog("Total Target Completed : ", String.valueOf(_targetAchievedInTopUpDetails));
        testInfoLog("Total Target Left : ", String.valueOf(_targetLeftInTopUpDetails));
        testInfoLog("Total Target", String.valueOf(_totalTargetForTopUp));
        testInfoLog("Total Days Completed : ", String.valueOf(_daysCompletedInTopUpDetails));
        testInfoLog("Total Days Left : ", String.valueOf(_daysLeftInTopUpDetails));
        testInfoLog("Total Days : ", String.valueOf(_totalDaysInTopUpDetails));

        return new AgentTargetTopUpDetailsVerification(driver);

    }

    public AgentTargetTopUpDetailsVerification clickTopUpConsumerNameToSort() {

        try {
            scrollToElement(driver, findElementByName(driver, "dataTbl_length"));
            updateShowList(driver, findElementByName(driver, "dataTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View display.");
        }

        for (WebElement name : lstTopUpConsumerName) {
            _consumerNameBeforeSort.add(getInnerText(name));
        }

        testStepsLog(_logStep++, "Click On Name to sort.");
        clickOn(driver, lblConsumerNameHeader);
        clickOn(driver, lblConsumerNameHeader);

        return new AgentTargetTopUpDetailsVerification(driver);
    }

    public AgentTargetTopUpDetailsVerification clickTopUpConsumerNumberToSort() {

        _consumerNumberBeforeSort.clear();

        try {
            scrollToElement(driver, findElementByName(driver, "dataTbl_length"));
            updateShowList(driver, findElementByName(driver, "dataTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View display.");
        }

        for (WebElement name : lstTopUpConsumerNumber) {
            _consumerNumberBeforeSort.add(getInnerText(name));
        }

        testStepsLog(_logStep++, "Click On Mobile Number to sort.");
        clickOn(driver, lblConsumerNumberHeader);

        return new AgentTargetTopUpDetailsVerification(driver);
    }

    public AgentTargetTopUpDetailsVerification enterTopUpConsumerSearchCriteria(boolean isInvalid) {

        if (isInvalid) {

            scrollToElement(driver, txtInputSearch);

            String searchCriteria = getRandomCharacters(10);
            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", searchCriteria);
            type(txtInputSearch, searchCriteria);

        } else {

            scrollToElement(driver, txtInputSearch);
            String number = getInnerText(lstTopUpConsumerNumber.get(0));
            _searchCriteria = number.substring(number.length() - 10);

            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", _searchCriteria);
            type(txtInputSearch, _searchCriteria);
        }

        return new AgentTargetTopUpDetailsVerification(driver);
    }

    public AgentTargetTopUpDetailsVerification clickOnFilterButton() {

        testStepsLog(_logStep++, "Click Filter button.");
        clickOn(driver, btnFilter);

        return new AgentTargetTopUpDetailsVerification(driver);
    }

    public AgentTargetTopUpDetailsVerification filterFirstName(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomCharacters(7);
            testStepsLog(_logStep++, "Enter First Name : " + _filterRandomValue);
            type(txtFilterFirstName, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter First Name : " + _filterConsumerFirstName);
            type(txtFilterFirstName, _filterConsumerFirstName);
        }

        return new AgentTargetTopUpDetailsVerification(driver);
    }

    public AgentTargetTopUpDetailsVerification clickOnFilterSearchButton() {

        testStepsLog(_logStep++, "Click on Search button.");
        clickOn(driver, btnFilterSearch);

        pause(2);

        try {
            updateShowList(driver, findElementByName(driver, "dataTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        pause(1);

        return new AgentTargetTopUpDetailsVerification(driver);

    }

    public AgentTargetTopUpDetailsVerification clearAllFilterDetails() {

        testStepsLog(_logStep++, "Click on Clear All button.");
        clickOn(driver, btnFilterClearAll);

        pause(2);

        try {
            updateShowList(driver, findElementByName(driver, "agentsTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        pause(1);

        return new AgentTargetTopUpDetailsVerification(driver);

    }

    public AgentTargetTopUpDetailsVerification filterLastName(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomCharacters(7);
            testStepsLog(_logStep++, "Enter Last Name : " + _filterRandomValue);
            type(txtFilterLastName, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter Last Name : " + _filterConsumerLastName);
            type(txtFilterLastName, _filterConsumerLastName);
        }

        return new AgentTargetTopUpDetailsVerification(driver);
    }

    public AgentTargetTopUpDetailsVerification filterConsumerNumber(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomNumber() + "" + getRandomNumber();
            testStepsLog(_logStep++, "Enter Mobile Number : " + _filterRandomValue);
            type(txtFilterConsumerMobile, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter Mobile Number : " + _filterConsumerPhone.split(" ")[1]);
            type(txtFilterConsumerMobile, _filterConsumerPhone.split(" ")[1]);
        }

        return new AgentTargetTopUpDetailsVerification(driver);
    }

    public AgentTargetTopUpDetailsVerification filterStatus(String status) {

        scrollToElement(driver, btnClearDateRange);

        clickOn(driver, btnStatusDropdown);

        _filterStatus = status;

        switch (status.toLowerCase()) {
            case "active":
                clickOn(driver, lstStatuses.get(1));
                break;
            case "kyc pending":
                clickOn(driver, lstStatuses.get(2));
                break;
            case "login pending":
                clickOn(driver, lstStatuses.get(3));
                break;
            default:
                clickOn(driver, lstStatuses.get(0));
                break;
        }

        testStepsLog(_logStep++, "Select Status : " + status);

        return new AgentTargetTopUpDetailsVerification(driver);

    }

    public AgentTargetTopUpDetailsVerification getConsumerStatus() {

        AgentConsumerTargetDetailsIndexPage._consumerStatusBeforeSort.clear();

        for (WebElement status : lstConsumerStatus) {
            AgentConsumerTargetDetailsIndexPage._consumerStatusBeforeSort.add(getInnerText(status));
        }

        return new AgentTargetTopUpDetailsVerification(driver);
    }

    public AgentTargetTopUpDetailsVerification getConsumerDetailsForFilter() {

        pause(2);

        _filterConsumerFirstName = getInnerText(lstConsumerName.get(0)).split(" ")[0].trim();
        _filterConsumerLastName = getInnerText(lstConsumerName.get(0)).replace(_filterConsumerFirstName, "").trim();
        _filterConsumerPhone = getInnerText(lstConsumerNumber.get(0));

        return new AgentTargetTopUpDetailsVerification(driver);

    }

    public AgentTargetTopUpDetailsVerification clickOnRechargeButton() {

        for (Map.Entry<WebElement, String> entry : mapRechargeStatus.entrySet()) {
            if (entry.getValue().equalsIgnoreCase("Pending")) {
                scrollToElement(driver, entry.getKey());
                clickOn(driver, entry.getKey());
                break;
            }
        }

        return new AgentTargetTopUpDetailsVerification(driver);

    }

    public boolean isFirstTimeConsumerRemaining() {

        try {
            updateShowList(driver, findElementByName(driver, "dataTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        for (int index = 0; index < sizeOf(lstFirstTimeRecharge); index++) {
            try {
                isElementDisplay(findElementByXPath(driver,
                        "//table[contains(@id,'dataTbl')]//tr[" + (index + 1) + "]/td[1]//h5//following-sibling::div[contains(text(),'Kyc Verification')]"));
            } catch (Exception e) {
                mapRechargeStatus.put(lstFirstTimeRecharge.get(index), getInnerText(lstFirstTimeRecharge.get(index)));
            }
        }

        return mapRechargeStatus.containsValue("Pending");
    }

    public boolean isSecondTimeConsumerRemaining() {

        mapRechargeStatus.clear();

        try {
            updateShowList(driver, findElementByName(driver, "dataTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        for (int index = 0; index < sizeOf(lstSecondTimeRecharge); index++) {
            try {
                isElementDisplay(findElementByXPath(driver,
                        "//table[contains(@id,'dataTbl')]//tr[" + (index + 1) + "]/td[1]//h5//following-sibling::div[contains(text(),'Kyc Verification')]"));
            } catch (Exception e) {
                mapRechargeStatus.put(lstSecondTimeRecharge.get(index), getInnerText(lstSecondTimeRecharge.get(index)));
            }
        }

        return mapRechargeStatus.containsValue("Pending");
    }

    public AgentTargetTopUpDetailsVerification enterZeroTopUpAmount() {

        AgentDashboardIndexPage._topUpAmount = 0;

        testStepsLog(_logStep++, "Enter Amount to TopUp");
        testInfoLog("TopUp Amount", String.valueOf(AgentDashboardIndexPage._topUpAmount));
        type(txtTopUpAmount, String.valueOf(AgentDashboardIndexPage._topUpAmount));

        return new AgentTargetTopUpDetailsVerification(driver);

    }

    public AgentTargetTopUpDetailsVerification enterInvalidTopUpAmount() {

        AgentDashboardIndexPage._topUpAmount = _minTopUpAmount - 1;

        testStepsLog(_logStep++, "Enter Amount to TopUp");
        testInfoLog("TopUp Amount", String.valueOf(AgentDashboardIndexPage._topUpAmount));
        type(txtTopUpAmount, String.valueOf(AgentDashboardIndexPage._topUpAmount));

        return new AgentTargetTopUpDetailsVerification(driver);

    }

    public AgentTargetTopUpDetailsVerification enterMaxBalanceTopUpAmount() {

        AgentDashboardIndexPage._topUpAmount = (int) Math.round(AgentDashboardIndexPage.balanceBeforeTopUp) + 1;

        testStepsLog(_logStep++, "Enter Amount to TopUp");
        testInfoLog("TopUp Amount", String.valueOf(AgentDashboardIndexPage._topUpAmount));
        type(txtTopUpAmount, String.valueOf(AgentDashboardIndexPage._topUpAmount));

        return new AgentTargetTopUpDetailsVerification(driver);

    }

    public AgentTargetTopUpDetailsVerification enterValidTopUpAmount() {

        AgentDashboardIndexPage._topUpAmount = _minTopUpAmount;

        testStepsLog(_logStep++, "Enter Amount to TopUp");
        testInfoLog("TopUp Amount", String.valueOf(AgentDashboardIndexPage._topUpAmount));
        type(txtTopUpAmount, String.valueOf(AgentDashboardIndexPage._topUpAmount));

        return new AgentTargetTopUpDetailsVerification(driver);

    }

    public AgentTargetTopUpDetailsVerification clickOnCompleteTopUpButton() {

        if (AgentDashboardIndexPage.balanceBeforeTopUp < _maxTopUpAmount) {
            testStepsLog(_logStep++, "Click On SEND Button.");
            clickOn(driver, btnSendTopUp);
        } else if (AgentDashboardIndexPage.balanceBeforeTopUp > _minTopUpAmount) {
            testStepsLog(_logStep++, "Click On SEND Button.");
            clickOn(driver, btnSendTopUp);
        }

        return new AgentTargetTopUpDetailsVerification(driver);
    }

    public AgentTargetTopUpDetailsVerification clickOnCloseTopUpButton() {
        testStepsLog(_logStep++, "Click On CLOSE Button.");
        clickOn(driver, btnCloseTopUpPopUp);
        return new AgentTargetTopUpDetailsVerification(driver);
    }


    public AgentTargetTopUpDetailsVerification getConsumerTopUpAmount() {

        String topUp = getText(lblInvalidTopUpValidation).substring(38, 44);
        String maxTopUP = getText(lblInvalidTopUpValidation).substring(getText(lblInvalidTopUpValidation).length() - 7,
                getText(lblInvalidTopUpValidation).length() - 1);

        System.out.println(topUp.trim());
        System.out.println(getDoubleFromString(topUp));
        System.out.println(getDoubleFromString(maxTopUP));
        _minTopUpAmount = (int) getDoubleFromString(topUp);
        _maxTopUpAmount = (int) getDoubleFromString(maxTopUP);
        return new AgentTargetTopUpDetailsVerification(driver);
    }
}
