package com.incashme.agent.indexpage;

import com.framework.init.AbstractPage;
import com.incashme.agent.verification.AgentCommissionVerification;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Rahul R.
 * Date: 2019-04-26
 * Time: 16:20
 * Project Name: InCashMe
 */
public class AgentCommissionIndexPage extends AbstractPage {

    public AgentCommissionIndexPage(WebDriver driver) {
        super(driver);
    }

    public static String _searchCriteria;

    public static double _totalCommissionAmount;

    public static List<Double> _commissionTopUpBeforeSort = new ArrayList<>();
    public static List<Double> _commissionAmountBeforeSort = new ArrayList<>();
    public static List<LocalDateTime> _commissionDateTimeBeforeSort = new ArrayList<>();

    public static List<Double> _commissionTopUpForFilter = new ArrayList<>();
    public static List<Double> _commissionAmountForFilter = new ArrayList<>();

    public static String _filterFirstName = "";
    public static String _filterLastName = "";
    public static String _filterShopName = "";
    public static String _filterPhone = "";
    public static String _filterID = "";
    public static String _filterStatus = "";

    public static double _filterTopUpFromAmount;
    public static double _filterTopUpToAmount;
    public static double _filterCommissionFromAmount;
    public static double _filterCommissionToAmount;

    public static String _filterRandomValue = "";
    public static boolean _isRandomFilter;

    @FindBy(xpath = "//li/button[contains(@class,'filterBtn')]")
    private WebElement btnFilter;

    @FindBy(xpath = "(//input[@type='search'])[1]")
    private WebElement txtInputConsumerSearch;

    @FindBy(xpath = "(//input[@type='search'])[2]")
    private WebElement txtInputMerchantSearch;

    @FindBy(xpath = "//div[contains(@class,'cmsn-tabs')]//li//a[text()='Merchant']")
    private WebElement btnCommissionMerchant;

    @FindBy(xpath = "//div[contains(@class,'cmsn-tabs')]//li//a[text()='Consumer']")
    private WebElement btnCommissionConsumer;

    @FindBy(xpath = "//label[@id='p_total']")
    private WebElement btnChartTotal;

    @FindBy(xpath = "//span[text()='Total']//following-sibling::span")
    private WebElement lblTotalCommission;

    @FindBy(xpath = "//div//p[contains(text(),'no data alive')]")
    private WebElement lblNoCommission;

    @FindAll(value = {@FindBy(xpath = "//table[@id='consumer-trans-history-table']//tr//td[2]/span")})
    private List<WebElement> lstConsumerTxId;

    @FindAll(value = {@FindBy(xpath = "//table[@id='merchant-trans-history-table']//tr//td[2]/span")})
    private List<WebElement> lstMerchantTxId;

    @FindAll(value = {@FindBy(xpath = "//input[@type='search']")})
    private List<WebElement> txtInputSearch;

    @FindAll(value = {@FindBy(xpath = "//table[@id='consumer-trans-history-table']//tr//td[1]//h5/span")})
    private List<WebElement> lstConsumerName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='consumer-trans-history-table']//tr//td[1]//following-sibling::div")})
    private List<WebElement> lstConsumerNumber;

    @FindAll(value = {@FindBy(xpath = "//table[@id='consumer-trans-history-table']//tr//td[3]")})
    private List<WebElement> lstConsumerTopUpAmount;

    @FindAll(value = {@FindBy(xpath = "//table[@id='consumer-trans-history-table']//tr//td[4]")})
    private List<WebElement> lstConsumerCommissionAmount;

    @FindAll(value = {@FindBy(xpath = "//table[@id='consumer-trans-history-table']//tr//td[5]")})
    private List<WebElement> lstConsumerTransactionTime;

    @FindAll(value = {@FindBy(xpath = "//table[@id='merchant-trans-history-table']//tr//td[1]//h5/span")})
    private List<WebElement> lstMerchantName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='merchant-trans-history-table']//tr//td[1]//following-sibling::div")})
    private List<WebElement> lstMerchantID;

    @FindAll(value = {@FindBy(xpath = "//table[@id='merchant-trans-history-table']//tr//td[3]")})
    private List<WebElement> lstMerchantTopUpAmount;

    @FindAll(value = {@FindBy(xpath = "//table[@id='merchant-trans-history-table']//tr//td[4]")})
    private List<WebElement> lstMerchantCommissionAmount;

    @FindAll(value = {@FindBy(xpath = "//table[@id='merchant-trans-history-table']//tr//td[5]")})
    private List<WebElement> lstMerchantTransactionTime;

    @FindBy(xpath = "//table[@id='consumer-trans-history-table']//th[3]")
    private WebElement lblConsumerTopupHeader;

    @FindBy(xpath = "//table[@id='consumer-trans-history-table']//th[4]")
    private WebElement lblConsumerCommissionHeader;

    @FindBy(xpath = "//table[@id='consumer-trans-history-table']//th[5]")
    private WebElement lblConsumerCommissionDateTimeHeader;

    @FindBy(xpath = "//table[@id='merchant-trans-history-table']//th[3]")
    private WebElement lblMerchantTopupHeader;

    @FindBy(xpath = "//table[@id='merchant-trans-history-table']//th[4]")
    private WebElement lblMerchantCommissionHeader;

    @FindBy(xpath = "//table[@id='merchant-trans-history-table']//th[5]")
    private WebElement lblMerchantCommissionDateTimeHeader;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//button[contains(@class,'searchbtn')]")
    private WebElement btnFilterSearch;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//a[contains(text(),'Clear All')]")
    private WebElement btnFilterClearAll;

    @FindBy(xpath = "//input[@formcontrolname='to_fname']")
    private WebElement txtFilterFirstName;

    @FindBy(xpath = "//input[@formcontrolname='to_lname']")
    private WebElement txtFilterLastName;

    @FindBy(xpath = "//input[@formcontrolname='to_cellnum']")
    private WebElement txtFilterConsumerMobile;

    @FindBy(xpath = "//h4[text()='Commissions']")
    private WebElement lblCommissionHeader;

    @FindBy(xpath = "//input[@formcontrolname='min_amount']")
    private WebElement txtFilterTopUpMinAmount;

    @FindBy(xpath = "//input[@formcontrolname='max_amount']")
    private WebElement txtFilterTopUpMaxAmount;

    @FindBy(xpath = "//input[@formcontrolname='comm_min_amount']")
    private WebElement txtFilterCommissionMinAmount;

    @FindBy(xpath = "//input[@formcontrolname='comm_max_amount']")
    private WebElement txtFilterCommissionMaxAmount;

    @FindBy(xpath = "//input[@formcontrolname='mi_companyname']")
    private WebElement txtFilterShopName;

    @FindBy(xpath = "//input[@formcontrolname='to_user_id']")
    private WebElement txtFilterID;

    public AgentCommissionVerification clickOnFilterIcon() {

        scrollToElement(driver, btnFilter);

        testStepsLog(_logStep++, "Click on Filter Button.");
        clickOn(driver, btnFilter);

        pause(2);

        return new AgentCommissionVerification(driver);

    }

    public AgentCommissionVerification searchRecentConsumerTransaction() {

        pause(3);

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        testStepsLog(_logStep++, "Search recent completed transaction.");

        scrollToElement(driver, txtInputConsumerSearch);

        String txTime = format.format(AgentDashboardIndexPage._topUpDateTime);

        _searchCriteria = txTime.split(" ")[0];

        testStepsLog(_logStep++, "Enter Search Criteria");
        testInfoLog("Search", _searchCriteria);
        type(txtInputConsumerSearch, _searchCriteria);

        return new AgentCommissionVerification(driver);
    }

    public AgentCommissionVerification searchRecentMerchantTransaction() {

        pause(3);

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        testStepsLog(_logStep++, "Search recent completed transaction.");

        scrollToElement(driver, txtInputMerchantSearch);

        String txTime = format.format(AgentDashboardIndexPage._topUpDateTime);

        _searchCriteria = txTime.split(" ")[0];

        testStepsLog(_logStep++, "Enter Search Criteria");
        testInfoLog("Search", _searchCriteria);
        type(txtInputMerchantSearch, _searchCriteria);

        return new AgentCommissionVerification(driver);
    }


    public AgentCommissionVerification clickOnMerchantTab() {

        testStepsLog(_logStep++, "Click On Merchant Tab.");
        clickOn(driver, btnCommissionMerchant);

        pause(3);

        return new AgentCommissionVerification(driver);

    }

    public AgentCommissionVerification clickOnTotalButton() {

        testStepsLog(_logStep++, "Click on Total button.");
        scrollToElement(driver, btnChartTotal);
        clickOn(driver, btnChartTotal);

        return new AgentCommissionVerification(driver);
    }

    public AgentCommissionVerification getTotalCommissionAmount() {

        pause(2);

        _totalCommissionAmount = getDoubleFromString(getInnerText(lblTotalCommission));

        return new AgentCommissionVerification(driver);
    }

    public boolean isConsumerCommissionDataDisplay() {

        try {
            updateShowList(driver, findElementByName(driver, "consumer-trans-history-table_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        return !isElementPresent(lblNoCommission);
    }

    public boolean isMerchantCommissionDataDisplay() {

        try {
            updateShowList(driver, findElementByName(driver, "merchant-trans-history-table_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        _commissionTopUpBeforeSort.clear();
        _commissionAmountBeforeSort.clear();
        _commissionDateTimeBeforeSort.clear();

        return !isElementPresent(lblNoCommission);
    }

    public AgentCommissionVerification enterConsumerSearchIDCriteria(boolean isInvalid) {

        if (isInvalid) {

            scrollToElement(driver, txtInputSearch.get(0));

            String searchCriteria = String.valueOf(getRandomNumber());
            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", searchCriteria);
            type(txtInputSearch.get(0), searchCriteria);

        } else {

            scrollToElement(driver, txtInputSearch.get(0));

            _searchCriteria = getInnerText(lstConsumerTxId.get(getRandomNumberBetween(0, lastIndexOf(lstConsumerTxId)))).split("-")[1];

            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", _searchCriteria);
            type(txtInputSearch.get(0), _searchCriteria);
        }

        return new AgentCommissionVerification(driver);
    }

    public AgentCommissionVerification enterMerchantSearchIDCriteria(boolean isInvalid) {

        if (isInvalid) {

            scrollToElement(driver, txtInputSearch.get(1));

            String searchCriteria = String.valueOf(getRandomNumber());
            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", searchCriteria);
            type(txtInputSearch.get(1), searchCriteria);

        } else {

            scrollToElement(driver, txtInputSearch.get(1));

            _searchCriteria = getInnerText(lstMerchantTxId.get(getRandomNumberBetween(0, lastIndexOf(lstMerchantTxId)))).split("-")[1];

            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", _searchCriteria);
            type(txtInputSearch.get(1), _searchCriteria);
        }

        return new AgentCommissionVerification(driver);
    }

    public AgentCommissionVerification clickConsumerTopUpToSort() {

        try {
            updateShowList(driver, findElementByName(driver, "consumer-trans-history-table_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        for (WebElement name : lstConsumerTopUpAmount) {
            _commissionTopUpBeforeSort.add(getDoubleFromString(getInnerText(name)));
        }

        testStepsLog(_logStep++, "Click On Top up to sort.");
        clickOn(driver, lblConsumerTopupHeader);

        return new AgentCommissionVerification(driver);
    }

    public AgentCommissionVerification clickMerchantTopUpToSort() {

        try {
            updateShowList(driver, findElementByName(driver, "merchant-trans-history-table_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        for (WebElement name : lstMerchantTopUpAmount) {
            _commissionTopUpBeforeSort.add(getDoubleFromString(getInnerText(name)));
        }

        testStepsLog(_logStep++, "Click On Top up to sort.");
        clickOn(driver, lblMerchantTopupHeader);

        return new AgentCommissionVerification(driver);
    }

    public AgentCommissionVerification clickConsumerCommissionToSort() {

        for (WebElement name : lstConsumerCommissionAmount) {
            _commissionAmountBeforeSort.add(getDoubleFromString(getInnerText(name)));
        }

        testStepsLog(_logStep++, "Click On Commission to sort.");
        clickOn(driver, lblConsumerCommissionHeader);

        return new AgentCommissionVerification(driver);
    }

    public AgentCommissionVerification clickMerchantCommissionToSort() {

        for (WebElement name : lstMerchantCommissionAmount) {
            _commissionAmountBeforeSort.add(getDoubleFromString(getInnerText(name)));
        }

        testStepsLog(_logStep++, "Click On Commission to sort.");
        clickOn(driver, lblMerchantCommissionHeader);

        return new AgentCommissionVerification(driver);
    }

    public AgentCommissionVerification clickOnConsumerCommissionDateTimeToSort() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        for (WebElement status : lstConsumerTransactionTime) {
            _commissionDateTimeBeforeSort.add(LocalDateTime.parse(getInnerText(status), format));
        }

        testStepsLog(_logStep++, "Click On Date & Time to sort.");
        clickOn(driver, lblConsumerCommissionDateTimeHeader);

        return new AgentCommissionVerification(driver);
    }

    public AgentCommissionVerification clickOnMerchantCommissionDateTimeToSort() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        for (WebElement status : lstMerchantTransactionTime) {
            _commissionDateTimeBeforeSort.add(LocalDateTime.parse(getInnerText(status), format));
        }

        testStepsLog(_logStep++, "Click On Date & Time to sort.");
        clickOn(driver, lblMerchantCommissionDateTimeHeader);

        return new AgentCommissionVerification(driver);
    }

    public AgentCommissionVerification clickConsumerOnFilterSearchButton() {

        testStepsLog(_logStep++, "Click on Search button.");
        clickOn(driver, btnFilterSearch);

        pause(2);

        try {
            updateShowList(driver, findElementByName(driver, "consumer-trans-history-table_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        pause(1);

        return new AgentCommissionVerification(driver);

    }

    public AgentCommissionVerification clickMerchantOnFilterSearchButton() {

        testStepsLog(_logStep++, "Click on Search button.");
        clickOn(driver, btnFilterSearch);

        pause(2);

        try {
            updateShowList(driver, findElementByName(driver, "merchant-trans-history-table_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        pause(1);

        return new AgentCommissionVerification(driver);

    }

    public AgentCommissionVerification clearAllConsumerFilterDetails() {

        testStepsLog(_logStep++, "Click on Clear All button.");
        clickOn(driver, btnFilterClearAll);

        pause(2);

        try {
            updateShowList(driver, findElementByName(driver, "consumer-trans-history-table_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        pause(1);

        return new AgentCommissionVerification(driver);

    }

    public AgentCommissionVerification clearAllMerchantFilterDetails() {

        testStepsLog(_logStep++, "Click on Clear All button.");
        clickOn(driver, btnFilterClearAll);

        pause(2);

        try {
            updateShowList(driver, findElementByName(driver, "merchant-trans-history-table_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        pause(1);

        return new AgentCommissionVerification(driver);

    }

    public AgentCommissionVerification getConsumerDetailsForFilter() {

        pause(2);

        _filterFirstName = getInnerText(lstConsumerName.get(0)).split(" ")[0].trim();
        _filterLastName = getInnerText(lstConsumerName.get(0)).replace(_filterFirstName, "").trim();
        _filterPhone = getInnerText(lstConsumerNumber.get(0));

        for (int num = 0; num < sizeOf(lstConsumerCommissionAmount); num++) {
            _commissionTopUpForFilter.add(getDoubleFromString(getInnerText(lstConsumerTopUpAmount.get(num))));
            _commissionAmountForFilter.add(getDoubleFromString(getInnerText(lstConsumerCommissionAmount.get(num))));
        }

        Collections.sort(_commissionAmountForFilter);
        Collections.sort(_commissionTopUpForFilter);

        return new AgentCommissionVerification(driver);

    }

    public AgentCommissionVerification getMerchantDetailsForFilter() {

        pause(2);

        _commissionTopUpForFilter.clear();
        _commissionAmountForFilter.clear();

        _filterShopName = getInnerText(lstMerchantName.get(0));
        _filterID = getInnerText(lstMerchantID.get(0));

        for (int num = 0; num < sizeOf(lstMerchantCommissionAmount); num++) {
            _commissionTopUpForFilter.add(getDoubleFromString(getInnerText(lstMerchantTopUpAmount.get(num))));
            _commissionAmountForFilter.add(getDoubleFromString(getInnerText(lstMerchantCommissionAmount.get(num))));
        }

        Collections.sort(_commissionAmountForFilter);
        Collections.sort(_commissionTopUpForFilter);

        return new AgentCommissionVerification(driver);

    }

    public AgentCommissionVerification filterFirstName(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomCharacters(7);
            testStepsLog(_logStep++, "Enter First Name : " + _filterRandomValue);
            type(txtFilterFirstName, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter First Name : " + _filterFirstName);
            type(txtFilterFirstName, _filterFirstName);
        }

        return new AgentCommissionVerification(driver);
    }

    public AgentCommissionVerification filterShopName(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomCharacters(7);
            testStepsLog(_logStep++, "Enter Shop Name : " + _filterRandomValue);
            type(txtFilterShopName, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter Shop Name : " + _filterShopName);
            type(txtFilterShopName, _filterShopName);
        }

        return new AgentCommissionVerification(driver);
    }

    public AgentCommissionVerification filterLastName(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomCharacters(7);
            testStepsLog(_logStep++, "Enter Last Name : " + _filterRandomValue);
            type(txtFilterLastName, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter Last Name : " + _filterLastName);
            type(txtFilterLastName, _filterLastName);
        }

        return new AgentCommissionVerification(driver);
    }

    public AgentCommissionVerification filterConsumerNumber(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomNumber() + "" + getRandomNumber();
            testStepsLog(_logStep++, "Enter Mobile Number : " + _filterRandomValue);
            type(txtFilterConsumerMobile, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter Mobile Number : " + _filterPhone.substring(_filterPhone.length() - 4));
            type(txtFilterConsumerMobile, _filterPhone.substring(_filterPhone.length() - 4));
        }

        return new AgentCommissionVerification(driver);
    }

    public AgentCommissionVerification enterTopUpAmountRange(boolean isInvalid) {

        _isRandomFilter = isInvalid;
        scrollToElement(driver, txtFilterTopUpMaxAmount);

        if (isInvalid) {
            double randomNumber = getRandomNumber();
            _filterTopUpFromAmount = randomNumber + 10;
            _filterTopUpToAmount = randomNumber;

            testStepsLog(_logStep++, "Enter From Amount Range : " + _filterTopUpFromAmount);
            type(txtFilterTopUpMinAmount, String.valueOf(_filterTopUpFromAmount));

            testStepsLog(_logStep++, "Enter To Amount Range : " + _filterTopUpToAmount);
            type(txtFilterTopUpMaxAmount, String.valueOf(_filterTopUpToAmount));
        } else {
            _filterTopUpFromAmount = formatTwoDecimal(findMin(_commissionTopUpForFilter)) - 100;
            _filterTopUpToAmount = formatTwoDecimal(findMax(_commissionTopUpForFilter)) + 100;

            testStepsLog(_logStep++, "Enter From Amount Range : " + _filterTopUpFromAmount);
            type(txtFilterTopUpMinAmount, String.valueOf(_filterTopUpFromAmount));

            testStepsLog(_logStep++, "Enter To Amount Range : " + _filterTopUpToAmount);
            type(txtFilterTopUpMaxAmount, String.valueOf(_filterTopUpToAmount));
        }

        return new AgentCommissionVerification(driver);

    }

    public AgentCommissionVerification enterTopUpAmount(String field) {

        scrollToElement(driver, txtFilterTopUpMaxAmount);

        System.out.println(_commissionTopUpForFilter);

        _filterTopUpFromAmount = formatTwoDecimal(findMin(_commissionTopUpForFilter)) - 100;
        _filterTopUpToAmount = formatTwoDecimal(findMax(_commissionTopUpForFilter)) + 100;

        switch (field.toLowerCase()) {
            case "from":
                testStepsLog(_logStep++, "Enter From Amount Range : " + _filterTopUpFromAmount);
                type(txtFilterTopUpMinAmount, String.valueOf(_filterTopUpFromAmount));
                break;
            case "to":
                testStepsLog(_logStep++, "Enter To Amount Range : " + _filterTopUpToAmount);
                type(txtFilterTopUpMaxAmount, String.valueOf(_filterTopUpToAmount));
                break;
        }

        return new AgentCommissionVerification(driver);

    }

    public AgentCommissionVerification enterCommissionAmountRange(boolean isInvalid) {

        _isRandomFilter = isInvalid;
        scrollToElement(driver, txtFilterCommissionMaxAmount);

        if (isInvalid) {
            double randomNumber = getRandomNumber() / 1000;
            _filterCommissionFromAmount = randomNumber + 10;
            _filterCommissionToAmount = randomNumber;

            testStepsLog(_logStep++, "Enter From Amount Range : " + _filterCommissionFromAmount);

            type(txtFilterCommissionMinAmount, String.valueOf(_filterCommissionFromAmount));

            testStepsLog(_logStep++, "Enter To Amount Range : " + _filterCommissionToAmount);
            type(txtFilterCommissionMaxAmount, String.valueOf(_filterCommissionToAmount));
        } else {
            _filterTopUpFromAmount = formatTwoDecimal(findMin(_commissionAmountForFilter) - 0.05);
            _filterTopUpToAmount = formatTwoDecimal(findMax(_commissionAmountForFilter) + 0.05);

            testStepsLog(_logStep++, "Enter From Amount Range : " + _filterCommissionFromAmount);
            type(txtFilterCommissionMinAmount, String.valueOf(_filterCommissionFromAmount));

            testStepsLog(_logStep++, "Enter To Amount Range : " + _filterTopUpToAmount);
            type(txtFilterCommissionMaxAmount, String.valueOf(_filterTopUpToAmount));
        }

        return new AgentCommissionVerification(driver);

    }

    public AgentCommissionVerification enterCommissionAmount(String field) {

        scrollToElement(driver, txtFilterCommissionMaxAmount);

        _filterCommissionFromAmount = formatTwoDecimal(findMin(_commissionAmountForFilter) - 0.05);
        _filterCommissionToAmount = formatTwoDecimal(findMax(_commissionAmountForFilter) + 0.05);

        switch (field.toLowerCase()) {
            case "from":
                testStepsLog(_logStep++, "Enter From Amount Range : " + _filterCommissionFromAmount);
                type(txtFilterCommissionMinAmount, String.valueOf(_filterCommissionFromAmount));
                break;
            case "to":
                testStepsLog(_logStep++, "Enter To Amount Range : " + _filterCommissionToAmount);
                type(txtFilterCommissionMaxAmount, String.valueOf(_filterCommissionToAmount));
                break;
        }

        return new AgentCommissionVerification(driver);

    }

    public AgentCommissionVerification filterMerchantID(boolean isInvalid) {

        if (isInvalid) {
            _filterRandomValue = getRandomNumberBetween(10000, 99999) + "" + getRandomNumberBetween(10000, 99999);
            testStepsLog(_logStep++, "Enter Merchant ID");
            testInfoLog("Merchant ID", _filterRandomValue);
            type(txtFilterID, _filterRandomValue);

        } else {
            testStepsLog(_logStep++, "Enter Merchant ID");
            testInfoLog("Merchant ID", _filterID);
            type(txtFilterID, _filterID);
        }

        return new AgentCommissionVerification(driver);
    }
}
