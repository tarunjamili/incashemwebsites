package com.incashme.agent.indexpage;

import com.framework.init.AbstractPage;
import com.incashme.agent.verification.AgentConsumerTargetDetailsVerification;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Rahul R.
 * Date: 2019-04-25
 * Time: 16:35
 * Project Name: InCashMe
 */
public class AgentConsumerTargetDetailsIndexPage extends AbstractPage {

    public AgentConsumerTargetDetailsIndexPage(WebDriver driver) {
        super(driver);
    }

    public static String _consumerName;

    public static int _targetAchievedInConsumerDetails;
    public static int _targetLeftInConsumerDetails;
    public static int _daysCompletedInConsumerDetails;
    public static int _daysLeftInConsumerDetails;
    public static int _totalDaysInConsumerDetails;
    public static int _totalTargetForConsumer;

    public static int _totalConsumersFromChart;

    public static String _searchCriteria;

    public static List<String> _lstConsumerNames = new LinkedList<>();
    public static List<String> _lstConsumerNumbers = new LinkedList<>();

    public static List<String> _consumerNameBeforeSort = new ArrayList<>();
    public static List<String> _consumerNumberBeforeSort = new ArrayList<>();
    public static List<String> _consumerStatusBeforeSort = new ArrayList<>();
    public static List<LocalDateTime> _consumerDateTimeBeforeSort = new ArrayList<>();

    public static String _filterConsumerFirstName = "";
    public static String _filterConsumerLastName = "";
    public static String _filterConsumerPhone = "";
    public static String _filterStatus = "";

    public static String _filterRandomValue = "";
    public static boolean _isRandomFilter;


    @FindBy(xpath = "//div[contains(text(),'Consumers')]//following-sibling::div//span[contains(@class,'chart-velue')]")
    private WebElement lblAddedConsumersInChart;

    @FindBy(xpath = "//div[contains(text(),'Merchants')]//following-sibling::div//span[contains(@class,'chart-velue')]")
    private WebElement lblAddedMerchantsInChart;

    @FindBy(xpath = "//div[contains(text(),'Target Left')]/following-sibling::div")
    private WebElement lblTaregtLeftInChart;

    @FindBy(xpath = "//div[contains(text(),'Total Target') or contains(text(),'Target Achieved')]/following-sibling::div")
    private WebElement lblTotalTargetInChart;

    @FindBy(xpath = "//div[contains(text(),'Time')]//following-sibling::div//span[contains(@class,'chart-velue')]")
    private WebElement lblDaysCompletedInChart;

    @FindBy(xpath = "//div[contains(text(),'Days Left')]/following-sibling::div")
    private WebElement lblDaysLeftInChart;

    @FindBy(xpath = "//div[text()='Target']/following-sibling::div")
    private WebElement lblTargetDaysInChart;

    @FindBy(xpath = "//label[@id='p_total']")
    private WebElement btnChartTotal;

    @FindBy(xpath = "//div[contains(@class,'merchant-count')]//span[contains(text(),'Consumer')]")
    private WebElement lblTotalConsumerCount;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[1]//h5")})
    private List<WebElement> lstConsumerName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[2]/span")})
    private List<WebElement> lstConsumerNumber;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[5]//button")})
    private List<WebElement> lstMOREButton;

    @FindBy(xpath = "//li/a[contains(text(),'Consumer List')]")
    private WebElement btnConsumerList;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[1]//h5")})
    private List<WebElement> lstAddedConsumerName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[2]//span")})
    private List<WebElement> lstAddedConsumerNumber;

    @FindBy(xpath = "//table[@id='dataTbl']//th[1]")
    private WebElement lblConsumerNameHeader;

    @FindBy(xpath = "//table[@id='dataTbl']//th[2]")
    private WebElement lblConsumerNumberHeader;

    @FindBy(xpath = "//table[@id='dataTbl']//th[3]")
    private WebElement lblConsumerStatusHeader;

    @FindBy(xpath = "//table[@id='dataTbl']//th[4]")
    private WebElement lblConsumerDateTimeHeader;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[3]//div")})
    private List<WebElement> lstConsumerStatus;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[4]")})
    private List<WebElement> lstConsumerCreatedDateTime;

    @FindBy(xpath = "//input[@type='search']")
    private WebElement txtInputSearch;

    @FindBy(xpath = "//div[contains(@id,'usersTbl2_filter')]//input[@type='search']")
    private WebElement txtPopupInputSearch;

    @FindAll(value = {@FindBy(xpath = "//table[@id='usersTbl2']//tr//td[1]//h5/following-sibling::div[1]")})
    private List<WebElement> lstListConsumerNumber;

    @FindBy(xpath = "//li/button[contains(@class,'filterBtn')]")
    private WebElement btnFilter;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//button[contains(@class,'searchbtn')]")
    private WebElement btnFilterSearch;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//a[contains(text(),'Clear All')]")
    private WebElement btnFilterClearAll;

    @FindBy(xpath = "//input[@formcontrolname='fname']")
    private WebElement txtFilterFirstName;

    @FindBy(xpath = "//input[@formcontrolname='lname']")
    private WebElement txtFilterLastName;

    @FindBy(xpath = "//input[@formcontrolname='phone']")
    private WebElement txtFilterConsumerMobile;

    @FindBy(xpath = "//div//a[contains(text(),'Clear Date Range')]")
    private WebElement btnClearDateRange;

    @FindAll(value = {@FindBy(xpath = "//mdb-select[@formcontrolname='status']//li/span")})
    private List<WebElement> lstStatuses;

    @FindBy(xpath = "//mdb-select[@formcontrolname='status']")
    private WebElement btnStatusDropdown;

    public AgentConsumerTargetDetailsVerification getConsumerDetails() {

        pause(2);

        _targetAchievedInConsumerDetails = getIntegerFromString(getInnerText(lblAddedConsumersInChart));
        _targetLeftInConsumerDetails = getIntegerFromString(getInnerText(lblTaregtLeftInChart));
        _totalTargetForConsumer = getIntegerFromString(getInnerText(lblTotalTargetInChart));
        _daysLeftInConsumerDetails = getIntegerFromString(getInnerText(lblDaysLeftInChart));
        _totalDaysInConsumerDetails = getIntegerFromString(getInnerText(lblTargetDaysInChart));
        _daysCompletedInConsumerDetails = getIntegerFromString(getInnerText(lblDaysCompletedInChart));

        testInfoLog("Total Target Completed", String.valueOf(_targetAchievedInConsumerDetails));
        testInfoLog("Total Target Left", String.valueOf(_targetLeftInConsumerDetails));
        testInfoLog("Total Target", String.valueOf(_totalTargetForConsumer));
        testInfoLog("Total Days Completed", String.valueOf(_daysCompletedInConsumerDetails));
        testInfoLog("Total Days Left", String.valueOf(_daysLeftInConsumerDetails));
        testInfoLog("Total Days", String.valueOf(_totalDaysInConsumerDetails));

        return new AgentConsumerTargetDetailsVerification(driver);

    }

    public AgentConsumerTargetDetailsVerification getTotalConsumerFromChart() {

        testStepsLog(_logStep++, "Click On Total in Chart.");
        clickOn(driver, btnChartTotal);

        _totalConsumersFromChart = getIntegerFromString(getText(lblTotalConsumerCount));

        testInfoLog("Total Consumer added", String.valueOf(_totalConsumersFromChart));

        return new AgentConsumerTargetDetailsVerification(driver);
    }

    public AgentConsumerTargetDetailsVerification getTotalConsumersDetails() {

        for (int con = 0; con < sizeOf(lstConsumerName); con++) {
            _lstConsumerNames.add(getInnerText(lstConsumerName.get(con)));
            _lstConsumerNumbers.add(getInnerText(lstConsumerNumber.get(con)));
        }

        return new AgentConsumerTargetDetailsVerification(driver);
    }

    public boolean isConsumerListDisplay() {
        return AgentDashboardIndexPage._totalConsumers > 0;
    }

    public AgentConsumerTargetDetailsVerification clickOnConsumerMOREButton() {

        scrollToElement(driver, lstMOREButton.get(0));

        _consumerName = getText(lstConsumerName.get(0));

        testStepsLog(_logStep++, "Click on MORE button.");
        clickOn(driver, lstMOREButton.get(0));

        return new AgentConsumerTargetDetailsVerification(driver);

    }

    public AgentConsumerTargetDetailsVerification clickOnConsumerListButton() {

        testStepsLog(_logStep++, "Click Consumer List button.");
        clickOn(driver, btnConsumerList);

        return new AgentConsumerTargetDetailsVerification(driver);
    }

    public AgentConsumerTargetDetailsVerification clickConsumerNameToSort() {

        try {
            scrollToElement(driver, findElementByName(driver, "dataTbl_length"));
            updateShowList(driver, findElementByName(driver, "dataTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View display.");
        }

        for (WebElement name : lstAddedConsumerName) {
            _consumerNameBeforeSort.add(getInnerText(name));
        }

        testStepsLog(_logStep++, "Click On Top up to sort.");
        clickOn(driver, lblConsumerNameHeader);

        return new AgentConsumerTargetDetailsVerification(driver);
    }


    public AgentConsumerTargetDetailsVerification clickConsumerNumberToSort() {

        _consumerNameBeforeSort.clear();

        try {
            scrollToElement(driver, findElementByName(driver, "dataTbl_length"));
            updateShowList(driver, findElementByName(driver, "dataTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View display.");
        }

        for (WebElement name : lstAddedConsumerNumber) {
            _consumerNumberBeforeSort.add(getInnerText(name));
        }

        testStepsLog(_logStep++, "Click On Top up to sort.");
        clickOn(driver, lblConsumerNumberHeader);

        return new AgentConsumerTargetDetailsVerification(driver);
    }

    public AgentConsumerTargetDetailsVerification clickOnConsumerStatusToSort() {

        for (WebElement status : lstConsumerStatus) {
            _consumerStatusBeforeSort.add(getInnerText(status));
        }

        testStepsLog(_logStep++, "Click On Account Status to sort.");
        clickOn(driver, lblConsumerStatusHeader);
        clickOn(driver, lblConsumerStatusHeader);

        return new AgentConsumerTargetDetailsVerification(driver);
    }

    public AgentConsumerTargetDetailsVerification clickOnConsumerDateTimeToSort() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        for (WebElement status : lstConsumerCreatedDateTime) {
            _consumerDateTimeBeforeSort.add(LocalDateTime.parse(getInnerText(status), format));
        }

        testStepsLog(_logStep++, "Click On Date & Time to sort.");
        clickOn(driver, lblConsumerDateTimeHeader);

        return new AgentConsumerTargetDetailsVerification(driver);
    }

    public AgentConsumerTargetDetailsVerification enterConsumerSearchCriteria(boolean isInvalid) {

        if (isInvalid) {

            scrollToElement(driver, txtInputSearch);

            String searchCriteria = getRandomCharacters(10);
            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", searchCriteria);
            type(txtInputSearch, searchCriteria);

        } else {

            scrollToElement(driver, txtInputSearch);

            _searchCriteria = getInnerText(lstAddedConsumerNumber.get(0)).substring(0, 10);

            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", _searchCriteria);
            type(txtInputSearch, _searchCriteria);
        }

        return new AgentConsumerTargetDetailsVerification(driver);
    }

    public AgentConsumerTargetDetailsVerification enterConsumerListSearchCriteria(boolean isInvalid) {

        if (isInvalid) {

            scrollToElement(driver, txtPopupInputSearch);

            String searchCriteria = getRandomCharacters(10);
            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", searchCriteria);
            type(txtPopupInputSearch, searchCriteria);

        } else {

            scrollToElement(driver, txtPopupInputSearch);

            _searchCriteria = getInnerText(lstListConsumerNumber.get(0));

            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", _searchCriteria);
            type(txtPopupInputSearch, _searchCriteria);
        }

        return new AgentConsumerTargetDetailsVerification(driver);
    }

    public AgentConsumerTargetDetailsVerification clickOnFilterButton() {

        testStepsLog(_logStep++, "Click Filter button.");
        clickOn(driver, btnFilter);

        return new AgentConsumerTargetDetailsVerification(driver);
    }

    public AgentConsumerTargetDetailsVerification enterConsumerSearchCriteria() {

        scrollToElement(driver, txtInputSearch);

        _searchCriteria = AgentUserListIndexPage._newUserCreatedDateTime;

        testStepsLog(_logStep++, "Enter Search Criteria");
        testInfoLog("Search", _searchCriteria);
        type(txtInputSearch, _searchCriteria);

        return new AgentConsumerTargetDetailsVerification(driver);

    }

    public AgentConsumerTargetDetailsVerification getConsumerDetailsForFilter() {

        pause(2);

        _filterConsumerFirstName = getInnerText(lstConsumerName.get(0)).split(" ")[0].trim();
        _filterConsumerLastName = getInnerText(lstConsumerName.get(0)).replace(_filterConsumerFirstName, "").trim();
        _filterConsumerPhone = getInnerText(lstConsumerNumber.get(0));

        return new AgentConsumerTargetDetailsVerification(driver);

    }

    public AgentConsumerTargetDetailsVerification filterFirstName(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomCharacters(7);
            testStepsLog(_logStep++, "Enter First Name : " + _filterRandomValue);
            type(txtFilterFirstName, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter First Name : " + _filterConsumerFirstName);
            type(txtFilterFirstName, _filterConsumerFirstName);
        }

        return new AgentConsumerTargetDetailsVerification(driver);
    }

    public AgentConsumerTargetDetailsVerification clickOnFilterSearchButton() {

        testStepsLog(_logStep++, "Click on Search button.");
        clickOn(driver, btnFilterSearch);

        pause(2);

        try {
            updateShowList(driver, findElementByName(driver, "dataTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        pause(1);

        return new AgentConsumerTargetDetailsVerification(driver);

    }

    public AgentConsumerTargetDetailsVerification clearAllFilterDetails() {

        testStepsLog(_logStep++, "Click on Clear All button.");
        clickOn(driver, btnFilterClearAll);

        pause(2);

        try {
            updateShowList(driver, findElementByName(driver, "agentsTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        pause(1);

        return new AgentConsumerTargetDetailsVerification(driver);

    }

    public AgentConsumerTargetDetailsVerification filterLastName(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomCharacters(7);
            testStepsLog(_logStep++, "Enter Last Name : " + _filterRandomValue);
            type(txtFilterLastName, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter Last Name : " + _filterConsumerLastName);
            type(txtFilterLastName, _filterConsumerLastName);
        }

        return new AgentConsumerTargetDetailsVerification(driver);
    }

    public AgentConsumerTargetDetailsVerification filterConsumerNumber(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomNumber() + "" + getRandomNumber();
            testStepsLog(_logStep++, "Enter Mobile Number : " + _filterRandomValue);
            type(txtFilterConsumerMobile, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter Mobile Number : " + _filterConsumerPhone.split(" ")[1]);
            type(txtFilterConsumerMobile, _filterConsumerPhone.split(" ")[1]);
        }

        return new AgentConsumerTargetDetailsVerification(driver);
    }

    public AgentConsumerTargetDetailsVerification filterStatus(String status) {

        scrollToElement(driver, btnClearDateRange);

        clickOn(driver, btnStatusDropdown);

        _filterStatus = status;

        switch (status.toLowerCase()) {
            case "active":
                clickOn(driver, lstStatuses.get(1));
                break;
            case "kyc pending":
                clickOn(driver, lstStatuses.get(2));
                break;
            case "login pending":
                clickOn(driver, lstStatuses.get(3));
                break;
            default:
                clickOn(driver, lstStatuses.get(0));
                break;
        }

        testStepsLog(_logStep++, "Select Status : " + status);

        return new AgentConsumerTargetDetailsVerification(driver);

    }

    public AgentConsumerTargetDetailsVerification getConsumerStatus() {

        _consumerStatusBeforeSort.clear();

        for (WebElement status : lstConsumerStatus) {
            _consumerStatusBeforeSort.add(getInnerText(status));
        }

        return new AgentConsumerTargetDetailsVerification(driver);
    }

}
