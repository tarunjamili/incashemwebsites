package com.incashme.agent.indexpage;

import com.framework.init.AbstractPage;
import com.incashme.agent.verification.AgentDashboardVerification;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Rahul R.
 * Date: 2019-04-11
 * Time: 11:36
 * Project Name: InCashMe
 */
public class AgentDashboardIndexPage extends AbstractPage {

    public AgentDashboardIndexPage(WebDriver driver) {
        super(driver);
    }

    public static String _currentBalance;

    public static double _todayCommission;
    public static int _topUpDaysLeft;
    public static int _topUpTarget;
    public static int _consumersDaysLeft;
    public static int _consumersTarget;
    public static int _merchantsDaysLeft;
    public static int _merchantsTarget;
    public static int _topUpTargetCompleted;
    public static int _consumerTargetCompleted;
    public static int _merchantTargetCompleted;

    public static int _todayCompletedTopUp;
    public static int _todayAddedConsumers;
    public static int _todayAddedMerchants;

    public static int _totalConsumers;
    public static int _totalMerchants;

    public static String _searchCriteria;

    public static List<List<String>> consumersDetails = new LinkedList<>();

    public static List<Double> _commissionTopUpBeforeSort = new ArrayList<>();
    public static List<Double> _commissionAmountBeforeSort = new ArrayList<>();
    public static List<LocalDateTime> _commissionDateTimeBeforeSort = new ArrayList<>();

    public static double balanceBeforeTopUp;
    public static double commissionBeforeTopUp;
    public static int completedTopUpsBeforeTopUp;
    public static int todayTopUpsBeforeTopUp;

    public static String _topUpMobileNumber;
    public static int _topUpAmount;
    public static LocalDateTime _topUpDateTime;

    public static int _totalAddedConsumerBeforeAddingNewConsumer;
    public static int _todayAddedConsumerBeforeAddingNewConsumer;
    public static int _totalAddedMerchantBeforeAddingNewMerchant;
    public static int _todayAddedMerchantBeforeAddingNewMerchant;

    @FindBy(xpath = "//div/button/i[contains(@class,'mnu-opn')]")
    private WebElement btnExpandMenu;

    @FindBy(xpath = "//ul/li[contains(@id,'sb_home')]")
    private WebElement btnMenuHome;

    @FindBy(xpath = "//ul/li[contains(@id,'sb_bal')]")
    private WebElement btnMenuBalance;

    @FindBy(xpath = "//ul/li[contains(@id,'sb_users')]")
    private WebElement btnMenuUsersList;

    @FindBy(xpath = "//ul/li[contains(@id,'sb_stmt')]")
    private WebElement btnMenuStatement;

    @FindBy(xpath = ".//div[contains(@class,'user-avatar')]//a")
    private WebElement btnUserProfile;

    @FindBy(xpath = "//div//a[contains(text(),'Create Consumer')]")
    private WebElement btnCreateConsumer;

    @FindBy(xpath = "//div//a[contains(text(),'Create Merchant')]")
    private WebElement btnCreateMerchant;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@class,'blnc-val')]")})
    private List<WebElement> lstLblBalance;

    @FindAll(value = {@FindBy(xpath = "//div[contains(text(),'Days Left')]//following-sibling::div")})
    private List<WebElement> lstLblDaysLeft;

    @FindAll(value = {@FindBy(xpath = "//div[contains(text(),'Total Target') or contains(text(),'Target Achieved')]//following-sibling::div")})
    private List<WebElement> lstLblTotalTarget;

    @FindBy(xpath = "//div[@class='profile-details']/ancestor::div//button[contains(@class,'close')]")
    private WebElement btnCloseProfilePopUp;

    @FindBy(xpath = "//h5[contains(text(),'ADD Users')]//following-sibling::button")
    private WebElement btnCloseAddUserPopUp;

    @FindBy(xpath = "//*[text()='Change Password']/ancestor::div//button[contains(@class,'close')]")
    private WebElement btnClosePasswordPopup;

    @FindBy(xpath = "//*[text()='Change Pin']/ancestor::div//button[contains(@class,'close')]")
    private WebElement btnClosePINPopup;

    @FindBy(xpath = "//div//ul//li//a[contains(text(),'Change Password')]")
    private WebElement btnChangePassword;

    @FindBy(xpath = "//div//ul//li//a[contains(text(),'Change PIN')]")
    private WebElement btnChangePIN;

    @FindBy(xpath = "//div//ul//li//a[contains(text(),'Privacy')]")
    private WebElement btnPrivacyPolicy;

    @FindBy(xpath = "//div//ul//li//a[contains(text(),'Terms')]")
    private WebElement btnTermsConditions;

    @FindBy(xpath = "//div//ul//li//a[contains(text(),'Login')]")
    private WebElement btnLoginHistory;

    @FindBy(xpath = "//div[contains(text(),'Top-Up')]//following-sibling::div//span[contains(@class,'chart-velue')]")
    private WebElement lblComletedTopups;

    @FindBy(xpath = "//div[contains(text(),'Consumers')]//following-sibling::div//span[contains(@class,'chart-velue')]")
    private WebElement lblAddedConsumers;

    @FindBy(xpath = "//div[contains(text(),'Merchants')]//following-sibling::div//span[contains(@class,'chart-velue')]")
    private WebElement lblAddedMerchants;

    @FindBy(xpath = "//div[contains(text(),'Top-Up User')]//following-sibling::div")
    private WebElement lblTopUpUserList;

    @FindBy(xpath = "//div[contains(text(),'Added Consumers')]//following-sibling::div")
    private WebElement lblAddedConsumerSection;

    @FindBy(xpath = "//div[contains(text(),'Added Merchants')]//following-sibling::div")
    private WebElement lblAddedMerchantSection;

    @FindBy(xpath = "//*[contains(text(),'TopUps List')]/following-sibling::button")
    private WebElement btnCloseTopUpList;

    @FindBy(xpath = "//*[contains(text(),'Consumers List')]/preceding-sibling::button")
    private WebElement btnCloseConsumersList;

    @FindBy(xpath = "//*[contains(text(),'Merchants List')]/preceding-sibling::button")
    private WebElement btnCloseMerchantsList;

    @FindBy(xpath = "//input[@formcontrolname='oldpassword']")
    private WebElement txtCurrentPassword;

    @FindBy(xpath = "//input[@formcontrolname='password2']")
    private WebElement txtNewPassword;

    @FindBy(xpath = "//input[@formcontrolname='password']")
    private WebElement txtConfirmPassword;

    @FindAll(value = {@FindBy(xpath = ".//div[contains(@class,'login-form')]//button[@type='submit']")})
    private List<WebElement> btnSubmit;

    @FindBy(xpath = "//input[@formcontrolname='oldpin']")
    private WebElement txtCurrentPIN;

    @FindBy(xpath = "//input[@formcontrolname='testData']")
    private WebElement txtNewPIN;

    @FindBy(xpath = "//input[@formcontrolname='pin2']")
    private WebElement txtConfirmPIN;

    @FindBy(xpath = "//div[contains(@class,'add-user')]//span")
    private WebElement btnAddTopupUser;

    @FindAll(value = {@FindBy(xpath = "//table[@id='consumersTbl']//tr//td[1]//h5")})
    private List<WebElement> lstAddUsersConsumerName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='consumersTbl']//tr//td[1]//h5/following-sibling::div[1]")})
    private List<WebElement> lstAddUsersConsumerNumber;

    @FindAll(value = {@FindBy(xpath = "//table[@id='merchantsTbl']//tr//td[1]//h5")})
    private List<WebElement> lstAddUsersMerchantName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='merchantsTbl']//tr//td[1]//h5/following-sibling::div[1]")})
    private List<WebElement> lstAddUsersMerchantID;

    @FindAll(value = {@FindBy(xpath = "//table[@id='consumersTbl']//div[contains(@class,'add-usr-rgt')]")})
    private List<WebElement> lstAddUsersConsumerDetails;

    @FindAll(value = {@FindBy(xpath = "//table[@id='merchantsTbl']//div[contains(@class,'add-usr-rgt')]")})
    private List<WebElement> lstAddUsersMerchantDetails;

    @FindAll(value = {@FindBy(xpath = "//table[@id='consumersTbl']//div[contains(@class,'add-usr-rgt')]/div[2]")})
    private List<WebElement> lstAddUsersNonKYCConsumers;

    @FindAll(value = {@FindBy(xpath = "//table[@id='merchantsTbl']//div[contains(@class,'add-usr-rgt')]/div[2]")})
    private List<WebElement> lstAddUsersNonKYCMerchants;

    @FindBy(xpath = "//div[contains(text(),'User')]//following-sibling::div//span[contains(@class,'chart-velue')]")
    private WebElement lblAddedUsersInChart;

    @FindBy(xpath = "//div[contains(text(),'Consumers')]//following-sibling::div//span[contains(@class,'chart-velue')]")
    private WebElement lblAddedConsumersInChart;

    @FindBy(xpath = "//div[contains(text(),'Merchants')]//following-sibling::div//span[contains(@class,'chart-velue')]")
    private WebElement lblAddedMerchantsInChart;

    @FindBy(xpath = "//div[contains(text(),'Target Left')]/following-sibling::div")
    private WebElement lblTaregtLeftInChart;

    @FindBy(xpath = "//div[contains(text(),'Total Target') or contains(text(),'Target Achieved')]/following-sibling::div")
    private WebElement lblTotalTargetInChart;

    @FindBy(xpath = "//div[contains(text(),'Time')]//following-sibling::div//span[contains(@class,'chart-velue')]")
    private WebElement lblDaysCompletedInChart;

    @FindBy(xpath = "//div[contains(text(),'Days Left')]/following-sibling::div")
    private WebElement lblDaysLeftInChart;

    @FindBy(xpath = "//div[text()='Target']/following-sibling::div")
    private WebElement lblTargetDaysInChart;

    @FindBy(xpath = "//div[contains(@class,'merchant-count')]//span[contains(text(),'Consumer')]")
    private WebElement lblTotalConsumerCount;


    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[5]//button")})
    private List<WebElement> lstMOREButton;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[1]//h5")})
    private List<WebElement> lstConsumerName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[1]//h5")})
    private List<WebElement> lstMerchantName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[1]//h5//following-sibling::div")})
    private List<WebElement> lstMerchantId;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[2]/span")})
    private List<WebElement> lstConsumerNumber;

    @FindBy(xpath = "//li/a[contains(text(),'Back')]")
    private WebElement btnBack;

    @FindBy(xpath = "//label[@id='p_total']")
    private WebElement btnChartTotal;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[4]")})
    private List<WebElement> lstCommissionTopUp;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[5]")})
    private List<WebElement> lstCommissionAmount;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[6]")})
    private List<WebElement> lstCommissionTxTime;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[1]//h5")})
    private List<WebElement> lstTopUpConsumerName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[2]//h5")})
    private List<WebElement> lstTopUpConsumerNumber;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[1]//h5")})
    private List<WebElement> lstAddedConsumerName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[2]//span")})
    private List<WebElement> lstAddedConsumerNumber;

    @FindAll(value = {@FindBy(xpath = "//table[@id='usersTbl2']//tr//td[1]//h5/following-sibling::div[1]")})
    private List<WebElement> lstListConsumerNumber;


    @FindBy(xpath = "//table[@id='trans-history-table']//th[4]")
    private WebElement lblTopupHeader;

    @FindBy(xpath = "//table[@id='trans-history-table']//th[5]")
    private WebElement lblCommissionHeader;

    @FindBy(xpath = "//table[@id='trans-history-table']//th[6]")
    private WebElement lblCommissionDateTimeHeader;

    @FindBy(xpath = "//input[@type='search']")
    private WebElement txtInputSearch;

    @FindBy(xpath = "//table[@id='dataTbl']//th[1]")
    private WebElement lblConsumerNameHeader;

    @FindBy(xpath = "//table[@id='dataTbl']//th[2]")
    private WebElement lblConsumerNumberHeader;

    @FindBy(xpath = "//table[@id='dataTbl']//th[3]")
    private WebElement lblConsumerStatusHeader;

    @FindBy(xpath = "//table[@id='dataTbl']//th[4]")
    private WebElement lblConsumerDateTimeHeader;

    @FindBy(xpath = "//input[contains(@id,'searchTopFromList')]")
    private WebElement txtPopupInputSearch;

    @FindBy(xpath = "//div[contains(@id,'usersTbl2_filter')]//input[@type='search']")
    private WebElement txtPopupInputSearch2;

    @FindBy(xpath = "//div[contains(@class,'add-usr-tab')]//li//span[text()='Merchants']")
    private WebElement btnAddUserMerchant;

    @FindBy(xpath = "//div[contains(@class,'add-usr-tab')]//li//span[text()='Consumers']")
    private WebElement btnAddUserConsumer;

    @FindBy(xpath = "//ul/li[contains(@id,'sb_comms')]")
    private WebElement btnMenuCommission;

    @FindBy(xpath = "//input[@id='payeeName']")
    private WebElement txtMobileNumber;

    @FindBy(xpath = "//input[@id='amount']")
    private WebElement txtTopUpAmount;

    @FindBy(xpath = "//div[contains(@class,'trfr-tpup-card')]//button")
    private WebElement btnSubmitTopup;

    @FindBy(xpath = "//div[contains(@class,'modal-content')]//button[contains(@class,'btn-red') and text()='CANCEL']")
    private WebElement btnCancelTopUp;

    @FindBy(xpath = "//div[contains(@class,'modal-content')]//button[contains(@class,'btn-green') and text()='SEND']")
    private WebElement btnSendTopUp;

    @FindBy(xpath = "//div[contains(@class,'modal-content')]//button[contains(@class,'btn-green') and text()='Close']")
    private WebElement btnCloseTopUpPopUp;

    @FindBy(xpath = "//div//ul//li//a[contains(text(),'KYC')]")
    private WebElement btnKYCandAML;

    @FindBy(xpath = "//div[contains(@class,'avtvty-ttl') and contains(text(),'Top-Up')]")
    private WebElement btnTopUpUser;

    private static boolean checkNonKYCConsumer(WebDriver driver, String consumerNumber) {
        try {
            driver.findElement(By.xpath("//table[@id='consumersTbl']//tr//td[1]//" +
                    "div[contains(text(),'" + consumerNumber + "')]//following-sibling::div"));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private static boolean checkNonKYCMerchant(WebDriver driver, String merchantID) {
        try {
            driver.findElement(By.xpath("//table[@id='consumersTbl']//tr//td[1]//" +
                    "div[contains(text(),'" + merchantID + "')]//following-sibling::div"));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public AgentDashboardVerification expandDashboardMenu() {

        testStepsLog(_logStep++, "Click on Expand Menu icon.");
        clickOn(driver, btnExpandMenu);

        return new AgentDashboardVerification(driver);
    }

    public AgentDashboardVerification clickOnStatementMenu() {

        testStepsLog(_logStep++, "Click on Statement Menu icon.");
        clickOn(driver, btnMenuStatement);

        return new AgentDashboardVerification(driver);
    }

    public AgentDashboardVerification clickOnBalanceMenu() {

        testStepsLog(_logStep++, "Click on Balance Menu icon.");
        clickOn(driver, btnMenuBalance);

        return new AgentDashboardVerification(driver);
    }

    public AgentDashboardVerification clickOnCommissionMenu() {

        testStepsLog(_logStep++, "Click on Commission Menu icon.");
        clickOn(driver, btnMenuCommission);

        pause(3);

        return new AgentDashboardVerification(driver);
    }

    public AgentDashboardVerification clickOnUsersListMenu() {

        pause(2);

        testStepsLog(_logStep++, "Click on Users List Menu icon.");
        clickOn(driver, btnMenuUsersList);

        pause(3);

        return new AgentDashboardVerification(driver);
    }

    public AgentDashboardVerification clickOnCreateConsumer() {

        pause(2);

        scrollToElement(driver, lblTopUpUserList);

        testStepsLog(_logStep++, "Click On Create Consumer button.");
        clickOn(driver, btnCreateConsumer);

        return new AgentDashboardVerification(driver);
    }

    public AgentDashboardVerification clickOnCreateMerchant() {

        pause(2);

        scrollToElement(driver, lblTopUpUserList);

        testStepsLog(_logStep++, "Click On Create Merchant button.");
        clickOn(driver, btnCreateMerchant);

        return new AgentDashboardVerification(driver);
    }

    public AgentDashboardVerification clickOnDashboardIcon() {

        pause(1);
        testStepsLog(_logStep++, "Click on Home Menu icon.");
        clickOn(driver, btnMenuHome);

        pause(5);

        return new AgentDashboardVerification(driver);
    }

    public AgentDashboardVerification getCurrentBalance() {

        _currentBalance = getText(lstLblBalance.get(0));

        return new AgentDashboardVerification(driver);
    }

    public AgentDashboardVerification openUserProfile() {

        testStepsLog(_logStep++, "Click on User Avatar icon.");
        clickOn(driver, btnUserProfile);

        return new AgentDashboardVerification(driver);
    }

    public AgentDashboardVerification closeProfilePopUp() {

        testStepsLog(_logStep++, "Click on Close button.");
        clickOn(driver, btnCloseProfilePopUp);

        return new AgentDashboardVerification(driver);

    }

    public AgentDashboardVerification clickOnChangePassword() {

        testStepsLog(_logStep++, "Click on Change Password button.");
        clickOn(driver, btnChangePassword);

        return new AgentDashboardVerification(driver);
    }

    public AgentDashboardVerification clickOnChangePIN() {

        testStepsLog(_logStep++, "Click on Change PIN button.");
        clickOn(driver, btnChangePIN);

        return new AgentDashboardVerification(driver);
    }

    public AgentDashboardVerification closePasswordPopUp() {

        testStepsLog(_logStep++, "Click on Close button.");
        clickOn(driver, btnClosePasswordPopup);

        return new AgentDashboardVerification(driver);
    }

    public AgentDashboardVerification closePINPopUp() {

        testStepsLog(_logStep++, "Click on Close button.");
        clickOn(driver, btnClosePINPopup);

        return new AgentDashboardVerification(driver);
    }

    public AgentDashboardVerification clickOnPrivacyPolicy() {

        testStepsLog(_logStep++, "Click on Privacy Policy button.");
        clickOn(driver, btnPrivacyPolicy);

        return new AgentDashboardVerification(driver);
    }

    public AgentDashboardVerification clickOnTermsCondition() {

        testStepsLog(_logStep++, "Click on Terms & Condition button.");
        clickOn(driver, btnTermsConditions);

        return new AgentDashboardVerification(driver);
    }

    public AgentDashboardVerification clickOnLoginHistory() {

        testStepsLog(_logStep++, "Click on Login History button.");
        clickOn(driver, btnLoginHistory);

        return new AgentDashboardVerification(driver);
    }

    public AgentDashboardVerification getTodayAgentDetails() {

        pause(2);

        scrollToElement(driver, lstLblTotalTarget.get(0));

        _todayCommission = getDoubleFromString(getInnerText(lstLblBalance.get(1)));
        _topUpDaysLeft = getIntegerFromString(getInnerText(lstLblDaysLeft.get(0)));
        _topUpTarget = getIntegerFromString(getInnerText(lstLblTotalTarget.get(0)));
        _consumersDaysLeft = getIntegerFromString(getInnerText(lstLblDaysLeft.get(1)));
        _consumersTarget = getIntegerFromString(getInnerText(lstLblTotalTarget.get(1)));
        _merchantsDaysLeft = getIntegerFromString(getInnerText(lstLblDaysLeft.get(2)));
        _merchantsTarget = getIntegerFromString(getInnerText(lstLblTotalTarget.get(2)));

        _topUpTargetCompleted = getIntegerFromString(getInnerText(lblComletedTopups));
        _consumerTargetCompleted = getIntegerFromString(getInnerText(lblAddedConsumers));
        _merchantTargetCompleted = getIntegerFromString(getInnerText(lblAddedMerchants));

        scrollToElement(driver, lblTopUpUserList);

        _todayCompletedTopUp = getIntegerFromString(getInnerText(lblTopUpUserList));
        _todayAddedConsumers = getIntegerFromString(getInnerText(lblAddedConsumerSection));
        _todayAddedMerchants = getIntegerFromString(getInnerText(lblAddedMerchantSection));

        return new AgentDashboardVerification(driver);

    }

    public boolean isCommissionDataDisplay() {
        return _todayCommission != 0.00;
    }

    public AgentDashboardVerification openTopUpUserList() {

        pause(2);

        scrollElement(lblTopUpUserList);
        testStepsLog(_logStep++, "Click on Top-Up User.");
        clickOn(driver, lblTopUpUserList);

        return new AgentDashboardVerification(driver);
    }

    public AgentDashboardVerification openAddedConsumerList() {

        scrollToElement(driver, lblAddedConsumerSection);
        testStepsLog(_logStep++, "Click on Added Consumers.");
        clickOn(driver, lblAddedConsumerSection);

        return new AgentDashboardVerification(driver);
    }

    public AgentDashboardVerification openAddedMerchantList() {

        scrollToElement(driver, lblAddedMerchantSection);
        testStepsLog(_logStep++, "Click on Added Merchants.");
        clickOn(driver, lblAddedMerchantSection);

        return new AgentDashboardVerification(driver);
    }

    public AgentDashboardVerification clickOnCloseTopUpList() {

        testStepsLog(_logStep++, "Click on Close button.");
        clickOn(driver, btnCloseTopUpList);

        return new AgentDashboardVerification(driver);
    }

    public AgentDashboardVerification clickOnCloseConsumersList() {
        testStepsLog(_logStep++, "Click on Close button.");
        clickOn(driver, btnCloseConsumersList);

        return new AgentDashboardVerification(driver);
    }

    public AgentDashboardVerification clickOnCloseMerchantsList() {
        testStepsLog(_logStep++, "Click on Close button.");
        clickOn(driver, btnCloseMerchantsList);

        return new AgentDashboardVerification(driver);
    }

    public AgentDashboardVerification changeInvalidCurrentPassword() {

        String randomPassword = getRandomPassword();

        enterCurrentPassword(getRandomPassword());
        enterNewPassword(randomPassword);
        enterConfirmNewPassword(randomPassword);

        clickOnSubmitButton();

        return new AgentDashboardVerification(driver);
    }

    private void enterNewPassword(String newPassword) {
        testStepsLog(_logStep++, "Enter New Password.");
        testInfoLog("New Password", newPassword);
        type(txtNewPassword, newPassword);
    }

    private void enterCurrentPassword(String currentPassword) {
        testStepsLog(_logStep++, "Enter Current Password.");
        testInfoLog("Current Password", currentPassword);
        type(txtCurrentPassword, currentPassword);
    }

    private void enterConfirmNewPassword(String confirmPassword) {
        testStepsLog(_logStep++, "Enter Confirm Password.");
        testInfoLog("Confirm Password", confirmPassword);
        type(txtConfirmPassword, confirmPassword);
    }

    private void clickOnSubmitButton() {

        testStepsLog(_logStep++, "Click on Submit button.");
        clickOn(driver, btnSubmit.get(0));

    }

    private void clickOnSubmitPINButton() {

        testStepsLog(_logStep++, "Click on Submit button.");
        clickOn(driver, btnSubmit.get(1));

    }

    public AgentDashboardVerification changeInvalidNewPassword() {

        String randomPassword = String.valueOf(getRandomNumber());

        enterCurrentPassword(testData);
        enterNewPassword(randomPassword);
        enterConfirmNewPassword(randomPassword);

        return new AgentDashboardVerification(driver);

    }

    public AgentDashboardVerification changeInvalidConfirmPassword() {

        enterCurrentPassword(password);
        enterNewPassword(getRandomPassword());
        enterConfirmNewPassword(String.valueOf(getRandomNumber()));

        pause(1);

        clickOnSubmitButton();

        return new AgentDashboardVerification(driver);

    }

    public AgentDashboardVerification changeAsPastPassword() {

        enterCurrentPassword(password);
        enterNewPassword(password);
        enterConfirmNewPassword(password);

        clickOnSubmitButton();

        return new AgentDashboardVerification(driver);

    }

    public AgentDashboardVerification changeInvalidCurrentPIN() {

        int randomPIN = getRandomPIN();

        enterCurrentPIN(String.valueOf(getRandomPIN()));
        enterNewPIN(String.valueOf(randomPIN));
        enterConfirmNewPIN(String.valueOf(randomPIN));

        clickOnSubmitPINButton();

        return new AgentDashboardVerification(driver);
    }

    private void enterNewPIN(String newPassword) {
        testStepsLog(_logStep++, "Enter New PIN.");
        testInfoLog("New PIN", newPassword);
        type(txtNewPIN, newPassword);
    }

    private void enterCurrentPIN(String currentPassword) {
        testStepsLog(_logStep++, "Enter Current PIN.");
        testInfoLog("Current PIN", currentPassword);
        type(txtCurrentPIN, currentPassword);
    }

    private void enterConfirmNewPIN(String confirmPassword) {
        testStepsLog(_logStep++, "Enter Confirm PIN.");
        testInfoLog("Confirm PIN", confirmPassword);
        type(txtConfirmPIN, confirmPassword);
    }

    public AgentDashboardVerification changeInvalidNewPIN() {

        String randomPassword = getRandomNumberBetween(100, 999) + getRandomCharacters(3);

        enterCurrentPIN(testData);
        enterNewPIN(randomPassword);
        enterConfirmNewPIN(randomPassword);

        return new AgentDashboardVerification(driver);

    }

    public AgentDashboardVerification changeInvalidConfirmPIN() {

        enterCurrentPIN(testData);
        enterNewPIN(String.valueOf(getRandomPIN()));
        enterConfirmNewPIN(String.valueOf(getRandomPIN()));

        pause(1);

        clickOnSubmitPINButton();

        return new AgentDashboardVerification(driver);

    }

    public AgentDashboardVerification changeAsPastPin() {

        enterCurrentPIN(testData);
        enterNewPIN(testData);
        enterConfirmNewPIN(testData);

        clickOnSubmitPINButton();

        return new AgentDashboardVerification(driver);

    }

    public AgentDashboardVerification clickOnAddUserIcon() {

        scrollToElement(driver, btnAddTopupUser);

        testStepsLog(_logStep++, "Click on Add button.");
        clickOn(driver, btnAddTopupUser);

        return new AgentDashboardVerification(driver);

    }

    public AgentDashboardVerification getConsumersListFromAddUsers() {

        clickOnConsumerTab();

        consumersDetails.clear();

        try {
            updateShowList(driver, findElementByName(driver, "consumersTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View display.");
        }

        int kycConsumers = 0;

        for (int con = 0; con < sizeOf(lstAddUsersConsumerDetails); con++) {

            List<String> temp = new LinkedList<>();

            System.out.println(getInnerText(lstAddUsersConsumerDetails.get(con)));

            temp.add(getInnerText(lstAddUsersConsumerName.get(con)));
            temp.add(getInnerText(lstAddUsersConsumerNumber.get(con)).replace("Mo-", ""));

            if (getInnerText(lstAddUsersConsumerDetails.get(con)).contains("Kyc Verification Pending")) {
                temp.add(getInnerText(lstAddUsersNonKYCConsumers.get(kycConsumers)));
                kycConsumers++;
            }

            System.out.println(temp);
            consumersDetails.add(temp);

        }

        _totalConsumers = consumersDetails.size();

        testInfoLog("Total Consumers : ", String.valueOf(_totalConsumers));

        return new AgentDashboardVerification(driver);
    }

    public AgentDashboardVerification clickOnCloseAddUser() {

        testStepsLog(_logStep++, "Click on Close button.");
        clickOn(driver, btnCloseAddUserPopUp);

        return new AgentDashboardVerification(driver);

    }

    public AgentDashboardVerification clickOnTopUpSection() {

        scrollElement(lblComletedTopups);
        testStepsLog(_logStep++, "Click on Topup section.");
        clickOn(driver, lblComletedTopups);

        return new AgentDashboardVerification(driver);
    }

    public AgentDashboardVerification clickOnConsumerSection() {

        testStepsLog(_logStep++, "Click on Consumers section.");
        scrollToElement(driver, lblAddedConsumers);
        clickOn(driver, lblAddedConsumers);

        return new AgentDashboardVerification(driver);

    }

    public AgentDashboardVerification clickOnMerchantSection() {

        testStepsLog(_logStep++, "Click on Merchant section.");
        scrollToElement(driver, lblAddedMerchants);
        clickOn(driver, lblAddedMerchants);

        return new AgentDashboardVerification(driver);

    }


    public boolean isAddUserMerchantListDisplay() {
        return sizeOf(lstAddUsersMerchantID) > 0;
    }


    public AgentDashboardVerification clickTopUpToSort() {

        try {
            scrollToElement(driver, findElementByName(driver, "trans-history-table_length"));
            updateShowList(driver, findElementByName(driver, "trans-history-table_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View display.");
        }

        for (WebElement name : lstCommissionTopUp) {
            _commissionTopUpBeforeSort.add(getDoubleFromString(getInnerText(name)));
        }

        scrollElement(btnTopUpUser);
        testStepsLog(_logStep++, "Click On Top up to sort.");
        clickOn(driver, lblTopupHeader);

        return new AgentDashboardVerification(driver);
    }

    public AgentDashboardVerification clickCommissionToSort() {

        for (WebElement name : lstCommissionAmount) {
            _commissionAmountBeforeSort.add(getDoubleFromString(getInnerText(name)));
        }

        testStepsLog(_logStep++, "Click On Commission to sort.");
        clickOn(driver, lblCommissionHeader);

        return new AgentDashboardVerification(driver);
    }

    public AgentDashboardVerification clickOnCommissionDateTimeToSort() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        for (WebElement status : lstCommissionTxTime) {
            _commissionDateTimeBeforeSort.add(LocalDateTime.parse(getInnerText(status), format));
        }

        testStepsLog(_logStep++, "Click On Date & Time to sort.");
        clickOn(driver, lblCommissionDateTimeHeader);

        return new AgentDashboardVerification(driver);
    }

    public AgentDashboardVerification enterSearchCriteria(boolean isInvalid) {

        if (isInvalid) {

            scrollToElement(driver, txtInputSearch);

            String searchCriteria = getRandomCharacters(10);
            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", searchCriteria);
            type(txtInputSearch, searchCriteria);

        } else {

            scrollToElement(driver, txtInputSearch);

            _searchCriteria = getInnerText(lstCommissionTxTime.get(0)).substring(0, 10);

            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", _searchCriteria);
            type(txtInputSearch, _searchCriteria);
        }

        return new AgentDashboardVerification(driver);
    }


    public AgentDashboardVerification enterAddUserConsumerListSearchCriteria(boolean isInvalid) {

        if (isInvalid) {

            scrollToElement(driver, txtPopupInputSearch);

            String searchCriteria = getRandomCharacters(10);
            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", searchCriteria);
            type(txtPopupInputSearch, searchCriteria);

        } else {

            scrollToElement(driver, txtPopupInputSearch);

            _searchCriteria = getInnerText(lstAddUsersConsumerNumber.get(0));

            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", _searchCriteria);
            type(txtPopupInputSearch, _searchCriteria);
        }

        return new AgentDashboardVerification(driver);
    }

    public AgentDashboardVerification clickOnMerchantTab() {

        testStepsLog(_logStep++, "Click On Merchant Tab.");
        clickOn(driver, btnAddUserMerchant);

        pause(3);

        return new AgentDashboardVerification(driver);

    }

    public AgentDashboardVerification clickOnConsumerTab() {

        testStepsLog(_logStep++, "Click On Consumer Tab.");
        clickOn(driver, btnAddUserConsumer);

        pause(3);

        return new AgentDashboardVerification(driver);

    }


    public AgentDashboardVerification enterAddUserMerchantListSearchCriteria(boolean isInvalid) {

        if (isInvalid) {

            scrollToElement(driver, txtPopupInputSearch);

            String searchCriteria = getRandomCharacters(10);
            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", searchCriteria);
            type(txtPopupInputSearch, searchCriteria);

        } else {

            scrollToElement(driver, txtPopupInputSearch);

            _searchCriteria = getInnerText(lstAddUsersMerchantID.get(0));

            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", _searchCriteria);
            type(txtPopupInputSearch, _searchCriteria);
        }

        return new AgentDashboardVerification(driver);
    }


    public AgentDashboardVerification getTopUpDetailsBeforeTopUp() {

        balanceBeforeTopUp = getDoubleFromString(getInnerText(lstLblBalance.get(0)));
        commissionBeforeTopUp = getDoubleFromString(getInnerText(lstLblBalance.get(1)));
        completedTopUpsBeforeTopUp = getIntegerFromString(getInnerText(lblComletedTopups));
        todayTopUpsBeforeTopUp = getIntegerFromString(getInnerText(lblTopUpUserList));

        return new AgentDashboardVerification(driver);
    }

    public boolean kycPendingAddConsumerDisplay() {

        updateShowList(driver, findElementByName(driver, "consumersTbl_length"), "All");

        int count = 0;
        for (int con = 0; con < sizeOf(lstAddUsersNonKYCConsumers); con++) {
            count++;
        }

        return count > 0;
    }

    public AgentDashboardVerification clickOnNonKYCConsumer() {

        for (int con = 0; con < sizeOf(lstAddUsersNonKYCConsumers); con++) {
            testStepsLog(_logStep++, "Click On Consumer for TopUp.");
            clickOn(driver, lstAddUsersNonKYCConsumers.get(con));
            break;
        }

        return new AgentDashboardVerification(driver);

    }

    public boolean kycPendingAddMerchantDisplay() {

        updateShowList(driver, findElementByName(driver, "merchantsTbl_length"), "All");

        int count = 0;

        for (int mer = 0; mer < sizeOf(lstAddUsersNonKYCMerchants); mer++) {
            count++;
        }

        return count > 0;

    }

    public AgentDashboardVerification clickOnNonKYCMerchant() {

        for (int mer = 0; mer < sizeOf(lstAddUsersNonKYCMerchants); mer++) {
            testStepsLog(_logStep++, "Click On Merchant for TopUp.");
            clickOn(driver, lstAddUsersNonKYCMerchants.get(mer));
            break;
        }

        return new AgentDashboardVerification(driver);

    }

    public AgentDashboardVerification enterUserMobileNumberForTopUp() {

        _topUpMobileNumber = getRandomNumber() + "" + getRandomNumber();
        testStepsLog(_logStep++, "Enter Random Number to TopUp");
        testInfoLog("Number", _topUpMobileNumber);
        type(txtMobileNumber, _topUpMobileNumber);

        return new AgentDashboardVerification(driver);
    }

    public AgentDashboardVerification selectConsumerForTopUp() {

        for (int con = 0; con < sizeOf(lstAddUsersConsumerNumber); con++) {
            int num = getRandomNumberBetween(0, sizeOf(lstAddUsersConsumerNumber));
            if (!checkNonKYCConsumer(driver, getInnerText(lstAddUsersConsumerNumber.get(num)))) {
                _topUpMobileNumber = getInnerText(lstAddUsersConsumerNumber.get(num));
                testStepsLog(_logStep++, "Click On Consumer for TopUp.");
                clickOn(driver, lstAddUsersConsumerNumber.get(num));
                break;
            }
        }

        return new AgentDashboardVerification(driver);
    }

    public AgentDashboardVerification selectMerchantForTopUp() {

        for (int con = 0; con < sizeOf(lstAddUsersMerchantID); con++) {
            int num = getRandomNumberBetween(0, sizeOf(lstAddUsersConsumerNumber));
            if (!checkNonKYCMerchant(driver, getInnerText(lstAddUsersMerchantID.get(num)))) {
                _topUpMobileNumber = getInnerText(lstAddUsersMerchantID.get(num));
                testStepsLog(_logStep++, "Click On Merchant for TopUp.");
                clickOn(driver, lstAddUsersMerchantID.get(num));
                break;
            }
        }

        return new AgentDashboardVerification(driver);
    }

    public AgentDashboardVerification enterZeroTopUpAmount() {

        _topUpAmount = 0;

        testStepsLog(_logStep++, "Enter Amount to TopUp");
        testInfoLog("TopUp Amount", String.valueOf(_topUpAmount));
        type(txtTopUpAmount, String.valueOf(_topUpAmount));

        return new AgentDashboardVerification(driver);

    }

    public AgentDashboardVerification enterInvalidTopUpAmount() {

        _topUpAmount = getRandomNumberBetween(1, 199);

        testStepsLog(_logStep++, "Enter Amount to TopUp");
        testInfoLog("TopUp Amount", String.valueOf(_topUpAmount));
        type(txtTopUpAmount, String.valueOf(_topUpAmount));

        return new AgentDashboardVerification(driver);

    }

    public AgentDashboardVerification enterMaxBalanceTopUpAmount() {

        _topUpAmount = (int) Math.round(balanceBeforeTopUp) + 1;

        testStepsLog(_logStep++, "Enter Amount to TopUp");
        testInfoLog("TopUp Amount", String.valueOf(_topUpAmount));
        type(txtTopUpAmount, String.valueOf(_topUpAmount));

        return new AgentDashboardVerification(driver);

    }

    public AgentDashboardVerification clickOnCompleteTopUpButton() {
        testStepsLog(_logStep++, "Click On TopUp Icon.");
        clickOn(driver, btnSubmitTopup);
        return new AgentDashboardVerification(driver);
    }

    public AgentDashboardVerification enterValidTopUpAmount() {

        _topUpAmount = getRandomNumberBetween(200, 300);

        testStepsLog(_logStep++, "Enter Amount to TopUp");
        testInfoLog("TopUp Amount", String.valueOf(_topUpAmount));
        type(txtTopUpAmount, String.valueOf(_topUpAmount));

        return new AgentDashboardVerification(driver);

    }

    public AgentDashboardVerification clickOnCancelTopUpButton() {

        pause(3);

        testStepsLog(_logStep++, "Click On Cancel Button.");
        clickOn(driver, btnCancelTopUp);
        return new AgentDashboardVerification(driver);
    }

    public AgentDashboardVerification clickOnSendTopUpButton() {

        pause(3);

        testStepsLog(_logStep++, "Click On Send Button.");
        clickOn(driver, btnSendTopUp);

        _topUpDateTime = LocalDateTime.now();

        return new AgentDashboardVerification(driver);
    }

    public AgentDashboardVerification clickOnCloseTopUpButton() {

        pause(3);

        testStepsLog(_logStep++, "Click On CLOSE Button.");
        clickOn(driver, btnCloseTopUpPopUp);
        return new AgentDashboardVerification(driver);
    }

    public AgentDashboardVerification getConsumerAndMerchantCount() {

        clickOnAddUserIcon();
        _totalConsumers = sizeOf(lstAddUsersConsumerName);

        clickOnMerchantTab();
        _totalMerchants = sizeOf(lstAddUsersMerchantName);

        clickOnCloseAddUser();

        return new AgentDashboardVerification(driver);
    }

    public AgentDashboardVerification getAddedConsumerCounts() {

        pause(3);

        _totalAddedConsumerBeforeAddingNewConsumer = getIntegerFromString(getInnerText(lblAddedConsumers));
        _todayAddedConsumerBeforeAddingNewConsumer = getIntegerFromString(getInnerText(lblAddedConsumerSection));

        return new AgentDashboardVerification(driver);
    }

    public AgentDashboardVerification getAddedMerchantCounts() {

        pause(5);

        System.out.println(getInnerText(lblAddedMerchants));
        System.out.println(getInnerText(lblAddedMerchantSection));

        _totalAddedMerchantBeforeAddingNewMerchant = getIntegerFromString(getInnerText(lblAddedMerchants));
        _todayAddedMerchantBeforeAddingNewMerchant = getIntegerFromString(getInnerText(lblAddedMerchantSection));

        return new AgentDashboardVerification(driver);
    }

    public AgentDashboardVerification searchCreatedConsumerInAddUserList() {

        scrollToElement(driver, txtPopupInputSearch);

        _searchCriteria = AgentUserListIndexPage._newUserMobileNumber;

        testStepsLog(_logStep++, "Enter Search Criteria");
        testInfoLog("Search", _searchCriteria);
        type(txtPopupInputSearch, _searchCriteria);

        return new AgentDashboardVerification(driver);
    }

    public AgentDashboardVerification searchCreatedMerchantInAddUserList() {

        scrollToElement(driver, txtPopupInputSearch);

        _searchCriteria = AgentUserListIndexPage._newUserMobileNumber;

        testStepsLog(_logStep++, "Enter Search Criteria");
        testInfoLog("Search", _searchCriteria);
        type(txtPopupInputSearch, _searchCriteria);

        return new AgentDashboardVerification(driver);
    }

    public AgentDashboardVerification clickOnKYCAMLPolicy() {

        testStepsLog(_logStep++, "Click on KYC & AML Policy button.");
        clickOn(driver, btnKYCandAML);

        return new AgentDashboardVerification(driver);
    }
}
