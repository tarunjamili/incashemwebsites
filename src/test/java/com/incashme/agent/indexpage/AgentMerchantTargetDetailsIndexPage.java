package com.incashme.agent.indexpage;

import com.framework.init.AbstractPage;
import com.incashme.agent.verification.AgentMerchantTargetDetailsVerification;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Rahul R.
 * Date: 2019-04-25
 * Time: 16:35
 * Project Name: InCashMe
 */
public class AgentMerchantTargetDetailsIndexPage extends AbstractPage {

    public AgentMerchantTargetDetailsIndexPage(WebDriver driver) {
        super(driver);
    }

    public static String _merchantName;
    public static String _merchantId;

    public static int _targetAchievedInMerchantDetails;
    public static int _targetLeftInMerchantDetails;
    public static int _daysCompletedInMerchantDetails;
    public static int _daysLeftInMerchantDetails;
    public static int _totalDaysInMerchantDetails;
    public static int _totalTargetForMerchant;

    public static int _totalMerchantFromChart;

    public static String _searchCriteria;

    public static List<String> _lstMerchantNames = new LinkedList<>();
    public static List<String> _lstMerchantId = new LinkedList<>();

    public static List<String> _merchantNameBeforeSort = new ArrayList<>();
    public static List<String> _merchantNumberBeforeSort = new ArrayList<>();
    public static List<String> _merchantStatusBeforeSort = new ArrayList<>();
    public static List<LocalDateTime> _merchantDateTimeBeforeSort = new ArrayList<>();

    public static String _filterMerchantName = "";
    public static String _filterMerchantID = "";
    public static String _filterMerchantPhone = "";
    public static String _filterStatus = "";

    public static String _filterRandomValue = "";
    public static boolean _isRandomFilter;

    @FindBy(xpath = "//div[contains(text(),'Merchants')]//following-sibling::div//span[contains(@class,'chart-velue')]")
    private WebElement lblAddedMerchantsInChart;

    @FindBy(xpath = "//div[contains(text(),'Target Left')]/following-sibling::div")
    private WebElement lblTargetLeftInChart;

    @FindBy(xpath = "//div[contains(text(),'Total Target') or contains(text(),'Target Achieved')]/following-sibling::div")
    private WebElement lblTotalTargetInChart;

    @FindBy(xpath = "//div[contains(text(),'Time')]//following-sibling::div//span[contains(@class,'chart-velue')]")
    private WebElement lblDaysCompletedInChart;

    @FindBy(xpath = "//div[contains(text(),'Days Left')]/following-sibling::div")
    private WebElement lblDaysLeftInChart;

    @FindBy(xpath = "//div[text()='Target']/following-sibling::div")
    private WebElement lblTargetDaysInChart;

    @FindBy(xpath = "//label[@id='p_total']")
    private WebElement btnChartTotal;

    @FindBy(xpath = "//div[contains(@class,'merchant-count')]//span[contains(text(),'Merchant')]")
    private WebElement lblTotalMerchantCount;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[1]//h5")})
    private List<WebElement> lstMerchantName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[1]//h5//following-sibling::div")})
    private List<WebElement> lstMerchantId;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[5]//button")})
    private List<WebElement> lstMOREButton;

    @FindBy(xpath = "//li/a[contains(text(),'Merchant List')]")
    private WebElement btnMerchantList;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[1]//h5")})
    private List<WebElement> lstAddedMerchantName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[2]//span")})
    private List<WebElement> lstAddedMerchantNumber;

    @FindBy(xpath = "//table[@id='dataTbl']//th[1]")
    private WebElement lblMerchantNameHeader;

    @FindBy(xpath = "//table[@id='dataTbl']//th[2]")
    private WebElement lblMerchantNumberHeader;

    @FindBy(xpath = "//table[@id='dataTbl']//th[3]")
    private WebElement lblMerchantStatusHeader;

    @FindBy(xpath = "//table[@id='dataTbl']//th[4]")
    private WebElement lblMerchantDateTimeHeader;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[3]//div")})
    private List<WebElement> lstMerchantStatus;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[4]")})
    private List<WebElement> lstMerchantCreatedDateTime;

    @FindBy(xpath = "//input[@type='search']")
    private WebElement txtInputSearch;

    @FindBy(xpath = "//div[contains(@id,'usersTbl2_filter')]//input[@type='search']")
    private WebElement txtPopupInputSearch;

    @FindAll(value = {@FindBy(xpath = "//table[@id='usersTbl2']//tr//td[1]//h5/following-sibling::div[1]")})
    private List<WebElement> lstListMerchantID;

    @FindBy(xpath = "//li/button[contains(@class,'filterBtn')]")
    private WebElement btnFilter;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//button[contains(@class,'searchbtn')]")
    private WebElement btnFilterSearch;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//a[contains(text(),'Clear All')]")
    private WebElement btnFilterClearAll;

    @FindBy(xpath = "//input[@formcontrolname='companyname']")
    private WebElement txtFilterShopName;

    @FindBy(xpath = "//input[@formcontrolname='id']")
    private WebElement txtFilterID;

    @FindBy(xpath = "//input[@formcontrolname='phone']")
    private WebElement txtFilterMerchantMobile;

    @FindBy(xpath = "//div//a[contains(text(),'Clear Date Range')]")
    private WebElement btnClearDateRange;

    @FindAll(value = {@FindBy(xpath = "//mdb-select[@formcontrolname='status']//li/span")})
    private List<WebElement> lstStatuses;

    @FindBy(xpath = "//mdb-select[@formcontrolname='status']")
    private WebElement btnStatusDropdown;

    public AgentMerchantTargetDetailsVerification getMerchantDetails() {

        pause(2);

        _targetAchievedInMerchantDetails = getIntegerFromString(getInnerText(lblAddedMerchantsInChart));
        _targetLeftInMerchantDetails = getIntegerFromString(getInnerText(lblTargetLeftInChart));
        _totalTargetForMerchant = getIntegerFromString(getInnerText(lblTotalTargetInChart));
        _daysLeftInMerchantDetails = getIntegerFromString(getInnerText(lblDaysLeftInChart));
        _totalDaysInMerchantDetails = getIntegerFromString(getInnerText(lblTargetDaysInChart));
        _daysCompletedInMerchantDetails = getIntegerFromString(getInnerText(lblDaysCompletedInChart));

        testInfoLog("Total Target Completed", String.valueOf(_targetAchievedInMerchantDetails));
        testInfoLog("Total Target Left", String.valueOf(_targetLeftInMerchantDetails));
        testInfoLog("Total Target", String.valueOf(_totalTargetForMerchant));
        testInfoLog("Total Days Completed", String.valueOf(_daysCompletedInMerchantDetails));
        testInfoLog("Total Days Left", String.valueOf(_daysLeftInMerchantDetails));
        testInfoLog("Total Days", String.valueOf(_totalDaysInMerchantDetails));

        return new AgentMerchantTargetDetailsVerification(driver);

    }

    public AgentMerchantTargetDetailsVerification getTotalMerchantFromChart() {

        testStepsLog(_logStep++, "Click On Total in Chart.");
        clickOn(driver, btnChartTotal);

        _totalMerchantFromChart = getIntegerFromString(getText(lblTotalMerchantCount));

        testInfoLog("Total Merchant added", String.valueOf(_totalMerchantFromChart));

        return new AgentMerchantTargetDetailsVerification(driver);
    }

    public AgentMerchantTargetDetailsVerification getTotalMerchantDetails() {

        for (int con = 0; con < sizeOf(lstMerchantName); con++) {
            _lstMerchantNames.add(getInnerText(lstMerchantName.get(con)));
            _lstMerchantId.add(getInnerText(lstMerchantId.get(con)));
        }

        return new AgentMerchantTargetDetailsVerification(driver);
    }

    public boolean isMerchantListDisplay() {
        return AgentDashboardIndexPage._totalMerchants > 0;
    }

    public AgentMerchantTargetDetailsVerification clickOnMerchantMOREButton() {

        scrollToElement(driver, lstMOREButton.get(0));

        _merchantName = getText(lstMerchantName.get(0));
        _merchantId = getText(lstMerchantId.get(0));

        testStepsLog(_logStep++, "Click on MORE button.");
        clickOn(driver, lstMOREButton.get(0));

        return new AgentMerchantTargetDetailsVerification(driver);

    }

    public AgentMerchantTargetDetailsVerification clickOnMerchantListButton() {

        testStepsLog(_logStep++, "Click Merchant List button.");
        clickOn(driver, btnMerchantList);

        return new AgentMerchantTargetDetailsVerification(driver);
    }

    public AgentMerchantTargetDetailsVerification clickMerchantNameToSort() {

        try {
            scrollToElement(driver, findElementByName(driver, "dataTbl_length"));
            updateShowList(driver, findElementByName(driver, "dataTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View display.");
        }

        for (WebElement name : lstAddedMerchantName) {
            _merchantNameBeforeSort.add(getInnerText(name));
        }

        testStepsLog(_logStep++, "Click On Top up to sort.");
        clickOn(driver, lblMerchantNameHeader);

        return new AgentMerchantTargetDetailsVerification(driver);
    }

    public AgentMerchantTargetDetailsVerification clickMerchantNumberToSort() {

        try {
            scrollToElement(driver, findElementByName(driver, "dataTbl_length"));
            updateShowList(driver, findElementByName(driver, "dataTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View display.");
        }

        for (WebElement name : lstAddedMerchantNumber) {
            _merchantNumberBeforeSort.add(getInnerText(name));
        }

        testStepsLog(_logStep++, "Click On Top up to sort.");
        clickOn(driver, lblMerchantNumberHeader);

        return new AgentMerchantTargetDetailsVerification(driver);
    }

    public AgentMerchantTargetDetailsVerification clickOnMerchantStatusToSort() {

        for (WebElement status : lstMerchantStatus) {
            _merchantStatusBeforeSort.add(getInnerText(status));
        }

        testStepsLog(_logStep++, "Click On Account Status to sort.");
        clickOn(driver, lblMerchantStatusHeader);
        clickOn(driver, lblMerchantStatusHeader);

        return new AgentMerchantTargetDetailsVerification(driver);
    }

    public AgentMerchantTargetDetailsVerification clickOnMerchantDateTimeToSort() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        for (WebElement status : lstMerchantCreatedDateTime) {
            _merchantDateTimeBeforeSort.add(LocalDateTime.parse(getInnerText(status), format));
        }

        testStepsLog(_logStep++, "Click On Date & Time to sort.");
        clickOn(driver, lblMerchantDateTimeHeader);

        return new AgentMerchantTargetDetailsVerification(driver);
    }

    public AgentMerchantTargetDetailsVerification enterMerchantSearchCriteria(boolean isInvalid) {

        if (isInvalid) {

            scrollToElement(driver, txtInputSearch);

            String searchCriteria = getRandomCharacters(10);
            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", searchCriteria);
            type(txtInputSearch, searchCriteria);

        } else {

            scrollToElement(driver, txtInputSearch);

            _searchCriteria = getInnerText(lstAddedMerchantNumber.get(0)).substring(0, 10);

            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", _searchCriteria);
            type(txtInputSearch, _searchCriteria);
        }

        return new AgentMerchantTargetDetailsVerification(driver);
    }

    public AgentMerchantTargetDetailsVerification enterMerchantListSearchCriteria(boolean isInvalid) {

        if (isInvalid) {

            scrollToElement(driver, txtPopupInputSearch);

            String searchCriteria = getRandomCharacters(10);
            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", searchCriteria);
            type(txtPopupInputSearch, searchCriteria);

        } else {

            scrollToElement(driver, txtPopupInputSearch);

            _searchCriteria = getInnerText(lstListMerchantID.get(0));

            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", _searchCriteria);
            type(txtPopupInputSearch, _searchCriteria);
        }

        return new AgentMerchantTargetDetailsVerification(driver);
    }

    public AgentMerchantTargetDetailsVerification clickOnFilterButton() {

        testStepsLog(_logStep++, "Click Filter button.");
        clickOn(driver, btnFilter);

        return new AgentMerchantTargetDetailsVerification(driver);
    }

    public AgentMerchantTargetDetailsVerification getMerchantDetailsForFilter() {

        pause(2);

        _filterMerchantName = getInnerText(lstAddedMerchantName.get(0));
        _filterMerchantPhone = getInnerText(lstAddedMerchantNumber.get(0));
        _filterMerchantID = getInnerText(lstMerchantId.get(0));

        return new AgentMerchantTargetDetailsVerification(driver);

    }

    public AgentMerchantTargetDetailsVerification filterShopName(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomCharacters(7);
            testStepsLog(_logStep++, "Enter Shop Name : " + _filterRandomValue);
            type(txtFilterShopName, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter Shop Name : " + _filterMerchantName);
            type(txtFilterShopName, _filterMerchantName);
        }

        return new AgentMerchantTargetDetailsVerification(driver);
    }

    public AgentMerchantTargetDetailsVerification clickOnFilterSearchButton() {

        testStepsLog(_logStep++, "Click on Search button.");
        clickOn(driver, btnFilterSearch);

        pause(2);

        try {
            updateShowList(driver, findElementByName(driver, "dataTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        pause(1);

        return new AgentMerchantTargetDetailsVerification(driver);

    }

    public AgentMerchantTargetDetailsVerification clearAllFilterDetails() {

        testStepsLog(_logStep++, "Click on Clear All button.");
        clickOn(driver, btnFilterClearAll);

        pause(2);

        try {
            updateShowList(driver, findElementByName(driver, "agentsTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        pause(1);

        return new AgentMerchantTargetDetailsVerification(driver);

    }

    public AgentMerchantTargetDetailsVerification filterMerchantID(boolean isInvalid) {

        if (isInvalid) {
            _filterRandomValue = getRandomNumberBetween(10000, 99999) + "" + getRandomNumberBetween(10000, 99999);
            testStepsLog(_logStep++, "Enter Merchant ID");
            testInfoLog("Merchant ID", _filterRandomValue);
            type(txtFilterID, _filterRandomValue);

        } else {
            testStepsLog(_logStep++, "Enter Merchant ID");
            testInfoLog("Merchant ID", _filterMerchantID);
            type(txtFilterID, _filterMerchantID);
        }

        return new AgentMerchantTargetDetailsVerification(driver);
    }

    public AgentMerchantTargetDetailsVerification filterMerchantNumber(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomNumber() + "" + getRandomNumber();
            testStepsLog(_logStep++, "Enter Mobile Number : " + _filterRandomValue);
            type(txtFilterMerchantMobile, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter Mobile Number : " + _filterMerchantPhone.split(" ")[1]);
            type(txtFilterMerchantMobile, _filterMerchantPhone.split(" ")[1]);
        }

        return new AgentMerchantTargetDetailsVerification(driver);
    }

    public AgentMerchantTargetDetailsVerification getMerchantStatus() {

        _merchantStatusBeforeSort.clear();

        for (WebElement status : lstMerchantStatus) {
            _merchantStatusBeforeSort.add(getInnerText(status));
        }

        return new AgentMerchantTargetDetailsVerification(driver);
    }

    public AgentMerchantTargetDetailsVerification filterStatus(String status) {

        scrollToElement(driver, btnClearDateRange);

        clickOn(driver, btnStatusDropdown);

        _filterStatus = status;

        switch (status.toLowerCase()) {
            case "active":
                clickOn(driver, lstStatuses.get(1));
                break;
            case "kyc pending":
                clickOn(driver, lstStatuses.get(2));
                break;
            case "login pending":
                clickOn(driver, lstStatuses.get(3));
                break;
            default:
                clickOn(driver, lstStatuses.get(0));
                break;
        }

        testStepsLog(_logStep++, "Select Status : " + status);

        return new AgentMerchantTargetDetailsVerification(driver);

    }

    public AgentMerchantTargetDetailsVerification enterMerchantSearchCriteria() {

        scrollToElement(driver, txtInputSearch);

        _searchCriteria = AgentUserListIndexPage._newUserCreatedDateTime;

        testStepsLog(_logStep++, "Enter Search Criteria");
        testInfoLog("Search", _searchCriteria);
        type(txtInputSearch, _searchCriteria);

        return new AgentMerchantTargetDetailsVerification(driver);

    }
}
