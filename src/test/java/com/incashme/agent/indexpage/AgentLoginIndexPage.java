package com.incashme.agent.indexpage;

import com.framework.init.AbstractPage;
import com.incashme.agent.verification.AgentLoginVerification;
import com.incashme.superAgent.verification.SuperAgentLoginVerification;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by Rahul R.
 * Date: 2019-04-09
 * Time
 * Project Name: InCashMe
 */

public class AgentLoginIndexPage extends AbstractPage {

    public static String _loginTime = "";

    public AgentLoginIndexPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = ".//div[not(@hidden) and @class='login-form']//input[@formcontrolname='email']")
    private WebElement txtEmailAddress;

    @FindBy(xpath = ".//div[not(@hidden) and @class='login-form']//input[@formcontrolname='password']")
    private WebElement txtPassword;

    @FindBy(xpath = ".//div[not(@hidden) and @class='login-form']//button[@type='submit']")
    private WebElement btnLogin;

    @FindBy(xpath = ".//div[contains(@class,'user-info')]//span[contains(@class,'name')]")
    private WebElement lblDashboardUserName;

    @FindBy(xpath = "//div//ul//li//a[contains(text(),'Logout')]")
    private WebElement btnLogout;

    @FindBy(xpath = "//div[contains(text(),'Version')]")
    private WebElement lblVersion;

    @FindBy(xpath = ".//div[not(@hidden) and @class='login-form']//a[contains(text(),'Forgot')]")
    private WebElement btnForgotPassword;

    @FindBy(xpath = ".//div[not(@hidden) and @class='login-form']//a[contains(text(),'Back')]")
    private WebElement btnBackToLogin;

    @FindBy(xpath = ".//div[not(@hidden) and @class='login-form']//input[@formcontrolname='otp']")
    private WebElement txtOTPField;

    @FindBy(xpath = ".//div[not(@hidden) and @class='login-form']//button[@type='submit']")
    private WebElement btnSubmit;

    @FindBy(xpath = ".//div[not(@hidden) and @class='login-form']//a[contains(text(),'Resend')]")
    private WebElement btnResendOTP;

    @FindBy(xpath = ".//div[not(@hidden) and @class='login-form']//input[@formcontrolname='password2']")
    private WebElement txtNewPassword;

    @FindBy(xpath = ".//div[not(@hidden) and @class='login-form']//input[@formcontrolname='password']")
    private WebElement txtConfirmPassword;

    @FindBy(xpath = ".//div[not(@hidden) and @class='login-form']//button[@type='submit']")
    private WebElement btnChangePassword;

    @FindBy(xpath = "//a[contains(text(),'Privacy')]")
    private WebElement btnPrivacyPolicy;

    @FindBy(xpath = "//a[contains(text(),'Terms')]")
    private WebElement btnTermsConditions;

    public AgentLoginVerification invalidLoginAs(String emailAddress, String password) {

        testStepsLog(_logStep++, "Enter Credentials for login.");

        enterEmailAddress(emailAddress);

        testInfoLog("Password", password);
        type(txtPassword, password);

        return new AgentLoginVerification(driver);

    }

    private void enterEmailAddress(String emailAddress) {

        testInfoLog("Email Address", emailAddress);
        type(txtEmailAddress, emailAddress);

    }

    public AgentLoginVerification loginAs(String emailAddress, String password) {

        testStepsLog(_logStep++, "Enter Credentials for login.");

        testInfoLog("Email Address", emailAddress);
        type(txtEmailAddress, emailAddress);

        testInfoLog("Password", password);
        type(txtPassword, password);

        testStepsLog(_logStep++, "Click on Login button.");
        clickOn(driver, btnLogin);

        return new AgentLoginVerification(driver);

    }

    public static void getLoginTime() {
        _loginTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a"));
        System.out.println(_loginTime);
    }

    public AgentLoginVerification blankLoginAs() {

        testStepsLog(_logStep++, "Enter Credentials for login.");

        testInfoLog("Email Address", "");
        clear(txtEmailAddress);

        testInfoLog("Password", "");
        clear(txtPassword);

        return new AgentLoginVerification(driver);
    }

    public AgentLoginVerification clickOnUserName() {

        testStepsLog(_logStep++, "Click on User Name(" + getText(lblDashboardUserName) + ") button.");
        clickOn(driver, lblDashboardUserName);

        return new AgentLoginVerification(driver);
    }

    public void getVersion() {
        try {
            testVerifyLog(getText(findElementByXPath(driver, "//div[contains(text(),'Version')]")));
        } catch (Exception ex) {
            testVerifyLog("Version is not available for the current build.");
        }
    }

    public AgentLoginVerification clickOnLogout() {

        clickOnUserName();

        pause(1);

        testStepsLog(_logStep++, "Click on Logout button.");
        clickOn(driver, btnLogout);

        return new AgentLoginVerification(driver);

    }

    public AgentLoginVerification clickOnForgotPassword() {

        testStepsLog(_logStep++, "Click on Forgot Password button.");
        clickOn(driver, btnForgotPassword);

        return new AgentLoginVerification(driver);

    }

    public AgentLoginVerification clickOnBackToLogin() {

        testStepsLog(_logStep++, "Click on Back To Login button.");
        clickOn(driver, btnBackToLogin);

        return new AgentLoginVerification(driver);

    }

    public AgentLoginVerification clickOnSubmitButton() {

        testStepsLog(_logStep++, "Click on Submit button.");
        clickOn(driver, btnSubmit);

        return new AgentLoginVerification(driver);

    }

    public AgentLoginVerification forgotPasswordAs(String emailAddress) {
        testStepsLog(_logStep++, "Enter Email Address.");
        enterEmailAddress(emailAddress);
        return new AgentLoginVerification(driver);
    }

    public AgentLoginVerification enterOTP(String otp) {
        testStepsLog(_logStep++, "Enter OTP for the Forgot Password");
        testInfoLog("OTP", otp);
        type(txtOTPField, otp);
        return new AgentLoginVerification(driver);
    }

    public AgentLoginVerification enterStep1OTP() {

        int count = 0;

        while (getPropertyValueOf("Agent", "forgotpass_otp1").isEmpty() && count < 16) {
            pause(1);
            count++;
        }

        testStepsLog(_logStep++, "Enter OTP for the Forgot Password");
        String otp = getPropertyValueOf("Agent", "forgotpass_otp1");
        testInfoLog("OTP", otp);
        type(txtOTPField, otp);
        return new AgentLoginVerification(driver);
    }

    public AgentLoginVerification enterStep2OTP() {

        int count = 0;

        while (getPropertyValueOf("Agent", "forgotpass_otp2").isEmpty() && count < 16) {
            pause(1);
            count++;
        }


        testStepsLog(_logStep++, "Enter OTP for the Forgot Password");
        String otp = getPropertyValueOf("Agent", "forgotpass_otp2");
        testInfoLog("OTP", otp);
        type(txtOTPField, otp);
        return new AgentLoginVerification(driver);
    }

    public AgentLoginVerification waitAndClickResendButton() {

        while (!isElementPresent(btnResendOTP)) {
            System.out.println("in loop");
            pause(10);
        }

        testStepsLog(_logStep++, "Click on Resend OTP button.");
        clickOn(driver, btnResendOTP);

        return new AgentLoginVerification(driver);
    }

    public AgentLoginVerification openResetPasswordLink(String username) {

        testStepsLog(_logStep++, "Go to registered email and open Recovery Password link.");

        if (username.split("@")[1].equalsIgnoreCase("mailinator.com")) {
            openMailinator(driver, username.split("@")[0]);
            openEmailAndGetURL(driver);
        } else {
            testValidationLog("Please run this scenario with Mailinator email address, " +
                    "can't open link for other than Mailinator email address.");
        }


        return new AgentLoginVerification(driver);
    }

    public AgentLoginVerification changeInvalidNewPassword() {

        String randomPassword = String.valueOf(getRandomNumber());

        enterNewPassword(randomPassword);
        enterConfirmNewPassword(randomPassword);

        return new AgentLoginVerification(driver);
    }

    public AgentLoginVerification changeAsPastPassword() {

        String password = getPropertyValueOf("Agent", "agentPassword");

        enterNewPassword(password);
        enterConfirmNewPassword(password);

        clickOnSubmitButton();

        return new AgentLoginVerification(driver);

    }

    private void enterNewPassword(String newPassword) {
        testStepsLog(_logStep++, "Enter New Password.");
        testInfoLog("New Password", newPassword);
        type(txtNewPassword, newPassword);
    }

    private void enterConfirmNewPassword(String confirmPassword) {
        testStepsLog(_logStep++, "Enter Confirm Password.");
        testInfoLog("Confirm Password", confirmPassword);
        type(txtConfirmPassword, confirmPassword);
    }

    public AgentLoginVerification changeAgentNewPassword() {

        String password = getPropertyValueOf("Agent", "agentPassword");

        String newPassword = "Baps@" + updatedPassword(password);

        System.out.println(newPassword);

        enterNewPassword(newPassword);
        enterConfirmNewPassword(newPassword);

        setPassword("Agent", "agentPassword", newPassword);

        clickOnSubmitButton();

        return new AgentLoginVerification(driver);

    }

    public AgentLoginVerification clickOnPrivacyPolicy() {

        testStepsLog(_logStep++, "Click on Privacy Policy button.");
        clickOn(driver, btnPrivacyPolicy);

        return new AgentLoginVerification(driver);
    }

    public AgentLoginVerification clickOnTermsCondition() {

        testStepsLog(_logStep++, "Click on Terms & Condition button.");
        clickOn(driver, btnTermsConditions);

        return new AgentLoginVerification(driver);
    }

    private int updatedPassword(String oldPassword) {
        return getIntegerFromString(oldPassword) + 1;
    }

}
