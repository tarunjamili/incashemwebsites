package com.incashme.agent.indexpage;

import com.framework.init.AbstractPage;
import com.incashme.agent.verification.AgentUserListVerification;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Rahul R.
 * Date: 2019-04-25
 * Time: 16:53
 * Project Name: InCashMe
 */
public class AgentUserListIndexPage extends AbstractPage {

    public AgentUserListIndexPage(WebDriver driver) {
        super(driver);
    }

    public static boolean _isRandomBoolean;
    public static String _searchCriteria;

    public static String _newUserFirstName = "";
    public static String _newUserLastName = "";
    public static String _newMerchantUsername = "";
    public static String _newUserEmail = "";
    public static String _newUserMobileNumber = "";
    public static String _newUserDOB = "";

    public static String _newUserFlatNumber = "";
    public static String _newUserStreet1 = "";
    public static String _newUserStreet2 = "";
    public static String _newUserVillage = "";
    public static String _newUserTeshsil = "";
    public static String _newUserDistrict = "";
    public static String _newUserState = "";
    public static String _newUserCity = "";
    public static String _newUserCountry = "";
    public static String _newUserPinCode = "";
    public static String _newUserCreatedDateTime = "";

    public static String _newMerchantBusinessName = "";
    public static String _newMerchantBusinessType = "";
    public static String _newMerchantDBAName = "";
    public static String _newMerchantBusinessNumber = "";
    public static String _newMerchantGSTNumber = "";
    public static String _newMerchantBusinessURL = "";

    public static String _userName = "";
    public static String _userMobileNumber = "";
    public static String _userAccountStatus = "";
    public static String _userCreatedDate;

    public static List<String> _userNameBeforeSort = new ArrayList<>();
    public static List<String> _userNumberBeforeSort = new ArrayList<>();
    public static List<String> _userStatusBeforeSort = new ArrayList<>();
    public static List<LocalDateTime> _userDateTimeBeforeSort = new ArrayList<>();

    public static String _filterFirstName = "";
    public static String _filterLastName = "";
    public static String _filterShopName = "";
    public static String _filterPhone = "";
    public static String _filterStatus = "";

    public static String _filterRandomValue = "";
    public static boolean _isRandomFilter;

    public static int _randomNumber;

    @FindBy(xpath = "//li/a[contains(text(),'Back')]")
    private WebElement btnBack;

    @FindBy(xpath = "//input[@formcontrolname='first_name']")
    private WebElement txtFirstName;

    @FindBy(xpath = "//input[@formcontrolname='last_name']")
    private WebElement txtLastName;

    @FindBy(xpath = "//input[contains(@placeholder,'Date of Birth')]")
    private WebElement txtDOBPicker;

    @FindBy(xpath = "//input[@formcontrolname='email']")
    private WebElement txtEmailAddress;

    @FindBy(xpath = "//input[@formcontrolname='phone']")
    private WebElement txtPhoneNumber;

    @FindBy(xpath = "//div[@id='step1']//button[contains(text(),'Next')]")
    private WebElement btnNextStep1;

    @FindBy(xpath = "//table//button[contains(@class,'yearlabel')]")
    private WebElement btnYearLabel;

    @FindAll(value = {@FindBy(xpath = "//table//button[contains(@class,'yearchangebtn')]")})
    private List<WebElement> lstChangeYearButton;

    @FindAll(value = {@FindBy(xpath = "//table//td//div[contains(@class,'yearvalue')]")})
    private List<WebElement> lstBirthYears;

    @FindBy(xpath = "//table//button[contains(@class,'monthlabel')]")
    private WebElement btnMonthLabel;

    @FindAll(value = {@FindBy(xpath = "//table//td//div[contains(@class,'monthvalue')]")})
    private List<WebElement> lstBirthMonths;

    @FindAll(value = {@FindBy(xpath = "//table//td//div[contains(@class,'currmonth')]")})
    private List<WebElement> lstBirthDates;

    @FindBy(xpath = "//input[@id='profile_pic']")
    private WebElement btnUploadProfilePicture;

    @FindBy(xpath = "//input[@id='aadhaar_front']")
    private WebElement btnUploadAdhaarFront;

    @FindBy(xpath = "//input[@id='aadhaar_back']")
    private WebElement btnUploadAdhharBack;

    @FindBy(xpath = ".//input[@id='otp_val']")
    private WebElement txtStep2OTP;

    @FindBy(xpath = "//div[@id='step2']//button[contains(text(),'Next')]")
    private WebElement btnNextStep2;

    @FindBy(xpath = "//div[@id='step3']//button[contains(text(),'Next')]")
    private WebElement btnNextStep3;

    @FindBy(xpath = "//div[@id='step2']//button[contains(text(),'Previous')]")
    private WebElement btnPreviousStep2;

    @FindBy(xpath = "//input[@formcontrolname='address1']")
    private WebElement txtHomeFlatAddress;

    @FindBy(xpath = "//input[@formcontrolname='address2']")
    private WebElement txtStreet1Address;

    @FindBy(xpath = "//input[@formcontrolname='address3']")
    private WebElement txtStreet2Address;

    @FindBy(xpath = "//input[@formcontrolname='village']")
    private WebElement txtVillage;

    @FindBy(xpath = "//input[@formcontrolname='tehsil']")
    private WebElement txtTehsil;

    @FindBy(xpath = "//input[@formcontrolname='district']")
    private WebElement txtDistrict;

    @FindBy(xpath = "//input[@formcontrolname='country']")
    private WebElement txtCountry;

    @FindBy(xpath = "//input[@formcontrolname='pincode']")
    private WebElement txtPinCode;

    @FindBy(xpath = "//select[@formcontrolname='state']")
    private WebElement btnState;

    @FindBy(xpath = "//select[@formcontrolname='city']")
    private WebElement btnCity;

    @FindBy(xpath = "//div[@id='step3']//button[contains(text(),'Cancel')]")
    private WebElement btnConsumerCancel;

    @FindBy(xpath = "//div[@id='step4']//button[contains(text(),'Cancel')]")
    private WebElement btnMerchantCancel;

    @FindBy(xpath = "//div[contains(@class,'swal2-actions')]//button[text()='Ok!']")
    private WebElement btnCancelOk;

    @FindBy(xpath = "//div[contains(@class,'swal2-actions')]//button[text()='Cancel']")
    private WebElement btnCancelCancel;

    @FindBy(xpath = "//div[@id='step3']//button[contains(text(),'Create')]")
    private WebElement btnCreateConsumer;

    @FindBy(xpath = "//div[@id='step4']//button[contains(text(),'Create')]")
    private WebElement btnCreateMerchant;

    @FindBy(xpath = "//input[@formcontrolname='username']")
    private WebElement txtUsername;

    @FindBy(xpath = "//input[@formcontrolname='companyname']")
    private WebElement txtBusinessName;

    @FindBy(xpath = "//input[@formcontrolname='dba']")
    private WebElement txtDBAName;

    @FindBy(xpath = "//input[@formcontrolname='filter_phone']")
    private WebElement txtBusinessPhone;

    @FindBy(xpath = "//input[@formcontrolname='taxid']")
    private WebElement txtGSTNumber;

    @FindBy(xpath = "//input[@formcontrolname='website']")
    private WebElement txtBusinessURL;

    @FindBy(xpath = "//mdb-select[@formcontrolname='bustype']")
    private WebElement btnBusinessTypeDropdown;

    @FindAll(value = {@FindBy(xpath = "//mdb-select[@formcontrolname='bustype']//li/span")})
    private List<WebElement> lstBusinessType;

    @FindBy(xpath = "//input[@id='buisness_pan']")
    private WebElement btnUploadBusinessPAN;

    @FindBy(xpath = "//input[@id='buisness_addr_proof']")
    private WebElement btnUploadBusinessAddressProof;

    @FindAll(value = {@FindBy(xpath = "//input[@type='search']")})
    private List<WebElement> txtInputSearch;

    @FindBy(xpath = "//div[contains(@class,'cmsn-tabs')]//li//a[text()='Merchant']")
    private WebElement btnMerchantTab;

    @FindBy(xpath = "//div[contains(@class,'cmsn-tabs')]//li//a[text()='Consumer']")
    private WebElement btnConsumerTab;

    @FindBy(xpath = "//div//p[contains(text(),'No consumers found!')]")
    private WebElement lblNoConsumer;

    @FindBy(xpath = "//div//p[contains(text(),'No merchants found!')]")
    private WebElement lblNoMerchant;

    @FindAll(value = {@FindBy(xpath = "//table[@id='consumersTbl']//tr//td[1]//h5")})
    private List<WebElement> lstConsumerName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='consumersTbl']//tr//td[2]")})
    private List<WebElement> lstConsumerNumber;

    @FindAll(value = {@FindBy(xpath = "//table[@id='consumersTbl']//tr//td[3]")})
    private List<WebElement> lstConsumerStatus;

    @FindAll(value = {@FindBy(xpath = "//table[@id='consumersTbl']//tr//td[4]")})
    private List<WebElement> lstConsumerDateTime;

    @FindAll(value = {@FindBy(xpath = "//table[@id='consumersTbl']//tr//td[5]//a")})
    private List<WebElement> lstConsumerMOREButton;

    @FindBy(xpath = "//div[contains(@class,'spr-usr-tabs')]//a[@id='transList']")
    private WebElement btnUserHistory;

    @FindAll(value = {@FindBy(xpath = "//table[@id='merchantsTbl']//tr//td[1]//h5")})
    private List<WebElement> lstMerchantName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='merchantsTbl']//tr//td[2]")})
    private List<WebElement> lstMerchantNumber;

    @FindAll(value = {@FindBy(xpath = "//table[@id='merchantsTbl']//tr//td[3]")})
    private List<WebElement> lstMerchantStatus;

    @FindAll(value = {@FindBy(xpath = "//table[@id='merchantsTbl']//tr//td[4]")})
    private List<WebElement> lstMerchantDateTime;

    @FindAll(value = {@FindBy(xpath = "//table[@id='merchantsTbl']//tr//td[5]//a")})
    private List<WebElement> lstMerchantMOREButton;

    @FindBy(xpath = "//table[@id='consumersTbl']//th[1]")
    private WebElement lblConsumerNameHeader;

    @FindBy(xpath = "//table[@id='consumersTbl']//th[2]")
    private WebElement lblConsumerNumberHeader;

    @FindBy(xpath = "//table[@id='consumersTbl']//th[3]")
    private WebElement lblConsumerStatusHeader;

    @FindBy(xpath = "//table[@id='consumersTbl']//th[4]")
    private WebElement lblConsumerDateTimeHeader;

    @FindBy(xpath = "//table[@id='merchantsTbl']//th[1]")
    private WebElement lblMerchantNameHeader;

    @FindBy(xpath = "//table[@id='merchantsTbl']//th[2]")
    private WebElement lblMerchantNumberHeader;

    @FindBy(xpath = "//table[@id='merchantsTbl']//th[3]")
    private WebElement lblMerchantStatusHeader;

    @FindBy(xpath = "//table[@id='merchantsTbl']//th[4]")
    private WebElement lblMerchantDateTimeHeader;

    @FindBy(xpath = "//li/button[contains(@class,'filterBtn')]")
    private WebElement btnFilter;

    @FindBy(xpath = "//input[@formcontrolname='fname']")
    private WebElement txtFilterFirstName;

    @FindBy(xpath = "//input[@formcontrolname='lname']")
    private WebElement txtFilterLastName;

    @FindBy(xpath = "//input[@formcontrolname='phone']")
    private WebElement txtFilterMobile;

    @FindBy(xpath = "//div//a[contains(text(),'Clear Date Range')]")
    private WebElement btnClearDateRange;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//button[contains(@class,'searchbtn')]")
    private WebElement btnFilterSearch;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//a[contains(text(),'Clear All')]")
    private WebElement btnFilterClearAll;

    @FindAll(value = {@FindBy(xpath = "//mdb-select[@formcontrolname='status']//li/span")})
    private List<WebElement> lstStatuses;

    @FindBy(xpath = "//mdb-select[@formcontrolname='status']")
    private WebElement btnStatusDropdown;

    @FindBy(xpath = "//input[@formcontrolname='companyname']")
    private WebElement txtFilterShopName;

    @FindBy(xpath = "//div[@class='input-group']//select[@id='fileType']")
    private WebElement selectFileType;

    public AgentUserListVerification clickOnBackButton() {

        pause(2);

        Actions action = new Actions(driver);
        action.sendKeys(Keys.PAGE_UP).build().perform();

        testStepsLog(_logStep++, "Click on Back button.");
        clickOn(driver, btnBack);

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification enterName(String field) {

        if (field.equalsIgnoreCase("Last Name")) {
            _newUserLastName = getRandomLastName();
            type(txtLastName, _newUserLastName);
            testStepsLog(_logStep++, "Enter Last Name");
            testInfoLog("Last Name", _newUserLastName);
        } else {
            _newUserFirstName = getRandomFirstName();
            type(txtFirstName, _newUserFirstName);
            testStepsLog(_logStep++, "Enter First Name");
            testInfoLog("First Name", _newUserFirstName);
        }

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification enterRandomDOB() {

        String dobYear = getDOBYear();
        String dobMonth = getDOBMonth();
        String dobDate = getDOBDate();

        _newUserDOB = dobDate + "/" + dobMonth + "/" + dobYear;

        testInfoLog("Selected DOB", _newUserDOB);

        return new AgentUserListVerification(driver);
    }

    private String getDOBYear() {

        testStepsLog(_logStep++, "Click on Date Picker.");
        clickOn(driver, txtDOBPicker);

        testStepsLog(_logStep++, "Click on Year to change.");
        clickOn(driver, btnYearLabel);
        clickOn(driver, lstChangeYearButton.get(0));

        int index = getRandomIndex(lstBirthYears);
        String year = getText(lstBirthYears.get(index));
        testInfoLog("Select Year", year);
        clickOn(driver, lstBirthYears.get(index));

        return year;
    }

    private String getDOBMonth() {


        testStepsLog(_logStep++, "Click on Month to change.");
        clickOn(driver, btnMonthLabel);

        int index = getRandomIndex(lstBirthMonths);
        String month = getText(lstBirthMonths.get(index));
        testInfoLog("Select Month", month);
        clickOn(driver, lstBirthMonths.get(index));

        return getNumericMonth(month);
    }

    private String getDOBDate() {

        testStepsLog(_logStep++, "Click on Date to select.");

        int index = getRandomIndex(lstBirthDates);
        int date = getIntegerFromString(getText(lstBirthDates.get(index)));
        testInfoLog("Select Date", String.valueOf(date));
        clickOn(driver, lstBirthDates.get(index));

        return (date < 10 ? "0" : "") + date;
    }

    public AgentUserListVerification enterEmail(boolean isInvalid) {

        if (isInvalid) {
            String randomMail = getInvalidEmail();
            type(txtEmailAddress, randomMail);
            testStepsLog(_logStep++, "Enter Email");
            testInfoLog("Email", randomMail);
        } else {
            _newUserEmail = getRegistrationEmail();
            type(txtEmailAddress, _newUserEmail);
            testStepsLog(_logStep++, "Enter Email");
            testInfoLog("Email", _newUserEmail);
        }

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification enterMinMobileNumber() {

        String randomNumber = getRandomNumber() + "";

        type(txtPhoneNumber, randomNumber);
        testStepsLog(_logStep++, "Enter Phone Number");
        testInfoLog("Phone Number", randomNumber);

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification enterMaxMobileNumber() {

        String randomNumber = getRandomNumber() + "" + getRandomNumber() + "" + getRandomNumberBetween(0, 9);

        type(txtPhoneNumber, randomNumber);
        testStepsLog(_logStep++, "Enter Phone Number");
        testInfoLog("Phone Number", randomNumber);

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification enterInvalidMobileNumber() {

        String randomNumber = getRandomCharacters(5);
        type(txtPhoneNumber, randomNumber);
        testStepsLog(_logStep++, "Enter Phone Number");
        testInfoLog("Phone Number", randomNumber);

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification enterMobileNumber() {

        _newUserMobileNumber = testData;

        type(txtPhoneNumber, _newUserMobileNumber);
        testStepsLog(_logStep++, "Enter Phone Number");
        testInfoLog("Phone Number", _newUserMobileNumber);

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification uploadProfilePicture(boolean isInvalid) {

        testStepsLog(_logStep++, "Upload Profile Picture");

        if (isInvalid)
            sendKeys(btnUploadProfilePicture, INVALID_IMAGE);
        else
            sendKeys(btnUploadProfilePicture, PROFILE_PIC);

        pause(1);

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification uploadFrontSide(boolean isInvalid) {

        testStepsLog(_logStep++, "Upload Document Front");

        scrollElement(btnNextStep1);

        if (isInvalid)
            sendKeys(btnUploadAdhaarFront, INVALID_IMAGE);
        else
            sendKeys(btnUploadAdhaarFront, ADHAAR_FRONT);

        pause(1);

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification uploadBackSide(boolean isInvalid) {

        testStepsLog(_logStep++, "Upload Document Back");

        scrollElement(btnNextStep1);

        if (isInvalid)
            sendKeys(btnUploadAdhharBack, INVALID_IMAGE);
        else
            sendKeys(btnUploadAdhharBack, ADHAAR_BACK);

        pause(1);

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification clickStep1OnNextButton() {

        scrollElement(btnNextStep1);

        testStepsLog(_logStep++, "Click on Next Button.");
        clickOn(driver, btnNextStep1);

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification enterStep2OTP() {

        int count = 0;

        while (getPropertyValueOf("Agent", "newuser_otp").isEmpty() && count < 30) {
            pause(1);
            count++;
        }

        testStepsLog(_logStep++, "Enter OTP.");
        String otp = getPropertyValueOf("Agent", "newuser_otp");
        testInfoLog("OTP", otp);
        type(txtStep2OTP, otp);

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification clickOnStep2NextButton() {

        scrollToElement(driver, btnNextStep2);

        testStepsLog(_logStep++, "Click on Next Button.");
        clickOn(driver, btnNextStep2);

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification enterHomeAddress(boolean isInvalid) {

        scrollToElement(driver, txtHomeFlatAddress);

        if (isInvalid) {
            String randomAddress = getRandomCharacters(51);
            type(txtHomeFlatAddress, randomAddress);
            testStepsLog(_logStep++, "Enter Home/Flat Address");
            testInfoLog("Home/Flat Address", randomAddress);
        } else {
            _newUserFlatNumber = getBuildingNumber();
            type(txtHomeFlatAddress, _newUserFlatNumber);
            testStepsLog(_logStep++, "Enter Home/Flat Address");
            testInfoLog("Home/Flat Address", _newUserFlatNumber);
        }
        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification enterInvalidBusinessName(String type) {

        scrollToElement(driver, txtBusinessName);

        if (type.equalsIgnoreCase("min")) {
            String businessName = getRandomCharacters(2);
            type(txtBusinessName, businessName);
            testStepsLog(_logStep++, "Enter Business Name");
            testInfoLog("Business Name", businessName);
        } else {
            String businessName = getRandomCharacters(51);
            type(txtBusinessName, businessName);
            testStepsLog(_logStep++, "Enter Business Name");
            testInfoLog("Business Name", businessName);
        }
        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification enterInvalidDBAName(String type) {

        scrollToElement(driver, txtDBAName);

        if (type.equalsIgnoreCase("min")) {
            String dbaName = getRandomCharacters(2);
            type(txtDBAName, dbaName);
            testStepsLog(_logStep++, "Enter DBA Name");
            testInfoLog("DBA Name", dbaName);
        } else {
            String dbaName = getRandomCharacters(51);
            type(txtDBAName, dbaName);
            testStepsLog(_logStep++, "Enter DBA Name");
            testInfoLog("DBA Name", dbaName);
        }
        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification enterStreet1Address(boolean isInvalid) {

        scrollToElement(driver, txtStreet1Address);

        if (isInvalid) {
            String randomAddress = getRandomCharacters(51);
            type(txtStreet1Address, randomAddress);
            testStepsLog(_logStep++, "Enter Street 1 Address");
            testInfoLog("Street 1", randomAddress);
        } else {
            _newUserStreet1 = getRandomStreetName();
            type(txtStreet1Address, _newUserStreet1);
            testStepsLog(_logStep++, "Enter Street 1 Address");
            testInfoLog("Street 1", _newUserStreet1);
        }
        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification enterStreet2Address(boolean isInvalid) {

        scrollToElement(driver, txtStreet2Address);

        _isRandomBoolean = getRandomBoolean();
        if (isInvalid) {
            String randomAddress = getRandomCharacters(51);
            type(txtStreet2Address, randomAddress);
            testStepsLog(_logStep++, "Enter Street 2 Address");
            testInfoLog("Street 2", randomAddress);
        } else {
            if (_isRandomBoolean) {
                _newUserStreet2 = getRandomStreetName();
                type(txtStreet2Address, _newUserStreet2);
                testStepsLog(_logStep++, "Enter Street 2 Address");
                testInfoLog("Street 2", _newUserStreet2);
            } else {
                type(txtStreet2Address, getRandomCharacters(3));
                clear(txtStreet2Address);
                _newUserStreet2 = "";
            }
        }
        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification enterVillageAddress(boolean isInvalid) {

        scrollToElement(driver, txtVillage);

        _isRandomBoolean = getRandomBoolean();
        if (isInvalid) {
            String randomAddress = getRandomCharacters(51);
            type(txtVillage, randomAddress);
            testStepsLog(_logStep++, "Enter Village");
            testInfoLog("Village", randomAddress);
        } else {
            if (_isRandomBoolean) {
                _newUserVillage = getRandomVillage();
                type(txtVillage, _newUserVillage);
                testStepsLog(_logStep++, "Enter Village");
                testInfoLog("Village", _newUserVillage);
            } else {
                type(txtVillage, getRandomCharacters(3));
                clear(txtVillage);
                _newUserVillage = "";
            }
        }
        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification enterTehsilAddress(boolean isInvalid) {

        scrollToElement(driver, txtTehsil);

        _isRandomBoolean = getRandomBoolean();
        if (isInvalid) {
            String randomAddress = getRandomCharacters(51);
            type(txtTehsil, randomAddress);
            testStepsLog(_logStep++, "Enter Tehsil/Taluka");
            testInfoLog("Tehsil/Taluka", randomAddress);
        } else {
            if (_isRandomBoolean) {
                _newUserTeshsil = getRandomVillage();
                type(txtTehsil, _newUserTeshsil);
                testStepsLog(_logStep++, "Enter Tehsil/Talika");
                testInfoLog("Tehsil/Taluka", _newUserTeshsil);
            } else {
                type(txtTehsil, getRandomCharacters(3));
                clear(txtTehsil);
                _newUserTeshsil = "";
            }
        }
        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification enterDistrictAddress(boolean isInvalid) {

        scrollToElement(driver, txtDistrict);

        _isRandomBoolean = getRandomBoolean();
        if (isInvalid) {
            String randomAddress = getRandomCharacters(51);
            type(txtDistrict, randomAddress);
            testStepsLog(_logStep++, "Enter District");
            testInfoLog("District", randomAddress);
        } else {
            if (_isRandomBoolean) {
                _newUserDistrict = getRandomDistrict();
                type(txtDistrict, _newUserDistrict);
                testStepsLog(_logStep++, "Enter District");
                testInfoLog("District", _newUserDistrict);
            } else {
                type(txtDistrict, getRandomCharacters(3));
                clear(txtDistrict);
                _newUserDistrict = "";
            }
        }
        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification selectState() {

        scrollToElement(driver, btnState);

        Select state = new Select(btnState);
        int index = getRandomNumberBetween(0, state.getOptions().size() - 1);

        testStepsLog(_logStep++, "Select State");
        state.selectByIndex(index);
        _newUserState = getText(state.getFirstSelectedOption());
        testInfoLog("State", _newUserState);

        pause(3);

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification selectCity() {

        scrollToElement(driver, btnCity);

        Select city = new Select(btnCity);
        int index = getRandomNumberBetween(0, city.getOptions().size() - 1);

        testStepsLog(_logStep++, "Select City");
        city.selectByIndex(index);
        _newUserCity = getText(city.getFirstSelectedOption());
        testInfoLog("City", _newUserCity);

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification enterNumberCountry() {

        scrollToElement(driver, txtCountry);

        String randomCountry = String.valueOf(getRandomNumber());
        type(txtCountry, randomCountry);
        testStepsLog(_logStep++, "Enter Country");
        testInfoLog("Country", randomCountry);

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification enterMinCharCountry() {

        scrollToElement(driver, txtCountry);

        String randomCountry = getRandomCharacters(1);
        type(txtCountry, randomCountry);
        testStepsLog(_logStep++, "Enter Country");
        testInfoLog("Country", randomCountry);

        return new AgentUserListVerification(driver);

    }

    public AgentUserListVerification enterMaxCharCountry() {

        scrollToElement(driver, txtCountry);

        String randomCountry = getRandomCharacters(31);
        type(txtCountry, randomCountry);
        testStepsLog(_logStep++, "Enter Country");
        testInfoLog("Country", randomCountry);

        return new AgentUserListVerification(driver);

    }

    public AgentUserListVerification enterCountry() {

        scrollToElement(driver, txtCountry);

        _newUserCountry = getRandomCountry().replaceAll("[^a-zA-Z0-9]", "");
        type(txtCountry, _newUserCountry);
        testStepsLog(_logStep++, "Enter Country");
        testInfoLog("Country", _newUserCountry);

        return new AgentUserListVerification(driver);

    }

    public AgentUserListVerification enterMinPinCode() {

        scrollToElement(driver, txtPinCode);

        String randomNumber = String.valueOf(getRandomNumberBetween(100, 999));

        type(txtPinCode, randomNumber);
        testStepsLog(_logStep++, "Enter Pin Code");
        testInfoLog("Pin Code", randomNumber);

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification enterMaxPinCode() {

        scrollToElement(driver, txtPinCode);

        String randomNumber = getRandomNumber() + "" + getRandomNumber();

        type(txtPinCode, randomNumber);
        testStepsLog(_logStep++, "Enter Pin Code");
        testInfoLog("Pin Code", randomNumber);

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification enterInvalidPinCode() {

        scrollToElement(driver, txtPinCode);

        String randomNumber = getRandomCharacters(5);

        type(txtPinCode, randomNumber);
        testStepsLog(_logStep++, "Enter Pin Code");
        testInfoLog("Pin Code", randomNumber);

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification enterPinCode() {

        scrollToElement(driver, txtPinCode);

        _newUserPinCode = getRandomPinCode();

        type(txtPinCode, _newUserPinCode);
        testStepsLog(_logStep++, "Enter Pin Code");
        testInfoLog("Pin Code", _newUserPinCode);

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification clickOnStep3NextButton() {

        scrollToElement(driver, btnNextStep3);

        testStepsLog(_logStep++, "Click on Next Button.");
        clickOn(driver, btnNextStep3);

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification clickOnConsumerCancelButton() {

        scrollToElement(driver, btnConsumerCancel);

        testStepsLog(_logStep++, "Click on Cancel Button.");
        clickOn(driver, btnConsumerCancel);

        return new AgentUserListVerification(driver);

    }

    public AgentUserListVerification clickOnMerchantCancelButton() {

        scrollToElement(driver, btnMerchantCancel);

        testStepsLog(_logStep++, "Click on Cancel Button.");
        clickOn(driver, btnMerchantCancel);

        return new AgentUserListVerification(driver);

    }

    public AgentUserListVerification clickOnCancelCancelButton() {

        scrollToElement(driver, btnCancelCancel);

        testStepsLog(_logStep++, "Click on Cancel Button.");
        clickOn(driver, btnCancelCancel);

        return new AgentUserListVerification(driver);

    }

    public AgentUserListVerification clickOnOKCancelButton() {

        scrollToElement(driver, btnCancelOk);

        testStepsLog(_logStep++, "Click on OK Button.");
        clickOn(driver, btnCancelOk);

        return new AgentUserListVerification(driver);

    }

    public AgentUserListVerification enterUsernameLessThan8Char() {

        scrollToElement(driver, txtUsername);

        String randomUserName = String.valueOf(getRandomNumber());
        type(txtUsername, randomUserName);
        testStepsLog(_logStep++, "Enter Username");
        testInfoLog("Username", randomUserName);

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification enterUsername(boolean isInvalid) {

        scrollToElement(driver, txtUsername);

        if (isInvalid) {
            String randomUsername = getRandomCharacters(10);
            type(txtUsername, randomUsername);
            testStepsLog(_logStep++, "Enter Username");
            testInfoLog("Username", randomUsername);
        } else {
            _newMerchantUsername = getRandomUsername();
            type(txtUsername, _newMerchantUsername);
            testStepsLog(_logStep++, "Enter Username");
            testInfoLog("Username", _newMerchantUsername);
        }
        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification enterBusinessName() {

        _newMerchantBusinessName = getRandomBusinessName();
        type(txtBusinessName, _newMerchantBusinessName);
        testStepsLog(_logStep++, "Enter Official Business Full Name");
        testInfoLog("Business Name", _newMerchantBusinessName);

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification enterDBAName() {

        _newMerchantDBAName = getRandomDBAName();
        type(txtDBAName, _newMerchantDBAName);
        testStepsLog(_logStep++, "Enter DBA Name");
        testInfoLog("DBA Name", _newMerchantDBAName);

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification enterGSTNumber() {

        _newMerchantGSTNumber = getRandomGSTNumber();
        type(txtGSTNumber, _newMerchantGSTNumber);
        testStepsLog(_logStep++, "Enter GST Number");
        testInfoLog("GST Number", _newMerchantGSTNumber);

        return new AgentUserListVerification(driver);
    }


    public AgentUserListVerification selectBusinessType() {

        clickOn(driver, btnBusinessTypeDropdown);

        if (getRandomBoolean()) {
            clickOn(driver, lstBusinessType.get(0));
            _newMerchantBusinessType = getInnerText(lstBusinessType.get(0));
        } else {
            clickOn(driver, lstBusinessType.get(1));
            _newMerchantBusinessType = getInnerText(lstBusinessType.get(1));
        }

        testStepsLog(_logStep++, "Select Business Type : " + _newMerchantBusinessType);

        return new AgentUserListVerification(driver);

    }

    public AgentUserListVerification uploadBusinessPANCard(boolean isInvalid) {

        testStepsLog(_logStep++, "Upload Business PAN Card");

        if (isInvalid)
            sendKeys(btnUploadBusinessPAN, INVALID_IMAGE);
        else
            sendKeys(btnUploadBusinessPAN, PROFILE_PIC);

        pause(1);

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification uploadBusinessAddressProof(boolean isInvalid) {

        testStepsLog(_logStep++, "Upload Business Address Proof");

        if (isInvalid)
            sendKeys(btnUploadBusinessAddressProof, INVALID_IMAGE);
        else
            sendKeys(btnUploadBusinessAddressProof, PROFILE_PIC);

        pause(1);

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification enterNameMinChar(String field) {

        String randomName = getRandomCharacters(1);

        if (field.equalsIgnoreCase("Last Name")) {
            type(txtLastName, randomName);
            testStepsLog(_logStep++, "Enter Last Name");
            testInfoLog("Last Name", randomName);
        } else {
            type(txtFirstName, randomName);
            testStepsLog(_logStep++, "Enter First Name");
            testInfoLog("First Name", randomName);
        }

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification enterNameNumber(String field) {

        String randomName = String.valueOf(getRandomNumber());

        if (field.equalsIgnoreCase("Last Name")) {
            type(txtLastName, randomName);
            testStepsLog(_logStep++, "Enter Last Name");
            testInfoLog("Last Name", randomName);
        } else {
            type(txtFirstName, randomName);
            testStepsLog(_logStep++, "Enter First Name");
            testInfoLog("First Name", randomName);
        }

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification enterNameMaxChar(String field) {

        String randomName = getRandomCharacters(21);

        if (field.equalsIgnoreCase("Last Name")) {
            type(txtLastName, randomName);
            testStepsLog(_logStep++, "Enter Last Name");
            testInfoLog("Last Name", randomName);
        } else {
            type(txtFirstName, randomName);
            testStepsLog(_logStep++, "Enter First Name");
            testInfoLog("First Name", randomName);
        }

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification enterOTP(String otp) {
        testStepsLog(_logStep++, "Enter OTP.");
        testInfoLog("OTP", otp);
        type(txtStep2OTP, otp);
        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification clickOnCreateConsumerButton() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        scrollToElement(driver, btnCreateConsumer);

        testStepsLog(_logStep++, "Click on Create Consumer Button.");
        clickOn(driver, btnCreateConsumer);

        _newUserCreatedDateTime = format.format(LocalDateTime.now());

        return new AgentUserListVerification(driver);

    }

    public AgentUserListVerification clickOnCreateMerchantButton() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        scrollToElement(driver, btnCreateMerchant);

        testStepsLog(_logStep++, "Click on Create Merchant Button.");
        clickOn(driver, btnCreateMerchant);

        _newUserCreatedDateTime = format.format(LocalDateTime.now());

        return new AgentUserListVerification(driver);

    }

    public AgentUserListVerification enterConsumerSearchCriteria() {

        scrollToElement(driver, txtInputSearch.get(0));

        _searchCriteria = AgentUserListIndexPage._newUserCreatedDateTime;

        testStepsLog(_logStep++, "Enter Search Criteria");
        testInfoLog("Search", _searchCriteria);
        type(txtInputSearch.get(0), _searchCriteria);

        return new AgentUserListVerification(driver);

    }

    public AgentUserListVerification enterConsumerSearchCriteria(boolean isInvalid) {

        if (isInvalid) {

            scrollToElement(driver, txtInputSearch.get(0));

            String searchCriteria = getRandomCharacters(10);
            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", searchCriteria);
            type(txtInputSearch.get(0), searchCriteria);

        } else {

            scrollToElement(driver, txtInputSearch.get(0));

            _searchCriteria = getInnerText(lstConsumerNumber.get(0));

            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", _searchCriteria);
            type(txtInputSearch.get(0), _searchCriteria);
        }

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification clickOnMerchantTab() {

        testStepsLog(_logStep++, "Click On Merchant Tab.");
        clickOn(driver, btnMerchantTab);

        pause(3);

        return new AgentUserListVerification(driver);

    }

    public AgentUserListVerification clickOnConsumerTab() {

        testStepsLog(_logStep++, "Click On Consumer Tab.");
        clickOn(driver, btnConsumerTab);

        pause(3);

        return new AgentUserListVerification(driver);

    }

    public boolean isConsumerDataDisplay() {

        try {
            updateShowList(driver, findElementByName(driver, "consumersTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        return !isElementPresent(lblNoConsumer);
    }

    public boolean isMerchantDataDisplay() {

        try {
            updateShowList(driver, findElementByName(driver, "merchantsTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        return !isElementPresent(lblNoMerchant);
    }

    public AgentUserListVerification getAnyConsumerDetails() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");
        DateTimeFormatter format2 = DateTimeFormatter.ofPattern("dd MMMM yyyy", Locale.ENGLISH);

        _randomNumber = getRandomNumberBetween(0, lastIndexOf(lstConsumerName));

        _userName = getInnerText(lstConsumerName.get(_randomNumber));
        _userMobileNumber = getInnerText(lstConsumerNumber.get(_randomNumber));
        _userAccountStatus = getInnerText(lstConsumerStatus.get(_randomNumber));

        _userCreatedDate = format2.format(LocalDate.parse(getInnerText(lstConsumerDateTime.get(_randomNumber)), format));

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification clickOnConsumerMOREButton() {

        scrollToElement(driver, lstConsumerMOREButton.get(_randomNumber));

        testStepsLog(_logStep++, "Click on MORE button.");
        clickOn(driver, lstConsumerMOREButton.get(_randomNumber));

        return new AgentUserListVerification(driver);

    }

    public AgentUserListVerification clickOnHistory() {

        scrollToElement(driver, btnUserHistory);

        testStepsLog(_logStep++, "Click on History tab.");
        clickOn(driver, btnUserHistory);

        return new AgentUserListVerification(driver);

    }

    public AgentUserListVerification getAnyMerchantDetails() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");
        DateTimeFormatter format2 = DateTimeFormatter.ofPattern("dd MMMM yyyy", Locale.ENGLISH);

        _randomNumber = getRandomNumberBetween(0, lastIndexOf(lstMerchantName));

        _userName = getInnerText(lstMerchantName.get(_randomNumber));
        _userMobileNumber = getInnerText(lstMerchantNumber.get(_randomNumber));
        _userAccountStatus = getInnerText(lstMerchantStatus.get(_randomNumber));

        _userCreatedDate = format2.format(LocalDate.parse(getInnerText(lstMerchantDateTime.get(_randomNumber)), format));

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification clickOnMerchantMOREButton() {

        scrollToElement(driver, lstMerchantMOREButton.get(_randomNumber));

        testStepsLog(_logStep++, "Click on MORE button.");
        clickOn(driver, lstMerchantMOREButton.get(_randomNumber));

        return new AgentUserListVerification(driver);

    }

    public AgentUserListVerification clickConsumerNameToSort() {

        try {
            scrollToElement(driver, findElementByName(driver, "consumersTbl_length"));
            updateShowList(driver, findElementByName(driver, "consumersTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View display.");
        }

        for (WebElement name : lstConsumerName) {
            _userNameBeforeSort.add(getInnerText(name));
        }

        testStepsLog(_logStep++, "Click On Name to sort.");
        clickOn(driver, lblConsumerNameHeader);

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification clickConsumerNumberToSort() {

        _userNameBeforeSort.clear();

        try {
            scrollToElement(driver, findElementByName(driver, "consumersTbl_length"));
            updateShowList(driver, findElementByName(driver, "consumersTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View display.");
        }

        for (WebElement name : lstConsumerNumber) {
            _userNumberBeforeSort.add(getInnerText(name));
        }

        testStepsLog(_logStep++, "Click On Mobile Number to sort.");
        clickOn(driver, lblConsumerNumberHeader);

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification clickOnConsumerStatusToSort() {

        for (WebElement status : lstConsumerStatus) {
            _userStatusBeforeSort.add(getInnerText(status));
        }

        testStepsLog(_logStep++, "Click On Account Status to sort.");
        clickOn(driver, lblConsumerStatusHeader);
        clickOn(driver, lblConsumerStatusHeader);

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification clickOnConsumerDateTimeToSort() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        for (WebElement status : lstConsumerDateTime) {
            _userDateTimeBeforeSort.add(LocalDateTime.parse(getInnerText(status), format));
        }

        testStepsLog(_logStep++, "Click On Date & Time to sort.");
        clickOn(driver, lblConsumerDateTimeHeader);

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification clickMerchantNameToSort() {

        _userNameBeforeSort.clear();

        try {
            scrollToElement(driver, findElementByName(driver, "merchantsTbl_length"));
            updateShowList(driver, findElementByName(driver, "merchantsTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View display.");
        }

        for (WebElement name : lstMerchantName) {
            _userNameBeforeSort.add(getInnerText(name));
        }

        testStepsLog(_logStep++, "Click On Name to sort.");
        clickOn(driver, lblMerchantNameHeader);

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification clickMerchantNumberToSort() {

        _userNumberBeforeSort.clear();

        try {
            scrollToElement(driver, findElementByName(driver, "merchantsTbl_length"));
            updateShowList(driver, findElementByName(driver, "merchantsTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View display.");
        }

        for (WebElement name : lstMerchantNumber) {
            _userNumberBeforeSort.add(getInnerText(name));
        }

        testStepsLog(_logStep++, "Click On Mobile Number to sort.");
        clickOn(driver, lblMerchantNumberHeader);

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification clickOnMerchantStatusToSort() {

        _userStatusBeforeSort.clear();

        for (WebElement status : lstMerchantStatus) {
            _userStatusBeforeSort.add(getInnerText(status));
        }

        testStepsLog(_logStep++, "Click On Account Status to sort.");
        clickOn(driver, lblMerchantStatusHeader);
        clickOn(driver, lblMerchantStatusHeader);

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification clickOnMerchantDateTimeToSort() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        _userDateTimeBeforeSort.clear();

        for (WebElement status : lstMerchantDateTime) {
            _userDateTimeBeforeSort.add(LocalDateTime.parse(getInnerText(status), format));
        }

        testStepsLog(_logStep++, "Click On Date & Time to sort.");
        clickOn(driver, lblMerchantDateTimeHeader);

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification enterMerchantSearchCriteria(boolean isInvalid) {

        if (isInvalid) {

            scrollToElement(driver, txtInputSearch.get(1));

            String searchCriteria = getRandomCharacters(10);
            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", searchCriteria);
            type(txtInputSearch.get(1), searchCriteria);

        } else {

            scrollToElement(driver, txtInputSearch.get(1));

            _searchCriteria = getInnerText(lstMerchantNumber.get(0));

            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", _searchCriteria);
            type(txtInputSearch.get(1), _searchCriteria);
        }

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification clickOnFilterIcon() {

        scrollToElement(driver, btnFilter);

        testStepsLog(_logStep++, "Click on Filter Button.");
        clickOn(driver, btnFilter);

        pause(2);

        return new AgentUserListVerification(driver);

    }

    public AgentUserListVerification getConsumerDetailsForFilter() {

        pause(2);

        _filterFirstName = getInnerText(lstConsumerName.get(0)).split(" ")[0].trim();
        _filterLastName = getInnerText(lstConsumerName.get(0)).replace(_filterFirstName, "").trim();
        _filterPhone = getInnerText(lstConsumerNumber.get(0));

        return new AgentUserListVerification(driver);

    }

    public AgentUserListVerification filterFirstName(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomCharacters(7);
            testStepsLog(_logStep++, "Enter First Name : " + _filterRandomValue);
            type(txtFilterFirstName, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter First Name : " + _filterFirstName);
            type(txtFilterFirstName, _filterFirstName);
        }

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification clickOnConsumerFilterSearchButton() {

        testStepsLog(_logStep++, "Click on Search button.");
        clickOn(driver, btnFilterSearch);

        pause(2);

        try {
            updateShowList(driver, findElementByName(driver, "consumersTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        pause(1);

        return new AgentUserListVerification(driver);

    }

    public AgentUserListVerification clearAllConsumerFilterDetails() {

        testStepsLog(_logStep++, "Click on Clear All button.");
        clickOn(driver, btnFilterClearAll);

        pause(2);

        try {
            updateShowList(driver, findElementByName(driver, "consumersTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        pause(1);

        return new AgentUserListVerification(driver);

    }

    public AgentUserListVerification clickOnMerchantFilterSearchButton() {

        testStepsLog(_logStep++, "Click on Search button.");
        clickOn(driver, btnFilterSearch);

        pause(2);

        try {
            updateShowList(driver, findElementByName(driver, "consumersTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        pause(1);

        return new AgentUserListVerification(driver);

    }

    public AgentUserListVerification clearAllMerchantFilterDetails() {

        testStepsLog(_logStep++, "Click on Clear All button.");
        clickOn(driver, btnFilterClearAll);

        pause(2);

        try {
            updateShowList(driver, findElementByName(driver, "consumersTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        pause(1);

        return new AgentUserListVerification(driver);

    }

    public AgentUserListVerification filterLastName(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomCharacters(7);
            testStepsLog(_logStep++, "Enter Last Name : " + _filterRandomValue);
            type(txtFilterLastName, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter Last Name : " + _filterLastName);
            type(txtFilterLastName, _filterLastName);
        }

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification filterNumber(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomNumber() + "" + getRandomNumber();
            testStepsLog(_logStep++, "Enter Mobile Number : " + _filterRandomValue);
            type(txtFilterMobile, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter Mobile Number : " + _filterPhone.split(" ")[1]);
            type(txtFilterMobile, _filterPhone.split(" ")[1]);
        }

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification filterStatus(String status) {

        scrollToElement(driver, btnClearDateRange);

        clickOn(driver, btnStatusDropdown);

        _filterStatus = status;

        switch (status.toLowerCase()) {
            case "active":
                clickOn(driver, lstStatuses.get(1));
                break;
            case "kyc pending":
                clickOn(driver, lstStatuses.get(2));
                break;
            case "login pending":
                clickOn(driver, lstStatuses.get(3));
                break;
            default:
                clickOn(driver, lstStatuses.get(0));
                break;
        }

        testStepsLog(_logStep++, "Select Status : " + status);

        return new AgentUserListVerification(driver);

    }

    public AgentUserListVerification getConsumerStatus() {

        _userStatusBeforeSort.clear();

        for (WebElement status : lstConsumerStatus) {
            _userStatusBeforeSort.add(getInnerText(status));
        }

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification geMerchantStatus() {

        _userStatusBeforeSort.clear();

        for (WebElement status : lstMerchantStatus) {
            _userStatusBeforeSort.add(getInnerText(status));
        }

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification getMerchantDetailsForFilter() {

        pause(2);

        _filterShopName = getInnerText(lstMerchantName.get(0));
        _filterPhone = getInnerText(lstMerchantNumber.get(0));

        return new AgentUserListVerification(driver);

    }

    public AgentUserListVerification filterShopName(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomCharacters(7);
            testStepsLog(_logStep++, "Enter Shop Name : " + _filterRandomValue);
            type(txtFilterShopName, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter Shop Name : " + _filterShopName);
            type(txtFilterShopName, _filterShopName);
        }

        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification enterMerchantSearchCriteria() {

        scrollToElement(driver, txtInputSearch.get(0));

        _searchCriteria = AgentUserListIndexPage._newUserCreatedDateTime;

        testStepsLog(_logStep++, "Enter Search Criteria");
        testInfoLog("Search", _searchCriteria);
        type(txtInputSearch.get(0), _searchCriteria);

        return new AgentUserListVerification(driver);

    }

    public AgentUserListVerification enterBusinessNumber(boolean isInvalid) {

        scrollToElement(driver, txtBusinessPhone);

        _isRandomBoolean = getRandomBoolean();
        if (isInvalid) {
            String randomNumber = getRandomCharacters(5);
            type(txtBusinessPhone, randomNumber);
            testStepsLog(_logStep++, "Enter Business Number");
            testInfoLog("Business Number", randomNumber);
        } else {
            if (false) {
                _newMerchantBusinessNumber = getRandomNumber() + "" + getRandomNumber();
                type(txtBusinessPhone, _newMerchantBusinessNumber);
                testStepsLog(_logStep++, "Enter Business Number");
                testInfoLog("Business Number", _newMerchantBusinessNumber);
            } else {
                type(txtBusinessPhone, getRandomCharacters(3));
                clear(txtBusinessPhone);
                _newMerchantBusinessNumber = "";
            }
        }
        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification enterGSTNumber(boolean isInvalid) {

        scrollToElement(driver, txtGSTNumber);

        _isRandomBoolean = getRandomBoolean();
        if (isInvalid) {
            String randomNumber = getRandomCharacters(5);
            type(txtGSTNumber, randomNumber);
            testStepsLog(_logStep++, "Enter GST Number");
            testInfoLog("GST Number", randomNumber);
        } else {
            if (_isRandomBoolean) {
                _newMerchantGSTNumber = getRandomGSTNumber();
                type(txtGSTNumber, _newMerchantGSTNumber);
                testStepsLog(_logStep++, "Enter GST Number");
                testInfoLog("GST Number", _newMerchantGSTNumber);
            } else {
                type(txtGSTNumber, getRandomCharacters(3));
                clear(txtGSTNumber);
                _newMerchantGSTNumber = "";
            }
        }
        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification enterBusinessURL() {

        scrollToElement(driver, txtBusinessURL);

        _isRandomBoolean = getRandomBoolean();
        if (_isRandomBoolean) {
            _newMerchantBusinessURL = getRandomURL();
            type(txtBusinessURL, _newMerchantBusinessURL);
            testStepsLog(_logStep++, "Enter Business URL");
            testInfoLog("Business URL", _newMerchantBusinessURL);
        } else {
            type(txtBusinessURL, getRandomCharacters(3));
            clear(txtBusinessURL);
            _newMerchantBusinessURL = "";
        }
        return new AgentUserListVerification(driver);
    }

    public AgentUserListVerification selectDocumentType() {
        Select select = new Select(selectFileType);
        select.selectByIndex(getRandomIndex(select.getOptions()));

        return new AgentUserListVerification(driver);
    }
}
