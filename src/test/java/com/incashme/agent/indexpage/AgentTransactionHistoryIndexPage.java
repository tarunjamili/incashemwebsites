package com.incashme.agent.indexpage;

import com.framework.init.AbstractPage;
import com.incashme.agent.verification.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Rahul R.
 * Date: 2019-04-26
 * Time: 15:09
 * Project Name: InCashMe
 */
public class AgentTransactionHistoryIndexPage extends AbstractPage {

    public AgentTransactionHistoryIndexPage(WebDriver driver) {
        super(driver);
    }

    public static String _searchCriteria;

    public static List<Double> _transactionTopUpBeforeSort = new ArrayList<>();
    public static List<Double> _commissionAmountBeforeSort = new ArrayList<>();
    public static List<LocalDateTime> _transactionDateTimeBeforeSort = new ArrayList<>();

    public static String _filterFirstName = "";
    public static String _filterLastName = "";
    public static String _filterShopName = "";
    public static String _filterPhone = "";
    public static String _filterID = "";
    public static String _filterTransactionId = "";
    public static String _transactionDateAndTime;

    public static double _filterTopUpFromAmount;
    public static double _filterTopUpToAmount;
    public static double _filterCommissionFromAmount;
    public static double _filterCommissionToAmount;

    public static String _transactionTopUpAmount;
    public static String _transactionCommissionAmount;

    public static List<Double> _commissionTopUpForFilter = new ArrayList<>();
    public static List<Double> _commissionAmountForFilter = new ArrayList<>();

    public static String _filterRandomValue = "";
    public static boolean _isRandomFilter;

    @FindBy(xpath = "//ul/li[contains(@id,'sb_trans')]")
    private WebElement btnMenuTransactionHistory;

    @FindBy(xpath = "//input[@type='search']")
    private WebElement txtInputSearch;

    @FindBy(xpath = "//div//p[contains(text(),'no data alive')]")
    private WebElement lblNoTransaction;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[1]//h5/span")})
    private List<WebElement> lstUserName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[1]//following-sibling::div[contains(text(),'ID') or contains(text(),'Mo')]")})
    private List<WebElement> lstUserIdNumber;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[2]/span")})
    private List<WebElement> lstUserTxID;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[1]//following-sibling::div[2]")})
    private List<WebElement> lstUserType;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[3]")})
    private List<WebElement> lstUserTopUpAmount;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[4]")})
    private List<WebElement> lstCommissionAmount;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[5]")})
    private List<WebElement> lstTransactionTime;

    @FindBy(xpath = "//table[@id='trans-history-table']//th[3]")
    private WebElement lblTopupHeader;

    @FindBy(xpath = "//table[@id='trans-history-table']//th[4]")
    private WebElement lblCommissionHeader;

    @FindBy(xpath = "//table[@id='trans-history-table']//th[5]")
    private WebElement lblCommissionDateTimeHeader;

    @FindBy(xpath = "//li/button[contains(@class,'filterBtn')]")
    private WebElement btnFilter;

    @FindBy(xpath = "//input[@formcontrolname='to_fname']")
    private WebElement txtFilterFirstName;

    @FindBy(xpath = "//input[@formcontrolname='to_lname']")
    private WebElement txtFilterLastName;

    @FindBy(xpath = "//input[@formcontrolname='to_cellnum']")
    private WebElement txtFilterConsumerMobile;

    @FindBy(xpath = "//input[@formcontrolname='atns_id']")
    private WebElement txtFilterTransactionID;

    @FindAll(value = {@FindBy(xpath = "//mdb-select[@formcontrolname='to_user_type']//li/span")})
    private List<WebElement> lstFilterUser;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//button[contains(@class,'searchbtn')]")
    private WebElement btnFilterSearch;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//a[contains(text(),'Clear All')]")
    private WebElement btnFilterClearAll;

    @FindBy(xpath = "//mdb-select[@formcontrolname='to_user_type']")
    private WebElement btnUserDropdown;

    @FindBy(xpath = "//input[@formcontrolname='min_amount']")
    private WebElement txtFilterTopUpMinAmount;

    @FindBy(xpath = "//input[@formcontrolname='max_amount']")
    private WebElement txtFilterTopUpMaxAmount;

    @FindBy(xpath = "//input[@formcontrolname='comm_min_amount']")
    private WebElement txtFilterCommissionMinAmount;

    @FindBy(xpath = "//input[@formcontrolname='comm_max_amount']")
    private WebElement txtFilterCommissionMaxAmount;

    @FindBy(xpath = "//input[@formcontrolname='mi_companyname']")
    private WebElement txtFilterShopName;

    @FindBy(xpath = "//input[@formcontrolname='to_user_id']")
    private WebElement txtFilterID;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[6]//button")})
    private List<WebElement> lstTransactionMOREButton;

    @FindBy(xpath = "//h5[contains(text(),'Transaction Detail')]//following-sibling::button")
    private WebElement btnCloseTransactionPopUp;

    @FindBy(xpath = "//input[@formcontrolname='to_user_id']//following-sibling::div//a")
    private WebElement btnClearMerchantID;

    @FindBy(xpath = "//input[@formcontrolname='min_amount']//following-sibling::div//a")
    private WebElement btnClearTopUpAmountRange;

    @FindBy(xpath = "//input[@formcontrolname='comm_min_amount']//following-sibling::div//a")
    private WebElement btnClearCommissionAmountRange;

    public AgentTransactionHistoryVerification clickOnTransactionHistory() {

        testStepsLog(_logStep++, "Click on Transaction History button.");
        clickOn(driver, btnMenuTransactionHistory);

        return new AgentTransactionHistoryVerification(driver);
    }

    public AgentTransactionHistoryVerification searchRecentTransaction() {

        pause(3);

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        testStepsLog(_logStep++, "Search recent completed transaction.");

        scrollToElement(driver, txtInputSearch);

        String txTime = format.format(AgentDashboardIndexPage._topUpDateTime);

        _searchCriteria = txTime.split(" ")[0];

        testStepsLog(_logStep++, "Enter Search Criteria");
        testInfoLog("Search", _searchCriteria);
        type(txtInputSearch, _searchCriteria);

        return new AgentTransactionHistoryVerification(driver);
    }

    public AgentTransactionHistoryVerification enterSearchCriteria(boolean isInvalid) {

        if (isInvalid) {

            scrollToElement(driver, txtInputSearch);

            String searchCriteria = getRandomCharacters(10);
            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", searchCriteria);
            type(txtInputSearch, searchCriteria);

        } else {

            scrollToElement(driver, txtInputSearch);

            // _searchCriteria = getInnerText(lstCommissionTxTime.get(0)).substring(0, 10);

            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", _searchCriteria);
            type(txtInputSearch, _searchCriteria);
        }

        return new AgentTransactionHistoryVerification(driver);
    }

    public boolean isTransactionDataDisplay() {

        try {
            updateShowList(driver, findElementByName(driver, "trans-history-table_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        return !isElementPresent(lblNoTransaction);
    }

    public AgentTransactionHistoryVerification enterTransactionSearchIDCriteria(boolean isInvalid) {

        if (isInvalid) {

            scrollToElement(driver, txtInputSearch);

            String searchCriteria = String.valueOf(getRandomNumber());
            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", searchCriteria);
            type(txtInputSearch, searchCriteria);

        } else {

            scrollToElement(driver, txtInputSearch);

            _searchCriteria = getInnerText(lstUserTxID.get(getRandomNumberBetween(0, lastIndexOf(lstUserTxID)))).split("-")[1];

            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", _searchCriteria);
            type(txtInputSearch, _searchCriteria);
        }

        return new AgentTransactionHistoryVerification(driver);
    }

    public AgentTransactionHistoryVerification clickTransactionTopUpToSort() {

        try {
            updateShowList(driver, findElementByName(driver, "trans-history-table_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        for (WebElement name : lstUserTopUpAmount) {
            _transactionTopUpBeforeSort.add(getDoubleFromString(getInnerText(name)));
        }

        testStepsLog(_logStep++, "Click On Top up to sort.");
        clickOn(driver, lblTopupHeader);

        return new AgentTransactionHistoryVerification(driver);
    }

    public AgentTransactionHistoryVerification clickCommissionToSort() {

        for (WebElement name : lstCommissionAmount) {
            _commissionAmountBeforeSort.add(getDoubleFromString(getInnerText(name)));
        }

        testStepsLog(_logStep++, "Click On Commission to sort.");
        clickOn(driver, lblCommissionHeader);

        return new AgentTransactionHistoryVerification(driver);
    }

    public AgentTransactionHistoryVerification clickOnCommissionDateTimeToSort() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        for (WebElement status : lstTransactionTime) {
            _transactionDateTimeBeforeSort.add(LocalDateTime.parse(getInnerText(status), format));
        }

        testStepsLog(_logStep++, "Click On Date & Time to sort.");
        clickOn(driver, lblCommissionDateTimeHeader);

        return new AgentTransactionHistoryVerification(driver);
    }

    public AgentTransactionHistoryVerification clickOnFilterIcon() {

        scrollToElement(driver, btnFilter);

        testStepsLog(_logStep++, "Click on Filter Button.");
        clickOn(driver, btnFilter);

        pause(2);

        return new AgentTransactionHistoryVerification(driver);

    }

    public AgentTransactionHistoryVerification getConsumerDetailsForFilter() {

        pause(2);

        try {
            updateShowList(driver, findElementByName(driver, "trans-history-table_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        for (int user = 0; user < sizeOf(lstUserType); user++) {
            if (getText(lstUserType.get(user)).equalsIgnoreCase("Consumer")) {
                _filterFirstName = getInnerText(lstUserName.get(user)).split(" ")[0].trim();
                _filterLastName = getInnerText(lstUserName.get(user)).replace(_filterFirstName, "").trim();
                _filterPhone = getInnerText(lstUserIdNumber.get(user));
                _filterTransactionId = String.valueOf(getIntegerFromString(getInnerText(lstUserTxID.get(user))));

                for (int num = 0; num < sizeOf(lstCommissionAmount); num++) {
                    _commissionTopUpForFilter.add(getDoubleFromString(getInnerText(lstUserTopUpAmount.get(num))));
                    _commissionAmountForFilter.add(getDoubleFromString(getInnerText(lstCommissionAmount.get(num))));
                }

                Collections.sort(_commissionAmountForFilter);
                Collections.sort(_commissionTopUpForFilter);

                break;
            }

        }

        return new AgentTransactionHistoryVerification(driver);

    }

    public AgentTransactionHistoryVerification getMerchantDetailsForFilter() {

        pause(2);

        try {
            updateShowList(driver, findElementByName(driver, "trans-history-table_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        _commissionAmountForFilter.clear();
        _commissionTopUpForFilter.clear();

        for (int user = 0; user < sizeOf(lstUserType); user++) {
            if (getText(lstUserType.get(user)).equalsIgnoreCase("Merchant")) {
                _filterShopName = getInnerText(lstUserName.get(user));
                _filterID = getInnerText(lstUserIdNumber.get(user));
                _filterTransactionId = String.valueOf(getIntegerFromString(getInnerText(lstUserTxID.get(user))));

                for (int num = 0; num < sizeOf(lstCommissionAmount); num++) {
                    _commissionTopUpForFilter.add(getDoubleFromString(getInnerText(lstUserTopUpAmount.get(num))));
                    _commissionAmountForFilter.add(getDoubleFromString(getInnerText(lstCommissionAmount.get(num))));
                }

                Collections.sort(_commissionAmountForFilter);
                Collections.sort(_commissionTopUpForFilter);

                break;
            }

        }

        return new AgentTransactionHistoryVerification(driver);

    }

    public AgentTransactionHistoryVerification filterFirstName(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomCharacters(7);
            testStepsLog(_logStep++, "Enter First Name : " + _filterRandomValue);
            type(txtFilterFirstName, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter First Name : " + _filterFirstName);
            type(txtFilterFirstName, _filterFirstName);
        }

        return new AgentTransactionHistoryVerification(driver);
    }

    public AgentTransactionHistoryVerification changeFilterUserTo(String user) {

        scrollToElement(driver, btnUserDropdown);

        clickOn(driver, btnUserDropdown);

        if (user.equalsIgnoreCase("Merchant"))
            clickOn(driver, lstFilterUser.get(1));
        else
            clickOn(driver, lstFilterUser.get(0));

        testStepsLog(_logStep++, "Select User : " + user);

        return new AgentTransactionHistoryVerification(driver);

    }

    public AgentTransactionHistoryVerification clickConsumerOnFilterSearchButton() {

        testStepsLog(_logStep++, "Click on Search button.");
        clickOn(driver, btnFilterSearch);

        pause(2);

        try {
            updateShowList(driver, findElementByName(driver, "trans-history-table_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        pause(1);

        return new AgentTransactionHistoryVerification(driver);

    }

    public AgentTransactionHistoryVerification filterLastName(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomCharacters(7);
            testStepsLog(_logStep++, "Enter Last Name : " + _filterRandomValue);
            type(txtFilterLastName, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter Last Name : " + _filterLastName);
            type(txtFilterLastName, _filterLastName);
        }

        return new AgentTransactionHistoryVerification(driver);
    }

    public AgentTransactionHistoryVerification clearAllMerchantFilterDetails() {

        testStepsLog(_logStep++, "Click on Clear All button.");
        clickOn(driver, btnFilterClearAll);

        pause(2);

        try {
            updateShowList(driver, findElementByName(driver, "trans-history-table_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        changeFilterUserTo("Merchant");

        pause(1);

        return new AgentTransactionHistoryVerification(driver);

    }

    public AgentTransactionHistoryVerification clearAllConsumerFilterDetails() {

        testStepsLog(_logStep++, "Click on Clear All button.");
        clickOn(driver, btnFilterClearAll);

        pause(2);

        try {
            updateShowList(driver, findElementByName(driver, "trans-history-table_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        pause(1);

        return new AgentTransactionHistoryVerification(driver);

    }

    public AgentTransactionHistoryVerification filterConsumerNumber(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomNumber() + "" + getRandomNumber();
            testStepsLog(_logStep++, "Enter Mobile Number : " + _filterRandomValue);
            type(txtFilterConsumerMobile, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter Mobile Number : " + _filterPhone);
            type(txtFilterConsumerMobile, _filterPhone);
        }

        return new AgentTransactionHistoryVerification(driver);
    }

    public AgentTransactionHistoryVerification filterTransactionId(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        scrollToElement(driver, txtFilterTransactionID);

        if (isInvalid) {
            _filterRandomValue = "Transaction ID - " + getRandomNumber() + "" + getRandomNumberBetween(1, 9);
            testStepsLog(_logStep++, "Enter Tx ID Number : " + _filterRandomValue);
            type(txtFilterTransactionID, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter Tx ID Number : " + _filterTransactionId);
            type(txtFilterTransactionID, _filterTransactionId);
        }

        return new AgentTransactionHistoryVerification(driver);
    }

    public AgentTransactionHistoryVerification enterTopUpAmountRange(boolean isInvalid) {

        _isRandomFilter = isInvalid;
        scrollToElement(driver, txtFilterTopUpMaxAmount);

        if (isInvalid) {
            double randomNumber = getRandomNumber();
            _filterTopUpFromAmount = randomNumber + 10;
            _filterTopUpToAmount = randomNumber;

            testStepsLog(_logStep++, "Enter From Amount Range : " + _filterTopUpFromAmount);
            type(txtFilterTopUpMinAmount, String.valueOf(_filterTopUpFromAmount));

            testStepsLog(_logStep++, "Enter To Amount Range : " + _filterTopUpToAmount);
            type(txtFilterTopUpMaxAmount, String.valueOf(_filterTopUpToAmount));
        } else {
            _filterTopUpFromAmount = formatTwoDecimal(findMin(_commissionTopUpForFilter)) - 100;
            _filterTopUpToAmount = formatTwoDecimal(findMax(_commissionTopUpForFilter)) + 100;

            testStepsLog(_logStep++, "Enter From Amount Range : " + _filterTopUpFromAmount);
            type(txtFilterTopUpMinAmount, String.valueOf(_filterTopUpFromAmount));

            testStepsLog(_logStep++, "Enter To Amount Range : " + _filterTopUpToAmount);
            type(txtFilterTopUpMaxAmount, String.valueOf(_filterTopUpToAmount));
        }

        return new AgentTransactionHistoryVerification(driver);

    }

    public AgentTransactionHistoryVerification enterTopUpAmount(String field) {

        scrollToElement(driver, txtFilterTopUpMaxAmount);

        System.out.println(_commissionTopUpForFilter);

        _filterTopUpFromAmount = formatTwoDecimal(findMin(_commissionTopUpForFilter)) - 100;
        _filterTopUpToAmount = formatTwoDecimal(findMax(_commissionTopUpForFilter)) + 100;

        switch (field.toLowerCase()) {
            case "from":
                testStepsLog(_logStep++, "Enter From Amount Range : " + _filterTopUpFromAmount);
                type(txtFilterTopUpMinAmount, String.valueOf(_filterTopUpFromAmount));
                break;
            case "to":
                testStepsLog(_logStep++, "Enter To Amount Range : " + _filterTopUpToAmount);
                type(txtFilterTopUpMaxAmount, String.valueOf(_filterTopUpToAmount));
                break;
        }

        return new AgentTransactionHistoryVerification(driver);

    }

    public AgentTransactionHistoryVerification enterCommissionAmountRange(boolean isInvalid) {

        _isRandomFilter = isInvalid;
        scrollToElement(driver, txtFilterCommissionMaxAmount);

        if (isInvalid) {
            double randomNumber = getRandomNumber() / 1000;
            _filterCommissionFromAmount = randomNumber + 10;
            _filterCommissionToAmount = randomNumber;

            testStepsLog(_logStep++, "Enter From Amount Range : " + _filterCommissionFromAmount);

            type(txtFilterCommissionMinAmount, String.valueOf(_filterCommissionFromAmount));

            testStepsLog(_logStep++, "Enter To Amount Range : " + _filterCommissionToAmount);
            type(txtFilterCommissionMaxAmount, String.valueOf(_filterCommissionToAmount));
        } else {
            _filterCommissionFromAmount = formatTwoDecimal(findMin(_commissionAmountForFilter) - 0.05);
            _filterCommissionToAmount = formatTwoDecimal(findMax(_commissionAmountForFilter) + 0.05);

            testStepsLog(_logStep++, "Enter From Amount Range : " + _filterCommissionFromAmount);
            type(txtFilterCommissionMinAmount, String.valueOf(_filterCommissionFromAmount));

            testStepsLog(_logStep++, "Enter To Amount Range : " + _filterTopUpToAmount);
            type(txtFilterCommissionMaxAmount, String.valueOf(_filterTopUpToAmount));
        }

        return new AgentTransactionHistoryVerification(driver);

    }

    public AgentTransactionHistoryVerification enterCommissionAmount(String field) {

        scrollToElement(driver, txtFilterCommissionMaxAmount);

        _filterCommissionFromAmount = formatTwoDecimal(findMin(_commissionAmountForFilter) - 0.05);
        _filterCommissionToAmount = formatTwoDecimal(findMax(_commissionAmountForFilter) + 0.05);

        switch (field.toLowerCase()) {
            case "from":
                testStepsLog(_logStep++, "Enter From Amount Range : " + _filterCommissionFromAmount);
                type(txtFilterCommissionMinAmount, String.valueOf(_filterCommissionFromAmount));
                break;
            case "to":
                testStepsLog(_logStep++, "Enter To Amount Range : " + _filterCommissionToAmount);
                type(txtFilterCommissionMaxAmount, String.valueOf(_filterCommissionToAmount));
                break;
        }

        return new AgentTransactionHistoryVerification(driver);

    }

    public AgentTransactionHistoryVerification filterShopName(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomCharacters(7);
            testStepsLog(_logStep++, "Enter Shop Name : " + _filterRandomValue);
            type(txtFilterShopName, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter Shop Name : " + _filterShopName);
            type(txtFilterShopName, _filterShopName);
        }

        return new AgentTransactionHistoryVerification(driver);
    }

    public AgentTransactionHistoryVerification filterMerchantID(boolean isInvalid) {

        if (isInvalid) {
            _filterRandomValue = getRandomNumberBetween(10000, 99999) + "" + getRandomNumberBetween(10000, 99999);
            testStepsLog(_logStep++, "Enter Merchant ID");
            testInfoLog("Merchant ID", _filterRandomValue);
            type(txtFilterID, _filterRandomValue);

        } else {
            testStepsLog(_logStep++, "Enter Merchant ID");
            testInfoLog("Merchant ID", _filterID);
            type(txtFilterID, _filterID);
        }

        return new AgentTransactionHistoryVerification(driver);
    }

    public AgentTransactionHistoryVerification searchUser(String userType) {

        pause(3);

        scrollToElement(driver, txtInputSearch);

        _searchCriteria = userType;

        testStepsLog(_logStep++, "Enter Search Criteria");
        testInfoLog("Search", _searchCriteria);
        type(txtInputSearch, _searchCriteria);

        return new AgentTransactionHistoryVerification(driver);
    }

    public AgentTransactionHistoryVerification getAnyConsumerDetails() {

        _filterRandomValue = String.valueOf(getRandomNumberBetween(0, sizeOf(lstUserName) - 1));

        _filterFirstName = getInnerText(lstUserName.get(getIntegerFromString(_filterRandomValue)));
        _filterPhone = getInnerText(lstUserIdNumber.get(getIntegerFromString(_filterRandomValue)));
        _filterTransactionId = String.valueOf(getIntegerFromString(getInnerText(lstUserTxID.get(getIntegerFromString(_filterRandomValue)))));
        _transactionDateAndTime = getInnerText(lstTransactionTime.get(getIntegerFromString(_filterRandomValue)));

        _transactionTopUpAmount = getInnerText(lstUserTopUpAmount.get(getIntegerFromString(_filterRandomValue)));
        _transactionCommissionAmount = (getInnerText(lstCommissionAmount.get(getIntegerFromString(_filterRandomValue))));

        return new AgentTransactionHistoryVerification(driver);
    }

    public AgentTransactionHistoryVerification clickOnMOREButton() {

        scrollToElement(driver, lstTransactionMOREButton.get(getIntegerFromString(_filterRandomValue)));

        testStepsLog(_logStep++, "Click on MORE button.");
        clickOn(driver, lstTransactionMOREButton.get(getIntegerFromString(_filterRandomValue)));

        return new AgentTransactionHistoryVerification(driver);

    }

    public AgentTransactionHistoryVerification clickOnClosePopUp() {

        testStepsLog(_logStep++, "Click on Close button.");
        clickOn(driver, btnCloseTransactionPopUp);

        return new AgentTransactionHistoryVerification(driver);

    }

    public AgentTransactionHistoryVerification clearMerchantIDField() {

        testStepsLog(_logStep++, "Click on Clear button.");
        clickOn(driver, btnClearMerchantID);

        return new AgentTransactionHistoryVerification(driver);
    }

    public AgentTransactionHistoryVerification clearMerchantTopupAmountRange() {

        testStepsLog(_logStep++, "Click on Clear Amount Range button.");
        clickOn(driver, btnClearTopUpAmountRange);

        return new AgentTransactionHistoryVerification(driver);
    }

    public AgentTransactionHistoryVerification clearMerchantCommissionAmountRange() {

        testStepsLog(_logStep++, "Click on Clear Amount Range button.");
        clickOn(driver, btnClearCommissionAmountRange);

        return new AgentTransactionHistoryVerification(driver);

    }
}
