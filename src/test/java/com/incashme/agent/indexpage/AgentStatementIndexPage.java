package com.incashme.agent.indexpage;

import com.framework.init.AbstractPage;
import com.incashme.agent.verification.AgentStatementVerification;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;

public class AgentStatementIndexPage extends AbstractPage {

    public AgentStatementIndexPage(WebDriver driver) {
        super(driver);
    }

    public static long _expectedStatements;

    public static String _statementMonthYear;
    public static String _statementMonth;
    public static String _statementYear;

    @FindBy(xpath = "//div[contains(@class,'profile-details')]//following-sibling::div[contains(text(),'Since')]")
    private WebElement lblProfileSince;

    @FindBy(xpath = "//ul/li[contains(@id,'sb_stmt')]")
    private WebElement btnMenuStatement;

    @FindBy(xpath = "//li/button[contains(@class,'filterBtn')]")
    private WebElement btnFilter;

    @FindBy(xpath = "//p[contains(@class,'ok_btn')]")
    private WebElement btnFilterOK;

    @FindBy(xpath = "//button[@title]")
    private List<WebElement> lstBtnYear;

    @FindBy(xpath = " //div[@role='gridcell']")
    private List<WebElement> lstBtnMonth;

    @FindAll(value = {@FindBy(xpath = "//div[@class='stmnt-cntnt']")})
    private List<WebElement> lstStatements;

    @FindAll(value = {@FindBy(xpath = "//div[@class='stmnt-view']//button")})
    private List<WebElement> lstStatementDownload;

    public AgentStatementVerification getProfileCreatedMonth() {

        pause(3);

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd MMMM yyyy", Locale.ENGLISH);

        LocalDate joinMonthYear = LocalDate.parse(getText(lblProfileSince).
                replace("Since - ", "").trim(), format);
        LocalDate currentMonthYear = LocalDate.parse(format.format(LocalDate.now()), format);

        System.out.println(joinMonthYear);
        System.out.println(currentMonthYear);

        _expectedStatements = getMonthDifferenceBetween(joinMonthYear, currentMonthYear);

        System.out.println(_expectedStatements);

        return new AgentStatementVerification(driver);
    }

    public AgentStatementVerification clickOnStatementMenu() {

        testStepsLog(_logStep++, "Click on Statement Menu icon.");
        clickOn(driver, btnMenuStatement);

        return new AgentStatementVerification(driver);
    }

    public AgentStatementVerification clickOnViewButton() {

        _statementMonthYear = getText(lstStatements.get(0)).split(" - ")[1] + "-" +
                getText(lstStatements.get(0)).split(" - ")[0];

        testStepsLog(_logStep++, "Click on View Button.");
        clickOnJS(driver, lstStatementDownload.get(0));

        pause(10);

        return new AgentStatementVerification(driver);
    }

    public boolean isStatementDisplay() {
        return !isListEmpty(lstStatements);
    }

    public AgentStatementVerification clickOnFilterButton() {

        testStepsLog(_logStep++, "Click on Filter Button.");
        clickOn(driver, btnFilter);

        return new AgentStatementVerification(driver);
    }

    public AgentStatementVerification selectStatementToFilter(boolean isInvalid) {

        clickOnFilterButton();

        if (isInvalid) {
            DateTimeFormatter format = DateTimeFormatter.ofPattern("MMM");
            _statementMonth = format.format(LocalDate.now());
            _statementYear = getText(lstStatements.get(0)).split("-")[1].trim();
        } else {
            _statementMonth = getText(lstStatements.get(0)).split("-")[0].trim();
            _statementYear = getText(lstStatements.get(0)).split("-")[1].trim();
        }

        while (!getText(lstBtnYear.get(1)).contains(_statementYear)) {
            clickOn(driver, lstBtnYear.get(0));
            pause(1);
        }

        for (int month = 0; month < sizeOf(lstBtnMonth); month++) {
            if (getText(lstBtnMonth.get(month)).equalsIgnoreCase(_statementMonth)) {
                clickOn(driver, lstBtnMonth.get(month));
            }
        }

        clickOnFilterOKButton();

        return new AgentStatementVerification(driver);

    }

    private void clickOnFilterOKButton() {
        testStepsLog(_logStep++, "Click on OK Button.");
        clickOn(driver, btnFilterOK);
    }

}
