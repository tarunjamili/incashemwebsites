package com.incashme.agent.verification;

import com.framework.init.AbstractPage;
import com.incashme.agent.indexpage.AgentConsumerTargetDetailsIndexPage;
import com.incashme.agent.indexpage.AgentDashboardIndexPage;
import com.incashme.agent.indexpage.AgentUserListIndexPage;
import com.incashme.agent.validations.DashboardValidations;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Rahul R.
 * Date: 2019-04-25
 * Time: 16:35
 * Project Name: InCashMe
 */
public class AgentConsumerTargetDetailsVerification extends AbstractPage implements DashboardValidations {

    public AgentConsumerTargetDetailsVerification(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//label[@id='p_total']")
    private WebElement btnChartTotal;

    @FindBy(xpath = "//li/a[contains(text(),'Consumer List')]")
    private WebElement btnConsumerList;

    @FindBy(xpath = "//li/button[contains(@class,'filterBtn')]")
    private WebElement btnFilter;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[1]//h5")})
    private List<WebElement> lstConsumerName;

    @FindBy(xpath = "//div//p[contains(text(),'couldn’t find any matches')]")
    private WebElement lblNoAddedConsumerDetails;

    @FindBy(xpath = "//*[contains(text(),'Consumers List')]//following-sibling::button")
    private WebElement btnCloseConsumerList;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@class,'add-user-list')]//h5")})
    private List<WebElement> lstAddedConsumerNames;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@class,'add-user-list')]//h5//following-sibling::div")})
    private List<WebElement> lstAddedConsumerNumbers;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[1]//h5")})
    private List<WebElement> lstAddedConsumerName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[2]//span")})
    private List<WebElement> lstAddedConsumerNumber;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[3]//div")})
    private List<WebElement> lstConsumerStatus;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[4]")})
    private List<WebElement> lstConsumerCreatedDateTime;

    @FindBy(xpath = "//input[@type='search']")
    private WebElement txtInputSearch;

    @FindBy(xpath = "//table//tr//td[contains(text(),'matching')]")
    private WebElement lblNoRecordsFound;

    @FindAll(value = {@FindBy(xpath = "//table[@id='usersTbl2']//tr//td[1]//h5/following-sibling::div[1]")})
    private List<WebElement> lstListConsumerNumber;

    @FindBy(xpath = "//div[contains(@id,'usersTbl2_filter')]//input[@type='search']")
    private WebElement txtPopupInputSearch;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//button[contains(@class,'searchbtn')]")
    private WebElement btnFilterSearch;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//a[contains(text(),'Clear All')]")
    private WebElement btnFilterClearAll;

    @FindBy(xpath = "//div//h2[contains(text(),'Wakeup')]")
    private WebElement lblNoFilterConsumerAvailable;

    public boolean verifyConsumerTargetDetailsScreen() {

        boolean bool;

        if (AgentConsumerTargetDetailsIndexPage._targetLeftInConsumerDetails == 0)
            bool = AgentConsumerTargetDetailsIndexPage._targetAchievedInConsumerDetails >=
                    (AgentConsumerTargetDetailsIndexPage._totalTargetForConsumer);
        else
            bool = AgentConsumerTargetDetailsIndexPage._targetAchievedInConsumerDetails ==
                    (AgentConsumerTargetDetailsIndexPage._totalTargetForConsumer -
                            AgentConsumerTargetDetailsIndexPage._targetLeftInConsumerDetails);


        System.out.println(AgentConsumerTargetDetailsIndexPage._targetAchievedInConsumerDetails);
        System.out.println(AgentConsumerTargetDetailsIndexPage._totalTargetForConsumer);
        System.out.println(AgentConsumerTargetDetailsIndexPage._targetLeftInConsumerDetails);

        scrollToElement(driver, btnChartTotal);

        bool = bool && AgentConsumerTargetDetailsIndexPage._daysCompletedInConsumerDetails ==
                (AgentConsumerTargetDetailsIndexPage._totalDaysInConsumerDetails -
                        AgentConsumerTargetDetailsIndexPage._daysLeftInConsumerDetails);

        System.out.println(AgentConsumerTargetDetailsIndexPage._daysCompletedInConsumerDetails);
        System.out.println(AgentConsumerTargetDetailsIndexPage._totalDaysInConsumerDetails);
        System.out.println(AgentConsumerTargetDetailsIndexPage._daysLeftInConsumerDetails);

        bool = bool && AgentDashboardIndexPage._consumerTargetCompleted == AgentConsumerTargetDetailsIndexPage._targetAchievedInConsumerDetails &&
                AgentDashboardIndexPage._consumersTarget == AgentConsumerTargetDetailsIndexPage._totalTargetForConsumer &&
                AgentDashboardIndexPage._consumersDaysLeft == AgentConsumerTargetDetailsIndexPage._daysLeftInConsumerDetails;

        System.out.println(AgentDashboardIndexPage._consumerTargetCompleted);
        System.out.println(AgentConsumerTargetDetailsIndexPage._targetAchievedInConsumerDetails);
        System.out.println(AgentDashboardIndexPage._consumersTarget);
        System.out.println(AgentConsumerTargetDetailsIndexPage._totalTargetForConsumer);
        System.out.println(AgentDashboardIndexPage._consumersDaysLeft);
        System.out.println(AgentConsumerTargetDetailsIndexPage._daysLeftInConsumerDetails);

        return bool && isElementDisplay(btnConsumerList) && isElementDisplay(btnFilter);
    }

    public boolean verifyConsumerListCount() {

        if (isListEmpty(lstConsumerName)) {
            pause(2);
            testValidationLog(getInnerText(lblNoAddedConsumerDetails));
            return isListEmpty(lstConsumerName)
                    && getInnerText(lblNoAddedConsumerDetails).contains(NO_CREATED_CONSUMER_LIST_MESSAGE);
        } else {
            updateShowList(driver, findElementByName(driver, "dataTbl_length"), "All");

            return !isElementPresent(lblNoAddedConsumerDetails)
                    && (sizeOf(lstConsumerName) == AgentConsumerTargetDetailsIndexPage._totalConsumersFromChart);
        }
    }

    public boolean verifyTotalAddedConsumerDetails() {

        if (sizeOf(AgentConsumerTargetDetailsIndexPage._lstConsumerNames) == 0) {
            pause(2);
            String validation = getText(lblNoAddedConsumerDetails);
            testValidationLog(validation);
            testStepsLog(_logStep++, "Click on Close button.");
            clickOn(driver, btnCloseConsumerList);
            return sizeOf(lstAddedConsumerNames) == sizeOf(AgentConsumerTargetDetailsIndexPage._lstConsumerNames) &&
                    validation.equalsIgnoreCase(NO_TOTAL_ADDED_CONSUMER_MESSAGE);
        } else {

            updateShowList(driver, findElementByName(driver, "usersTbl2_length"), "All");

            testInfoLog("Total Added Consumers", String.valueOf(sizeOf(lstAddedConsumerNames)));
            testInfoLog("________________________________________________________________________", "");
            for (int conNum = 0; conNum < sizeOf(lstAddedConsumerNames); conNum++) {

                testInfoLog("Consumer Name :" + getInnerText(lstAddedConsumerNames.get(conNum))
                        + "<br>Consumer Number : " + getInnerText(lstAddedConsumerNumbers.get(conNum)), "");
                testInfoLog("________________________________________________________________________", "");
            }
            return sizeOf(lstAddedConsumerNames) == sizeOf(AgentConsumerTargetDetailsIndexPage._lstConsumerNames);
        }

    }

    public boolean verifyConsumerNameSorted() {

        List<String> consumerNameAfterSort = new ArrayList<>();
        List<String> consumerNameBeforeSort = AgentConsumerTargetDetailsIndexPage._consumerNameBeforeSort;

        for (WebElement name : lstAddedConsumerName) {
            consumerNameAfterSort.add(getInnerText(name));
        }

        consumerNameBeforeSort.sort(
                (String o1, String o2) -> {
                    if (o1.equalsIgnoreCase(o2)) {
                        return o2.compareTo(o1);
                    }
                    return o1.toLowerCase().compareTo(o2.toLowerCase());
                }
        );

        System.out.println(consumerNameAfterSort);
        System.out.println(consumerNameBeforeSort);

        System.out.println(consumerNameBeforeSort.equals(consumerNameAfterSort));

        return consumerNameBeforeSort.equals(consumerNameAfterSort);
    }

    public boolean verifyConsumerNumberSorted() {

        List<String> consumerNumberAfterSort = new ArrayList<>();
        List<String> consumerNumberBeforeSort = AgentConsumerTargetDetailsIndexPage._consumerNumberBeforeSort;

        for (WebElement name : lstAddedConsumerNumber) {
            consumerNumberAfterSort.add(getInnerText(name));
        }

        consumerNumberBeforeSort.sort(
                (String o1, String o2) -> {
                    if (o1.equalsIgnoreCase(o2)) {
                        return o2.compareTo(o1);
                    }
                    return o1.toLowerCase().compareTo(o2.toLowerCase());
                }
        );

        System.out.println(consumerNumberAfterSort);
        System.out.println(consumerNumberBeforeSort);

        System.out.println(consumerNumberBeforeSort.equals(consumerNumberAfterSort));

        return consumerNumberBeforeSort.equals(consumerNumberAfterSort);
    }

    public boolean verifyConsumerStatusSorted() {

        List<String> agentStatusAfterSort = new ArrayList<>();
        List<String> agentStatusBeforeSort = AgentConsumerTargetDetailsIndexPage._consumerStatusBeforeSort;

        for (WebElement name : lstConsumerStatus) {
            agentStatusAfterSort.add(getInnerText(name));
        }

        agentStatusBeforeSort.sort(
                (String o1, String o2) -> {
                    if (o1.equalsIgnoreCase(o2)) {
                        return o2.compareTo(o1);
                    }
                    return o1.toLowerCase().compareTo(o2.toLowerCase());
                }
        );

        System.out.println(agentStatusAfterSort);
        System.out.println(agentStatusBeforeSort);

        System.out.println(agentStatusBeforeSort.equals(agentStatusAfterSort));

        return agentStatusBeforeSort.equals(agentStatusAfterSort);
    }


    public boolean verifyConsumerDateTimeSorted() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        List<LocalDateTime> agentDateTimeAfterSort = new ArrayList<>();
        List<LocalDateTime> agentDateTimeBeforeSort = AgentConsumerTargetDetailsIndexPage._consumerDateTimeBeforeSort;

        for (WebElement name : lstConsumerCreatedDateTime) {
            agentDateTimeAfterSort.add(LocalDateTime.parse(getInnerText(name), format));
        }

        Collections.sort(agentDateTimeBeforeSort);

        System.out.println(agentDateTimeAfterSort);
        System.out.println(agentDateTimeBeforeSort);

        System.out.println(agentDateTimeBeforeSort.equals(agentDateTimeAfterSort));

        return agentDateTimeBeforeSort.equals(agentDateTimeAfterSort);
    }

    public boolean verifyConsumerSearchSuccessful() {

        boolean bool = getInnerText(lstAddedConsumerNumber.get(0)).contains(AgentConsumerTargetDetailsIndexPage._searchCriteria) &&
                sizeOf(lstAddedConsumerNumber) == 1;

        type(txtInputSearch, "");

        return bool;

    }

    public boolean verifyConsumerValidationForInvalidSearch() {

        testValidationLog(getInnerText(lblNoRecordsFound));

        boolean bool = getInnerText(lblNoRecordsFound).equalsIgnoreCase(NO_SEARCH_RECORD_FOUND);

        type(txtInputSearch, "x");
        txtInputSearch.clear();

        return bool;
    }

    public boolean verifyListConsumerSearchSuccessful() {

        boolean bool = getInnerText(lstListConsumerNumber.get(0)).contains(AgentConsumerTargetDetailsIndexPage._searchCriteria) &&
                sizeOf(lstListConsumerNumber) == 1;

        type(txtPopupInputSearch, "");

        return bool;

    }

    public boolean verifyValidationForListInvalidSearch() {

        testValidationLog(getInnerText(lblNoRecordsFound));

        boolean bool = getInnerText(lblNoRecordsFound).equalsIgnoreCase(NO_SEARCH_RECORD_FOUND);

        type(txtPopupInputSearch, "x");
        txtPopupInputSearch.clear();

        return bool;
    }

    public boolean verifyCreatedConsumerSearchSuccessful() {

        boolean bool = getInnerText(lstAddedConsumerNumber.get(0)).contains(AgentUserListIndexPage._newUserMobileNumber) &&
                sizeOf(lstAddedConsumerNumber) == 1;

        type(txtInputSearch, "");

        return bool;

    }

    public boolean verifyConsumerFilterScreenDisplay() {

        updateShowList(driver, findElementByName(driver, "dataTbl_length"), "All");

        return isElementDisplay(btnFilterSearch) && isElementDisplay(btnFilterClearAll);
    }

    public boolean verifyRandomDataFiltered() {
        return getText(lblNoFilterConsumerAvailable).equalsIgnoreCase(NO_FILTER_DATA_AVAILABLE);
    }

    public boolean verifyNameFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement number : lstAddedConsumerNumber) {
            finalFilterResult.add(getInnerText(number));
        }

        System.out.println(finalFilterResult);
        System.out.println(AgentConsumerTargetDetailsIndexPage._filterConsumerPhone);

        return finalFilterResult.contains(AgentConsumerTargetDetailsIndexPage._filterConsumerPhone);
    }

    public boolean verifyPhoneNumberFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement number : lstAddedConsumerNumber) {
            finalFilterResult.add(getInnerText(number));
        }

        System.out.println(finalFilterResult);
        System.out.println(AgentConsumerTargetDetailsIndexPage._filterConsumerPhone);

        return finalFilterResult.contains(AgentConsumerTargetDetailsIndexPage._filterConsumerPhone);
    }

    public boolean verifyStatusFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement status : lstConsumerStatus) {
            finalFilterResult.add(getInnerText(status));
        }

        System.out.println(finalFilterResult);
        System.out.println(AgentConsumerTargetDetailsIndexPage._filterStatus);

        if (isListEmpty(finalFilterResult)) {
            testValidationLog(getText(lblNoFilterConsumerAvailable));
            return !AgentConsumerTargetDetailsIndexPage._consumerStatusBeforeSort.contains(AgentConsumerTargetDetailsIndexPage._filterStatus)
                    && getText(lblNoFilterConsumerAvailable).equalsIgnoreCase(NO_FILTER_DATA_AVAILABLE);
        } else {
            return finalFilterResult.stream().distinct().limit(2).count() <= 1 &&
                    finalFilterResult.contains(AgentConsumerTargetDetailsIndexPage._filterStatus);
        }
    }

}
