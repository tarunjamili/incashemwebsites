package com.incashme.agent.verification;

import com.framework.init.AbstractPage;
import com.incashme.agent.indexpage.*;
import com.incashme.agent.validations.UsersListValidation;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Rahul R.
 * Date: 2019-04-25
 * Time: 16:53
 * Project Name: InCashMe
 */
public class AgentUserListVerification extends AbstractPage implements UsersListValidation {

    public AgentUserListVerification(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//div[contains(@class,'spr-usr-name')]")
    private WebElement lblConsumerNameInProfile;

    @FindBy(xpath = "//div[contains(@class,'spr-usr-name')]")
    private WebElement lblMerchantNameInProfile;

    @FindBy(xpath = "//div[contains(@class,'spr-usr-id')]")
    private WebElement lblMerchantIDInProfile;

    @FindBy(xpath = "//input[@formcontrolname='first_name']")
    private WebElement txtFirstName;

    @FindBy(xpath = "//input[@formcontrolname='last_name']")
    private WebElement txtLastName;

    @FindBy(xpath = "//input[contains(@placeholder,'Date of Birth')]")
    private WebElement txtDOBPicker;

    @FindBy(xpath = "//input[@formcontrolname='email']")
    private WebElement txtEmailAddress;

    @FindBy(xpath = "//input[@formcontrolname='phone']")
    private WebElement txtPhoneNumber;

    @FindBy(xpath = "//div[@id='step1']//button[contains(text(),'Next')]")
    private WebElement btnNextStep1;

    @FindAll(value = {@FindBy(xpath = "//div[@class='input-group']")})
    private List<WebElement> lstFileUploads;

    @FindBy(xpath = "//div[@class='input-group']//select[@id='fileType']")
    private WebElement selectFileType;

    @FindBy(xpath = "//input[@formcontrolname='username']")
    private WebElement txtUsername;

    @FindBy(xpath = ".//input[@id='otp_val']")
    private WebElement txtStep2OTP;

    @FindBy(xpath = "//div[@id='step2']//button[contains(text(),'Next')]")
    private WebElement btnNextStep2;

    @FindBy(xpath = "//div[@id='step3']//button[contains(text(),'Next')]")
    private WebElement btnNextStep3;

    @FindBy(xpath = "//div[@id='step2']//button[contains(text(),'Previous')]")
    private WebElement btnPreviousStep2;

    @FindBy(xpath = "//input[@formcontrolname='address1']")
    private WebElement txtHomeFlatAddress;

    @FindBy(xpath = "//input[@formcontrolname='address2']")
    private WebElement txtStreet1Address;

    @FindBy(xpath = "//input[@formcontrolname='address3']")
    private WebElement txtStreet2Address;

    @FindBy(xpath = "//input[@formcontrolname='village']")
    private WebElement txtVillage;

    @FindBy(xpath = "//input[@formcontrolname='tehsil']")
    private WebElement txtTehsil;

    @FindBy(xpath = "//input[@formcontrolname='district']")
    private WebElement txtDistrict;

    @FindBy(xpath = "//input[@formcontrolname='country']")
    private WebElement txtCountry;

    @FindBy(xpath = "//input[@formcontrolname='pincode']")
    private WebElement txtPinCode;

    @FindBy(xpath = "//select[@formcontrolname='state']")
    private WebElement btnState;

    @FindBy(xpath = "//select[@formcontrolname='city']")
    private WebElement btnCity;

    @FindBy(xpath = "//div[contains(text(),'Added Consumers')]//following-sibling::div")
    private WebElement lblAddedConsumerSection;

    @FindBy(xpath = "//div[contains(text(),'Consumers')]//following-sibling::div//span[contains(@class,'chart-velue')]")
    private WebElement lblAddedConsumers;

    @FindBy(xpath = "//div[contains(text(),'Merchants')]//following-sibling::div//span[contains(@class,'chart-velue')]")
    private WebElement lblAddedMerchants;

    @FindBy(xpath = "//div[@id='step3']//button[contains(text(),'Cancel')]")
    private WebElement btnConsumerCancel;

    @FindBy(xpath = "//div[@id='step4']//button[contains(text(),'Cancel')]")
    private WebElement btnMerchantCancel;

    @FindBy(xpath = "//div[@id='step3']//button[contains(text(),'Create')]")
    private WebElement btnCreateConsumer;

    @FindBy(xpath = "//div[@id='step4']//button[contains(text(),'Create')]")
    private WebElement btnCreateMerchant;

    @FindBy(xpath = "//input[@formcontrolname='companyname']")
    private WebElement txtBusinessName;

    @FindBy(xpath = "//input[@formcontrolname='dba']")
    private WebElement txtDBAName;

    @FindBy(xpath = "//input[@formcontrolname='filter_phone']")
    private WebElement txtBusinessPhone;

    @FindBy(xpath = "//input[@formcontrolname='taxid']")
    private WebElement txtGSTNumber;

    @FindBy(xpath = "//input[@formcontrolname='website']")
    private WebElement txtBusinessURL;

    @FindBy(xpath = "//mdb-select[@formcontrolname='bustype']")
    private WebElement btnBusinessTypeDropdown;

    @FindAll(value = {@FindBy(xpath = "//mdb-select[@formcontrolname='bustype']//li/span")})
    private List<WebElement> lstBusinessType;

    @FindBy(xpath = "//div[contains(text(),'Added Merchants')]//following-sibling::div")
    private WebElement lblAddedMerchantSection;

    @FindBy(xpath = "//small[contains(@class,'text-danger') and contains(text(),'First Name')]")
    private WebElement lblFirstNameValidation;

    @FindBy(xpath = "//small[contains(@class,'text-danger') and contains(text(),'Last Name')]")
    private WebElement lblLastNameValidation;

    @FindBy(xpath = "//small[contains(@class,'text-danger') and contains(text(),'User Name')]")
    private WebElement lblUserNameValidation;

    @FindBy(xpath = "//small[contains(@class,'text-danger') and contains(text(),'valid email')]")
    private WebElement lblEmailValidation;

    @FindBy(xpath = "//small[contains(@class,'text-danger') and contains(text(),'Address should not')]")
    private WebElement lblAddressValidation;

    @FindBy(xpath = "//small[contains(@class,'text-danger') and contains(text(),'Company Name should be')]")
    private WebElement lblBusinessNameValidation;

    @FindBy(xpath = "//small[contains(@class,'text-danger') and contains(text(),'DBA should be')]")
    private WebElement lblDBANameValidation;

    @FindBy(xpath = "//small[contains(@class,'text-danger') and contains(text(),'GST Number should be')]")
    private WebElement lblGSTNameValidation;

    @FindBy(xpath = "//small[contains(@class,'text-danger') and contains(text(),'Village should not')]")
    private WebElement lblVillageValidation;

    @FindBy(xpath = "//small[contains(@class,'text-danger') and contains(text(),'Tehsil should not')]")
    private WebElement lblTehsilValidation;

    @FindBy(xpath = "//small[contains(@class,'text-danger') and contains(text(),'District should not')]")
    private WebElement lblDistrictValidation;

    @FindBy(xpath = "//small[contains(@class,'text-danger') and contains(text(),'Country')]")
    private WebElement lblCountryValidation;

    @FindBy(xpath = "//small[contains(@class,'text-danger') and contains(text(),'Phone Number')]")
    private WebElement lblMobileValidation;

    @FindBy(xpath = "//small[contains(@class,'text-danger') and contains(text(),'Pin Code')]")
    private WebElement lblPinCodeValidation;

    @FindBy(xpath = "//span[contains(@class,'text-danger') and contains(text(),'OTP Mismatch')]")
    private WebElement lblOTPMisMatchValidation;

    @FindBy(xpath = "//span[contains(@class,'text-danger') and contains(text(),'must be between')]")
    private WebElement lblOTPInvalidValidation;

    @FindBy(xpath = ".//div[contains(@id,'swal2-content') and contains(text(),'Please Upload files')]")
    private WebElement lblInvalidFileUploadValidation;

    @FindBy(xpath = ".//div[contains(@id,'swal2-content') and contains(text(),'The otp must be')]")
    private WebElement lblInvalidOTPValidation;

    @FindBy(xpath = ".//div[contains(@class,'swal2-actions')]//button[text()='OK']")
    private WebElement btnOTPConfirmOK;

    @FindBy(xpath = ".//span[@id='profile_pic_select']")
    private WebElement lblProfilePicSelected;

    @FindBy(xpath = ".//span[@id='aadhaar_front_select']")
    private WebElement lblAdhaarFrontSelected;

    @FindBy(xpath = ".//span[@id='aadhaar_back_select']")
    private WebElement lblAdhaarBackSelected;

    @FindBy(xpath = ".//span[@id='buisness_pan_select']")
    private WebElement lblBusinessPANSelected;

    @FindBy(xpath = ".//span[@id='buisness_addr_proof_select']")
    private WebElement lblBusinessAddressProofSelected;

    @FindBy(xpath = "//div[contains(@class,'trnfr-tpup-txt')]")
    private WebElement lblUserCreateSuccessMessage;

    @FindBy(xpath = "//div[contains(@class,'tpup-mnyscs-txt')]")
    private WebElement lblUserDetailsMessage;

    @FindBy(xpath = "//div[contains(@class,'modal-content')]//button[contains(@class,'btn-blue') and text()='Done']")
    private WebElement btnDone;

    @FindAll(value = {@FindBy(xpath = "//table[@id='consumersTbl']//tr//td[2]")})
    private List<WebElement> lstAddedConsumerNumber;

    @FindAll(value = {@FindBy(xpath = "//table[@id='merchantsTbl']//tr//td[2]")})
    private List<WebElement> lstAddedMerchantNumber;

    @FindAll(value = {@FindBy(xpath = "//input[@type='search']")})
    private List<WebElement> txtInputSearch;

    @FindBy(xpath = "//div//p[contains(text(),'No consumers')]")
    private WebElement lblNoConsumersFound;

    @FindBy(xpath = "//div//p[contains(text(),'No merchants')]")
    private WebElement lblNoMerchantFound;

    @FindBy(xpath = "//li/button[contains(@class,'filterBtn')]")
    private WebElement btnFilter;

    @FindBy(xpath = "//ul//li/a[@id='merchantList']")
    private WebElement tabMerchantList;

    @FindBy(xpath = "//table[@id='consumersTbl']")
    private WebElement consumersTable;

    @FindBy(xpath = "//table[@id='merchantsTbl']")
    private WebElement merchantsTable;

    @FindAll(value = {@FindBy(xpath = "//table[@id='consumersTbl']//tr//td[1]//h5")})
    private List<WebElement> lstConsumerName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='consumersTbl']//tr//td[2]")})
    private List<WebElement> lstConsumerNumber;

    @FindAll(value = {@FindBy(xpath = "//table[@id='consumersTbl']//tr//td[3]")})
    private List<WebElement> lstConsumerStatus;

    @FindAll(value = {@FindBy(xpath = "//table[@id='consumersTbl']//tr//td[4]")})
    private List<WebElement> lstConsumerDateTime;

    @FindAll(value = {@FindBy(xpath = "//table[@id='merchantsTbl']//tr//td[1]//h5")})
    private List<WebElement> lstMerchantName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='merchantsTbl']//tr//td[2]")})
    private List<WebElement> lstMerchantNumber;

    @FindAll(value = {@FindBy(xpath = "//table[@id='merchantsTbl']//tr//td[3]")})
    private List<WebElement> lstMerchantStatus;

    @FindAll(value = {@FindBy(xpath = "//table[@id='merchantsTbl']//tr//td[4]")})
    private List<WebElement> lstMerchantDateTime;

    @FindBy(xpath = "//div[contains(@class,'spr-usr-name')]")
    private WebElement lblUserNameInProfile;

    @FindBy(xpath = "//div[contains(@class,'spr-usr-id')]")
    private WebElement lblUserIDInProfile;

    @FindBy(xpath = "//div[contains(@class,'spr-usr-sts')]")
    private WebElement lblUserJoinedInProfile;

    @FindBy(xpath = "//div//p[contains(text(),'No Transactions')]")
    private WebElement lblNoTransactioHistory;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table2']//tr//td[3]")})
    private List<WebElement> lstCommissionAmount;

    @FindBy(xpath = "//table//tr//td[contains(text(),'matching')]")
    private WebElement lblNoRecordsFound;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//button[contains(@class,'searchbtn')]")
    private WebElement btnFilterSearch;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//a[contains(text(),'Clear All')]")
    private WebElement btnFilterClearAll;

    @FindBy(xpath = "//div//h2[contains(text(),'Wakeup')]")
    private WebElement lblNoFilterConsumerAvailable;


    public boolean verifyConsumerDetailsScreenDisplay() {
        pause(3);
        return getText(lblConsumerNameInProfile).equalsIgnoreCase(AgentConsumerTargetDetailsIndexPage._consumerName);
    }

    public boolean verifyMerchantDetailsScreenDisplay() {
        pause(3);
        return getText(lblMerchantNameInProfile).equalsIgnoreCase(AgentMerchantTargetDetailsIndexPage._merchantName) &&
                getText(lblMerchantIDInProfile).equalsIgnoreCase(AgentMerchantTargetDetailsIndexPage._merchantId);
    }

    public boolean verifyCreateConsumerScreen() {

        pause(2);

        boolean bool = isElementDisplay(txtFirstName) && isElementDisplay(txtLastName) &&
                isElementDisplay(txtDOBPicker) && isElementDisplay(txtEmailAddress) &&
                isElementDisplay(txtPhoneNumber);

        scrollElement(btnNextStep1);

        return bool && isElementDisplay(lstFileUploads.get(0)) && isElementDisplay(selectFileType)
                && isElementDisplay(btnNextStep1);

    }

    public boolean verifyCreateMerchantScreen() {

        pause(2);

        boolean bool = isElementDisplay(txtFirstName) && isElementDisplay(txtLastName) &&
                isElementDisplay(txtDOBPicker) && isElementDisplay(txtEmailAddress) &&
                isElementDisplay(txtPhoneNumber) && isElementDisplay(txtUsername);

        scrollElement(btnNextStep1);

        return bool && isElementDisplay(lstFileUploads.get(0)) && isElementDisplay(selectFileType)
                && isElementDisplay(btnNextStep1);

    }

    public boolean verifyStep2OTPScreenDisplay() {
        pause(3);
        return isElementDisplay(txtStep2OTP) && isElementDisplay(btnNextStep2) &&
                isElementDisplay(btnPreviousStep2);

    }

    public boolean verifyConsumerStep3ScreenDisplay() {

        pause(2);

        boolean bool = isElementDisplay(txtHomeFlatAddress) && isElementDisplay(txtStreet1Address) &&
                isElementDisplay(txtStreet2Address) && isElementDisplay(txtVillage) &&
                isElementDisplay(txtTehsil) && isElementDisplay(txtDistrict) &&
                isElementDisplay(btnState) && isElementDisplay(btnCity) && isElementDisplay(txtCountry);

        System.out.println(bool);

        scrollToElement(driver, btnConsumerCancel);

        return bool && isElementDisplay(btnConsumerCancel) && isElementDisplay(txtPinCode) && isElementDisplay(btnCreateConsumer);

    }

    public boolean verifyMerchantStep3ScreenDisplay() {

        pause(2);

        boolean bool = isElementDisplay(txtBusinessName) && isElementDisplay(btnBusinessTypeDropdown) &&
                isElementDisplay(txtDBAName) && isElementDisplay(txtBusinessPhone) &&
                isElementDisplay(txtGSTNumber) && isElementDisplay(txtBusinessURL);
        System.out.println(bool);

        scrollToElement(driver, btnNextStep3);

        return bool && isElementDisplay(btnNextStep3) && isElementDisplay(lstFileUploads.get(4)) &&
                isElementDisplay(lstFileUploads.get(3));

    }

    public boolean verifyMerchantStep4ScreenDisplay() {

        pause(2);

        boolean bool = isElementDisplay(txtHomeFlatAddress) && isElementDisplay(txtStreet1Address) &&
                isElementDisplay(txtStreet2Address) && isElementDisplay(txtVillage) &&
                isElementDisplay(txtTehsil) && isElementDisplay(txtDistrict) &&
                isElementDisplay(btnState) && isElementDisplay(btnCity) && isElementDisplay(txtCountry);

        System.out.println(bool);

        scrollToElement(driver, btnMerchantCancel);

        return bool && isElementDisplay(btnMerchantCancel) && isElementDisplay(txtPinCode) && isElementDisplay(btnCreateMerchant);

    }

    public boolean verifyCancelCreateConsumer() {

        pause(5);

        int totalAddedConsumers = getIntegerFromString(getInnerText(lblAddedConsumers));
        int todayAddedConsumers = getIntegerFromString(getInnerText(lblAddedConsumerSection));

        return AgentDashboardIndexPage._totalAddedConsumerBeforeAddingNewConsumer == totalAddedConsumers &&
                AgentDashboardIndexPage._todayAddedConsumerBeforeAddingNewConsumer == todayAddedConsumers;
    }

    public boolean verifyCancelCreateMerchant() {

        pause(5);

        int totalAddedMerchants = getIntegerFromString(getInnerText(lblAddedMerchants));
        int todayAddedMerchants = getIntegerFromString(getInnerText(lblAddedMerchantSection));

        return AgentDashboardIndexPage._totalAddedMerchantBeforeAddingNewMerchant == totalAddedMerchants &&
                AgentDashboardIndexPage._todayAddedMerchantBeforeAddingNewMerchant == todayAddedMerchants;
    }

    public boolean verifyMinCharName(String field) {

        if (field.equalsIgnoreCase("Last Name")) {
            testValidationLog(getText(lblLastNameValidation));
            return getText(lblLastNameValidation).contains(LAST_NAME_MORE_THAN_1_CHAR);
        } else {
            testValidationLog(getText(lblFirstNameValidation));
            return getText(lblFirstNameValidation).contains(FIRST_NAME_MORE_THAN_1_CHAR);
        }

    }

    public boolean verifyMaxCharName(String field) {

        if (field.equalsIgnoreCase("Last Name")) {
            testValidationLog(getText(lblLastNameValidation));
            return getText(lblLastNameValidation).contains(LAST_NAME_LESS_THAN_20_CHAR);
        } else {
            testValidationLog(getText(lblFirstNameValidation));
            return getText(lblFirstNameValidation).contains(FIRST_NAME_LESS_THAN_20_CHAR);
        }

    }

    public boolean verifyNumericName(String field) {

        if (field.equalsIgnoreCase("Last Name")) {
            testValidationLog(getText(lblLastNameValidation));
            return getText(lblLastNameValidation).contains(LAST_NAME_NUMERIC);
        } else {
            testValidationLog(getText(lblFirstNameValidation));
            return getText(lblFirstNameValidation).contains(FIRST_NAME_NUMERIC);
        }

    }

    public boolean verifyNoValidationName(String field) {
        return field.equalsIgnoreCase("Last Name") ?
                !isElementPresent(lblLastNameValidation) :
                !isElementPresent(lblFirstNameValidation);
    }

    public boolean verifyDOBSelected() {
        return txtDOBPicker.getAttribute("ng-reflect-model").
                equalsIgnoreCase(AgentUserListIndexPage._newUserDOB);

    }

    public boolean verifyInvalidEmailValidation() {
        testValidationLog(getText(lblEmailValidation));
        return getText(lblEmailValidation).contains(EMAIL_ADDRESS_INVALID);
    }

    public boolean verifyNoValidationEmail() {
        return !isElementPresent(lblEmailValidation);
    }

    public boolean verifyNoValidationAddress() {
        return !AgentUserListIndexPage._isRandomBoolean || !isElementPresent(lblAddressValidation);
    }

    public boolean verifyInvalidMobileNumberValidation() {
        testValidationLog(getText(lblMobileValidation));
        return getText(lblMobileValidation).contains(MOBILE_NUMBER_VALIDATION);
    }

    public boolean verifyNoValidationMobile() {
        return !isElementPresent(lblMobileValidation);
    }

    public boolean verifyInvalidFileUpload() {

        boolean bool = getText(lblInvalidFileUploadValidation).equalsIgnoreCase(INVALID_FILE_FORMAT_UPLOAD);

        testValidationLog(getText(lblInvalidFileUploadValidation));
        clickOn(driver, btnOTPConfirmOK);

        return bool;

    }

    public boolean verifyProfilePicSelected() {
        return getText(lblProfilePicSelected).equalsIgnoreCase("Selected");
    }

    public boolean verifyAdhaarFrontSelected() {
        scrollElement(btnNextStep1);
        return getText(lblAdhaarFrontSelected).equalsIgnoreCase("Selected");
    }

    public boolean verifyAdhaarBackSelected() {
        scrollElement(btnNextStep1);
        return getText(lblAdhaarBackSelected).equalsIgnoreCase("Selected");
    }

    public boolean verifyOTPMisMatchValidation() {
        testValidationLog(getText(lblOTPMisMatchValidation));
        return getText(lblOTPMisMatchValidation).equalsIgnoreCase(OTP_MISMATCH);
    }

    public boolean verifyInvalidOTPValidation() {
        boolean bool = getText(lblInvalidOTPValidation).equalsIgnoreCase(INVALID_OTP_MESSAGE);

        testValidationLog(getText(lblInvalidOTPValidation));
        clickOn(driver, btnOTPConfirmOK);

        return bool;
    }

    public boolean verifyInvalidAddress() {
        testValidationLog(getText(lblAddressValidation));
        return getText(lblAddressValidation).contains(ADDRESS_MORE_THAN_50_CHAR);
    }

    public boolean verifyInvalidBusinessName() {
        testValidationLog(getText(lblBusinessNameValidation));
        return getText(lblBusinessNameValidation).contains(BUSINESS_NAME_LESS_THAN_4_CHAR) ||
                getText(lblBusinessNameValidation).contains(BUSINESS_NAME_MORE_THAN_50_CHAR);
    }

    public boolean verifyNoValidationBusinessName() {
        return !isElementPresent(lblBusinessNameValidation);
    }

    public boolean verifyInvalidDBAName() {
        testValidationLog(getText(lblDBANameValidation));
        return getText(lblDBANameValidation).contains(DBA_NAME_LESS_THAN_4_CHAR) ||
                getText(lblDBANameValidation).contains(DBA_NAME_MORE_THAN_50_CHAR);
    }

    public boolean verifyNoValidationDBAName() {
        return !isElementPresent(lblDBANameValidation);
    }

    public boolean verifyInvalidVillageAddress() {
        testValidationLog(getText(lblVillageValidation));
        return getText(lblVillageValidation).contains(VILLAGE_MORE_THAN_50_CHAR);
    }

    public boolean verifyInvalidTehsilAddress() {
        testValidationLog(getText(lblTehsilValidation));
        return getText(lblTehsilValidation).contains(TEHSIL_MORE_THAN_50_CHAR);
    }

    public boolean verifyInvalidDistrictAddress() {
        testValidationLog(getText(lblDistrictValidation));
        return getText(lblDistrictValidation).contains(DISTRICT_MORE_THAN_50_CHAR);
    }

    public boolean verifyNumericCountryValidation() {
        testValidationLog(getText(lblCountryValidation));
        return getText(lblCountryValidation).contains(COUNTRY_NUMERIC_VALUES);
    }

    public boolean verifyCountryMinCharValidation() {
        testValidationLog(getText(lblCountryValidation));
        return getText(lblCountryValidation).contains(COUNTRY_LESS_THAN_1_CHAR);
    }

    public boolean verifyCountryMaxCharValidation() {
        testValidationLog(getText(lblCountryValidation));
        return getText(lblCountryValidation).contains(COUNTRY_MORE_THAN_30_CHAR);
    }

    public boolean verifyNoValidationCountry() {
        return !isElementPresent(lblCountryValidation);
    }

    public boolean verifyInvalidPinCodeValidation() {
        testValidationLog(getText(lblPinCodeValidation));
        return getText(lblPinCodeValidation).equalsIgnoreCase(PIN_CODE_VALIDATION);
    }

    public boolean verifyNoValidationPinCode() {
        return !isElementPresent(lblPinCodeValidation);
    }

    public boolean verifyUserCreatedSuccessfully() {

        pause(5);

        testConfirmationLog(getText(lblUserCreateSuccessMessage) + "<br>" + getText(lblUserDetailsMessage));

        boolean bool = getText(lblUserDetailsMessage).equalsIgnoreCase(USER_CONFIRMATION_DETAILS) &&
                getText(lblUserCreateSuccessMessage).equalsIgnoreCase(USER_CREATED_SUCCESSFULLY) &&
                isElementDisplay(btnDone);

        clickOn(driver, btnDone);

        return bool;

    }

    public boolean verifyCreatedConsumerCount() {

        pause(5);

        int totalAddedConsumers = getIntegerFromString(getInnerText(lblAddedConsumers));
        int todayAddedConsumers = getIntegerFromString(getInnerText(lblAddedConsumerSection));

        return AgentDashboardIndexPage._totalAddedConsumerBeforeAddingNewConsumer == (totalAddedConsumers - 1) &&
                AgentDashboardIndexPage._todayAddedConsumerBeforeAddingNewConsumer == (todayAddedConsumers - 1);
    }

    public boolean verifyCreatedMerchantCount() {

        pause(5);

        int totalAddedMerchants = getIntegerFromString(getInnerText(lblAddedMerchants));
        int todayAddedMerchants = getIntegerFromString(getInnerText(lblAddedMerchantSection));

        return AgentDashboardIndexPage._totalAddedMerchantBeforeAddingNewMerchant == (totalAddedMerchants - 1) &&
                AgentDashboardIndexPage._todayAddedMerchantBeforeAddingNewMerchant == (todayAddedMerchants - 1);
    }

    public boolean verifyCreatedConsumerSearchSuccessful() {

        boolean bool = getInnerText(lstAddedConsumerNumber.get(0)).contains(AgentUserListIndexPage._newUserMobileNumber) &&
                sizeOf(lstAddedConsumerNumber) == 1;

        type(txtInputSearch.get(0), "");

        return bool;

    }

    public boolean verifyUsersListScreen() {

        boolean bool;

        if (isElementPresent(lblNoConsumersFound)) {
            testValidationLog(getText(lblNoConsumersFound));
            bool = isElementDisplay(lblNoConsumersFound) && isElementDisplay(btnFilter) &&
                    getText(lblNoConsumersFound).equalsIgnoreCase(UsersListValidation.NO_CONSUMER_FOUND);
        } else {
            bool = isElementDisplay(btnFilter) && isElementDisplay(consumersTable);
        }

        clickOn(driver, tabMerchantList);

        if (isElementPresent(lblNoMerchantFound)) {
            testValidationLog(getText(lblNoMerchantFound));
            bool = bool && isElementDisplay(lblNoMerchantFound) && isElementDisplay(btnFilter) &&
                    getText(lblNoMerchantFound).equalsIgnoreCase(UsersListValidation.NO_MERCHANT_FOUND);
        } else {
            bool = bool && isElementDisplay(btnFilter) && isElementDisplay(merchantsTable);
        }

        return bool;
    }

    public boolean verifyUserCountSimilarToDashboard() {

        boolean bool = sizeOf(lstConsumerName) == AgentDashboardIndexPage._totalConsumers;

        testStepsLog(_logStep++, "Click On Merchant Tab.");
        clickOn(driver, tabMerchantList);

        pause(3);

        return bool && sizeOf(lstMerchantName) == AgentDashboardIndexPage._totalMerchants;

    }

    public boolean verifyUserPersonalInfo() {

        boolean bool = getInnerText(lblUserNameInProfile).equalsIgnoreCase(AgentUserListIndexPage._userName) &&
                getInnerText(lblUserJoinedInProfile).contains(AgentUserListIndexPage._userCreatedDate);

        System.out.println(getInnerText(lblUserNameInProfile) + " " + AgentUserListIndexPage._userName);
        System.out.println(getInnerText(lblUserJoinedInProfile) + " " + AgentUserListIndexPage._userCreatedDate);

        return bool;
    }

    public boolean verifyUserHistoryInfo() {

        pause(3);

        double totalCommissionByUser = 0.0;

        try {
            updateShowList(driver, findElementByName(driver, "trans-history-table2_length"), "All");
        } catch (Exception e) {
            scrollToElement(driver, lblNoTransactioHistory);
        }

        if (!isElementPresent(lblNoTransactioHistory)) {

            for (WebElement commission : lstCommissionAmount) {
                totalCommissionByUser += getDoubleFromString(getInnerText(commission));
            }

            testInfoLog("Total Topup of User", String.valueOf(formatTwoDecimal(totalCommissionByUser)));

            System.out.println(formatTwoDecimal(totalCommissionByUser));

            return !isElementPresent(lblNoTransactioHistory);

        } else {
            testValidationLog(getText(lblNoTransactioHistory));
            return getInnerText(lblNoTransactioHistory).equalsIgnoreCase(NO_TRANSACTION_FOUND);
        }
    }

    public boolean verifyConsumerNameSorted() {

        List<String> consumerNameAfterSort = new ArrayList<>();
        List<String> consumerNameBeforeSort = AgentUserListIndexPage._userNameBeforeSort;

        for (WebElement name : lstConsumerName) {
            consumerNameAfterSort.add(getInnerText(name));
        }

        consumerNameBeforeSort.sort(
                (String o1, String o2) -> {
                    if (o1.equalsIgnoreCase(o2)) {
                        return o2.compareTo(o1);
                    }
                    return o1.toLowerCase().compareTo(o2.toLowerCase());
                }
        );

        System.out.println(consumerNameAfterSort);
        System.out.println(consumerNameBeforeSort);

        System.out.println(consumerNameBeforeSort.equals(consumerNameAfterSort));

        return consumerNameBeforeSort.equals(consumerNameAfterSort);
    }

    public boolean verifyConsumerNumberSorted() {

        List<String> consumerNumberAfterSort = new ArrayList<>();
        List<String> consumerNumberBeforeSort = AgentUserListIndexPage._userNumberBeforeSort;

        for (WebElement name : lstConsumerNumber) {
            consumerNumberAfterSort.add(getInnerText(name));
        }

        consumerNumberBeforeSort.sort(
                (String o1, String o2) -> {
                    if (o1.equalsIgnoreCase(o2)) {
                        return o2.compareTo(o1);
                    }
                    return o1.toLowerCase().compareTo(o2.toLowerCase());
                }
        );

        System.out.println(consumerNumberAfterSort);
        System.out.println(consumerNumberBeforeSort);

        System.out.println(consumerNumberBeforeSort.equals(consumerNumberAfterSort));

        return consumerNumberBeforeSort.equals(consumerNumberAfterSort);
    }

    public boolean verifyConsumerStatusSorted() {

        List<String> agentStatusAfterSort = new ArrayList<>();
        List<String> agentStatusBeforeSort = AgentUserListIndexPage._userStatusBeforeSort;

        for (WebElement name : lstConsumerStatus) {
            agentStatusAfterSort.add(getInnerText(name));
        }

        agentStatusBeforeSort.sort(
                (String o1, String o2) -> {
                    if (o1.equalsIgnoreCase(o2)) {
                        return o2.compareTo(o1);
                    }
                    return o1.toLowerCase().compareTo(o2.toLowerCase());
                }
        );

        System.out.println(agentStatusAfterSort);
        System.out.println(agentStatusBeforeSort);

        System.out.println(agentStatusBeforeSort.equals(agentStatusAfterSort));

        return agentStatusBeforeSort.equals(agentStatusAfterSort);
    }


    public boolean verifyConsumerDateTimeSorted() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        List<LocalDateTime> agentDateTimeAfterSort = new ArrayList<>();
        List<LocalDateTime> agentDateTimeBeforeSort = AgentUserListIndexPage._userDateTimeBeforeSort;

        for (WebElement name : lstConsumerDateTime) {
            agentDateTimeAfterSort.add(LocalDateTime.parse(getInnerText(name), format));
        }

        Collections.sort(agentDateTimeBeforeSort);

        System.out.println(agentDateTimeAfterSort);
        System.out.println(agentDateTimeBeforeSort);

        System.out.println(agentDateTimeBeforeSort.equals(agentDateTimeAfterSort));

        return agentDateTimeBeforeSort.equals(agentDateTimeAfterSort);
    }

    public boolean verifyConsumerSearchSuccessful() {

        boolean bool = getInnerText(lstConsumerNumber.get(0)).contains(AgentUserListIndexPage._searchCriteria) &&
                sizeOf(lstConsumerNumber) == 1;

        type(txtInputSearch.get(0), "");

        return bool;

    }

    public boolean verifyConsumerValidationForInvalidSearch() {

        testValidationLog(getInnerText(lblNoRecordsFound));

        boolean bool = getInnerText(lblNoRecordsFound).equalsIgnoreCase(NO_SEARCH_RECORD_FOUND);

        type(txtInputSearch.get(0), "x");
        txtInputSearch.get(0).clear();

        return bool;
    }

    public boolean verifyMerchantNameSorted() {

        List<String> consumerNameAfterSort = new ArrayList<>();
        List<String> consumerNameBeforeSort = AgentUserListIndexPage._userNameBeforeSort;

        for (WebElement name : lstMerchantName) {
            consumerNameAfterSort.add(getInnerText(name));
        }

        consumerNameBeforeSort.sort(
                (String o1, String o2) -> {
                    if (o1.equalsIgnoreCase(o2)) {
                        return o2.compareTo(o1);
                    }
                    return o1.toLowerCase().compareTo(o2.toLowerCase());
                }
        );

        System.out.println(consumerNameAfterSort);
        System.out.println(consumerNameBeforeSort);

        System.out.println(consumerNameBeforeSort.equals(consumerNameAfterSort));

        return consumerNameBeforeSort.equals(consumerNameAfterSort);
    }

    public boolean verifyMerchantNumberSorted() {

        List<String> consumerNumberAfterSort = new ArrayList<>();
        List<String> consumerNumberBeforeSort = AgentUserListIndexPage._userNumberBeforeSort;

        for (WebElement name : lstMerchantNumber) {
            consumerNumberAfterSort.add(getInnerText(name));
        }

        consumerNumberBeforeSort.sort(
                (String o1, String o2) -> {
                    if (o1.equalsIgnoreCase(o2)) {
                        return o2.compareTo(o1);
                    }
                    return o1.toLowerCase().compareTo(o2.toLowerCase());
                }
        );

        System.out.println(consumerNumberAfterSort);
        System.out.println(consumerNumberBeforeSort);

        System.out.println(consumerNumberBeforeSort.equals(consumerNumberAfterSort));

        return consumerNumberBeforeSort.equals(consumerNumberAfterSort);
    }

    public boolean verifyMerchantStatusSorted() {

        List<String> agentStatusAfterSort = new ArrayList<>();
        List<String> agentStatusBeforeSort = AgentUserListIndexPage._userStatusBeforeSort;

        for (WebElement name : lstMerchantStatus) {
            agentStatusAfterSort.add(getInnerText(name));
        }

        agentStatusBeforeSort.sort(
                (String o1, String o2) -> {
                    if (o1.equalsIgnoreCase(o2)) {
                        return o2.compareTo(o1);
                    }
                    return o1.toLowerCase().compareTo(o2.toLowerCase());
                }
        );

        System.out.println(agentStatusAfterSort);
        System.out.println(agentStatusBeforeSort);

        System.out.println(agentStatusBeforeSort.equals(agentStatusAfterSort));

        return agentStatusBeforeSort.equals(agentStatusAfterSort);
    }


    public boolean verifyMerchantDateTimeSorted() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        List<LocalDateTime> agentDateTimeAfterSort = new ArrayList<>();
        List<LocalDateTime> agentDateTimeBeforeSort = AgentUserListIndexPage._userDateTimeBeforeSort;

        for (WebElement name : lstMerchantDateTime) {
            agentDateTimeAfterSort.add(LocalDateTime.parse(getInnerText(name), format));
        }

        Collections.sort(agentDateTimeBeforeSort);

        System.out.println(agentDateTimeAfterSort);
        System.out.println(agentDateTimeBeforeSort);

        System.out.println(agentDateTimeBeforeSort.equals(agentDateTimeAfterSort));

        return agentDateTimeBeforeSort.equals(agentDateTimeAfterSort);
    }

    public boolean verifyMerchantSearchSuccessful() {

        boolean bool = getInnerText(lstMerchantNumber.get(0)).contains(AgentUserListIndexPage._searchCriteria);

        type(txtInputSearch.get(1), "");

        return bool;

    }

    public boolean verifyMerchantValidationForInvalidSearch() {

        testValidationLog(getInnerText(lblNoRecordsFound));

        boolean bool = getInnerText(lblNoRecordsFound).equalsIgnoreCase(NO_SEARCH_RECORD_FOUND);

        type(txtInputSearch.get(1), "x");
        txtInputSearch.get(1).clear();

        return bool;
    }

    public boolean verifyConsumerFilterScreenDisplay() {

        updateShowList(driver, findElementByName(driver, "consumersTbl_length"), "All");

        return isElementDisplay(btnFilterSearch) && isElementDisplay(btnFilterClearAll);
    }

    public boolean verifyFirstNameFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement name : lstConsumerName) {
            finalFilterResult.add(getInnerText(name).split(" ")[0].trim());
        }

        System.out.println(finalFilterResult);
        System.out.println(AgentUserListIndexPage._filterFirstName);

        return finalFilterResult.contains(AgentUserListIndexPage._filterFirstName);
    }

    public boolean verifyShopNameFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement name : lstMerchantName) {
            finalFilterResult.add(getInnerText(name));
        }

        System.out.println(finalFilterResult);
        System.out.println(AgentUserListIndexPage._filterShopName);

        return finalFilterResult.contains(AgentUserListIndexPage._filterShopName);
    }

    public boolean verifyLastNameFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement name : lstConsumerName) {
            finalFilterResult.add(getInnerText(name).replace(AgentUserListIndexPage._filterFirstName, "").trim());
        }

        System.out.println(finalFilterResult);
        System.out.println(AgentUserListIndexPage._filterLastName);

        return finalFilterResult.contains(AgentUserListIndexPage._filterLastName);
    }

    public boolean verifyPhoneNumberFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement number : lstConsumerNumber) {
            finalFilterResult.add(getInnerText(number));
        }

        System.out.println(finalFilterResult);
        System.out.println(AgentUserListIndexPage._filterPhone);

        return finalFilterResult.contains(AgentUserListIndexPage._filterPhone);
    }

    public boolean verifyStatusFiltered(String userType) {

        List<String> finalFilterResult = new ArrayList<>();

        pause(2);

        if (userType.equalsIgnoreCase("Merchant")) {
            for (WebElement status : lstMerchantStatus) {
                finalFilterResult.add(getInnerText(status));
            }
        } else {
            for (WebElement status : lstConsumerStatus) {
                finalFilterResult.add(getInnerText(status));
            }
        }

        System.out.println(finalFilterResult);
        System.out.println(AgentUserListIndexPage._filterStatus);

        if (isListEmpty(finalFilterResult)) {
            testValidationLog(getText(lblNoFilterConsumerAvailable));
            return !AgentUserListIndexPage._userStatusBeforeSort.contains(AgentUserListIndexPage._filterStatus)
                    && getText(lblNoFilterConsumerAvailable).equalsIgnoreCase(NO_FILTER_DATA_AVAILABLE);
        } else {
            return finalFilterResult.stream().distinct().limit(2).count() <= 1 &&
                    finalFilterResult.contains(AgentUserListIndexPage._filterStatus);
        }
    }

    public boolean verifyRandomDataFiltered() {
        return getText(lblNoFilterConsumerAvailable).equalsIgnoreCase(NO_FILTER_DATA_AVAILABLE);
    }

    public boolean verifyMerchantNumberFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement number : lstMerchantNumber) {
            finalFilterResult.add(getInnerText(number));
        }

        System.out.println(finalFilterResult);
        System.out.println(AgentUserListIndexPage._filterPhone);

        return finalFilterResult.contains(AgentUserListIndexPage._filterPhone);
    }


    public boolean verifyInvalidUsername() {
        testValidationLog(getText(lblUserNameValidation));
        return getText(lblUserNameValidation).contains(USER_NAME_VALIDATION_MESSAGE);
    }

    public boolean verifyInvalidGSTNumber() {
        testValidationLog(getText(lblGSTNameValidation));
        return getText(lblGSTNameValidation).contains(GST_NUMBER_VALIDATION);
    }

    public boolean verifyNoValidationGST() {
        return !isElementPresent(lblGSTNameValidation);
    }

    public boolean verifyNoValidationUserName() {
        return !isElementPresent(lblUserNameValidation);
    }

    public boolean verifyCreatedMerchantSearchSuccessful() {

        boolean bool = getInnerText(lstAddedMerchantNumber.get(0)).contains(AgentUserListIndexPage._newUserMobileNumber) &&
                sizeOf(lstAddedMerchantNumber) == 1;

        type(txtInputSearch.get(0), "");

        return bool;

    }

    public boolean verifyBusinessPANSelected() {
        return getText(lblBusinessPANSelected).equalsIgnoreCase("Selected");
    }

    public boolean verifyBusinessAddressProofSelected() {
        return getText(lblBusinessAddressProofSelected).equalsIgnoreCase("Selected");
    }
}
