package com.incashme.agent.verification;

import com.framework.init.AbstractPage;
import com.incashme.agent.indexpage.AgentCommissionIndexPage;
import com.incashme.agent.indexpage.AgentDashboardIndexPage;
import com.incashme.agent.validations.CommissionValidations;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Rahul R.
 * Date: 2019-04-26
 * Time: 16:21
 * Project Name: InCashMe
 */
public class AgentCommissionVerification extends AbstractPage implements CommissionValidations {

    public AgentCommissionVerification(WebDriver driver) {
        super(driver);
    }

    private static double totalConsumerCommission = 0.00;
    private static double totalMerchantCommission = 0.00;

    @FindAll(value = {@FindBy(xpath = "//table[@id='consumer-trans-history-table']//tr//td[1]//h5/span")})
    private List<WebElement> lstConsumerName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='consumer-trans-history-table']//tr//td[1]//following-sibling::div")})
    private List<WebElement> lstConsumerNumber;

    @FindAll(value = {@FindBy(xpath = "//table[@id='consumer-trans-history-table']//tr//td[2]/span")})
    private List<WebElement> lstConsumerTxId;

    @FindAll(value = {@FindBy(xpath = "//table[@id='consumer-trans-history-table']//tr//td[3]")})
    private List<WebElement> lstConsumerTopUpAmount;

    @FindAll(value = {@FindBy(xpath = "//table[@id='consumer-trans-history-table']//tr//td[4]")})
    private List<WebElement> lstConsumerCommissionAmount;

    @FindAll(value = {@FindBy(xpath = "//table[@id='consumer-trans-history-table']//tr//td[5]")})
    private List<WebElement> lstConsumerTransactionTime;

    @FindAll(value = {@FindBy(xpath = "//table[@id='merchant-trans-history-table']//tr//td[1]//h5/span")})
    private List<WebElement> lstMerchantName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='merchant-trans-history-table']//tr//td[1]//following-sibling::div")})
    private List<WebElement> lstMerchantID;

    @FindAll(value = {@FindBy(xpath = "//table[@id='merchant-trans-history-table']//tr//td[2]/span")})
    private List<WebElement> lstMerchantTxId;

    @FindAll(value = {@FindBy(xpath = "//table[@id='merchant-trans-history-table']//tr//td[3]")})
    private List<WebElement> lstMerchantTopUpAmount;

    @FindAll(value = {@FindBy(xpath = "//table[@id='merchant-trans-history-table']//tr//td[4]")})
    private List<WebElement> lstMerchantCommissionAmount;

    @FindAll(value = {@FindBy(xpath = "//table[@id='merchant-trans-history-table']//tr//td[5]")})
    private List<WebElement> lstMerchantTransactionTime;

    @FindBy(xpath = "//div[contains(@class,'column-chart-div')]")
    private WebElement commissionChart;

    @FindBy(xpath = "//label[@id='p_week']")
    private WebElement btnChartWeek;

    @FindBy(xpath = "//label[@id='p_month']")
    private WebElement btnChartMonth;

    @FindBy(xpath = "//label[@id='p_year']")
    private WebElement btnChartYear;

    @FindBy(xpath = "//label[@id='p_total']")
    private WebElement btnChartTotal;

    @FindBy(xpath = "//table[@id='consumer-trans-history-table']")
    private WebElement commissionTable;

    @FindBy(xpath = "//div//p[contains(text(),'No TopUp')]")
    private WebElement lblNoCommission;

    @FindBy(xpath = "//div//p[contains(text(),'No consumers')]")
    private WebElement lblNoConsumersFound;

    @FindBy(xpath = "//div//p[contains(text(),'No merchants')]")
    private WebElement lblNoMerchantsFound;

    @FindBy(xpath = "//ul//li/a[@id='consumerList']")
    private WebElement tabConsumerList;

    @FindBy(xpath = "//ul//li/a[@id='merchantList']")
    private WebElement tabMerchantList;

    @FindBy(xpath = "//li/button[contains(@class,'filterBtn')]")
    private WebElement btnFilter;

    @FindBy(xpath = "//div//p[contains(text(),'no data alive')]")
    private WebElement lblNoConsumerCommission;

    @FindAll(value = {@FindBy(xpath = "//input[@type='search']")})
    private List<WebElement> txtInputSearch;

    @FindBy(xpath = "//table//tr//td[contains(text(),'matching')]")
    private WebElement lblNoRecordsFound;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//button[contains(@class,'searchbtn')]")
    private WebElement btnFilterSearch;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//a[contains(text(),'Clear All')]")
    private WebElement btnFilterClearAll;

    @FindBy(xpath = "//div//h2[contains(text(),'Wakeup')]")
    private WebElement lblNoFilterConsumerAvailable;

    @FindBy(xpath = ".//div[contains(@id,'swal2-content') and contains(text(),'Invalid')]")
    private WebElement lblInvalidAmountValidation;

    @FindBy(xpath = ".//div[contains(@class,'swal2-actions')]//button[text()='OK']")
    private WebElement btnOTPConfirmOK;


    public boolean verifyConsumerCommissionDisplay() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        List<String> lstExpectedTimes = new ArrayList<>();

        double commissionAmount = 8.75;

        for (int sec = 1; sec <= 20; sec++) {
            lstExpectedTimes.add((AgentDashboardIndexPage._topUpDateTime.plusSeconds(sec)).format(format).trim());
        }

        return getInnerText(lstConsumerName.get(0)).equalsIgnoreCase(AgentDashboardVerification.topUpUserName) &&
                getInnerText(lstConsumerTxId.get(0)).contains("Consumer") &&
                getInnerText(lstConsumerNumber.get(0)).contains(AgentDashboardIndexPage._topUpMobileNumber) &&
                getDoubleFromString(getInnerText(lstConsumerTopUpAmount.get(0))) == (double) (AgentDashboardIndexPage._topUpAmount) &&
                getDoubleFromString(getInnerText(lstConsumerCommissionAmount.get(0))) == commissionAmount &&
                lstExpectedTimes.contains(getInnerText(lstConsumerTransactionTime.get(0)));

    }

    public boolean verifyMerchantCommissionDisplay() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        List<String> lstExpectedTimes = new ArrayList<>();

        double commissionAmount = 8.75;

        for (int sec = 1; sec <= 20; sec++) {
            lstExpectedTimes.add((AgentDashboardIndexPage._topUpDateTime.plusSeconds(sec)).format(format).trim());
        }

        return getInnerText(lstMerchantName.get(0)).equalsIgnoreCase(AgentDashboardVerification.topUpUserName) &&
                getInnerText(lstMerchantTxId.get(0)).contains("Merchant") &&
                getInnerText(lstMerchantID.get(0)).contains(AgentDashboardIndexPage._topUpMobileNumber) &&
                getDoubleFromString(getInnerText(lstMerchantTopUpAmount.get(0))) == (double) (AgentDashboardIndexPage._topUpAmount) &&
                getDoubleFromString(getInnerText(lstMerchantCommissionAmount.get(0))) == commissionAmount &&
                lstExpectedTimes.contains(getInnerText(lstMerchantTransactionTime.get(0)));

    }

    public boolean verifyCommissionScreen() {

        boolean bool;

        if (isElementPresent(lblNoCommission)) {

            scrollToElement(driver, lblNoCommission);
            testValidationLog(getText(lblNoCommission));
            return isElementDisplay(lblNoCommission) && isElementDisplay(btnFilter) &&
                    getText(lblNoCommission).equalsIgnoreCase(NO_COMMISSION_FOUND);

        } else {

            bool = isElementDisplay(commissionChart) && isElementDisplay(btnFilter);

            scrollToElement(driver, btnChartWeek);

            bool = bool && isElementDisplay(btnChartWeek) && isElementDisplay(btnChartMonth) &&
                    isElementDisplay(btnChartYear) && isElementDisplay(btnChartTotal);

            scrollToElement(driver, tabConsumerList);

            return bool && isElementDisplay(tabConsumerList) && isElementDisplay(tabMerchantList);

        }
    }

    public boolean verifyConsumerCommissionCalculation() {

        boolean bool = true;

        if (isListEmpty(lstConsumerTopUpAmount)) {
            testValidationLog(getInnerText(lblNoConsumerCommission));
        } else {
            for (int commId = 0; commId < sizeOf(lstConsumerTopUpAmount); commId++) {
                double expectedCommission = 8.75;
                double actualCommission = getDoubleFromString(getInnerText(lstConsumerCommissionAmount.get(commId)));

                bool = bool && (expectedCommission == actualCommission);
                totalConsumerCommission += expectedCommission;
            }
        }

        return bool;
    }

    public boolean verifyMerchantCommissionCalculation() {

        boolean bool = true;

        if (isListEmpty(lstMerchantTopUpAmount)) {
            testValidationLog(getInnerText(lblNoConsumerCommission));
        } else {
            for (int commId = 0; commId < sizeOf(lstMerchantTopUpAmount); commId++) {
                double expectedCommission = 8.75;
                double actualCommission = getDoubleFromString(getInnerText(lstMerchantCommissionAmount.get(commId)));

                bool = bool && (expectedCommission == actualCommission);
                totalMerchantCommission += expectedCommission;
            }
        }

        return bool;
    }

    public boolean verifyTotalConsumerCommissionAmount() {

        pause(2);

        testInfoLog("Total Earned Commission so far",
                String.valueOf(formatTwoDecimal(totalConsumerCommission + totalMerchantCommission)));

        return AgentCommissionIndexPage._totalCommissionAmount == formatTwoDecimal(totalConsumerCommission + totalMerchantCommission);
    }

    public boolean verifySearchSuccessful(String user) {

        boolean bool = true;
        List<WebElement> lstUsersTxID;

        if (user.equalsIgnoreCase("merchant")) lstUsersTxID = lstMerchantTxId;
        else lstUsersTxID = lstConsumerTxId;

        for (WebElement e : lstUsersTxID) {
            bool = bool && getInnerText(e).contains(AgentCommissionIndexPage._searchCriteria);
        }

        if (user.equalsIgnoreCase("merchant")) type(txtInputSearch.get(1), "");
        else type(txtInputSearch.get(0), "");

        return bool;

    }

    public boolean verifyValidationForConsumerInvalidSearch() {

        testValidationLog(getInnerText(lblNoRecordsFound));

        boolean bool = getInnerText(lblNoRecordsFound).equalsIgnoreCase(NO_SEARCH_RECORD_FOUND);

        type(txtInputSearch.get(0), "x");
        txtInputSearch.get(0).clear();

        return bool;
    }

    public boolean verifyValidationForMerchantInvalidSearch() {

        testValidationLog(getInnerText(lblNoRecordsFound));

        boolean bool = getInnerText(lblNoRecordsFound).equalsIgnoreCase(NO_SEARCH_RECORD_FOUND);

        type(txtInputSearch.get(1), "x");
        txtInputSearch.get(1).clear();

        return bool;
    }

    public boolean verifyTopUpsSorted(String userType) {

        List<Double> commissionTopUpAfterSort = new ArrayList<>();
        List<Double> commissionTopUpBeforeSort = AgentCommissionIndexPage._commissionTopUpBeforeSort;

        if (userType.equalsIgnoreCase("consumer")) {
            for (WebElement name : lstConsumerTopUpAmount) {
                commissionTopUpAfterSort.add(getDoubleFromString(getInnerText(name)));
            }
        } else {
            for (WebElement name : lstMerchantTopUpAmount) {
                commissionTopUpAfterSort.add(getDoubleFromString(getInnerText(name)));
            }
        }

        Collections.sort(commissionTopUpBeforeSort);

        System.out.println(commissionTopUpAfterSort);
        System.out.println(commissionTopUpBeforeSort);

        System.out.println(commissionTopUpBeforeSort.equals(commissionTopUpAfterSort));

        return commissionTopUpBeforeSort.equals(commissionTopUpAfterSort);
    }

    public boolean verifyCommissionSorted(String userType) {

        List<Double> commissionAmountAfterSort = new ArrayList<>();
        List<Double> commissionAmountBeforeSort = AgentCommissionIndexPage._commissionAmountBeforeSort;

        if (userType.equalsIgnoreCase("consumer")) {
            for (WebElement name : lstConsumerCommissionAmount) {
                commissionAmountAfterSort.add(getDoubleFromString(getInnerText(name)));
            }
        } else {
            for (WebElement name : lstMerchantCommissionAmount) {
                commissionAmountAfterSort.add(getDoubleFromString(getInnerText(name)));
            }
        }

        Collections.sort(commissionAmountBeforeSort);

        System.out.println(commissionAmountAfterSort);
        System.out.println(commissionAmountBeforeSort);

        System.out.println(commissionAmountBeforeSort.equals(commissionAmountAfterSort));

        return commissionAmountBeforeSort.equals(commissionAmountAfterSort);
    }

    public boolean verifyCommissionDateTimeSorted(String userType) {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        List<LocalDateTime> commissionDateTimeAfterSort = new ArrayList<>();
        List<LocalDateTime> commissionDateTimeBeforeSort = AgentCommissionIndexPage._commissionDateTimeBeforeSort;

        if (userType.equalsIgnoreCase("consumer")) {
            for (WebElement name : lstConsumerTransactionTime) {
                commissionDateTimeAfterSort.add(LocalDateTime.parse(getInnerText(name), format));
            }
        } else {
            for (WebElement name : lstMerchantTransactionTime) {
                commissionDateTimeAfterSort.add(LocalDateTime.parse(getInnerText(name), format));
            }
        }

        Collections.sort(commissionDateTimeBeforeSort);

        System.out.println(commissionDateTimeAfterSort);
        System.out.println(commissionDateTimeBeforeSort);

        System.out.println(commissionDateTimeBeforeSort.equals(commissionDateTimeAfterSort));

        return commissionDateTimeBeforeSort.equals(commissionDateTimeAfterSort);
    }

    public boolean verifyConsumerFilterScreenDisplay() {

        try {
            updateShowList(driver, findElementByName(driver, "consumer-trans-history-table_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        return isElementDisplay(btnFilterSearch) && isElementDisplay(btnFilterClearAll);
    }

    public boolean verifyMerchantFilterScreenDisplay() {

        try {
            updateShowList(driver, findElementByName(driver, "merchant-trans-history-table_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        return isElementDisplay(btnFilterSearch) && isElementDisplay(btnFilterClearAll);
    }

    public boolean verifyRandomDataFiltered() {
        return getText(lblNoFilterConsumerAvailable).equalsIgnoreCase(NO_FILTER_DATA_AVAILABLE);
    }

    public boolean verifyFirstNameFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement name : lstConsumerName) {
            finalFilterResult.add(getInnerText(name).split(" ")[0].trim());
        }

        System.out.println(finalFilterResult);
        System.out.println(AgentCommissionIndexPage._filterFirstName);

        return finalFilterResult.contains(AgentCommissionIndexPage._filterFirstName);
    }

    public boolean verifyShopNameFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement name : lstMerchantName) {
            finalFilterResult.add(getInnerText(name));
        }

        System.out.println(finalFilterResult);
        System.out.println(AgentCommissionIndexPage._filterShopName);

        return finalFilterResult.contains(AgentCommissionIndexPage._filterShopName);
    }

    public boolean verifyLastNameFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement name : lstConsumerName) {
            finalFilterResult.add(getInnerText(name).replace(AgentCommissionIndexPage._filterFirstName, "").trim());
        }

        System.out.println(finalFilterResult);
        System.out.println(AgentCommissionIndexPage._filterLastName);

        return finalFilterResult.contains(AgentCommissionIndexPage._filterLastName);
    }

    public boolean verifyPhoneNumberFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement number : lstConsumerNumber) {
            finalFilterResult.add(getInnerText(number));
        }

        System.out.println(finalFilterResult);
        System.out.println(AgentCommissionIndexPage._filterPhone);

        return finalFilterResult.contains(AgentCommissionIndexPage._filterPhone);
    }

    public boolean verifyMaxMinTopupValidation() {

        boolean bool = getText(lblInvalidAmountValidation).equalsIgnoreCase(INVALID_TOPUP_AMOUNT_RANGE);

        testValidationLog(getText(lblInvalidAmountValidation));
        clickOn(driver, btnOTPConfirmOK);

        return bool;
    }

    public boolean verifyMaxMinCommissionValidation() {

        boolean bool = getText(lblInvalidAmountValidation).equalsIgnoreCase(INVALID_COMMISSION_AMOUNT_RANGE);

        testValidationLog(getText(lblInvalidAmountValidation));
        clickOn(driver, btnOTPConfirmOK);

        return bool;
    }

    public boolean verifyTopUpFromFieldFilter(String userType) {

        List<Double> finalFilterResult = new ArrayList<>();

        if (userType.equalsIgnoreCase("Merchant")) {
            for (WebElement name : lstMerchantTopUpAmount) {
                finalFilterResult.add(getDoubleFromString(getInnerText(name)));
            }
        } else {
            for (WebElement name : lstConsumerTopUpAmount) {
                finalFilterResult.add(getDoubleFromString(getInnerText(name)));
            }
        }

        System.out.println(finalFilterResult);
        System.out.println(AgentCommissionIndexPage._filterTopUpFromAmount);

        return !isListEmpty(finalFilterResult) &&
                findMin(finalFilterResult) >= AgentCommissionIndexPage._filterTopUpFromAmount;
    }

    public boolean verifyTopUpToFieldFilter(String userType) {

        List<Double> finalFilterResult = new ArrayList<>();

        if (userType.equalsIgnoreCase("Merchant")) {
            for (WebElement name : lstMerchantTopUpAmount) {
                finalFilterResult.add(getDoubleFromString(getInnerText(name)));
            }
        } else {
            for (WebElement name : lstConsumerTopUpAmount) {
                finalFilterResult.add(getDoubleFromString(getInnerText(name)));
            }
        }

        System.out.println(finalFilterResult);
        System.out.println(AgentCommissionIndexPage._filterTopUpToAmount);

        return !isListEmpty(finalFilterResult) &&
                findMax(finalFilterResult) <= AgentCommissionIndexPage._filterTopUpToAmount;
    }

    public boolean verifyCommissionFromFieldFilter(String userType) {

        List<Double> finalFilterResult = new ArrayList<>();

        if (userType.equalsIgnoreCase("Merchant")) {
            for (WebElement name : lstMerchantCommissionAmount) {
                finalFilterResult.add(getDoubleFromString(getInnerText(name)));
            }
        } else {
            for (WebElement name : lstConsumerCommissionAmount) {
                finalFilterResult.add(getDoubleFromString(getInnerText(name)));
            }
        }

        System.out.println(finalFilterResult);
        System.out.println(AgentCommissionIndexPage._filterCommissionFromAmount);

        return !isListEmpty(finalFilterResult) &&
                findMin(finalFilterResult) >= AgentCommissionIndexPage._filterCommissionFromAmount;
    }

    public boolean verifyCommissionToFieldFilter(String userType) {

        List<Double> finalFilterResult = new ArrayList<>();

        if (userType.equalsIgnoreCase("Merchant")) {
            for (WebElement name : lstMerchantCommissionAmount) {
                finalFilterResult.add(getDoubleFromString(getInnerText(name)));
            }
        } else {
            for (WebElement name : lstConsumerCommissionAmount) {
                finalFilterResult.add(getDoubleFromString(getInnerText(name)));
            }
        }

        System.out.println(finalFilterResult);
        System.out.println(AgentCommissionIndexPage._filterCommissionToAmount);

        return !isListEmpty(finalFilterResult) &&
                findMax(finalFilterResult) <= AgentCommissionIndexPage._filterCommissionToAmount;
    }

    public boolean verifyTopUpMaxMinFilter(String userType) {

        List<Double> finalFilterResult = new ArrayList<>();

        if (userType.equalsIgnoreCase("Merchant")) {
            for (WebElement name : lstMerchantTopUpAmount) {
                finalFilterResult.add(getDoubleFromString(getInnerText(name)));
            }
        } else {
            for (WebElement name : lstConsumerTopUpAmount) {
                finalFilterResult.add(getDoubleFromString(getInnerText(name)));
            }
        }

        System.out.println(finalFilterResult);
        System.out.println(AgentCommissionIndexPage._filterTopUpFromAmount);
        System.out.println(AgentCommissionIndexPage._filterTopUpToAmount);

        return findMin(finalFilterResult) >= AgentCommissionIndexPage._filterTopUpFromAmount &&
                findMax(finalFilterResult) <= AgentCommissionIndexPage._filterTopUpToAmount;

    }

    public boolean verifyCommissionMaxMinFilter(String userType) {

        List<Double> finalFilterResult = new ArrayList<>();

        if (userType.equalsIgnoreCase("Merchant")) {
            for (WebElement name : lstMerchantCommissionAmount) {
                finalFilterResult.add(getDoubleFromString(getInnerText(name)));
            }
        } else {
            for (WebElement name : lstConsumerCommissionAmount) {
                finalFilterResult.add(getDoubleFromString(getInnerText(name)));
            }
        }

        System.out.println(finalFilterResult);
        System.out.println(AgentCommissionIndexPage._filterCommissionFromAmount);
        System.out.println(AgentCommissionIndexPage._filterCommissionToAmount);

        return findMin(finalFilterResult) >= AgentCommissionIndexPage._filterCommissionFromAmount &&
                findMax(finalFilterResult) <= AgentCommissionIndexPage._filterCommissionToAmount;

    }

    public boolean verifyMerchantIDFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement number : lstMerchantID) {
            finalFilterResult.add(getInnerText(number));
        }

        System.out.println(finalFilterResult);
        System.out.println(AgentCommissionIndexPage._filterID);

        return finalFilterResult.contains(AgentCommissionIndexPage._filterID);
    }

}
