package com.incashme.agent.verification;

import com.framework.init.AbstractPage;
import com.incashme.agent.indexpage.AgentDashboardIndexPage;
import com.incashme.agent.indexpage.AgentLoginIndexPage;
import com.incashme.agent.validations.DashboardValidations;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Rahul R.
 * Date: 2019-04-11
 * Time: 11:36
 * Project Name: InCashMe
 */
public class AgentDashboardVerification extends AbstractPage implements DashboardValidations {

    public AgentDashboardVerification(WebDriver driver) {
        super(driver);
    }

    public static String topUpUserName;

    @FindBy(xpath = "//input[@id='payeeName']")
    private WebElement txtMobileNumber;

    @FindBy(xpath = "//input[@id='amount']")
    private WebElement txtTopUpAmount;

    @FindBy(xpath = "//div[contains(@class,'add-user')]//span")
    private WebElement btnAddTopupUser;

    @FindBy(xpath = "//div[contains(@class,'trfr-tpup-card')]//button")
    private WebElement btnSubmitTopup;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@class,'blnc-val')]")})
    private List<WebElement> lstLblBalance;

    @FindBy(xpath = "//div[contains(@class,'avtvty-ttl') and contains(text(),'Top-Up')]")
    private WebElement btnTopUpUser;

    @FindBy(xpath = "//div[contains(@class,'avtvty-ttl') and contains(text(),'Added Merchants')]")
    private WebElement btnAddedMerchants;

    @FindBy(xpath = "//div[contains(@class,'avtvty-ttl') and contains(text(),'Added Consumers')]")
    private WebElement btnAddedConsumers;

    @FindBy(xpath = "//div//a[contains(text(),'Create Consumer')]")
    private WebElement btnCreateConsumer;

    @FindBy(xpath = "//div//a[contains(text(),'Create Merchant')]")
    private WebElement btnCreateMerchant;

    @FindAll(value = {@FindBy(xpath = "//div[@class='stmnt-cntnt']")})
    private List<WebElement> lstStatements;

    @FindAll(value = {@FindBy(xpath = "//div[@class='pst-dt']")})
    private List<WebElement> lstStatementMonth;

    @FindBy(xpath = "//div//p[contains(text(),'No Statement')]")
    private WebElement lblNoStatement;

    @FindBy(xpath = "//div[@class='blnc-txt']")
    private WebElement lblBalance;

    @FindBy(xpath = "//div//p[contains(text(),'No TopUp')]")
    private WebElement lblNoCommission;

    @FindBy(xpath = "//div//p[contains(text(),'No consumers')]")
    private WebElement lblNoTodayConsumersFound;

    @FindBy(xpath = "//div//p[contains(text(),'No merchants')]")
    private WebElement lblNoTodayMerchantFound;

    @FindBy(xpath = "//div[contains(@class,'column-chart-div')]")
    private WebElement commissionChart;

    @FindBy(xpath = "//label[@id='p_week']")
    private WebElement btnChartWeek;

    @FindBy(xpath = "//label[@id='p_month']")
    private WebElement btnChartMonth;

    @FindBy(xpath = "//label[@id='p_year']")
    private WebElement btnChartYear;

    @FindBy(xpath = "//label[@id='p_total']")
    private WebElement btnChartTotal;

    @FindBy(xpath = "//table[@id='consumer-trans-history-table']")
    private WebElement commissionTable;

    @FindBy(xpath = "//ul//li/a[@id='consumerList']")
    private WebElement tabConsumerList;

    @FindBy(xpath = "//ul//li/a[@id='merchantList']")
    private WebElement tabMerchantList;

    @FindBy(xpath = "//table[@id='consumersTbl']")
    private WebElement consumersTable;

    @FindBy(xpath = "//table[@id='merchantsTbl']")
    private WebElement merchantsTable;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@class,'profile-details')]//div[contains(@class,'avatar-user')]/following-sibling::div")})
    private List<WebElement> lstUserAccountDetails;

    @FindAll(value = {@FindBy(xpath = "//ul[contains(@class,'small-info')]//li")})
    private List<WebElement> lstUserPersonalDetails;

    @FindAll(value = {@FindBy(xpath = "//ul[contains(@class,'address-info')]//li")})
    private List<WebElement> lstUserAddressDetails;

    @FindBy(xpath = "//div[contains(@class,'profile-details')]//h2")
    private WebElement lblUserName;

    @FindBy(xpath = ".//div[contains(@class,'user-info')]//span[contains(@class,'name')]")
    private WebElement lblDashboardUserName;

    @FindBy(xpath = "//input[@formcontrolname='oldpassword']")
    private WebElement txtCurrentPassword;

    @FindBy(xpath = "//input[@formcontrolname='password2']")
    private WebElement txtNewPassword;

    @FindBy(xpath = "//input[@formcontrolname='password']")
    private WebElement txtConfirmPassword;

    @FindBy(xpath = "//input[@formcontrolname='oldpin']")
    private WebElement txtCurrentPIN;

    @FindBy(xpath = "//input[@formcontrolname='testData']")
    private WebElement txtNewPIN;

    @FindBy(xpath = "//input[@formcontrolname='pin2']")
    private WebElement txtConfirmPIN;

    @FindAll(value = {@FindBy(xpath = ".//div[contains(@class,'login-form')]//button[@type='submit']")})
    private List<WebElement> btnSubmit;

    @FindBy(xpath = "//table//tr[1]/td[6]")
    private WebElement lblLastLoginTime;

    @FindBy(xpath = "//table//tr[1]/td[5]")
    private WebElement lblUserAgent;

    @FindBy(xpath = "//table//tr[1]/td[4]")
    private WebElement lblIpAddress;

    @FindBy(xpath = "//table//tr[1]/td[3]")
    private WebElement lblAppVersion;

    @FindBy(xpath = "//table//tr[1]/td[2]")
    private WebElement lblSources;

    @FindBy(xpath = "//table//tr[1]/td[1]")
    private WebElement lblLoginId;

    @FindBy(xpath = "//table[@id='trans-history-table']")
    private WebElement loginHistoryTable;

    @FindBy(xpath = "//div//p[contains(text(),'no data alive')]")
    private WebElement lblNoTopUpCommission;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[1]//h5/span")})
    private List<WebElement> lstCommissionAgentNames;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[2]//span[contains(text(),'Consumer')]//..//preceding-sibling::td//h5//span")})
    private List<WebElement> lstCommissionConsumerAgentNames;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[2]//span[contains(text(),'Consumer')]//..//preceding-sibling::td//h5//following-sibling::div")})
    private List<WebElement> lstCommissionConsumerAgentIDNumber;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[1]//following-sibling::div")})
    private List<WebElement> lstCommissionAgentIdNumber;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[2]/span")})
    private List<WebElement> lstCommissionTxID;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[3]/span")})
    private List<WebElement> lstCommissionUserType;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[4]")})
    private List<WebElement> lstCommissionTopUp;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[2]//span[contains(text(),'Consumer')]//..//preceding-sibling::td//following-sibling::td[3]")})
    private List<WebElement> lstConsumerCommissionTopUp;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[5]")})
    private List<WebElement> lstCommissionAmount;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[6]")})
    private List<WebElement> lstCommissionTxTime;

    @FindBy(xpath = "//table[@id='trans-history-table']")
    private WebElement dashbaordCommissionTable;

    @FindBy(xpath = "//div[contains(@class,'show')]//p[contains(text(),'No Topups found!')]")
    private WebElement lblNoTodayTopUpFound;

    @FindBy(xpath = "//*[contains(text(),'TopUps List')]/preceding-sibling::button")
    private WebElement btnCloseTopUpList;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dashboardTopupsTble']//h5")})
    private List<WebElement> lstTodayTopUpsUserName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='merchantssTodayTbl']//h5")})
    private List<WebElement> lstTodayMerchantUserName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='consumersTodayTbl']//h5")})
    private List<WebElement> lstTodayConsumerUserName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='consumersTodayTbl']//h5/following-sibling::div[1]")})
    private List<WebElement> lstTodayConsumerNumber;

    @FindAll(value = {@FindBy(xpath = "//table[@id='MerchantsTodayTbl']//h5/following-sibling::div[1]")})
    private List<WebElement> lstTodayMerchantID;

    @FindAll(value = {@FindBy(xpath = "//table[@id='consumersTbl']//h5")})
    private List<WebElement> lstTodayAddedConsumer;

    @FindAll(value = {@FindBy(xpath = "//table[@id='consumersTbl']//tr//td[1]//h5/following-sibling::div[1]")})
    private List<WebElement> lstAddUsersConsumerNumber;

    @FindAll(value = {@FindBy(xpath = "//table[@id='usersTbl2']//tr//td[1]//h5/following-sibling::div[1]")})
    private List<WebElement> lstListConsumerNumber;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dashboardTopupsTble']//div[contains(@class,'trans-sub-title')]")})
    private List<WebElement> lstTodayTopUpsUserIDNumber;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dashboardTopupsTble']//div[contains(@class,'tpup-price')]")})
    private List<WebElement> lstTodayTopUpsAmount;

    @FindBy(xpath = ".//div[contains(@id,'swal2-content') and contains(text(),'Unsuccessful attempt')]")
    private WebElement lblInvalidCurrentPasswordValidation;

    @FindBy(xpath = ".//div[contains(@id,'swal2-content') and contains(text(),'Kyc Verification')]")
    private WebElement lblNonKYCTopUpConsumerValidation;

    @FindBy(xpath = ".//div[contains(@id,'swal2-content') and contains(text(),'200.00')]")
    private WebElement lblMinimumTopUpAmountValidation;

    @FindBy(xpath = ".//div[contains(@id,'swal2-content') and contains(text(),'Insufficient Funds')]")
    private WebElement lblInsufficientBalanceForTopUpValidation;

    @FindBy(xpath = ".//div[contains(@id,'swal2-content') and contains(text(),'Unsuccessful attempt')]")
    private WebElement lblInvalidCurrentPINValidation;

    @FindBy(xpath = ".//div[contains(@id,'swal2-content') and contains(text(),'old Password')]")
    private WebElement lblPastPasswordValidation;

    @FindBy(xpath = ".//div[contains(@class,'swal2-actions')]//button[text()='OK']")
    private WebElement btnConfirmOK;

    @FindBy(xpath = ".//div[contains(@class,'login-form')]//*[@class='text-danger']")
    private WebElement lblInvalidNewPasswordValidation;

    @FindBy(xpath = ".//div[contains(@class,'login-form')]//*[@class='text-danger']")
    private WebElement lblInvalidConfirmPasswordValidation;

    @FindBy(xpath = ".//div[contains(@id,'swal2-content') and contains(text(),'changed successfully')]")
    private WebElement lblConfirmPasswordChange;

    @FindAll(value = {@FindBy(xpath = ".//div[contains(@class,'login-form')]//*[@class='text-danger']")})
    private List<WebElement> lblInvalidNewPINValidation;

    @FindBy(xpath = ".//div[contains(@class,'login-form')]//*[@class='text-danger']")
    private WebElement lblInvalidConfirmPinValidation;

    @FindBy(xpath = ".//div[contains(@id,'swal2-content') and contains(text(),'old PIN')]")
    private WebElement lblPastPinValidation;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//td[2]//h5")})
    private List<WebElement> lstConsumerNumber;

    @FindBy(xpath = "//div[contains(@class,'merchant-count')]//span[contains(text(),'Consumer')]")
    private WebElement lblTotalConsumerCount;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[1]//h5")})
    private List<WebElement> lstConsumerName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[1]//h5")})
    private List<WebElement> lstMerchantName;

    @FindBy(xpath = "//div//p[contains(text(),'couldn’t find any matches')]")
    private WebElement lblNoAddedConsumerDetails;

    @FindBy(xpath = "//div//p[contains(text(),'couldn’t find any matches')]")
    private WebElement lblNoAddedMerchantDetails;

    @FindBy(xpath = "//*[contains(text(),'Consumers List')]//following-sibling::button")
    private WebElement btnCloseConsumerList;

    @FindBy(xpath = "//*[contains(text(),'Merchants List')]//following-sibling::button")
    private WebElement btnCloseMerchantList;

    @FindBy(xpath = "//div[contains(@class,'spr-usr-name')]")
    private WebElement lblMerchantNameInProfile;

    @FindBy(xpath = "//div[contains(@class,'spr-usr-id')]")
    private WebElement lblMerchantIDInProfile;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@class,'add-user-list')]//h5")})
    private List<WebElement> lstAddedConsumerNames;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@class,'add-user-list')]//h5//following-sibling::div")})
    private List<WebElement> lstAddedConsumerNumbers;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@class,'add-user-list')]//h5")})
    private List<WebElement> lstAddedMerchantNames;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@class,'add-user-list')]//h5//following-sibling::div")})
    private List<WebElement> lstAddedMerchantID;

    @FindBy(xpath = "//input[@type='search']")
    private WebElement txtInputSearch;

    @FindBy(xpath = "//table//tr//td[contains(text(),'matching')]")
    private WebElement lblNoRecordsFound;

    @FindBy(xpath = "//li/button[contains(@class,'filterBtn')]")
    private WebElement btnFilter;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[1]//h5")})
    private List<WebElement> lstAddedConsumerName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[2]//span")})
    private List<WebElement> lstAddedConsumerNumber;

    @FindBy(xpath = "//*[contains(text(),'Lookup')]/preceding-sibling::button")
    private WebElement btnCloseLookUpResult;

    @FindBy(xpath = "//input[contains(@id,'searchTopFromList')]")
    private WebElement txtPopupInputSearch;

    @FindBy(xpath = "//div[contains(@id,'usersTbl2_filter')]//input[@type='search']")
    private WebElement txtPopupInputSearch2;

    @FindAll(value = {@FindBy(xpath = "//table[@id='merchantsTbl']//tr//td[1]//h5/following-sibling::div[1]")})
    private List<WebElement> lstAddUsersMerchantID;

    @FindBy(xpath = "//table[@id='trans-history-table']")
    private WebElement transactionHistoryTable;

    @FindBy(xpath = "//div//h5[contains(text(),'Lookup')]")
    private WebElement lblLookingForTopUpResult;

    @FindBy(xpath = "//table//td[contains(text(),'No User')]")
    private WebElement lblNoTopUpUserFound;

    @FindBy(xpath = "//div[contains(@class,'modal-content')]//div[contains(@class,'amnt-txt')]")
    private WebElement lblTopUpAmountInPopUp;

    @FindBy(xpath = "//div[contains(@class,'modal-content')]//div[contains(@class,'tppup-usr-nm')]")
    private WebElement lblTopUpUserNameInPopUp;

    @FindBy(xpath = "//div[contains(@class,'modal-content')]//div[contains(@class,'tppup-usr-no')]")
    private WebElement lblTopUpUserNumberInPopUp;

    @FindBy(xpath = "//div[contains(@class,'modal-content')]//div[contains(@class,'tpup-mnyscs-txt')]")
    private WebElement lblTopUpSuccessful;

    @FindBy(xpath = "//div[contains(@class,'modal-content')]//button[contains(@class,'btn-green') and text()='Close']")
    private WebElement btnCloseTopUpPopUp;

    @FindBy(xpath = "//div[contains(text(),'Top-Up')]//following-sibling::div//span[contains(@class,'chart-velue')]")
    private WebElement lblComletedTopups;

    @FindBy(xpath = "//div[contains(text(),'Top-Up User')]//following-sibling::div")
    private WebElement lblTopUpUserList;


    public boolean verifyDashboardDetails() {

        pause(5);

        testInfoLog("Available Balance", getText(lstLblBalance.get(0)));
        testInfoLog("Today Commission", getText(lstLblBalance.get(1)));

        return isElementDisplay(txtMobileNumber) && isElementDisplay(txtTopUpAmount) &&
                isElementDisplay(btnAddTopupUser) && isElementDisplay(btnSubmitTopup) &&
                isElementDisplay(lstLblBalance.get(0)) && isElementDisplay(lstLblBalance.get(1));

    }

    public boolean verifyDashboardButtons() {

        scrollToElement(driver, btnAddedConsumers);

        return isElementDisplay(btnTopUpUser) && isElementDisplay(btnAddedMerchants) &&
                isElementDisplay(btnAddedConsumers) && isElementDisplay(btnCreateConsumer) &&
                isElementDisplay(btnCreateMerchant);

    }

    public boolean verifyBalanceScreen() {
        testInfoLog("Current Account Balance", getText(lblBalance));
        return isElementDisplay(lblBalance) && getText(lblBalance).equalsIgnoreCase(AgentDashboardIndexPage._currentBalance);
    }

    public boolean verifyUserProfileDetails() {

        pause(3);

        testInfoLog("User Account Information", "");
        for (WebElement element : lstUserAccountDetails) {
            System.out.println(getText(element));
            testInfoLog(getInnerText(element).split(" - ")[0], getInnerText(element).split(" - ")[1]);
        }

        testInfoLog("User Personal Information", "");
        testInfoLog("DOB", getText(lstUserPersonalDetails.get(0)));
        testInfoLog("Email", getText(lstUserPersonalDetails.get(1)));
        testInfoLog("Mobile Number", getText(lstUserPersonalDetails.get(2)));

        testInfoLog("User Address Information", "");
        for (WebElement element : lstUserAddressDetails) {
            if (getText(element).split("\n").length == 1) {
                testInfoLog(getText(element).split("\n")[0], "");
            } else {
                testInfoLog(getText(element).split("\n")[0], getText(element).split("\n")[1]);
            }
        }

        return getText(lblUserName).equalsIgnoreCase(getText(lblDashboardUserName)) &&
                getText(lstUserPersonalDetails.get(1)).equalsIgnoreCase(username);
    }

    public boolean verifyChangePasswordScreen() {

        pause(1);

        return isElementDisplay(txtCurrentPassword) && isElementDisplay(txtNewPassword) &&
                isElementDisplay(txtConfirmPassword) && isElementDisplay(btnSubmit.get(0));

    }

    public boolean verifyChangePINScreen() {

        pause(1);

        return isElementDisplay(txtCurrentPIN) && isElementDisplay(txtNewPIN) &&
                isElementDisplay(txtConfirmPIN) && isElementDisplay(btnSubmit.get(1));

    }

    public boolean verifyPrivacyPolicyScreen() {

        switchToWindow(driver);

        String expectedTitle = getTitle(driver);
        String actualTitle = "InCashMe™";

        close(driver);

        switchToWindow(driver);

        return expectedTitle.equalsIgnoreCase(actualTitle);

    }

    public boolean verifyTnCScreen() {

        switchToWindow(driver);

        String expectedTitle = getTitle(driver);
        String actualTitle = "InCashMe™";

        close(driver);

        switchToWindow(driver);

        return expectedTitle.equalsIgnoreCase(actualTitle);

    }

    public boolean verifyKYCandAMLScreen() {

        switchToWindow(driver);

        String expectedTitle = getTitle(driver);
        String actualTitle = "InCashMe™";

        close(driver);

        switchToWindow(driver);

        return expectedTitle.equalsIgnoreCase(actualTitle);

    }

    public boolean verifyLoginHistory() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        pause(5);

        String actualTime = getText(lblLastLoginTime).trim();

        testVerifyLog("Login ID : " + getText(lblLoginId) +
                "<br>Source : " + getText(lblSources) +
                "<br>App version : " + getText(lblAppVersion) +
                "<br>IP : " + getText(lblIpAddress) +
                "<br>User Agent : " + getText(lblUserAgent) +
                "<br>Login Time : " + getText(lblLastLoginTime));

        List<String> lstExpectedTimes = new ArrayList<>();

        for (int sec = -10; sec <= 20; sec++) {
            lstExpectedTimes.add((LocalDateTime.parse(AgentLoginIndexPage._loginTime, format)
                    .plusSeconds(sec)).format(format).trim());
        }

        System.out.println(lstExpectedTimes);
        System.out.println(actualTime);

        return isElementDisplay(loginHistoryTable) && lstExpectedTimes.contains(actualTime);

    }

    public boolean verifyCommissionDetailsOnDashboard() {

        if (AgentDashboardIndexPage._todayCommission == 0.00) {

            scrollToElement(driver, lblNoTopUpCommission);

            testValidationLog(getText(lblNoTopUpCommission));

            return isElementDisplay(lblNoTopUpCommission) &&
                    getText(lblNoTopUpCommission).equalsIgnoreCase(NO_TOPUP_COMMISSION_TODAY_MESSAGE);
        } else {

            scrollToElement(driver, dashbaordCommissionTable);

            testInfoLog("Total Topup Transaction display", String.valueOf(sizeOf(lstCommissionAgentNames)));

            for (int comm = 0; comm < sizeOf(lstCommissionAgentNames); comm++) {
                testInfoLog("________________________________________________________________________", "");
                testInfoLog("Agent Name & ID", getInnerText(lstCommissionAgentNames.get(comm)) +
                        "  " + getText(lstCommissionAgentIdNumber.get(comm)));
                testInfoLog("Transaction ID", getInnerText(lstCommissionTxID.get(comm)));
                testInfoLog("Agent Type", getInnerText(lstCommissionUserType.get(comm)));
                testInfoLog("Top up Amount", getInnerText(lstCommissionTopUp.get(comm)));
                testInfoLog("Commission", getInnerText(lstCommissionAmount.get(comm)));
                testInfoLog("Date & Time", getInnerText(lstCommissionTxTime.get(comm)));
                testInfoLog("________________________________________________________________________", "");
            }

            return isElementDisplay(dashbaordCommissionTable);

        }
    }


    public boolean verifyTotalCommissionAmount() {

        double expectedTotalAmount = 0.00;

        pause(2);

        if (isListEmpty(lstCommissionAmount)) {
            testValidationLog(getText(lblNoTopUpCommission));
        } else {
            scrollElement(btnTopUpUser);
            updateShowList(driver, findElementByName(driver, "trans-history-table_length"), "All");

            for (WebElement commission : lstCommissionAmount) {
                expectedTotalAmount += getDoubleFromString(getInnerText(commission));
            }

            System.out.println(AgentDashboardIndexPage._todayCommission);
            System.out.println(formatTwoDecimal(expectedTotalAmount));
        }

        return formatTwoDecimal(expectedTotalAmount) == AgentDashboardIndexPage._todayCommission;
    }

    public boolean verifyCommissionCalculation() {

        boolean bool = false;

        if (isListEmpty(lstCommissionAmount)) {
            testValidationLog(getText(lblNoTopUpCommission));
        } else {
            for (int commId = 0; commId < sizeOf(lstCommissionTopUp); commId++) {
                double expectedCommission = 8.75;
                double actualCommission = getDoubleFromString(getInnerText(lstCommissionAmount.get(commId)));

                System.out.println(expectedCommission + "   " + actualCommission);

                bool = (expectedCommission == actualCommission);
            }
        }

        return bool;
    }

    public boolean verifyTodayCompletedTopupsDetails() {

        if (AgentDashboardIndexPage._todayCompletedTopUp == 0) {
            pause(2);
            String validation = getInnerText(lblNoTodayTopUpFound);
            testValidationLog(validation);
            return sizeOf(lstTodayTopUpsUserName) == AgentDashboardIndexPage._todayCompletedTopUp &&
                    validation.equalsIgnoreCase(NO_TODAY_COMPLETED_TOPUP_MESSAGE);
        } else {

            updateShowList(driver, findElementByName(driver, "dashboardTopupsTble_length"), "All");

            testInfoLog("Total Completed Topups", String.valueOf(sizeOf(lstTodayTopUpsUserName)));
            testInfoLog("________________________________________________________________________", "");
            for (int agentNum = 0; agentNum < sizeOf(lstTodayTopUpsUserName); agentNum++) {

                testInfoLog("Agent Name :" + getInnerText(lstTodayTopUpsUserName.get(agentNum))
                        + "<br>Agent ID : " + getInnerText(lstTodayTopUpsUserIDNumber.get(agentNum))
                        + "<br>Topup Amount : " + getInnerText(lstTodayTopUpsAmount.get(agentNum)), "");
                testInfoLog("________________________________________________________________________", "");
            }
            return sizeOf(lstTodayTopUpsUserName) == AgentDashboardIndexPage._todayCompletedTopUp;
        }
    }

    public boolean verifyTopUpDetailsSameAsDashboard() {

        if (AgentDashboardIndexPage._todayCompletedTopUp == sizeOf(lstCommissionAgentNames) &&
                AgentDashboardIndexPage._todayCompletedTopUp == 0) {
            pause(2);
            String validation = getInnerText(lblNoTodayTopUpFound);
            testValidationLog(validation);
            return sizeOf(lstTodayTopUpsUserName) == AgentDashboardIndexPage._todayCompletedTopUp &&
                    validation.equalsIgnoreCase(NO_TODAY_COMPLETED_TOPUP_MESSAGE);
        } else {

            boolean bool = true;

            updateShowList(driver, findElementByName(driver, "dashboardTopupsTble_length"), "All");

            for (int agentNum = 0; agentNum < sizeOf(lstTodayTopUpsUserName); agentNum++) {

                System.out.println(getInnerText(lstCommissionConsumerAgentNames.get(agentNum)));
                System.out.println(getInnerText(lstTodayTopUpsUserName.get(agentNum)));
                System.out.println(getInnerText(lstCommissionConsumerAgentIDNumber.get(agentNum)));
                System.out.println(getInnerText(lstTodayTopUpsUserIDNumber.get(agentNum)));
                System.out.println(getInnerText(lstConsumerCommissionTopUp.get(agentNum)).replace("₹ ", ""));
                System.out.println(getInnerText(lstTodayTopUpsAmount.get(agentNum)).replace("₹", ""));


                bool = getInnerText(lstCommissionConsumerAgentNames.get(agentNum)).
                        equalsIgnoreCase(getInnerText(lstTodayTopUpsUserName.get(agentNum))) &&
                        getInnerText(lstTodayTopUpsUserIDNumber.get(agentNum)).
                                contains(getInnerText(lstCommissionConsumerAgentIDNumber.get(agentNum))) &&
                        getInnerText(lstConsumerCommissionTopUp.get(agentNum)).replace("₹ ", "").
                                equalsIgnoreCase(getInnerText(lstTodayTopUpsAmount.get(agentNum))
                                        .replace("₹", ""));
            }
            return bool;
        }

    }


    public boolean verifyTodayAddedConsumerDetails() {

        if (AgentDashboardIndexPage._todayAddedConsumers == 0) {
            pause(2);
            String validation = getInnerText(lblNoTodayConsumersFound);
            testValidationLog(validation);
            return sizeOf(lstTodayConsumerUserName) == AgentDashboardIndexPage._todayAddedConsumers &&
                    validation.equalsIgnoreCase(NO_TODAY_ADDED_CONSUMERS);
        } else {

            updateShowList(driver, findElementByName(driver, "consumersTodayTbl_length"), "All");

            testInfoLog("Total Added Consumers Today", String.valueOf(sizeOf(lstTodayConsumerUserName)));
            testInfoLog("________________________________________________________________________", "");
            for (int agentNum = 0; agentNum < sizeOf(lstTodayConsumerUserName); agentNum++) {

                testInfoLog("Consumer Name :" + getInnerText(lstTodayConsumerUserName.get(agentNum))
                        + "<br>Consumer Number: " + getInnerText(lstTodayConsumerNumber.get(agentNum)), "");
                testInfoLog("________________________________________________________________________", "");
            }
            return sizeOf(lstTodayConsumerUserName) == AgentDashboardIndexPage._todayAddedConsumers;
        }

    }

    public boolean verifyTodayAddedMerchantDetails() {

        if (AgentDashboardIndexPage._todayAddedMerchants == 0) {
            pause(2);
            String validation = getInnerText(lblNoTodayMerchantFound);
            testValidationLog(validation);
            return sizeOf(lstTodayMerchantUserName) == AgentDashboardIndexPage._todayAddedMerchants &&
                    validation.equalsIgnoreCase(NO_TODAY_ADDED_MERCHANTS);
        } else {

            updateShowList(driver, findElementByName(driver, "merchantsTodayTbl_length"), "All");

            testInfoLog("Total Added Consumers Today", String.valueOf(sizeOf(lstTodayMerchantUserName)));
            testInfoLog("________________________________________________________________________", "");
            for (int agentNum = 0; agentNum < sizeOf(lstTodayMerchantUserName); agentNum++) {

                testInfoLog("Merchant Name :" + getInnerText(lstTodayMerchantUserName.get(agentNum))
                        + "<br>Merchant ID : " + getInnerText(lstTodayMerchantID.get(agentNum)), "");
                testInfoLog("________________________________________________________________________", "");
            }
            return sizeOf(lstTodayMerchantUserName) == AgentDashboardIndexPage._todayAddedMerchants;
        }

    }

    public boolean verifyInvalidCurrentPasswordValidation() {

        testValidationLog(getText(lblInvalidCurrentPasswordValidation));

        boolean bool = isElementDisplay(lblInvalidCurrentPasswordValidation) &&
                getText(lblInvalidCurrentPasswordValidation).equalsIgnoreCase(FIRST_UNSUCCESSFUL_ATTEMPTS) &&
                isElementDisplay(btnConfirmOK);

        clickOn(driver, btnConfirmOK);

        return bool;
    }

    public boolean verifyInvalidCurrentPasswordSecondTime() {

        testValidationLog(getText(lblInvalidCurrentPasswordValidation));

        boolean bool = isElementDisplay(lblInvalidCurrentPasswordValidation) &&
                getText(lblInvalidCurrentPasswordValidation).equalsIgnoreCase(SECOND_UNSUCCESSFUL_ATTEMPTS) &&
                isElementDisplay(btnConfirmOK);

        clickOn(driver, btnConfirmOK);

        return bool;
    }

    public boolean verifyAccountIsBlockedForInvalidPassword() {

        testValidationLog(getText(lblInvalidCurrentPasswordValidation));

        boolean bool = isElementDisplay(lblInvalidCurrentPasswordValidation) &&
                getText(lblInvalidCurrentPasswordValidation).equalsIgnoreCase(THIRD_UNSUCCESSFUL_ATTEMPTS) &&
                isElementDisplay(btnConfirmOK);

        clickOn(driver, btnConfirmOK);

        return bool;
    }

    public boolean verifyInvalidNewPasswordValidation() {

        testValidationLog(getText(lblInvalidNewPasswordValidation));

        return isElementDisplay(lblInvalidNewPasswordValidation) &&
                getText(lblInvalidNewPasswordValidation).equalsIgnoreCase(INVALID_NEW_PASSWORD);
    }

    public boolean verifyInvalidConfirmPasswordValidation() {

        testValidationLog(getText(lblInvalidConfirmPasswordValidation));

        return isElementDisplay(lblInvalidConfirmPasswordValidation) &&
                getText(lblInvalidConfirmPasswordValidation).equalsIgnoreCase(NEW_CONFIRM_PASSWORD_NOT_MATCHED);
    }

    public boolean verifyPastPasswordValidation() {

        testValidationLog(getText(lblPastPasswordValidation));

        boolean bool = isElementDisplay(lblPastPasswordValidation) &&
                getText(lblPastPasswordValidation).equalsIgnoreCase(PAST_PASSWORD_MESSAGE) &&
                isElementDisplay(btnConfirmOK);

        clickOn(driver, btnConfirmOK);

        return bool;

    }

    public boolean verifyPasswordChangeSuccessfully() {

        testConfirmationLog(getText(lblConfirmPasswordChange));

        boolean bool = isElementDisplay(lblConfirmPasswordChange) &&
                getText(lblConfirmPasswordChange).equalsIgnoreCase(SUCCESSFUL_PASSWORD_CHANGE_MESSAGE) &&
                isElementDisplay(btnConfirmOK);

        clickOn(driver, btnConfirmOK);

        return bool;

    }

    public boolean verifyInvalidCurrentPINValidation() {

        testValidationLog(getText(lblInvalidCurrentPINValidation));

        boolean bool = isElementDisplay(lblInvalidCurrentPINValidation) &&
                getText(lblInvalidCurrentPINValidation).equalsIgnoreCase(FIRST_UNSUCCESSFUL_ATTEMPTS) &&
                isElementDisplay(btnConfirmOK);

        clickOn(driver, btnConfirmOK);

        return bool;
    }

    public boolean verifyInvalidNewPINValidation() {

        testValidationLog(getText(lblInvalidNewPINValidation.get(0)));
        testValidationLog(getText(lblInvalidNewPINValidation.get(1)));

        return isElementDisplay(lblInvalidNewPINValidation.get(0)) &&
                getText(lblInvalidNewPINValidation.get(0)).equalsIgnoreCase(INVALID_NEW_PIN) &&
                isElementDisplay(lblInvalidNewPINValidation.get(1)) &&
                getText(lblInvalidNewPINValidation.get(1)).equalsIgnoreCase(INVALID_CONFIRM_PIN);
    }

    public boolean verifyInvalidConfirmPinValidation() {

        testValidationLog(getText(lblInvalidConfirmPinValidation));

        return isElementDisplay(lblInvalidConfirmPinValidation) &&
                getText(lblInvalidConfirmPinValidation).equalsIgnoreCase(NEW_CONFIRM_PIN_NOT_MATCHED);
    }

    public boolean verifyPastPinValidation() {

        testValidationLog(getText(lblPastPinValidation));

        boolean bool = isElementDisplay(lblPastPinValidation) &&
                getText(lblPastPinValidation).equalsIgnoreCase(PAST_PIN_MESSAGE) &&
                isElementDisplay(btnConfirmOK);

        clickOn(driver, btnConfirmOK);

        return bool;

    }

    public boolean verifyInvalidCurrentPINSecondTime() {

        testValidationLog(getText(lblInvalidCurrentPINValidation));

        boolean bool = isElementDisplay(lblInvalidCurrentPINValidation) &&
                getText(lblInvalidCurrentPINValidation).equalsIgnoreCase(SECOND_UNSUCCESSFUL_ATTEMPTS) &&
                isElementDisplay(btnConfirmOK);

        clickOn(driver, btnConfirmOK);

        return bool;
    }

    public boolean verifyAccountIsBlockedForInvalidPIN() {

        testValidationLog(getText(lblInvalidCurrentPINValidation));

        boolean bool = isElementDisplay(lblInvalidCurrentPINValidation) &&
                getText(lblInvalidCurrentPINValidation).equalsIgnoreCase(THIRD_UNSUCCESSFUL_ATTEMPTS) &&
                isElementDisplay(btnConfirmOK);

        clickOn(driver, btnConfirmOK);

        return bool;
    }


    public boolean verifyTopUpsSorted() {

        List<Double> commissionTopUpAfterSort = new ArrayList<>();
        List<Double> commissionTopUpBeforeSort = AgentDashboardIndexPage._commissionTopUpBeforeSort;

        for (WebElement name : lstCommissionTopUp) {
            commissionTopUpAfterSort.add(getDoubleFromString(getInnerText(name)));
        }

        Collections.sort(commissionTopUpBeforeSort);

        System.out.println(commissionTopUpAfterSort);
        System.out.println(commissionTopUpBeforeSort);

        System.out.println(commissionTopUpBeforeSort.equals(commissionTopUpAfterSort));

        return commissionTopUpBeforeSort.equals(commissionTopUpAfterSort);
    }

    public boolean verifyCommissionSorted() {

        List<Double> commissionAmountAfterSort = new ArrayList<>();
        List<Double> commissionAmountBeforeSort = AgentDashboardIndexPage._commissionAmountBeforeSort;

        for (WebElement name : lstCommissionAmount) {
            commissionAmountAfterSort.add(getDoubleFromString(getInnerText(name)));
        }

        Collections.sort(commissionAmountBeforeSort);

        System.out.println(commissionAmountAfterSort);
        System.out.println(commissionAmountBeforeSort);

        System.out.println(commissionAmountBeforeSort.equals(commissionAmountAfterSort));

        return commissionAmountBeforeSort.equals(commissionAmountAfterSort);
    }

    public boolean verifyCommissionDateTimeSorted() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        List<LocalDateTime> commissionDateTimeAfterSort = new ArrayList<>();
        List<LocalDateTime> commissionDateTimeBeforeSort = AgentDashboardIndexPage._commissionDateTimeBeforeSort;

        for (WebElement name : lstCommissionTxTime) {
            commissionDateTimeAfterSort.add(LocalDateTime.parse(getInnerText(name), format));
        }

        Collections.sort(commissionDateTimeBeforeSort);

        System.out.println(commissionDateTimeAfterSort);
        System.out.println(commissionDateTimeBeforeSort);

        System.out.println(commissionDateTimeBeforeSort.equals(commissionDateTimeAfterSort));

        return commissionDateTimeBeforeSort.equals(commissionDateTimeAfterSort);
    }

    public boolean verifySearchSuccessful() {

        boolean bool = getInnerText(lstCommissionTxTime.get(0)).contains(AgentDashboardIndexPage._searchCriteria);

        type(txtInputSearch, "");

        return bool;

    }

    public boolean verifyValidationForInvalidSearch() {

        testValidationLog(getInnerText(lblNoRecordsFound));

        boolean bool = getInnerText(lblNoRecordsFound).equalsIgnoreCase(NO_SEARCH_RECORD_FOUND);

        type(txtInputSearch, "x");
        txtInputSearch.clear();

        return bool;
    }


    public boolean verifyConsumerListSearchSuccessful() {

        boolean bool = getInnerText(lstAddUsersConsumerNumber.get(0)).contains(AgentDashboardIndexPage._searchCriteria) &&
                sizeOf(lstAddUsersConsumerNumber) == 1;

        type(txtPopupInputSearch, "");

        return bool;

    }

    public boolean verifyMerchantListSearchSuccessful() {

        boolean bool = getInnerText(lstAddUsersMerchantID.get(0)).contains(AgentDashboardIndexPage._searchCriteria) &&
                sizeOf(lstAddUsersMerchantID) == 1;

        type(txtPopupInputSearch, "");

        return bool;

    }

    public boolean verifyValidationForAddUserInvalidSearch() {

        testValidationLog(getInnerText(lblNoRecordsFound));

        boolean bool = getInnerText(lblNoRecordsFound).equalsIgnoreCase(NO_SEARCH_RECORD_FOUND);

        type(txtPopupInputSearch, "x");
        txtPopupInputSearch.clear();

        return bool;
    }


    public boolean verifyNonKYCConsumerTopUpValidation() {

        testValidationLog(getText(lblNonKYCTopUpConsumerValidation));

        boolean bool = isElementDisplay(lblNonKYCTopUpConsumerValidation) &&
                getText(lblNonKYCTopUpConsumerValidation).equalsIgnoreCase(KYC_PENDING_FOR_TOPUP) &&
                isElementDisplay(btnConfirmOK);

        clickOn(driver, btnConfirmOK);

        return bool;

    }

    public boolean verifyNoUserFoundForTopUp() {

        testValidationLog(getText(lblLookingForTopUpResult));
        testValidationLog(getText(lblNoTopUpUserFound));

        boolean bool = isElementDisplay(lblLookingForTopUpResult) && isElementDisplay(lblNoTopUpUserFound) &&
                getText(lblLookingForTopUpResult).contains(AgentDashboardIndexPage._topUpMobileNumber) &&
                getText(lblNoTopUpUserFound).equalsIgnoreCase(NO_USER_FOUND);

        clickOn(driver, btnCloseLookUpResult);

        return bool;
    }

    public boolean verifyZeroTopUpAmountValidation() {
        System.out.println(btnSubmitTopup.getAttribute("disabled"));
        return btnSubmitTopup.getAttribute("disabled").equalsIgnoreCase("true");
    }

    public boolean verifyInvalidTopUpAmountValidation() {

        testValidationLog(getText(lblMinimumTopUpAmountValidation));

        boolean bool = isElementDisplay(lblMinimumTopUpAmountValidation) &&
                getText(lblMinimumTopUpAmountValidation).equalsIgnoreCase(MINIMUM_TOPUP_AMOUNT_VALIDATION) &&
                isElementDisplay(btnConfirmOK);

        clickOn(driver, btnConfirmOK);

        return bool;

    }

    public boolean verifyInsufficientBalanceTopUpValidation() {

        testValidationLog(getText(lblInsufficientBalanceForTopUpValidation));

        boolean bool = isElementDisplay(lblInsufficientBalanceForTopUpValidation) &&
                getText(lblInsufficientBalanceForTopUpValidation).equalsIgnoreCase(INSUFFICIENT_TOPUP_BALANCE) &&
                isElementDisplay(btnConfirmOK);

        clickOn(driver, btnConfirmOK);

        return bool;

    }

    public boolean verifyConsumerTopUpDetailsBeforeComplete() {

        testInfoLog("User Name", getInnerText(lblTopUpUserNameInPopUp));
        testInfoLog("User Number", getInnerText(lblTopUpUserNumberInPopUp));
        testInfoLog("Top up Amount", getInnerText(lblTopUpAmountInPopUp));

        topUpUserName = getInnerText(lblTopUpUserNameInPopUp);

        return getInnerText(lblTopUpUserNumberInPopUp).contains(AgentDashboardIndexPage._topUpMobileNumber) &&
                getIntegerFromString(getInnerText(lblTopUpAmountInPopUp).replace("₹", "")) ==
                        AgentDashboardIndexPage._topUpAmount;

    }

    public boolean verifyConsumerTopUpDetailsAfterComplete() {

        testConfirmationLog(getInnerText(lblTopUpSuccessful));

        testInfoLog("User Name", getInnerText(lblTopUpUserNameInPopUp));
        testInfoLog("User Number", getInnerText(lblTopUpUserNumberInPopUp));
        testInfoLog("Top up Amount", getInnerText(lblTopUpAmountInPopUp));

        return getInnerText(lblTopUpUserNumberInPopUp).contains(AgentDashboardIndexPage._topUpMobileNumber) &&
                getIntegerFromString(getInnerText(lblTopUpAmountInPopUp).replace("₹", "")) ==
                        AgentDashboardIndexPage._topUpAmount &&
                isElementDisplay(btnCloseTopUpPopUp);

    }

    public boolean verifyMerchantTopUpDetailsBeforeComplete() {

        testInfoLog("User Name", getInnerText(lblTopUpUserNameInPopUp));
        testInfoLog("User Number", getInnerText(lblTopUpUserNumberInPopUp));
        testInfoLog("Top up Amount", getInnerText(lblTopUpAmountInPopUp));

        topUpUserName = getInnerText(lblTopUpUserNameInPopUp);

        return getIntegerFromString(getInnerText(lblTopUpAmountInPopUp).replace("₹", "")) ==
                AgentDashboardIndexPage._topUpAmount;

    }

    public boolean verifyMerchantTopUpDetailsAfterComplete() {

        testConfirmationLog(getInnerText(lblTopUpSuccessful));

        testInfoLog("User Name", getInnerText(lblTopUpUserNameInPopUp));
        testInfoLog("User Number", getInnerText(lblTopUpUserNumberInPopUp));
        testInfoLog("Top up Amount", getInnerText(lblTopUpAmountInPopUp));

        return getIntegerFromString(getInnerText(lblTopUpAmountInPopUp).replace("₹", "")) ==
                AgentDashboardIndexPage._topUpAmount &&
                isElementDisplay(btnCloseTopUpPopUp);

    }

    public boolean verifyCompletedConsumerTopUpInTodayTransactionHistory() {

        scrollToElement(driver, transactionHistoryTable);

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        List<String> lstExpectedTimes = new ArrayList<>();

        String topUpDetails = getInnerText(lstCommissionAgentNames.get(0)) + "<br>" +
                getInnerText(lstCommissionAgentIdNumber.get(0)) + "<br>" +
                getInnerText(lstCommissionTxID.get(0)) + "<br>" +
                getInnerText(lstCommissionUserType.get(0)) + "<br>" +
                getInnerText(lstCommissionTopUp.get(0)) + "<br>" +
                getInnerText(lstCommissionAmount.get(0)) + "<br>" +
                getInnerText(lstCommissionTxTime.get(0));

        testConfirmationLog(topUpDetails);

        double commissionAmount = 8.75;

        for (int sec = 1; sec <= 20; sec++) {
            lstExpectedTimes.add((AgentDashboardIndexPage._topUpDateTime.plusSeconds(sec)).format(format).trim());
        }

        System.out.println(getInnerText(lstCommissionAgentNames.get(0)) + "  " + (topUpUserName));
        System.out.println(getInnerText(lstCommissionAgentIdNumber.get(0)) + "   " + (AgentDashboardIndexPage._topUpMobileNumber));
        System.out.println(getInnerText(lstCommissionUserType.get(0)) + "   " + "Consumer");
        System.out.println(getDoubleFromString(getInnerText(lstCommissionTopUp.get(0))) + "   " + (double) (AgentDashboardIndexPage._topUpAmount));
        System.out.println(getDoubleFromString(getInnerText(lstCommissionAmount.get(0))) + "   " + commissionAmount);
        System.out.println(lstExpectedTimes + "  " + (getInnerText(lstCommissionTxTime.get(0))));

        return getInnerText(lstCommissionAgentNames.get(0)).equalsIgnoreCase(topUpUserName) &&
                getInnerText(lstCommissionTxID.get(0)).contains("Consumer") &&
                getInnerText(lstCommissionAgentIdNumber.get(0)).equalsIgnoreCase(AgentDashboardIndexPage._topUpMobileNumber) &&
                getInnerText(lstCommissionUserType.get(0)).equalsIgnoreCase("Consumer") &&
                getDoubleFromString(getInnerText(lstCommissionTopUp.get(0))) == (double) (AgentDashboardIndexPage._topUpAmount) &&
                getDoubleFromString(getInnerText(lstCommissionAmount.get(0))) == commissionAmount &&
                lstExpectedTimes.contains(getInnerText(lstCommissionTxTime.get(0)));


    }

    public boolean verifyCompletedMerchantTopUpInTodayTransactionHistory() {

        scrollToElement(driver, transactionHistoryTable);

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        List<String> lstExpectedTimes = new ArrayList<>();

        String topUpDetails = getInnerText(lstCommissionAgentNames.get(0)) + "<br>" +
                getInnerText(lstCommissionAgentIdNumber.get(0)) + "<br>" +
                getInnerText(lstCommissionTxID.get(0)) + "<br>" +
                getInnerText(lstCommissionUserType.get(0)) + "<br>" +
                getInnerText(lstCommissionTopUp.get(0)) + "<br>" +
                getInnerText(lstCommissionAmount.get(0)) + "<br>" +
                getInnerText(lstCommissionTxTime.get(0));

        testConfirmationLog(topUpDetails);

        double commissionAmount = formatTwoDecimal((AgentDashboardIndexPage._topUpAmount * 0.0089));

        for (int sec = 1; sec <= 20; sec++) {
            lstExpectedTimes.add((AgentDashboardIndexPage._topUpDateTime.plusSeconds(sec)).format(format).trim());
        }

        System.out.println(getInnerText(lstCommissionAgentNames.get(0)) + "  " + (topUpUserName));
        System.out.println(getInnerText(lstCommissionAgentIdNumber.get(0)) + "   " + (AgentDashboardIndexPage._topUpMobileNumber));
        System.out.println(getInnerText(lstCommissionUserType.get(0)) + "   " + "Merchant");
        System.out.println(getDoubleFromString(getInnerText(lstCommissionTopUp.get(0))) + "   " + (double) (AgentDashboardIndexPage._topUpAmount));
        System.out.println(getDoubleFromString(getInnerText(lstCommissionAmount.get(0))) + "   " + commissionAmount);
        System.out.println(lstExpectedTimes + "  " + (getInnerText(lstCommissionTxTime.get(0))));

        return getInnerText(lstCommissionAgentNames.get(0)).equalsIgnoreCase(topUpUserName) &&
                getInnerText(lstCommissionTxID.get(0)).contains("Merchant") &&
                getInnerText(lstCommissionAgentIdNumber.get(0)).equalsIgnoreCase(AgentDashboardIndexPage._topUpMobileNumber) &&
                getInnerText(lstCommissionUserType.get(0)).equalsIgnoreCase("Merchant") &&
                getDoubleFromString(getInnerText(lstCommissionTopUp.get(0))) == (double) (AgentDashboardIndexPage._topUpAmount) &&
                getDoubleFromString(getInnerText(lstCommissionAmount.get(0))) == commissionAmount &&
                lstExpectedTimes.contains(getInnerText(lstCommissionTxTime.get(0)));


    }

    public boolean verifyBalanceAndTodayConsumerTopUpDetails() {

        pause(5);

        scrollToElement(driver, lstLblBalance.get(0));

        double expectedCommission = formatTwoDecimal((AgentDashboardIndexPage._topUpAmount * 0.0089));
        double totalExpectedCommission = formatTwoDecimal(AgentDashboardIndexPage.commissionBeforeTopUp + expectedCommission);
        double expectedBalance = formatTwoDecimal(AgentDashboardIndexPage.balanceBeforeTopUp - AgentDashboardIndexPage._topUpAmount
                + expectedCommission);

        return getDoubleFromString(getInnerText(lstLblBalance.get(0))) == expectedBalance &&
                getDoubleFromString(getInnerText(lstLblBalance.get(1))) == totalExpectedCommission &&
                (getIntegerFromString(getInnerText(lblComletedTopups)) ==
                        AgentDashboardIndexPage.completedTopUpsBeforeTopUp + 1) &&
                (getIntegerFromString(getInnerText(lblTopUpUserList)) ==
                        AgentDashboardIndexPage.todayTopUpsBeforeTopUp + 1);
    }

    public boolean verifyBalanceAndTodayMerchantTopUpDetails() {

        scrollToElement(driver, lstLblBalance.get(0));

        double expectedCommission = formatTwoDecimal((AgentDashboardIndexPage._topUpAmount * 0.0089));
        double totalExpectedCommission = formatTwoDecimal(AgentDashboardIndexPage.commissionBeforeTopUp + expectedCommission);
        double expectedBalance = formatTwoDecimal(AgentDashboardIndexPage.balanceBeforeTopUp - AgentDashboardIndexPage._topUpAmount
                + expectedCommission);

        return getDoubleFromString(getInnerText(lstLblBalance.get(0))) == expectedBalance &&
                getDoubleFromString(getInnerText(lstLblBalance.get(1))) == totalExpectedCommission &&
                (getIntegerFromString(getInnerText(lblComletedTopups)) ==
                        AgentDashboardIndexPage.completedTopUpsBeforeTopUp) &&
                (getIntegerFromString(getInnerText(lblTopUpUserList)) ==
                        AgentDashboardIndexPage.todayTopUpsBeforeTopUp);
    }
}
