package com.incashme.agent.verification;

import com.framework.init.AbstractPage;
import com.incashme.agent.indexpage.AgentDashboardIndexPage;
import com.incashme.agent.indexpage.AgentTransactionHistoryIndexPage;
import com.incashme.agent.validations.TransactionHistoryValidations;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Rahul R.
 * Date: 2019-04-26
 * Time: 15:10
 * Project Name: InCashMe
 */
public class AgentTransactionHistoryVerification extends AbstractPage implements TransactionHistoryValidations {

    public AgentTransactionHistoryVerification(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//div//p[contains(text(),'no data alive')]")
    private WebElement lblNoTxhistory;

    @FindBy(xpath = "//li/button[contains(@class,'filterBtn')]")
    private WebElement btnFilter;

    @FindBy(xpath = "//table[@id='trans-history-table']")
    private WebElement transactionHistoryTable;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[1]//h5/span")})
    private List<WebElement> lstUserName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[1]//following-sibling::div[contains(text(),'ID') or contains(text(),'Mo')] ")})
    private List<WebElement> lstUserIdNumber;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[2]/span")})
    private List<WebElement> lstUserTxID;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[1]//following-sibling::div[2]")})
    private List<WebElement> lstUserType;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[3]")})
    private List<WebElement> lstUserTopUpAmount;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[4]")})
    private List<WebElement> lstCommissionAmount;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[5]")})
    private List<WebElement> lstTransactionTime;

    @FindBy(xpath = "//input[@type='search']")
    private WebElement txtInputSearch;

    @FindBy(xpath = "//table//tr//td[contains(text(),'matching')]")
    private WebElement lblNoRecordsFound;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//button[contains(@class,'searchbtn')]")
    private WebElement btnFilterSearch;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//a[contains(text(),'Clear All')]")
    private WebElement btnFilterClearAll;

    @FindBy(xpath = "//div//h2[contains(text(),'Wakeup')]")
    private WebElement lblNoFilterConsumerAvailable;

    @FindBy(xpath = ".//div[contains(@id,'swal2-content') and contains(text(),'Invalid')]")
    private WebElement lblInvalidAmountValidation;

    @FindBy(xpath = ".//div[contains(@class,'swal2-actions')]//button[text()='OK']")
    private WebElement btnOTPConfirmOK;

    @FindBy(xpath = "//div[@role='dialog']//div[contains(@class,'transfer-id')]")
    private WebElement lblPopUpTransactionID;

    @FindAll(value = {@FindBy(xpath = "//ul[contains(@class,'transfer-details')]//li//div[2]")})
    private List<WebElement> lstTransactionDetails;

    public boolean verifyTransactionHistoryScreen() {

        pause(5);

        if (isElementPresent(lblNoTxhistory)) {

            scrollToElement(driver, lblNoTxhistory);
            testValidationLog(getText(lblNoTxhistory));
            return isElementDisplay(lblNoTxhistory) && isElementDisplay(btnFilter) &&
                    getText(lblNoTxhistory).equalsIgnoreCase(NO_TRANSACTION_HISTORY_FOUND);

        } else {
            return isElementDisplay(transactionHistoryTable) && isElementDisplay(btnFilter);
        }

    }

    public boolean verifyCompletedTopUpDisplayOnScreen() {

        scrollToElement(driver, transactionHistoryTable);

        boolean bool;

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        List<String> lstExpectedTimes = new ArrayList<>();

        double commissionAmount = formatTwoDecimal((AgentDashboardIndexPage._topUpAmount * 0.0089));

        for (int sec = 1; sec <= 20; sec++) {
            lstExpectedTimes.add((AgentDashboardIndexPage._topUpDateTime.plusSeconds(sec)).format(format).trim());
        }

        pause(2);

        System.out.println(getInnerText(lstUserType.get(0)));
        System.out.println(getInnerText(lstUserTxID.get(0)));

        if (getInnerText(lstUserType.get(0)).equalsIgnoreCase("Consumer")) {
            bool = getInnerText(lstUserType.get(0)).contains("Consumer") &&
                    getInnerText(lstUserTxID.get(0)).contains("Consumer");
        } else {
            System.out.println(getInnerText(lstUserType.get(0)));
            bool = getInnerText(lstUserType.get(0)).contains("Merchant") &&
                    getInnerText(lstUserTxID.get(0)).contains("Merchant");
        }

        System.out.println("Bool : " + bool);

        return getInnerText(lstUserName.get(0)).equalsIgnoreCase(AgentDashboardVerification.topUpUserName) &&
                getInnerText(lstUserIdNumber.get(0)).contains(AgentDashboardIndexPage._topUpMobileNumber) &&
                getDoubleFromString(getInnerText(lstUserTopUpAmount.get(0))) == (double) (AgentDashboardIndexPage._topUpAmount) &&
                getDoubleFromString(getInnerText(lstCommissionAmount.get(0))) == commissionAmount &&
                lstExpectedTimes.contains(getInnerText(lstTransactionTime.get(0))) && bool;

    }

    public boolean verifySearchSuccessful() {

        boolean bool = true;

        for (WebElement e : lstUserTxID) {
            bool = bool && getInnerText(e).contains(AgentTransactionHistoryIndexPage._searchCriteria);
        }

        return bool;

    }

    public boolean verifyValidationForInvalidSearch() {

        testValidationLog(getInnerText(lblNoRecordsFound));

        boolean bool = getInnerText(lblNoRecordsFound).equalsIgnoreCase(NO_SEARCH_RECORD_FOUND);

        type(txtInputSearch, "x");
        txtInputSearch.clear();

        return bool;
    }

    public boolean verifyTopUpsSorted() {

        List<Double> commissionTopUpAfterSort = new ArrayList<>();
        List<Double> commissionTopUpBeforeSort = AgentTransactionHistoryIndexPage._transactionTopUpBeforeSort;

        for (WebElement name : lstUserTopUpAmount) {
            commissionTopUpAfterSort.add(getDoubleFromString(getInnerText(name)));
        }

        Collections.sort(commissionTopUpBeforeSort);

        System.out.println(commissionTopUpAfterSort);
        System.out.println(commissionTopUpBeforeSort);

        System.out.println(commissionTopUpBeforeSort.equals(commissionTopUpAfterSort));

        return commissionTopUpBeforeSort.equals(commissionTopUpAfterSort);
    }

    public boolean verifyCommissionSorted() {

        List<Double> commissionAmountAfterSort = new ArrayList<>();
        List<Double> commissionAmountBeforeSort = AgentTransactionHistoryIndexPage._commissionAmountBeforeSort;

        for (WebElement name : lstCommissionAmount) {
            commissionAmountAfterSort.add(getDoubleFromString(getInnerText(name)));
        }

        Collections.sort(commissionAmountBeforeSort);

        System.out.println(commissionAmountAfterSort);
        System.out.println(commissionAmountBeforeSort);

        System.out.println(commissionAmountBeforeSort.equals(commissionAmountAfterSort));

        return commissionAmountBeforeSort.equals(commissionAmountAfterSort);
    }

    public boolean verifyCommissionDateTimeSorted() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        List<LocalDateTime> commissionDateTimeAfterSort = new ArrayList<>();
        List<LocalDateTime> commissionDateTimeBeforeSort = AgentTransactionHistoryIndexPage._transactionDateTimeBeforeSort;

        for (WebElement name : lstTransactionTime) {
            commissionDateTimeAfterSort.add(LocalDateTime.parse(getInnerText(name), format));
        }

        Collections.sort(commissionDateTimeBeforeSort);

        System.out.println(commissionDateTimeAfterSort);
        System.out.println(commissionDateTimeBeforeSort);

        System.out.println(commissionDateTimeBeforeSort.equals(commissionDateTimeAfterSort));

        return commissionDateTimeBeforeSort.equals(commissionDateTimeAfterSort);
    }

    public boolean verifyTransactionFilterScreenDisplay() {

        try {
            updateShowList(driver, findElementByName(driver, "trans-history-table_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        return isElementDisplay(btnFilterSearch) && isElementDisplay(btnFilterClearAll);
    }

    public boolean verifyRandomDataFiltered() {
        return getText(lblNoFilterConsumerAvailable).equalsIgnoreCase(NO_FILTER_DATA_AVAILABLE);
    }

    public boolean verifyFirstNameFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement name : lstUserName) {
            finalFilterResult.add(getInnerText(name).split(" ")[0].trim());
        }

        System.out.println(finalFilterResult);
        System.out.println(AgentTransactionHistoryIndexPage._filterFirstName);

        return finalFilterResult.contains(AgentTransactionHistoryIndexPage._filterFirstName);
    }

    public boolean verifyLastNameFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement name : lstUserName) {
            finalFilterResult.add(getInnerText(name).replace(AgentTransactionHistoryIndexPage._filterFirstName, "").trim());
        }

        System.out.println(finalFilterResult);
        System.out.println(AgentTransactionHistoryIndexPage._filterLastName);

        return finalFilterResult.contains(AgentTransactionHistoryIndexPage._filterLastName);
    }

    public boolean verifyPhoneNumberFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement number : lstUserIdNumber) {
            finalFilterResult.add(getInnerText(number));
        }

        System.out.println(finalFilterResult);
        System.out.println(AgentTransactionHistoryIndexPage._filterPhone);

        return finalFilterResult.contains(AgentTransactionHistoryIndexPage._filterPhone);
    }

    public boolean verifyTxIDFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement number : lstUserTxID) {
            finalFilterResult.add(String.valueOf(getIntegerFromString(getInnerText(number))));
        }

        System.out.println(finalFilterResult);
        System.out.println(AgentTransactionHistoryIndexPage._filterTransactionId);

        return finalFilterResult.contains(AgentTransactionHistoryIndexPage._filterTransactionId) &&
                sizeOf(finalFilterResult) == 1;
    }

    public boolean verifyMaxMinTopupValidation() {

        boolean bool = getText(lblInvalidAmountValidation).equalsIgnoreCase(INVALID_TOPUP_AMOUNT_RANGE);

        testValidationLog(getText(lblInvalidAmountValidation));
        clickOn(driver, btnOTPConfirmOK);

        return bool;
    }

    public boolean verifyMaxMinCommissionValidation() {

        boolean bool = getText(lblInvalidAmountValidation).equalsIgnoreCase(INVALID_COMMISSION_AMOUNT_RANGE);

        testValidationLog(getText(lblInvalidAmountValidation));
        clickOn(driver, btnOTPConfirmOK);

        return bool;
    }

    public boolean verifyTopUpFromFieldFilter() {

        List<Double> finalFilterResult = new ArrayList<>();

        for (WebElement name : lstUserTopUpAmount) {
            finalFilterResult.add(getDoubleFromString(getInnerText(name)));
        }

        System.out.println(finalFilterResult);
        System.out.println(AgentTransactionHistoryIndexPage._filterTopUpFromAmount);

        return !isListEmpty(finalFilterResult) &&
                findMin(finalFilterResult) >= AgentTransactionHistoryIndexPage._filterTopUpFromAmount;
    }

    public boolean verifyTopUpToFieldFilter() {

        List<Double> finalFilterResult = new ArrayList<>();

        for (WebElement name : lstUserTopUpAmount) {
            finalFilterResult.add(getDoubleFromString(getInnerText(name)));
        }

        System.out.println(finalFilterResult);
        System.out.println(AgentTransactionHistoryIndexPage._filterTopUpToAmount);

        return !isListEmpty(finalFilterResult) &&
                findMax(finalFilterResult) <= AgentTransactionHistoryIndexPage._filterTopUpToAmount;
    }

    public boolean verifyCommissionFromFieldFilter() {

        List<Double> finalFilterResult = new ArrayList<>();

        for (WebElement name : lstCommissionAmount) {
            finalFilterResult.add(getDoubleFromString(getInnerText(name)));
        }

        System.out.println(finalFilterResult);
        System.out.println(AgentTransactionHistoryIndexPage._filterCommissionFromAmount);

        return !isListEmpty(finalFilterResult) &&
                findMin(finalFilterResult) >= AgentTransactionHistoryIndexPage._filterCommissionFromAmount;
    }

    public boolean verifyCommissionToFieldFilter() {

        List<Double> finalFilterResult = new ArrayList<>();

        for (WebElement name : lstCommissionAmount) {
            finalFilterResult.add(getDoubleFromString(getInnerText(name)));
        }

        System.out.println(finalFilterResult);
        System.out.println(AgentTransactionHistoryIndexPage._filterCommissionToAmount);

        return !isListEmpty(finalFilterResult) &&
                findMax(finalFilterResult) <= AgentTransactionHistoryIndexPage._filterCommissionToAmount;
    }

    public boolean verifyTopUpMaxMinFilter() {

        List<Double> finalFilterResult = new ArrayList<>();

        for (WebElement name : lstUserTopUpAmount) {
            finalFilterResult.add(getDoubleFromString(getInnerText(name)));
        }

        System.out.println(finalFilterResult);
        System.out.println(AgentTransactionHistoryIndexPage._filterTopUpFromAmount);
        System.out.println(AgentTransactionHistoryIndexPage._filterTopUpToAmount);

        return findMin(finalFilterResult) >= AgentTransactionHistoryIndexPage._filterTopUpFromAmount &&
                findMax(finalFilterResult) <= AgentTransactionHistoryIndexPage._filterTopUpToAmount;

    }

    public boolean verifyCommissionMaxMinFilter() {

        List<Double> finalFilterResult = new ArrayList<>();

        for (WebElement name : lstCommissionAmount) {
            finalFilterResult.add(getDoubleFromString(getInnerText(name)));
        }

        System.out.println(finalFilterResult);
        System.out.println(AgentTransactionHistoryIndexPage._filterCommissionFromAmount);
        System.out.println(AgentTransactionHistoryIndexPage._filterCommissionToAmount);

        return findMin(finalFilterResult) >= AgentTransactionHistoryIndexPage._filterCommissionFromAmount &&
                findMax(finalFilterResult) <= AgentTransactionHistoryIndexPage._filterCommissionToAmount;

    }

    public boolean verifyShopNameFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement name : lstUserName) {
            finalFilterResult.add(getInnerText(name));
        }

        System.out.println(finalFilterResult);
        System.out.println(AgentTransactionHistoryIndexPage._filterShopName);

        return finalFilterResult.contains(AgentTransactionHistoryIndexPage._filterShopName);
    }

    public boolean verifyMerchantIDFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement number : lstUserIdNumber) {
            finalFilterResult.add(getInnerText(number));
        }

        System.out.println(finalFilterResult);
        System.out.println(AgentTransactionHistoryIndexPage._filterID);

        return finalFilterResult.contains(AgentTransactionHistoryIndexPage._filterID);
    }

    public boolean verifyTransactionDetails(String userType) {

        pause(2);

        System.out.println(getText(lblPopUpTransactionID));
        System.out.println(userType + " Transfer ID-" + AgentTransactionHistoryIndexPage._filterTransactionId);

        boolean bool = getText(lblPopUpTransactionID).equalsIgnoreCase(
                userType + " Transfer ID-" + AgentTransactionHistoryIndexPage._filterTransactionId);

        bool = bool && getText(lstTransactionDetails.get(0)).
                equalsIgnoreCase(AgentTransactionHistoryIndexPage._transactionDateAndTime);

        System.out.println(getText(lstTransactionDetails.get(1)));
        System.out.println(AgentTransactionHistoryIndexPage._filterFirstName);
        System.out.println(getLongFromString(AgentTransactionHistoryIndexPage._filterPhone));

        bool = bool && getText(lstTransactionDetails.get(1)).split("\n")[0].equalsIgnoreCase(AgentTransactionHistoryIndexPage._filterFirstName)
                && String.valueOf(getLongFromString(getText(lstTransactionDetails.get(1)).split("\n")[1])).equalsIgnoreCase
                (String.valueOf(getLongFromString(AgentTransactionHistoryIndexPage._filterPhone)));

        System.out.println(getText(lstTransactionDetails.get(2)));
        System.out.println(AgentLoginVerification._username);

        bool = bool && getText(lstTransactionDetails.get(2)).contains(AgentLoginVerification._username);

        System.out.println(getText(lstTransactionDetails.get(3)));
        System.out.println(AgentTransactionHistoryIndexPage._transactionTopUpAmount);

        bool = bool && getText(lstTransactionDetails.get(3)).equalsIgnoreCase(AgentTransactionHistoryIndexPage._transactionTopUpAmount);

        System.out.println(formatTwoDecimal(getDoubleFromString(getText(lstTransactionDetails.get(3)))
                - getDoubleFromString(getText(lstTransactionDetails.get(3))) / 100));
        System.out.println(getDoubleFromString(getText(lstTransactionDetails.get(4))));

        bool = bool && (formatTwoDecimal(getDoubleFromString(getText(lstTransactionDetails.get(3))))
                - formatTwoDecimal(getDoubleFromString(getText(lstTransactionDetails.get(3))) / 100)) ==
                getDoubleFromString(getText(lstTransactionDetails.get(4)));

        System.out.println(getText(lstTransactionDetails.get(5)));
        System.out.println(AgentTransactionHistoryIndexPage._transactionCommissionAmount);

        return bool && getText(lstTransactionDetails.get(5)).
                equalsIgnoreCase((AgentTransactionHistoryIndexPage._transactionCommissionAmount));
    }
}
