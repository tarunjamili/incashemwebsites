package com.incashme.agent.verification;

import com.framework.init.AbstractPage;
import com.incashme.agent.indexpage.AgentConsumerTargetDetailsIndexPage;
import com.incashme.agent.indexpage.AgentDashboardIndexPage;
import com.incashme.agent.indexpage.AgentMerchantTargetDetailsIndexPage;
import com.incashme.agent.indexpage.AgentUserListIndexPage;
import com.incashme.agent.validations.DashboardValidations;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Rahul R.
 * Date: 2019-04-25
 * Time: 16:35
 * Project Name: InCashMe
 */
public class AgentMerchantTargetDetailsVerification extends AbstractPage implements DashboardValidations {

    public AgentMerchantTargetDetailsVerification(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//li/button[contains(@class,'filterBtn')]")
    private WebElement btnFilter;

    @FindBy(xpath = "//li/a[contains(text(),'Merchant List')]")
    private WebElement btnMerchantList;

    @FindBy(xpath = "//label[@id='p_total']")
    private WebElement btnChartTotal;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[1]//h5")})
    private List<WebElement> lstMerchantName;

    @FindBy(xpath = "//div//p[contains(text(),'couldn’t find any matches')]")
    private WebElement lblNoAddedConsumerDetails;

    @FindBy(xpath = "//div//p[contains(text(),'couldn’t find any matches')]")
    private WebElement lblNoAddedMerchantDetails;

    @FindBy(xpath = "//*[contains(text(),'Consumers List')]//following-sibling::button")
    private WebElement btnCloseConsumerList;

    @FindBy(xpath = "//*[contains(text(),'Merchants List')]//following-sibling::button")
    private WebElement btnCloseMerchantList;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@class,'add-user-list')]//h5")})
    private List<WebElement> lstAddedMerchantNames;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@class,'add-user-list')]//h5//following-sibling::div")})
    private List<WebElement> lstAddedMerchantIDs;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@id,'dataTbl')]//h5//following-sibling::div")})
    private List<WebElement> lstAddedMerchantID;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[1]//h5")})
    private List<WebElement> lstAddedMerchantName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[2]//span")})
    private List<WebElement> lstAddedMerchantNumber;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[3]//div")})
    private List<WebElement> lstMerchantStatus;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[4]")})
    private List<WebElement> lstMerchantCreatedDateTime;

    @FindBy(xpath = "//input[@type='search']")
    private WebElement txtInputSearch;

    @FindBy(xpath = "//table//tr//td[contains(text(),'matching')]")
    private WebElement lblNoRecordsFound;

    @FindAll(value = {@FindBy(xpath = "//table[@id='usersTbl2']//tr//td[1]//h5/following-sibling::div[1]")})
    private List<WebElement> lstListMerchantID;

    @FindBy(xpath = "//div[contains(@id,'usersTbl2_filter')]//input[@type='search']")
    private WebElement txtPopupInputSearch;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//button[contains(@class,'searchbtn')]")
    private WebElement btnFilterSearch;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//a[contains(text(),'Clear All')]")
    private WebElement btnFilterClearAll;

    @FindBy(xpath = "//div//h2[contains(text(),'Wakeup')]")
    private WebElement lblNoFilterMerchantAvailable;


    public boolean verifyMerchantTargetDetailsScreen() {

        boolean bool;

        if (AgentMerchantTargetDetailsIndexPage._targetLeftInMerchantDetails == 0)
            bool = AgentMerchantTargetDetailsIndexPage._targetAchievedInMerchantDetails >=
                    (AgentMerchantTargetDetailsIndexPage._totalTargetForMerchant);
        else
            bool = AgentMerchantTargetDetailsIndexPage._targetAchievedInMerchantDetails ==
                    (AgentMerchantTargetDetailsIndexPage._totalTargetForMerchant -
                            AgentMerchantTargetDetailsIndexPage._targetLeftInMerchantDetails);

        scrollToElement(driver, btnChartTotal);

        bool = bool && AgentMerchantTargetDetailsIndexPage._daysCompletedInMerchantDetails ==
                (AgentMerchantTargetDetailsIndexPage._totalDaysInMerchantDetails - AgentMerchantTargetDetailsIndexPage._daysLeftInMerchantDetails);

        bool = bool && AgentDashboardIndexPage._merchantTargetCompleted == AgentMerchantTargetDetailsIndexPage._targetAchievedInMerchantDetails &&
                AgentDashboardIndexPage._merchantsTarget == AgentMerchantTargetDetailsIndexPage._totalTargetForMerchant &&
                AgentDashboardIndexPage._merchantsDaysLeft == AgentMerchantTargetDetailsIndexPage._daysLeftInMerchantDetails;

        return bool && isElementDisplay(btnMerchantList) && isElementDisplay(btnFilter);
    }

    public boolean verifyMerchantListCount() {

        if (isListEmpty(lstMerchantName)) {
            pause(2);
            testValidationLog(getInnerText(lblNoAddedMerchantDetails));
            return isListEmpty(lstMerchantName)
                    && getInnerText(lblNoAddedMerchantDetails).contains(NO_CREATED_MERCHANT_LIST_MESSAGE);
        } else {
            scrollElement(findElementByName(driver, "dataTbl_length"));
            updateShowList(driver, findElementByName(driver, "dataTbl_length"), "All");

            return !isElementPresent(lblNoAddedMerchantDetails)
                    && (sizeOf(lstMerchantName) == AgentMerchantTargetDetailsIndexPage._totalMerchantFromChart);
        }
    }

    public boolean verifyTotalAddedMerchantDetails() {

        if (sizeOf(AgentMerchantTargetDetailsIndexPage._lstMerchantNames) == 0) {
            pause(2);
            String validation = getText(lblNoAddedMerchantDetails);
            testValidationLog(validation);
            testStepsLog(_logStep++, "Click on Close button.");
            clickOn(driver, btnCloseConsumerList);
            return sizeOf(lstAddedMerchantNames) == sizeOf(AgentMerchantTargetDetailsIndexPage._lstMerchantNames) &&
                    validation.equalsIgnoreCase(NO_TOTAL_ADDED_MERCHANT_MESSAGE);
        } else {

            updateShowList(driver, findElementByName(driver, "usersTbl2_length"), "All");

            testInfoLog("Total Added Merchants", String.valueOf(sizeOf(lstAddedMerchantNames)));
            testInfoLog("________________________________________________________________________", "");
            for (int conNum = 0; conNum < sizeOf(lstAddedMerchantNames); conNum++) {

                testInfoLog("Merchant Name :" + getInnerText(lstAddedMerchantNames.get(conNum))
                        + "<br>Merchant ID : " + getInnerText(lstAddedMerchantIDs.get(conNum)), "");
                testInfoLog("________________________________________________________________________", "");
            }
            return sizeOf(lstAddedMerchantNames) == sizeOf(AgentMerchantTargetDetailsIndexPage._lstMerchantNames);
        }

    }

    public boolean verifyMerchantNameSorted() {

        List<String> merchantNameAfterSort = new ArrayList<>();
        List<String> merchantNameBeforeSort = AgentMerchantTargetDetailsIndexPage._merchantNameBeforeSort;

        for (WebElement name : lstAddedMerchantName) {
            merchantNameAfterSort.add(getInnerText(name));
        }

        merchantNameBeforeSort.sort(
                (String o1, String o2) -> {
                    if (o1.equalsIgnoreCase(o2)) {
                        return o2.compareTo(o1);
                    }
                    return o1.toLowerCase().compareTo(o2.toLowerCase());
                }
        );

        System.out.println(merchantNameAfterSort);
        System.out.println(merchantNameBeforeSort);

        System.out.println(merchantNameBeforeSort.equals(merchantNameAfterSort));

        return merchantNameBeforeSort.equals(merchantNameAfterSort);
    }

    public boolean verifyMerchantNumberSorted() {

        List<String> merchantNumberAfterSort = new ArrayList<>();
        List<String> merchantNumberBeforeSort = AgentMerchantTargetDetailsIndexPage._merchantNumberBeforeSort;

        for (WebElement name : lstAddedMerchantNumber) {
            merchantNumberAfterSort.add(getInnerText(name));
        }

        merchantNumberBeforeSort.sort(
                (String o1, String o2) -> {
                    if (o1.equalsIgnoreCase(o2)) {
                        return o2.compareTo(o1);
                    }
                    return o1.toLowerCase().compareTo(o2.toLowerCase());
                }
        );

        System.out.println(merchantNumberAfterSort);
        System.out.println(merchantNumberBeforeSort);

        System.out.println(merchantNumberBeforeSort.equals(merchantNumberAfterSort));

        return merchantNumberBeforeSort.equals(merchantNumberAfterSort);
    }


    public boolean verifyMerchantStatusSorted() {

        List<String> agentStatusAfterSort = new ArrayList<>();
        List<String> agentStatusBeforeSort = AgentMerchantTargetDetailsIndexPage._merchantStatusBeforeSort;

        for (WebElement name : lstMerchantStatus) {
            agentStatusAfterSort.add(getInnerText(name));
        }

        agentStatusBeforeSort.sort(
                (String o1, String o2) -> {
                    if (o1.equalsIgnoreCase(o2)) {
                        return o2.compareTo(o1);
                    }
                    return o1.toLowerCase().compareTo(o2.toLowerCase());
                }
        );

        System.out.println(agentStatusAfterSort);
        System.out.println(agentStatusBeforeSort);

        System.out.println(agentStatusBeforeSort.equals(agentStatusAfterSort));

        return agentStatusBeforeSort.equals(agentStatusAfterSort);
    }


    public boolean verifyMerchantDateTimeSorted() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        List<LocalDateTime> agentDateTimeAfterSort = new ArrayList<>();
        List<LocalDateTime> agentDateTimeBeforeSort = AgentMerchantTargetDetailsIndexPage._merchantDateTimeBeforeSort;

        for (WebElement name : lstMerchantCreatedDateTime) {
            agentDateTimeAfterSort.add(LocalDateTime.parse(getInnerText(name), format));
        }

        Collections.sort(agentDateTimeBeforeSort);

        System.out.println(agentDateTimeAfterSort);
        System.out.println(agentDateTimeBeforeSort);

        System.out.println(agentDateTimeBeforeSort.equals(agentDateTimeAfterSort));

        return agentDateTimeBeforeSort.equals(agentDateTimeAfterSort);
    }

    public boolean verifyMerchantSearchSuccessful() {

        boolean bool = getInnerText(lstAddedMerchantNumber.get(0)).contains(AgentMerchantTargetDetailsIndexPage._searchCriteria) &&
                sizeOf(lstAddedMerchantNumber) == 1;

        type(txtInputSearch, "");

        return bool;

    }

    public boolean verifyMerchantValidationForInvalidSearch() {

        testValidationLog(getInnerText(lblNoRecordsFound));

        boolean bool = getInnerText(lblNoRecordsFound).equalsIgnoreCase(NO_SEARCH_RECORD_FOUND);

        type(txtInputSearch, "x");
        txtInputSearch.clear();

        return bool;
    }

    public boolean verifyListMerchantSearchSuccessful() {

        boolean bool = getInnerText(lstListMerchantID.get(0)).contains(AgentMerchantTargetDetailsIndexPage._searchCriteria) &&
                sizeOf(lstListMerchantID) == 1;

        type(txtPopupInputSearch, "");

        return bool;

    }

    public boolean verifyValidationForListInvalidSearch() {

        testValidationLog(getInnerText(lblNoRecordsFound));

        boolean bool = getInnerText(lblNoRecordsFound).equalsIgnoreCase(NO_SEARCH_RECORD_FOUND);

        type(txtPopupInputSearch, "x");
        txtPopupInputSearch.clear();

        return bool;
    }

    public boolean verifyMerchantFilterScreenDisplay() {

        updateShowList(driver, findElementByName(driver, "dataTbl_length"), "All");

        return isElementDisplay(btnFilterSearch) && isElementDisplay(btnFilterClearAll);
    }

    public boolean verifyRandomDataFiltered() {
        return getText(lblNoFilterMerchantAvailable).equalsIgnoreCase(NO_FILTER_DATA_AVAILABLE);
    }

    public boolean verifyShopNameFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement number : lstAddedMerchantID) {
            finalFilterResult.add(getInnerText(number));
        }

        System.out.println(finalFilterResult);
        System.out.println(AgentMerchantTargetDetailsIndexPage._filterMerchantID);

        return finalFilterResult.contains(AgentMerchantTargetDetailsIndexPage._filterMerchantID);
    }

    public boolean verifyMerchantIDFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement number : lstAddedMerchantID) {
            finalFilterResult.add(getInnerText(number));
        }

        System.out.println(finalFilterResult);
        System.out.println(AgentMerchantTargetDetailsIndexPage._filterMerchantID);

        return finalFilterResult.contains(AgentMerchantTargetDetailsIndexPage._filterMerchantID);
    }

    public boolean verifyPhoneNumberFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement number : lstAddedMerchantNumber) {
            finalFilterResult.add(getInnerText(number));
        }

        System.out.println(finalFilterResult);
        System.out.println(AgentMerchantTargetDetailsIndexPage._filterMerchantPhone);

        return finalFilterResult.contains(AgentMerchantTargetDetailsIndexPage._filterMerchantPhone);
    }

    public boolean verifyStatusFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement status : lstMerchantStatus) {
            finalFilterResult.add(getInnerText(status));
        }

        System.out.println(finalFilterResult);
        System.out.println(AgentMerchantTargetDetailsIndexPage._filterStatus);

        if (isListEmpty(finalFilterResult)) {
            testValidationLog(getText(lblNoFilterMerchantAvailable));
            return !AgentMerchantTargetDetailsIndexPage._merchantStatusBeforeSort.contains(AgentMerchantTargetDetailsIndexPage._filterStatus)
                    && getText(lblNoFilterMerchantAvailable).equalsIgnoreCase(NO_FILTER_DATA_AVAILABLE);
        } else {
            return finalFilterResult.stream().distinct().limit(2).count() <= 1 &&
                    finalFilterResult.contains(AgentMerchantTargetDetailsIndexPage._filterStatus);
        }
    }

    public boolean verifyCreatedMerchantSearchSuccessful() {

        boolean bool = getInnerText(lstAddedMerchantNumber.get(0)).contains(AgentUserListIndexPage._newUserMobileNumber) &&
                sizeOf(lstAddedMerchantNumber) == 1;

        type(txtInputSearch, "");

        return bool;

    }
}
