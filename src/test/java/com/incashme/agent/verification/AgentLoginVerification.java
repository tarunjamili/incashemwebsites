package com.incashme.agent.verification;

import com.framework.init.AbstractPage;
import com.incashme.agent.validations.LoginValidations;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by Rahul R.
 * Date: 2019-04-09
 * Time
 * Project Name: InCashMe
 */

public class AgentLoginVerification extends AbstractPage implements LoginValidations {

    public AgentLoginVerification(WebDriver driver) {
        super(driver);
    }

    public static String _username;

    @FindBy(xpath = ".//div[not(@hidden) and @class='login-form']//span[@class='err-msg']")
    private WebElement lblInvalidLoginEmailValidation;

    @FindBy(xpath = ".//div[not(@hidden) and @class='login-form']//span[@class='err-msg' and contains(text(),'Email')]")
    private WebElement lblBlankLoginEmailValidation;

    @FindBy(xpath = ".//div[not(@hidden) and @class='login-form']//span[@class='err-msg' and contains(text(),'Password')]")
    private WebElement lblBalnkLoginPasswordValidation;

    @FindBy(xpath = "//div[@class='pageError' and contains(text(),'Invalid')]")
    private WebElement lblUnRegisteredEmailValidation;

    @FindBy(xpath = "//div[@class='pageError' and contains(text(),'kyc')]")
    private WebElement lblNoKYCValidation;

    @FindBy(xpath = "//div[@class='pageError' and contains(text(),'past password')]")
    private WebElement lblPastPasswordValidation;

    @FindBy(xpath = ".//div[contains(@class,'user-info')]//span[contains(@class,'name')]")
    private WebElement lblDashboardUserName;

    @FindBy(xpath = ".//div[not(@hidden) and @class='login-form']//input[@formcontrolname='email']")
    private WebElement txtEmailAddress;

    @FindBy(xpath = ".//div[not(@hidden) and @class='login-form']//input[@formcontrolname='password']")
    private WebElement txtPassword;

    @FindBy(xpath = ".//div[not(@hidden) and @class='login-form']//button[@type='submit']")
    private WebElement btnLogin;

    @FindBy(xpath = ".//div[not(@hidden) and @class='login-form']//button[@type='submit']")
    private WebElement btnSubmit;

    @FindBy(xpath = ".//div[not(@hidden) and @class='login-form']//a[contains(text(),'Back')]")
    private WebElement btnBackToLogin;

    @FindBy(xpath = ".//div[not(@hidden) and @class='login-form']//a[contains(text(),'Forgot')]")
    private WebElement btnForgotPassword;

    @FindBy(xpath = ".//div[contains(@id,'swal2-content') and contains(text(),'OTP')]")
    private WebElement lblForgotPasswordConfirmOTPSent;

    @FindBy(xpath = ".//div[contains(@id,'swal2-content') and contains(text(),'Email')]")
    private WebElement lblForgotPasswordConfirmEmailSent;

    @FindBy(xpath = ".//div[contains(@class,'swal2-actions')]//button[text()='OK']")
    private WebElement btnOTPConfirmOK;

    @FindBy(xpath = ".//div[not(@hidden) and @class='login-form']//input[@formcontrolname='otp']")
    private WebElement txtOTPField;

    @FindBy(xpath = ".//div[not(@hidden) and @class='login-form']//input[@formcontrolname='password2']")
    private WebElement txtNewPassword;

    @FindBy(xpath = ".//div[not(@hidden) and @class='login-form']//input[@formcontrolname='password']")
    private WebElement txtConfirmPassword;

    @FindBy(xpath = ".//div[not(@hidden) and @class='login-form']//button[@type='submit']")
    private WebElement btnChangePassword;

    @FindBy(xpath = ".//div[not(@hidden) and @class='login-form']//span[@class='err-msg']")
    private WebElement lblInvalidOTPDigit;

    @FindBy(xpath = ".//div[not(@hidden) and @class='login-form']//span[@class='err-msg']")
    private WebElement lblInvalidNewPasswordValidation;

    @FindBy(xpath = ".//div[contains(@id,'swal2-content') and contains(text(),'successfully changed')]")
    private WebElement lblConfirmPasswordChange;

    @FindBy(xpath = ".//div[contains(@class,'swal2-actions')]//button[text()='OK']")
    private WebElement btnConfirmOK;

    @FindBy(xpath = "//div[@class='pageError' and contains(text(),'locked')]")
    private WebElement lblAccountLockedValidation;

    @FindBy(xpath = ".//div[contains(@id,'swal2-content') and contains(text(),'expired')]")
    private WebElement lblPasswordLinkExpired;

    public boolean verifyInvalidEmailValidation() {

        testValidationLog(getText(lblInvalidLoginEmailValidation));

        return isElementDisplay(lblInvalidLoginEmailValidation) &&
                getText(lblInvalidLoginEmailValidation).equalsIgnoreCase(LOGIN_INVALID_EMAIL_ADDRESS);
    }

    public boolean verifyBlankLoginValidation() {

        testValidationLog(getText(lblBlankLoginEmailValidation));
        testValidationLog(getText(lblBalnkLoginPasswordValidation));

        return isElementDisplay(lblBlankLoginEmailValidation) && isElementDisplay(lblBalnkLoginPasswordValidation) &&
                getText(lblBlankLoginEmailValidation).equalsIgnoreCase(LOGIN_BLANK_EMAIL_ADDRESS) &&
                getText(lblBalnkLoginPasswordValidation).equalsIgnoreCase(LOGIN_BLANK_PASSWORD_ADDRESS);
    }

    public boolean verifyUserLoginSuccessfully() {
        _username = getText(lblDashboardUserName);
        testInfoLog("Logged in User Name", _username);
        return isElementDisplay(lblDashboardUserName);
    }

    public boolean verifyUserLogoutSuccessfully() {
        return isElementDisplay(txtEmailAddress) && isElementDisplay(txtPassword) &&
                isElementDisplay(btnLogin) && isElementDisplay(btnForgotPassword);
    }

    public boolean verifyUnregisteredEmailValidation() {

        testValidationLog(getText(lblUnRegisteredEmailValidation));

        return isElementDisplay(lblUnRegisteredEmailValidation) &&
                getText(lblUnRegisteredEmailValidation).equalsIgnoreCase(LOGIN_UNREGISTERED_EMAIL_ADDRESS);
    }

    public boolean verifyForgotPasswordScreenDisplay() {
        return isElementDisplay(txtEmailAddress) && isElementDisplay(btnBackToLogin) && isElementDisplay(btnSubmit);
    }

    public boolean verifyStep1OTPScreenDisplay() {

        boolean bool = getText(lblForgotPasswordConfirmOTPSent).equalsIgnoreCase(FORGOT_PASSWORD_OTP_CONFIRMATION_STEP_1);

        testConfirmationLog(getText(lblForgotPasswordConfirmOTPSent));
        clickOn(driver, btnOTPConfirmOK);

        return isElementDisplay(txtOTPField) && isElementDisplay(btnBackToLogin) && isElementDisplay(btnSubmit) && bool;
    }

    public boolean verifyStep2OTPScreenDisplay() {

        boolean bool = getText(lblForgotPasswordConfirmEmailSent).equalsIgnoreCase(FORGOT_PASSWORD_OTP_CONFIRMATION_STEP_2);

        testConfirmationLog(getText(lblForgotPasswordConfirmEmailSent));
        clickOn(driver, btnOTPConfirmOK);

        return isElementDisplay(txtEmailAddress) && isElementDisplay(txtPassword) &&
                isElementDisplay(btnLogin) && isElementDisplay(btnForgotPassword) && bool;
    }

    public boolean verifyInvalidOTPValidation() {

        boolean bool = getText(lblForgotPasswordConfirmOTPSent).equalsIgnoreCase(FORGOT_PASSWORD_OTP_MISMATCH);

        testValidationLog(getText(lblForgotPasswordConfirmOTPSent));
        clickOn(driver, btnOTPConfirmOK);

        return isElementDisplay(txtOTPField) && isElementDisplay(btnBackToLogin) && isElementDisplay(btnSubmit) && bool;
    }

    public boolean verifyLoginKYCValidationMessage() {
        testValidationLog(getText(lblNoKYCValidation));
        return getText(lblNoKYCValidation).equalsIgnoreCase(LOGIN_KYC_PENDING_MESSAGE);
    }

    public boolean verifyForgotPasswordKYCValidationMessage() {
        testValidationLog(getText(lblNoKYCValidation));
        return getText(lblNoKYCValidation).equalsIgnoreCase(FORGOT_PASSWORD_KYC_PENDING_MESSAGE);
    }

    public boolean verifyResetPasswordScreen() {
        pause(5);
        return isElementDisplay(txtOTPField) && isElementDisplay(txtNewPassword) &&
                isElementDisplay(txtConfirmPassword) && isElementDisplay(btnChangePassword) &&
                isElementDisplay(btnBackToLogin);
    }

    public boolean verifyInvalidOTPDigitMessage() {
        testValidationLog(getText(lblInvalidOTPDigit));
        return isElementDisplay(lblInvalidOTPDigit) &&
                getText(lblInvalidOTPDigit).trim().equalsIgnoreCase(INVALID_OTP_DIGIT_MESSAGE);
    }

    public boolean verifyInvalidNewPasswordValidation() {
        testValidationLog(getText(lblInvalidNewPasswordValidation));
        return isElementDisplay(lblInvalidNewPasswordValidation) &&
                getText(lblInvalidNewPasswordValidation).equalsIgnoreCase(INVALID_NEW_PASSWORD);
    }

    public boolean verifyPastPasswordValidation() {

        testValidationLog(getText(lblPastPasswordValidation));

        return isElementDisplay(lblPastPasswordValidation) &&
                getText(lblPastPasswordValidation).equalsIgnoreCase(PAST_PASSWORD_MESSAGE);
    }

    public boolean verifyPasswordChangeSuccessfully() {

        testConfirmationLog(getText(lblConfirmPasswordChange));

        boolean bool = isElementDisplay(lblConfirmPasswordChange) &&
                getText(lblConfirmPasswordChange).equalsIgnoreCase(SUCCESSFUL_PASSWORD_RESET_MESSAGE) &&
                isElementDisplay(btnConfirmOK);

        clickOn(driver, btnConfirmOK);

        return bool;

    }

    public boolean verifyUserLockedSuccessfullyForWrongPassword() {
        testValidationLog(getText(lblAccountLockedValidation));

        return isElementDisplay(lblAccountLockedValidation) &&
                getText(lblAccountLockedValidation).equalsIgnoreCase(ACCOUNT_LOCKED_WRONG_PASSWORD_THREE_TIMES);
    }

    public boolean verifyUserLockedSuccessfullyForWrongPIN() {
        testValidationLog(getText(lblAccountLockedValidation));

        return isElementDisplay(lblAccountLockedValidation) &&
                getText(lblAccountLockedValidation).equalsIgnoreCase(ACCOUNT_LOCKED_WRONG_PIN_THREE_TIMES);
    }

    public boolean verifyPrivacyPolicyScreen() {

        switchToWindow(driver);

        String expectedTitle = getTitle(driver);
        String actualTitle = "InCashMe™";

        close(driver);

        switchToWindow(driver);

        return expectedTitle.equalsIgnoreCase(actualTitle);

    }

    public boolean verifyTnCScreen() {

        switchToWindow(driver);

        String expectedTitle = getTitle(driver);
        String actualTitle = "InCashMe™";

        close(driver);

        switchToWindow(driver);

        return expectedTitle.equalsIgnoreCase(actualTitle);

    }
}