package com.incashme.agent.verification;

import com.framework.init.AbstractPage;
import com.incashme.agent.indexpage.AgentConsumerTargetDetailsIndexPage;
import com.incashme.agent.indexpage.AgentDashboardIndexPage;
import com.incashme.agent.indexpage.AgentTargetTopUpDetailsIndexPage;
import com.incashme.agent.validations.DashboardValidations;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Rahul R.
 * Date: 2019-04-25
 * Time: 16:18
 * Project Name: InCashMe
 */
public class AgentTargetTopUpDetailsVerification extends AbstractPage implements DashboardValidations {

    public AgentTargetTopUpDetailsVerification(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//label[@id='p_total']")
    private WebElement btnChartTotal;

    @FindBy(xpath = "//li/button[contains(@class,'filterBtn')]")
    private WebElement btnFilter;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//h5[contains(@class,'user-name')]//parent::div")})
    private List<WebElement> lstConsumerDetails;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//td[2]//h5")})
    private List<WebElement> lstConsumerNumber;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[1]//h5")})
    private List<WebElement> lstTopUpConsumerName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[2]//h5")})
    private List<WebElement> lstTopUpConsumerNumber;

    @FindBy(xpath = "//input[@type='search']")
    private WebElement txtInputSearch;

    @FindBy(xpath = "//table//tr//td[contains(text(),'matching')]")
    private WebElement lblNoRecordsFound;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//button[contains(@class,'searchbtn')]")
    private WebElement btnFilterSearch;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//a[contains(text(),'Clear All')]")
    private WebElement btnFilterClearAll;

    @FindBy(xpath = "//div//h2[contains(text(),'Wakeup')]")
    private WebElement lblNoFilterConsumerAvailable;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@class,'add-user-list')]//h5")})
    private List<WebElement> lstAddedConsumerNames;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@class,'add-user-list')]//h5//following-sibling::div")})
    private List<WebElement> lstAddedConsumerNumbers;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[1]//h5")})
    private List<WebElement> lstAddedConsumerName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[2]//h5")})
    private List<WebElement> lstAddedConsumerNumber;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTbl']//tr//td[3]//div")})
    private List<WebElement> lstConsumerStatus;

    @FindBy(xpath = "//div[contains(@class,'tpup-pnk-txt') and contains(text(),'Greater than')]")
    private WebElement lblInvalidTopUpValidation;

    @FindBy(xpath = "//div[contains(@class,'text-danger') and contains(text(),'Insufficient')]")
    private WebElement lblInsufficientBalanceValidation;

    @FindBy(xpath = "//div[contains(@class,'tppup-usr-nm')]")
    private WebElement lblTopUpUserNameInPopUp;

    @FindBy(xpath = "//div[contains(@class,'tppup-usr-no')]")
    private WebElement lblTopUpUserNumberInPopUp;

    @FindBy(xpath = "//div[contains(@class,'tpup-mnyscs-txt')]")
    private WebElement lblTopUpSuccessful;

    @FindBy(xpath = "//div[contains(@class,'amnt-txt')]")
    private WebElement lblTopUpAmountInPopUp;

    @FindBy(xpath = "//button[contains(@class,'btn-green') and text()='Close']")
    private WebElement btnCloseTopUpPopUp;

    public boolean verifyTopupAgentTargetDetails() {

        scrollToElement(driver, btnChartTotal);

        boolean bool = AgentDashboardIndexPage._topUpTargetCompleted == AgentTargetTopUpDetailsIndexPage._targetAchievedInTopUpDetails;

        System.out.println(AgentTargetTopUpDetailsIndexPage._daysCompletedInTopUpDetails + "   " + AgentTargetTopUpDetailsIndexPage._totalDaysInTopUpDetails
                + "   " + AgentTargetTopUpDetailsIndexPage._daysLeftInTopUpDetails);

        return bool && (AgentTargetTopUpDetailsIndexPage._daysLeftInTopUpDetails == AgentDashboardIndexPage._topUpDaysLeft) &&
                AgentTargetTopUpDetailsIndexPage._daysCompletedInTopUpDetails ==
                        (AgentTargetTopUpDetailsIndexPage._totalDaysInTopUpDetails - AgentDashboardIndexPage._topUpDaysLeft);

    }


    public boolean verifyConsumerDetailsList() {

        testStepsLog(_logStep++, "Click On Filter Button.");
        clickOn(driver, btnFilter);

        List<List<String>> consumerTableDetails = new LinkedList<>();

        try {
            scrollToElement(driver, findElementByName(driver, "dataTbl_length"));
            updateShowList(driver, findElementByName(driver, "dataTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View display.");
        }

        for (int con = 0; con < sizeOf(lstConsumerDetails); con++) {
            List<String> temp = new LinkedList<>();
            for (int num = 0; num < getInnerText(lstConsumerDetails.get(con)).split("\n").length; num++) {
                temp.add(getInnerText(lstConsumerDetails.get(con)).split("\n")[num]);
                if (num == 0) {
                    temp.add(getInnerText(lstConsumerNumber.get(con)).split(" ")[1]);
                }

            }

            consumerTableDetails.add(temp);
        }

        System.out.println(AgentDashboardIndexPage.consumersDetails);
        System.out.println(consumerTableDetails);

        return AgentDashboardIndexPage.consumersDetails.equals(consumerTableDetails);

    }

    public boolean verifyTopUpConsumerNameSorted() {

        List<String> consumerNameAfterSort = new ArrayList<>();
        List<String> consumerNameBeforeSort = AgentTargetTopUpDetailsIndexPage._consumerNameBeforeSort;

        for (WebElement name : lstTopUpConsumerName) {
            consumerNameAfterSort.add(getInnerText(name));
        }

        consumerNameBeforeSort.sort(
                (String o1, String o2) -> {
                    if (o1.equalsIgnoreCase(o2)) {
                        return o2.compareTo(o1);
                    }
                    return o1.toLowerCase().compareTo(o2.toLowerCase());
                }
        );

        System.out.println(consumerNameAfterSort);
        System.out.println(consumerNameBeforeSort);

        System.out.println(consumerNameBeforeSort.equals(consumerNameAfterSort));

        return consumerNameBeforeSort.equals(consumerNameAfterSort);
    }

    public boolean verifyTopUpConsumerNumberSorted() {

        List<String> consumerNumberAfterSort = new ArrayList<>();
        List<String> consumerNumberBeforeSort = AgentTargetTopUpDetailsIndexPage._consumerNumberBeforeSort;

        for (WebElement name : lstTopUpConsumerNumber) {
            consumerNumberAfterSort.add(getInnerText(name));
        }

        consumerNumberBeforeSort.sort(
                (String o1, String o2) -> {
                    if (o1.equalsIgnoreCase(o2)) {
                        return o2.compareTo(o1);
                    }
                    return o1.toLowerCase().compareTo(o2.toLowerCase());
                }
        );

        System.out.println(consumerNumberAfterSort);
        System.out.println(consumerNumberBeforeSort);

        System.out.println(consumerNumberBeforeSort.equals(consumerNumberAfterSort));

        return consumerNumberBeforeSort.equals(consumerNumberAfterSort);
    }

    public boolean verifyTopUpConsumerSearchSuccessful() {

        boolean bool = getInnerText(lstTopUpConsumerNumber.get(0)).
                contains(AgentTargetTopUpDetailsIndexPage._searchCriteria) &&
                sizeOf(lstTopUpConsumerNumber) == 1;

        type(txtInputSearch, "");

        return bool;

    }

    public boolean verifyTopUpValidationForInvalidSearch() {

        testValidationLog(getInnerText(lblNoRecordsFound));

        boolean bool = getInnerText(lblNoRecordsFound).equalsIgnoreCase(NO_SEARCH_RECORD_FOUND);

        type(txtInputSearch, "x");
        txtInputSearch.clear();

        return bool;
    }

    public boolean verifyConsumerFilterScreenDisplay() {

        updateShowList(driver, findElementByName(driver, "dataTbl_length"), "All");

        return isElementDisplay(btnFilterSearch) && isElementDisplay(btnFilterClearAll);
    }

    public boolean verifyRandomDataFiltered() {
        return getText(lblNoFilterConsumerAvailable).equalsIgnoreCase(NO_FILTER_DATA_AVAILABLE);
    }

    public boolean verifyNameFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement number : lstAddedConsumerNumber) {
            finalFilterResult.add(getInnerText(number));
        }

        System.out.println(finalFilterResult);
        System.out.println(AgentTargetTopUpDetailsIndexPage._filterConsumerPhone);

        return finalFilterResult.contains(AgentTargetTopUpDetailsIndexPage._filterConsumerPhone);
    }

    public boolean verifyPhoneNumberFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement number : lstAddedConsumerNumber) {
            finalFilterResult.add(getInnerText(number));
        }

        System.out.println(finalFilterResult);
        System.out.println(AgentTargetTopUpDetailsIndexPage._filterConsumerPhone);

        return finalFilterResult.contains(AgentTargetTopUpDetailsIndexPage._filterConsumerPhone);
    }

    public boolean verifyStatusFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement status : lstConsumerStatus) {
            finalFilterResult.add(getInnerText(status));
        }

        System.out.println(finalFilterResult);
        System.out.println(AgentTargetTopUpDetailsIndexPage._filterStatus);

        if (isListEmpty(finalFilterResult)) {
            testValidationLog(getText(lblNoFilterConsumerAvailable));
            return !AgentConsumerTargetDetailsIndexPage._consumerStatusBeforeSort.contains(AgentTargetTopUpDetailsIndexPage._filterStatus)
                    && getText(lblNoFilterConsumerAvailable).equalsIgnoreCase(NO_FILTER_DATA_AVAILABLE);
        } else {
            return finalFilterResult.stream().distinct().limit(2).count() <= 1 &&
                    finalFilterResult.contains(AgentTargetTopUpDetailsIndexPage._filterStatus);
        }
    }

    public boolean verifyValidationMessageForZeroTopUpAmount() {

        testValidationLog(getText(lblInvalidTopUpValidation));

        return isElementDisplay(lblInvalidTopUpValidation) &&
                getText(lblInvalidTopUpValidation).contains(CONSUMER_TOPUP_VALIDATION);

    }

    public boolean verifyValidationMessageForInsufficientBalanceTopUp() {

        try {
            testValidationLog(getText(lblInsufficientBalanceValidation));
        } catch (Exception e) {
            testValidationLog(getText(lblInvalidTopUpValidation));
        }

        return (getText(lblInsufficientBalanceValidation).contains(CONSUMER_INSUFFICIENT_BALANCE_TOPUP) ||
                getText(lblInvalidTopUpValidation).contains(CONSUMER_TOPUP_VALIDATION));

    }

    public boolean verifyConsumerTopUpDetailsAfterComplete() {

        testConfirmationLog(getInnerText(lblTopUpSuccessful));

        testInfoLog("User Name", getInnerText(lblTopUpUserNameInPopUp));
        testInfoLog("User Number", getInnerText(lblTopUpUserNumberInPopUp));
        testInfoLog("Top up Amount", getInnerText(lblTopUpAmountInPopUp));

        return getIntegerFromString(getInnerText(lblTopUpAmountInPopUp).replace("₹", "")) ==
                AgentDashboardIndexPage._topUpAmount && isElementDisplay(btnCloseTopUpPopUp);

    }
}
