package com.incashme.superAgent.validations;

/**
 * Created by Rahul R.
 * Date: 2019-03-29
 * Time: 14:40
 * Project Name: InCashMe
 */
public interface DashboardValidations {

    String FIRST_UNSUCCESSFUL_ATTEMPTS = "Unsuccessful attempt 1/3. Account locked temporarily after 3 attempts";
    String SECOND_UNSUCCESSFUL_ATTEMPTS = "Unsuccessful attempt 2/3. Account locked temporarily after 3 attempts";
    String THIRD_UNSUCCESSFUL_ATTEMPTS = "Unsuccessful attempt 3/3. Account locked temporarily for 40 mins.";
    String INVALID_NEW_PASSWORD = "Password should be minimum 8 characters. Must contain atleast 1 lowercase alphabetical character." +
            " Must contain atleast 1 uppercase alphabetical character. Must contain at least one special character." +
            " Must contain at least one number.";
    String NEW_CONFIRM_NOT_MATCHED = "New Password and Confirm Password does not match.";
    String PAST_PASSWORD_MESSAGE = "New Password cannot be same as the old Password.";
    String SUCCESSFUL_PASSWORD_CHANGE_MESSAGE = "Your password was changed successfully. Please login with the new password.";

    String NO_AGENT_COMMISSION_TODAY_MESSAGE = "Here is no data alive. Start adding target to earn Money.";
    String NO_TODAY_ADDED_AGENT_MESSAGE = "No agents found! Start adding Agents today to earn Money.";
    String NO_TOTAL_ADDED_AGENT_MESSAGE = "No data available in table";
    String NO_CREATED_AGENT_LIST_MESSAGE = "Here is no data alive.";

    String NO_AGENT_TARGET_STATUS = "No target Consumer Topups Status!";
    String NO_CONSUMER_TARGET_STATUS = "No Created New Consumer!";
    String NO_MERCHANT_TARGET_STATUS = "No Created new Merchant!";

    String NO_FILTER_DATA_AVAILABLE_DASHBOARD = "No data available in table";

    String NO_SEARCH_RECORD_FOUND = "No matching records found";

}
