package com.incashme.superAgent.validations;

/**
 * Created by Rahul R.
 * Date: 2019-03-19
 * Time:
 * Project Name: InCashMe
 */

public interface LoginValidations {

    String LOGIN_INVALID_EMAIL_ADDRESS = "Please Provide A Valid Email Address.";
    String LOGIN_BLANK_EMAIL_ADDRESS = "Email Is Required.";
    String LOGIN_BLANK_PASSWORD_ADDRESS = "Password Is Required.";
    String LOGIN_UNREGISTERED_EMAIL_ADDRESS = "Invalid Credentials. Please Try Again";

    String ACCOUNT_LOCKED_FOR_40_MINS = "Account locked temporarily. Please try again later after 40 mins.";

    String FORGOT_PASSWORD_OTP_CONFIRMATION_STEP_1 = "An OTP will be sent to the registered cellphone number associated with this account.";
    String FORGOT_PASSWORD_OTP_MISMATCH = "OTP Mismatch";
    String FORGOT_PASSWORD_OTP_CONFIRMATION_STEP_2 = "An Email has been sent to your registered mail with instructions to reset your password.";
    String FORGOT_PASSWORD_LINK_EXPIRED = "Invalid request. Your email link has expired.";

    String LOGIN_KYC_PENDING_MESSAGE = "Your Kyc Verification Is Pending";
    String FORGOT_PASSWORD_KYC_PENDING_MESSAGE = "Your Kyc Verification Is Pending";

    String INVALID_OTP_DIGIT_MESSAGE = "OTP should be 5 digit.";
    String INVALID_NEW_PASSWORD = "Password should be minimum 8 characters. Must contain atleast 1 lowercase alphabetical character." +
            " Must contain atleast 1 uppercase alphabetical character. Must contain at least one special character." +
            " Must contain at least one number.";
    String PAST_PASSWORD_MESSAGE = "Cannot Use A Past Password";
    String SUCCESSFUL_PASSWORD_RESET_MESSAGE = "Your password has been successfully changed";


}
