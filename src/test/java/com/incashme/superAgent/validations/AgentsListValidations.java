package com.incashme.superAgent.validations;

/**
 * Created by Rahul R.
 * Date: 2019-03-25
 * Time: 14:30
 * Project Name: InCashMe
 */
public interface AgentsListValidations {

    String NO_AGENTS_FOUND = "No Agent Lists !";
    String NO_TRANSACTION_FOUND = "No Transactions found !";
    String NO_FILTER_DATA_AVAILABLE = "No Agent Lists !";

    String FIRST_NAME_MORE_THAN_1_CHAR = "First Name should be greater than 1 characters long.";
    String FIRST_NAME_NUMERIC = "First Name should be Minimum 2 characters long and Maximum 20 characters and can only contain letters.";
    String FIRST_NAME_LESS_THAN_20_CHAR = "First Name should not be greater than 20 characters long.";
    String LAST_NAME_MORE_THAN_1_CHAR = "Last Name should be greater than 1 characters long.";
    String LAST_NAME_NUMERIC = "Last Name should be Minimum 2 characters long and Maximum 20 characters and can only contain letters.";
    String LAST_NAME_LESS_THAN_20_CHAR = "Last Name should not be greater than 20 characters long.";
    String EMAIL_ADDRESS_INVALID = "Please provide a valid email address.";
    String MOBILE_NUMBER_VALIDATION = "Phone Number should be 10 Digit";
    String INVALID_FILE_FORMAT_UPLOAD = "Please Upload files in png, jpg, jpeg format.";

    String OTP_MISMATCH = "OTP Mismatch";
    String INVALID_OTP_VALUE = "The otp must be between 1 and 5 digits.";

    String ADDRESS_MORE_THAN_50_CHAR = "Address should not be greater than 50 characters long.";
    String VILLAGE_MORE_THAN_50_CHAR = "Village should not be greater than 50 characters long.";
    String TEHSIL_MORE_THAN_50_CHAR = "Tehsil should not be greater than 50 characters long.";
    String DISTRICT_MORE_THAN_50_CHAR = "District should not be greater than 50 characters long.";
    String COUNTRY_NUMERIC_VALUES = "Country can only contain letter and spaces";
    String COUNTRY_LESS_THAN_1_CHAR = "Country should be Minimum 2 characters long";
    String COUNTRY_MORE_THAN_30_CHAR = "Country should be Maximum 30 character long.";
    String PIN_CODE_VALIDATION = "Pin Code should be 6 Digit";

    String NEW_MIN_CONSUMER_TARGET_VALIDATION = "Consumer Target should be greater than 0.";
    String NEW_MAX_CONSUMER_TARGET_VALIDATION = "Consumer Target should be less than 999.";
    String NEW_MIN_MERCHANT_TARGET_VALIDATION = "Merchant Target should be greater than 0.";
    String NEW_MAX_MERCHANT_TARGET_VALIDATION = "Merchant Target should be less than 999.";
    String MIN_TOPUP_AMOUNT_LESS_THAN_200 = "Minimum Top up Amount should be greater than 200";
    String MIN_TOPUP_AMOUNT_MORE_THAN_100001 = "Minimum Top up Amount should be less than 100001";
    String MIN_TOPUP_INVALID_AMOUNT = "Please enter a valid amount.";
    String MAX_TOPUP_AMOUNT_LESS_THAN_200 = "Maximum Top up Amount should be greater than 200";
    String MAX_TOPUP_AMOUNT_MORE_THAN_100001 = "Maximum Top up Amount should be less than 100001";
    String MAX_TOPUP_INVALID_AMOUNT = "Please enter a valid amount.";
    String MIN_TOPUP_MORE_THAN_MAX_TOPUP = "Minimum Topup Amount cannot be greater than Maximum Topup Amount.";

    String NO_SEARCH_RECORD_FOUND = "No matching records found";

    String AGENT_CREATED_SUCCESSFULLY = "Successfully Created !";
    String AGENT_CONFIRMATION_DETAILS = "Temporary Password sent to registered Email & Mobile Number.";

    String FILTER_DATE_VALIDATION = "Start date should be before End date!";

}
