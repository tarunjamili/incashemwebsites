package com.incashme.superAgent.verification;

import com.framework.init.AbstractPage;
import com.incashme.superAgent.indexpage.SuperAgentAgentListIndexPage;
import com.incashme.superAgent.indexpage.SuperAgentDashboardIndexPage;
import com.incashme.superAgent.validations.AgentsListValidations;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Rahul R.
 * Date: 2019-04-02
 * Time: 10:56
 * Project Name: InCashMe
 */
public class SuperAgentAgentListVerification extends AbstractPage implements AgentsListValidations {

    public SuperAgentAgentListVerification(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//div[@class='main-container']//h4")
    private List<WebElement> lblAgentList;

    @FindBy(xpath = "//div//p//preceding-sibling::h2")
    private WebElement lblNoAgents;

    @FindBy(xpath = "//div//h2[contains(text(),'No Agent')]")
    private WebElement lblNoFilterAgentsAvailable;

    @FindBy(xpath = "//div//p[contains(text(),'No Transactions')]")
    private WebElement lblNoTransactioHistory;

    @FindBy(xpath = "//li/button[contains(@class,'filterBtn')]")
    private WebElement btnFilter;

    @FindBy(xpath = "//table[@id='agentsTbl']")
    private WebElement agentsTable;

    @FindAll(value = {@FindBy(xpath = "//table[@id='agentsTbl']//tr//td[1]//h5")})
    private List<WebElement> lstAgentName;

    @FindBy(xpath = "//div[contains(@class,'spr-usr-name')]")
    private WebElement lblAgentNameInProfile;

    @FindBy(xpath = "//div[contains(@class,'spr-usr-id')]")
    private WebElement lblAgentIDInProfile;

    @FindBy(xpath = "//div[contains(@class,'spr-usr-sts')]")
    private WebElement lblAgentJoinedInProfile;

    @FindBy(xpath = "//form//div//input[@id='adf']")
    private WebElement lblAdhaarFront;

    @FindBy(xpath = "//form//div//input[@id='fname']")
    private WebElement lblFirstName;

    @FindBy(xpath = "//form//div//input[@id='lname']")
    private WebElement lblLastName;

    @FindBy(xpath = "//form//div//input[@id='phno']")
    private WebElement lblMobileNumber;

    @FindBy(xpath = "//form//div//input[@id='status']")
    private WebElement lblAccountStatus;

    @FindAll(value = {@FindBy(xpath = "//div[@id='TargetsInfo']//label[contains(text(),'Status')]")})
    private List<WebElement> lstAgentTopUpStatus;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table2']//tr//td[3]")})
    private List<WebElement> lstCommissionAmount;

    @FindAll(value = {@FindBy(xpath = "//div[@id='TargetsInfo']//label[contains(text(),'Top up')]")})
    private List<WebElement> lstTopUpAmount;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//button[contains(@class,'searchbtn')]")
    private WebElement btnFilterSearch;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//a[contains(text(),'Clear All')]")
    private WebElement btnFilterClearAll;

    @FindAll(value = {@FindBy(xpath = "//table[@id='agentsTbl']//tr//td[2]//div")})
    private List<WebElement> lstAgentMobileNumber;

    @FindAll(value = {@FindBy(xpath = "//table[@id='agentsTbl']//tr//td[3]//div")})
    private List<WebElement> lstAgentStatus;

    @FindAll(value = {@FindBy(xpath = "//table[@id='agentsTbl']//tr//td[5]//div")})
    private List<WebElement> lstAgentCreatedDateTime;

    @FindAll(value = {@FindBy(xpath = "//table[@id='agentsTbl']//tr//td[1]//following-sibling::div")})
    private List<WebElement> lstAgentId;

    @FindBy(xpath = "//input[@formcontrolname='first_name']")
    private WebElement txtFirstName;

    @FindBy(xpath = "//input[@formcontrolname='last_name']")
    private WebElement txtLastName;

    @FindBy(xpath = "//input[contains(@placeholder,'Date of Birth')]")
    private WebElement txtDOBPicker;

    @FindBy(xpath = "//input[@formcontrolname='email']")
    private WebElement txtEmailAddress;

    @FindBy(xpath = "//input[@formcontrolname='phone']")
    private WebElement txtPhoneNumber;

    @FindBy(xpath = "//div[@id='step1']//button[contains(text(),'Next')]")
    private WebElement btnNextStep1;

    @FindBy(xpath = "//div[@id='step2']//button[contains(text(),'Next')]")
    private WebElement btnNextStep2;

    @FindBy(xpath = "//div[@id='step3']//button[contains(text(),'Next')]")
    private WebElement btnNextStep3;

    @FindBy(xpath = "//div[@id='step2']//button[contains(text(),'Previous')]")
    private WebElement btnPreviousStep2;

    @FindAll(value = {@FindBy(xpath = "//div[@class='input-group']")})
    private List<WebElement> lstFileUploads;

    @FindBy(xpath = "//input[@id='profile_pic']")
    private WebElement btnUploadProfilePicture;

    @FindBy(xpath = "//input[@id='aadhaar_front']")
    private WebElement btnUploadAdhaarFront;

    @FindBy(xpath = "//input[@id='aadhaar_back']")
    private WebElement btnUploadAdhharBack;

    @FindBy(xpath = "//small[@class='text-danger' and contains(text(),'First Name')]")
    private WebElement lblFirstNameValidation;

    @FindBy(xpath = "//small[@class='text-danger' and contains(text(),'Last Name')]")
    private WebElement lblLastNameValidation;

    @FindBy(xpath = "//small[@class='text-danger' and contains(text(),'valid email')]")
    private WebElement lblEmailValidation;

    @FindBy(xpath = "//small[@class='text-danger' and contains(text(),'Address should not')]")
    private WebElement lblAddressValidation;

    @FindBy(xpath = "//small[@class='text-danger' and contains(text(),'Village should not')]")
    private WebElement lblVillageValidation;

    @FindBy(xpath = "//small[@class='text-danger' and contains(text(),'Tehsil should not')]")
    private WebElement lblTehsilValidation;

    @FindBy(xpath = "//small[@class='text-danger' and contains(text(),'District should not')]")
    private WebElement lblDistrictValidation;

    @FindBy(xpath = "//small[@class='text-danger' and contains(text(),'Country')]")
    private WebElement lblCountryValidation;

    @FindBy(xpath = "//small[@class='text-danger' and contains(text(),'Phone Number')]")
    private WebElement lblMobileValidation;

    @FindBy(xpath = "//small[@class='text-danger' and contains(text(),'Pin Code')]")
    private WebElement lblPinCodeValidation;

    @FindBy(xpath = "//span[contains(@class,'text-danger') and contains(text(),'OTP Mismatch')]")
    private WebElement lblOTPMisMatchValidation;

    @FindBy(xpath = "//span[contains(@class,'text-danger') and contains(text(),'must be between')]")
    private WebElement lblOTPInvalidValidation;

    @FindBy(xpath = ".//div[contains(@id,'swal2-content') and contains(text(),'Please Upload files')]")
    private WebElement lblInvalidFileUploadValidation;

    @FindBy(xpath = ".//div[contains(@class,'swal2-actions')]//button[text()='OK']")
    private WebElement btnOTPConfirmOK;

    @FindBy(xpath = ".//span[@id='profile_pic_select']")
    private WebElement lblProfilePicSelected;

    @FindBy(xpath = ".//span[@id='aadhaar_front_select']")
    private WebElement lblAdhaarFrontSelected;

    @FindBy(xpath = ".//span[@id='aadhaar_back_select']")
    private WebElement lblAdhaarBackSelected;

    @FindBy(xpath = ".//input[@id='otp_val']")
    private WebElement txtStep2OTP;

    @FindBy(xpath = "//input[@formcontrolname='address1']")
    private WebElement txtHomeFlatAddress;

    @FindBy(xpath = "//input[@formcontrolname='address2']")
    private WebElement txtStreet1Address;

    @FindBy(xpath = "//input[@formcontrolname='address3']")
    private WebElement txtStreet2Address;

    @FindBy(xpath = "//input[@formcontrolname='village']")
    private WebElement txtVillage;

    @FindBy(xpath = "//input[@formcontrolname='tehsil']")
    private WebElement txtTehsil;

    @FindBy(xpath = "//input[@formcontrolname='district']")
    private WebElement txtDistrict;

    @FindBy(xpath = "//input[@formcontrolname='country']")
    private WebElement txtCountry;

    @FindBy(xpath = "//input[@formcontrolname='pincode']")
    private WebElement txtPinCode;

    @FindBy(xpath = "//select[@formcontrolname='state']")
    private WebElement btnState;

    @FindBy(xpath = "//select[@formcontrolname='city']")
    private WebElement btnCity;

    @FindBy(xpath = "//input[@formcontrolname='target_consumer']")
    private WebElement txtTargetNewConsumer;

    @FindBy(xpath = "//input[@formcontrolname='target_merchant']")
    private WebElement txtTargetNewMerchant;

    @FindBy(xpath = "//input[@formcontrolname='min_topup_amount']")
    private WebElement txtMinTopUpAmount;

    @FindBy(xpath = "//input[@formcontrolname='max_topup_amount']")
    private WebElement txtMaxTopUpAmount;

    @FindBy(xpath = "//div[@id='step4']//button[contains(text(),'Cancel')]")
    private WebElement btnCancel;

    @FindBy(xpath = "//div[@id='step4']//button[contains(text(),'Create')]")
    private WebElement btnCreateAgent;

    @FindBy(xpath = "//small[@class='text-danger' and contains(text(),'greater than 0.')]")
    private WebElement lblZeroTargetValidation;

    @FindBy(xpath = "//small[@class='text-danger' and contains(text(),'less than 999.')]")
    private WebElement lblInvalidTargetValidation;

    @FindBy(xpath = "//small[@class='text-danger' and contains(text(),'greater than 200')]")
    private WebElement lblMinTopUpValidation;

    @FindBy(xpath = "//small[@class='text-danger' and contains(text(),'less than 100001')]")
    private WebElement lblMaxTopUpValidation;

    @FindBy(xpath = "//small[@class='text-danger' and contains(text(),'valid amount')]")
    private WebElement lblInvalidTopUpValidation;

    @FindBy(xpath = ".//div[contains(@id,'swal2-content') and contains(text(),'cannot be greater than')]")
    private WebElement lblMaxLessThanMinTopUp;

    @FindBy(xpath = "//div[contains(text(),'Added New Agent')]//following-sibling::div")
    private WebElement lblAddedNewAgent;

    @FindBy(xpath = "//input[@type='search']")
    private WebElement txtInputSearch;

    @FindBy(xpath = "//table//tr//td[contains(text(),'matching')]")
    private WebElement lblNoRecordsFound;

    @FindBy(xpath = "//div[contains(@class,'trnfr-tpup-txt')]")
    private WebElement lblAgentCreateSuccessMessage;

    @FindBy(xpath = "//div[contains(@class,'tpup-mnyscs-txt')]")
    private WebElement lblAgentDetailsMessage;

    @FindBy(xpath = "//div[contains(@class,'modal-content')]//button[contains(@class,'btn-green') and text()='Done']")
    private WebElement btnDone;

    @FindBy(xpath = ".//span[@id='buisness_pan_select']")
    private WebElement lblBusinessPANSelected;

    @FindBy(xpath = ".//span[@id='buisness_addr_proof_select']")
    private WebElement lblBusinessAddressProofSelected;

    @FindBy(xpath = ".//div[contains(@id,'swal2-content') and contains(text(),'before End date')]")
    private WebElement lblInvalidDateFilterValidation;

    @FindBy(xpath = "//div[@class='input-group']//select[@id='fileType']")
    private WebElement selectFileType;

    public boolean verifyAgentsListScreen() {

        if (sizeOf(lblAgentList) == 1) {
            testValidationLog(getText(lblNoAgents));
            return getText(lblNoAgents).equalsIgnoreCase(NO_AGENTS_FOUND) && isElementDisplay(btnFilter);
        } else {
            return isElementDisplay(btnFilter) && isElementDisplay(agentsTable);
        }
    }

    public boolean verifyTotalAgentCountFromDashboard() {

        pause(3);

        try {
            updateShowList(driver, findElementByName(driver, "agentsTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        if (isListEmpty(lstAgentName)) {
            testValidationLog(getText(lblNoAgents));
            return getText(lblNoAgents).equalsIgnoreCase(NO_AGENTS_FOUND) && isElementDisplay(btnFilter);
        } else {
            return sizeOf(lstAgentName) == SuperAgentDashboardIndexPage._totalAddedAgent;
        }

    }

    public boolean verifyAgentPersonalInfo() {

        boolean bool = getInnerText(lblAgentNameInProfile).equalsIgnoreCase(SuperAgentAgentListIndexPage._agentName) &&
                getInnerText(lblAgentIDInProfile).equalsIgnoreCase(SuperAgentAgentListIndexPage._agentId) &&
                getInnerText(lblAgentJoinedInProfile).contains(SuperAgentAgentListIndexPage._agentCreatedDate);

        System.out.println(getInnerText(lblAgentNameInProfile) + " " + SuperAgentAgentListIndexPage._agentName);
        System.out.println(getInnerText(lblAgentIDInProfile) + " " + SuperAgentAgentListIndexPage._agentId);
        System.out.println(getInnerText(lblAgentJoinedInProfile) + " " + SuperAgentAgentListIndexPage._agentCreatedDate);

        return bool;
    }

    public boolean verifyAgentTaskInfo() {

        scrollToElement(driver, lstTopUpAmount.get(lastIndexOf(lstTopUpAmount)));

        pause(3);

        return isElementDisplay(lstAgentTopUpStatus.get(0)) && isElementDisplay(lstAgentTopUpStatus.get(1))
                && isElementDisplay(lstAgentTopUpStatus.get(2));
    }

    public boolean verifyAgentHistoryInfo() {

        pause(3);

        double totalCommissionByAgent = 0.0;

        try {
            updateShowList(driver, findElementByName(driver, "trans-history-table2_length"), "All");
        } catch (Exception e) {
            scrollToElement(driver, lblNoTransactioHistory);
        }

        if (!isElementPresent(lblNoTransactioHistory)) {

            for (WebElement commission : lstCommissionAmount) {
                totalCommissionByAgent += getDoubleFromString(getInnerText(commission));
            }

            testInfoLog("Total Commission of Agent", String.valueOf(formatTwoDecimal(totalCommissionByAgent)));

            System.out.println(formatTwoDecimal(totalCommissionByAgent));

            return !isElementPresent(lblNoTransactioHistory);

        } else {
            testValidationLog(getText(lblNoTransactioHistory));
            return getInnerText(lblNoTransactioHistory).equalsIgnoreCase(NO_TRANSACTION_FOUND);
        }
    }

    public boolean verifyFilterScreenDisplay() {

        updateShowList(driver, findElementByName(driver, "agentsTbl_length"), "All");

        return isElementDisplay(btnFilterSearch) && isElementDisplay(btnFilterClearAll);
    }

    public boolean verifyRandomDataFiltered() {
        return getText(lblNoFilterAgentsAvailable).equalsIgnoreCase(NO_FILTER_DATA_AVAILABLE);
    }

    public boolean verifyNameFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement number : lstAgentMobileNumber) {
            finalFilterResult.add(getInnerText(number));
        }

        System.out.println(finalFilterResult);
        System.out.println(SuperAgentAgentListIndexPage._filterAgentPhone);

        return finalFilterResult.contains(SuperAgentAgentListIndexPage._filterAgentPhone);
    }

    public boolean verifyIdFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement id : lstAgentId) {
            finalFilterResult.add(getInnerText(id));
        }

        System.out.println(finalFilterResult);
        System.out.println(SuperAgentAgentListIndexPage._filterAgentId);

        return finalFilterResult.contains(SuperAgentAgentListIndexPage._filterAgentId);
    }

    public boolean verifyPhoneNumberFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement number : lstAgentMobileNumber) {
            finalFilterResult.add(getInnerText(number));
        }

        System.out.println(finalFilterResult);
        System.out.println(SuperAgentAgentListIndexPage._filterAgentPhone);

        return finalFilterResult.contains(SuperAgentAgentListIndexPage._filterAgentPhone);
    }

    public boolean verifyStatusFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement status : lstAgentStatus) {
            finalFilterResult.add(getInnerText(status));
        }

        System.out.println(finalFilterResult);
        System.out.println(SuperAgentAgentListIndexPage._filterStatus);

        if (isListEmpty(finalFilterResult)) {
            testValidationLog(getText(lblNoFilterAgentsAvailable));
            return !SuperAgentAgentListIndexPage._agentStatusBeforeSort.contains(SuperAgentAgentListIndexPage._filterStatus)
                    && getText(lblNoFilterAgentsAvailable).equalsIgnoreCase(NO_FILTER_DATA_AVAILABLE);
        } else {
            return finalFilterResult.stream().distinct().limit(2).count() <= 1 &&
                    finalFilterResult.contains(SuperAgentAgentListIndexPage._filterStatus);
        }
    }

    public boolean verifyAgentNameSorted() {

        List<String> agentNameAfterSort = new ArrayList<>();
        List<String> agentNameBeforeSort = SuperAgentAgentListIndexPage._agentNamesBeforeSort;

        for (WebElement name : lstAgentName) {
            agentNameAfterSort.add(getInnerText(name));
        }

        agentNameBeforeSort.sort(
                (String o1, String o2) -> {
                    if (o1.equalsIgnoreCase(o2)) {
                        return o2.compareTo(o1);
                    }
                    return o1.toLowerCase().compareTo(o2.toLowerCase());
                }
        );

        System.out.println(agentNameAfterSort);
        System.out.println(agentNameBeforeSort);

        System.out.println(agentNameBeforeSort.equals(agentNameAfterSort));

        return agentNameBeforeSort.equals(agentNameAfterSort);
    }

    public boolean verifyAgentNumberSorted() {

        List<String> agentNumberAfterSort = new ArrayList<>();
        List<String> agentNumberBeforeSort = SuperAgentAgentListIndexPage._agentNumberBeforeSort;

        for (WebElement name : lstAgentMobileNumber) {
            agentNumberAfterSort.add(getInnerText(name));
        }

        agentNumberBeforeSort.sort(
                (String o1, String o2) -> {
                    if (o1.equalsIgnoreCase(o2)) {
                        return o2.compareTo(o1);
                    }
                    return o1.toLowerCase().compareTo(o2.toLowerCase());
                }
        );

        System.out.println(agentNumberAfterSort);
        System.out.println(agentNumberBeforeSort);

        System.out.println(agentNumberBeforeSort.equals(agentNumberAfterSort));

        return agentNumberBeforeSort.equals(agentNumberAfterSort);
    }

    public boolean verifyAgentStatusSorted() {

        List<String> agentStatusAfterSort = new ArrayList<>();
        List<String> agentStatusBeforeSort = SuperAgentAgentListIndexPage._agentStatusBeforeSort;

        for (WebElement name : lstAgentStatus) {
            agentStatusAfterSort.add(getInnerText(name));
        }

        agentStatusBeforeSort.sort(
                (String o1, String o2) -> {
                    if (o1.equalsIgnoreCase(o2)) {
                        return o2.compareTo(o1);
                    }
                    return o1.toLowerCase().compareTo(o2.toLowerCase());
                }
        );

        System.out.println(agentStatusAfterSort);
        System.out.println(agentStatusBeforeSort);

        System.out.println(agentStatusBeforeSort.equals(agentStatusAfterSort));

        return agentStatusBeforeSort.equals(agentStatusAfterSort);
    }


    public boolean verifyAgentDateTimeSorted() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        List<LocalDateTime> agentDateTimeAfterSort = new ArrayList<>();
        List<LocalDateTime> agentDateTimeBeforeSort = SuperAgentAgentListIndexPage._agentDateTimeBeforeSort;

        for (WebElement name : lstAgentCreatedDateTime) {
            agentDateTimeAfterSort.add(LocalDateTime.parse(getInnerText(name), format));
        }

        Collections.sort(agentDateTimeBeforeSort);

        System.out.println(agentDateTimeAfterSort);
        System.out.println(agentDateTimeBeforeSort);

        System.out.println(agentDateTimeBeforeSort.equals(agentDateTimeAfterSort));

        return agentDateTimeBeforeSort.equals(agentDateTimeAfterSort);
    }

    public boolean verifyCreateAgentScreen() {

        pause(2);

        boolean bool = isElementDisplay(txtFirstName) && isElementDisplay(txtLastName) &&
                isElementDisplay(txtDOBPicker) && isElementDisplay(txtEmailAddress) &&
                isElementDisplay(txtPhoneNumber);

        scrollElement(btnNextStep1);

        return bool && isElementDisplay(lstFileUploads.get(0)) && isElementDisplay(selectFileType) &&
                isElementDisplay(btnNextStep1);
    }

    public boolean verifyMinCharName(String field) {

        if (field.equalsIgnoreCase("Last Name")) {
            testValidationLog(getText(lblLastNameValidation));
            return getText(lblLastNameValidation).contains(LAST_NAME_MORE_THAN_1_CHAR);
        } else {
            testValidationLog(getText(lblFirstNameValidation));
            return getText(lblFirstNameValidation).contains(FIRST_NAME_MORE_THAN_1_CHAR);
        }

    }

    public boolean verifyMaxCharName(String field) {

        if (field.equalsIgnoreCase("Last Name")) {
            testValidationLog(getText(lblLastNameValidation));
            return getText(lblLastNameValidation).contains(LAST_NAME_LESS_THAN_20_CHAR);
        } else {
            testValidationLog(getText(lblFirstNameValidation));
            return getText(lblFirstNameValidation).contains(FIRST_NAME_LESS_THAN_20_CHAR);
        }

    }

    public boolean verifyNumericName(String field) {

        if (field.equalsIgnoreCase("Last Name")) {
            testValidationLog(getText(lblLastNameValidation));
            return getText(lblLastNameValidation).contains(LAST_NAME_NUMERIC);
        } else {
            testValidationLog(getText(lblFirstNameValidation));
            return getText(lblFirstNameValidation).contains(FIRST_NAME_NUMERIC);
        }

    }

    public boolean verifyNoValidationName(String field) {
        return field.equalsIgnoreCase("Last Name") ?
                !isElementPresent(lblLastNameValidation) :
                !isElementPresent(lblFirstNameValidation);
    }

    public boolean verifyDOBSelected() {
        return txtDOBPicker.getAttribute("ng-reflect-model").
                equalsIgnoreCase(SuperAgentAgentListIndexPage._newAgentDOB);

    }

    public boolean verifyInvalidEmailValidation() {
        testValidationLog(getText(lblEmailValidation));
        return getText(lblEmailValidation).contains(EMAIL_ADDRESS_INVALID);
    }

    public boolean verifyNoValidationEmail() {
        return !isElementPresent(lblEmailValidation);
    }

    public boolean verifyNoValidationAddress() {
        return !SuperAgentAgentListIndexPage._isRandomBoolean || !isElementPresent(lblAddressValidation);
    }

    public boolean verifyInvalidMobileNumberValidation() {
        testValidationLog(getText(lblMobileValidation));
        return getText(lblMobileValidation).contains(MOBILE_NUMBER_VALIDATION);
    }

    public boolean verifyNoValidationMobile() {
        return !isElementPresent(lblMobileValidation);
    }

    public boolean verifyInvalidFileUpload() {

        boolean bool = getText(lblInvalidFileUploadValidation).equalsIgnoreCase(INVALID_FILE_FORMAT_UPLOAD);

        testValidationLog(getText(lblInvalidFileUploadValidation));
        clickOn(driver, btnOTPConfirmOK);

        return isElementDisplay(txtEmailAddress) && bool;

    }

    public boolean verifyProfilePicSelected() {
        return getText(lblProfilePicSelected).equalsIgnoreCase("Selected");
    }

    public boolean verifyAdhaarFrontSelected() {
        return getText(lblAdhaarFrontSelected).equalsIgnoreCase("Selected");
    }

    public boolean verifyAdhaarBackSelected() {
        return getText(lblAdhaarBackSelected).equalsIgnoreCase("Selected");
    }

    public boolean verifyStep2OTPScreenDisplay() {
        pause(3);
        return isElementDisplay(txtStep2OTP) && isElementDisplay(btnNextStep2) &&
                isElementDisplay(btnPreviousStep2);

    }

    public boolean verifyOTPMisMatchValidation() {
        testValidationLog(getText(lblOTPMisMatchValidation));
        return getText(lblOTPMisMatchValidation).equalsIgnoreCase(OTP_MISMATCH);
    }

    public boolean verifyInvalidOTPValidation() {
        testValidationLog(getText(lblOTPInvalidValidation));
        return getText(lblOTPInvalidValidation).equalsIgnoreCase(INVALID_OTP_VALUE);
    }

    public boolean verifyStep3ScreenDisplay() {

        pause(2);

        boolean bool = isElementDisplay(txtHomeFlatAddress) && isElementDisplay(txtStreet1Address) &&
                isElementDisplay(txtStreet2Address) && isElementDisplay(txtVillage) &&
                isElementDisplay(txtTehsil) && isElementDisplay(txtDistrict) &&
                isElementDisplay(btnState) && isElementDisplay(btnCity) && isElementDisplay(txtCountry);

        System.out.println(bool);

        scrollToElement(driver, btnNextStep3);

        return bool && isElementDisplay(btnNextStep3) && isElementDisplay(txtPinCode);

    }

    public boolean verifyInvalidAddress() {
        testValidationLog(getText(lblAddressValidation));
        return getText(lblAddressValidation).contains(ADDRESS_MORE_THAN_50_CHAR);
    }

    public boolean verifyInvalidVillageAddress() {
        testValidationLog(getText(lblVillageValidation));
        return getText(lblVillageValidation).contains(VILLAGE_MORE_THAN_50_CHAR);
    }

    public boolean verifyInvalidTehsilAddress() {
        testValidationLog(getText(lblTehsilValidation));
        return getText(lblTehsilValidation).contains(TEHSIL_MORE_THAN_50_CHAR);
    }

    public boolean verifyInvalidDistrictAddress() {
        testValidationLog(getText(lblDistrictValidation));
        return getText(lblDistrictValidation).contains(DISTRICT_MORE_THAN_50_CHAR);
    }

    public boolean verifyNumericCountryValidation() {
        testValidationLog(getText(lblCountryValidation));
        return getText(lblCountryValidation).contains(COUNTRY_NUMERIC_VALUES);
    }

    public boolean verifyCountryMinCharValidation() {
        testValidationLog(getText(lblCountryValidation));
        return getText(lblCountryValidation).contains(COUNTRY_LESS_THAN_1_CHAR);
    }

    public boolean verifyCountryMaxCharValidation() {
        testValidationLog(getText(lblCountryValidation));
        return getText(lblCountryValidation).contains(COUNTRY_MORE_THAN_30_CHAR);
    }

    public boolean verifyNoValidationCountry() {
        return !isElementPresent(lblCountryValidation);
    }

    public boolean verifyInvalidPinCodeValidation() {
        testValidationLog(getText(lblPinCodeValidation));
        return getText(lblPinCodeValidation).equalsIgnoreCase(PIN_CODE_VALIDATION);
    }

    public boolean verifyNoValidationPinCode() {
        return !isElementPresent(lblPinCodeValidation);
    }

    public boolean verifyStep4ScreenDisplay() {

        pause(2);

        boolean bool = isElementDisplay(txtTargetNewConsumer) && isElementDisplay(txtTargetNewMerchant) &&
                isElementDisplay(txtMinTopUpAmount) && isElementDisplay(txtMaxTopUpAmount);

        System.out.println(bool);

        scrollToElement(driver, btnCancel);

        return bool && isElementDisplay(btnCancel) && isElementDisplay(btnCreateAgent);

    }

    public boolean verifyMinNewConsumerTarget() {
        testValidationLog(getText(lblZeroTargetValidation));
        return getText(lblZeroTargetValidation).contains(NEW_MIN_CONSUMER_TARGET_VALIDATION);
    }

    public boolean verifyInvalidNewConsumerTarget() {
        testValidationLog(getText(lblInvalidTargetValidation));
        return getText(lblInvalidTargetValidation).contains(NEW_MAX_CONSUMER_TARGET_VALIDATION);
    }

    public boolean verifyMinNewMerchantTarget() {
        testValidationLog(getText(lblZeroTargetValidation));
        return getText(lblZeroTargetValidation).contains(NEW_MIN_MERCHANT_TARGET_VALIDATION);
    }

    public boolean verifyInvalidNewMerchantTarget() {
        testValidationLog(getText(lblInvalidTargetValidation));
        return getText(lblInvalidTargetValidation).contains(NEW_MAX_MERCHANT_TARGET_VALIDATION);
    }

    public boolean verifyNoValidationNewTarget() {
        return !isElementPresent(lblInvalidTargetValidation);
    }

    public boolean verifyMinMinimumTopUpValidation() {
        testValidationLog(getText(lblMinTopUpValidation));
        return getText(lblMinTopUpValidation).contains(MIN_TOPUP_AMOUNT_LESS_THAN_200);
    }

    public boolean verifyMaxMinimumTopUpValidation() {
        testValidationLog(getText(lblMaxTopUpValidation));
        return getText(lblMaxTopUpValidation).contains(MIN_TOPUP_AMOUNT_MORE_THAN_100001);
    }

    public boolean verifyInvalidMinimumTopUpValidation() {
        testValidationLog(getText(lblInvalidTopUpValidation));
        return getText(lblInvalidTopUpValidation).contains(MIN_TOPUP_INVALID_AMOUNT);
    }

    public boolean verifyMinMaximumTopUpValidation() {
        testValidationLog(getText(lblMinTopUpValidation));
        return getText(lblMinTopUpValidation).contains(MAX_TOPUP_AMOUNT_LESS_THAN_200);
    }

    public boolean verifyMaxMaximumTopUpValidation() {
        testValidationLog(getText(lblMaxTopUpValidation));
        return getText(lblMaxTopUpValidation).contains(MAX_TOPUP_AMOUNT_MORE_THAN_100001);
    }

    public boolean verifyInvalidMaximumTopUpValidation() {
        testValidationLog(getText(lblInvalidTopUpValidation));
        return getText(lblInvalidTopUpValidation).contains(MAX_TOPUP_INVALID_AMOUNT);
    }

    public boolean verifyMaxLessThanMinAmountValidation() {

        boolean bool = getText(lblMaxLessThanMinTopUp).equalsIgnoreCase(MIN_TOPUP_MORE_THAN_MAX_TOPUP);

        testValidationLog(getText(lblMaxLessThanMinTopUp));
        clickOn(driver, btnOTPConfirmOK);

        return isElementDisplay(btnCreateAgent) && bool;
    }

    public boolean verifyAgentCreatedSuccessfully() {

        pause(5);

        testConfirmationLog(getText(lblAgentCreateSuccessMessage) + "<br>" + getText(lblAgentDetailsMessage));

        boolean bool = getText(lblAgentDetailsMessage).equalsIgnoreCase(AGENT_CONFIRMATION_DETAILS) &&
                getText(lblAgentCreateSuccessMessage).equalsIgnoreCase(AGENT_CREATED_SUCCESSFULLY) &&
                isElementDisplay(btnDone);

        clickOn(driver, btnDone);

        return bool;

    }


    public boolean verifyCancelCreateNewAgent() {

        pause(3);

        int todayAddedAgent = getIntegerFromString(getText(lblAddedNewAgent));
        return SuperAgentDashboardIndexPage._todayAddedAgent == todayAddedAgent;
    }


    public boolean verifySearchSuccessful() {

        boolean bool = true;

        for (WebElement e : lstAgentId) {
            bool = bool && getInnerText(e).contains(SuperAgentAgentListIndexPage._searchCriteria);
        }

        type(txtInputSearch, "");

        return bool;

    }

    public boolean verifyValidationForInvalidSearch() {

        testValidationLog(getInnerText(lblNoRecordsFound));

        boolean bool = getInnerText(lblNoRecordsFound).equalsIgnoreCase(NO_SEARCH_RECORD_FOUND);

        type(txtInputSearch, "x");
        txtInputSearch.clear();

        return bool;
    }

    public boolean verifyCurrentAgentInForDisplayProperly() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        boolean bool = SuperAgentAgentListIndexPage._agentName.
                equals(SuperAgentAgentListIndexPage._newAgentFirstName + " " + SuperAgentAgentListIndexPage._newAgentLastName);

        bool = bool && SuperAgentAgentListIndexPage._agentMobileNumber.
                contains(SuperAgentAgentListIndexPage._newAgentMobileNumber);

        bool = bool && SuperAgentAgentListIndexPage._agentAccountStatus.equals("Kyc Pending") &&
                SuperAgentAgentListIndexPage._agentConsumerTopUpStatus.equals("Not Available") &&
                SuperAgentAgentListIndexPage._agentMerchantTargetStatus.equals("Not Available") &&
                SuperAgentAgentListIndexPage._agentConsumerTargetStatus.equals("Not Available");

        List<String> lstExpectedTimes = new ArrayList<>();

        for (int sec = 1; sec <= 20; sec++) {
            lstExpectedTimes.add((LocalDateTime.parse(SuperAgentAgentListIndexPage._newAgentCreatedDateTime, format)
                    .plusSeconds(sec)).format(format).trim());
        }

        System.out.println(lstExpectedTimes);
        System.out.println(SuperAgentAgentListIndexPage._agentCreatedDateTime);

        return bool && lstExpectedTimes.contains(SuperAgentAgentListIndexPage._agentCreatedDateTime);

    }

    public boolean verifyAgentPersonalInfoForCreatedAgent() {

        boolean bool = getInnerText(lblAgentNameInProfile).equalsIgnoreCase(SuperAgentAgentListIndexPage._newAgentFirstName +
                " " + SuperAgentAgentListIndexPage._newAgentLastName) &&
                getInnerText(lblAgentIDInProfile).equalsIgnoreCase(SuperAgentAgentListIndexPage._agentId) &&
                getInnerText(lblAgentJoinedInProfile).contains(SuperAgentAgentListIndexPage._agentCreatedDate);

        System.out.println(getInnerText(lblAgentNameInProfile) + " " + SuperAgentAgentListIndexPage._agentName);
        System.out.println(getInnerText(lblAgentIDInProfile) + " " + SuperAgentAgentListIndexPage._agentId);
        System.out.println(getInnerText(lblAgentJoinedInProfile) + " " + SuperAgentAgentListIndexPage._agentCreatedDate);

        return bool;
    }

    public boolean verifyBusinessPANSelected() {
        return getText(lblBusinessPANSelected).equalsIgnoreCase("Selected");
    }

    public boolean verifyBusinessAddressProofSelected() {
        return getText(lblBusinessAddressProofSelected).equalsIgnoreCase("Selected");
    }

    public boolean verifyInvalidDateFilter() {

        boolean bool = getText(lblInvalidDateFilterValidation).equalsIgnoreCase(FILTER_DATE_VALIDATION);

        testValidationLog(getText(lblInvalidDateFilterValidation));
        clickOn(driver, btnOTPConfirmOK);

        return bool;

    }

    public boolean verifyDateFilteredSuccessfully() {

        return true;

    }
}