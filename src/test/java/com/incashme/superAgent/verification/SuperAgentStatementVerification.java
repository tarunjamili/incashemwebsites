package com.incashme.superAgent.verification;

import com.framework.init.AbstractPage;
import com.incashme.superAgent.indexpage.SuperAgentStatementIndexPage;
import com.incashme.superAgent.validations.StatementsValidations;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.util.List;

/**
 * Created by Rahul R.
 * Date: 2019-03-29
 * Time: 15:52
 * Project Name: InCashMe
 */
public class SuperAgentStatementVerification extends AbstractPage implements StatementsValidations {

    public SuperAgentStatementVerification(WebDriver driver) {
        super(driver);
    }

    @FindAll(value = {@FindBy(xpath = "//div[@class='stmnt-cntnt']")})
    private List<WebElement> lstStatements;

    @FindAll(value = {@FindBy(xpath = "//div[@class='pst-dt']")})
    private List<WebElement> lstStatementMonth;

    @FindBy(xpath = "//div//p[contains(text(),'No Statement')]")
    private WebElement lblNoStatement;

    @FindBy(xpath = "//li/button[contains(@class,'filterBtn')]")
    private WebElement btnFilter;

    public boolean verifyStatementsScreen() {

        if (isListEmpty(lstStatements)) {
            testInfoLog("Total Statements", String.valueOf(sizeOf(lstStatements)));
            testValidationLog(getText(lblNoStatement));
            return isElementDisplay(lblNoStatement) && isElementDisplay(btnFilter) &&
                    getText(lblNoStatement).equalsIgnoreCase(NO_STATEMENTS_FOUND);
        } else {
            testInfoLog("Total Statements", String.valueOf(sizeOf(lstStatements)));
            return sizeOf(lstStatements) > 0 && !isElementPresent(lblNoStatement) && isElementDisplay(btnFilter);
        }

    }

    public boolean verifyStatementDetails() {

        if (SuperAgentStatementIndexPage._expectedStatements == 0) {
            return isElementDisplay(lblNoStatement) && isElementDisplay(btnFilter) &&
                    getText(lblNoStatement).equalsIgnoreCase(NO_STATEMENTS_FOUND);
        } else {
            testInfoLog("Total Statements", String.valueOf(sizeOf(lstStatements)));
            return sizeOf(lstStatements) == SuperAgentStatementIndexPage._expectedStatements &&
                    !isElementPresent(lblNoStatement) && isElementDisplay(btnFilter);
        }

    }

    public boolean verifyStatementDownloaded() {

        String downloadedFileName = getLastFileModified(FILE_DOWNLOAD_PATH);

        System.out.println(downloadedFileName);
        System.out.println(SuperAgentStatementIndexPage._statementMonthYear);

        testInfoLog("Downloaded Statement Name", downloadedFileName);

        return downloadedFileName.contains(SuperAgentStatementIndexPage._statementMonthYear);

    }

    public boolean verifyStatementFilterSuccessfully() {

        String actualStatementMonthYear = getText(lstStatements.get(0));
        String expectedStatementMonthYear = SuperAgentStatementIndexPage._statementMonth + " - " +
                SuperAgentStatementIndexPage._statementYear;

        System.out.println(actualStatementMonthYear);
        System.out.println(expectedStatementMonthYear);

        testInfoLog("Filtered Statement", actualStatementMonthYear);

        return actualStatementMonthYear.equalsIgnoreCase(expectedStatementMonthYear) && sizeOf(lstStatements) == 1;
    }

    public boolean verifyNoStatementDisplay() {
        testValidationLog(getText(lblNoStatement));
        return isElementDisplay(lblNoStatement) &&
                getText(lblNoStatement).equalsIgnoreCase(NO_STATEMENTS_FOUND);
    }
}
