package com.incashme.superAgent.verification;

import com.framework.init.AbstractPage;
import com.incashme.superAgent.indexpage.SuperAgentCommissionIndexPage;
import com.incashme.superAgent.validations.CommissionValidations;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Rahul R.
 * Date: 2019-04-01
 * Time: 11:27
 * Project Name: InCashMe
 */
public class SuperAgentCommissionVerification extends AbstractPage implements CommissionValidations {

    @FindAll(value = {@FindBy(xpath = "//table[@id='agentListTbl']//tr//td[3]")})
    private List<WebElement> lstCommissionAmount;

    @FindAll(value = {@FindBy(xpath = "//table[@id='agentListTbl']//tr//td[4]")})
    private List<WebElement> lstCommissionDateTime;

    @FindBy(xpath = "//div//p[contains(text(),'no data alive')]")
    private WebElement lblNoAgentCommission;

    @FindAll(value = {@FindBy(xpath = "//table[@id='agentListTbl']//tr//td[2]")})
    private List<WebElement> lstCommissionTopUp;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//button[contains(@class,'searchbtn')]")
    private WebElement btnFilterSearch;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//a[contains(text(),'Clear All')]")
    private WebElement btnFilterClearAll;

    @FindBy(xpath = "//div//h2[contains(text(),'No Commission')]")
    private WebElement lblNoCommission;

    @FindAll(value = {@FindBy(xpath = "//table[@id='agentListTbl']//tr//td[1]//span")})
    private List<WebElement> lstAgentName;

    @FindAll(value = {@FindBy(xpath = " //table[@id='agentListTbl']//tr//td[1]//following-sibling::div")})
    private List<WebElement> lstAgentId;

    @FindBy(xpath = ".//div[contains(@id,'swal2-content') and contains(text(),'Invalid')]")
    private WebElement lblInvalidAmountValidation;

    @FindBy(xpath = ".//div[contains(@class,'swal2-actions')]//button[text()='OK']")
    private WebElement btnOTPConfirmOK;

    @FindBy(xpath = "//div[contains(@class,'column-chart-div')]")
    private WebElement commissionChart;

    @FindBy(xpath = "//label[@id='p_week']")
    private WebElement btnChartWeek;

    @FindBy(xpath = "//label[@id='p_month']")
    private WebElement btnChartMonth;

    @FindBy(xpath = "//label[@id='p_year']")
    private WebElement btnChartYear;

    @FindBy(xpath = "//label[@id='p_total']")
    private WebElement btnChartTotal;

    @FindBy(xpath = "//table[@id='agentListTbl']")
    private WebElement commissionTable;

    @FindBy(xpath = "//li/button[contains(@class,'filterBtn')]")
    private WebElement btnFilter;

    @FindBy(xpath = "//input[@type='search']")
    private WebElement txtInputSearch;

    @FindBy(xpath = "//table//tr//td[contains(text(),'matching')]")
    private WebElement lblNoRecordsFound;

    public SuperAgentCommissionVerification(WebDriver driver) {
        super(driver);
    }

    public boolean verifyCommissionScreen() {

        boolean bool;

        if (!isElementPresent(lblNoCommission)) {
            bool = isElementDisplay(commissionChart);

            scrollToElement(driver, btnChartWeek);

            bool = bool && isElementDisplay(btnChartWeek) && isElementDisplay(btnChartMonth) &&
                    isElementDisplay(btnChartYear) && isElementDisplay(btnChartTotal);

            //  scrollToElement(driver, commissionTable);
            // return bool && isElementDisplay(commissionTable);
            return bool;
        } else {
            scrollToElement(driver, lblNoCommission);
            testValidationLog(getText(lblNoCommission));
            return isElementDisplay(lblNoCommission) &&
                    getText(lblNoCommission).equalsIgnoreCase(NO_COMMISSION_FOUND);
        }

    }

    public boolean verifyTotalCommissionAmount() {

        double expectedTotalAmount = 0.00;

        pause(2);

        if (isListEmpty(lstCommissionAmount)) {
            testValidationLog(getInnerText(lblNoAgentCommission));
        } else {
            updateShowList(driver, findElementByName(driver, "agentListTbl_length"), "All");

            for (WebElement commission : lstCommissionAmount) {
                expectedTotalAmount += formatTwoDecimal(getDoubleFromString(getInnerText(commission)));
            }

            System.out.println(SuperAgentCommissionIndexPage._totalCommissionAmount);
            System.out.println(formatTwoDecimal(expectedTotalAmount));
        }

        testInfoLog("Total Earned Commission so far", String.valueOf(formatTwoDecimal(expectedTotalAmount)));

        return formatTwoDecimal(expectedTotalAmount) == SuperAgentCommissionIndexPage._totalCommissionAmount;
    }

    public boolean verifyCommissionCalculation() {

        boolean bool = false;

        if (isListEmpty(lstCommissionTopUp)) {
            testValidationLog(getInnerText(lblNoAgentCommission));
        } else {
            for (int commId = 0; commId < sizeOf(lstCommissionTopUp); commId++) {
                double expectedCommission = 0.25;
                double actualCommission = getDoubleFromString(getInnerText(lstCommissionAmount.get(commId)));

                bool = (expectedCommission == actualCommission);
            }
        }

        return bool;
    }

    public boolean verifyTopUpsSorted() {

        List<Double> commissionTopUpAfterSort = new ArrayList<>();
        List<Double> commissionTopUpBeforeSort = SuperAgentCommissionIndexPage._commissionTopUpBeforeSort;

        for (WebElement name : lstCommissionTopUp) {
            commissionTopUpAfterSort.add(getDoubleFromString(getInnerText(name)));
        }

        Collections.sort(commissionTopUpBeforeSort);

        System.out.println(commissionTopUpAfterSort);
        System.out.println(commissionTopUpBeforeSort);

        System.out.println(commissionTopUpBeforeSort.equals(commissionTopUpAfterSort));

        return commissionTopUpBeforeSort.equals(commissionTopUpAfterSort);
    }

    public boolean verifyCommissionSorted() {

        List<Double> commissionAmountAfterSort = new ArrayList<>();
        List<Double> commissionAmountBeforeSort = SuperAgentCommissionIndexPage._commissionAmountBeforeSort;

        for (WebElement name : lstCommissionAmount) {
            commissionAmountAfterSort.add(getDoubleFromString(getInnerText(name)));
        }

        Collections.sort(commissionAmountBeforeSort);

        System.out.println(commissionAmountAfterSort);
        System.out.println(commissionAmountBeforeSort);

        System.out.println(commissionAmountBeforeSort.equals(commissionAmountAfterSort));

        return commissionAmountBeforeSort.equals(commissionAmountAfterSort);
    }

    public boolean verifyCommissionDateTimeSorted() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        List<LocalDateTime> commissionDateTimeAfterSort = new ArrayList<>();
        List<LocalDateTime> commissionDateTimeBeforeSort = SuperAgentCommissionIndexPage._commissionDateTimeBeforeSort;

        for (WebElement name : lstCommissionDateTime) {
            commissionDateTimeAfterSort.add(LocalDateTime.parse(getInnerText(name), format));
        }

        Collections.sort(commissionDateTimeBeforeSort);

        System.out.println(commissionDateTimeAfterSort);
        System.out.println(commissionDateTimeBeforeSort);

        System.out.println(commissionDateTimeBeforeSort.equals(commissionDateTimeAfterSort));

        return commissionDateTimeBeforeSort.equals(commissionDateTimeAfterSort);
    }

    public boolean verifyFilterScreenDisplay() {
        pause(2);
        return isElementDisplay(btnFilterSearch) && isElementDisplay(btnFilterClearAll);
    }

    public boolean verifyRandomDataFiltered() {
        return getText(lblNoCommission).equalsIgnoreCase(NO_COMMISSION_FOUND);
    }

    public boolean verifyFirstNameFiltered() {

        boolean bool = false;

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement name : lstAgentName) {
            finalFilterResult.add(getInnerText(name));
        }

        System.out.println(finalFilterResult);
        System.out.println(SuperAgentCommissionIndexPage._filterAgentFirstName);

        for (String name : finalFilterResult) {
            bool = name.contains(SuperAgentCommissionIndexPage._filterAgentFirstName);
        }

        return bool;
    }

    public boolean verifyLastNameFiltered() {

        boolean bool = false;

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement name : lstAgentName) {
            finalFilterResult.add(getInnerText(name));
        }

        System.out.println(finalFilterResult);
        System.out.println(SuperAgentCommissionIndexPage._filterAgentLastName);

        for (String name : finalFilterResult) {
            bool = name.contains(SuperAgentCommissionIndexPage._filterAgentLastName);
        }

        return bool;
    }

    public boolean verifyIdFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement id : lstAgentId) {
            finalFilterResult.add(getInnerText(id));
        }

        System.out.println(finalFilterResult);
        System.out.println(SuperAgentCommissionIndexPage._filterAgentId);

        return finalFilterResult.contains(SuperAgentCommissionIndexPage._filterAgentId);
    }

    public boolean verifyMaxMinTopupValidation() {

        boolean bool = getText(lblInvalidAmountValidation).equalsIgnoreCase(INVALID_TOPUP_AMOUNT_RANGE);

        testValidationLog(getText(lblInvalidAmountValidation));
        clickOn(driver, btnOTPConfirmOK);

        return bool;
    }

    public boolean verifyMaxMinCommissionValidation() {

        boolean bool = getText(lblInvalidAmountValidation).equalsIgnoreCase(INVALID_COMMISSION_AMOUNT_RANGE);

        testValidationLog(getText(lblInvalidAmountValidation));
        clickOn(driver, btnOTPConfirmOK);

        return bool;
    }

    public boolean verifyTopUpFromFieldFilter() {

        List<Double> finalFilterResult = new ArrayList<>();

        for (WebElement name : lstCommissionTopUp) {
            finalFilterResult.add(getDoubleFromString(getInnerText(name)));
        }

        System.out.println(finalFilterResult);
        System.out.println(SuperAgentCommissionIndexPage._filterTopUpFromAmount);

        return !isListEmpty(finalFilterResult) &&
                findMin(finalFilterResult) >= SuperAgentCommissionIndexPage._filterTopUpFromAmount;
    }

    public boolean verifyTopUpToFieldFilter() {

        List<Double> finalFilterResult = new ArrayList<>();

        for (WebElement name : lstCommissionTopUp) {
            finalFilterResult.add(getDoubleFromString(getInnerText(name)));
        }

        System.out.println(finalFilterResult);
        System.out.println(SuperAgentCommissionIndexPage._filterTopUpToAmount);

        return !isListEmpty(finalFilterResult) &&
                findMax(finalFilterResult) <= SuperAgentCommissionIndexPage._filterTopUpToAmount;
    }

    public boolean verifyCommissionFromFieldFilter() {

        List<Double> finalFilterResult = new ArrayList<>();

        for (WebElement name : lstCommissionAmount) {
            finalFilterResult.add(getDoubleFromString(getInnerText(name)));
        }

        System.out.println(finalFilterResult);
        System.out.println(SuperAgentCommissionIndexPage._filterCommissionFromAmount);

        return !isListEmpty(finalFilterResult) &&
                findMin(finalFilterResult) >= SuperAgentCommissionIndexPage._filterCommissionFromAmount;
    }

    public boolean verifyCommissionToFieldFilter() {

        List<Double> finalFilterResult = new ArrayList<>();

        for (WebElement name : lstCommissionAmount) {
            finalFilterResult.add(getDoubleFromString(getInnerText(name)));
        }

        System.out.println(finalFilterResult);
        System.out.println(SuperAgentCommissionIndexPage._filterCommissionToAmount);

        return !isListEmpty(finalFilterResult) &&
                findMax(finalFilterResult) <= SuperAgentCommissionIndexPage._filterCommissionToAmount;
    }

    public boolean verifyTopUpMaxMinFilter() {

        List<Double> finalFilterResult = new ArrayList<>();

        for (WebElement name : lstCommissionTopUp) {
            finalFilterResult.add(getDoubleFromString(getInnerText(name)));
        }

        System.out.println(finalFilterResult);
        System.out.println(SuperAgentCommissionIndexPage._filterTopUpFromAmount);
        System.out.println(SuperAgentCommissionIndexPage._filterTopUpToAmount);

        return findMin(finalFilterResult) >= SuperAgentCommissionIndexPage._filterTopUpFromAmount &&
                findMax(finalFilterResult) <= SuperAgentCommissionIndexPage._filterTopUpToAmount;

    }

    public boolean verifyCommissionMaxMinFilter() {

        List<Double> finalFilterResult = new ArrayList<>();

        for (WebElement name : lstCommissionAmount) {
            finalFilterResult.add(getDoubleFromString(getInnerText(name)));
        }

        System.out.println(finalFilterResult);
        System.out.println(SuperAgentCommissionIndexPage._filterCommissionFromAmount);
        System.out.println(SuperAgentCommissionIndexPage._filterCommissionToAmount);

        return findMin(finalFilterResult) >= SuperAgentCommissionIndexPage._filterCommissionFromAmount &&
                findMax(finalFilterResult) <= SuperAgentCommissionIndexPage._filterCommissionToAmount;

    }

    public boolean verifySearchSuccessful() {

        boolean bool = true;

        for (WebElement e : lstAgentId)
            bool = bool && getInnerText(e).contains(SuperAgentCommissionIndexPage._searchCriteria);

        type(txtInputSearch, "");

        return bool;

    }

    public boolean verifyValidationForInvalidSearch() {

        testValidationLog(getInnerText(lblNoRecordsFound));

        boolean bool = getInnerText(lblNoRecordsFound).equalsIgnoreCase(NO_SEARCH_RECORD_FOUND);

        type(txtInputSearch, "x");
        txtInputSearch.clear();

        return bool;
    }
}
