package com.incashme.superAgent.verification;

import com.framework.init.AbstractPage;
import com.incashme.superAgent.indexpage.SuperAgentDashboardIndexPage;
import com.incashme.superAgent.indexpage.SuperAgentLoginIndexPage;
import com.incashme.superAgent.validations.DashboardValidations;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Rahul R.
 * Date: 2019-03-25
 * Time: 11:49
 * Project Name: InCashMe
 */
public class SuperAgentDashboardVerification extends AbstractPage implements DashboardValidations {

    public SuperAgentDashboardVerification(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//div[contains(text(),'Added New Agent')]//following-sibling::div")
    private WebElement lblAddedNewAgent;

    @FindBy(xpath = "//div[contains(text(),'Today Commission')]//following-sibling::div")
    private WebElement lblTodayCommission;

    @FindBy(xpath = "//div[contains(text(),'Target Left')]//following-sibling::div")
    private WebElement lblCreateAgentTargetLeft;

    @FindBy(xpath = "//div[text()='Target']//following-sibling::div")
    private WebElement lblCreateAgentTarget;

    @FindBy(xpath = "//div[contains(text(),'Days left')]//following-sibling::div")
    private WebElement lblTargetCreateTimeLeft;

    @FindBy(xpath = "//div[contains(text(),'Total Days')]//following-sibling::div")
    private WebElement lblTargetTotalDays;

    @FindBy(xpath = "//div//a[contains(text(),'Topups Status')]")
    private WebElement btnConsumerTopupStatus;

    @FindBy(xpath = "//div//a[contains(text(),'Consumers Status')]")
    private WebElement btnCreateConsumerStatus;

    @FindBy(xpath = "//div//a[contains(text(),'Merchants Status')]")
    private WebElement btnCreateMerchantStatus;

    @FindBy(xpath = "//div//a[contains(text(),'New Agent')]")
    private WebElement btnCreateNewAgent;

    @FindBy(xpath = "//div//p[contains(text(),'No Statement')]")
    private WebElement lblNoStatement;

    @FindBy(xpath = "//div[@class='blnc-txt']")
    private WebElement lblBalance;

    @FindBy(xpath = "//div[contains(@class,'column-chart-div')]")
    private WebElement commissionChart;

    @FindBy(xpath = "//label[@id='p_week']")
    private WebElement btnChartWeek;

    @FindBy(xpath = "//label[@id='p_month']")
    private WebElement btnChartMonth;

    @FindBy(xpath = "//label[@id='p_year']")
    private WebElement btnChartYear;

    @FindBy(xpath = "//label[@id='p_total']")
    private WebElement btnChartTotal;

    @FindBy(xpath = "//table[@id='agentListTbl']")
    private WebElement commissionTable;

    @FindBy(xpath = "//table[@id='agentsTbl']")
    private WebElement agentsTable;

    @FindBy(xpath = "//table[@id='superAgentTransactions']")
    private WebElement dashbaordCommissionTable;

    @FindBy(xpath = "//table[@id='trans-history-table']")
    private WebElement loginHistoryTable;

    @FindBy(xpath = "//table[@id='dataTblSA']")
    private WebElement agentListDetailsTable;

    @FindBy(xpath = "//li/button[contains(@class,'filterBtn')]")
    private WebElement btnFilter;

    @FindBy(xpath = "//div//h4[contains(text(),'Agent Lists')]")
    private WebElement lblAgentList;

    @FindBy(xpath = "//div//p[contains(text(),'No agents')]")
    private WebElement lblNoAgents;

    @FindBy(xpath = "//div//p[contains(text(),'No TopUp')]")
    private WebElement lblNoCommission;

    @FindBy(xpath = "//div[contains(@class,'profile-details')]//h2")
    private WebElement lblUserName;

    @FindBy(xpath = ".//div[contains(@class,'user-info')]//span[contains(@class,'name')]")
    private WebElement lblDashboardUserName;

    @FindBy(xpath = "//input[@formcontrolname='oldpassword']")
    private WebElement txtCurrentPassword;

    @FindBy(xpath = "//input[@formcontrolname='password2']")
    private WebElement txtNewPassword;

    @FindBy(xpath = "//input[@formcontrolname='password']")
    private WebElement txtConfirmPassword;

    @FindBy(xpath = ".//div[contains(@class,'login-form')]//button[@type='submit']")
    private WebElement btnSubmit;

    @FindBy(xpath = "//table//tr[1]/td[6]")
    private WebElement lblLastLoginTime;

    @FindBy(xpath = "//table//tr[1]/td[5]")
    private WebElement lblUserAgent;

    @FindBy(xpath = "//table//tr[1]/td[4]")
    private WebElement lblIpAddress;

    @FindBy(xpath = "//table//tr[1]/td[3]")
    private WebElement lblAppVersion;

    @FindBy(xpath = "//table//tr[1]/td[2]")
    private WebElement lblSources;

    @FindBy(xpath = "//table//tr[1]/td[1]")
    private WebElement lblLoginId;

    @FindBy(xpath = ".//div[contains(@id,'swal2-content') and contains(text(),'Unsuccessful attempt')]")
    private WebElement lblInvalidCurrentPasswordValidation;

    @FindBy(xpath = ".//div[contains(@id,'swal2-content') and contains(text(),'old Password')]")
    private WebElement lblPastPasswordValidation;

    @FindBy(xpath = ".//div[contains(@class,'swal2-actions')]//button[text()='OK']")
    private WebElement btnConfirmOK;

    @FindBy(xpath = ".//div[contains(@class,'login-form')]//*[@class='text-danger']")
    private WebElement lblInvalidNewPasswordValidation;

    @FindBy(xpath = ".//div[contains(@class,'login-form')]//*[@class='text-danger']")
    private WebElement lblInvalidConfirmPasswordValidation;

    @FindBy(xpath = ".//div[contains(@id,'swal2-content') and contains(text(),'changed successfully')]")
    private WebElement lblConfirmPasswordChange;

    @FindBy(xpath = "//div//p[contains(text(),'no data alive')]")
    private WebElement lblNoAgentCommission;

    @FindBy(xpath = "//div//h2[contains(text(),'No target Consumer')]")
    private WebElement lblNoTargetsFound;

    @FindBy(xpath = "//div//h2[contains(text(),'No Created New Consumer!')]")
    private WebElement lblNoConsumerFound;

    @FindBy(xpath = "//div//h2[contains(text(),'No Created New Merchant!')]")
    private WebElement lblNoMerchantFound;

    @FindBy(xpath = "//table[@id='usersTbl2']//td[contains(text(),'No data')]")
    private WebElement lblNoAgentsAvailable;

    @FindBy(xpath = "//table[@id='dataTblSA']//td[contains(text(),'No data')]")
    private WebElement lblNoFilterDataAvailable;

    @FindBy(xpath = "//div//p[contains(text(),'Here is no data alive.')]")
    private WebElement lblNoAddedAgentDetails;

    @FindBy(xpath = "//div[contains(@class,'spr-usr-name')]")
    private WebElement lblAgentNameInProfile;

    @FindBy(xpath = "//div[contains(@class,'spr-usr-id')]")
    private WebElement lblAgentIDInProfile;

    @FindBy(xpath = "//div[@id='agentChartCon']//following-sibling::span")
    private WebElement lblTotalAddedAgent;

    @FindBy(xpath = "//li/a[contains(text(),'Agent List')]")
    private WebElement btnAgentList;

    @FindBy(xpath = "//div[contains(text(),'Agents')]//following-sibling::div//span[contains(@class,'chart-velue')]")
    private WebElement lblAddedAgentInChart;

    @FindBy(xpath = "//div[contains(text(),'Target Left')]/following-sibling::div")
    private WebElement lblTaregtLeftInChart;

    @FindBy(xpath = "//div[contains(text(),'Total Target')]/following-sibling::div")
    private WebElement lblTotalTargetInChart;

    @FindBy(xpath = "//div[contains(text(),'Time')]//following-sibling::div//span[contains(@class,'chart-velue')]")
    private WebElement lblDaysCompletedInChart;

    @FindBy(xpath = "//div[contains(text(),'Days Left')]/following-sibling::div")
    private WebElement lblDaysLeftInChart;

    @FindBy(xpath = "//div[text()='Target']/following-sibling::div")
    private WebElement lblTargetDaysInChart;

    @FindBy(xpath = "//div[contains(@class,'merchant-count')]//span[contains(text(),'Agent')]")
    private WebElement lblTotalAgentCount;

    @FindBy(xpath = "//*[contains(text(),'Agents List')]/preceding-sibling::button")
    private WebElement btnCloseAgentList;

    @FindBy(xpath = "//a/h3[contains(text(),'Ontime')]")
    private WebElement btnStatusOnTime;

    @FindBy(xpath = "//a/h3[contains(text(),'Ontime')]/following-sibling::span")
    private WebElement lblOnTimeCount;

    @FindBy(xpath = "//a/h3[contains(text(),'Delay')]")
    private WebElement btnStatusDelay;

    @FindBy(xpath = "//a/h3[contains(text(),'Delay')]/following-sibling::span")
    private WebElement lblDelayCount;

    @FindBy(xpath = "//a/h3[contains(text(),'Completed')]")
    private WebElement btnStatusCompleted;

    @FindBy(xpath = "//a/h3[contains(text(),'Completed')]/following-sibling::span")
    private WebElement lblCompletedCount;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//button[contains(@class,'searchbtn')]")
    private WebElement btnFilterSearch;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//a[contains(text(),'Clear All')]")
    private WebElement btnFilterClearAll;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@class,'profile-details')]//div[contains(@class,'avatar-user')]/following-sibling::div")})
    private List<WebElement> lstUserAccountDetails;

    @FindAll(value = {@FindBy(xpath = "//ul[contains(@class,'small-info')]//li")})
    private List<WebElement> lstUserPersonalDetails;

    @FindAll(value = {@FindBy(xpath = "//ul[contains(@class,'address-info')]//li")})
    private List<WebElement> lstUserAddressDetails;

    @FindAll(value = {@FindBy(xpath = "//table[@id='superAgentTransactions']//tr//td[1]//h5/span")})
    private List<WebElement> lstCommissionAgentNames;

    @FindAll(value = {@FindBy(xpath = "//table[@id='superAgentTransactions']//tr//td[1]//following-sibling::div")})
    private List<WebElement> lstCommissionAgentId;

    @FindAll(value = {@FindBy(xpath = "//table[@id='superAgentTransactions']//tr//td[2]/div")})
    private List<WebElement> lstCommissionTopUp;

    @FindAll(value = {@FindBy(xpath = "//table[@id='superAgentTransactions']//tr//td[3]/div")})
    private List<WebElement> lstCommissionAmount;

    @FindAll(value = {@FindBy(xpath = "//table[@id='superAgentTransactions']//tr//td[4]/div")})
    private List<WebElement> lstCommissionTxTime;

    @FindAll(value = {@FindBy(xpath = "//label//select")})
    private List<WebElement> selectShowView;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@class,'add-user-list')]//h5")})
    private List<WebElement> lstAddedAgentNames;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@class,'add-user-list')]//div[contains(text(),'ID')]")})
    private List<WebElement> lstAddedAgentId;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTblSA']//tr//td[1]//h5")})
    private List<WebElement> lstAgentName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTblSA']//tr//td[1]//following-sibling::div")})
    private List<WebElement> lstAgentId;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTblSA']//tr//td[2]//div")})
    private List<WebElement> lstAgentMobileNumber;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTblSA']//tr//td[3]//div")})
    private List<WebElement> lstAgentStatus;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTblSA']//tr//td[5]//div")})
    private List<WebElement> lstAgentCreatedDateTime;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTblSA']//tr//td[6]//a")})
    private List<WebElement> lstMOREButton;

    @FindAll(value = {@FindBy(xpath = "//table[@id='agentListTbl']//tr//td[1]//h5")})
    private List<WebElement> lstStatusAgentName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='agentListTbl']//tr//td[1]//following-sibling::div")})
    private List<WebElement> lstStatusAgentId;

    @FindAll(value = {@FindBy(xpath = "//table[@id='agentListTbl']//tr//td[2]")})
    private List<WebElement> lstStatusAgentMobile;

    @FindAll(value = {@FindBy(xpath = "//table[@id='agentListTbl']//tr//td[3]")})
    private List<WebElement> lstStatusAgentAssigned;

    @FindAll(value = {@FindBy(xpath = "//table[@id='agentListTbl']//tr//td[4]")})
    private List<WebElement> lstStatusAgentCompleted;

    @FindAll(value = {@FindBy(xpath = "//table[@id='agentListTbl']//tr//td[5]")})
    private List<WebElement> lstStatusAgentTargetDays;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[1]")})
    private List<WebElement> lstHistoryLoginID;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[2]")})
    private List<WebElement> lstHistoryLoginBrowser;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[4]")})
    private List<WebElement> lstHistoryLoginIP;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[5]")})
    private List<WebElement> lstHistoryLoginUserAgent;

    @FindAll(value = {@FindBy(xpath = "//table[@id='trans-history-table']//tr//td[6]")})
    private List<WebElement> lstHistoryLoginDateTime;

    @FindBy(xpath = "//table//tr//td[contains(text(),'matching')]")
    private WebElement lblNoRecordsFound;

    @FindBy(xpath = "//input[@type='search']")
    private WebElement txtInputSearch;

    public boolean verifyDashboardDetails() {

        pause(5);

        testInfoLog("Added New Agent", getText(lblAddedNewAgent));
        testInfoLog("Today Commission", getText(lblTodayCommission));
        testInfoLog("Create Agent -  Target Left", getText(lblCreateAgentTargetLeft));
        testInfoLog("Create Agent -  Target", getText(lblCreateAgentTarget));
        testInfoLog("Create Time - Days Left", getText(lblTargetCreateTimeLeft));
        testInfoLog("Create Time - Total Days", getText(lblTargetTotalDays));

        return isElementDisplay(lblTargetTotalDays) && isElementDisplay(lblTargetCreateTimeLeft) &&
                isElementDisplay(lblCreateAgentTarget) && isElementDisplay(lblTodayCommission) &&
                isElementDisplay(lblAddedNewAgent) && isElementDisplay(lblCreateAgentTargetLeft);

    }

    public boolean verifyDashboardButtons() {

        boolean bool = isElementDisplay(btnConsumerTopupStatus) && isElementDisplay(btnCreateConsumerStatus) &&
                isElementDisplay(btnCreateMerchantStatus);

        scrollToElement(driver, btnCreateNewAgent);

        return bool && isElementDisplay(btnCreateNewAgent);

    }

    public boolean verifyBalanceScreen() {
        testInfoLog("Current Account Balance", getText(lblBalance));
        return isElementDisplay(lblBalance);
    }

    public boolean verifyUserProfileDetails() {

        pause(3);

        testInfoLog("", "User Account Information");
        for (WebElement element : lstUserAccountDetails) {
            System.out.println(getText(element));
            testInfoLog(getInnerText(element).split(" - ")[0], getInnerText(element).split(" - ")[1]);
        }

        testInfoLog("", "User Personal Information");
        testInfoLog("DOB", getText(lstUserPersonalDetails.get(0)));
        testInfoLog("Email", getText(lstUserPersonalDetails.get(1)));
        testInfoLog("Mobile Number", getText(lstUserPersonalDetails.get(2)));

        testInfoLog("", "User Address Information");
        for (WebElement element : lstUserAddressDetails) {
            if (getText(element).split("\n").length == 1) {
                testInfoLog(getText(element).split("\n")[0], "");
            } else {
                testInfoLog(getText(element).split("\n")[0], getText(element).split("\n")[1]);
            }
        }

        return getText(lblUserName).equalsIgnoreCase(getText(lblDashboardUserName)) &&
                getText(lstUserPersonalDetails.get(1)).equalsIgnoreCase(username);
    }

    public boolean verifyChangePasswordScreen() {

        pause(1);

        return isElementDisplay(txtCurrentPassword) && isElementDisplay(txtNewPassword) &&
                isElementDisplay(txtConfirmPassword) && isElementDisplay(btnSubmit) &&
                btnSubmit.getAttribute("disabled").equalsIgnoreCase("true");

    }

    public boolean verifyPrivacyPolicyScreen() {

        switchToWindow(driver);

        String expectedTitle = getTitle(driver);
        String actualTitle = "InCashMe™";

        close(driver);

        switchToWindow(driver);

        return expectedTitle.equalsIgnoreCase(actualTitle);

    }

    public boolean verifyTnCScreen() {

        switchToWindow(driver);

        String expectedTitle = getTitle(driver);
        String actualTitle = "InCashMe™";

        close(driver);

        switchToWindow(driver);

        return expectedTitle.equalsIgnoreCase(actualTitle);

    }

    public boolean verifyLoginHistory() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        pause(5);

        String actualTime = getText(lblLastLoginTime).trim();

        testVerifyLog("Login ID : " + getText(lblLoginId) +
                "<br>Source : " + getText(lblSources) +
                "<br>App version : " + getText(lblAppVersion) +
                "<br>IP : " + getText(lblIpAddress) +
                "<br>User Agent : " + getText(lblUserAgent) +
                "<br>Login Time : " + getText(lblLastLoginTime));

        List<String> lstExpectedTimes = new ArrayList<>();

        for (int sec = 1; sec <= 20; sec++) {
            lstExpectedTimes.add((LocalDateTime.parse(SuperAgentLoginIndexPage._loginTime, format)
                    .plusSeconds(sec)).format(format).trim());
        }

        System.out.println(lstExpectedTimes);
        System.out.println(actualTime);

        return isElementDisplay(loginHistoryTable) && lstExpectedTimes.contains(actualTime);

    }

    public boolean verifyInvalidCurrentPasswordValidation() {

        testValidationLog(getText(lblInvalidCurrentPasswordValidation));

        boolean bool = isElementDisplay(lblInvalidCurrentPasswordValidation) &&
                getText(lblInvalidCurrentPasswordValidation).equalsIgnoreCase(FIRST_UNSUCCESSFUL_ATTEMPTS) &&
                isElementDisplay(btnConfirmOK);

        clickOn(driver, btnConfirmOK);

        return bool;
    }

    public boolean verifyInvalidCurrentPasswordSecondTime() {

        testValidationLog(getText(lblInvalidCurrentPasswordValidation));

        boolean bool = isElementDisplay(lblInvalidCurrentPasswordValidation) &&
                getText(lblInvalidCurrentPasswordValidation).equalsIgnoreCase(SECOND_UNSUCCESSFUL_ATTEMPTS) &&
                isElementDisplay(btnConfirmOK);

        clickOn(driver, btnConfirmOK);

        return bool;
    }

    public boolean verifyAccountIsBlocked() {

        testValidationLog(getText(lblInvalidCurrentPasswordValidation));

        boolean bool = isElementDisplay(lblInvalidCurrentPasswordValidation) &&
                getText(lblInvalidCurrentPasswordValidation).equalsIgnoreCase(THIRD_UNSUCCESSFUL_ATTEMPTS) &&
                isElementDisplay(btnConfirmOK);

        clickOn(driver, btnConfirmOK);

        return bool;
    }

    public boolean verifyInvalidNewPasswordValidation() {

        testValidationLog(getText(lblInvalidNewPasswordValidation));

        return isElementDisplay(lblInvalidNewPasswordValidation) &&
                getText(lblInvalidNewPasswordValidation).equalsIgnoreCase(INVALID_NEW_PASSWORD);
    }

    public boolean verifyInvalidConfirmPasswordValidation() {

        testValidationLog(getText(lblInvalidConfirmPasswordValidation));

        return isElementDisplay(lblInvalidConfirmPasswordValidation) &&
                getText(lblInvalidConfirmPasswordValidation).equalsIgnoreCase(NEW_CONFIRM_NOT_MATCHED);
    }

    public boolean verifyPastPasswordValidation() {

        testValidationLog(getText(lblPastPasswordValidation));

        boolean bool = isElementDisplay(lblPastPasswordValidation) &&
                getText(lblPastPasswordValidation).equalsIgnoreCase(PAST_PASSWORD_MESSAGE) &&
                isElementDisplay(btnConfirmOK);

        clickOn(driver, btnConfirmOK);

        return bool;

    }

    public boolean verifyPasswordChangeSuccessfully() {

        testConfirmationLog(getText(lblConfirmPasswordChange));

        boolean bool = isElementDisplay(lblConfirmPasswordChange) &&
                getText(lblConfirmPasswordChange).equalsIgnoreCase(SUCCESSFUL_PASSWORD_CHANGE_MESSAGE) &&
                isElementDisplay(btnConfirmOK);

        clickOn(driver, btnConfirmOK);

        return bool;

    }

    public boolean verifyCommissionDetailsOnDashboard() {

        if (SuperAgentDashboardIndexPage._todayCommission == 0.00) {

            scrollToElement(driver, lblNoAgentCommission);

            testValidationLog(getText(lblNoAgentCommission));

            return isElementDisplay(lblNoAgentCommission) &&
                    getText(lblNoAgentCommission).equalsIgnoreCase(NO_AGENT_COMMISSION_TODAY_MESSAGE);
        } else {

            scrollToElement(driver, btnCreateNewAgent);

            testInfoLog("Total Commission Transaction display", String.valueOf(sizeOf(lstCommissionAgentNames)));

            for (int comm = 0; comm < sizeOf(lstCommissionAgentNames); comm++) {
                testInfoLog("________________________________________________________________________", "");
                testInfoLog("Agent Name & ID", getText(lstCommissionAgentNames.get(comm)) +
                        "  " + getText(lstCommissionAgentId.get(comm)));
                testInfoLog("Top up Amount", getText(lstCommissionTopUp.get(comm)));
                testInfoLog("Commission", getText(lstCommissionAmount.get(comm)));
                testInfoLog("Date & Time", getText(lstCommissionTxTime.get(comm)));
                testInfoLog("________________________________________________________________________", "");
            }

            return isElementDisplay(dashbaordCommissionTable);

        }
    }

    public boolean verifyTotalCommissionAmount() {

        double expectedTotalAmount = 0.00;

        pause(2);

        if (isListEmpty(lstCommissionAmount)) {
            testValidationLog(getText(lblNoAgentCommission));
        } else {
            updateShowList(driver, findElementByName(driver, "superAgentTransactions_length"), "All");

            for (WebElement commission : lstCommissionAmount) {
                expectedTotalAmount += getDoubleFromString(getText(commission));
            }

            System.out.println(SuperAgentDashboardIndexPage._todayCommission);
            System.out.println(formatTwoDecimal(expectedTotalAmount));
        }

        return formatTwoDecimal(expectedTotalAmount) == SuperAgentDashboardIndexPage._todayCommission;
    }


    public boolean verifyCommissionCalculation() {

        boolean bool = false;

        if (isListEmpty(lstCommissionAmount)) {
            testValidationLog(getText(lblNoAgentCommission));
        } else {
            for (int commId = 0; commId < sizeOf(lstCommissionTopUp); commId++) {
                double expectedCommission = formatTwoDecimal((getDoubleFromString(getInnerText(lstCommissionTopUp.get(commId))) * 0.010) / 100);
                double actualCommission = getDoubleFromString(getInnerText(lstCommissionAmount.get(commId)));

                System.out.println(expectedCommission + "   " + actualCommission);

                bool = (expectedCommission == actualCommission);
            }
        }

        return bool;
    }

    public boolean verifyTodayAddedAgentDetails() {

        if (SuperAgentDashboardIndexPage._todayAddedAgent == 0) {
            pause(2);
            String validation = getText(lblNoAgents);
            testValidationLog(validation);
            testStepsLog(_logStep++, "Click on Close button.");
            clickOn(driver, btnCloseAgentList);
            return sizeOf(lstAddedAgentNames) == SuperAgentDashboardIndexPage._todayAddedAgent &&
                    validation.equalsIgnoreCase(NO_TODAY_ADDED_AGENT_MESSAGE);
        } else {
            updateShowList(driver, findElementByName(driver, "usersTbl_length"), "All");

            testInfoLog("Total Added Agents", String.valueOf(sizeOf(lstAddedAgentNames)));
            testInfoLog("________________________________________________________________________", "");
            for (int agentNum = 0; agentNum < sizeOf(lstAddedAgentNames); agentNum++) {

                testInfoLog("Agent Name", getInnerText(lstAddedAgentNames.get(agentNum))
                        + "<br>Agent ID : " + getInnerText(lstAddedAgentId.get(agentNum)));
                testInfoLog("________________________________________________________________________", "");
            }
            return sizeOf(lstAddedAgentNames) == SuperAgentDashboardIndexPage._todayAddedAgent;
        }
    }

    public boolean verifyTotalAddedAgentDetails() {

        if (SuperAgentDashboardIndexPage._targetAchievedInAgentDetails == 0) {
            pause(2);
            String validation = getText(lblNoAgentsAvailable);
            testValidationLog(validation);
            testStepsLog(_logStep++, "Click on Close button.");
            clickOn(driver, btnCloseAgentList);
            return sizeOf(lstAddedAgentNames) == SuperAgentDashboardIndexPage._targetAchievedInAgentDetails &&
                    validation.equalsIgnoreCase(NO_TOTAL_ADDED_AGENT_MESSAGE);
        } else {

            pause(2);

            updateShowList(driver, findElementByName(driver, "usersTbl2_length"), "All");

            testInfoLog("Total Added Agents", String.valueOf(sizeOf(lstAddedAgentNames)));
            testInfoLog("________________________________________________________________________", "");
            for (int agentNum = 0; agentNum < sizeOf(lstAddedAgentNames); agentNum++) {

                testInfoLog("Agent Name", getInnerText(lstAddedAgentNames.get(agentNum))
                        + "<br>Agent ID : " + getInnerText(lstAddedAgentId.get(agentNum)));
                testInfoLog("________________________________________________________________________", "");
            }
            return sizeOf(lstAddedAgentNames) == SuperAgentDashboardIndexPage._targetAchievedInAgentDetails;
        }
    }

    public boolean verifyAgentDetailsScreenDisplay() {
        pause(3);
        return getText(lblAgentNameInProfile).equalsIgnoreCase(SuperAgentDashboardIndexPage._agentName) &&
                getText(lblAgentIDInProfile).equalsIgnoreCase(SuperAgentDashboardIndexPage._agentId);
    }

    public boolean verifyDashboardAgentCount() {

        if (SuperAgentDashboardIndexPage._targetLeft != 0)
            return SuperAgentDashboardIndexPage._totalAddedAgent ==
                    (SuperAgentDashboardIndexPage._totalTarget - SuperAgentDashboardIndexPage._targetLeft);
        else
            return SuperAgentDashboardIndexPage._totalAddedAgent >= SuperAgentDashboardIndexPage._totalTarget;

    }

    public boolean verifyAgentTargetDetailsScreen() {

        boolean bool;

        if (SuperAgentDashboardIndexPage._targetLeft != 0)
            bool = SuperAgentDashboardIndexPage._targetAchievedInAgentDetails ==
                    (SuperAgentDashboardIndexPage._totalTargetInAgentDetails - SuperAgentDashboardIndexPage._targetLeftInAgentDetails);
        else
            bool = SuperAgentDashboardIndexPage._targetAchievedInAgentDetails >= SuperAgentDashboardIndexPage._targetLeftInAgentDetails;

        scrollToElement(driver, btnChartTotal);

        bool = bool && SuperAgentDashboardIndexPage._daysCompletedInAgentDetails ==
                (SuperAgentDashboardIndexPage._totalDaysInAgentDetails - SuperAgentDashboardIndexPage._daysLeftInAgentDetails);

        bool = bool && (SuperAgentDashboardIndexPage._targetAchievedInAgentDetails == SuperAgentDashboardIndexPage._totalAddedAgent) &&
                (SuperAgentDashboardIndexPage._totalTarget == SuperAgentDashboardIndexPage._totalTargetInAgentDetails) &&
                (SuperAgentDashboardIndexPage._targetLeft == SuperAgentDashboardIndexPage._targetLeftInAgentDetails) &&
                (SuperAgentDashboardIndexPage._totalDaysInAgentDetails == SuperAgentDashboardIndexPage._totalDays) &&
                (SuperAgentDashboardIndexPage._daysLeftInAgentDetails == SuperAgentDashboardIndexPage._daysLeft);

        testStepsLog(_logStep++, "Click On Total in Chart.");
        clickOn(driver, btnChartTotal);

        int totalAgentFromChart = getIntegerFromString(getText(lblTotalAgentCount));

        return bool && (SuperAgentDashboardIndexPage._targetAchievedInAgentDetails == totalAgentFromChart) &&
                isElementDisplay(btnAgentList) && isElementDisplay(btnFilter);
    }

    public boolean verifyAgentListCount() {

        if (isListEmpty(lstAgentName)) {
            pause(2);
            testValidationLog(getInnerText(lblNoAddedAgentDetails));
            return isListEmpty(lstAgentName)
                    && getInnerText(lblNoAddedAgentDetails).contains(NO_CREATED_AGENT_LIST_MESSAGE);
        } else {

            updateShowList(driver, findElementByName(driver, "dataTblSA_length"), "All");

            for (int agent = 0; agent < sizeOf(lstAgentName); agent++) {
                testInfoLog("Agent Name & ID", getInnerText(lstAgentName.get(agent)) + "  " + getInnerText(lstAgentId.get(agent)) +
                        "<br>Mobile Number : " + getInnerText(lstAgentMobileNumber.get(agent)) +
                        "<br>Account Status : " + getInnerText(lstAgentStatus.get(agent)) +
                        "<br>Date & Time : " + getInnerText(lstAgentCreatedDateTime.get(agent)));
                testInfoLog("________________________________________________________________________", "");
            }

            return !isElementPresent(lblNoAddedAgentDetails)
                    && (sizeOf(lstAgentName) == SuperAgentDashboardIndexPage._targetAchievedInAgentDetails);
        }
    }

    public boolean verifyAgentNameSorted() {

        List<String> agentNameAfterSort = new ArrayList<>();
        List<String> agentNameBeforeSort = SuperAgentDashboardIndexPage._agentNamesBeforeSort;

        for (WebElement name : lstAgentName) {
            agentNameAfterSort.add(getInnerText(name));
        }

        agentNameBeforeSort.sort(
                (String o1, String o2) -> {
                    if (o1.equalsIgnoreCase(o2)) {
                        return o2.compareTo(o1);
                    }
                    return o1.toLowerCase().compareTo(o2.toLowerCase());
                }
        );

        System.out.println(agentNameAfterSort);
        System.out.println(agentNameBeforeSort);

        System.out.println(agentNameBeforeSort.equals(agentNameAfterSort));

        return agentNameBeforeSort.equals(agentNameAfterSort);
    }

    public boolean verifyAgentNumberSorted() {

        List<String> agentNumberAfterSort = new ArrayList<>();
        List<String> agentNumberBeforeSort = SuperAgentDashboardIndexPage._agentNumberBeforeSort;

        for (WebElement name : lstAgentMobileNumber) {
            agentNumberAfterSort.add(getInnerText(name));
        }

        agentNumberBeforeSort.sort(
                (String o1, String o2) -> {
                    if (o1.equalsIgnoreCase(o2)) {
                        return o2.compareTo(o1);
                    }
                    return o1.toLowerCase().compareTo(o2.toLowerCase());
                }
        );

        System.out.println(agentNumberAfterSort);
        System.out.println(agentNumberBeforeSort);

        System.out.println(agentNumberBeforeSort.equals(agentNumberAfterSort));

        return agentNumberBeforeSort.equals(agentNumberAfterSort);
    }

    public boolean verifyAgentStatusSorted() {

        List<String> agentStatusAfterSort = new ArrayList<>();
        List<String> agentStatusBeforeSort = SuperAgentDashboardIndexPage._agentStatusBeforeSort;

        for (WebElement name : lstAgentStatus) {
            agentStatusAfterSort.add(getInnerText(name));
        }

        agentStatusBeforeSort.sort(
                (String o1, String o2) -> {
                    if (o1.equalsIgnoreCase(o2)) {
                        return o2.compareTo(o1);
                    }
                    return o1.toLowerCase().compareTo(o2.toLowerCase());
                }
        );

        System.out.println(agentStatusAfterSort);
        System.out.println(agentStatusBeforeSort);

        System.out.println(agentStatusBeforeSort.equals(agentStatusAfterSort));

        return agentStatusBeforeSort.equals(agentStatusAfterSort);
    }


    public boolean verifyAgentDateTimeSorted() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        List<LocalDateTime> agentDateTimeAfterSort = new ArrayList<>();
        List<LocalDateTime> agentDateTimeBeforeSort = SuperAgentDashboardIndexPage._agentDateTimeBeforeSort;

        for (WebElement name : lstAgentCreatedDateTime) {
            agentDateTimeAfterSort.add(LocalDateTime.parse(getInnerText(name), format));
        }

        Collections.sort(agentDateTimeBeforeSort);

        System.out.println(agentDateTimeAfterSort);
        System.out.println(agentDateTimeBeforeSort);

        System.out.println(agentDateTimeBeforeSort.equals(agentDateTimeAfterSort));

        return agentDateTimeBeforeSort.equals(agentDateTimeAfterSort);
    }

    public boolean verifyConsumerTopUpStatusCount() {

        boolean bool;

        testStepsLog(_logStep++, "Click On Ontime button.");
        clickOn(driver, btnStatusOnTime);

        if (getIntegerFromString(getInnerText(lblOnTimeCount)) == 0) {
            testValidationLog(getInnerText(lblNoTargetsFound));
            bool = isElementDisplay(lblNoTargetsFound) && getInnerText(lblNoTargetsFound).equalsIgnoreCase(NO_AGENT_TARGET_STATUS);
        } else {
            updateShowList(driver, findElementByName(driver, "agentListTbl_length"), "All");
            testInfoLog("Total Ontime Top ups", String.valueOf(getIntegerFromString(getText(lblOnTimeCount))));
            bool = getIntegerFromString(getText(lblOnTimeCount)) == sizeOf(lstStatusAgentName);
        }

        testStepsLog(_logStep++, "Click On Delay button.");
        clickOn(driver, btnStatusDelay);

        if (getIntegerFromString(getInnerText(lblDelayCount)) == 0) {
            testValidationLog(getInnerText(lblNoTargetsFound));
            bool = bool && isElementDisplay(lblNoTargetsFound) && getInnerText(lblNoTargetsFound).equalsIgnoreCase(NO_AGENT_TARGET_STATUS);
        } else {
            updateShowList(driver, findElementByName(driver, "agentListTbl_length"), "All");
            testInfoLog("Total Delay Top ups", String.valueOf(getIntegerFromString(getText(lblDelayCount))));
            bool = bool && getIntegerFromString(getText(lblDelayCount)) == sizeOf(lstStatusAgentName);
        }

        testStepsLog(_logStep++, "Click On Completed button.");
        clickOn(driver, btnStatusCompleted);

        if (getIntegerFromString(getInnerText(lblCompletedCount)) == 0) {
            testValidationLog(getInnerText(lblNoTargetsFound));
            bool = bool && isElementDisplay(lblNoTargetsFound) && getInnerText(lblNoTargetsFound).equalsIgnoreCase(NO_AGENT_TARGET_STATUS);
        } else {
            updateShowList(driver, findElementByName(driver, "agentListTbl_length"), "All");
            testInfoLog("Total Completed Top ups", String.valueOf(getIntegerFromString(getInnerText(lblCompletedCount))));
            bool = bool && getIntegerFromString(getText(lblCompletedCount)) == sizeOf(lstStatusAgentName);
        }

        return bool;
    }

    public boolean verifyCreateConsumerStatusCount() {

        boolean bool;

        testStepsLog(_logStep++, "Click On Ontime button.");
        clickOn(driver, btnStatusOnTime);

        if (getIntegerFromString(getInnerText(lblOnTimeCount)) == 0) {
            testValidationLog(getInnerText(lblNoConsumerFound));
            bool = isElementDisplay(lblNoConsumerFound) && getInnerText(lblNoConsumerFound).equalsIgnoreCase(NO_CONSUMER_TARGET_STATUS);
        } else {
            updateShowList(driver, findElementByName(driver, "agentListTbl_length"), "All");
            testInfoLog("Total Ontime Top ups", String.valueOf(getIntegerFromString(getText(lblOnTimeCount))));
            bool = getIntegerFromString(getText(lblOnTimeCount)) == sizeOf(lstStatusAgentName);
        }

        testStepsLog(_logStep++, "Click On Delay button.");
        clickOn(driver, btnStatusDelay);

        if (getIntegerFromString(getInnerText(lblDelayCount)) == 0) {
            testValidationLog(getInnerText(lblNoConsumerFound));
            bool = bool && isElementDisplay(lblNoConsumerFound) && getInnerText(lblNoConsumerFound).equalsIgnoreCase(NO_CONSUMER_TARGET_STATUS);
        } else {
            updateShowList(driver, findElementByName(driver, "agentListTbl_length"), "All");
            testInfoLog("Total Delay Top ups", String.valueOf(getIntegerFromString(getText(lblDelayCount))));
            bool = bool && getIntegerFromString(getText(lblDelayCount)) == sizeOf(lstStatusAgentName);
        }

        testStepsLog(_logStep++, "Click On Completed button.");
        clickOn(driver, btnStatusCompleted);

        if (getIntegerFromString(getInnerText(lblCompletedCount)) == 0) {
            testValidationLog(getInnerText(lblNoConsumerFound));
            bool = bool && isElementDisplay(lblNoConsumerFound) && getInnerText(lblNoConsumerFound).equalsIgnoreCase(NO_CONSUMER_TARGET_STATUS);
        } else {
            updateShowList(driver, findElementByName(driver, "agentListTbl_length"), "All");
            testInfoLog("Total Completed Top ups", String.valueOf(getIntegerFromString(getText(lblCompletedCount))));
            bool = bool && getIntegerFromString(getText(lblCompletedCount)) == sizeOf(lstStatusAgentName);
        }

        return bool;
    }

    public boolean verifyCreateMerchantStatusCount() {

        boolean bool;

        testStepsLog(_logStep++, "Click On Ontime button.");
        clickOn(driver, btnStatusOnTime);

        if (getIntegerFromString(getInnerText(lblOnTimeCount)) == 0) {
            testValidationLog(getInnerText(lblNoMerchantFound));
            bool = isElementDisplay(lblNoMerchantFound) && getInnerText(lblNoMerchantFound).equalsIgnoreCase(NO_MERCHANT_TARGET_STATUS);

            System.out.println(bool);
        } else {
            updateShowList(driver, findElementByName(driver, "agentListTbl_length"), "All");
            testInfoLog("Total Ontime Top ups", String.valueOf(getIntegerFromString(getText(lblOnTimeCount))));
            bool = getIntegerFromString(getText(lblOnTimeCount)) == sizeOf(lstStatusAgentName);

            System.out.println(getIntegerFromString(getText(lblOnTimeCount)));
            System.out.println(sizeOf(lstStatusAgentName));
            System.out.println(bool);

        }

        testStepsLog(_logStep++, "Click On Delay button.");
        clickOn(driver, btnStatusDelay);

        if (getIntegerFromString(getInnerText(lblDelayCount)) == 0) {
            testValidationLog(getInnerText(lblNoMerchantFound));
            bool = bool && isElementDisplay(lblNoMerchantFound) && getInnerText(lblNoMerchantFound).equalsIgnoreCase(NO_MERCHANT_TARGET_STATUS);

            System.out.println(bool);
        } else {
            updateShowList(driver, findElementByName(driver, "agentListTbl_length"), "All");
            testInfoLog("Total Delay Top ups", String.valueOf(getIntegerFromString(getText(lblDelayCount))));
            bool = bool && getIntegerFromString(getText(lblDelayCount)) == sizeOf(lstStatusAgentName);

            System.out.println(getIntegerFromString(getText(lblDelayCount)));
            System.out.println(sizeOf(lstStatusAgentName));
            System.out.println(bool);
        }

        testStepsLog(_logStep++, "Click On Completed button.");
        clickOn(driver, btnStatusCompleted);

        if (getIntegerFromString(getInnerText(lblCompletedCount)) == 0) {
            testValidationLog(getInnerText(lblNoMerchantFound));
            bool = bool && isElementDisplay(lblNoMerchantFound) && getInnerText(lblNoMerchantFound).equalsIgnoreCase(NO_MERCHANT_TARGET_STATUS);
            System.out.println(bool);

        } else {
            updateShowList(driver, findElementByName(driver, "agentListTbl_length"), "All");
            testInfoLog("Total Completed Top ups", String.valueOf(getIntegerFromString(getText(lblCompletedCount))));
            bool = bool && getIntegerFromString(getText(lblCompletedCount)) == sizeOf(lstStatusAgentName);

            System.out.println(getIntegerFromString(getText(lblCompletedCount)));
            System.out.println(sizeOf(lstStatusAgentName));
            System.out.println(bool);
        }

        return bool;
    }

    public boolean verifyConsumerOntimeCalculation() {

        boolean bool = false;

        updateShowList(driver, findElementByName(driver, "agentListTbl_length"), "All");

        List<Integer> lstCompletedTargetDays = new ArrayList<>();
        List<Integer> lstTotalTargetDays = new ArrayList<>();

        for (int agent = 0; agent < sizeOf(lstStatusAgentTargetDays); agent++) {
            lstTotalTargetDays.add(getIntegerFromString(getInnerText(lstStatusAgentTargetDays.get(agent)).split("/")[1]));
            lstCompletedTargetDays.add(getIntegerFromString(getInnerText(lstStatusAgentTargetDays.get(agent)).split("/")[0]));
        }

        for (int agent = 0; agent < sizeOf(lstStatusAgentName); agent++) {
            if (getDoubleFromString(getInnerText(lstStatusAgentCompleted.get(agent))) >
                    formatTwoDecimal((getDoubleFromString(getInnerText(lstStatusAgentAssigned.get(agent))) /
                            lstTotalTargetDays.get(agent))) &&
                    getDoubleFromString(getInnerText(lstStatusAgentCompleted.get(agent)))
                            < getDoubleFromString(getInnerText(lstStatusAgentAssigned.get(agent)))) {
                testInfoLog("Completed Topup so far", getInnerText(lstStatusAgentCompleted.get(agent)));
                testInfoLog("Assigned Topup", getInnerText(lstStatusAgentAssigned.get(agent)));
                testInfoLog("Target Days", String.valueOf(lstTotalTargetDays.get(agent)));
                testInfoLog("Completed Days", String.valueOf(lstCompletedTargetDays.get(agent)));
                testConfirmationLog("Expected Topup : between " + Math.ceil((formatTwoDecimal((
                        getDoubleFromString(getInnerText(lstStatusAgentAssigned.get(agent))) /
                                lstTotalTargetDays.get(agent))) *
                        getDoubleFromString(getInnerText(lstStatusAgentAssigned.get(agent))))) + " to " +
                        getDoubleFromString(getInnerText(lstStatusAgentAssigned.get(agent))));
                bool = bool || getDoubleFromString(getInnerText(lstStatusAgentCompleted.get(agent))) >
                        formatTwoDecimal((getDoubleFromString(getInnerText(lstStatusAgentAssigned.get(agent))) /
                                lstTotalTargetDays.get(agent))) &&
                        getDoubleFromString(getInnerText(lstStatusAgentCompleted.get(agent)))
                                < getDoubleFromString(getInnerText(lstStatusAgentAssigned.get(agent)));
            } else {
                testInfoLog("Completed Topup so far", getInnerText(lstStatusAgentCompleted.get(agent)));
                testInfoLog("Assigned Topup", getInnerText(lstStatusAgentAssigned.get(agent)));
                testInfoLog("Target Days", String.valueOf(lstTotalTargetDays.get(agent)));
                testInfoLog("Completed Days", String.valueOf(lstCompletedTargetDays.get(agent)));
                testValidationLog("Expected Topup : between " + Math.ceil((formatTwoDecimal((
                        getDoubleFromString(getInnerText(lstStatusAgentAssigned.get(agent))) /
                                lstTotalTargetDays.get(agent))) *
                        getDoubleFromString(getInnerText(lstStatusAgentAssigned.get(agent))))) + " to " +
                        getDoubleFromString(getInnerText(lstStatusAgentAssigned.get(agent))));
                bool = bool && getDoubleFromString(getInnerText(lstStatusAgentCompleted.get(agent))) >
                        formatTwoDecimal((getDoubleFromString(getInnerText(lstStatusAgentAssigned.get(agent))) /
                                lstTotalTargetDays.get(agent))) &&
                        getDoubleFromString(getInnerText(lstStatusAgentCompleted.get(agent)))
                                < getDoubleFromString(getInnerText(lstStatusAgentAssigned.get(agent)));
            }
        }

        return bool;

    }

    public boolean verifyConsumerDelayCalculation() {

        boolean bool = false;

        updateShowList(driver, findElementByName(driver, "agentListTbl_length"), "All");

        List<Integer> lstCompletedTargetDays = new ArrayList<>();
        List<Integer> lstTotalTargetDays = new ArrayList<>();

        for (int agent = 0; agent < sizeOf(lstStatusAgentTargetDays); agent++) {
            lstTotalTargetDays.add(getIntegerFromString(getInnerText(lstStatusAgentTargetDays.get(agent)).split("/")[1]));
            lstCompletedTargetDays.add(getIntegerFromString(getInnerText(lstStatusAgentTargetDays.get(agent)).split("/")[0]));
        }

        for (int agent = 0; agent < sizeOf(lstStatusAgentName); agent++) {
            if (getDoubleFromString(getInnerText(lstStatusAgentCompleted.get(agent))) <
                    formatTwoDecimal((getDoubleFromString(getInnerText(lstStatusAgentAssigned.get(agent))) /
                            lstTotalTargetDays.get(agent)))) {
                testInfoLog("Completed Topup so far", getInnerText(lstStatusAgentCompleted.get(agent)));
                testInfoLog("Assigned Topup", getInnerText(lstStatusAgentAssigned.get(agent)));
                testInfoLog("Target Days", String.valueOf(lstTotalTargetDays.get(agent)));
                testInfoLog("Completed Days", String.valueOf(lstCompletedTargetDays.get(agent)));
                testConfirmationLog("Expected Topup : less than " + Math.ceil((formatTwoDecimal((getDoubleFromString(getInnerText(lstStatusAgentAssigned.get(agent))) /
                        lstTotalTargetDays.get(agent))) * getDoubleFromString(getInnerText(lstStatusAgentAssigned.get(agent))))));
                bool = bool || getDoubleFromString(getInnerText(lstStatusAgentCompleted.get(agent))) <
                        formatTwoDecimal((getDoubleFromString(getInnerText(lstStatusAgentAssigned.get(agent))) /
                                lstTotalTargetDays.get(agent)));
            } else {
                testInfoLog("Completed Topup so far", getInnerText(lstStatusAgentCompleted.get(agent)));
                testInfoLog("Assigned Topup", getInnerText(lstStatusAgentAssigned.get(agent)));
                testInfoLog("Target Days", String.valueOf(lstTotalTargetDays.get(agent)));
                testInfoLog("Completed Days", String.valueOf(lstCompletedTargetDays.get(agent)));
                testValidationLog("Expected Topup : less than " + Math.ceil((formatTwoDecimal((getDoubleFromString(getInnerText(lstStatusAgentAssigned.get(agent))) /
                        lstTotalTargetDays.get(agent))) * getDoubleFromString(getInnerText(lstStatusAgentAssigned.get(agent))))));
                bool = bool && getDoubleFromString(getInnerText(lstStatusAgentCompleted.get(agent))) <
                        formatTwoDecimal((getDoubleFromString(getInnerText(lstStatusAgentAssigned.get(agent))) /
                                lstTotalTargetDays.get(agent)));
            }
        }

        return bool;
    }

    public boolean verifyConsumerCompletedCalculation() {

        boolean bool = false;

        updateShowList(driver, findElementByName(driver, "agentListTbl_length"), "All");

        List<Integer> lstCompletedTargetDays = new ArrayList<>();
        List<Integer> lstTotalTargetDays = new ArrayList<>();

        for (int agent = 0; agent < sizeOf(lstStatusAgentTargetDays); agent++) {
            lstTotalTargetDays.add(getIntegerFromString(getInnerText(lstStatusAgentTargetDays.get(agent)).split("/")[1]));
            lstCompletedTargetDays.add(getIntegerFromString(getInnerText(lstStatusAgentTargetDays.get(agent)).split("/")[0]));
        }

        for (int agent = 0; agent < sizeOf(lstStatusAgentName); agent++) {
            if (getDoubleFromString(getInnerText(lstStatusAgentCompleted.get(agent)))
                    >= getDoubleFromString(getInnerText(lstStatusAgentAssigned.get(agent)))) {
                testInfoLog("Completed Topup so far", getInnerText(lstStatusAgentCompleted.get(agent)));
                testInfoLog("Assigned Topup", getInnerText(lstStatusAgentAssigned.get(agent)));
                testInfoLog("Target Days", String.valueOf(lstTotalTargetDays.get(agent)));
                testInfoLog("Completed Days", String.valueOf(lstCompletedTargetDays.get(agent)));
                testConfirmationLog("Expected Topup : more than or equal to " +
                        getDoubleFromString(getInnerText(lstStatusAgentAssigned.get(agent))));
                bool = bool || getDoubleFromString(getInnerText(lstStatusAgentCompleted.get(agent)))
                        >= getDoubleFromString(getInnerText(lstStatusAgentAssigned.get(agent)));
            } else {
                testInfoLog("Completed Topup so far", getInnerText(lstStatusAgentCompleted.get(agent)));
                testInfoLog("Assigned Topup", getInnerText(lstStatusAgentAssigned.get(agent)));
                testInfoLog("Target Days", String.valueOf(lstTotalTargetDays.get(agent)));
                testInfoLog("Completed Days", String.valueOf(lstCompletedTargetDays.get(agent)));
                testValidationLog("Expected Topup : more than or equal to " +
                        getDoubleFromString(getInnerText(lstStatusAgentAssigned.get(agent))));
                bool = bool && getDoubleFromString(getInnerText(lstStatusAgentCompleted.get(agent)))
                        >= getDoubleFromString(getInnerText(lstStatusAgentAssigned.get(agent)));
            }
        }

        return bool;
    }

    public boolean verifyStatusAgentNumberSorted() {

        List<String> agentNumberAfterSort = new ArrayList<>();
        List<String> agentNumberBeforeSort = SuperAgentDashboardIndexPage._agentNumberBeforeSort;

        for (WebElement name : lstStatusAgentMobile) {
            agentNumberAfterSort.add(getInnerText(name));
        }

        agentNumberBeforeSort.sort(
                (String o1, String o2) -> {
                    if (o1.equalsIgnoreCase(o2)) {
                        return o2.compareTo(o1);
                    }
                    return o1.toLowerCase().compareTo(o2.toLowerCase());
                }
        );

        System.out.println(agentNumberAfterSort);
        System.out.println(agentNumberBeforeSort);

        System.out.println(agentNumberBeforeSort.equals(agentNumberAfterSort));

        return agentNumberBeforeSort.equals(agentNumberAfterSort);

    }

    public boolean verifyStatusTopupAssignedSorted() {

        List<Integer> agentTopupAssigendAfterSort = new ArrayList<>();
        List<Integer> agentTopupAssigendBeforeSort = SuperAgentDashboardIndexPage._agentTopupAssignedBeforeSort;

        for (WebElement name : lstStatusAgentAssigned) {
            agentTopupAssigendAfterSort.add(getIntegerFromString(getInnerText(name)));
        }

        Collections.sort(agentTopupAssigendBeforeSort);

        System.out.println(agentTopupAssigendAfterSort);
        System.out.println(agentTopupAssigendBeforeSort);

        System.out.println(agentTopupAssigendBeforeSort.equals(agentTopupAssigendAfterSort));

        return agentTopupAssigendBeforeSort.equals(agentTopupAssigendAfterSort);

    }

    public boolean verifyStatusTopupCompletedSorted() {

        List<Integer> agentTopupCompletedAfterSort = new ArrayList<>();
        List<Integer> agentTopupCompletedBeforeSort = SuperAgentDashboardIndexPage._agentTopupCompletedBeforeSort;

        for (WebElement name : lstStatusAgentCompleted) {
            agentTopupCompletedAfterSort.add(getIntegerFromString(getInnerText(name)));
        }

        Collections.sort(agentTopupCompletedBeforeSort);

        System.out.println(agentTopupCompletedAfterSort);
        System.out.println(agentTopupCompletedBeforeSort);

        System.out.println(agentTopupCompletedBeforeSort.equals(agentTopupCompletedAfterSort));

        return agentTopupCompletedBeforeSort.equals(agentTopupCompletedAfterSort);

    }

    public boolean verifyStatusTargetDaysSorted() {

        List<Integer> agentTargetDaysAfterSort = new ArrayList<>();
        List<Integer> agentTargetDaysBeforeSort = SuperAgentDashboardIndexPage._agentTopupTargetDaysBeforeSort;

        for (WebElement cell : lstStatusAgentTargetDays) {
            agentTargetDaysAfterSort.add(getIntegerFromString(getInnerText(cell).split("/")[0]));
        }

        Collections.sort(agentTargetDaysBeforeSort);

        System.out.println(agentTargetDaysAfterSort);
        System.out.println(agentTargetDaysBeforeSort);

        System.out.println(agentTargetDaysBeforeSort.equals(agentTargetDaysAfterSort));

        return agentTargetDaysBeforeSort.equals(agentTargetDaysAfterSort);

    }

    public boolean verifyFilterScreenDisplay() {

        updateShowList(driver, findElementByName(driver, "dataTblSA_length"), "All");

        return isElementDisplay(btnFilterSearch) && isElementDisplay(btnFilterClearAll);

    }

    public boolean verifyRandomDataFiltered() {
        return getText(lblNoFilterDataAvailable).equalsIgnoreCase(NO_FILTER_DATA_AVAILABLE_DASHBOARD);
    }

    public boolean verifyNameFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement number : lstAgentMobileNumber) {
            finalFilterResult.add(getInnerText(number));
        }

        System.out.println(finalFilterResult);
        System.out.println(SuperAgentDashboardIndexPage._filterAgentPhone);

        return finalFilterResult.contains(SuperAgentDashboardIndexPage._filterAgentPhone);
    }

    public boolean verifyIdFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement id : lstAgentId) {
            finalFilterResult.add(getInnerText(id));
        }

        System.out.println(finalFilterResult);
        System.out.println(SuperAgentDashboardIndexPage._filterAgentId);

        return finalFilterResult.contains(SuperAgentDashboardIndexPage._filterAgentId);
    }

    public boolean verifyPhoneNumberFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement number : lstAgentMobileNumber) {
            finalFilterResult.add(getInnerText(number));
        }

        System.out.println(finalFilterResult);
        System.out.println(SuperAgentDashboardIndexPage._filterAgentPhone);

        return finalFilterResult.contains(SuperAgentDashboardIndexPage._filterAgentPhone);
    }

    public boolean verifyStatusFiltered() {

        List<String> finalFilterResult = new ArrayList<>();

        for (WebElement status : lstAgentStatus) {
            finalFilterResult.add(getInnerText(status));
        }

        System.out.println(finalFilterResult);
        System.out.println(SuperAgentDashboardIndexPage._filterStatus);
        System.out.println(SuperAgentDashboardIndexPage._agentStatusBeforeSort);

        if (isListEmpty(finalFilterResult))
            return !SuperAgentDashboardIndexPage._agentStatusBeforeSort.contains(SuperAgentDashboardIndexPage._filterStatus)
                    && getText(lblNoFilterDataAvailable).equalsIgnoreCase(NO_FILTER_DATA_AVAILABLE_DASHBOARD);
        else
            return finalFilterResult.stream().distinct().limit(2).count() <= 1 &&
                    finalFilterResult.contains(SuperAgentDashboardIndexPage._filterStatus);
    }

    public boolean verifyValidationForInvalidSearch() {

        testValidationLog(getInnerText(lblNoRecordsFound));

        boolean bool = getInnerText(lblNoRecordsFound).equalsIgnoreCase(NO_SEARCH_RECORD_FOUND);

        type(txtInputSearch, "x");
        txtInputSearch.clear();

        return bool;
    }

    public boolean verifyTopUpsSorted() {

        List<Double> commissionTopUpAfterSort = new ArrayList<>();
        List<Double> commissionTopUpBeforeSort = SuperAgentDashboardIndexPage._commissionTopUpBeforeSort;

        for (WebElement name : lstCommissionTopUp) {
            commissionTopUpAfterSort.add(getDoubleFromString(getInnerText(name)));
        }

        Collections.sort(commissionTopUpBeforeSort);

        System.out.println(commissionTopUpAfterSort);
        System.out.println(commissionTopUpBeforeSort);

        System.out.println(commissionTopUpBeforeSort.equals(commissionTopUpAfterSort));

        return commissionTopUpBeforeSort.equals(commissionTopUpAfterSort);
    }

    public boolean verifyCommissionSorted() {

        List<Double> commissionAmountAfterSort = new ArrayList<>();
        List<Double> commissionAmountBeforeSort = SuperAgentDashboardIndexPage._commissionAmountBeforeSort;

        for (WebElement name : lstCommissionAmount) {
            commissionAmountAfterSort.add(getDoubleFromString(getInnerText(name)));
        }

        Collections.sort(commissionAmountBeforeSort);

        System.out.println(commissionAmountAfterSort);
        System.out.println(commissionAmountBeforeSort);

        System.out.println(commissionAmountBeforeSort.equals(commissionAmountAfterSort));

        return commissionAmountBeforeSort.equals(commissionAmountAfterSort);
    }

    public boolean verifyCommissionDateTimeSorted() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        List<LocalDateTime> commissionDateTimeAfterSort = new ArrayList<>();
        List<LocalDateTime> commissionDateTimeBeforeSort = SuperAgentDashboardIndexPage._commissionDateTimeBeforeSort;

        for (WebElement name : lstCommissionTxTime) {
            commissionDateTimeAfterSort.add(LocalDateTime.parse(getInnerText(name), format));
        }

        Collections.sort(commissionDateTimeBeforeSort);

        System.out.println(commissionDateTimeAfterSort);
        System.out.println(commissionDateTimeBeforeSort);

        System.out.println(commissionDateTimeBeforeSort.equals(commissionDateTimeAfterSort));

        return commissionDateTimeBeforeSort.equals(commissionDateTimeAfterSort);
    }

    public boolean verifySearchSuccessful() {

        boolean bool = getInnerText(lstCommissionTxTime.get(0)).contains(SuperAgentDashboardIndexPage._searchCriteria) &&
                sizeOf(lstCommissionTxTime) == 1;

        type(txtInputSearch, "");

        return bool;

    }

    public boolean verifyAgentSearchSuccessful() {

        boolean bool = getInnerText(lstAgentId.get(0)).contains(SuperAgentDashboardIndexPage._searchCriteria) &&
                sizeOf(lstAgentId) == 1;

        type(txtInputSearch, "");

        return bool;

    }

    public boolean verifyAgentListSearchSuccessful() {

        boolean bool = getInnerText(lstAddedAgentId.get(0)).contains(SuperAgentDashboardIndexPage._searchCriteria) &&
                sizeOf(lstAddedAgentId) == 1;

        type(txtInputSearch, "");

        return bool;

    }
}
