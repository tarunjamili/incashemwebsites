package com.incashme.superAgent.index;

import com.framework.init.SeleniumInit;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Rahul R.
 * Date: 2019-04-01
 * Time: 11:25
 * Project Name: InCashMe
 */
public class SuperAgentCommissionIndex extends SeleniumInit {

    @Test
    public void superAgent_TotalCommissionCalculation() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_014 :: To verify Super Agent can see the list of received commission on the screen with valid commission amount.");
        superAgentLoginIndexPage.getVersion();

        superAgentLoginVerification = superAgentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (superAgentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify user can see the Dashboard screen with details.");
        if (superAgentDashboardVerification.verifyDashboardDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnCommissionMenu();

        testVerifyLog("Verify user can see the Commission Screen.");
        if (superAgentCommissionVerification.verifyCommissionScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentCommissionVerification = superAgentCommissionIndexPage.clickOnTotalButton();

        superAgentCommissionVerification = superAgentCommissionIndexPage.getTotalCommissionAmount();

        if (superAgentCommissionIndexPage.isCommissionDataDisplay()) {
            testVerifyLog("Verify user can see the commission amount total properly.");
            if (superAgentCommissionVerification.verifyTotalCommissionAmount()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            testVerifyLog("Verify commission calculated properly for the top up.");
            if (superAgentCommissionVerification.verifyCommissionCalculation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        } else {
            testWarningLog("No Commission!");
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    //TODO - Deprecated Test as Filter functionality is removed from the Commission
    @Test(enabled = false)
    public void superAgent_CommissionSortAndFilter() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_015 :: To verify Super Agent can sort and Filter the Commission details.");
        superAgentLoginIndexPage.getVersion();

        superAgentLoginVerification = superAgentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (superAgentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnCommissionMenu();

        superAgentCommissionVerification = superAgentCommissionIndexPage.clickOnTotalButton();

        superAgentCommissionVerification = superAgentCommissionIndexPage.getTotalCommissionAmount();

        if (superAgentCommissionIndexPage.isCommissionDataDisplay()) {

            superAgentCommissionVerification = superAgentCommissionIndexPage.clickTopUpToSort();

            testVerifyLog("Verify Top ups are sorted successfully.");
            if (superAgentCommissionVerification.verifyTopUpsSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentCommissionVerification = superAgentCommissionIndexPage.clickCommissionToSort();

            testVerifyLog("Verify Commission are sorted successfully.");
            if (superAgentCommissionVerification.verifyCommissionSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentCommissionVerification = superAgentCommissionIndexPage.clickOnDateTimeToSort();

            testVerifyLog("Verify Date & Time are sorted successfully.");
            if (superAgentCommissionVerification.verifyCommissionDateTimeSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentCommissionVerification = superAgentCommissionIndexPage.clickOnFilterButton();

            testVerifyLog("Verify user can see the Filter screen.");
            if (superAgentCommissionVerification.verifyFilterScreenDisplay()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentCommissionVerification = superAgentCommissionIndexPage.getCommissionDetailsForFilter();

            superAgentCommissionVerification = superAgentCommissionIndexPage.filterFirstName(true);

            superAgentCommissionVerification = superAgentCommissionIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify First Name filter successfully.");
            if (superAgentCommissionVerification.verifyRandomDataFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentCommissionVerification = superAgentCommissionIndexPage.filterFirstName(false);

            superAgentCommissionVerification = superAgentCommissionIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify First Name filter successfully.");
            if (superAgentCommissionVerification.verifyFirstNameFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentCommissionVerification = superAgentCommissionIndexPage.clearAllFilterDetails();

            superAgentCommissionVerification = superAgentCommissionIndexPage.filterLastName(true);

            superAgentCommissionVerification = superAgentCommissionIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify Last Name filter successfully.");
            if (superAgentCommissionVerification.verifyRandomDataFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentCommissionVerification = superAgentCommissionIndexPage.filterLastName(false);

            superAgentCommissionVerification = superAgentCommissionIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify Last Name filter successfully.");
            if (superAgentCommissionVerification.verifyLastNameFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentCommissionVerification = superAgentCommissionIndexPage.clearAllFilterDetails();

            superAgentCommissionVerification = superAgentCommissionIndexPage.filterAgentId(true);

            superAgentCommissionVerification = superAgentCommissionIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify Agent ID filter successfully.");
            if (superAgentCommissionVerification.verifyRandomDataFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentCommissionVerification = superAgentCommissionIndexPage.filterAgentId(false);

            superAgentCommissionVerification = superAgentCommissionIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify Agent ID filter successfully.");
            if (superAgentCommissionVerification.verifyIdFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentCommissionVerification = superAgentCommissionIndexPage.clearAllFilterDetails();

            superAgentCommissionVerification = superAgentCommissionIndexPage.enterTopUpAmountRange(true);

            superAgentCommissionVerification = superAgentCommissionIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify validation message for Max amount is less than Min amount.");
            if (superAgentCommissionVerification.verifyMaxMinTopupValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentCommissionVerification = superAgentCommissionIndexPage.clearAllFilterDetails();

            superAgentCommissionVerification = superAgentCommissionIndexPage.enterTopUpAmount("from");

            superAgentCommissionVerification = superAgentCommissionIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify user can filter by min amount.");
            if (superAgentCommissionVerification.verifyTopUpFromFieldFilter()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentCommissionVerification = superAgentCommissionIndexPage.clearAllFilterDetails();

            superAgentCommissionVerification = superAgentCommissionIndexPage.enterTopUpAmount("to");

            superAgentCommissionVerification = superAgentCommissionIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify user can filter by max amount.");
            if (superAgentCommissionVerification.verifyTopUpToFieldFilter()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentCommissionVerification = superAgentCommissionIndexPage.clearAllFilterDetails();

            superAgentCommissionVerification = superAgentCommissionIndexPage.enterTopUpAmountRange(false);

            superAgentCommissionVerification = superAgentCommissionIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify user can filter with From and To Amount.");
            if (superAgentCommissionVerification.verifyTopUpMaxMinFilter()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentCommissionVerification = superAgentCommissionIndexPage.clearAllFilterDetails();

            superAgentCommissionVerification = superAgentCommissionIndexPage.enterCommissionAmountRange(true);

            superAgentCommissionVerification = superAgentCommissionIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify validation message for Max amount is less than Min amount.");
            if (superAgentCommissionVerification.verifyMaxMinCommissionValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentCommissionVerification = superAgentCommissionIndexPage.clearAllFilterDetails();

            superAgentCommissionVerification = superAgentCommissionIndexPage.enterCommissionAmount("from");

            superAgentCommissionVerification = superAgentCommissionIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify user can filter by min amount.");
            if (superAgentCommissionVerification.verifyCommissionFromFieldFilter()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentCommissionVerification = superAgentCommissionIndexPage.clearAllFilterDetails();

            superAgentCommissionVerification = superAgentCommissionIndexPage.enterCommissionAmount("to");

            superAgentCommissionVerification = superAgentCommissionIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify user can filter by max amount.");
            if (superAgentCommissionVerification.verifyCommissionToFieldFilter()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentCommissionVerification = superAgentCommissionIndexPage.clearAllFilterDetails();

            superAgentCommissionVerification = superAgentCommissionIndexPage.enterCommissionAmountRange(false);

            superAgentCommissionVerification = superAgentCommissionIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify user can filter with From and To Amount.");
            if (superAgentCommissionVerification.verifyCommissionMaxMinFilter()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void superAgent_CommissionSearchForTables() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_021 :: To verify Super Agent can search the details in Commission Table.");
        superAgentLoginIndexPage.getVersion();

        superAgentLoginVerification = superAgentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (superAgentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnCommissionMenu();

        superAgentCommissionVerification = superAgentCommissionIndexPage.clickOnTotalButton();

        superAgentCommissionVerification = superAgentCommissionIndexPage.getTotalCommissionAmount();

        if (superAgentCommissionIndexPage.isCommissionDataDisplay()) {

            superAgentCommissionVerification = superAgentCommissionIndexPage.enterSearchIDCriteria(false);

            testVerifyLog("Verify search details display properly.");
            if (superAgentCommissionVerification.verifySearchSuccessful()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            if (!isPositiveExecution) {
                superAgentCommissionVerification = superAgentCommissionIndexPage.enterSearchIDCriteria(true);

                testVerifyLog("Verify validation message display for the invalid search criteria.");
                if (superAgentCommissionVerification.verifyValidationForInvalidSearch()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }
        } else {
            testWarningLog("No Commission!");
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");

    }

}
