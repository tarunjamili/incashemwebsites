package com.incashme.superAgent.index;

import com.framework.init.SeleniumInit;
import com.incashme.superAgent.indexpage.SuperAgentLoginIndexPage;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Rahul R.
 * Date: 2019-03-25
 * Time: 11:48
 * Project Name: InCashMe
 */
public class SuperAgentDashboardIndex extends SeleniumInit {

    @Test
    public void superAgent_DashboardMenuVerification() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_004 :: To verify Dashboard Screen and Menu Links.<br>" +
                "ICM_SC_013 :: To verify Super Agent can see the Balance on the screen.");
        superAgentLoginIndexPage.getVersion();

        superAgentLoginVerification = superAgentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (superAgentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify user can see the Dashboard screen with details.");
        if (superAgentDashboardVerification.verifyDashboardDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify user can see the Dashboard screen buttons.");
        if (superAgentDashboardVerification.verifyDashboardButtons()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentDashboardVerification = superAgentDashboardIndexPage.expandDashboardMenu();
        superAgentStatementVerification = superAgentStatementIndexPage.clickOnStatementMenu();

        testVerifyLog("Verify user can see the Statement Screen with list of statements.");
        if (superAgentStatementVerification.verifyStatementsScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnBalanceMenu();

        testVerifyLog("Verify user can see the Balance Screen with current balance.");
        if (superAgentDashboardVerification.verifyBalanceScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnCommissionMenu();

        testVerifyLog("Verify user can see the Commission Screen.");
        if (superAgentCommissionVerification.verifyCommissionScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnAgentsListMenu();

        testVerifyLog("Verify user can see the Agents List Screen.");
        if (superAgentAgentListVerification.verifyAgentsListScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentAgentListVerification = superAgentAgentListIndexPage.clickOnCreateNewAgentMenu();

        testVerifyLog("Verify user can see the Create New Agent Step 1.");
        if (superAgentAgentListVerification.verifyCreateAgentScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnDashboardIcon();
        superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnCreateNewAgentButton();

        testVerifyLog("Verify user can see the Create New Agent Step 1.");
        if (superAgentAgentListVerification.verifyCreateAgentScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }


        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");

    }

    @Test
    public void superAgent_NewAgentLists() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_005 :: To verify Added Agent and Agent Target details display properly. <br>" +
                "ICM_SC_009 :: To verify Super Agent can see the Agent List and Agent Profile Details on the screen.");
        superAgentLoginIndexPage.getVersion();

        superAgentLoginVerification = superAgentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (superAgentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify user can see the Dashboard screen with details.");
        if (superAgentDashboardVerification.verifyDashboardDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentDashboardVerification = superAgentDashboardIndexPage.getTodayAgentDetailsFromDashboard();

        testVerifyLog("Verify user can see the Agent Commission Details on the Dashboard.");
        if (superAgentDashboardVerification.verifyCommissionDetailsOnDashboard()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (superAgentDashboardIndexPage.isCommissionDataDisplay()) {
            testVerifyLog("Verify user can see the commission amount total properly.");
            if (superAgentDashboardVerification.verifyTotalCommissionAmount()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            testVerifyLog("Verify commission calculated properly for the top up.");
            if (superAgentDashboardVerification.verifyCommissionCalculation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }


            superAgentDashboardVerification = superAgentDashboardIndexPage.clickTopUpToSort();

            testVerifyLog("Verify Top ups are sorted successfully.");
            if (superAgentDashboardVerification.verifyTopUpsSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickCommissionToSort();

            testVerifyLog("Verify Commission are sorted successfully.");
            if (superAgentDashboardVerification.verifyCommissionSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnCommissionDateTimeToSort();

            testVerifyLog("Verify Date & Time are sorted successfully.");
            if (superAgentDashboardVerification.verifyCommissionDateTimeSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

        }

        superAgentDashboardVerification = superAgentDashboardIndexPage.openAddedAgentListFromDashboard();

        testVerifyLog("Verify list of added agent display on the screen.");
        if (superAgentDashboardVerification.verifyTodayAddedAgentDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (superAgentDashboardIndexPage.isTodayAgentListDisplay()) {

            superAgentDashboardVerification = superAgentDashboardIndexPage.openAddedAgent();

            testVerifyLog("Verify agent details screen open.");
            if (superAgentDashboardVerification.verifyAgentDetailsScreenDisplay()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnBackButton();

            superAgentDashboardVerification = superAgentDashboardIndexPage.openAddedAgentListFromDashboard();

        }

        testVerifyLog("Verify Target Agent Count in Dashboard.");
        if (superAgentDashboardVerification.verifyDashboardAgentCount()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentDashboardVerification = superAgentDashboardIndexPage.openAgentTargetDetails();

        superAgentDashboardVerification = superAgentDashboardIndexPage.getTotalAgentDetails();

        testVerifyLog("Verify Agent Target Details Screen Display.");
        if (superAgentDashboardVerification.verifyAgentTargetDetailsScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify Agent List count is display properly.");
        if (superAgentDashboardVerification.verifyAgentListCount()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (superAgentDashboardIndexPage.isAgentListDisplay()) {

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnMOREButton();

            testVerifyLog("Verify agent details screen open.");
            if (superAgentDashboardVerification.verifyAgentDetailsScreenDisplay()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnBackButton();

        }

        superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnAgentListButton();

        testVerifyLog("Verify list of added agent display on the screen.");
        if (superAgentDashboardVerification.verifyTotalAddedAgentDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (superAgentDashboardIndexPage.isAgentListDisplay()) {

            superAgentDashboardVerification = superAgentDashboardIndexPage.openAddedAgent();

            testVerifyLog("Verify agent details screen open.");
            if (superAgentDashboardVerification.verifyAgentDetailsScreenDisplay()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnBackButton();

        }

        superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnFilterButton();

        if (superAgentDashboardIndexPage.isAgentListDisplay()) {

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnAgentNameToSort();

            testVerifyLog("Verify Agent Name are sorted successfully.");
            if (superAgentDashboardVerification.verifyAgentNameSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnPhoneNumberToSort();

            testVerifyLog("Verify Agent Mobile Number are sorted successfully.");
            if (superAgentDashboardVerification.verifyAgentNumberSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnStatusToSort();

            testVerifyLog("Verify Agent Status are sorted successfully.");
            if (superAgentDashboardVerification.verifyAgentStatusSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnDateTimeToSort();

            testVerifyLog("Verify Date & Time are sorted successfully.");
            if (superAgentDashboardVerification.verifyAgentDateTimeSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void superAgent_ProfileDetailsVerification() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_006 :: To verify Super Agent profile details and profile links.");
        superAgentLoginIndexPage.getVersion();

        superAgentLoginVerification = superAgentLoginIndexPage.loginAs(username, password);

        SuperAgentLoginIndexPage.getLoginTime();

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (superAgentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentDashboardVerification = superAgentDashboardIndexPage.openUserProfile();

        testVerifyLog("Verify user can see the profile details on the screen.");
        if (superAgentDashboardVerification.verifyUserProfileDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentDashboardVerification = superAgentDashboardIndexPage.closeProfilePopUp();
        superAgentLoginVerification = superAgentLoginIndexPage.clickOnUserName();
        superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnChangePassword();

        testVerifyLog("Verify user can see the Change Password screen.");
        if (superAgentDashboardVerification.verifyChangePasswordScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentDashboardVerification = superAgentDashboardIndexPage.closePasswordPopUp();
        superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnPrivacyPolicy();

        testVerifyLog("Verify user can see the Privacy Policy screen.");
        if (superAgentDashboardVerification.verifyPrivacyPolicyScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnTermsCondition();

        testVerifyLog("Verify user can see the Terms & Condition screen.");
        if (superAgentDashboardVerification.verifyTnCScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnLoginHistory();

        testVerifyLog("Verify user can see the Login History details.");
        if (superAgentDashboardVerification.verifyLoginHistory()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentDashboardVerification = superAgentDashboardIndexPage.enterLoginIDSearch();

        testVerifyLog("Verify user can see the validation for invalid search message.");
        if (superAgentDashboardVerification.verifyValidationForInvalidSearch()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentLoginVerification = superAgentLoginIndexPage.clickOnLogout();

        testVerifyLog("Verify user logout successfully from the Application.");
        if (superAgentLoginVerification.verifyUserLogoutSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }


        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test(enabled = false)
    public void superAgent_ChangePassword() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_007 :: To verify Super Agent can change password successfully.");
        superAgentLoginIndexPage.getVersion();

        superAgentLoginVerification = superAgentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (superAgentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentLoginVerification = superAgentLoginIndexPage.clickOnUserName();
        superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnChangePassword();

        testVerifyLog("Verify user can see the Change Password screen.");
        if (superAgentDashboardVerification.verifyChangePasswordScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentDashboardVerification = superAgentDashboardIndexPage.changeInvalidCurrentPassword();

        testVerifyLog("Verify validation message for the invalid current password.");
        if (superAgentDashboardVerification.verifyInvalidCurrentPasswordValidation()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentDashboardVerification = superAgentDashboardIndexPage.changeInvalidNewPassword();

        testVerifyLog("Verify validation message for the invalid new password.");
        if (superAgentDashboardVerification.verifyInvalidNewPasswordValidation()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentDashboardVerification = superAgentDashboardIndexPage.closePasswordPopUp();
        superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnChangePassword();
        superAgentDashboardVerification = superAgentDashboardIndexPage.changeInvalidConfirmPassword();

        testVerifyLog("Verify validation message for the different new and confirm password.");
        if (superAgentDashboardVerification.verifyInvalidConfirmPasswordValidation()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentDashboardVerification = superAgentDashboardIndexPage.changeAsPastPassword();

        testVerifyLog("Verify validation message for the past password as new password.");
        if (superAgentDashboardVerification.verifyPastPasswordValidation()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

       /* superAgentDashboardVerification = superAgentDashboardIndexPage.changeNewPassword();

        testVerifyLog("Verify confirmation message for the Password changed successfully.");
        if (superAgentDashboardVerification.verifyPasswordChangeSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify user logout successfully from the Application.");
        if (superAgentLoginVerification.verifyUserLogoutSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentLoginVerification = superAgentLoginIndexPage.loginAs("testsuperagent121@mailinator.com", "Nagesh@2");

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (superAgentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentLoginVerification = superAgentLoginIndexPage.clickOnUserName();
        superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnChangePassword();

        testVerifyLog("Verify user can see the Change Password screen.");
        if (superAgentDashboardVerification.verifyChangePasswordScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }*/

        superAgentDashboardVerification = superAgentDashboardIndexPage.changeInvalidCurrentPassword();

        testVerifyLog("Verify validation message for the invalid current password.");
        if (superAgentDashboardVerification.verifyInvalidCurrentPasswordSecondTime()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentDashboardVerification = superAgentDashboardIndexPage.changeInvalidCurrentPassword();

        testVerifyLog("Verify user account blocked after three invalid attempts.");
        if (superAgentDashboardVerification.verifyAccountIsBlocked()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify user logout successfully from the Application.");
        if (superAgentLoginVerification.verifyUserLogoutSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentLoginVerification = superAgentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can see the account locked validation.");
        if (superAgentLoginVerification.verifyUserLockedSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void superAgent_AgentUsersTopupDetails() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_008 :: To verify Super Agent can see the Top up status, Consumer Status & Merchant Status.");
        superAgentLoginIndexPage.getVersion();

        superAgentLoginVerification = superAgentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (superAgentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify user can see the Dashboard screen with details.");
        if (superAgentDashboardVerification.verifyDashboardDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnConsumerTopupsStatus();

        testVerifyLog("Verify user can see the Consumer Top up Status with counts.");
        if (superAgentDashboardVerification.verifyConsumerTopUpStatusCount()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (superAgentDashboardIndexPage.isOntimeDisplay()) {

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnOnTimeButton();

            testVerifyLog("Verify user can see the Ontime consumer top ups properly.");
            if (superAgentDashboardVerification.verifyConsumerOntimeCalculation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnStatusPhoneNumberToSort();

            testVerifyLog("Verify Agent Mobile Number are sorted successfully.");
            if (superAgentDashboardVerification.verifyStatusAgentNumberSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnStatusTopupAssignedToSort();

            testVerifyLog("Verify Agent Topup Assigned are sorted successfully.");
            if (superAgentDashboardVerification.verifyStatusTopupAssignedSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnStatusTopupCompletedToSort();

            testVerifyLog("Verify Agent Topup Completed are sorted successfully.");
            if (superAgentDashboardVerification.verifyStatusTopupCompletedSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnStatusTargetDaysToSort();

            testVerifyLog("Verify Agent Target Days are sorted successfully.");
            if (superAgentDashboardVerification.verifyStatusTargetDaysSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        if (superAgentDashboardIndexPage.isDelayDisplay()) {

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnDelayButton();

            testVerifyLog("Verify user can see the Delay consumer top ups properly.");
            if (superAgentDashboardVerification.verifyConsumerDelayCalculation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnStatusPhoneNumberToSort();

            testVerifyLog("Verify Agent Mobile Number are sorted successfully.");
            if (superAgentDashboardVerification.verifyStatusAgentNumberSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnStatusTopupAssignedToSort();

            testVerifyLog("Verify Agent Topup Assigned are sorted successfully.");
            if (superAgentDashboardVerification.verifyStatusTopupAssignedSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnStatusTopupCompletedToSort();

            testVerifyLog("Verify Agent Topup Completed are sorted successfully.");
            if (superAgentDashboardVerification.verifyStatusTopupCompletedSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnStatusTargetDaysToSort();

            testVerifyLog("Verify Agent Target Days are sorted successfully.");
            if (superAgentDashboardVerification.verifyStatusTargetDaysSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

        }

        if (superAgentDashboardIndexPage.isCompletedDisplay()) {

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnCompletedButton();

            testVerifyLog("Verify user can see the Delay consumer top ups properly.");
            if (superAgentDashboardVerification.verifyConsumerCompletedCalculation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnStatusPhoneNumberToSort();

            testVerifyLog("Verify Agent Mobile Number are sorted successfully.");
            if (superAgentDashboardVerification.verifyStatusAgentNumberSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnStatusTopupAssignedToSort();

            testVerifyLog("Verify Agent Topup Assigned are sorted successfully.");
            if (superAgentDashboardVerification.verifyStatusTopupAssignedSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnStatusTopupCompletedToSort();

            testVerifyLog("Verify Agent Topup Completed are sorted successfully.");
            if (superAgentDashboardVerification.verifyStatusTopupCompletedSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnStatusTargetDaysToSort();

            testVerifyLog("Verify Agent Target Days are sorted successfully.");
            if (superAgentDashboardVerification.verifyStatusTargetDaysSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnDashboardIcon();

        superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnCreateConsumerStatus();

        testVerifyLog("Verify user can see the Create Consumer Status with counts.");
        if (superAgentDashboardVerification.verifyCreateConsumerStatusCount()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (superAgentDashboardIndexPage.isOntimeDisplay()) {

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnOnTimeButton();

            testVerifyLog("Verify user can see the Ontime consumer top ups properly.");
            if (superAgentDashboardVerification.verifyConsumerOntimeCalculation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnStatusPhoneNumberToSort();

            testVerifyLog("Verify Agent Mobile Number are sorted successfully.");
            if (superAgentDashboardVerification.verifyStatusAgentNumberSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnStatusTopupAssignedToSort();

            testVerifyLog("Verify Agent Topup Assigned are sorted successfully.");
            if (superAgentDashboardVerification.verifyStatusTopupAssignedSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnStatusTopupCompletedToSort();

            testVerifyLog("Verify Agent Topup Completed are sorted successfully.");
            if (superAgentDashboardVerification.verifyStatusTopupCompletedSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnStatusTargetDaysToSort();

            testVerifyLog("Verify Agent Target Days are sorted successfully.");
            if (superAgentDashboardVerification.verifyStatusTargetDaysSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        if (superAgentDashboardIndexPage.isDelayDisplay()) {

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnDelayButton();

            testVerifyLog("Verify user can see the Delay consumer top ups properly.");
            if (superAgentDashboardVerification.verifyConsumerDelayCalculation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnStatusPhoneNumberToSort();

            testVerifyLog("Verify Agent Mobile Number are sorted successfully.");
            if (superAgentDashboardVerification.verifyStatusAgentNumberSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnStatusTopupAssignedToSort();

            testVerifyLog("Verify Agent Topup Assigned are sorted successfully.");
            if (superAgentDashboardVerification.verifyStatusTopupAssignedSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnStatusTopupCompletedToSort();

            testVerifyLog("Verify Agent Topup Completed are sorted successfully.");
            if (superAgentDashboardVerification.verifyStatusTopupCompletedSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnStatusTargetDaysToSort();

            testVerifyLog("Verify Agent Target Days are sorted successfully.");
            if (superAgentDashboardVerification.verifyStatusTargetDaysSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

        }

        if (superAgentDashboardIndexPage.isCompletedDisplay()) {

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnCompletedButton();

            testVerifyLog("Verify user can see the Delay consumer top ups properly.");
            if (superAgentDashboardVerification.verifyConsumerCompletedCalculation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnStatusPhoneNumberToSort();

            testVerifyLog("Verify Agent Mobile Number are sorted successfully.");
            if (superAgentDashboardVerification.verifyStatusAgentNumberSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnStatusTopupAssignedToSort();

            testVerifyLog("Verify Agent Topup Assigned are sorted successfully.");
            if (superAgentDashboardVerification.verifyStatusTopupAssignedSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnStatusTopupCompletedToSort();

            testVerifyLog("Verify Agent Topup Completed are sorted successfully.");
            if (superAgentDashboardVerification.verifyStatusTopupCompletedSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnStatusTargetDaysToSort();

            testVerifyLog("Verify Agent Target Days are sorted successfully.");
            if (superAgentDashboardVerification.verifyStatusTargetDaysSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnDashboardIcon();

        superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnCreateMerchantStatus();

        testVerifyLog("Verify user can see the Create Merchant Status with counts.");
        if (superAgentDashboardVerification.verifyCreateMerchantStatusCount()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (superAgentDashboardIndexPage.isOntimeDisplay()) {

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnOnTimeButton();

            testVerifyLog("Verify user can see the Ontime consumer top ups properly.");
            if (superAgentDashboardVerification.verifyConsumerOntimeCalculation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnStatusPhoneNumberToSort();

            testVerifyLog("Verify Agent Mobile Number are sorted successfully.");
            if (superAgentDashboardVerification.verifyStatusAgentNumberSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnStatusTopupAssignedToSort();

            testVerifyLog("Verify Agent Topup Assigned are sorted successfully.");
            if (superAgentDashboardVerification.verifyStatusTopupAssignedSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnStatusTopupCompletedToSort();

            testVerifyLog("Verify Agent Topup Completed are sorted successfully.");
            if (superAgentDashboardVerification.verifyStatusTopupCompletedSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnStatusTargetDaysToSort();

            testVerifyLog("Verify Agent Target Days are sorted successfully.");
            if (superAgentDashboardVerification.verifyStatusTargetDaysSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        if (superAgentDashboardIndexPage.isDelayDisplay()) {

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnDelayButton();

            testVerifyLog("Verify user can see the Delay consumer top ups properly.");
            if (superAgentDashboardVerification.verifyConsumerDelayCalculation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnStatusPhoneNumberToSort();

            testVerifyLog("Verify Agent Mobile Number are sorted successfully.");
            if (superAgentDashboardVerification.verifyStatusAgentNumberSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnStatusTopupAssignedToSort();

            testVerifyLog("Verify Agent Topup Assigned are sorted successfully.");
            if (superAgentDashboardVerification.verifyStatusTopupAssignedSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnStatusTopupCompletedToSort();

            testVerifyLog("Verify Agent Topup Completed are sorted successfully.");
            if (superAgentDashboardVerification.verifyStatusTopupCompletedSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnStatusTargetDaysToSort();

            testVerifyLog("Verify Agent Target Days are sorted successfully.");
            if (superAgentDashboardVerification.verifyStatusTargetDaysSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

        }

        if (superAgentDashboardIndexPage.isCompletedDisplay()) {

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnCompletedButton();

            testVerifyLog("Verify user can see the Delay consumer top ups properly.");
            if (superAgentDashboardVerification.verifyConsumerCompletedCalculation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnStatusPhoneNumberToSort();

            testVerifyLog("Verify Agent Mobile Number are sorted successfully.");
            if (superAgentDashboardVerification.verifyStatusAgentNumberSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnStatusTopupAssignedToSort();

            testVerifyLog("Verify Agent Topup Assigned are sorted successfully.");
            if (superAgentDashboardVerification.verifyStatusTopupAssignedSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnStatusTopupCompletedToSort();

            testVerifyLog("Verify Agent Topup Completed are sorted successfully.");
            if (superAgentDashboardVerification.verifyStatusTopupCompletedSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnStatusTargetDaysToSort();

            testVerifyLog("Verify Agent Target Days are sorted successfully.");
            if (superAgentDashboardVerification.verifyStatusTargetDaysSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");

    }

    @Test
    public void superAgent_AgentListFilter() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_010 :: To verify Super Agent can sort and Filter the Agent details.");
        superAgentLoginIndexPage.getVersion();

        superAgentLoginVerification = superAgentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (superAgentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentDashboardVerification = superAgentDashboardIndexPage.openAgentTargetDetails();

        superAgentDashboardVerification = superAgentDashboardIndexPage.getTotalAgentDetails();

        if (superAgentDashboardIndexPage.isAgentListDisplay()) {

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnFilterButton();

            testVerifyLog("Verify user can see the Filter screen.");
            if (superAgentDashboardVerification.verifyFilterScreenDisplay()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.getAgentDetailsForFilter();

            if (!isPositiveExecution) {
                superAgentDashboardVerification = superAgentDashboardIndexPage.filterFirstName(true);

                superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnFilterSearchButton();

                testVerifyLog("Verify First Name filter successfully.");
                if (superAgentDashboardVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.filterFirstName(false);

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify First Name filter successfully.");
            if (superAgentDashboardVerification.verifyNameFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clearAllFilterDetails();

            if (!isPositiveExecution) {
                superAgentDashboardVerification = superAgentDashboardIndexPage.filterLastName(true);

                superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnFilterSearchButton();

                testVerifyLog("Verify Last Name filter successfully.");
                if (superAgentDashboardVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.filterLastName(false);

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify Last Name filter successfully.");
            if (superAgentDashboardVerification.verifyNameFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clearAllFilterDetails();

            if (!isPositiveExecution) {
                superAgentDashboardVerification = superAgentDashboardIndexPage.filterAgentId(true);

                superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnFilterSearchButton();

                testVerifyLog("Verify Agent ID filter successfully.");
                if (superAgentDashboardVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.filterAgentId(false);

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify Agent ID filter successfully.");
            if (superAgentDashboardVerification.verifyIdFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clearAllFilterDetails();

            if (!isPositiveExecution) {
                superAgentDashboardVerification = superAgentDashboardIndexPage.filterAgentNumber(true);

                superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnFilterSearchButton();

                testVerifyLog("Verify Mobile Number filter successfully.");
                if (superAgentDashboardVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.filterAgentNumber(false);

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify Mobile Number filter successfully.");
            if (superAgentDashboardVerification.verifyPhoneNumberFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.clearAllFilterDetails();

            superAgentDashboardVerification = superAgentDashboardIndexPage.setAgentStatus();

            superAgentDashboardVerification = superAgentDashboardIndexPage.filterStatus("Active");

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify Status filter successfully.");
            if (superAgentDashboardVerification.verifyStatusFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.filterStatus("Kyc Pending");

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify Status filter successfully.");
            if (superAgentDashboardVerification.verifyStatusFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.filterStatus("Login Pending");

            superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify Status filter successfully.");
            if (superAgentDashboardVerification.verifyStatusFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");

    }

    @Test
    public void superAgent_AgentSearchForTables() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_020 :: To verify Super Agent can search the details in Dashboard tables.");
        superAgentLoginIndexPage.getVersion();

        superAgentLoginVerification = superAgentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (superAgentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify user can see the Dashboard screen with details.");
        if (superAgentDashboardVerification.verifyDashboardDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentDashboardVerification = superAgentDashboardIndexPage.getTodayAgentDetailsFromDashboard();

        if (superAgentDashboardIndexPage.isCommissionDataDisplay()) {

            superAgentDashboardVerification = superAgentDashboardIndexPage.enterSearchCriteria(false);

            testVerifyLog("Verify search details display properly.");
            if (superAgentDashboardVerification.verifySearchSuccessful()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentDashboardVerification = superAgentDashboardIndexPage.enterSearchCriteria(true);

            testVerifyLog("Verify validation message display for the invalid search criteria.");
            if (superAgentDashboardVerification.verifyValidationForInvalidSearch()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

        }

        superAgentDashboardVerification = superAgentDashboardIndexPage.openAgentTargetDetails();

        superAgentDashboardVerification = superAgentDashboardIndexPage.getTotalAgentDetails();


        if (superAgentDashboardIndexPage.isAgentListDisplay()) {

            superAgentDashboardVerification = superAgentDashboardIndexPage.enterAgentSearchCriteria(false);

            testVerifyLog("Verify search details display properly.");
            if (superAgentDashboardVerification.verifyAgentSearchSuccessful()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            if (!isPositiveExecution) {
                superAgentDashboardVerification = superAgentDashboardIndexPage.enterAgentSearchCriteria(true);

                testVerifyLog("Verify validation message display for the invalid search criteria.");
                if (superAgentDashboardVerification.verifyValidationForInvalidSearch()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }
        }

        superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnAgentListButton();

        if (superAgentDashboardIndexPage.isAgentListDisplay()) {

            superAgentDashboardVerification = superAgentDashboardIndexPage.enterAgentListSearchCriteria(false);

            testVerifyLog("Verify search details display properly.");
            if (superAgentDashboardVerification.verifyAgentListSearchSuccessful()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            if (!isPositiveExecution) {
                superAgentDashboardVerification = superAgentDashboardIndexPage.enterAgentListSearchCriteria(true);

                testVerifyLog("Verify validation message display for the invalid search criteria.");
                if (superAgentDashboardVerification.verifyValidationForInvalidSearch()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");

    }

}