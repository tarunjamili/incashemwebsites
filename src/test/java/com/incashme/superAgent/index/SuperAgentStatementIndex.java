package com.incashme.superAgent.index;

import com.framework.init.SeleniumInit;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Rahul R.
 * Date: 2019-03-29
 * Time: 15:49
 * Project Name: InCashMe
 */
public class SuperAgentStatementIndex extends SeleniumInit {

    @Test
    public void superAgent_StatementDownload() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_011 :: To verify Super Agent can download the Monthly Statement.");
        superAgentLoginIndexPage.getVersion();

        superAgentLoginVerification = superAgentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (superAgentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        createDownloadDirectory();

        superAgentDashboardVerification = superAgentDashboardIndexPage.openUserProfile();

        superAgentStatementVerification = superAgentStatementIndexPage.getProfileCreatedMonth();

        superAgentDashboardVerification = superAgentDashboardIndexPage.closeProfilePopUp();

        superAgentStatementVerification = superAgentStatementIndexPage.clickOnStatementMenu();

        testVerifyLog("Verify Monthly statements display since user started the account.");
        if (superAgentStatementVerification.verifyStatementDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (superAgentStatementIndexPage.isStatementDisplay()) {

            superAgentStatementVerification = superAgentStatementIndexPage.clickOnViewButton();

            testVerifyLog("Verify Statement downloaded successfully.");
            if (superAgentStatementVerification.verifyStatementDownloaded()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");

    }

    @Test
    public void superAgent_StatementFilter() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_012 :: To verify Super Agent can filter the Statements.");
        superAgentLoginIndexPage.getVersion();

        superAgentLoginVerification = superAgentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (superAgentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentStatementVerification = superAgentStatementIndexPage.clickOnStatementMenu();

        testVerifyLog("Verify user can see the Statement Screen with list of statements.");
        if (superAgentStatementVerification.verifyStatementsScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (superAgentStatementIndexPage.isStatementDisplay()) {

            if (!isPositiveExecution) {
                superAgentStatementVerification = superAgentStatementIndexPage.selectStatementToFilter(true);

                testVerifyLog("Verify No Statement display for current month.");
                if (superAgentStatementVerification.verifyNoStatementDisplay()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }

                superAgentStatementVerification = superAgentStatementIndexPage.clickOnFilterButton();
            }

            superAgentStatementVerification = superAgentStatementIndexPage.selectStatementToFilter(false);

            testVerifyLog("Verify Statement filtered successfully.");
            if (superAgentStatementVerification.verifyStatementFilterSuccessfully()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");

    }
}
