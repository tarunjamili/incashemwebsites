package com.incashme.superAgent.index;

import com.framework.init.SeleniumInit;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Rahul R.
 * Date: 2019-03-19
 * Time:
 * Project Name: InCashMe
 */

public class SuperAgentLoginIndex extends SeleniumInit {

    @Test
    public void superAgent_NonKYCLogin() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_001 :: To verify Non KYC Super Agent tries to access the application with Login and Forgot Password.");
        superAgentLoginIndexPage.getVersion();

        superAgentLoginVerification = superAgentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can see the KYC Validation message for the non kyc users.");
        if (superAgentLoginVerification.verifyLoginKYCValidationMessage()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentLoginVerification = superAgentLoginIndexPage.clickOnForgotPassword();

        testVerifyLog("Verify user can see the Forgot Password screen.");
        if (superAgentLoginVerification.verifyForgotPasswordScreenDisplay()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentLoginVerification = superAgentLoginIndexPage.forgotPasswordAs(username);
        superAgentLoginVerification = superAgentLoginIndexPage.clickOnSubmitButton();

        testVerifyLog("Verify user can see the KYC Validation message for the non kyc users.");
        if (superAgentLoginVerification.verifyForgotPasswordKYCValidationMessage()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void superAgent_LoginLogout() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_002 :: To verify Super Agent can Login and Logout with the valid email credentials.");
        superAgentLoginIndexPage.getVersion();

        testVerifyLog("Verify Login button is disabled for the blank credentials.");
        if (superAgentLoginVerification.verifyLoginButtonDisabled()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            superAgentLoginVerification = superAgentLoginIndexPage.invalidLoginAs(getInvalidEmail(), getRandomPassword());

            testVerifyLog("Verify validation message for the invalid email address.");
            if (superAgentLoginVerification.verifyInvalidEmailValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentLoginVerification = superAgentLoginIndexPage.loginAs(getUnRegisteredEmail(), getRandomPassword());

            testVerifyLog("Verify validation message for the blank credentials.");
            if (superAgentLoginVerification.verifyUnregisteredEmailValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        superAgentLoginVerification = superAgentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (superAgentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentLoginVerification = superAgentLoginIndexPage.clickOnLogout();

        testVerifyLog("Verify user logout successfully from the Application.");
        if (superAgentLoginVerification.verifyUserLogoutSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentLoginVerification = superAgentLoginIndexPage.clickOnPrivacyPolicy();

        testVerifyLog("Verify user can see the Privacy Policy screen.");
        if (superAgentLoginVerification.verifyPrivacyPolicyScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentLoginVerification = superAgentLoginIndexPage.clickOnTermsCondition();

        testVerifyLog("Verify user can see the Terms & Condition screen.");
        if (superAgentLoginVerification.verifyTnCScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");

    }

    @Test(enabled = false)
    public void superAgent_ForgotPassword() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_003 :: To verify Forgot Password functionality.");
        superAgentLoginIndexPage.getVersion();

        superAgentLoginVerification = superAgentLoginIndexPage.clickOnForgotPassword();

        superAgentLoginVerification = superAgentLoginIndexPage.clickOnBackToLogin();

        superAgentLoginVerification = superAgentLoginIndexPage.clickOnForgotPassword();

        testVerifyLog("Verify user can see the Forgot Password screen.");
        if (superAgentLoginVerification.verifyForgotPasswordScreenDisplay()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentLoginVerification = superAgentLoginIndexPage.forgotPasswordAs(getInvalidEmail());

        testVerifyLog("Verify validation message for the invalid email address.");
        if (superAgentLoginVerification.verifyInvalidEmailValidation()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentLoginVerification = superAgentLoginIndexPage.forgotPasswordAs(username);
        superAgentLoginVerification = superAgentLoginIndexPage.clickOnSubmitButton();

        testVerifyLog("Verify OTP Screen display for entering the OTP.");
        if (superAgentLoginVerification.verifyStep1OTPScreenDisplay()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentLoginVerification = superAgentLoginIndexPage.enterOTP(getRandomNumber() + "");
        superAgentLoginVerification = superAgentLoginIndexPage.clickOnSubmitButton();

        testVerifyLog("Verify invalid entered OTP validation message.");
        if (superAgentLoginVerification.verifyInvalidOTPValidation()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentLoginVerification = superAgentLoginIndexPage.waitAndClickResendButton();

        testVerifyLog("Verify OTP Screen display for entering the OTP.");
        if (superAgentLoginVerification.verifyStep1OTPScreenDisplay()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentLoginVerification = superAgentLoginIndexPage.enterStep1OTP();
        superAgentLoginVerification = superAgentLoginIndexPage.clickOnSubmitButton();

        testVerifyLog("Verify OTP Screen display after entering Step 1 OTP.");
        if (superAgentLoginVerification.verifyStep2OTPScreenDisplay()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentLoginVerification = superAgentLoginIndexPage.openResetPasswordLink(username);

        testVerifyLog("Verify Reset Password screen display.");
        if (superAgentLoginVerification.verifyResetPasswordScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentLoginVerification = superAgentLoginIndexPage.enterOTP(getRandomNumber() + "1");

        testVerifyLog("Verify Invalid OTP digits message.");
        if (superAgentLoginVerification.verifyInvalidOTPDigitMessage()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentLoginVerification = superAgentLoginIndexPage.enterStep2OTP();

        superAgentLoginVerification = superAgentLoginIndexPage.changeInvalidNewPassword();

        testVerifyLog("Verify validation message for the invalid new password.");
        if (superAgentLoginVerification.verifyInvalidNewPasswordValidation()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentLoginVerification = superAgentLoginIndexPage.changeAsPastPassword();

        testVerifyLog("Verify validation message for the past password as new password.");
        if (superAgentLoginVerification.verifyPastPasswordValidation()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

       /* superAgentLoginVerification = superAgentLoginIndexPage.changeNewPassword();

        testVerifyLog("Verify confirmation message for the Password changed successfully.");
        if (superAgentLoginVerification.verifyPasswordChangeSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentLoginVerification = superAgentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (superAgentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }


        superAgentLoginVerification = superAgentLoginIndexPage.openResetPasswordRecoveryURL();

        testVerifyLog("Verify Reset Password link expired once password is set.");
        if (superAgentLoginVerification.verifyLinkExpired()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }*/

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

}