package com.incashme.superAgent.index;

import com.framework.init.SeleniumInit;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Rahul R.
 * Date: 2019-04-02
 * Time: 10:55
 * Project Name: InCashMe
 */


public class SuperAgentAgentListIndex extends SeleniumInit {

    @Test
    public void superAgent_AgentListDetails() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_016 :: To verify Super Agent can see the Agent List and Agent Profile Details on the screen.");
        superAgentLoginIndexPage.getVersion();

        superAgentLoginVerification = superAgentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (superAgentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify user can see the Dashboard screen with details.");
        if (superAgentDashboardVerification.verifyDashboardDetails()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentDashboardVerification = superAgentDashboardIndexPage.getTodayAgentDetailsFromDashboard();

        superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnAgentsListMenu();

        testVerifyLog("Verify user can see the Agents List Screen.");
        if (superAgentAgentListVerification.verifyAgentsListScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        testVerifyLog("Verify Total Added agents are same as display on Dashboard.");
        if (superAgentAgentListVerification.verifyTotalAgentCountFromDashboard()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (superAgentAgentListIndexPage.isAgentListDisplay()) {

            superAgentAgentListVerification = superAgentAgentListIndexPage.getAnyAgentDetails();
            superAgentAgentListVerification = superAgentAgentListIndexPage.clickOnMOREButton();

            testVerifyLog("Verify user can see the Agent Personal Info in Profile.");
            if (superAgentAgentListVerification.verifyAgentPersonalInfo()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentAgentListVerification = superAgentAgentListIndexPage.clickOnAgentTask();

            testVerifyLog("Verify user can see the Agent Task Info in Profile.");
            if (superAgentAgentListVerification.verifyAgentTaskInfo()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentAgentListVerification = superAgentAgentListIndexPage.clickOnHistory();

            testVerifyLog("Verify user can see the Agent History Info in Profile.");
            if (superAgentAgentListVerification.verifyAgentHistoryInfo()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test
    public void superAgent_AgentListSortFilter() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_017 :: To verify Super Agent can sort and Filter the Agent details.");
        superAgentLoginIndexPage.getVersion();

        superAgentLoginVerification = superAgentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (superAgentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnAgentsListMenu();

        testVerifyLog("Verify user can see the Agents List Screen.");
        if (superAgentAgentListVerification.verifyAgentsListScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }


        if (superAgentAgentListIndexPage.isAgentListDisplay()) {

          /*  superAgentAgentListVerification = superAgentAgentListIndexPage.clickOnAgentNameToSort();

            testVerifyLog("Verify Agent Name are sorted successfully.");
            if (superAgentAgentListVerification.verifyAgentNameSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }*/

            superAgentAgentListVerification = superAgentAgentListIndexPage.clickOnPhoneNumberToSort();

            testVerifyLog("Verify Agent Mobile Number are sorted successfully.");
            if (superAgentAgentListVerification.verifyAgentNumberSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentAgentListVerification = superAgentAgentListIndexPage.clickOnStatusToSort();

            testVerifyLog("Verify Agent Status are sorted successfully.");
            if (superAgentAgentListVerification.verifyAgentStatusSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentAgentListVerification = superAgentAgentListIndexPage.clickOnDateTimeToSort();

            testVerifyLog("Verify Date & Time are sorted successfully.");
            if (superAgentAgentListVerification.verifyAgentDateTimeSorted()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentAgentListVerification = superAgentAgentListIndexPage.clickOnFilterButton();

            testVerifyLog("Verify user can see the Filter screen.");
            if (superAgentAgentListVerification.verifyFilterScreenDisplay()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentAgentListVerification = superAgentAgentListIndexPage.getAgentDetailsForFilter();

            if (!isPositiveExecution) {
                superAgentAgentListVerification = superAgentAgentListIndexPage.filterFirstName(true);

                superAgentAgentListVerification = superAgentAgentListIndexPage.clickOnFilterSearchButton();

                testVerifyLog("Verify First Name filter successfully.");
                if (superAgentAgentListVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            superAgentAgentListVerification = superAgentAgentListIndexPage.filterFirstName(false);

            superAgentAgentListVerification = superAgentAgentListIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify First Name filter successfully.");
            if (superAgentAgentListVerification.verifyNameFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentAgentListVerification = superAgentAgentListIndexPage.clearAllFilterDetails();

            if (!isPositiveExecution) {
                superAgentAgentListVerification = superAgentAgentListIndexPage.filterLastName(true);

                superAgentAgentListVerification = superAgentAgentListIndexPage.clickOnFilterSearchButton();

                testVerifyLog("Verify Last Name filter successfully.");
                if (superAgentAgentListVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            superAgentAgentListVerification = superAgentAgentListIndexPage.filterLastName(false);

            superAgentAgentListVerification = superAgentAgentListIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify Last Name filter successfully.");
            if (superAgentAgentListVerification.verifyNameFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentAgentListVerification = superAgentAgentListIndexPage.clearAllFilterDetails();

            if (!isPositiveExecution) {
                superAgentAgentListVerification = superAgentAgentListIndexPage.filterAgentId(true);

                superAgentAgentListVerification = superAgentAgentListIndexPage.clickOnFilterSearchButton();

                testVerifyLog("Verify Agent ID filter successfully.");
                if (superAgentAgentListVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            superAgentAgentListVerification = superAgentAgentListIndexPage.filterAgentId(false);

            superAgentAgentListVerification = superAgentAgentListIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify Agent ID filter successfully.");
            if (superAgentAgentListVerification.verifyIdFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentAgentListVerification = superAgentAgentListIndexPage.clearAllFilterDetails();

            if (!isPositiveExecution) {
                superAgentAgentListVerification = superAgentAgentListIndexPage.filterAgentNumber(true);

                superAgentAgentListVerification = superAgentAgentListIndexPage.clickOnFilterSearchButton();

                testVerifyLog("Verify Mobile Number filter successfully.");
                if (superAgentAgentListVerification.verifyRandomDataFiltered()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

            superAgentAgentListVerification = superAgentAgentListIndexPage.filterAgentNumber(false);

            superAgentAgentListVerification = superAgentAgentListIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify Mobile Number filter successfully.");
            if (superAgentAgentListVerification.verifyPhoneNumberFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentAgentListVerification = superAgentAgentListIndexPage.clearAllFilterDetails();

            superAgentAgentListVerification = superAgentAgentListIndexPage.setAgentStatus();

            superAgentAgentListVerification = superAgentAgentListIndexPage.filterStatus("Active");

            superAgentAgentListVerification = superAgentAgentListIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify Status filter successfully.");
            if (superAgentAgentListVerification.verifyStatusFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentAgentListVerification = superAgentAgentListIndexPage.filterStatus("Kyc Pending");

            superAgentAgentListVerification = superAgentAgentListIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify Status filter successfully.");
            if (superAgentAgentListVerification.verifyStatusFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentAgentListVerification = superAgentAgentListIndexPage.filterStatus("Login Pending");

            superAgentAgentListVerification = superAgentAgentListIndexPage.clickOnFilterSearchButton();

            testVerifyLog("Verify Status filter successfully.");
            if (superAgentAgentListVerification.verifyStatusFiltered()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

        } else {
            testWarningLog("Validation Message : No Agent Lists !");
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");

    }

    @Test
    public void superAgent_AgentListSearchForTables() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_022 :: To verify Super Agent can search the details in Agent List Table.");
        superAgentLoginIndexPage.getVersion();

        superAgentLoginVerification = superAgentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (superAgentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentDashboardVerification = superAgentDashboardIndexPage.clickOnAgentsListMenu();

        testVerifyLog("Verify user can see the Agents List Screen.");
        if (superAgentAgentListVerification.verifyAgentsListScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (superAgentAgentListIndexPage.isAgentListDisplay()) {

            superAgentAgentListVerification = superAgentAgentListIndexPage.enterSearchIDCriteria(false);

            testVerifyLog("Verify search details display properly.");
            if (superAgentAgentListVerification.verifySearchSuccessful()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            if (!isPositiveExecution) {
                superAgentAgentListVerification = superAgentAgentListIndexPage.enterSearchIDCriteria(true);

                testVerifyLog("Verify validation message display for the invalid search criteria.");
                if (superAgentAgentListVerification.verifyValidationForInvalidSearch()) {
                    stepPassed();
                } else {
                    stepFailure(driver);
                    numOfFailedSteps++;
                }
            }

        } else {
            testWarningLog("Validation Message : No Agent Lists !");
        }


        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");

    }

    @Test(enabled = false)
    public void superAgent_CreateANewAgent() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_018 :: To verify Super Agent can create a new agent, create new agent is reflect in the list.");
        superAgentLoginIndexPage.getVersion();

        superAgentLoginVerification = superAgentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (superAgentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentAgentListVerification = superAgentAgentListIndexPage.clickOnCreateNewAgentMenu();

        testVerifyLog("Verify user can see the Agents List Screen.");
        if (superAgentAgentListVerification.verifyCreateAgentScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            superAgentAgentListVerification = superAgentAgentListIndexPage.enterNameMinChar("first name");

            testVerifyLog("Verify validation message for invalid first name.");
            if (superAgentAgentListVerification.verifyMinCharName("first name")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentAgentListVerification = superAgentAgentListIndexPage.enterNameNumber("first name");

            testVerifyLog("Verify validation message for invalid first name.");
            if (superAgentAgentListVerification.verifyNumericName("first name")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentAgentListVerification = superAgentAgentListIndexPage.enterNameMaxChar("first name");

            testVerifyLog("Verify validation message for invalid first name.");
            if (superAgentAgentListVerification.verifyMaxCharName("first name")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        superAgentAgentListVerification = superAgentAgentListIndexPage.enterName("first name");

        testVerifyLog("Verify no validation message display for valid name.");
        if (superAgentAgentListVerification.verifyNoValidationName("first name")) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            superAgentAgentListVerification = superAgentAgentListIndexPage.enterNameMinChar("last name");

            testVerifyLog("Verify validation message for invalid last name.");
            if (superAgentAgentListVerification.verifyMinCharName("last name")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentAgentListVerification = superAgentAgentListIndexPage.enterNameNumber("last name");

            testVerifyLog("Verify validation message for invalid last name.");
            if (superAgentAgentListVerification.verifyNumericName("last name")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentAgentListVerification = superAgentAgentListIndexPage.enterNameMaxChar("last name");

            testVerifyLog("Verify validation message for invalid last name.");
            if (superAgentAgentListVerification.verifyMaxCharName("last name")) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        superAgentAgentListVerification = superAgentAgentListIndexPage.enterName("last name");

        testVerifyLog("Verify no validation message display for valid name.");
        if (superAgentAgentListVerification.verifyNoValidationName("last name")) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentAgentListVerification = superAgentAgentListIndexPage.enterRandomDOB();

        testVerifyLog("Verify DOB selected successfully.");
        if (superAgentAgentListVerification.verifyDOBSelected()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            superAgentAgentListVerification = superAgentAgentListIndexPage.enterEmail(true);

            testVerifyLog("Verify validation message for invalid email address.");
            if (superAgentAgentListVerification.verifyInvalidEmailValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        superAgentAgentListVerification = superAgentAgentListIndexPage.enterEmail(false);

        testVerifyLog("Verify no validation message display for valid email.");
        if (superAgentAgentListVerification.verifyNoValidationEmail()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            superAgentAgentListVerification = superAgentAgentListIndexPage.enterMinMobileNumber();

            testVerifyLog("Verify validation message display for invalid mobile number.");
            if (superAgentAgentListVerification.verifyInvalidMobileNumberValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentAgentListVerification = superAgentAgentListIndexPage.enterMaxMobileNumber();

            testVerifyLog("Verify validation message display for invalid mobile number.");
            if (superAgentAgentListVerification.verifyInvalidMobileNumberValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentAgentListVerification = superAgentAgentListIndexPage.enterInvalidMobileNumber();

            testVerifyLog("Verify validation message display for invalid mobile number.");
            if (superAgentAgentListVerification.verifyInvalidMobileNumberValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        superAgentAgentListVerification = superAgentAgentListIndexPage.enterMobileNumber();

        testVerifyLog("Verify no validation message display for valid Mobile Number.");
        if (superAgentAgentListVerification.verifyNoValidationMobile()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            superAgentAgentListVerification = superAgentAgentListIndexPage.uploadProfilePicture(true);

            testVerifyLog("Verify validation message for invalid file format upload.");
            if (superAgentAgentListVerification.verifyInvalidFileUpload()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        superAgentAgentListVerification = superAgentAgentListIndexPage.uploadProfilePicture(false);

        testVerifyLog("Verify profile pic selected successfully.");
        if (superAgentAgentListVerification.verifyProfilePicSelected()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            superAgentAgentListVerification = superAgentAgentListIndexPage.uploadAdhaarFront(true);

            testVerifyLog("Verify validation message for invalid file format upload.");
            if (superAgentAgentListVerification.verifyInvalidFileUpload()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        superAgentAgentListVerification = superAgentAgentListIndexPage.uploadAdhaarFront(false);

        testVerifyLog("Verify Adhaar Front selected successfully.");
        if (superAgentAgentListVerification.verifyAdhaarFrontSelected()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            superAgentAgentListVerification = superAgentAgentListIndexPage.uploadAdhaarBack(true);

            testVerifyLog("Verify validation message for invalid file format upload.");
            if (superAgentAgentListVerification.verifyInvalidFileUpload()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        superAgentAgentListVerification = superAgentAgentListIndexPage.uploadAdhaarBack(false);

        testVerifyLog("Verify Adhaar Back selected successfully.");
        if (superAgentAgentListVerification.verifyAdhaarBackSelected()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentAgentListVerification = superAgentAgentListIndexPage.clickStep1OnNextButton();

        testVerifyLog("Verify Step 2 OTP screen display.");
        if (superAgentAgentListVerification.verifyStep2OTPScreenDisplay()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            superAgentAgentListVerification = superAgentAgentListIndexPage.enterOTP(getRandomNumber() + "");
            superAgentAgentListVerification = superAgentAgentListIndexPage.clickOnStep2NextButton();

            testVerifyLog("Verify validation message for invalid OTP.");
            if (superAgentAgentListVerification.verifyOTPMisMatchValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentAgentListVerification = superAgentAgentListIndexPage.enterOTP(getRandomCharacters(5));
            superAgentAgentListVerification = superAgentAgentListIndexPage.clickOnStep2NextButton();

            testVerifyLog("Verify validation message for invalid OTP.");
            if (superAgentAgentListVerification.verifyInvalidOTPValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentAgentListVerification = superAgentAgentListIndexPage.enterOTP(getRandomNumber() + "1");
            superAgentAgentListVerification = superAgentAgentListIndexPage.clickOnStep2NextButton();

            testVerifyLog("Verify validation message for invalid OTP.");
            if (superAgentAgentListVerification.verifyInvalidOTPValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentAgentListVerification = superAgentAgentListIndexPage.enterOTP("");
            superAgentAgentListVerification = superAgentAgentListIndexPage.clickOnStep2NextButton();

            testVerifyLog("Verify validation message for invalid OTP.");
            if (superAgentAgentListVerification.verifyOTPMisMatchValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        superAgentAgentListVerification = superAgentAgentListIndexPage.enterStep2OTP();
        superAgentAgentListVerification = superAgentAgentListIndexPage.clickOnStep2NextButton();

        testVerifyLog("Verify Step 3 screen display.");
        if (superAgentAgentListVerification.verifyStep3ScreenDisplay()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            superAgentAgentListVerification = superAgentAgentListIndexPage.enterHomeAddress(true);

            testVerifyLog("Verify validation message for invalid address details.");
            if (superAgentAgentListVerification.verifyInvalidAddress()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        superAgentAgentListVerification = superAgentAgentListIndexPage.enterHomeAddress(false);

        testVerifyLog("Verify no validation message display for valid address.");
        if (superAgentAgentListVerification.verifyNoValidationAddress()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            superAgentAgentListVerification = superAgentAgentListIndexPage.enterStreet1Address(true);

            testVerifyLog("Verify validation message for invalid address details.");
            if (superAgentAgentListVerification.verifyInvalidAddress()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        superAgentAgentListVerification = superAgentAgentListIndexPage.enterStreet1Address(false);

        testVerifyLog("Verify no validation message display for valid address.");
        if (superAgentAgentListVerification.verifyNoValidationAddress()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            superAgentAgentListVerification = superAgentAgentListIndexPage.enterStreet2Address(true);

            testVerifyLog("Verify validation message for invalid address details.");
            if (superAgentAgentListVerification.verifyInvalidAddress()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        superAgentAgentListVerification = superAgentAgentListIndexPage.enterStreet2Address(false);

        testVerifyLog("Verify no validation message display for valid address.");
        if (superAgentAgentListVerification.verifyNoValidationAddress()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            superAgentAgentListVerification = superAgentAgentListIndexPage.enterVillageAddress(true);

            testVerifyLog("Verify validation message for invalid address details.");
            if (superAgentAgentListVerification.verifyInvalidVillageAddress()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        superAgentAgentListVerification = superAgentAgentListIndexPage.enterVillageAddress(false);

        testVerifyLog("Verify no validation message display for valid address.");
        if (superAgentAgentListVerification.verifyNoValidationAddress()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            superAgentAgentListVerification = superAgentAgentListIndexPage.enterTehsilAddress(true);

            testVerifyLog("Verify validation message for invalid address details.");
            if (superAgentAgentListVerification.verifyInvalidTehsilAddress()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        superAgentAgentListVerification = superAgentAgentListIndexPage.enterTehsilAddress(false);

        testVerifyLog("Verify no validation message display for valid address.");
        if (superAgentAgentListVerification.verifyNoValidationAddress()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            superAgentAgentListVerification = superAgentAgentListIndexPage.enterDistrictAddress(true);

            testVerifyLog("Verify validation message for invalid address details.");
            if (superAgentAgentListVerification.verifyInvalidDistrictAddress()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        superAgentAgentListVerification = superAgentAgentListIndexPage.enterDistrictAddress(false);

        testVerifyLog("Verify no validation message display for valid address.");
        if (superAgentAgentListVerification.verifyNoValidationAddress()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentAgentListVerification = superAgentAgentListIndexPage.selectState();

        superAgentAgentListVerification = superAgentAgentListIndexPage.selectCity();

        if (!isPositiveExecution) {
            superAgentAgentListVerification = superAgentAgentListIndexPage.enterNumberCountry();

            testVerifyLog("Verify validation message for numeric values in country.");
            if (superAgentAgentListVerification.verifyNumericCountryValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentAgentListVerification = superAgentAgentListIndexPage.enterMinCharCountry();

            testVerifyLog("Verify validation message for minimum characters in country.");
            if (superAgentAgentListVerification.verifyCountryMinCharValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentAgentListVerification = superAgentAgentListIndexPage.enterMaxCharCountry();

            testVerifyLog("Verify validation message for maximum characters in country.");
            if (superAgentAgentListVerification.verifyCountryMaxCharValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        superAgentAgentListVerification = superAgentAgentListIndexPage.enterCountry();

        testVerifyLog("Verify no validation message display for valid country.");
        if (superAgentAgentListVerification.verifyNoValidationCountry()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            superAgentAgentListVerification = superAgentAgentListIndexPage.enterMinPinCode();

            testVerifyLog("Verify validation message display for invalid testData code.");
            if (superAgentAgentListVerification.verifyInvalidPinCodeValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentAgentListVerification = superAgentAgentListIndexPage.enterMaxPinCode();

            testVerifyLog("Verify validation message display for invalid testData code.");
            if (superAgentAgentListVerification.verifyInvalidPinCodeValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentAgentListVerification = superAgentAgentListIndexPage.enterInvalidPinCode();

            testVerifyLog("Verify validation message display for invalid testData code.");
            if (superAgentAgentListVerification.verifyInvalidPinCodeValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        superAgentAgentListVerification = superAgentAgentListIndexPage.enterPinCode();

        testVerifyLog("Verify no validation message display for valid Pin Code.");
        if (superAgentAgentListVerification.verifyNoValidationPinCode()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentAgentListVerification = superAgentAgentListIndexPage.clickOnStep3NextButton();

        testVerifyLog("Verify Step 4 screen display.");
        if (superAgentAgentListVerification.verifyStep4ScreenDisplay()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            superAgentAgentListVerification = superAgentAgentListIndexPage.clickOnCancelButton();
            pause(1);
            superAgentAgentListVerification = superAgentAgentListIndexPage.clickOnCancelCancelButton();


            superAgentAgentListVerification = superAgentAgentListIndexPage.enterMinNewConMer("C");

            testVerifyLog("Verify validation message display for invalid target assigned.");
            if (superAgentAgentListVerification.verifyMinNewConsumerTarget()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentAgentListVerification = superAgentAgentListIndexPage.enterMaxNewConMer("C");

            testVerifyLog("Verify validation message display for invalid target assigned.");
            if (superAgentAgentListVerification.verifyInvalidNewConsumerTarget()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentAgentListVerification = superAgentAgentListIndexPage.enterInvalidNewConMer("C");

            testVerifyLog("Verify validation message display for invalid target assigned.");
            if (superAgentAgentListVerification.verifyInvalidNewConsumerTarget()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        superAgentAgentListVerification = superAgentAgentListIndexPage.enterNewTargetForConMer("C");

        testVerifyLog("Verify no validation message display for valid Target.");
        if (superAgentAgentListVerification.verifyNoValidationNewTarget()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            superAgentAgentListVerification = superAgentAgentListIndexPage.enterMinNewConMer("M");

            testVerifyLog("Verify validation message display for invalid target assigned.");
            if (superAgentAgentListVerification.verifyMinNewMerchantTarget()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentAgentListVerification = superAgentAgentListIndexPage.enterMaxNewConMer("M");

            testVerifyLog("Verify validation message display for invalid target assigned.");
            if (superAgentAgentListVerification.verifyInvalidNewMerchantTarget()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentAgentListVerification = superAgentAgentListIndexPage.enterInvalidNewConMer("M");

            testVerifyLog("Verify validation message display for invalid target assigned.");
            if (superAgentAgentListVerification.verifyInvalidNewMerchantTarget()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        superAgentAgentListVerification = superAgentAgentListIndexPage.enterNewTargetForConMer("M");

        testVerifyLog("Verify no validation message display for valid Target.");
        if (superAgentAgentListVerification.verifyNoValidationNewTarget()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (!isPositiveExecution) {
            superAgentAgentListVerification = superAgentAgentListIndexPage.enterMinTopUpAmount("Min");

            testVerifyLog("Verify validation message display for invalid minimum topup entered.");
            if (superAgentAgentListVerification.verifyMinMinimumTopUpValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentAgentListVerification = superAgentAgentListIndexPage.enterMaxTopUpAmount("Min");

            testVerifyLog("Verify validation message display for invalid minimum topup entered.");
            if (superAgentAgentListVerification.verifyMaxMinimumTopUpValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentAgentListVerification = superAgentAgentListIndexPage.enterInvalidTopUpAmount("Min");

            testVerifyLog("Verify validation message display for invalid minimum topup entered.");
            if (superAgentAgentListVerification.verifyInvalidMinimumTopUpValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentAgentListVerification = superAgentAgentListIndexPage.enterMinTopUpAmount("Max");

            testVerifyLog("Verify validation message display for invalid maximum topup entered.");
            if (superAgentAgentListVerification.verifyMinMaximumTopUpValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentAgentListVerification = superAgentAgentListIndexPage.enterMaxTopUpAmount("Max");

            testVerifyLog("Verify validation message display for invalid maximum topup entered.");
            if (superAgentAgentListVerification.verifyMaxMaximumTopUpValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentAgentListVerification = superAgentAgentListIndexPage.enterInvalidTopUpAmount("Max");

            testVerifyLog("Verify validation message display for invalid maximum topup entered.");
            if (superAgentAgentListVerification.verifyInvalidMaximumTopUpValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }

            superAgentAgentListVerification = superAgentAgentListIndexPage.enterMinMaxSwapAmount("Min");
            pause(1);
            superAgentAgentListVerification = superAgentAgentListIndexPage.enterMinMaxSwapAmount("Max");
            pause(1);
            superAgentAgentListVerification = superAgentAgentListIndexPage.clickOnCreateAgentButton();

            testVerifyLog("Verify validation message display for maximum topup amount less than minimum topup amount.");
            if (superAgentAgentListVerification.verifyMaxLessThanMinAmountValidation()) {
                stepPassed();
            } else {
                stepFailure(driver);
                numOfFailedSteps++;
            }
        }

        superAgentAgentListVerification = superAgentAgentListIndexPage.enterNewTopUpAmount("Min");
        pause(1);
        superAgentAgentListVerification = superAgentAgentListIndexPage.enterNewTopUpAmount("Max");
        pause(1);

        superAgentAgentListVerification = superAgentAgentListIndexPage.clickOnCreateAgentButton();

        testVerifyLog("Verify user see the confirmation message for Agent created successfully.");
        if (superAgentAgentListVerification.verifyAgentCreatedSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentAgentListVerification = superAgentAgentListIndexPage.displayUserDetails();

        testVerifyLog("Verify user can see the Agents List Screen.");
        if (superAgentAgentListVerification.verifyAgentsListScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentAgentListVerification = superAgentAgentListIndexPage.getNewAgentDetails();

        testVerifyLog("Verify user can see the Agent Info in Agent List.");
        if (superAgentAgentListVerification.verifyCurrentAgentInForDisplayProperly()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentAgentListVerification = superAgentAgentListIndexPage.openMOREInfo();

        testVerifyLog("Verify user can see the Agent Personal Info in Profile.");
        if (superAgentAgentListVerification.verifyAgentPersonalInfoForCreatedAgent()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentLoginVerification = superAgentLoginIndexPage.clickOnLogout();

        testVerifyLog("Verify user logout successfully from the Application.");
        if (superAgentLoginVerification.verifyUserLogoutSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentLoginVerification = superAgentLoginIndexPage.clickOnForgotPassword();

        superAgentLoginVerification = superAgentLoginIndexPage.forgotPasswordNonKYC();
        superAgentLoginVerification = superAgentLoginIndexPage.clickOnSubmitButton();

        testVerifyLog("Verify user can see the KYC Validation message for the recent created agent.");
        if (superAgentLoginVerification.verifyForgotPasswordKYCValidationMessage()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }

    @Test(enabled = false)
    public void superAgent_CancelCreateANewAgent() {

        int numOfFailedSteps = 0;
        _logStep = 1;

        testCaseLog("ICM_SC_019 :: To verify Super Agent can cancel Create a New Agent scenario.");
        superAgentLoginIndexPage.getVersion();

        superAgentLoginVerification = superAgentLoginIndexPage.loginAs(username, password);

        testVerifyLog("Verify user can login successfully and redirect to the Dashboard screen.");
        if (superAgentLoginVerification.verifyUserLoginSuccessfully()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentDashboardVerification = superAgentDashboardIndexPage.getTodayAgentDetailsFromDashboard();

        superAgentAgentListVerification = superAgentAgentListIndexPage.clickOnCreateNewAgentMenu();

        testVerifyLog("Verify user can see the Agents List Screen.");
        if (superAgentAgentListVerification.verifyCreateAgentScreen()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentAgentListVerification = superAgentAgentListIndexPage.enterName("first name");

        superAgentAgentListVerification = superAgentAgentListIndexPage.enterName("last name");

        superAgentAgentListVerification = superAgentAgentListIndexPage.enterRandomDOB();

        testVerifyLog("Verify DOB selected successfully.");
        if (superAgentAgentListVerification.verifyDOBSelected()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentAgentListVerification = superAgentAgentListIndexPage.enterEmail(false);

        superAgentAgentListVerification = superAgentAgentListIndexPage.enterMobileNumber();

        superAgentAgentListVerification = superAgentAgentListIndexPage.uploadProfilePicture(false);

        testVerifyLog("Verify profile pic selected successfully.");
        if (superAgentAgentListVerification.verifyProfilePicSelected()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentAgentListVerification = superAgentAgentListIndexPage.uploadAdhaarFront(false);

        testVerifyLog("Verify Adhaar Front selected successfully.");
        if (superAgentAgentListVerification.verifyAdhaarFrontSelected()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentAgentListVerification = superAgentAgentListIndexPage.uploadAdhaarBack(false);

        testVerifyLog("Verify Adhaar Back selected successfully.");
        if (superAgentAgentListVerification.verifyAdhaarBackSelected()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentAgentListVerification = superAgentAgentListIndexPage.clickStep1OnNextButton();

        testVerifyLog("Verify Step 2 OTP screen display.");
        if (superAgentAgentListVerification.verifyStep2OTPScreenDisplay()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentAgentListVerification = superAgentAgentListIndexPage.enterStep2OTP();
        superAgentAgentListVerification = superAgentAgentListIndexPage.clickOnStep2NextButton();

        testVerifyLog("Verify Step 3 screen display.");
        if (superAgentAgentListVerification.verifyStep3ScreenDisplay()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentAgentListVerification = superAgentAgentListIndexPage.enterHomeAddress(false);

        superAgentAgentListVerification = superAgentAgentListIndexPage.enterStreet1Address(false);

        superAgentAgentListVerification = superAgentAgentListIndexPage.enterStreet2Address(false);

        superAgentAgentListVerification = superAgentAgentListIndexPage.enterVillageAddress(false);

        superAgentAgentListVerification = superAgentAgentListIndexPage.enterTehsilAddress(false);

        superAgentAgentListVerification = superAgentAgentListIndexPage.enterDistrictAddress(false);

        superAgentAgentListVerification = superAgentAgentListIndexPage.selectState();

        pause(3);

        superAgentAgentListVerification = superAgentAgentListIndexPage.selectCity();

        superAgentAgentListVerification = superAgentAgentListIndexPage.enterCountry();

        superAgentAgentListVerification = superAgentAgentListIndexPage.enterPinCode();

        superAgentAgentListVerification = superAgentAgentListIndexPage.clickOnStep3NextButton();

        testVerifyLog("Verify Step 4 screen display.");
        if (superAgentAgentListVerification.verifyStep4ScreenDisplay()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        superAgentAgentListVerification = superAgentAgentListIndexPage.enterNewTargetForConMer("C");

        superAgentAgentListVerification = superAgentAgentListIndexPage.enterNewTargetForConMer("M");

        superAgentAgentListVerification = superAgentAgentListIndexPage.enterNewTopUpAmount("Min");
        pause(1);
        superAgentAgentListVerification = superAgentAgentListIndexPage.enterNewTopUpAmount("Max");
        pause(1);

        superAgentAgentListVerification = superAgentAgentListIndexPage.clickOnCancelButton();
        pause(1);
        superAgentAgentListVerification = superAgentAgentListIndexPage.clickOnOKCancelButton();

        testVerifyLog("Verify Create a New Agent cancelled successfully.");
        if (superAgentAgentListVerification.verifyCancelCreateNewAgent()) {
            stepPassed();
        } else {
            stepFailure(driver);
            numOfFailedSteps++;
        }

        if (numOfFailedSteps > 0) Assert.fail("Test Verification failed, please check test logs.");
    }


}
