package com.incashme.superAgent.indexpage;

import com.framework.init.AbstractPage;
import com.incashme.superAgent.verification.SuperAgentCommissionVerification;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Rahul R.
 * Date: 2019-04-01
 * Time: 11:26
 * Project Name: InCashMe
 */
public class SuperAgentCommissionIndexPage extends AbstractPage {

    public static double _totalCommissionAmount;

    public static List<Double> _commissionTopUpBeforeSort = new ArrayList<>();
    public static List<Double> _commissionAmountBeforeSort = new ArrayList<>();
    public static List<LocalDateTime> _commissionDateTimeBeforeSort = new ArrayList<>();

    public static List<Double> _commissionTopUpForFilter = new ArrayList<>();
    public static List<Double> _commissionAmountForFilter = new ArrayList<>();

    public static String _filterAgentFirstName;
    public static String _filterAgentLastName;
    public static String _filterAgentId;
    public static String _filterRandomValue;

    public static double _filterTopUpFromAmount;
    public static double _filterTopUpToAmount;
    public static double _filterCommissionFromAmount;
    public static double _filterCommissionToAmount;

    public static boolean _isRandomFilter;

    public static String _searchCriteria;


    @FindBy(xpath = "//label[@id='p_total']")
    private WebElement btnChartTotal;

    @FindBy(xpath = "//span[text()='Total']//following-sibling::span")
    private WebElement lblTotalCommission;

    @FindAll(value = {@FindBy(xpath = "//table[@id='agentListTbl']//tr//td[2]")})
    private List<WebElement> lstCommissionTopUp;

    @FindAll(value = {@FindBy(xpath = "//table[@id='agentListTbl']//tr//td[3]")})
    private List<WebElement> lstCommissionAmount;

    @FindAll(value = {@FindBy(xpath = "//table[@id='agentListTbl']//tr//td[1]//span")})
    private List<WebElement> lstAgentName;

    @FindAll(value = {@FindBy(xpath = " //table[@id='agentListTbl']//tr//td[1]//following-sibling::div")})
    private List<WebElement> lstAgentId;

    @FindBy(xpath = "//table[@id='agentListTbl']//th[2]")
    private WebElement lblTopupHeader;

    @FindBy(xpath = "//table[@id='agentListTbl']//th[3]")
    private WebElement lblCommissionHeader;

    @FindBy(xpath = "//table[@id='agentListTbl']//th[4]")
    private WebElement lblCommissionDateTimeHeader;

    @FindAll(value = {@FindBy(xpath = "//table[@id='agentListTbl']//tr//td[4]")})
    private List<WebElement> lstCommissionTxDateTime;

    @FindBy(xpath = "//li/button[contains(@class,'filterBtn')]")
    private WebElement btnFilter;

    @FindBy(xpath = "//input[@formcontrolname='from_fname']")
    private WebElement txtFilterFirstName;

    @FindBy(xpath = "//input[@formcontrolname='from_lname']")
    private WebElement txtFilterLastName;

    @FindBy(xpath = "//input[@formcontrolname='from_user_id']")
    private WebElement txtFilterAgentID;

    @FindBy(xpath = "//input[@formcontrolname='min_amount']")
    private WebElement txtFilterTopUpMinAmount;

    @FindBy(xpath = "//input[@formcontrolname='max_amount']")
    private WebElement txtFilterTopUpMaxAmount;

    @FindBy(xpath = "//input[@formcontrolname='comm_min_amount']")
    private WebElement txtFilterCommissionMinAmount;

    @FindBy(xpath = "//input[@formcontrolname='comm_max_amount']")
    private WebElement txtFilterCommissionMaxAmount;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//button[contains(@class,'searchbtn')]")
    private WebElement btnFilterSearch;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//a[contains(text(),'Clear All')]")
    private WebElement btnFilterClearAll;

    @FindBy(xpath = "//input[@type='search']")
    private WebElement txtInputSearch;

    public SuperAgentCommissionIndexPage(WebDriver driver) {
        super(driver);
    }

    public SuperAgentCommissionVerification clickOnTotalButton() {

        testStepsLog(_logStep++, "Click on Total button.");
        scrollToElement(driver, btnChartTotal);
        clickOn(driver, btnChartTotal);

        return new SuperAgentCommissionVerification(driver);
    }

    public SuperAgentCommissionVerification getTotalCommissionAmount() {

        pause(2);

        scrollToElement(driver, lblTotalCommission);
        _totalCommissionAmount = getDoubleFromString(getText(lblTotalCommission));

        try {
            updateShowList(driver, findElementByName(driver, "agentListTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        return new SuperAgentCommissionVerification(driver);
    }

    public boolean isCommissionDataDisplay() {
        return _totalCommissionAmount != 0.00;
    }

    public SuperAgentCommissionVerification clickTopUpToSort() {

        for (WebElement name : lstCommissionTopUp) {
            _commissionTopUpBeforeSort.add(getDoubleFromString(getInnerText(name)));
        }

        testStepsLog(_logStep++, "Click On Top up to sort.");
        clickOn(driver, lblTopupHeader);

        return new SuperAgentCommissionVerification(driver);
    }

    public SuperAgentCommissionVerification clickCommissionToSort() {

        for (WebElement name : lstCommissionAmount) {
            _commissionAmountBeforeSort.add(getDoubleFromString(getInnerText(name)));
        }

        testStepsLog(_logStep++, "Click On Commission to sort.");
        clickOn(driver, lblCommissionHeader);

        return new SuperAgentCommissionVerification(driver);
    }

    public SuperAgentCommissionVerification clickOnDateTimeToSort() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        for (WebElement status : lstCommissionTxDateTime) {
            _commissionDateTimeBeforeSort.add(LocalDateTime.parse(getInnerText(status), format));
        }

        testStepsLog(_logStep++, "Click On Date & Time to sort.");
        clickOn(driver, lblCommissionDateTimeHeader);

        return new SuperAgentCommissionVerification(driver);
    }

    public SuperAgentCommissionVerification clickOnFilterButton() {

        testStepsLog(_logStep++, "Click Filter button.");
        clickOn(driver, btnFilter);

        return new SuperAgentCommissionVerification(driver);
    }

    public SuperAgentCommissionVerification getCommissionDetailsForFilter() {

        pause(2);

        _filterAgentFirstName = getInnerText(lstAgentName.get(0)).split(" ")[0].trim();
        _filterAgentLastName = getInnerText(lstAgentName.get(0)).replace(_filterAgentFirstName, "").trim();
        _filterAgentId = getInnerText(lstAgentId.get(0));

        for (int num = 0; num < sizeOf(lstCommissionTopUp); num++) {
            _commissionTopUpForFilter.add(getDoubleFromString(getInnerText(lstCommissionTopUp.get(num))));
            _commissionAmountForFilter.add(getDoubleFromString(getInnerText(lstCommissionAmount.get(num))));
        }

        Collections.sort(_commissionAmountForFilter);
        Collections.sort(_commissionTopUpForFilter);

        return new SuperAgentCommissionVerification(driver);
    }

    public SuperAgentCommissionVerification filterFirstName(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomCharacters(7);
            testStepsLog(_logStep++, "Enter First Name : " + _filterRandomValue);
            type(txtFilterFirstName, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter First Name : " + _filterAgentFirstName);
            type(txtFilterFirstName, _filterAgentFirstName);
        }

        return new SuperAgentCommissionVerification(driver);
    }

    public SuperAgentCommissionVerification filterLastName(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomCharacters(7);
            testStepsLog(_logStep++, "Enter Last Name : " + _filterRandomValue);
            type(txtFilterLastName, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter Last Name : " + _filterAgentLastName);
            type(txtFilterLastName, _filterAgentLastName);
        }

        return new SuperAgentCommissionVerification(driver);
    }

    public SuperAgentCommissionVerification filterAgentId(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomNumber() + "" + getRandomNumber();
            testStepsLog(_logStep++, "Enter Random ID : " + _filterRandomValue);
            type(txtFilterAgentID, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter Agent ID : " + _filterAgentId);
            type(txtFilterAgentID, _filterAgentId);
        }

        return new SuperAgentCommissionVerification(driver);
    }

    public SuperAgentCommissionVerification clickOnFilterSearchButton() {

        testStepsLog(_logStep++, "Click on Search button.");
        clickOn(driver, btnFilterSearch);

        pause(2);

        try {
            updateShowList(driver, findElementByName(driver, "agentListTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        pause(1);

        return new SuperAgentCommissionVerification(driver);

    }

    public SuperAgentCommissionVerification clearAllFilterDetails() {

        testStepsLog(_logStep++, "Click on Clear All button.");
        clickOn(driver, btnFilterClearAll);

        pause(2);

        try {
            updateShowList(driver, findElementByName(driver, "agentListTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        pause(1);

        return new SuperAgentCommissionVerification(driver);

    }

    public SuperAgentCommissionVerification enterTopUpAmountRange(boolean isInvalid) {

        _isRandomFilter = isInvalid;
        scrollToElement(driver, txtFilterTopUpMaxAmount);

        if (isInvalid) {
            double randomNumber = getRandomNumber();
            _filterTopUpFromAmount = randomNumber + 10;
            _filterTopUpToAmount = randomNumber;

            testStepsLog(_logStep++, "Enter From Amount Range : " + _filterTopUpFromAmount);

            type(txtFilterTopUpMinAmount, String.valueOf(_filterTopUpFromAmount));

            testStepsLog(_logStep++, "Enter To Amount Range : " + _filterTopUpToAmount);
            type(txtFilterTopUpMaxAmount, String.valueOf(_filterTopUpToAmount));
        } else {
            _filterTopUpFromAmount = findMin(_commissionTopUpForFilter) + 100;
            _filterTopUpToAmount = findMax(_commissionTopUpForFilter) - 100;

            testStepsLog(_logStep++, "Enter From Amount Range : " + _filterTopUpFromAmount);
            type(txtFilterTopUpMinAmount, String.valueOf(_filterTopUpFromAmount));

            testStepsLog(_logStep++, "Enter To Amount Range : " + _filterTopUpToAmount);
            type(txtFilterTopUpMaxAmount, String.valueOf(_filterTopUpToAmount));
        }

        return new SuperAgentCommissionVerification(driver);

    }

    public SuperAgentCommissionVerification enterTopUpAmount(String field) {

        scrollToElement(driver, txtFilterTopUpMaxAmount);

        _filterTopUpFromAmount = findMin(_commissionTopUpForFilter) + 100;
        _filterTopUpToAmount = findMax(_commissionTopUpForFilter) - 100;

        switch (field.toLowerCase()) {
            case "from":
                testStepsLog(_logStep++, "Enter From Amount Range : " + _filterTopUpFromAmount);
                type(txtFilterTopUpMinAmount, String.valueOf(_filterTopUpFromAmount));
                break;
            case "to":
                testStepsLog(_logStep++, "Enter To Amount Range : " + _filterTopUpToAmount);
                type(txtFilterTopUpMaxAmount, String.valueOf(_filterTopUpToAmount));
                break;
        }

        return new SuperAgentCommissionVerification(driver);

    }

    public SuperAgentCommissionVerification enterCommissionAmountRange(boolean isInvalid) {

        _isRandomFilter = isInvalid;
        scrollToElement(driver, txtFilterCommissionMaxAmount);

        if (isInvalid) {
            double randomNumber = getRandomNumber() / 1000;
            _filterCommissionFromAmount = randomNumber + 10;
            _filterCommissionToAmount = randomNumber;

            testStepsLog(_logStep++, "Enter From Amount Range : " + _filterCommissionFromAmount);

            type(txtFilterCommissionMinAmount, String.valueOf(_filterCommissionFromAmount));

            testStepsLog(_logStep++, "Enter To Amount Range : " + _filterCommissionToAmount);
            type(txtFilterCommissionMaxAmount, String.valueOf(_filterCommissionToAmount));
        } else {
            _filterTopUpFromAmount = findMin(_commissionAmountForFilter) + 0.05;
            _filterTopUpToAmount = findMax(_commissionAmountForFilter) - 0.05;

            testStepsLog(_logStep++, "Enter From Amount Range : " + _filterCommissionFromAmount);
            type(txtFilterCommissionMinAmount, String.valueOf(_filterCommissionFromAmount));

            testStepsLog(_logStep++, "Enter To Amount Range : " + _filterTopUpToAmount);
            type(txtFilterCommissionMaxAmount, String.valueOf(_filterTopUpToAmount));
        }

        return new SuperAgentCommissionVerification(driver);

    }

    public SuperAgentCommissionVerification enterCommissionAmount(String field) {

        scrollToElement(driver, txtFilterCommissionMaxAmount);

        _filterCommissionFromAmount = findMin(_commissionAmountForFilter) + 0.05;
        _filterCommissionToAmount = findMax(_commissionAmountForFilter) - 0.05;

        switch (field.toLowerCase()) {
            case "from":
                testStepsLog(_logStep++, "Enter From Amount Range : " + _filterCommissionFromAmount);
                type(txtFilterCommissionMinAmount, String.valueOf(_filterCommissionFromAmount));
                break;
            case "to":
                testStepsLog(_logStep++, "Enter To Amount Range : " + _filterCommissionToAmount);
                type(txtFilterCommissionMaxAmount, String.valueOf(_filterCommissionToAmount));
                break;
        }

        return new SuperAgentCommissionVerification(driver);

    }

    public SuperAgentCommissionVerification enterSearchIDCriteria(boolean isInvalid) {

        if (isInvalid) {

            scrollToElement(driver, txtInputSearch);

            String searchCriteria = getRandomCharacters(10);
            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", searchCriteria);
            type(txtInputSearch, searchCriteria);

        } else {

            scrollToElement(driver, txtInputSearch);

            _searchCriteria = getInnerText(lstAgentId.get(getRandomNumberBetween(0, lastIndexOf(lstAgentId)))).split("-")[3];

            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", _searchCriteria);
            type(txtInputSearch, _searchCriteria);
        }

        return new SuperAgentCommissionVerification(driver);
    }


}
