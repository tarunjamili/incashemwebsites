package com.incashme.superAgent.indexpage;

import com.framework.init.AbstractPage;
import com.incashme.superAgent.verification.SuperAgentAgentListVerification;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Rahul R.
 * Date: 2019-04-02
 * Time: 10:55
 * Project Name: InCashMe
 */
public class SuperAgentAgentListIndexPage extends AbstractPage {

    public SuperAgentAgentListIndexPage(WebDriver driver) {
        super(driver);
    }

    public static String _agentName = "";
    public static String _agentId = "";
    public static String _agentMobileNumber = "";
    public static String _agentAccountStatus = "";
    public static String _agentConsumerTargetStatus = "";
    public static String _agentMerchantTargetStatus = "";
    public static String _agentConsumerTopUpStatus = "";
    public static String _agentCreatedDate;
    public static String _agentCreatedDateTime;

    public static String _filterAgentFirstName = "";
    public static String _filterAgentLastName = "";
    public static String _filterAgentPhone = "";
    public static String _filterAgentId = "";
    public static String _filterRandomValue = "";
    public static String _filterStatus = "";
    public static List<String> _filterAgentDate = new ArrayList<>();

    public static String _newAgentFirstName = "";
    public static String _newAgentLastName = "";
    public static String _newAgentEmail = "";
    public static String _newAgentMobileNumber = "";
    public static String _newAgentDOB = "";

    public static String _newAgentFlatNumber = "";
    public static String _newAgentStreet1 = "";
    public static String _newAgentStreet2 = "";
    public static String _newAgentVillage = "";
    public static String _newAgentTeshsil = "";
    public static String _newAgentDistrict = "";
    public static String _newAgentState = "";
    public static String _newAgentCity = "";
    public static String _newAgentCountry = "";
    public static String _newAgentPinCode = "";

    public static String _newAgentNewConsumerTarget = "";
    public static String _newAgentNewMerchantTarget = "";
    public static String _newAgentMinTopUpAmount = "";
    public static String _newAgentMaxTopUpAmount = "";

    public static String _newAgentCreatedDateTime;

    public static List<String> _agentNamesBeforeSort = new ArrayList<>();
    public static List<String> _agentNumberBeforeSort = new ArrayList<>();
    public static List<String> _agentStatusBeforeSort = new ArrayList<>();
    public static List<LocalDateTime> _agentDateTimeBeforeSort = new ArrayList<>();

    public static boolean _isRandomFilter;
    public static boolean _isRandomBoolean;

    public static int _randomNumber;

    public static String _searchCriteria;


    @FindAll(value = {@FindBy(xpath = "//table[@id='agentsTbl']//tr//td[2]//div")})
    private List<WebElement> lstAgentMobileNumber;

    @FindAll(value = {@FindBy(xpath = "//table[@id='agentsTbl']//tr//td[3]//div")})
    private List<WebElement> lstAgentStatus;

    @FindAll(value = {@FindBy(xpath = "//table[@id='agentsTbl']//tr//td[5]//div")})
    private List<WebElement> lstAgentCreatedDateTime;

    @FindAll(value = {@FindBy(xpath = "//table[@id='agentsTbl']//tr//td[6]//a")})
    private List<WebElement> lstMOREButton;

    @FindAll(value = {@FindBy(xpath = "//table[@id='agentsTbl']//tr//td[1]//h5")})
    private List<WebElement> lstAgentName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='agentsTbl']//tr//td[1]//following-sibling::div")})
    private List<WebElement> lstAgentId;

    @FindAll(value = {@FindBy(xpath = "//table[@id='agentsTbl']//tr//td[4]//a")})
    private List<WebElement> lstAgentTopUpStatus;

    @FindAll(value = {@FindBy(xpath = "//ul[contains(@class,'tstatus-list')]//li//span")})
    private List<WebElement> lstAgentTopUpStatusDetails;

    @FindBy(xpath = "//div[contains(@class,'spr-usr-tabs')]//a[@id='infoTargets']")
    private WebElement btnAgentTask;

    @FindBy(xpath = "//div[contains(@class,'spr-usr-tabs')]//a[@id='transList']")
    private WebElement btnAgentHistory;

    @FindBy(xpath = "//li/button[contains(@class,'filterBtn')]")
    private WebElement btnFilter;

    @FindBy(xpath = "//input[@formcontrolname='fname']")
    private WebElement txtFilterFirstName;

    @FindBy(xpath = "//input[@formcontrolname='lname']")
    private WebElement txtFilterLastName;

    @FindBy(xpath = "//input[@formcontrolname='id']")
    private WebElement txtFilterAgentID;

    @FindBy(xpath = "//input[@formcontrolname='phone']")
    private WebElement txtFilterAgentMobile;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//button[contains(@class,'searchbtn')]")
    private WebElement btnFilterSearch;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//a[contains(text(),'Clear All')]")
    private WebElement btnFilterClearAll;

    @FindBy(xpath = "//mdb-date-picker[@formcontrolname='date_begin']//input")
    private WebElement btnFilterDateBegin;

    @FindBy(xpath = "//mdb-date-picker[@formcontrolname='date_end']//input")
    private WebElement btnFilterDateEnd;

    @FindBy(xpath = "//mdb-select[@formcontrolname='status']")
    private WebElement btnStatusDropdown;

    @FindAll(value = {@FindBy(xpath = "//mdb-select[@formcontrolname='status']//li/span")})
    private List<WebElement> lstStatuses;

    @FindBy(xpath = "//table[@id='agentsTbl']//th[1]")
    private WebElement lblAgentNameHeader;

    @FindBy(xpath = "//table[@id='agentsTbl']//th[2]")
    private WebElement lblAgentNumberHeader;

    @FindBy(xpath = "//table[@id='agentsTbl']//th[3]")
    private WebElement lblAgentStatusHeader;

    @FindBy(xpath = "//table[@id='agentsTbl']//th[5]")
    private WebElement lblAgentDateTimeHeader;

    @FindBy(xpath = "//ul/li[contains(@id,'sb_addagents')]")
    private WebElement btnMenuCreateNewAgent;

    @FindBy(xpath = "//input[@formcontrolname='first_name']")
    private WebElement txtFirstName;

    @FindBy(xpath = "//input[@formcontrolname='last_name']")
    private WebElement txtLastName;

    @FindBy(xpath = "//input[contains(@placeholder,'Date of Birth')]")
    private WebElement txtDOBPicker;

    @FindBy(xpath = "//input[@formcontrolname='email']")
    private WebElement txtEmailAddress;

    @FindBy(xpath = "//input[@formcontrolname='phone']")
    private WebElement txtPhoneNumber;

    @FindBy(xpath = "//div[@id='step1']//button[contains(text(),'Next')]")
    private WebElement btnNextStep1;

    @FindBy(xpath = "//table//button[contains(@class,'yearlabel')]")
    private WebElement btnYearLabel;

    @FindAll(value = {@FindBy(xpath = "//table//button[contains(@class,'yearchangebtn')]")})
    private List<WebElement> lstChangeYearButton;

    @FindAll(value = {@FindBy(xpath = "//table//td//div[contains(@class,'yearvalue')]")})
    private List<WebElement> lstBirthYears;

    @FindBy(xpath = "//table//button[contains(@class,'monthlabel')]")
    private WebElement btnMonthLabel;

    @FindAll(value = {@FindBy(xpath = "//table//td//div[contains(@class,'monthvalue')]")})
    private List<WebElement> lstBirthMonths;

    @FindAll(value = {@FindBy(xpath = "//table//td//div[contains(@class,'currmonth')]")})
    private List<WebElement> lstBirthDates;

    @FindBy(xpath = "//input[@id='profile_pic']")
    private WebElement btnUploadProfilePicture;

    @FindBy(xpath = "//input[@id='aadhaar_front']")
    private WebElement btnUploadAdhaarFront;

    @FindBy(xpath = "//input[@id='aadhaar_back']")
    private WebElement btnUploadAdhharBack;

    @FindBy(xpath = ".//input[@id='otp_val']")
    private WebElement txtStep2OTP;

    @FindBy(xpath = "//div[@id='step2']//button[contains(text(),'Next')]")
    private WebElement btnNextStep2;

    @FindBy(xpath = "//div[@id='step3']//button[contains(text(),'Next')]")
    private WebElement btnNextStep3;

    @FindBy(xpath = "//div[@id='step2']//button[contains(text(),'Previous')]")
    private WebElement btnPreviousStep2;

    @FindBy(xpath = "//a[contains(text(),'Resend')]")
    private WebElement btnResendOTP;

    @FindBy(xpath = "//input[@formcontrolname='address1']")
    private WebElement txtHomeFlatAddress;

    @FindBy(xpath = "//input[@formcontrolname='address2']")
    private WebElement txtStreet1Address;

    @FindBy(xpath = "//input[@formcontrolname='address3']")
    private WebElement txtStreet2Address;

    @FindBy(xpath = "//input[@formcontrolname='village']")
    private WebElement txtVillage;

    @FindBy(xpath = "//input[@formcontrolname='tehsil']")
    private WebElement txtTehsil;

    @FindBy(xpath = "//input[@formcontrolname='district']")
    private WebElement txtDistrict;

    @FindBy(xpath = "//input[@formcontrolname='country']")
    private WebElement txtCountry;

    @FindBy(xpath = "//input[@formcontrolname='pincode']")
    private WebElement txtPinCode;

    @FindBy(xpath = "//select[@formcontrolname='state']")
    private WebElement btnState;

    @FindBy(xpath = "//select[@formcontrolname='city']")
    private WebElement btnCity;

    @FindBy(xpath = "//input[@formcontrolname='target_consumer']")
    private WebElement txtTargetNewConsumer;

    @FindBy(xpath = "//input[@formcontrolname='target_merchant']")
    private WebElement txtTargetNewMerchant;

    @FindBy(xpath = "//input[@formcontrolname='min_topup_amount']")
    private WebElement txtMinTopUpAmount;

    @FindBy(xpath = "//input[@formcontrolname='max_topup_amount']")
    private WebElement txtMaxTopUpAmount;

    @FindBy(xpath = "//div[@id='step4']//button[contains(text(),'Cancel')]")
    private WebElement btnCancel;

    @FindBy(xpath = "//div[@id='step4']//button[contains(text(),'Create')]")
    private WebElement btnCreateAgent;

    @FindBy(xpath = "//div[contains(@class,'swal2-actions')]//button[text()='Ok!']")
    private WebElement btnCancelOk;

    @FindBy(xpath = "//div[contains(@class,'swal2-actions')]//button[text()='Cancel']")
    private WebElement btnCancelCancel;

    @FindBy(xpath = "//input[@type='search']")
    private WebElement txtInputSearch;

    @FindBy(xpath = "//div//a[contains(text(),'Clear Date Range')]")
    private WebElement btnClearDateRange;

    @FindBy(xpath = "//label[@for='filter-start']//..//input")
    private WebElement txtFromDateFilter;

    @FindBy(xpath = "//label[@for='filter-end']//..//input")
    private WebElement txtToDateFilter;

    @FindBy(xpath = "//div[contains(@class,'headeryeartxt')]//button")
    private WebElement lblCurrentYear;

    @FindBy(xpath = "//div[contains(@class,'headermonthtxt')]//button")
    private WebElement lblCurrentMonth;

    @FindBy(xpath = "//table[contains(@class,'yeartable')]//div[contains(@class,'yearvalue')]")
    private List<WebElement> lstDateYears;

    @FindBy(xpath = "//table[contains(@class,'monthtable')]//div[contains(@class,'monthvalue')]")
    private List<WebElement> lstDateMonths;

    @FindBy(xpath = "//table[contains(@class,'caltable')]//div[contains(@class,'datevalue')]/span")
    private List<WebElement> lstDateMonthDates;

    public SuperAgentAgentListVerification getAnyAgentDetails() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");
        DateTimeFormatter format2 = DateTimeFormatter.ofPattern("dd MMMM yyyy", Locale.ENGLISH);

        _randomNumber = getRandomNumberBetween(0, lastIndexOf(lstAgentName));

        _agentName = getInnerText(lstAgentName.get(_randomNumber));
        _agentId = getInnerText(lstAgentId.get(_randomNumber));
        _agentMobileNumber = getInnerText(lstAgentMobileNumber.get(_randomNumber));
        _agentAccountStatus = getInnerText(lstAgentStatus.get(_randomNumber));

        scrollToElement(driver, lstAgentTopUpStatus.get(_randomNumber));
        clickOn(driver, lstAgentTopUpStatus.get(_randomNumber));

        _agentConsumerTargetStatus = getInnerText(lstAgentTopUpStatusDetails.get(0));
        _agentMerchantTargetStatus = getInnerText(lstAgentTopUpStatusDetails.get(1));
        _agentConsumerTopUpStatus = getInnerText(lstAgentTopUpStatusDetails.get(2));

        _agentCreatedDate = format2.format(LocalDate.parse(getInnerText(lstAgentCreatedDateTime.get(_randomNumber)), format));

        return new SuperAgentAgentListVerification(driver);
    }

    public boolean isAgentListDisplay() {
        return !isListEmpty(lstAgentName);
    }

    public SuperAgentAgentListVerification clickOnMOREButton() {

        scrollToElement(driver, lstMOREButton.get(_randomNumber));

        testStepsLog(_logStep++, "Click on MORE button.");
        clickOn(driver, lstMOREButton.get(_randomNumber));

        return new SuperAgentAgentListVerification(driver);

    }

    public SuperAgentAgentListVerification clickOnAgentTask() {

        scrollToElement(driver, btnAgentTask);

        testStepsLog(_logStep++, "Click on Agent Task tab.");
        clickOn(driver, btnAgentTask);

        return new SuperAgentAgentListVerification(driver);

    }

    public SuperAgentAgentListVerification clickOnHistory() {

        scrollElement(btnAgentTask);

        testStepsLog(_logStep++, "Click on History tab.");
        clickOn(driver, btnAgentHistory);

        return new SuperAgentAgentListVerification(driver);

    }

    public SuperAgentAgentListVerification clickOnFilterButton() {

        testStepsLog(_logStep++, "Click Filter button.");
        clickOn(driver, btnFilter);

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification getAgentDetailsForFilter() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");
        DateTimeFormatter format2 = DateTimeFormatter.ofPattern("dd MMM yyyy", Locale.ENGLISH);

        pause(2);

        _filterAgentFirstName = getInnerText(lstAgentName.get(0)).split(" ")[0].trim();
        _filterAgentLastName = getInnerText(lstAgentName.get(0)).replace(_filterAgentFirstName, "").trim();
        _filterAgentId = getInnerText(lstAgentId.get(0));
        _filterAgentPhone = getInnerText(lstAgentMobileNumber.get(0));

        for (int num = 0; num < sizeOf(lstAgentCreatedDateTime); num++) {
            _filterAgentDate.add(format2.format(LocalDate.parse(getInnerText(lstAgentCreatedDateTime.get(0)), format)));
        }

        System.out.println(_filterAgentFirstName);

        return new SuperAgentAgentListVerification(driver);

    }

    public SuperAgentAgentListVerification filterFirstName(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomCharacters(7);
            testStepsLog(_logStep++, "Enter First Name : " + _filterRandomValue);
            type(txtFilterFirstName, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter First Name : " + _filterAgentFirstName);
            type(txtFilterFirstName, _filterAgentFirstName);
        }

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification clickOnFilterSearchButton() {

        testStepsLog(_logStep++, "Click on Search button.");
        clickOn(driver, btnFilterSearch);

        pause(2);

        try {
            updateShowList(driver, findElementByName(driver, "agentsTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        pause(1);

        return new SuperAgentAgentListVerification(driver);

    }

    public SuperAgentAgentListVerification clearAllFilterDetails() {

        testStepsLog(_logStep++, "Click on Clear All button.");
        clickOn(driver, btnFilterClearAll);

        pause(2);

        try {
            updateShowList(driver, findElementByName(driver, "agentsTbl_length"), "All");
        } catch (Exception e) {
            System.out.println("No Show View.");
        }

        pause(1);

        return new SuperAgentAgentListVerification(driver);

    }

    public SuperAgentAgentListVerification filterLastName(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomCharacters(7);
            testStepsLog(_logStep++, "Enter Last Name : " + _filterRandomValue);
            type(txtFilterLastName, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter Last Name : " + _filterAgentLastName);
            type(txtFilterLastName, _filterAgentLastName);
        }

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification filterAgentId(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomNumber() + "" + getRandomNumber();
            testStepsLog(_logStep++, "Enter Random ID : " + _filterRandomValue);
            type(txtFilterAgentID, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter Agent ID : " + _filterAgentId);
            type(txtFilterAgentID, _filterAgentId);
        }

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification filterAgentNumber(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomNumber() + "" + getRandomNumber();
            testStepsLog(_logStep++, "Enter Mobile Number : " + _filterRandomValue);
            type(txtFilterAgentMobile, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter Mobile Number : " + _filterAgentPhone.split(" ")[1]);
            type(txtFilterAgentMobile, _filterAgentPhone.split(" ")[1]);
        }

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification filterStatus(String status) {

        scrollToElement(driver, btnClearDateRange);

        clickOn(driver, btnStatusDropdown);

        _filterStatus = status;

        switch (status.toLowerCase()) {
            case "active":
                clickOn(driver, lstStatuses.get(1));
                break;
            case "kyc pending":
                clickOn(driver, lstStatuses.get(2));
                break;
            case "login pending":
                clickOn(driver, lstStatuses.get(3));
                break;
            default:
                clickOn(driver, lstStatuses.get(0));
                break;
        }

        testStepsLog(_logStep++, "Select Status : " + status);

        return new SuperAgentAgentListVerification(driver);

    }

    public SuperAgentAgentListVerification setAgentStatus() {

        _agentStatusBeforeSort.clear();

        for (WebElement status : lstAgentStatus) {
            _agentStatusBeforeSort.add(getInnerText(status));
        }

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification clickOnAgentNameToSort() {

        for (WebElement name : lstAgentName) {
            _agentNamesBeforeSort.add(getInnerText(name));
        }

        testStepsLog(_logStep++, "Click On Agent Name to sort.");
        clickOn(driver, lblAgentNameHeader);

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification clickOnPhoneNumberToSort() {

        for (WebElement cell : lstAgentMobileNumber) {
            _agentNumberBeforeSort.add(getInnerText(cell));
        }

        testStepsLog(_logStep++, "Click On Mobile Number to sort.");
        clickOn(driver, lblAgentNumberHeader);

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification clickOnStatusToSort() {

        for (WebElement status : lstAgentStatus) {
            _agentStatusBeforeSort.add(getInnerText(status));
        }

        testStepsLog(_logStep++, "Click On Account Status to sort.");
        clickOn(driver, lblAgentStatusHeader);

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification clickOnDateTimeToSort() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        for (WebElement status : lstAgentCreatedDateTime) {
            _agentDateTimeBeforeSort.add(LocalDateTime.parse(getInnerText(status), format));
        }

        testStepsLog(_logStep++, "Click On Date & Time to sort.");
        clickOn(driver, lblAgentDateTimeHeader);

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification clickOnCreateNewAgentMenu() {

        testStepsLog(_logStep++, "Click on Create New Agent Menu icon.");
        clickOn(driver, btnMenuCreateNewAgent);

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification enterNameMinChar(String field) {

        String randomName = getRandomCharacters(1);

        if (field.equalsIgnoreCase("Last Name")) {
            type(txtLastName, randomName);
            testStepsLog(_logStep++, "Enter Last Name");
            testInfoLog("Last Name", randomName);
        } else {
            type(txtFirstName, randomName);
            testStepsLog(_logStep++, "Enter First Name");
            testInfoLog("First Name", randomName);
        }

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification enterNameNumber(String field) {

        String randomName = String.valueOf(getRandomNumber());

        if (field.equalsIgnoreCase("Last Name")) {
            type(txtLastName, randomName);
            testStepsLog(_logStep++, "Enter Last Name");
            testInfoLog("Last Name", randomName);
        } else {
            type(txtFirstName, randomName);
            testStepsLog(_logStep++, "Enter First Name");
            testInfoLog("First Name", randomName);
        }

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification enterNameMaxChar(String field) {

        String randomName = getRandomCharacters(21);

        if (field.equalsIgnoreCase("Last Name")) {
            type(txtLastName, randomName);
            testStepsLog(_logStep++, "Enter Last Name");
            testInfoLog("Last Name", randomName);
        } else {
            type(txtFirstName, randomName);
            testStepsLog(_logStep++, "Enter First Name");
            testInfoLog("First Name", randomName);
        }

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification enterName(String field) {

        if (field.equalsIgnoreCase("Last Name")) {
            _newAgentLastName = getRandomLastName();
            type(txtLastName, _newAgentLastName);
            testStepsLog(_logStep++, "Enter Last Name");
            testInfoLog("Last Name", _newAgentLastName);
        } else {
            _newAgentFirstName = getRandomFirstName();
            type(txtFirstName, _newAgentFirstName);
            testStepsLog(_logStep++, "Enter First Name");
            testInfoLog("First Name", _newAgentFirstName);
        }

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification enterRandomDOB() {

        String dobYear = getDOBYear();
        String dobMonth = getDOBMonth();
        String dobDate = getDOBDate();

        _newAgentDOB = dobDate + "/" + dobMonth + "/" + dobYear;

        testInfoLog("Selected DOB", _newAgentDOB);

        return new SuperAgentAgentListVerification(driver);
    }

    private String getDOBYear() {

        testStepsLog(_logStep++, "Click on Date Picker.");
        clickOn(driver, txtDOBPicker);

        testStepsLog(_logStep++, "Click on Year to change.");
        clickOn(driver, btnYearLabel);
        clickOn(driver, lstChangeYearButton.get(0));

        int index = getRandomIndex(lstBirthYears);
        String year = getText(lstBirthYears.get(index));
        testInfoLog("Select Year", year);
        clickOn(driver, lstBirthYears.get(index));

        return year;
    }

    private String getDOBMonth() {


        testStepsLog(_logStep++, "Click on Month to change.");
        clickOn(driver, btnMonthLabel);

        int index = getRandomIndex(lstBirthMonths);
        String month = getText(lstBirthMonths.get(index));
        testInfoLog("Select Month", month);
        clickOn(driver, lstBirthMonths.get(index));

        return getNumericMonth(month);
    }

    private String getDOBDate() {

        testStepsLog(_logStep++, "Click on Date to select.");

        int index = getRandomIndex(lstBirthDates);
        int date = getIntegerFromString(getText(lstBirthDates.get(index)));
        testInfoLog("Select Date", String.valueOf(date));
        clickOn(driver, lstBirthDates.get(index));

        return (date < 10 ? "0" : "") + date;
    }

    public SuperAgentAgentListVerification enterEmail(boolean isInvalid) {

        if (isInvalid) {
            String randomMail = getInvalidEmail();
            type(txtEmailAddress, randomMail);
            testStepsLog(_logStep++, "Enter Email");
            testInfoLog("Email", randomMail);
        } else {
            _newAgentEmail = getRegistrationEmail();
            type(txtEmailAddress, _newAgentEmail);
            testStepsLog(_logStep++, "Enter Email");
            testInfoLog("Email", _newAgentEmail);
        }

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification enterMinMobileNumber() {

        String randomNumber = getRandomNumber() + "";

        type(txtPhoneNumber, randomNumber);
        testStepsLog(_logStep++, "Enter Phone Number");
        testInfoLog("Phone Number", randomNumber);

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification enterMaxMobileNumber() {

        String randomNumber = getRandomNumber() + "" + getRandomNumber() + "" + getRandomNumberBetween(0, 9);

        type(txtPhoneNumber, randomNumber);
        testStepsLog(_logStep++, "Enter Phone Number");
        testInfoLog("Phone Number", randomNumber);

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification enterInvalidMobileNumber() {

        String randomNumber = getRandomCharacters(5);

        type(txtPhoneNumber, randomNumber);
        testStepsLog(_logStep++, "Enter Phone Number");
        testInfoLog("Phone Number", randomNumber);

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification enterMobileNumber() {

        _newAgentMobileNumber = createUserNumber;

        type(txtPhoneNumber, _newAgentMobileNumber);
        testStepsLog(_logStep++, "Enter Phone Number");
        testInfoLog("Phone Number", _newAgentMobileNumber);

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification uploadProfilePicture(boolean isInvalid) {

        testStepsLog(_logStep++, "Upload Profile Picture");

        if (isInvalid)
            sendKeys(btnUploadProfilePicture, INVALID_IMAGE);
        else
            sendKeys(btnUploadProfilePicture, PROFILE_PIC);

        pause(1);

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification uploadAdhaarFront(boolean isInvalid) {

        testStepsLog(_logStep++, "Upload Adhaar Front");

        if (isInvalid)
            sendKeys(btnUploadAdhaarFront, INVALID_IMAGE);
        else
            sendKeys(btnUploadAdhaarFront, ADHAAR_FRONT);

        pause(1);

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification uploadAdhaarBack(boolean isInvalid) {

        testStepsLog(_logStep++, "Upload Adhaar Back");

        if (isInvalid)
            sendKeys(btnUploadAdhharBack, INVALID_IMAGE);
        else
            sendKeys(btnUploadAdhharBack, ADHAAR_BACK);

        pause(1);

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification clickStep1OnNextButton() {

        scrollToElement(driver, btnNextStep1);

        testStepsLog(_logStep++, "Click on Next Button.");
        clickOn(driver, btnNextStep1);

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification enterOTP(String otp) {
        testStepsLog(_logStep++, "Enter OTP.");
        testInfoLog("OTP", otp);
        type(txtStep2OTP, otp);
        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification clickOnStep2NextButton() {

        scrollToElement(driver, btnNextStep2);

        testStepsLog(_logStep++, "Click on Next Button.");
        clickOn(driver, btnNextStep2);

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification clickOnStep3NextButton() {

        scrollToElement(driver, btnNextStep3);

        testStepsLog(_logStep++, "Click on Next Button.");
        clickOn(driver, btnNextStep3);

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification waitAndClickResendButton() {

        while (!isElementPresent(btnResendOTP)) {
            System.out.println("in loop");
            pause(10);
        }

        testStepsLog(_logStep++, "Click on Resend OTP button.");
        clickOn(driver, btnResendOTP);

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification enterStep2OTP() {

        int count = 0;

        while (getPropertyValueOf("Super Agent", "newagent_otp").isEmpty() && count < 16) {
            pause(1);
            count++;
        }

        testStepsLog(_logStep++, "Enter OTP.");
        String otp = getPropertyValueOf("Super Agent", "newagent_otp");
        testInfoLog("OTP", otp);
        type(txtStep2OTP, otp);

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification enterHomeAddress(boolean isInvalid) {

        scrollToElement(driver, txtHomeFlatAddress);

        if (isInvalid) {
            String randomAddress = getRandomCharacters(51);
            type(txtHomeFlatAddress, randomAddress);
            testStepsLog(_logStep++, "Enter Home/Flat Address");
            testInfoLog("Home/Flat Address", randomAddress);
        } else {
            _newAgentFlatNumber = getBuildingNumber();
            type(txtHomeFlatAddress, _newAgentFlatNumber);
            testStepsLog(_logStep++, "Enter Home/Flat Address");
            testInfoLog("Home/Flat Address", _newAgentFlatNumber);
        }
        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification enterStreet1Address(boolean isInvalid) {
        if (isInvalid) {
            String randomAddress = getRandomCharacters(51);
            type(txtStreet1Address, randomAddress);
            testStepsLog(_logStep++, "Enter Street 1 Address");
            testInfoLog("Street 1", randomAddress);
        } else {
            _newAgentStreet1 = getRandomStreetName();
            type(txtStreet1Address, _newAgentStreet1);
            testStepsLog(_logStep++, "Enter Street 1 Address");
            testInfoLog("Street 1", _newAgentStreet1);
        }
        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification enterStreet2Address(boolean isInvalid) {
        _isRandomBoolean = getRandomBoolean();
        if (isInvalid) {
            String randomAddress = getRandomCharacters(51);
            type(txtStreet2Address, randomAddress);
            testStepsLog(_logStep++, "Enter Street 2 Address");
            testInfoLog("Street 2", randomAddress);
        } else {
            if (_isRandomBoolean) {
                _newAgentStreet2 = getRandomStreetName();
                type(txtStreet2Address, _newAgentStreet2);
                testStepsLog(_logStep++, "Enter Street 2 Address");
                testInfoLog("Street 2", _newAgentStreet2);
            } else {
                type(txtStreet2Address, getRandomCharacters(3));
                clear(txtStreet2Address);
                _newAgentStreet2 = "";
            }
        }
        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification enterVillageAddress(boolean isInvalid) {
        _isRandomBoolean = getRandomBoolean();
        if (isInvalid) {
            String randomAddress = getRandomCharacters(51);
            type(txtVillage, randomAddress);
            testStepsLog(_logStep++, "Enter Village");
            testInfoLog("Village", randomAddress);
        } else {
            if (_isRandomBoolean) {
                _newAgentVillage = getRandomVillage();
                type(txtVillage, _newAgentVillage);
                testStepsLog(_logStep++, "Enter Village");
                testInfoLog("Village", _newAgentVillage);
            } else {
                type(txtVillage, getRandomCharacters(3));
                clear(txtVillage);
                _newAgentVillage = "";
            }
        }
        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification enterTehsilAddress(boolean isInvalid) {
        _isRandomBoolean = getRandomBoolean();
        if (isInvalid) {
            String randomAddress = getRandomCharacters(51);
            type(txtTehsil, randomAddress);
            testStepsLog(_logStep++, "Enter Tehsil/Taluka");
            testInfoLog("Tehsil/Taluka", randomAddress);
        } else {
            if (_isRandomBoolean) {
                _newAgentTeshsil = getRandomVillage();
                type(txtTehsil, _newAgentTeshsil);
                testStepsLog(_logStep++, "Enter Tehsil/Talika");
                testInfoLog("Tehsil/Taluka", _newAgentTeshsil);
            } else {
                type(txtTehsil, getRandomCharacters(3));
                clear(txtTehsil);
                _newAgentTeshsil = "";
            }
        }
        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification enterDistrictAddress(boolean isInvalid) {
        _isRandomBoolean = getRandomBoolean();
        if (isInvalid) {
            String randomAddress = getRandomCharacters(51);
            type(txtDistrict, randomAddress);
            testStepsLog(_logStep++, "Enter District");
            testInfoLog("District", randomAddress);
        } else {
            if (_isRandomBoolean) {
                _newAgentDistrict = getRandomDistrict();
                type(txtDistrict, _newAgentDistrict);
                testStepsLog(_logStep++, "Enter District");
                testInfoLog("District", _newAgentDistrict);
            } else {
                type(txtDistrict, getRandomCharacters(3));
                clear(txtDistrict);
                _newAgentDistrict = "";
            }
        }
        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification selectState() {

        Select state = new Select(btnState);
        int index = getRandomNumberBetween(0, state.getOptions().size() - 1);

        testStepsLog(_logStep++, "Select State");
        state.selectByIndex(index);
        _newAgentState = getText(state.getFirstSelectedOption());
        testInfoLog("State", _newAgentState);

        pause(3);

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification selectCity() {

        Select city = new Select(btnCity);
        int index = getRandomNumberBetween(0, city.getOptions().size() - 1);

        testStepsLog(_logStep++, "Select City");
        city.selectByIndex(index);
        _newAgentCity = getText(city.getFirstSelectedOption());
        testInfoLog("City", _newAgentCity);

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification enterNumberCountry() {

        String randomCountry = String.valueOf(getRandomNumber());
        type(txtCountry, randomCountry);
        testStepsLog(_logStep++, "Enter Country");
        testInfoLog("Country", randomCountry);

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification enterMinCharCountry() {

        String randomCountry = getRandomCharacters(1);
        type(txtCountry, randomCountry);
        testStepsLog(_logStep++, "Enter Country");
        testInfoLog("Country", randomCountry);

        return new SuperAgentAgentListVerification(driver);

    }

    public SuperAgentAgentListVerification enterMaxCharCountry() {

        String randomCountry = getRandomCharacters(31);
        type(txtCountry, randomCountry);
        testStepsLog(_logStep++, "Enter Country");
        testInfoLog("Country", randomCountry);

        return new SuperAgentAgentListVerification(driver);

    }

    public SuperAgentAgentListVerification enterCountry() {

        _newAgentCountry = getRandomCountry().replaceAll("[^a-zA-Z0-9]", "");
        type(txtCountry, _newAgentCountry);
        testStepsLog(_logStep++, "Enter Country");
        testInfoLog("Country", _newAgentCountry);

        return new SuperAgentAgentListVerification(driver);

    }

    public SuperAgentAgentListVerification enterMinPinCode() {

        scrollToElement(driver, txtPinCode);

        String randomNumber = String.valueOf(getRandomNumberBetween(100, 999));

        type(txtPinCode, randomNumber);
        testStepsLog(_logStep++, "Enter Pin Code");
        testInfoLog("Pin Code", randomNumber);

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification enterMaxPinCode() {

        scrollToElement(driver, txtPinCode);

        String randomNumber = getRandomNumber() + "" + getRandomNumber();

        type(txtPinCode, randomNumber);
        testStepsLog(_logStep++, "Enter Pin Code");
        testInfoLog("Pin Code", randomNumber);

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification enterInvalidPinCode() {

        scrollToElement(driver, txtPinCode);

        String randomNumber = getRandomCharacters(5);

        type(txtPinCode, randomNumber);
        testStepsLog(_logStep++, "Enter Pin Code");
        testInfoLog("Pin Code", randomNumber);

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification enterPinCode() {

        scrollToElement(driver, txtPinCode);

        _newAgentPinCode = getRandomPinCode();

        type(txtPinCode, _newAgentPinCode);
        testStepsLog(_logStep++, "Enter Pin Code");
        testInfoLog("Pin Code", _newAgentPinCode);

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification clickOnCancelButton() {

        scrollToElement(driver, btnCancel);

        testStepsLog(_logStep++, "Click on Cancel Button.");
        clickOn(driver, btnCancel);

        return new SuperAgentAgentListVerification(driver);

    }

    public SuperAgentAgentListVerification clickOnCancelCancelButton() {

        scrollToElement(driver, btnCancelCancel);

        testStepsLog(_logStep++, "Click on Cancel Button.");
        clickOn(driver, btnCancelCancel);

        return new SuperAgentAgentListVerification(driver);

    }

    public SuperAgentAgentListVerification clickOnOKCancelButton() {

        scrollToElement(driver, btnCancelOk);

        testStepsLog(_logStep++, "Click on OK Button.");
        clickOn(driver, btnCancelOk);

        return new SuperAgentAgentListVerification(driver);

    }

    public SuperAgentAgentListVerification clickOnCreateAgentButton() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        scrollToElement(driver, btnCreateAgent);

        testStepsLog(_logStep++, "Click on Create Agent Button.");
        clickOn(driver, btnCreateAgent);

        _newAgentCreatedDateTime = format.format(LocalDateTime.now());

        return new SuperAgentAgentListVerification(driver);

    }

    public SuperAgentAgentListVerification enterMinNewConMer(String user) {

        scrollToElement(driver, txtTargetNewConsumer);

        if (user.equalsIgnoreCase("C")) {
            type(txtTargetNewConsumer, "0");
            testStepsLog(_logStep++, "Enter Target for New Consumer");
            testInfoLog("Enter Target for new Consumers", "0");
        } else {
            type(txtTargetNewMerchant, "0");
            testStepsLog(_logStep++, "Enter Target for New Merchant");
            testInfoLog("Enter Target for new Merchant", "0");
        }

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification enterMaxNewConMer(String user) {

        scrollToElement(driver, txtTargetNewConsumer);

        if (user.equalsIgnoreCase("C")) {
            String randomNumber = String.valueOf(getRandomNumberBetween(1000, 9999));
            type(txtTargetNewConsumer, randomNumber);
            testStepsLog(_logStep++, "Enter Target for New Consumer");
            testInfoLog("Enter Target for new Consumers", randomNumber);
        } else {
            String randomNumber = String.valueOf(getRandomNumberBetween(1000, 9999));
            type(txtTargetNewMerchant, randomNumber);
            testStepsLog(_logStep++, "Enter Target for New Merchant");
            testInfoLog("Enter Target for new Merchant", randomNumber);
        }

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification enterInvalidNewConMer(String user) {

        scrollToElement(driver, txtTargetNewConsumer);

        if (user.equalsIgnoreCase("C")) {
            String randomNumber = getRandomCharacters(3);
            type(txtTargetNewConsumer, randomNumber);
            testStepsLog(_logStep++, "Enter Target for New Consumer");
            testInfoLog("Enter Target for new Consumers", randomNumber);
        } else {
            String randomNumber = getRandomCharacters(3);
            type(txtTargetNewMerchant, randomNumber);
            testStepsLog(_logStep++, "Enter Target for New Merchant");
            testInfoLog("Enter Target for new Merchant", randomNumber);
        }

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification enterNewTargetForConMer(String user) {

        scrollToElement(driver, txtTargetNewConsumer);

        if (user.equalsIgnoreCase("C")) {
            _newAgentNewConsumerTarget = String.valueOf(getRandomNumberBetween(1, 10));
            type(txtTargetNewConsumer, _newAgentNewConsumerTarget);
            testStepsLog(_logStep++, "Enter Target for New Consumer");
            testInfoLog("Enter Target for new Consumers", _newAgentNewConsumerTarget);
        } else {
            _newAgentNewMerchantTarget = String.valueOf(getRandomNumberBetween(1, 10));
            type(txtTargetNewMerchant, _newAgentNewMerchantTarget);
            testStepsLog(_logStep++, "Enter Target for New Merchant");
            testInfoLog("Enter Target for new Merchant", _newAgentNewMerchantTarget);
        }

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification enterMinTopUpAmount(String type) {

        if (type.equalsIgnoreCase("Min")) {
            String randomNumber = String.valueOf(getRandomNumberBetween(0, 199));
            type(txtMinTopUpAmount, randomNumber);
            testStepsLog(_logStep++, "Enter Minimum Topup Amount");
            testInfoLog("Enter Minimum Topup Amount", randomNumber);
        } else {
            String randomNumber = String.valueOf(getRandomNumberBetween(0, 199));
            type(txtMaxTopUpAmount, randomNumber);
            testStepsLog(_logStep++, "Enter Maximum Topup Amount");
            testInfoLog("Enter Maximum Topup Amount", randomNumber);
        }

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification enterMaxTopUpAmount(String type) {

        if (type.equalsIgnoreCase("Min")) {
            String randomNumber = String.valueOf(getRandomNumberBetween(100001, 999999));
            type(txtMinTopUpAmount, randomNumber);
            testStepsLog(_logStep++, "Enter Minimum Topup Amount");
            testInfoLog("Enter Minimum Topup Amount", randomNumber);
        } else {
            String randomNumber = String.valueOf(getRandomNumberBetween(100001, 999999));
            type(txtMaxTopUpAmount, randomNumber);
            testStepsLog(_logStep++, "Enter Maximum Topup Amount");
            testInfoLog("Enter Maximum Topup Amount", randomNumber);
        }

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification enterInvalidTopUpAmount(String type) {

        if (type.equalsIgnoreCase("Min")) {
            String randomNumber = getRandomCharacters(3);
            type(txtMinTopUpAmount, randomNumber);
            testStepsLog(_logStep++, "Enter Minimum Topup Amount");
            testInfoLog("Enter Minimum Topup Amount", randomNumber);
        } else {
            String randomNumber = getRandomCharacters(3);
            type(txtMaxTopUpAmount, randomNumber);
            testStepsLog(_logStep++, "Enter Maximum Topup Amount");
            testInfoLog("Enter Maximum Topup Amount", randomNumber);
        }

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification enterMinMaxSwapAmount(String type) {

        if (type.equalsIgnoreCase("Min")) {
            String randomNumber = String.valueOf(getRandomNumberBetween(700, 1000));
            type(txtMinTopUpAmount, randomNumber);
            testStepsLog(_logStep++, "Enter Minimum Topup Amount");
            testInfoLog("Enter Minimum Topup Amount", randomNumber);
        } else {
            String randomNumber = String.valueOf(getRandomNumberBetween(200, 500));
            type(txtMaxTopUpAmount, randomNumber);
            testStepsLog(_logStep++, "Enter Maximum Topup Amount");
            testInfoLog("Enter Maximum Topup Amount", randomNumber);
        }

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification enterNewTopUpAmount(String type) {

        if (type.equalsIgnoreCase("Min")) {
            _newAgentMinTopUpAmount = String.valueOf(getRandomNumberBetween(200, 500));
            type(txtMinTopUpAmount, _newAgentMinTopUpAmount);
            testStepsLog(_logStep++, "Enter Minimum Topup Amount");
            testInfoLog("Enter Minimum Topup Amount", _newAgentMinTopUpAmount);
        } else {
            _newAgentMaxTopUpAmount = String.valueOf(getRandomNumberBetween(700, 1000));
            type(txtMaxTopUpAmount, _newAgentMaxTopUpAmount);
            testStepsLog(_logStep++, "Enter Maximum Topup Amount");
            testInfoLog("Enter Maximum Topup Amount", _newAgentMaxTopUpAmount);
        }

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification displayUserDetails() {

        System.out.println(_newAgentFirstName + "  " + _newAgentLastName + "  "
                + _newAgentEmail + "  " + _newAgentMobileNumber);

        System.out.println(_newAgentFlatNumber + "  " + _newAgentStreet1 + "  " + _newAgentStreet2 + "  " +
                _newAgentVillage + "  " + _newAgentTeshsil + "  " + _newAgentDistrict + "  " + _newAgentState + "  " +
                _newAgentCity + "  " + _newAgentCountry + "  " + _newAgentPinCode);

        System.out.println(_newAgentNewConsumerTarget + "  " + _newAgentNewMerchantTarget + "  " +
                _newAgentMinTopUpAmount + "  " + _newAgentMaxTopUpAmount);

        System.out.println(_newAgentCreatedDateTime);

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification enterSearchIDCriteria(boolean isInvalid) {

        if (isInvalid) {

            scrollToElement(driver, txtInputSearch);

            String searchCriteria = getRandomCharacters(10);
            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", searchCriteria);
            type(txtInputSearch, searchCriteria);

        } else {

            scrollToElement(driver, txtInputSearch);

            _searchCriteria = getInnerText(lstAgentId.get(getRandomNumberBetween(0, lastIndexOf(lstAgentId)))).split("-")[3];

            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", _searchCriteria);
            type(txtInputSearch, _searchCriteria);
        }

        return new SuperAgentAgentListVerification(driver);
    }


    public SuperAgentAgentListVerification getNewAgentDetails() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");
        DateTimeFormatter format2 = DateTimeFormatter.ofPattern("dd MMMM yyyy", Locale.ENGLISH);

        _agentName = getInnerText(lstAgentName.get(0));
        _agentId = getInnerText(lstAgentId.get(0));
        _agentMobileNumber = getInnerText(lstAgentMobileNumber.get(0));
        _agentAccountStatus = getInnerText(lstAgentStatus.get(0));

        scrollToElement(driver, lstAgentTopUpStatus.get(0));
        clickOn(driver, lstAgentTopUpStatus.get(0));

        _agentConsumerTargetStatus = getInnerText(lstAgentTopUpStatusDetails.get(0));
        _agentMerchantTargetStatus = getInnerText(lstAgentTopUpStatusDetails.get(1));
        _agentConsumerTopUpStatus = getInnerText(lstAgentTopUpStatusDetails.get(2));

        _agentCreatedDate = format2.format(LocalDate.parse(getInnerText(lstAgentCreatedDateTime.get(0)), format));
        _agentCreatedDateTime = getInnerText(lstAgentCreatedDateTime.get(0));

        return new SuperAgentAgentListVerification(driver);
    }

    public SuperAgentAgentListVerification openMOREInfo() {

        scrollToElement(driver, lstMOREButton.get(0));

        testStepsLog(_logStep++, "Click on MORE button.");
        clickOn(driver, lstMOREButton.get(0));

        return new SuperAgentAgentListVerification(driver);

    }


    public SuperAgentAgentListVerification filterInvalidDateRange() {

        scrollToElement(driver, btnClearDateRange);
        clickOn(driver, btnClearDateRange);

        pause(2);
        try {
            clickOn(driver, txtFromDateFilter);
            lblCurrentYear.isDisplayed();
        } catch (Exception e) {
            scrollToElement(driver, btnClearDateRange);
            pause(2);
            clickOn(driver, txtFromDateFilter);
        }
        pause(2);

        System.out.println(_filterAgentDate);

        selectDate(_filterAgentDate.get(lastIndexOf(_filterAgentDate)));

        try {
            clickOn(driver, txtToDateFilter);
            lblCurrentYear.isDisplayed();
        } catch (Exception e) {
            scrollToElement(driver, btnClearDateRange);
            pause(2);
            clickOn(driver, txtToDateFilter);
        }

        selectDate(_filterAgentDate.get(0));

        clickOnFilterSearchButton();

        return new SuperAgentAgentListVerification(driver);
    }

    private void selectDate(String date) {

        List<String> listOfYears = new ArrayList<>();
        List<String> listOfMonths = new ArrayList<>();
        List<String> listOfDays = new ArrayList<>();

        while (!date.split(" ")[2].equalsIgnoreCase(getText(lblCurrentYear))) {
            clickOn(driver, lblCurrentYear);
            for (WebElement year : lstDateYears) {
                listOfYears.add(getText(year));
            }
            while (!listOfYears.contains(date.split(" ")[2])) {

                clickOn(driver, lstChangeYearButton.get(0));

                lstDateYears = findElementsByXPath(driver,
                        "//table[contains(@class,'yeartable')]//div[contains(@class,'yearvalue')]");
                for (WebElement year : lstDateYears) {
                    listOfYears.add(getText(year));
                }
            }

            clickOn(driver, lstDateYears.get(listOfYears.indexOf(date.split(" ")[2])));
        }

        while (!date.split(" ")[1].equalsIgnoreCase(getText(lblCurrentMonth))) {
            clickOn(driver, lblCurrentMonth);
            for (WebElement month : lstDateMonths) {
                listOfMonths.add(getText(month));
            }

            System.out.println(date.split(" ")[1]);
            System.out.println(listOfMonths);

            clickOn(driver, lstDateMonths.get(listOfMonths.indexOf(date.split(" ")[1])));
        }

        for (WebElement day : lstDateMonthDates) {
            listOfDays.add(getText(day));
        }
        clickOn(driver, lstDateMonthDates.get(listOfDays.indexOf(date.split(" ")[0])));

    }

    public SuperAgentAgentListVerification filterValidDateRange() {

        scrollToElement(driver, btnClearDateRange);
        clickOn(driver, btnClearDateRange);
        clickOn(driver, txtFromDateFilter);

        if (sizeOf(lstAgentName) > 2) {
            selectDate(_filterAgentDate.get(1));
            selectDate(_filterAgentDate.get(sizeOf(_filterAgentDate) - 1));
        } else {
            selectDate(_filterAgentDate.get(0));
            selectDate(_filterAgentDate.get(1));
        }

        clickOnFilterSearchButton();

        return new SuperAgentAgentListVerification(driver);
    }
}