package com.incashme.superAgent.indexpage;

import com.framework.init.AbstractPage;
import com.incashme.superAgent.verification.SuperAgentDashboardVerification;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rahul R.
 * Date: 2019-03-25
 * Time: 11:48
 * Project Name: InCashMe
 */
public class SuperAgentDashboardIndexPage extends AbstractPage {

    public SuperAgentDashboardIndexPage(WebDriver driver) {
        super(driver);
    }

    public static int _todayAddedAgent = 0;
    public static double _todayCommission = 0.00;
    public static int _targetLeft = 0;
    public static int _totalTarget = 0;
    public static int _daysLeft = 0;
    public static int _totalDays = 0;
    public static int _totalAddedAgent = 0;

    public static String _agentName = "";
    public static String _agentId = "";

    public static int _targetAchievedInAgentDetails = 0;
    public static int _targetLeftInAgentDetails = 0;
    public static int _totalTargetInAgentDetails = 0;
    public static int _daysCompletedInAgentDetails = 0;
    public static int _daysLeftInAgentDetails = 0;
    public static int _totalDaysInAgentDetails = 0;

    public static List<String> _agentNamesBeforeSort = new ArrayList<>();
    public static List<String> _agentNumberBeforeSort = new ArrayList<>();
    public static List<String> _agentStatusBeforeSort = new ArrayList<>();
    public static List<Integer> _agentTopupAssignedBeforeSort = new ArrayList<>();
    public static List<Integer> _agentTopupCompletedBeforeSort = new ArrayList<>();
    public static List<Integer> _agentTopupTargetDaysBeforeSort = new ArrayList<>();
    public static List<LocalDateTime> _agentDateTimeBeforeSort = new ArrayList<>();

    public static String _filterAgentFirstName = "";
    public static String _filterAgentLastName = "";
    public static String _filterAgentPhone = "";
    public static String _filterAgentId = "";
    public static String _filterRandomValue = "";
    public static String _filterStatus = "";

    public static boolean _isRandomFilter;

    public static String _searchCriteria;

    public static List<Double> _commissionTopUpBeforeSort = new ArrayList<>();
    public static List<Double> _commissionAmountBeforeSort = new ArrayList<>();
    public static List<LocalDateTime> _commissionDateTimeBeforeSort = new ArrayList<>();


    @FindBy(xpath = "//div/button/i[contains(@class,'mnu-opn')]")
    private WebElement btnExpandMenu;

    @FindBy(xpath = "//ul/li[contains(@id,'sb_home')]")
    private WebElement btnMenuHome;

    @FindBy(xpath = "//ul/li[contains(@id,'sb_bal')]")
    private WebElement btnMenuBalance;

    @FindBy(xpath = "//ul/li[contains(@id,'sb_comms')]")
    private WebElement btnMenuCommission;

    @FindBy(xpath = "//ul/li[contains(@id,'sb_agents')]")
    private WebElement btnMenuAgentsList;

    @FindBy(xpath = ".//div[contains(@class,'user-avatar')]//a")
    private WebElement btnUserProfile;

    @FindBy(xpath = "//div[@class='profile-details']/ancestor::div//button[contains(@class,'close')]")
    private WebElement btnCloseProfilePopUp;

    @FindBy(xpath = "//*[text()='Change Password']/ancestor::div//button[contains(@class,'close')]")
    private WebElement btnClosePasswordPopup;

    @FindBy(xpath = "//div//ul//li//a[contains(text(),'Change Password')]")
    private WebElement btnChangePassword;

    @FindBy(xpath = "//div//ul//li//a[contains(text(),'Privacy')]")
    private WebElement btnPrivacyPolicy;

    @FindBy(xpath = "//div//ul//li//a[contains(text(),'Terms')]")
    private WebElement btnTermsConditions;

    @FindBy(xpath = "//div//ul//li//a[contains(text(),'Login')]")
    private WebElement btnLoginHistory;

    @FindBy(xpath = "//input[@formcontrolname='oldpassword']")
    private WebElement txtCurrentPassword;

    @FindBy(xpath = "//input[@formcontrolname='password2']")
    private WebElement txtNewPassword;

    @FindBy(xpath = "//input[@formcontrolname='password']")
    private WebElement txtConfirmPassword;

    @FindBy(xpath = ".//div[contains(@class,'login-form')]//button[@type='submit']")
    private WebElement btnSubmit;

    @FindBy(xpath = "//div[contains(text(),'Added New Agent')]//following-sibling::div")
    private WebElement lblAddedNewAgent;

    @FindBy(xpath = "//div[contains(text(),'Today Commission')]//following-sibling::div")
    private WebElement lblTodayCommission;

    @FindBy(xpath = "//div[contains(text(),'Target Left')]//following-sibling::div")
    private WebElement lblCreateAgentTargetLeft;

    @FindBy(xpath = "//div[text()='Target']//following-sibling::div")
    private WebElement lblCreateAgentTarget;

    @FindBy(xpath = "//div[contains(text(),'Days left')]//following-sibling::div")
    private WebElement lblTargetCreateTimeLeft;

    @FindBy(xpath = "//div[contains(text(),'Total Days')]//following-sibling::div")
    private WebElement lblTargetTotalDays;

    @FindBy(xpath = "//div//p[contains(text(),'no data alive')]")
    private WebElement lblNoAgentCommission;

    @FindBy(xpath = "//div//p[contains(text(),'No agents found')]")
    private WebElement lblNoAgentsFound;

    @FindBy(xpath = "//li/a[contains(text(),'Back')]")
    private WebElement btnBack;

    @FindBy(xpath = "//div[@id='agentChartCon']//following-sibling::span")
    private WebElement lblTotalAddedAgent;

    @FindBy(xpath = "//div[contains(text(),'Agents')]//following-sibling::div//span[contains(@class,'chart-velue')]")
    private WebElement lblAddedAgentInChart;

    @FindBy(xpath = "//div[contains(text(),'Target Left')]/following-sibling::div")
    private WebElement lblTaregtLeftInChart;

    @FindBy(xpath = "//div[contains(text(),'Total Target') or contains(text(),'Target Achieved')]/following-sibling::div")
    private WebElement lblTotalTargetInChart;

    @FindBy(xpath = "//div[contains(text(),'Time')]//following-sibling::div//span[contains(@class,'chart-velue')]")
    private WebElement lblDaysCompletedInChart;

    @FindBy(xpath = "//div[contains(text(),'Days Left')]/following-sibling::div")
    private WebElement lblDaysLeftInChart;

    @FindBy(xpath = "//div[text()='Target']/following-sibling::div")
    private WebElement lblTargetDaysInChart;

    @FindBy(xpath = "//li/a[contains(text(),'Agent List')]")
    private WebElement btnAgentList;

    @FindBy(xpath = "//li/button[contains(@class,'filterBtn')]")
    private WebElement btnFilter;

    @FindBy(xpath = "//table[@id='dataTblSA']//th[1]")
    private WebElement lblAgentNameHeader;

    @FindBy(xpath = "//table[@id='dataTblSA']//th[2]")
    private WebElement lblAgentNumberHeader;

    @FindBy(xpath = "//table[@id='dataTblSA']//th[3]")
    private WebElement lblAgentStatusHeader;

    @FindBy(xpath = "//table[@id='dataTblSA']//th[5]")
    private WebElement lblAgentDateTimeHeader;

    @FindBy(xpath = "//div//a[contains(text(),'Topups Status')]")
    private WebElement btnConsumerTopupStatus;

    @FindBy(xpath = "//div//a[contains(text(),'Consumers Status')]")
    private WebElement btnCreateConsumerStatus;

    @FindBy(xpath = "//div//a[contains(text(),'Merchants Status')]")
    private WebElement btnCreateMerchantStatus;

    @FindBy(xpath = "//a/h3[contains(text(),'Ontime')]")
    private WebElement btnStatusOnTime;

    @FindBy(xpath = "//a/h3[contains(text(),'Ontime')]/following-sibling::span")
    private WebElement lblOnTimeCount;

    @FindBy(xpath = "//a/h3[contains(text(),'Delay')]")
    private WebElement btnStatusDelay;

    @FindBy(xpath = "//a/h3[contains(text(),'Delay')]/following-sibling::span")
    private WebElement lblDelayCount;

    @FindBy(xpath = "//a/h3[contains(text(),'Completed')]")
    private WebElement btnStatusCompleted;

    @FindBy(xpath = "//a/h3[contains(text(),'Completed')]/following-sibling::span")
    private WebElement lblCompletedCount;

    @FindBy(xpath = "//table[@id='agentListTbl']//th[2]")
    private WebElement lblStatusAgentNumberHeader;

    @FindBy(xpath = "//table[@id='agentListTbl']//th[3]")
    private WebElement lblStatusTopupAssignedHeader;

    @FindBy(xpath = "//table[@id='agentListTbl']//th[4]")
    private WebElement lblStatusTopupCompletedHeader;

    @FindBy(xpath = "//table[@id='agentListTbl']//th[5]")
    private WebElement lblStatusTargetDaysHeader;

    @FindBy(xpath = "//form//input[@id='fname']")
    private WebElement lblAgentProfileFirstName;

    @FindBy(xpath = "//form//input[@id='lname']")
    private WebElement lblAgentProfileLastName;

    @FindBy(xpath = "//form//input[@id='dob']")
    private WebElement lblAgentProfileDOB;

    @FindBy(xpath = "//form//input[@id='phno']")
    private WebElement lblAgentProfilePhone;

    @FindBy(xpath = "//form//input[@id='email']")
    private WebElement lblAgentProfileEmail;

    @FindBy(xpath = "//div[contains(@class,'spr-usr-id')]")
    private WebElement lblAgentProfileID;

    @FindBy(xpath = "//input[@formcontrolname='fname']")
    private WebElement txtFilterFirstName;

    @FindBy(xpath = "//input[@formcontrolname='lname']")
    private WebElement txtFilterLastName;

    @FindBy(xpath = "//input[@formcontrolname='id']")
    private WebElement txtFilterAgentID;

    @FindBy(xpath = "//input[@formcontrolname='phone']")
    private WebElement txtFilterAgentMobile;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//button[contains(@class,'searchbtn')]")
    private WebElement btnFilterSearch;

    @FindBy(xpath = "//div[contains(@class,'filter_view')]//a[contains(text(),'Clear All')]")
    private WebElement btnFilterClearAll;

    @FindBy(xpath = "//mdb-select[@formcontrolname='status']")
    private WebElement btnStatusDropdown;

    @FindBy(xpath = "//mdb-date-picker[@formcontrolname='date_begin']//input")
    private WebElement btnFilterDateBegin;

    @FindBy(xpath = "//mdb-date-picker[@formcontrolname='date_end']//input")
    private WebElement btnFilterDateEnd;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTblSA']//tr//td[2]//div")})
    private List<WebElement> lstAgentMobileNumber;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTblSA']//tr//td[3]//div")})
    private List<WebElement> lstAgentStatus;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTblSA']//tr//td[5]//div")})
    private List<WebElement> lstAgentCreatedDateTime;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTblSA']//tr//td[6]//a")})
    private List<WebElement> lstMOREButton;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTblSA']//tr//td[1]//h5")})
    private List<WebElement> lstAgentName;

    @FindAll(value = {@FindBy(xpath = "//table[@id='dataTblSA']//tr//td[1]//following-sibling::div")})
    private List<WebElement> lstAgentId;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@class,'add-user-list')]//h5")})
    private List<WebElement> lstAddedAgentNames;

    @FindAll(value = {@FindBy(xpath = "//div[contains(@class,'add-user-list')]//div[contains(text(),'ID')]")})
    private List<WebElement> lstAddedAgentId;

    @FindAll(value = {@FindBy(xpath = "//mdb-select[@formcontrolname='status']//li/span")})
    private List<WebElement> lstStatuses;

    @FindAll(value = {@FindBy(xpath = "//table[@id='agentListTbl']//tr//td[2]")})
    private List<WebElement> lstStatusAgentMobile;

    @FindAll(value = {@FindBy(xpath = "//table[@id='agentListTbl']//tr//td[3]")})
    private List<WebElement> lstStatusAgentAssigned;

    @FindAll(value = {@FindBy(xpath = "//table[@id='agentListTbl']//tr//td[4]")})
    private List<WebElement> lstStatusAgentCompleted;

    @FindAll(value = {@FindBy(xpath = "//table[@id='agentListTbl']//tr//td[5]")})
    private List<WebElement> lstStatusAgentTargetDays;

    @FindBy(xpath = "//div//a[contains(text(),'New Agent')]")
    private WebElement btnCreateNewAgent;

    @FindBy(xpath = "//table//tr[1]/td[6]")
    private WebElement lblLastLoginTime;

    @FindBy(xpath = "//table//tr[1]/td[5]")
    private WebElement lblUserAgent;

    @FindBy(xpath = "//table//tr[1]/td[4]")
    private WebElement lblIpAddress;

    @FindBy(xpath = "//table//tr[1]/td[3]")
    private WebElement lblAppVersion;

    @FindBy(xpath = "//table//tr[1]/td[2]")
    private WebElement lblSources;

    @FindBy(xpath = "//input[@type='search']")
    private WebElement txtInputSearch;

    @FindBy(xpath = "//div[contains(@id,'usersTbl2_filter')]//input[@type='search']")
    private WebElement txtPopupInputSearch;

    @FindBy(xpath = "//table//tr[1]/td[1]")
    private WebElement lblLoginId;

    @FindAll(value = {@FindBy(xpath = "//table[@id='superAgentTransactions']//tr//td[1]//h5/span")})
    private List<WebElement> lstCommissionAgentNames;

    @FindAll(value = {@FindBy(xpath = "//table[@id='superAgentTransactions']//tr//td[1]//following-sibling::div")})
    private List<WebElement> lstCommissionAgentId;

    @FindAll(value = {@FindBy(xpath = "//table[@id='superAgentTransactions']//tr//td[2]/div")})
    private List<WebElement> lstCommissionTopUp;

    @FindAll(value = {@FindBy(xpath = "//table[@id='superAgentTransactions']//tr//td[3]/div")})
    private List<WebElement> lstCommissionAmount;

    @FindAll(value = {@FindBy(xpath = "//table[@id='superAgentTransactions']//tr//td[4]/div")})
    private List<WebElement> lstCommissionTxTime;

    @FindBy(xpath = "//table[@id='superAgentTransactions']//th[2]")
    private WebElement lblTopupHeader;

    @FindBy(xpath = "//table[@id='superAgentTransactions']//th[3]")
    private WebElement lblCommissionHeader;

    @FindBy(xpath = "//table[@id='superAgentTransactions']//th[4]")
    private WebElement lblCommissionDateTimeHeader;

    @FindBy(xpath = "//div//a[contains(text(),'Clear Date Range')]")
    private WebElement btnClearDateRange;


    public SuperAgentDashboardVerification expandDashboardMenu() {

        testStepsLog(_logStep++, "Click on Expand Menu icon.");
        clickOn(driver, btnExpandMenu);

        return new SuperAgentDashboardVerification(driver);
    }

    public SuperAgentDashboardVerification clickOnBalanceMenu() {

        testStepsLog(_logStep++, "Click on Balance Menu icon.");
        clickOn(driver, btnMenuBalance);

        return new SuperAgentDashboardVerification(driver);
    }

    public SuperAgentDashboardVerification clickOnCommissionMenu() {

        testStepsLog(_logStep++, "Click on Commission Menu icon.");
        clickOn(driver, btnMenuCommission);

        return new SuperAgentDashboardVerification(driver);
    }

    public SuperAgentDashboardVerification clickOnAgentsListMenu() {

        testStepsLog(_logStep++, "Click on Agents List Menu icon.");
        clickOn(driver, btnMenuAgentsList);

        return new SuperAgentDashboardVerification(driver);
    }

    public SuperAgentDashboardVerification openUserProfile() {

        pause(3);

        testStepsLog(_logStep++, "Click on User Avatar icon.");
        clickOn(driver, btnUserProfile);

        return new SuperAgentDashboardVerification(driver);
    }

    public SuperAgentDashboardVerification closeProfilePopUp() {

        testStepsLog(_logStep++, "Click on Close button.");
        clickOn(driver, btnCloseProfilePopUp);

        return new SuperAgentDashboardVerification(driver);

    }

    public SuperAgentDashboardVerification clickOnChangePassword() {

        testStepsLog(_logStep++, "Click on Change Password button.");
        clickOn(driver, btnChangePassword);

        return new SuperAgentDashboardVerification(driver);
    }

    public SuperAgentDashboardVerification closePasswordPopUp() {

        testStepsLog(_logStep++, "Click on Close button.");
        clickOn(driver, btnClosePasswordPopup);

        return new SuperAgentDashboardVerification(driver);
    }

    public SuperAgentDashboardVerification clickOnPrivacyPolicy() {

        testStepsLog(_logStep++, "Click on Privacy Policy button.");
        clickOn(driver, btnPrivacyPolicy);

        return new SuperAgentDashboardVerification(driver);
    }

    public SuperAgentDashboardVerification clickOnTermsCondition() {

        testStepsLog(_logStep++, "Click on Terms & Condition button.");
        clickOn(driver, btnTermsConditions);

        return new SuperAgentDashboardVerification(driver);
    }

    public SuperAgentDashboardVerification clickOnLoginHistory() {

        testStepsLog(_logStep++, "Click on Login History button.");
        clickOn(driver, btnLoginHistory);

        return new SuperAgentDashboardVerification(driver);
    }

    public SuperAgentDashboardVerification changeInvalidCurrentPassword() {

        String randomPassword = getRandomPassword();

        enterCurrentPassword(getRandomPassword());
        enterNewPassword(randomPassword);
        enterConfirmNewPassword(randomPassword);

        clickOnSubmitButton();

        return new SuperAgentDashboardVerification(driver);
    }

    private void enterNewPassword(String newPassword) {
        testStepsLog(_logStep++, "Enter New Password.");
        testInfoLog("New Password", newPassword);
        type(txtNewPassword, newPassword);
    }

    private void enterCurrentPassword(String currentPassword) {
        testStepsLog(_logStep++, "Enter Current Password.");
        testInfoLog("Current Password", currentPassword);
        type(txtCurrentPassword, currentPassword);
    }

    private void enterConfirmNewPassword(String confirmPassword) {
        testStepsLog(_logStep++, "Enter Confirm Password.");
        testInfoLog("Confirm Password", confirmPassword);
        type(txtConfirmPassword, confirmPassword);
    }

    private void clickOnSubmitButton() {

        testStepsLog(_logStep++, "Click on Submit button.");
        clickOn(driver, btnSubmit);

    }

    public SuperAgentDashboardVerification changeInvalidNewPassword() {

        String randomPassword = String.valueOf(getRandomNumber());

        enterCurrentPassword(password);
        enterNewPassword(randomPassword);
        enterConfirmNewPassword(randomPassword);

        return new SuperAgentDashboardVerification(driver);

    }

    public SuperAgentDashboardVerification changeInvalidConfirmPassword() {

        enterCurrentPassword(password);
        enterNewPassword(getRandomPassword());
        enterConfirmNewPassword(String.valueOf(getRandomNumber()));

        pause(1);

        clickOnSubmitButton();

        return new SuperAgentDashboardVerification(driver);

    }

    public SuperAgentDashboardVerification changeAsPastPassword() {

        enterCurrentPassword(password);
        enterNewPassword(password);
        enterConfirmNewPassword(password);

        clickOnSubmitButton();

        return new SuperAgentDashboardVerification(driver);

    }

    public SuperAgentDashboardVerification changeNewPassword() {

        String newPassword = "Baps@" + updatedPassword(password);

        System.out.println(newPassword);

        enterCurrentPassword(password);
        enterNewPassword(newPassword);
        enterConfirmNewPassword(newPassword);

        //TODO
        //Need to sort how to change password and update
        setPassword("Super Agent", "superAgentPassword", newPassword);

        clickOnSubmitButton();

        return new SuperAgentDashboardVerification(driver);

    }

    private int updatedPassword(String oldPassword) {
        return getIntegerFromString(oldPassword) + 1;
    }

    public SuperAgentDashboardVerification getTodayAgentDetailsFromDashboard() {

        pause(2);

        _todayAddedAgent = getIntegerFromString(getText(lblAddedNewAgent));
        _todayCommission = getDoubleFromString(getText(lblTodayCommission));
        _targetLeft = getIntegerFromString(getText(lblCreateAgentTargetLeft));
        _totalTarget = getIntegerFromString(getText(lblCreateAgentTarget));
        _daysLeft = getIntegerFromString(getText(lblTargetCreateTimeLeft));
        _totalDays = getIntegerFromString(getText(lblTargetTotalDays));
        _totalAddedAgent = getIntegerFromString(getText(lblTotalAddedAgent));

        return new SuperAgentDashboardVerification(driver);

    }

    public boolean isCommissionDataDisplay() {
        return _todayCommission != 0.00;
    }

    public SuperAgentDashboardVerification openAddedAgentListFromDashboard() {

        scrollToElement(driver, lblAddedNewAgent);
        testStepsLog(_logStep++, "Click on Added New Agent.");
        clickOn(driver, lblAddedNewAgent);

        return new SuperAgentDashboardVerification(driver);
    }

    public boolean isAgentListDisplay() {
        return _targetAchievedInAgentDetails != 0;
    }

    public boolean isTodayAgentListDisplay() {
        return _todayAddedAgent != 0;
    }

    public SuperAgentDashboardVerification openAddedAgent() {

        _agentName = getText(lstAddedAgentNames.get(0));
        _agentId = getText(lstAddedAgentId.get(0));

        testStepsLog(_logStep++, "Click on Any agent.");
        clickOn(driver, lstAddedAgentNames.get(0));

        return new SuperAgentDashboardVerification(driver);
    }

    public SuperAgentDashboardVerification clickOnBackButton() {

        testStepsLog(_logStep++, "Click on Back button.");
        clickOn(driver, btnBack);

        return new SuperAgentDashboardVerification(driver);
    }

    public SuperAgentDashboardVerification openAgentTargetDetails() {

        testStepsLog(_logStep++, "Click Target Section.");
        clickOn(driver, lblTotalAddedAgent);

        return new SuperAgentDashboardVerification(driver);
    }

    public SuperAgentDashboardVerification getTotalAgentDetails() {

        pause(5);

        _targetAchievedInAgentDetails = getIntegerFromString(getInnerText(lblAddedAgentInChart));
        _targetLeftInAgentDetails = getIntegerFromString(getInnerText(lblTaregtLeftInChart));
        _totalTargetInAgentDetails = getIntegerFromString(getInnerText(lblTotalTargetInChart));
        _daysCompletedInAgentDetails = getIntegerFromString(getInnerText(lblDaysCompletedInChart));
        _daysLeftInAgentDetails = getIntegerFromString(getInnerText(lblDaysLeftInChart));
        _totalDaysInAgentDetails = getIntegerFromString(getInnerText(lblTargetDaysInChart));

        return new SuperAgentDashboardVerification(driver);

    }

    public SuperAgentDashboardVerification clickOnAgentListButton() {

        testStepsLog(_logStep++, "Click Agent List button.");
        clickOn(driver, btnAgentList);

        return new SuperAgentDashboardVerification(driver);
    }

    public SuperAgentDashboardVerification clickOnMOREButton() {

        scrollToElement(driver, lstMOREButton.get(0));

        _agentName = getText(lstAgentName.get(0));
        _agentId = getText(lstAgentId.get(0));

        testStepsLog(_logStep++, "Click on MORE button.");
        clickOn(driver, lstMOREButton.get(0));

        return new SuperAgentDashboardVerification(driver);

    }

    public SuperAgentDashboardVerification clickOnFilterButton() {

        testStepsLog(_logStep++, "Click Filter button.");
        clickOn(driver, btnFilter);

        return new SuperAgentDashboardVerification(driver);
    }

    public SuperAgentDashboardVerification clickOnAgentNameToSort() {

        for (WebElement name : lstAgentName) {
            _agentNamesBeforeSort.add(getInnerText(name));
        }

        testStepsLog(_logStep++, "Click On Agent Name to sort.");
        clickOn(driver, lblAgentNameHeader);

        return new SuperAgentDashboardVerification(driver);
    }

    public SuperAgentDashboardVerification clickOnPhoneNumberToSort() {

        for (WebElement cell : lstAgentMobileNumber) {
            _agentNumberBeforeSort.add(getInnerText(cell));
        }

        testStepsLog(_logStep++, "Click On Mobile Number to sort.");
        clickOn(driver, lblAgentNumberHeader);

        return new SuperAgentDashboardVerification(driver);
    }

    public SuperAgentDashboardVerification clickOnStatusToSort() {

        for (WebElement status : lstAgentStatus) {
            _agentStatusBeforeSort.add(getInnerText(status));
        }

        testStepsLog(_logStep++, "Click On Account Status to sort.");
        clickOn(driver, lblAgentStatusHeader);

        return new SuperAgentDashboardVerification(driver);
    }

    public SuperAgentDashboardVerification clickOnDateTimeToSort() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        for (WebElement status : lstAgentCreatedDateTime) {
            _agentDateTimeBeforeSort.add(LocalDateTime.parse(getInnerText(status), format));
        }

        testStepsLog(_logStep++, "Click On Date & Time to sort.");
        clickOn(driver, lblAgentDateTimeHeader);

        return new SuperAgentDashboardVerification(driver);
    }

    public SuperAgentDashboardVerification clickOnConsumerTopupsStatus() {

        testStepsLog(_logStep++, "Click On Consumer Topups Status button.");
        clickOn(driver, btnConsumerTopupStatus);

        return new SuperAgentDashboardVerification(driver);
    }

    public SuperAgentDashboardVerification clickOnCreateConsumerStatus() {

        testStepsLog(_logStep++, "Click On Create Consumer Status button.");
        clickOn(driver, btnCreateConsumerStatus);

        return new SuperAgentDashboardVerification(driver);
    }

    public SuperAgentDashboardVerification clickOnCreateMerchantStatus() {

        testStepsLog(_logStep++, "Click On Create Merchant Status button.");
        clickOn(driver, btnCreateMerchantStatus);

        return new SuperAgentDashboardVerification(driver);
    }

    public boolean isOntimeDisplay() {
        return getIntegerFromString(getText(lblOnTimeCount)) > 0;
    }

    public boolean isDelayDisplay() {
        return getIntegerFromString(getText(lblDelayCount)) > 0;
    }

    public boolean isCompletedDisplay() {
        return getIntegerFromString(getText(lblCompletedCount)) > 0;
    }

    public SuperAgentDashboardVerification clickOnStatusPhoneNumberToSort() {

        updateShowList(driver, findElementByName(driver, "agentListTbl_length"), "All");

        _agentNumberBeforeSort.clear();

        pause(3);

        for (WebElement cell : lstStatusAgentMobile) {
            _agentNumberBeforeSort.add(getInnerText(cell));
        }

        testStepsLog(_logStep++, "Click On Mobile Number to sort.");
        clickOn(driver, lblStatusAgentNumberHeader);

        return new SuperAgentDashboardVerification(driver);
    }

    public SuperAgentDashboardVerification clickOnOnTimeButton() {

        testStepsLog(_logStep++, "Click On Ontime button.");
        clickOn(driver, btnStatusOnTime);

        return new SuperAgentDashboardVerification(driver);
    }

    public SuperAgentDashboardVerification clickOnDelayButton() {

        testStepsLog(_logStep++, "Click On Delay button.");
        clickOn(driver, btnStatusDelay);

        return new SuperAgentDashboardVerification(driver);
    }

    public SuperAgentDashboardVerification clickOnCompletedButton() {

        testStepsLog(_logStep++, "Click On Completed button.");
        clickOn(driver, btnStatusCompleted);

        return new SuperAgentDashboardVerification(driver);
    }

    public SuperAgentDashboardVerification clickOnStatusTopupAssignedToSort() {

        updateShowList(driver, findElementByName(driver, "agentListTbl_length"), "All");

        _agentTopupAssignedBeforeSort.clear();

        for (WebElement cell : lstStatusAgentAssigned) {
            _agentTopupAssignedBeforeSort.add(getIntegerFromString(getInnerText(cell)));
        }

        testStepsLog(_logStep++, "Click On Topup Assigned to sort.");
        clickOn(driver, lblStatusTopupAssignedHeader);

        return new SuperAgentDashboardVerification(driver);
    }

    public SuperAgentDashboardVerification clickOnStatusTopupCompletedToSort() {

        updateShowList(driver, findElementByName(driver, "agentListTbl_length"), "All");

        _agentTopupCompletedBeforeSort.clear();

        for (WebElement cell : lstStatusAgentCompleted) {
            _agentTopupCompletedBeforeSort.add(getIntegerFromString(getInnerText(cell)));
        }

        testStepsLog(_logStep++, "Click On Topup Completed to sort.");
        clickOn(driver, lblStatusTopupCompletedHeader);

        return new SuperAgentDashboardVerification(driver);
    }

    public SuperAgentDashboardVerification clickOnStatusTargetDaysToSort() {

        updateShowList(driver, findElementByName(driver, "agentListTbl_length"), "All");

        _agentTopupTargetDaysBeforeSort.clear();

        for (WebElement cell : lstStatusAgentTargetDays) {
            _agentTopupTargetDaysBeforeSort.add(getIntegerFromString(getInnerText(cell).split("/")[0]));
        }

        testStepsLog(_logStep++, "Click On Target Days to sort.");
        clickOn(driver, lblStatusTargetDaysHeader);
        clickOn(driver, lblStatusTargetDaysHeader);

        return new SuperAgentDashboardVerification(driver);
    }

    public SuperAgentDashboardVerification clickOnDashboardIcon() {

        testStepsLog(_logStep++, "Click on Home Menu icon.");
        clickOn(driver, btnMenuHome);

        return new SuperAgentDashboardVerification(driver);
    }

    public SuperAgentDashboardVerification filterFirstName(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomCharacters(7);
            testStepsLog(_logStep++, "Enter First Name : " + _filterRandomValue);
            type(txtFilterFirstName, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter First Name : " + _filterAgentFirstName);
            type(txtFilterFirstName, _filterAgentFirstName);
        }

        return new SuperAgentDashboardVerification(driver);
    }

    public SuperAgentDashboardVerification getAgentDetailsForFilter() {

        pause(2);

        _filterAgentFirstName = getInnerText(lstAgentName.get(0)).split(" ")[0].trim();
        _filterAgentLastName = getInnerText(lstAgentName.get(0)).replace(_filterAgentFirstName, "").trim();
        _filterAgentId = getInnerText(lstAgentId.get(0));
        // _filterAgentDOB = getInnerText(lblAgentProfileDOB);
        _filterAgentPhone = getInnerText(lstAgentMobileNumber.get(0));
        // _filterAgentEmail = getInnerText(lblAgentProfileEmail);

        System.out.println(_filterAgentFirstName);

        return new SuperAgentDashboardVerification(driver);

    }

    public SuperAgentDashboardVerification clickOnFilterSearchButton() {

        testStepsLog(_logStep++, "Click on Search button.");
        clickOn(driver, btnFilterSearch);

        pause(2);

        try {
            updateShowList(driver, findElementByName(driver, "dataTblSA_length"), "All");
        } catch (Exception ex) {
            System.out.println("No Show view display");
        }


        pause(1);

        return new SuperAgentDashboardVerification(driver);

    }

    public SuperAgentDashboardVerification clearAllFilterDetails() {

        testStepsLog(_logStep++, "Click on Clear All button.");
        clickOn(driver, btnFilterClearAll);

        pause(2);

        updateShowList(driver, findElementByName(driver, "dataTblSA_length"), "All");

        pause(1);

        return new SuperAgentDashboardVerification(driver);

    }

    public SuperAgentDashboardVerification filterLastName(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomCharacters(7);
            testStepsLog(_logStep++, "Enter Last Name : " + _filterRandomValue);
            type(txtFilterLastName, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter Last Name : " + _filterAgentLastName);
            type(txtFilterLastName, _filterAgentLastName);
        }

        return new SuperAgentDashboardVerification(driver);
    }

    public SuperAgentDashboardVerification filterAgentId(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomNumber() + "" + getRandomNumber();
            testStepsLog(_logStep++, "Enter Random ID : " + _filterRandomValue);
            type(txtFilterAgentID, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter Agent ID : " + _filterAgentId);
            type(txtFilterAgentID, _filterAgentId);
        }

        return new SuperAgentDashboardVerification(driver);
    }

    public SuperAgentDashboardVerification filterAgentNumber(boolean isInvalid) {

        _isRandomFilter = isInvalid;

        if (isInvalid) {
            _filterRandomValue = getRandomNumber() + "" + getRandomNumber();
            testStepsLog(_logStep++, "Enter Mobile Number : " + _filterRandomValue);
            type(txtFilterAgentMobile, _filterRandomValue);
        } else {
            testStepsLog(_logStep++, "Enter Mobile Number : " + _filterAgentPhone.split(" ")[1]);
            type(txtFilterAgentMobile, _filterAgentPhone.split(" ")[1]);
        }

        return new SuperAgentDashboardVerification(driver);
    }

    public SuperAgentDashboardVerification filterStatus(String status) {

        scrollToElement(driver, btnClearDateRange);

        clickOn(driver, btnStatusDropdown);

        _filterStatus = status;

        switch (status.toLowerCase()) {
            case "active":
                clickOn(driver, lstStatuses.get(1));
                break;
            case "kyc pending":
                clickOn(driver, lstStatuses.get(2));
                break;
            case "login pending":
                clickOn(driver, lstStatuses.get(3));
                break;
            default:
                clickOn(driver, lstStatuses.get(0));
                break;
        }

        testStepsLog(_logStep++, "Select Status : " + status);

        return new SuperAgentDashboardVerification(driver);

    }

    public SuperAgentDashboardVerification setAgentStatus() {

        _agentStatusBeforeSort.clear();

        for (WebElement status : lstAgentStatus) {
            _agentStatusBeforeSort.add(getInnerText(status));
        }

        return new SuperAgentDashboardVerification(driver);
    }

    public SuperAgentDashboardVerification clickOnCreateNewAgentButton() {

        pause(2);

        scrollToElement(driver, btnCreateNewAgent);

        testStepsLog(_logStep++, "Click On Create A new Agent button.");
        clickOn(driver, btnCreateNewAgent);

        return new SuperAgentDashboardVerification(driver);
    }


    public SuperAgentDashboardVerification enterLoginIDSearch() {

        testStepsLog(_logStep++, "Enter Login ID to search.");
        type(txtInputSearch, getRandomCharacters(10));

        return new SuperAgentDashboardVerification(driver);

    }


    public SuperAgentDashboardVerification clickTopUpToSort() {

        for (WebElement name : lstCommissionTopUp) {
            _commissionTopUpBeforeSort.add(getDoubleFromString(getInnerText(name)));
        }

        testStepsLog(_logStep++, "Click On Top up to sort.");
        clickOn(driver, lblTopupHeader);

        return new SuperAgentDashboardVerification(driver);
    }

    public SuperAgentDashboardVerification clickCommissionToSort() {

        for (WebElement name : lstCommissionAmount) {
            _commissionAmountBeforeSort.add(getDoubleFromString(getInnerText(name)));
        }

        testStepsLog(_logStep++, "Click On Commission to sort.");
        clickOn(driver, lblCommissionHeader);

        return new SuperAgentDashboardVerification(driver);
    }

    public SuperAgentDashboardVerification clickOnCommissionDateTimeToSort() {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy h:mm:ss a");

        for (WebElement status : lstCommissionTxTime) {
            _commissionDateTimeBeforeSort.add(LocalDateTime.parse(getInnerText(status), format));
        }

        testStepsLog(_logStep++, "Click On Date & Time to sort.");
        clickOn(driver, lblCommissionDateTimeHeader);

        return new SuperAgentDashboardVerification(driver);
    }

    public SuperAgentDashboardVerification enterSearchCriteria(boolean isInvalid) {

        if (isInvalid) {

            scrollToElement(driver, txtInputSearch);

            String searchCriteria = getRandomCharacters(10);
            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", searchCriteria);
            type(txtInputSearch, searchCriteria);

        } else {

            scrollToElement(driver, txtInputSearch);

            _searchCriteria = getInnerText(lstCommissionTxTime.get(0)).substring(0, 10);

            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", _searchCriteria);
            type(txtInputSearch, _searchCriteria);
        }

        return new SuperAgentDashboardVerification(driver);
    }

    public SuperAgentDashboardVerification enterAgentSearchCriteria(boolean isInvalid) {

        if (isInvalid) {

            scrollToElement(driver, txtInputSearch);

            String searchCriteria = getRandomCharacters(10);
            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", searchCriteria);
            type(txtInputSearch, searchCriteria);

        } else {

            scrollToElement(driver, txtInputSearch);

            _searchCriteria = getInnerText(lstAddedAgentId.get(0));

            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", _searchCriteria);
            type(txtInputSearch, _searchCriteria);
        }

        return new SuperAgentDashboardVerification(driver);
    }

    public SuperAgentDashboardVerification enterAgentListSearchCriteria(boolean isInvalid) {

        if (isInvalid) {

            scrollToElement(driver, txtPopupInputSearch);

            String searchCriteria = getRandomCharacters(10);
            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", searchCriteria);
            type(txtPopupInputSearch, searchCriteria);

        } else {

            scrollToElement(driver, txtPopupInputSearch);

            _searchCriteria = getInnerText(lstAddedAgentId.get(0));

            testStepsLog(_logStep++, "Enter Search Criteria");
            testInfoLog("Search", _searchCriteria);
            type(txtPopupInputSearch, _searchCriteria);
        }

        return new SuperAgentDashboardVerification(driver);
    }
}
